# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2024-03-01 17:01+0100\n"
"PO-Revision-Date: 2023-05-21 21:21+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mkdir"
msgstr "mkdir"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mkdir, mkdirat - create a directory"
msgstr "mkdir, mkdirat - maak een map"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#.  .B #include <unistd.h>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr "B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int mkdir(const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"
msgstr "B<int mkdir(const char *>I<padnaam>B<, mode_t >I<modus>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definitie van AT_* constanten */\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int mkdirat(int >I<dirfd>B<, const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"
msgstr "B<int mkdirat(int >I<map_bi>B<, const char *>I<padnaam>B<, mode_t >I<modus>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Feature Test Macro´s eisen in  glibc (zie B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mkdirat>():"
msgstr "B<mkdirat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""
"    Vanaf glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Voor glibc 2.10:\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mkdir>()  attempts to create a directory named I<pathname>."
msgstr "B<mkdir>() probeert een map te maken met de naam I<padnaam>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The argument I<mode> specifies the mode for the new directory (see "
"B<inode>(7)).  It is modified by the process's I<umask> in the usual way: in "
"the absence of a default ACL, the mode of the created directory is (I<mode> "
"& \\[ti]I<umask> & 0777).  Whether other I<mode> bits are honored for the "
"created directory depends on the operating system.  For Linux, see NOTES "
"below."
msgstr ""
"Het argument I<modus> specificeert de modus voor de nieuwe map (zie "
"B<inode>(7)). Deze wordt aangepast door het I<umask> van het proces op de "
"gebruikelijke wijze: in de afwezigheid van een standaard ACL, is de modus "
"van de aangemaakte map (I<mode> & \\[ti]I<umask> & 0777).  Of de andere "
"I<modus> bits worden in acht genomen voor de aangemaakte map is afhankelijk "
"van het operating systeem. Voor Linux, zie OPMERKINGEN hieronder."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The newly created directory will be owned by the effective user ID of the "
"process.  If the directory containing the file has the set-group-ID bit set, "
"or if the filesystem is mounted with BSD group semantics (I<mount -o "
"bsdgroups> or, synonymously I<mount -o grpid>), the new directory will "
"inherit the group ownership from its parent; otherwise it will be owned by "
"the effective group ID of the process."
msgstr ""
"De nieuw gemaakte map zal eigendom worden van het geldende gebruiker ID van "
"het proces. Als de map die het bestand bevat het set-group-ID bit gezet "
"heeft, of als het bestandsysteem is gekoppeld met BSD groep regels ,  "
"(I<mount -o bsdgroups> of, synoniem I<mount -o grpid>) dan zal de nieuwe map "
"het groep eigendom van zijn ouder-map erven; anders zal het eigendom worden "
"van het geldende groep-id van het proces."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the parent directory has the set-group-ID bit set, then so will the newly "
"created directory."
msgstr ""
"Al de ouder-map het zet-groep-id bit gezet heeft, dan zal dat ook gezet "
"worden voor de nieuw gemaakte map."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mkdirat()"
msgstr "mkdirat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mkdirat>()  system call operates in exactly the same way as "
"B<mkdir>(), except for the differences described here."
msgstr ""
"De B<mkdirat>() systeem aanroep werkt op precies dezelfde manier als "
"B<mkdir>(), behalve voor de hieronder beschreven verschillen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<mkdir>()  for a relative pathname)."
msgstr ""
"Als de padnaam in I<padnaam> relatief is, dan wordt deze geïnterpreteerd "
"relatief aan de map aangewezen door de bestand indicator I<dirfd> (liever "
"dan relatief aan de huidige werkmap van het aanroepende proces, zoals gedaan "
"door B<mkdir>() voor een relatieve padnaam)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<pathname> is relative and I<dirfd> is the special value B<AT_FDCWD>, "
"then I<pathname> is interpreted relative to the current working directory of "
"the calling process (like B<mkdir>())."
msgstr ""
"Als I<padnaam> relatief is en I<dirfd> heeft de speciale waarde B<AT_FDCWD>, "
"dan wordt I<padnaam> geïnterpreteerd relatief aan de huidige werkmap van het "
"aanroepende proces (zoals B<mkdir>())."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<pathname> is absolute, then I<dirfd> is ignored."
msgstr "Als I<padnaam> absoluut is, dan wordt I<mapbi> genegeerd."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<mkdirat>()."
msgstr "Zie B<openat>(2) voor een uitleg over de noodzaak van B<mkdirat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mkdir>()  and B<mkdirat>()  return zero on success.  On error, -1 is "
"returned and I<errno> is set to indicate the error."
msgstr ""
"B<mkdir>() en B<mkdirat>() geven nul terug bij slagen, of een -1 als een "
"fout optrad en wordt I<errno> overeenkomstig gezet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The parent directory does not allow write permission to the process, or one "
"of the directories in I<pathname> did not allow search permission.  (See "
"also B<path_resolution>(7).)"
msgstr ""
"De ouder map status schrijf toestemming niet toe voor het proces, of een van "
"de mappen in I<padnaam> stond zoek (uitvoer) toestemming niet toe."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<mkdirat>())  I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> "
"nor a valid file descriptor."
msgstr ""
"(B<mkdirat>())  I<padnaam> is relatiet maar  I<dirfd> is noch B<AT_FDCWD> "
"noch een geldige bestandsindicator."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The user's quota of disk blocks or inodes on the filesystem has been "
"exhausted."
msgstr ""
"De gebruiker quota aan schijf blokken of inodes van het bestandssysteem is "
"uitgeput. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> already exists (not necessarily as a directory).  This includes "
"the case where I<pathname> is a symbolic link, dangling or not."
msgstr ""
"I<padnaam> bestaat al (niet perse een map). Dit bevat ook het geval waar "
"I<padnaam> een symbolische koppeling is, bungelend of niet. "

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> points outside your accessible address space."
msgstr "I<padnaam> wijst buiten de voor u toegankelijke adresruimte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The final component (\"basename\") of the new directory's I<pathname> is "
"invalid (e.g., it contains characters not permitted by the underlying "
"filesystem)."
msgstr ""
"De uiteindelijke component (\"basisnaam\") van de nieuwe map I<padnaam> is "
"ongeldig (b.v. het bevat tekens die niet toegestaan zijn in het "
"onderliggende bestandssysteem)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in resolving I<pathname>."
msgstr ""
"Teveel symbolische koppelingen werden tegengekomen bij het vaststellen van "
"I<padnaam>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EMLINK>"
msgstr "B<EMLINK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The number of links to the parent directory would exceed B<LINK_MAX>."
msgstr ""
"Het aantal koppelingen naar de ouder map zou B<LINK_MAX> overschrijden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> was too long."
msgstr "I<padnaam> was te lang."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A directory component in I<pathname> does not exist or is a dangling "
"symbolic link."
msgstr ""
"Een map deel van I<padnaam> bestaat niet of is een loshangende symbolische "
"koppeling."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Onvoldoende kernelgeheugen voorhanden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The device containing I<pathname> has no room for the new directory."
msgstr ""
"Het apparaat dat I<padnaam> bevat heeft geen ruimte voor een nieuwe map."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The new directory cannot be created because the user's disk quota is "
"exhausted."
msgstr ""
"De nieuwe dir kan niet gemaakt worden omdat het schijf quota van de "
"gebruiker uitgeput is."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<pathname> is not, in fact, a directory."
msgstr "Een onderdeel gebruikt als map in I<padnaam> is in feite geen map."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<mkdirat>())  I<pathname> is relative and I<dirfd> is a file descriptor "
"referring to a file other than a directory."
msgstr ""
"(B<mkdirat>())  I<padnaam> is relatief en I<mapbi> is een bestandsindicatoor "
"die naar een bestand wijst dat geen map is."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The filesystem containing I<pathname> does not support the creation of "
"directories."
msgstr ""
"Het bestandssysteen dat I<padnaam> bevat ondersteund de aanmaak van mappen "
"niet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> refers to a file on a read-only filesystem."
msgstr ""
"I<padnaam> verwijst naar een bestand op een alleen-lezen bestandsysteem."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Under Linux, apart from the permission bits, the B<S_ISVTX> I<mode> bit is "
"also honored."
msgstr ""
"Onder Linux, afgezien van de rechten bits, wordt ook het B<S_ISVTX> I<mode>  "
"bit gehonoreerd."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "glibc notes"
msgstr "Glibc-opmerkingen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On older kernels where B<mkdirat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<mkdir>().  When I<pathname> is a "
"relative pathname, glibc constructs a pathname based on the symbolic link in "
"I</proc/self/fd> that corresponds to the I<dirfd> argument."
msgstr ""
"Op ouder kernels waar B<mkdirat>() niet beschikbaar is, valt de glibc "
"omwikkel functie terug op het gebruik van B<mkdir>(). Als I<padnaam> een "
"relatieve padnaam is, dan construeert glibc een padnaam gebaseerd op de "
"symbolische koppeling in I</proc/self/fd> die overeenkomt met het I<dirfd> "
"argument.´ "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mkdir>()"
msgstr "B<mkdir>()"

#.  SVr4 documents additional EIO, EMULTIHOP
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, BSD, POSIX.1-2001."
msgstr "SVr4, BSD, POSIX.1-2001."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mkdirat>()"
msgstr "B<mkdirat>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 2.6.16, glibc 2.4."
msgstr "Linux 2.6.16, glibc 2.4."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"There are many infelicities in the protocol underlying NFS.  Some of these "
"affect B<mkdir>()."
msgstr ""
"Er zijn veel ongelukkigheden in het onderliggende NFS protocol. Sommige van "
"deze beïnvloeden B<mkdir>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mkdir>(1), B<chmod>(2), B<chown>(2), B<mknod>(2), B<mount>(2), "
"B<rmdir>(2), B<stat>(2), B<umask>(2), B<unlink>(2), B<acl>(5), "
"B<path_resolution>(7)"
msgstr ""
"B<mkdir>(1), B<chmod>(2), B<chown>(2), B<mknod>(2), B<mount>(2), "
"B<rmdir>(2), B<stat>(2), B<umask>(2), B<unlink>(2), B<acl>(5), "
"B<path_resolution>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<mkdirat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""
"B<mkdirat()> werd toegevoegd aan Linux 2.6.16; bibliotheek ondersteuning "
"werd toegevoegd aan glibc 2.4."

#.  SVr4 documents additional EIO, EMULTIHOP
#. type: Plain text
#: debian-bookworm
msgid "B<mkdir>(): SVr4, BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<mkdir>(): SVr4, BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid "B<mkdirat>(): POSIX.1-2008."
msgstr "B<mkdirat>(): POSIX.1-2008."

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 maart 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pagina's 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
