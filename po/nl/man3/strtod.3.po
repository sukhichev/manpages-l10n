# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
#
# Jos Boersema <joshb@xs4all.nl>, 2000.
# Joost van Baal <joostv-manpages-nl-2398@mdcc.cx>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2024-03-01 17:09+0100\n"
"PO-Revision-Date: 2023-05-21 22:01+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "strtod"
msgstr "strtod"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "strtod, strtof, strtold - convert ASCII string to floating-point number"
msgstr ""
"strtod, strtof, strtold - zet ASCII string om naar floating point number"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double strtod(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
msgstr ""
"B<double strtod(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<float strtof(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"
"B<long double strtold(const char *restrict >I<nptr>B<, char **restrict >I<endptr>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Feature Test Macro´s eisen in  glibc (zie B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<strtof>(), B<strtold>():"
msgstr "B<strtof>(), B<strtold>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<strtod>(), B<strtof>(), and B<strtold>()  functions convert the "
"initial portion of the string pointed to by I<nptr> to I<double>, I<float>, "
"and I<long double> representation, respectively."
msgstr ""
"De B<strtod>(), B<strtof>() en B<strtold>() functies zetten het eerste deel "
"van de string waar I<nptr> naar wijst om in respectievelijk een B<double>, "
"B<float> en B<long double> voorstelling."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The expected form of the (initial portion of the) string is optional leading "
"white space as recognized by B<isspace>(3), an optional plus (\\[aq]+\\[aq]) "
"or minus sign (\\[aq]-\\[aq]) and then either (i) a decimal number, or (ii) "
"a hexadecimal number, or (iii) an infinity, or (iv) a NAN (not-a-number)."
msgstr ""
"De verwachte vorm voor (het eerste deel van) de string is eventuele "
"voorafgaande witruimte zoals herkend door B<isspace>(3), een optioneel plus "
"(\\[aq]+\\[aq]) of min (\\[aq]-\\[aq]) teken, gevolgd door ofwel (i) een "
"decimaal getal, ofwel (ii) een hexadecimaal getal, ofwel (iii) een "
"oneindigheid ofwel (iv) een NAN (not-a-number, geen-getal)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A I<decimal number> consists of a nonempty sequence of decimal digits "
"possibly containing a radix character (decimal point, locale-dependent, "
"usually \\[aq].\\[aq]), optionally followed by a decimal exponent.  A "
"decimal exponent consists of an \\[aq]E\\[aq] or \\[aq]e\\[aq], followed by "
"an optional plus or minus sign, followed by a nonempty sequence of decimal "
"digits, and indicates multiplication by a power of 10."
msgstr ""
"Een I<decimaal getal> bestaat uit een niet-lege rij van decimale cijfers, "
"mogelijk een radix karakter (een taaldefinitie-afhankelijke decimale punt, "
"gewoonlijk \\[aq].\\[aq]), mogelijk gevolgd door een decimale exponent.  Een "
"decimale exponent bestaat uit een \\[aq]E\\[aq] of \\[aq]e\\[aq] karakter, "
"gevolgd door een optioneel plus of min teken, gevolgd door een niet-lege "
"serie van decimale cijfers, en geeft vermenigvuldiging met een macht van 10 "
"aan."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A I<hexadecimal number> consists of a \"0x\" or \"0X\" followed by a "
"nonempty sequence of hexadecimal digits possibly containing a radix "
"character, optionally followed by a binary exponent.  A binary exponent "
"consists of a \\[aq]P\\[aq] or \\[aq]p\\[aq], followed by an optional plus "
"or minus sign, followed by a nonempty sequence of decimal digits, and "
"indicates multiplication by a power of 2.  At least one of radix character "
"and binary exponent must be present."
msgstr ""
"Een I<hexadecimaal getal> bestaat uit een \"0x\" of \"0X\" gevolgd door een "
"niet-lege rij van hexadecimale cijfers, mogelijk bevattend een radix "
"karakter, mogelijk gevolgd door een binaire exponent.  Een binaire exponent "
"bestaat uit een \\[aq]P\\[aq] of \\[aq]p\\[aq], gevolgd door een optioneel "
"plus- of min-teken, gevolgd door een niet-lege rij van decimale getallen, en "
"geeft een vermenigvuldiging met een macht van 2 aan.  Ten minste één van het "
"radix karakter en de binaire exponent moeten aanwezig zijn."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An I<infinity> is either \"INF\" or \"INFINITY\", disregarding case."
msgstr ""
"Een I<oneindigheid> is òf \"INF\" òf \"INFINITY\"; op gebruik van hoofd- of "
"kleine letters wordt niet gelet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A I<NAN> is \"NAN\" (disregarding case) optionally followed by a string, "
"I<(n-char-sequence)>, where I<n-char-sequence> specifies in an "
"implementation-dependent way the type of NAN (see NOTES)."
msgstr ""
"Een I<NAN> is \"NAN\" (negeer hoofd- of kleine letters) optioneel gevolgd "
"door een tekenreeks, I<(n-char-sequence)>, waar  I<(n-char-sequence)> in een "
"implementatie-afhankelijke manier het type van de NAN specificeert (zie "
"NOTITIES)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "These functions return the converted value, if any."
msgstr "Deze functies geven de omgezette waarde terug, als die er is."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<endptr> is not NULL, a pointer to the character after the last "
"character used in the conversion is stored in the location referenced by "
"I<endptr>."
msgstr ""
"Als I<endptr> niet B<NULL> is, dan wordt een pointer naar het karakter "
"direct achter het laatste in deconversie gebruikte karakter opgeslagen op de "
"locatie waar I<endptr> naar wijst."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no conversion is performed, zero is returned and (unless I<endptr> is "
"null) the value of I<nptr> is stored in the location referenced by I<endptr>."
msgstr ""
"Als geen conversie gedaan is, dat wordt nul teruggegeven, en (tenzij "
"I<endptr> is null) de waarde van I<nptr> is opgeslagen in de locatie waar "
"I<endptr> naar wijst."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the correct value would cause overflow, plus or minus B<HUGE_VAL>, "
"B<HUGE_VALF>, or B<HUGE_VALL> is returned (according to the return type and "
"sign of the value), and B<ERANGE> is stored in I<errno>."
msgstr ""
"Als de correcte waarde overloop zou veroorzaken, dan wordt plus of minus "
"B<HUGE_VAL> (B<HUGE_VALF>, B<HUGE_VALL>) teruggegeven (overeenkomstig het "
"uitvoer type en het teken van de waarde)s, en B<ERANGE> wordt opgeslagen in "
"I<errno>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the correct value would cause underflow, a value with magnitude no larger "
"than B<DBL_MIN>, B<FLT_MIN>, or B<LDBL_MIN> is returned and B<ERANGE> is "
"stored in I<errno>."
msgstr ""
"Als de correcte waarde onderloop zou veroorzaken, dan wordt een waarde niet "
"groter dan de grootte van B<HUGE_VAL> (B<HUGE_VALF>, B<HUGE_VALL>) "
"teruggegeven en B<ERANGE> wordt opgeslagen in I<errno>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Overflow or underflow occurred."
msgstr "Overflow of underflow trad op."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Voor een uitleg van de termen in deze sectie, zie B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribuut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Waarde"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<strtod>(),\n"
"B<strtof>(),\n"
"B<strtold>()"
msgstr ""
"B<strtod>(),\n"
"B<strtof>(),\n"
"B<strtold>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread veiligheid"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe taalgebied"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#.  From glibc 2.8's stdlib/strtod_l.c:
#.      We expect it to be a number which is put in the
#.      mantissa of the number.
#.  It looks as though at least FreeBSD (according to the manual) does
#.  something similar.
#.  C11 says: "An implementation may use the n-char sequence to determine
#. 	extra information to be represented in the NaN's significant."
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the glibc implementation, the I<n-char-sequence> that optionally follows "
"\"NAN\" is interpreted as an integer number (with an optional '0' or '0x' "
"prefix to select base 8 or 16)  that is to be placed in the mantissa "
"component of the returned value."
msgstr ""
"In de glibc implementatie, wordt de  I<n-char-sequence>  die optioneel "
"\"NAN\" volgt geïnterpreteerd als een gehele waarde (met optioneel '0' of "
"'0x' als voorvoegsel om 8 of 16 als basis te selecteren) die geplaatste "
"wordt in de mantisse component van de teruggegeven waarde."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<strtod>()"
msgstr "B<strtod>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<strtof>()"
msgstr "B<strtof>()"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<strtold>()"
msgstr "B<strtold>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since 0 can legitimately be returned on both success and failure, the "
"calling program should set I<errno> to 0 before the call, and then determine "
"if an error occurred by checking whether I<errno> has a nonzero value after "
"the call."
msgstr ""
"Omdat 0 een geldige uitvoerwaarde is zowel bij succes als falen, moet het "
"aanroepende programma I<errno> instellen op 0 voor de aanroep, en vervolgens "
"bepalen of een fout optrad door te controleren of I<errno> een niet-nul "
"waarde heeft na de aanroep."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "VOORBEELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See the example on the B<strtol>(3)  manual page; the use of the functions "
"described in this manual page is similar."
msgstr ""
"Zie het voorbeeld in de B<strtol>(3)  handleiding; het gebruik van de "
"functies beschreven in deze handleiding is vergelijkbaar."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<nan>(3), B<nanf>(3), B<nanl>(3), "
"B<strfromd>(3), B<strtol>(3), B<strtoul>(3)"
msgstr ""
"B<atof>(3), B<atoi>(3), B<atol>(3), B<nan>(3), B<nanf>(3), B<nanl>(3), "
"B<strfromd>(3), B<strtol>(3), B<strtoul>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 juli 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pagina's 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 maart 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
