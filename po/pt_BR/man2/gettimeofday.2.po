# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
# Ricardo C.O.Freitas <english.quest@best-service.com>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:57+0100\n"
"PO-Revision-Date: 2001-06-02 19:20-0300\n"
"Last-Translator: Ricardo C.O.Freitas <english.quest@best-service.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "gettimeofday"
msgstr "gettimeofday"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 outubro 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "gettimeofday, settimeofday - get / set time"
msgstr "gettimeofday, settimeofday - obtém/seleciona a hora"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C Padrão (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/time.hE<gt>>\n"
msgstr "B<#include E<lt>sys/time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int gettimeofday(struct timeval *restrict >I<tv>B<,>\n"
"B<                 struct timezone *_Nullable restrict >I<tz>B<);>\n"
"B<int settimeofday(const struct timeval *>I<tv>B<,>\n"
"B<                 const struct timezone *_Nullable >I<tz>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<settimeofday>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Desde o glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 e anterior:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<gettimeofday> and B<settimeofday> can set the time as well as a "
#| "timezone.  I<tv> is a B<timeval> struct, as specified in /usr/include/sys/"
#| "time.h:"
msgid ""
"The functions B<gettimeofday>()  and B<settimeofday>()  can get and set the "
"time as well as a timezone."
msgstr ""
"B<gettimeofday> e B<settimeofday> podem selecionar tanto o horário quanto o "
"fuso horário. I<tv> é um estrutura B<timeval>, como especificada em /usr/"
"include/sys/time.h:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<tv> argument is a I<struct timeval> (as specified in I<E<lt>sys/time."
"hE<gt>>):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct timeval {\n"
"    time_t      tv_sec;     /* seconds */\n"
"    suseconds_t tv_usec;    /* microseconds */\n"
"};\n"
msgstr ""
"struct timeval {\n"
"    time_t      tv_sec;     /* segundos */\n"
"    suseconds_t tv_usec;    /* microsegundos */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"and gives the number of seconds and microseconds since the Epoch (see "
"B<time>(2))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "and I<tz> is a B<timezone> :"
msgid "The I<tz> argument is a I<struct timezone>:"
msgstr "e I<tz> é um B<fuso horário>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct timezone {\n"
"    int tz_minuteswest;     /* minutes west of Greenwich */\n"
"    int tz_dsttime;         /* type of DST correction */\n"
"};\n"
msgstr ""
"struct timezone {\n"
"    int tz_minuteswest;     /* minutos a Oeste de Greenwich */\n"
"    int tz_dsttime;         /* tipo da correção dst */\n"
"};\n"

#. #-#-#-#-#  archlinux: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .P
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .PP
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#. #-#-#-#-#  debian-unstable: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .PP
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#. #-#-#-#-#  fedora-40: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .P
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .P
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .P
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .PP
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: gettimeofday.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The compilation warning looks to be going away in glibc 2.17
#.  see glibc commit 4b7634a5e03b0da6f8875de9d3f74c1cf6f2a6e8
#.  The following is covered under EPERM below:
#.  .PP
#.  Only the superuser may use
#.  .BR settimeofday ().
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If either I<tv> or I<tz> is null, the corresponding structure is not set "
#| "or returned."
msgid ""
"If either I<tv> or I<tz> is NULL, the corresponding structure is not set or "
"returned.  (However, compilation warnings will result if I<tv> is NULL.)"
msgstr ""
"Se I<tv> ou I<tz> é nulo, a estrutura correspondente não é selecionada ou "
"devolvida."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The use of the I<timezone> structure is obsolete; the I<tz> argument should "
"normally be specified as NULL.  (See NOTES below.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Under Linux there is some peculiar `warp clock' semantics associated to "
#| "the B<settimeofday> system call if on the very first call (after "
#| "booting)  that has a non-NULL I<tz> argument, the I<tv> argument is NULL "
#| "and the I<tz_minuteswest> field is nonzero. In such a case it is assumed "
#| "that the CMOS clock is on local time, and that it has to be incremented "
#| "by this amount to get UTC system time.  No doubt it is a bad idea to use "
#| "this feature."
msgid ""
"Under Linux, there are some peculiar \"warp clock\" semantics associated "
"with the B<settimeofday>()  system call if on the very first call (after "
"booting)  that has a non-NULL I<tz> argument, the I<tv> argument is NULL and "
"the I<tz_minuteswest> field is nonzero.  (The I<tz_dsttime> field should be "
"zero for this case.)  In such a case it is assumed that the CMOS clock is on "
"local time, and that it has to be incremented by this amount to get UTC "
"system time.  No doubt it is a bad idea to use this feature."
msgstr ""
"No Linux há alguma 'distorção peculiar' na semântica associada ao relógio "
"para a chamada de sistema B<settimeofday> se na primeira chamada (depois da "
"inicialização) que tem um argumento não nulo I<tz>, o argumento I<tv> é nulo "
"e o campo I<tz_minuteswest> não é zero. Em tais casos é assumido que o "
"relogío CMOS está em horário local, e que ele deve ser incrementado desta "
"quantia para obter o horário UTC. Nenhuma dúvida que, não é uma boa idéia "
"usar esta característica."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DE RETORNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<gettimeofday>()  and B<settimeofday>()  return 0 for success, or -1 for "
#| "failure (in which case I<errno> is set appropriately)."
msgid ""
"B<gettimeofday>()  and B<settimeofday>()  return 0 for success.  On error, "
"-1 is returned and I<errno> is set to indicate the error."
msgstr ""
"B<gettimeofday>() e B<settimeofday>() em caso de sucesso, devolvem 0. Caso "
"contrário, -1 é devolvido e I<errno> é selecionado adequadamente."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERROS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "One of I<tv> or I<tz> pointed outside your accessible address space."
msgid "One of I<tv> or I<tz> pointed outside the accessible address space."
msgstr ""
"Um dos I<tv> ou I<tz> apontam para fora do seu espaço de endereços acessível."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<settimeofday>()): I<timezone> is invalid."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<settimeofday>()): I<tv.tv_sec> is negative or I<tv.tv_usec> is outside "
"the range [0, 999,999]."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL> (since Linux 4.3)"
msgstr ""

#.  commit e1d7ba8735551ed79c7a0463a042353574b96da3
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<settimeofday>()): An attempt was made to set the time to a value less "
"than the current value of the B<CLOCK_MONOTONIC> clock (see "
"B<clock_gettime>(2))."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process has insufficient privilege to call B<settimeofday>(); "
"under Linux the B<CAP_SYS_TIME> capability is required."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSÕES"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Diferenças entre kernel/biblioteca C"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On some architectures, an implementation of B<gettimeofday>()  is provided "
"in the B<vdso>(7)."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"The kernel accepts NULL for both I<tv> and I<tz>.  The timezone argument is "
"ignored by glibc and musl, and not passed to/from the kernel.  Android's "
"bionic passes the timezone argument to/from the kernel, but Android does not "
"update the kernel timezone based on the device timezone in Settings, so the "
"kernel's timezone is typically UTC."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "PADRÕES"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "gettimeofday"
msgid "B<gettimeofday>()"
msgstr "gettimeofday"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2008."
msgid "POSIX.1-2008 (obsolete)."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "gettimeofday"
msgid "B<settimeofday>()"
msgstr "gettimeofday"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTÓRICO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"SVr4, 4.3BSD.  POSIX.1-2001 describes B<gettimeofday>()  but not "
"B<settimeofday>().  POSIX.1-2008 marks B<gettimeofday>()  as obsolete, "
"recommending the use of B<clock_gettime>(2)  instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Traditionally, the fields of I<struct timeval> were of type I<long>."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "The tz_dsttime field"
msgstr "O campo tz_dsttime"

#.  it has not
#.  been and will not be supported by libc or glibc.
#.  Each and every occurrence of this field in the kernel source
#.  (other than the declaration) is a bug.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On a non-Linux kernel, with glibc, the I<tz_dsttime> field of I<struct "
"timezone> will be set to a nonzero value by B<gettimeofday>()  if the "
"current timezone has ever had or will have a daylight saving rule applied.  "
"In this sense it exactly mirrors the meaning of B<daylight>(3)  for the "
"current zone.  On Linux, with glibc, the setting of the I<tz_dsttime> field "
"of I<struct timezone> has never been used by B<settimeofday>()  or "
"B<gettimeofday>().  Thus, the following is purely of historical interest."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The field I<tz_dsttime> contains a symbolic constant (values are given "
#| "below)  that indicates in which part of the year Daylight Saving Time is "
#| "in force. (Note: its value is constant throughout the year - it does not "
#| "indicate that DST is in force, it just selects an algorithm.)  The "
#| "daylight saving time algorithms defined are as follows :"
msgid ""
"On old systems, the field I<tz_dsttime> contains a symbolic constant (values "
"are given below)  that indicates in which part of the year Daylight Saving "
"Time is in force.  (Note: this value is constant throughout the year: it "
"does not indicate that DST is in force, it just selects an algorithm.)  The "
"daylight saving time algorithms defined are as follows:"
msgstr ""
"O campo I<tz_dsttime> contém uma constante simbólica (os valores são "
"fornecido abaixo) que indica em que parte do ano o horário de verão está em "
"vigência. (Nota: este valor é constante durante todo o ano - ele não indica "
"que DST vigora, ele apenas seleciona um algoritmo.) O algorítmo definido do "
"horário de verão como segue:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<DST_NONE>     /* not on DST */\n"
"B<DST_USA>      /* USA style DST */\n"
"B<DST_AUST>     /* Australian style DST */\n"
"B<DST_WET>      /* Western European DST */\n"
"B<DST_MET>      /* Middle European DST */\n"
"B<DST_EET>      /* Eastern European DST */\n"
"B<DST_CAN>      /* Canada */\n"
"B<DST_GB>       /* Great Britain and Eire */\n"
"B<DST_RUM>      /* Romania */\n"
"B<DST_TUR>      /* Turkey */\n"
"B<DST_AUSTALT>  /* Australian style with shift in 1986 */\n"
msgstr ""
"B<DST_NONE>     /* Não está em DST */\n"
"B<DST_USA>      /* USA DST */\n"
"B<DST_AUST>     /* Australia DST */\n"
"B<DST_WET>      /* Europa Ocidental DST */\n"
"B<DST_MET>      /* Europa Central DST */\n"
"B<DST_EET>      /* Europa Oriental DST */\n"
"B<DST_CAN>      /* Canadá */\n"
"B<DST_GB>       /* Grã Bretanha e Irlanda */\n"
"B<DST_RUM>      /* Romênia */\n"
"B<DST_TUR>      /* Turquia */\n"
"B<DST_AUSTALT>  /* Australia com troca em 1986 */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Of course it turned out that the period in which Daylight Saving Time is in "
"force cannot be given by a simple algorithm, one per country; indeed, this "
"period is determined by unpredictable political decisions.  So this method "
"of representing timezones has been abandoned."
msgstr ""
"Claro que se mostrou que o período em qual o horário de verão vigora não "
"pode ser dado por um algoritmo simples, um por país; realmente, este periodo "
"é determindao por decisões políticas imprevisíveis. Assim este método de "
"representação de fusos horários foi abandonado."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The time returned by B<gettimeofday>()  I<is> affected by discontinuous "
"jumps in the system time (e.g., if the system administrator manually changes "
"the system time).  If you need a monotonically increasing clock, see "
"B<clock_gettime>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Macros for operating on I<timeval> structures are described in "
"B<timeradd>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<date>(1), B<adjtimex>(2), B<clock_gettime>(2), B<time>(2), B<ctime>(3), "
"B<ftime>(3), B<timeradd>(3), B<capabilities>(7), B<time>(7), B<vdso>(7), "
"B<hwclock>(8)"
msgstr ""
"B<date>(1), B<adjtimex>(2), B<clock_gettime>(2), B<time>(2), B<ctime>(3), "
"B<ftime>(3), B<timeradd>(3), B<capabilities>(7), B<time>(7), B<vdso>(7), "
"B<hwclock>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-12"
msgstr "12 fevereiro 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-28"
msgstr "28 julho 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: Plain text
#: debian-unstable opensuse-tumbleweed
msgid ""
"The kernel accepts NULL for both I<tv> and I<tz.> The timezone argument is "
"ignored by glibc and musl, and not passed to/from the kernel.  Android's "
"bionic passes the timezone argument to/from the kernel, but Android does not "
"update the kernel timezone based on the device timezone in Settings, so the "
"kernel's timezone is typically UTC."
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 março 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
