# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Rafael Fontenelle <rafaelff@gnome.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-01 16:57+0100\n"
"PO-Revision-Date: 2023-12-04 10:22-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <ldpbr-translation@lists.sourceforge."
"net>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 45.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "GETTEXT"
msgstr "GETTEXT"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "September 2023"
msgstr "Setembro de 2023"

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "GNU gettext-runtime 0.22.2"
msgstr "GNU gettext-runtime 0.22.2"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Comandos de usuário"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "gettext - translate message"
msgstr "gettext - traduza mensagens"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<gettext> [I<\\,OPTION\\/>] [[I<\\,TEXTDOMAIN\\/>] I<\\,MSGID\\/>]"
msgstr "B<gettext> [I<\\,OPÇÃO\\/>] [[I<\\,DOMÍNIOTEXTO\\/>] I<\\,MSGID\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<gettext> [I<\\,OPTION\\/>] I<\\,-s \\/>[I<\\,MSGID\\/>]..."
msgstr "B<gettext> [I<\\,OPÇÃO\\/>] I<\\,-s \\/>[I<\\,MSGID\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#.  Add any additional description here
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<gettext> program translates a natural language message into the user's "
"language, by looking up the translation in a message catalog."
msgstr ""
"O programa B<gettext> traduz uma mensagem de linguagem natural para a "
"linguagem do usuário, consultando a tradução em um catálogo de mensagens."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display native language translation of a textual message."
msgstr "Exibe tradução no idioma nativo de uma mensagem de texto."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--domain>=I<\\,TEXTDOMAIN\\/>"
msgstr "B<-d>, B<--domain>=I<\\,DOMÍNIOTEXTO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "retrieve translated messages from TEXTDOMAIN"
msgstr "obtém mensagens traduzidas de DOMÍNIOTEXTO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--context>=I<\\,CONTEXT\\/>"
msgstr "B<-c>, B<--context>=I<\\,CONTEXTO\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "specify context for MSGID"
msgstr "especifica contexto para MSGID"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>"
msgstr "B<-e>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "enable expansion of some escape sequences"
msgstr "habilita expansão de algumas sequências de escape"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "suppress trailing newline"
msgstr "suprime nova linha ao final"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>"
msgstr "B<-E>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(ignored for compatibility)"
msgstr "(ignorado por questão de compatibilidade)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "[TEXTDOMAIN] MSGID"
msgstr "[DOMÍNIOTEXTO] MSGID"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "retrieve translated message corresponding to MSGID from TEXTDOMAIN"
msgstr "obtém mensagem traduzida correspondendo a MSGID de DOMÍNIOTEXTO"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Informative output:"
msgstr "Saída informativa:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "mostra esta ajuda e sai"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display version information and exit"
msgstr "exibe a informação da versão e sai"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the TEXTDOMAIN parameter is not given, the domain is determined from the "
"environment variable TEXTDOMAIN.  If the message catalog is not found in the "
"regular directory, another location can be specified with the environment "
"variable TEXTDOMAINDIR.  When used with the B<-s> option the program behaves "
"like the 'echo' command.  But it does not simply copy its arguments to "
"stdout.  Instead those messages found in the selected catalog are "
"translated.  Standard search directory: /usr/share/locale"
msgstr ""
"Se o parâmetro DOMÍNIOTEXTO não for informado, o domínio é determinado por "
"meio da variável de ambiente TEXTDOMAIN. Se o catálogo de mensagens não for "
"encontrado no diretório padrão, outra localização pode ser especificada na "
"variável de ambiente TEXTDOMAINDIR. Quando USADO com a opção B<-s> o "
"programa comporta-se como o comando \"echo\". Mas ele não apenas copia seus "
"argumentos para stdout. Em vez disso, as mensagens encontradas no catálogo "
"selecionado são traduzidas. Diretório de pesquisa padrão: /usr/share/locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Ulrich Drepper."
msgstr "Escrito por Ulrich Drepper."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RELATANDO PROBLEMAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""
"Relate erros no rastreador em E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> ou por e-mail para E<lt>bug-gettext@gnu.orgE<gt>. Relate erros "
"de tradução para E<lt>https://translationproject.org/team/pt_BR.htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "DIREITOS AUTORAIS"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide
msgid ""
"Copyright \\(co 1995-2023 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 1995-2023 Free Software Foundation, Inc. Licença GPLv3+: GNU "
"GPL versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Este é um software livre: você é livre para alterá-lo e redistribuí-lo. NÃO "
"HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The full documentation for B<gettext> is maintained as a Texinfo manual.  If "
"the B<info> and B<gettext> programs are properly installed at your site, the "
"command"
msgstr ""
"A documentação completa do B<gettext> é mantida como um manual Texinfo. Se "
"os programas B<info> e B<gettext> estiverem adequadamente instalados em seu "
"site, o comando"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<info gettext>"
msgstr "B<info gettext>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "deve lhe dar acesso ao manual completo."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr "Fevereiro de 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GNU gettext-runtime 0.21"
msgstr "GNU gettext-runtime 0.21"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Copyright \\(co 1995-2020 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 1995-2020 Free Software Foundation, Inc. Licença GPLv3+: GNU "
"GPL versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "February 2024"
msgstr "Fevereiro de 2024"

#. type: TH
#: fedora-40
#, no-wrap
msgid "June 2023"
msgstr "Julho de 2023"

#. type: TH
#: fedora-40
#, no-wrap
msgid "GNU gettext-runtime 0.22"
msgstr "GNU gettext-runtime 0.22"

#. type: TH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "October 2022"
msgstr "Outubro de 2023"

#. type: TH
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "GNU gettext-runtime 0.21.1"
msgstr "GNU gettext-runtime 0.21.1"

#. type: Plain text
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Copyright \\(co 1995-2022 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 1995-2022 Free Software Foundation, Inc. Licença GPLv3+: GNU "
"GPL versão 3 ou posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
