# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-06-26 14:01+0200\n"
"PO-Revision-Date: 2021-06-26 18:04+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
#, no-wrap
msgid "GENDIFF"
msgstr "GENDIFF"

#. type: TH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
#, no-wrap
msgid "Mon Jan 10 2000"
msgstr "Montag, 10. Januar 2000"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
msgid "gendiff - utility to aid in error-free diff file generation"
msgstr "gendiff - Dienstprogramm zur Erzeugung fehlerfreier Differenzdateien"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-buster debian-unstable opensuse-leap-15-3
#: opensuse-tumbleweed
msgid "B<gendiff> E<lt>directoryE<gt> E<lt>diff-extensionE<gt>"
msgstr "B<gendiff> E<lt>VerzeichnisE<gt> E<lt>Diff-ErweiterungE<gt>"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-buster debian-unstable opensuse-leap-15-3
#: opensuse-tumbleweed
msgid ""
"B<gendiff> is a rather simple script which aids in generating a diff file "
"from a single directory.  It takes a directory name and a \"diff-extension\" "
"as its only arguments.  The diff extension should be a unique sequence of "
"characters added to the end of all original, unmodified files.  The output "
"of the program is a diff file which may be applied with the B<patch> program "
"to recreate the changes."
msgstr ""
"B<gendiff> ist ein recht einfaches Skript, das Sie beim Erzeugen einer "
"Differenzdatei für ein einzelnes Verzeichnis unterstützt. Es akzeptiert "
"einen Verzeichnisnamen und eine »Diff-Endung« als einzige Argumente. Diese "
"Endung sollte eine eindeutige Zeichenfolge sein, die an das Ende aller "
"ursprünglichen, unveränderten Dateien angehängt wird. Die Ausgabe des "
"Programms ist eine Differenzdatei, die mit dem Programm B<patch> angewendet "
"werden kann, um die Änderungen wiederherzustellen."

#. type: Plain text
#: archlinux debian-buster debian-unstable opensuse-leap-15-3
#: opensuse-tumbleweed
msgid ""
"The usual sequence of events for creating a diff is to create two identical "
"directories, make changes in one directory, and then use the B<diff> utility "
"to create a list of differences between the two.  Using gendiff eliminates "
"the need for the extra, original and unmodified directory copy.  Instead, "
"only the individual files that are modified need to be saved."
msgstr ""
"Der übliche Ablauf zum Erzeugen einer Differenzdatei ist die Erstellung "
"zweier identischer Verzeichnisse, dann die Änderung der Inhalte des einen "
"Verzeichnisses und anschließend die Anwendung des Dienstprogramms B<diff> "
"zum Erstellen einer Liste von Unterschieden zwischen den zwei "
"Verzeichnissen. Bei der Verwendung von B<gendiff> entfällt das Anlegen einer "
"zusätzlichen, unveränderten Verzeichniskopie. Stattdessen müssen nur die "
"einzelnen Dateien gespeichert werden, die tatsächlich geändert wurden."

#. type: Plain text
#: archlinux debian-buster debian-unstable opensuse-leap-15-3
#: opensuse-tumbleweed
msgid ""
"Before editing a file, copy the file, appending the extension you have "
"chosen to the filename.  I.e. if you were going to edit somefile.cpp and "
"have chosen the extension \"fix\", copy it to somefile.cpp.fix before "
"editing it.  Then edit the first copy (somefile.cpp)."
msgstr ""
"Vor dem Bearbeiten einer Datei kopieren Sie diese und hängen die Endung an, "
"die Sie für den Dateinamen gewählt haben. Wenn Sie beispielsweise "
"I<eine_datei.cpp> bearbeiten wollen und die Endung »fix« gewählt haben, "
"kopieren Sie sie vor dem Bearbeiten nach I<eine_datei.cpp.fix>. Dann "
"bearbeiten Sie die Ursprungsdatei (I<eine_datei.cpp>)."

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
msgid ""
"After editing all the files you need to edit in this fashion, enter the "
"directory one level above where your source code resides, and then type"
msgstr ""
"Nach dem Bearbeiten aller gewünschten Dateien auf diese Weise wechseln Sie "
"in das Verzeichnis oberhalb von dem, in dem sich Ihr Quellcode befindet, und "
"geben Sie"

#. type: Plain text
#: archlinux debian-buster debian-unstable opensuse-leap-15-3
#: opensuse-tumbleweed
#, no-wrap
msgid "    $ gendiff somedirectory .fix E<gt> mydiff-fix.patch\n"
msgstr "    $ gendiff irgendein_verzeichnis .fix E<gt> mein_diff-fix.patch\n"

#. type: Plain text
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
msgid ""
"You should redirect the output to a file (as illustrated) unless you want to "
"see the results on stdout."
msgstr ""
"ein. Sie sollten die Ausgabe in eine Datei umleiten (wie im Beispiel), es "
"sei denn, Sie wollen die Ergebnisse in der Standardausgabe ansehen."

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-buster debian-unstable opensuse-leap-15-3
#: opensuse-tumbleweed
msgid "B<diff>(1), B<patch>(1)"
msgstr "B<diff>(1), B<patch>(1)"

#. type: SH
#: archlinux debian-buster debian-unstable fedora-rawhide mageia-cauldron
#: opensuse-leap-15-3 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-buster debian-unstable opensuse-leap-15-3
#: opensuse-tumbleweed
#, no-wrap
msgid "Marc Ewing E<lt>marc@redhat.comE<gt>\n"
msgstr "Marc Ewing E<lt>marc@redhat.comE<gt>\n"

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "\\f[B]gendiff\\f[R] E<lt>directoryE<gt> E<lt>diff-extensionE<gt>\\fR"
msgstr ""
"\\f[B]gendiff\\f[R] E<lt>VerzeichnisE<gt> E<lt>Diff-ErweiterungE<gt>\\fR"

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"\\f[B]gendiff\\f[R] is a rather simple script which aids in generating a "
"diff file from a single directory.  It takes a directory name and a "
"\\[dq]diff-extension\\[dq] as its only arguments.  The diff extension should "
"be a unique sequence of characters added to the end of all original, "
"unmodified files.  The output of the program is a diff file which may be "
"applied with the \\f[B]patch\\f[R] program to recreate the changes.\\fR"
msgstr ""
"\\f[B]gendiff\\f[R] ist ein recht einfaches Skript, das Sie beim Erzeugen "
"einer Differenzdatei für ein einzelnes Verzeichnis unterstützt. Es "
"akzeptiert einen Verzeichnisnamen und eine »Diff-Endung« als einzige "
"Argumente. Diese Endung sollte eine eindeutige Zeichenfolge sein, die an das "
"Ende aller ursprünglichen, unveränderten Dateien angehängt wird. Die Ausgabe "
"des Programms ist eine Differenzdatei, die mit dem Programm \\f[B]patch"
"\\f[R] angewendet werden kann, um die Änderungen wiederherzustellen."

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"The usual sequence of events for creating a diff is to create two identical "
"directories, make changes in one directory, and then use the \\f[B]diff"
"\\f[R] utility to create a list of differences between the two.  Using "
"gendiff eliminates the need for the extra, original and unmodified directory "
"copy.  Instead, only the individual files that are modified need to be saved."
"\\fR"
msgstr ""
"Der übliche Ablauf zum Erzeugen einer Differenzdatei ist die Erstellung "
"zweier identischer Verzeichnisse, dann die Änderung der Inhalte des einen "
"Verzeichnisses und anschließend die Anwendung des Dienstprogramms \\f[B]diff"
"\\f[R] zum Erstellen einer Liste von Unterschieden zwischen den zwei "
"Verzeichnissen. Bei der Verwendung von \\f[B]gendiff\\f[R] entfällt das "
"Anlegen einer zusätzlichen, unveränderten Verzeichniskopie. Stattdessen "
"müssen nur die einzelnen Dateien gespeichert werden, die tatsächlich "
"geändert wurden.\\fR"

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"Before editing a file, copy the file, appending the extension you have "
"chosen to the filename.  I.e.  if you were going to edit somefile.cpp and "
"have chosen the extension \\[dq]fix\\[dq], copy it to somefile.cpp.fix "
"before editing it.  Then edit the first copy (somefile.cpp)."
msgstr ""
"Vor dem Bearbeiten einer Datei kopieren Sie diese und hängen die Endung an, "
"die Sie für den Dateinamen gewählt haben. Wenn Sie beispielsweise "
"I<eine_datei.cpp> bearbeiten wollen und die Endung »fix« gewählt haben, "
"kopieren Sie sie vor dem Bearbeiten nach I<eine_datei.cpp.fix>. Dann "
"bearbeiten Sie die Ursprungsdatei (I<eine_datei.cpp>)."

#. type: Plain text
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"\\f[C]\n"
"    $ gendiff somedirectory .fix E<gt> mydiff-fix.patch\\fR\n"
"\n"
msgstr ""
"\\f[C]\n"
"    $ gendiff irgendein_verzeichnis .fix E<gt> mein_diff-fix.patch\\fR\n"
"\n"

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "\\f[B]diff\\f[R](1), \\f[B]patch\\f[R](1)\\fR"
msgstr "\\f[B]diff\\f[R](1), \\f[B]patch\\f[R](1)\\fR"

#. type: Plain text
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"\\f[C]\n"
"Marc Ewing E<lt>marc\\[at]redhat.comE<gt>\\fR\n"
"\n"
msgstr ""
"\\f[C]\n"
"Marc Ewing E<lt>marc\\[at]redhat.comE<gt>\\fR\n"
"\n"
