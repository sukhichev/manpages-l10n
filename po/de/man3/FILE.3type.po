# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-01 16:56+0100\n"
"PO-Revision-Date: 2023-09-05 22:23+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "FILE"
msgstr "DATEI"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "FILE - input/output stream"
msgstr "FILE - Ein-/Ausgabedatenstrom"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>)"
msgstr "Standard-C-Bibliothek (I<libc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<typedef> /* ... */  B<FILE;>\n"
msgstr "B<typedef> /* … */  B<FILE;>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "An object type used for streams."
msgstr "Ein für Datenströme verwandter Objekttyp."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The following header also provides this type: I<E<lt>wchar.hE<gt>>."
msgstr ""
"Die folgenden Header stellen diesen Typ auch bereit: I<E<lt>wchar.hE<gt>>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<fclose>(3), B<flockfile>(3), B<fopen>(3), B<fprintf>(3), B<fread>(3), "
"B<fscanf>(3), B<stdin>(3), B<stdio>(3)"
msgstr ""
"B<fclose>(3), B<flockfile>(3), B<fopen>(3), B<fprintf>(3), B<fread>(3), "
"B<fscanf>(3), B<stdin>(3), B<stdio>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30. Oktober 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "C99 and later; POSIX.1-2001 and later."
msgstr "C99 und neuer; POSIX.1-2001 und neuer."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"
