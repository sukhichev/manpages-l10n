# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Patrick Rother <krd@gulu.net>
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2010.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2021, 2023.
# Helge Kreutzmann <debian@helgefjell.de>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2024-03-01 16:56+0100\n"
"PO-Revision-Date: 2023-05-07 09:06+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ftime"
msgstr "ftime"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ftime - return date and time"
msgstr "ftime - gibt Datum und Uhrzeit zurück"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/timeb.hE<gt>>\n"
msgstr "B<#include E<lt>sys/timeb.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ftime(struct timeb *>I<tp>B<);>\n"
msgstr "B<int ftime(struct timeb *>I<tp>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<NOTE>: This function is no longer provided by the GNU C library.  Use "
"B<clock_gettime>(2)  instead."
msgstr ""
"B<HINWEIS>: Diese Funktion wird von der GNU-C-Bibliothek nicht mehr "
"bereitgestellt. Verwenden Sie stattdessen B<clock_gettime>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This function returns the current time as seconds and milliseconds since the "
"Epoch, 1970-01-01 00:00:00 +0000 (UTC).  The time is returned in I<tp>, "
"which is declared as follows:"
msgstr ""
"Diese Funktion gibt die aktuelle Zeit als Zeitdifferenz zu »The Epoch« an. "
"The Epoch steht für den 1. Januar 1970, 00:00 UTC. Die Zeit wird in der "
"Struktur I<tp> zurückgegeben, die wie folgt festgelegt ist:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct timeb {\n"
"    time_t         time;\n"
"    unsigned short millitm;\n"
"    short          timezone;\n"
"    short          dstflag;\n"
"};\n"
msgstr ""
"struct timeb {\n"
"    time_t         time;\n"
"    unsigned short millitm;\n"
"    short          timezone;\n"
"    short          dstflag;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Here I<time> is the number of seconds since the Epoch, and I<millitm> is the "
"number of milliseconds since I<time> seconds since the Epoch.  The "
"I<timezone> field is the local timezone measured in minutes of time west of "
"Greenwich (with a negative value indicating minutes east of Greenwich).  The "
"I<dstflag> field is a flag that, if nonzero, indicates that Daylight Saving "
"time applies locally during the appropriate part of the year."
msgstr ""
"Hier ist I<time> die Anzahl der Sekunden seit The Epoch; I<millitm> ist die "
"Zeitdifferenz in Millisekunden seit dem Zeitpunkt, der I<time> Sekunden nach "
"The Epoch liegt. Das Feld I<timezone> gibt die lokale Zeitzone westlich von "
"Greenwich in Minuten an (östlich von Greenwich sind die Werte negativ). Das "
"Feld I<dstflag> zeigt mit einem Wert ungleich Null an, dass im "
"entsprechenden Teil des Jahres lokal Sommerzeit gilt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 says that the contents of the I<timezone> and I<dstflag> fields "
"are unspecified; avoid relying on them."
msgstr ""
"POSIX.1-2001 legt die Inhalte der Felder I<timezone> und I<dstflag> nicht "
"fest. Verlassen Sie sich also nicht auf deren Inhalt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This function always returns 0.  (POSIX.1-2001 specifies, and some systems "
"document, a -1 error return.)"
msgstr ""
"Diese Funktion gibt immer 0 zurück. (POSIX.1-2001 spezifiert und einige "
"Systeme dokumentieren -1 als Rückgabewert bei Fehlern)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ftime>()"
msgstr "B<ftime>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Sicher"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr "Keine."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Removed in glibc 2.33.  4.2BSD, POSIX.1-2001.  Removed in POSIX.1-2008."
msgstr ""
"Wurde in Glibc 2.33 entfernt. 4.2BSD, POSIX.1-2001. Wurde in POSIX.1-2008 "
"entfernt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This function is obsolete.  Don't use it.  If the time in seconds suffices, "
"B<time>(2)  can be used; B<gettimeofday>(2)  gives microseconds; "
"B<clock_gettime>(2)  gives nanoseconds but is not as widely available."
msgstr ""
"Diese Funktion ist veraltet. Verwenden Sie sie nicht. Wenn die Zeit in "
"Sekunden genau genug ist, kann B<time>(2) benutzt werden; B<gettimeofday>(2) "
"liefert Mikrosekunden; B<clock_gettime>(2) sogar Nanosekunden, ist aber "
"nicht weit verbreitet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#.  .SH HISTORY
#.  The
#.  .BR ftime ()
#.  function appeared in 4.2BSD.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Early glibc2 is buggy and returns 0 in the I<millitm> field; glibc 2.1.1 is "
"correct again."
msgstr ""
"Die frühe Glibc2 ist fehlerhaft und gibt im Feld I<millitm> 0 zurück. Glibc "
"2.1.1 gibt wieder korrekte Ergebnisse."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<gettimeofday>(2), B<time>(2)"
msgstr "B<gettimeofday>(2), B<time>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15. Dezember 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: debian-bookworm
msgid ""
"Starting with glibc 2.33, the B<ftime>()  function and the I<E<lt>sys/timeb."
"hE<gt>> header have been removed.  To support old binaries, glibc continues "
"to provide a compatibility symbol for applications linked against glibc 2.32 "
"and earlier."
msgstr ""
"Ab Glibc Version 2.33 wurden die Funktionen B<ftime>() und I<E<lt>sys/timeb."
"hE<gt>> entfernt. Zur Unterstützung älterer Binaries stellt Glibc weiterhin "
"ein Konmpatibilitätssymbol für Anwendungen bereit, die gegen Glibc 2.32 und "
"ältere Versionen gelinkt sind."

#. type: Plain text
#: debian-bookworm
msgid ""
"4.2BSD, POSIX.1-2001.  POSIX.1-2008 removes the specification of B<ftime>()."
msgstr ""
"4.2BSD, POSIX.1-2001. POSIX.1-2008 entfernt die Spezifikation von B<ftime>()."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20. Juli 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
