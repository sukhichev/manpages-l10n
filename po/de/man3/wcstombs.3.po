# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-03-01 17:13+0100\n"
"PO-Revision-Date: 2024-03-02 14:16+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "wcstombs"
msgstr "wcstombs"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "wcstombs - convert a wide-character string to a multibyte string"
msgstr ""
"wcstombs - Eine Zeichenkette weiter Zeichen in eine Multibyte-Zeichenkette "
"konvertieren"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<size_t wcstombs(char >I<dest>B<[restrict .>I<n>B<], const wchar_t *restrict >I<src>B<,>\n"
"B<                size_t >I<n>B<);>\n"
msgstr ""
"B<size_t wcstombs(char >I<Ziel>B<[restrict .>I<n>B<], const wchar_t *restrict >I<Quelle>B<,>\n"
"B<                size_t >I<n>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME: shift state korrekt übersetzt?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<dest> is not NULL, the B<wcstombs>()  function converts the wide-"
"character string I<src> to a multibyte string starting at I<dest>.  At most "
"I<n> bytes are written to I<dest>.  The sequence of characters placed in "
"I<dest> begins in the initial shift state.  The conversion can stop for "
"three reasons:"
msgstr ""
"Falls I<Ziel> nicht NULL ist, konvertiert die Funktion B<wcstombs>() die "
"Zeichenkette weiter Zeichen I<Quelle> in eine bei I<Ziel> beginnende "
"Multibyte-Zeichenkette. Es werden nach I<Ziel> höchstens I<n> byte "
"geschrieben. Die in I<Ziel> abgelegte Zeichensequenz beginnt im anfänglichen "
"Schiebe-Zustand. Die Konvertierung kann aus drei Gründen stoppen:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A wide character has been encountered that can not be represented as a "
"multibyte sequence (according to the current locale).  In this case, "
"I<(size_t)\\ -1> is returned."
msgstr ""
"Es wurde auf ein weites Zeichen gestossen, das nicht als Multibyte-Sequenz "
"dargestellt werden kann (gemäß der aktuellen Locale). In diesem Fall wird "
"I<(size_t)\\ -1> zurückgeliefert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The length limit forces a stop.  In this case, the number of bytes written "
"to I<dest> is returned, but the shift state at this point is lost."
msgstr ""
"Die Längenbegrenzung erzwingt einen Stopp. In diesem Fall wird die Anzahl "
"der nach I<Ziel> geschriebenen Bytes zurückgeliefert, aber der Schiebe-"
"Zustand geht an diesem Punkt verloren."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The wide-character string has been completely converted, including the "
"terminating null wide character (L\\[aq]\\e0\\[aq]).  In this case, the "
"conversion ends in the initial shift state.  The number of bytes written to "
"I<dest>, excluding the terminating null byte (\\[aq]\\e0\\[aq]), is returned."
msgstr ""
"Die Zeichenkette weiter Zeichen wurde vollständig umgewandelt, "
"einschließlich des abschließenden weiten Nullzeichens (L\\[aq]\\e0\\[aq]). "
"In diesem Fall endet die Umwandlung im anfänglichen Schiebe-Zustand. Es wird "
"die Anzahl der nach I<Ziel> geschriebenen Bytes ohne das abschließende "
"Nullbyte (»\\e0«) zurückgeliefert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The programmer must ensure that there is room for at least I<n> bytes at "
"I<dest>."
msgstr ""
"Der Programmierer muss sicherstellen, dass bei I<Ziel> Platz für mindestens "
"I<n> byte ist."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<dest> is NULL, I<n> is ignored, and the conversion proceeds as above, "
"except that the converted bytes are not written out to memory, and no length "
"limit exists."
msgstr ""
"Falls I<Ziel> NULL ist, wird I<n> ignoriert und die Umwandlung fährt wie "
"oben fort, außer dass die umgewandelten Bytes nicht in den Speicher "
"geschrieben werden und keine Längenbeschränkung existiert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In order to avoid the case 2 above, the programmer should make sure I<n> is "
"greater than or equal to I<wcstombs(NULL,src,0)+1>."
msgstr ""
"Um den obigen zweiten Fall zu vermeiden, sollte der Programmierer "
"sicherstellen, dass I<n> größer oder gleich I<wcstombs(NULL,Quelle,0)+1> ist."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<wcstombs>()  function returns the number of bytes that make up the "
"converted part of a multibyte sequence, not including the terminating null "
"byte.  If a wide character was encountered which could not be converted, "
"I<(size_t)\\ -1> is returned."
msgstr ""
"Die Funktion B<wcstombs>() liefert die Anzahl der Bytes zurück, die den "
"umgewandelten Teil der Multibyte-Sequenz darstellen, ohne das abschließende "
"Nullbyte. Falls ein weites Zeichen angetroffen wurde, das nicht umgewandelt "
"werden konnte, wird I<(size_t)\\ -1> zurückgeliefert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<wcstombs>()"
msgstr "B<wcstombs>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Sicher"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<wcsrtombs>(3)  provides a better interface to the same "
"functionality."
msgstr ""
"Die Funktion B<wcsrtombs>(3) stellt eine bessere Schnittstelle für die "
"gleiche Funktionalität bereit."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, C99."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The behavior of B<wcstombs>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"Das Verhalten von B<wcstombs>() hängt von der Kategorie B<LC_CTYPE> der "
"aktuellen Locale ab."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mblen>(3), B<mbstowcs>(3), B<mbtowc>(3), B<wcsrtombs>(3), B<wctomb>(3)"
msgstr ""
"B<mblen>(3), B<mbstowcs>(3), B<mbtowc>(3), B<wcsrtombs>(3), B<wctomb>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20. Juli 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
