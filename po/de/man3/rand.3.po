# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Patrick Rother <krd@gulu.net>, 1996.
# Chris Leick <c.leick@vollbio.de>, 2010-2011.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.3\n"
"POT-Creation-Date: 2024-03-01 17:05+0100\n"
"PO-Revision-Date: 2023-11-11 14:59+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "rand"
msgstr "rand"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "rand, rand_r, srand - pseudo-random number generator"
msgstr "rand, rand_r, srand - Pseudo-Zufallszahlengenerator"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int rand(void);>\n"
"B<void srand(unsigned int >I<seed>B<);>\n"
msgstr ""
"B<int rand(void);>\n"
"B<void srand(unsigned int >I<seed>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int rand_r(unsigned int *>I<seedp>B<);>\n"
msgstr "B<[[veraltet]] int rand_r(unsigned int *>I<seedp>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<rand_r>():"
msgstr "B<rand_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.24:\n"
"        _POSIX_C_SOURCE E<gt>= 199506L\n"
"    glibc 2.23 and earlier\n"
"        _POSIX_C_SOURCE\n"
msgstr ""
"    Seit Glibc 2.24:\n"
"        _POSIX_C_SOURCE E<gt>= 199506L\n"
"    Glibc 2.23 und älter\n"
"        _POSIX_C_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<rand>()  function returns a pseudo-random integer in the range 0 to "
"B<RAND_MAX> inclusive (i.e., the mathematical range [0,\\ B<RAND_MAX>])."
msgstr ""
"Die Funktion B<rand>() gibt eine pseudo-zufällige Ganzzahl im Bereich 0 bis "
"B<RAND_MAX> inklusive zurück (d.h. dem mathematischen Bereich [0,\\ "
"B<RAND_MAX>])."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<srand>()  function sets its argument as the seed for a new sequence of "
"pseudo-random integers to be returned by B<rand>().  These sequences are "
"repeatable by calling B<srand>()  with the same seed value."
msgstr ""
"Die Funktion B<srand>() setzt ihr Argument als Seed (Zufallswert zum "
"Erzeugen kryptografischer Schlüssel) für eine neue Reihe von pseudo-"
"zufälligen Ganzzahlen ein, die von B<rand>() zurückgegeben werden. Diese "
"Sequenzen sind durch Aufruf von B<srand>() mit dem selben Zufallsstartwert "
"wiederholbar."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no seed value is provided, the B<rand>()  function is automatically "
"seeded with a value of 1."
msgstr ""
"Wenn kein Zufallsstartwert angegeben wird, wird automatisch 1 als "
"Zufallsstartwert für B<rand>() genommen."

# HK reentrant = ablaufinvariant???
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The function B<rand>()  is not reentrant, since it uses hidden state that is "
"modified on each call.  This might just be the seed value to be used by the "
"next call, or it might be something more elaborate.  In order to get "
"reproducible behavior in a threaded application, this state must be made "
"explicit; this can be done using the reentrant function B<rand_r>()."
msgstr ""
"Die Funktion B<rand>() ist nicht ablaufinvariant, da sie versteckten Status "
"benutzt, der bei jedem Aufruf geändert wird. Dies ist möglicherweise nur der "
"Zufallsstartwert, der beim nächsten Aufruf verwendet werden soll, oder etwas "
"komplizierteres. Um ein reproduzierbares Verhalten für eine Anwendung mit "
"Threads zu erhalten, muss dieser Status explizit gesetzt werden; dies kann "
"mit der ablaufinvarianten Funktion B<rand_r>() erledigt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Like B<rand>(), B<rand_r>()  returns a pseudo-random integer in the range [0,"
"\\ B<RAND_MAX>].  The I<seedp> argument is a pointer to an I<unsigned int> "
"that is used to store state between calls.  If B<rand_r>()  is called with "
"the same initial value for the integer pointed to by I<seedp>, and that "
"value is not modified between calls, then the same pseudo-random sequence "
"will result."
msgstr ""
"Wie B<rand>() gibt B<rand_r>() eine pseudo-zufällige Ganzzahl im Bereich [0,"
"\\ B<RAND_MAX>] zurück. Das Argument I<seedp> ist ein Zeiger auf einen "
"I<unsigned int>, der benutzt wird, um den Status zwischen Aufrufen zu "
"speichern. Falls B<rand_r>() mit dem gleichen Anfangswert für die Ganzzahl "
"aufgerufen wird, auf die I<seedp> zeigt, und der Wert zwischen den Aufrufen "
"nicht verändert wurde, dann wird das Ergebnis die gleiche pseudo-zufällige "
"Sequenz sein."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The value pointed to by the I<seedp> argument of B<rand_r>()  provides only "
"a very small amount of state, so this function will be a weak pseudo-random "
"generator.  Try B<drand48_r>(3)  instead."
msgstr ""
"Der Wert, auf den das Argument I<seedp> von B<rand_r>() zeigt, stellt nur "
"einen kleinen Anteil des Status bereit, daher wird diese Funktion nur ein "
"schwacher Pseudo-Zufallsgenerator sein. Probieren Sie stattdessen "
"B<drand48_r>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<rand>()  and B<rand_r>()  functions return a value between 0 and "
"B<RAND_MAX> (inclusive).  The B<srand>()  function returns no value."
msgstr ""
"Die Funktionen B<rand>() und B<rand_r>() geben einen Wert zwischen 0 und "
"B<RAND_MAX> (inklusive) zurück. Die Funktion B<srand>() gibt keinen Wert "
"zurück."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<rand>(),\n"
"B<rand_r>(),\n"
"B<srand>()"
msgstr ""
"B<rand>(),\n"
"B<rand_r>(),\n"
"B<srand>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Sicher"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The versions of B<rand>()  and B<srand>()  in the Linux C Library use the "
"same random number generator as B<random>(3)  and B<srandom>(3), so the "
"lower-order bits should be as random as the higher-order bits.  However, on "
"older B<rand>()  implementations, and on current implementations on "
"different systems, the lower-order bits are much less random than the higher-"
"order bits.  Do not use this function in applications intended to be "
"portable when good randomness is needed.  (Use B<random>(3)  instead.)"
msgstr ""
"Die Versionen von B<rand>() und B<srand>() in der Linux C-Bibliothek "
"benutzen den selben Zufallszahlengenerator wie B<random>(3) und "
"B<srandom>(3), daher sollten niederwertige Bits genauso zufällig wie "
"höherwertige Bits sein. Bei älteren Implementierungen von B<rand>() sind "
"niederwertige Bits jedoch viel weniger zufällig als höherwertige Bits. "
"Benutzen Sie diese Funktion nicht in Anwendungen, die portierbar sein "
"sollen, wenn ein hochwertiger Zufall benötigt wird. (Benutzen Sie "
"stattdessen B<random>(3).)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<rand>()"
msgstr "B<rand>()"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<srand>()"
msgstr "B<srand>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<rand_r>()"
msgstr "B<rand_r>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, 4.3BSD, C89, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, C89, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001.  Obsolete in POSIX.1-2008."
msgstr "POSIX.1-2001. In POSIX.1-2008 als veraltet markiert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 gives the following example of an implementation of B<rand>()  "
"and B<srand>(), possibly useful when one needs the same sequence on two "
"different machines."
msgstr ""
"POSIX.1-2001 gibt das folgende Beispiel einer Implementierung von B<rand>() "
"und B<srand>(), das vielleicht nützlich ist, wenn es darum geht, die gleiche "
"Abfolge auf zwei unterschiedlichen Rechnern zu erhalten."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"static unsigned long next = 1;\n"
"\\&\n"
"/* RAND_MAX assumed to be 32767 */\n"
"int myrand(void) {\n"
"    next = next * 1103515245 + 12345;\n"
"    return((unsigned)(next/65536) % 32768);\n"
"}\n"
"\\&\n"
"void mysrand(unsigned int seed) {\n"
"    next = seed;\n"
"}\n"
msgstr ""
"static unsigned long next = 1;\n"
"\\&\n"
"/* es wird angenommen, dass RAND_MAX 32767 ist */\n"
"int myrand(void) {\n"
"    next = next * 1103515245 + 12345;\n"
"    return((unsigned)(next/65536) % 32768);\n"
"}\n"
"\\&\n"
"void mysrand(unsigned int seed) {\n"
"    next = seed;\n"
"}\n"

# http://de.wikipedia.org/wiki/Seed_key
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following program can be used to display the pseudo-random sequence "
"produced by B<rand>()  when given a particular seed.  When the seed is "
"I<-1>, the program uses a random seed."
msgstr ""
"Das folgende Programm kann benutzt werden, um eine pseudo-zufällige Sequenz "
"anzuzeigen, die durch B<rand>() erzeugt wird, wenn ein bestimmter "
"Zufallsstartwert (Zufallswert zum Erzeugen kryptografischer Schlüssel) "
"vorgegeben wird. Wenn der Zufallsstartwert als I<-1> angegeben wird, "
"verwendet das Programm einen zufälligen Zufallsstartwert."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int           r;\n"
"    unsigned int  seed, nloops;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>seedE<gt> E<lt>nloopsE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    seed = atoi(argv[1]);\n"
"    nloops = atoi(argv[2]);\n"
"\\&\n"
"    if (seed == -1) {\n"
"        seed = arc4random();\n"
"        printf(\"seed: %u\\en\", seed);\n"
"    }\n"
"\\&\n"
"    srand(seed);\n"
"    for (unsigned int j = 0; j E<lt> nloops; j++) {\n"
"        r =  rand();\n"
"        printf(\"%d\\en\", r);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int           r;\n"
"    unsigned int  seed, nloops;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>seedE<gt> E<lt>nloopsE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    seed = atoi(argv[1]);\n"
"    nloops = atoi(argv[2]);\n"
"\\&\n"
"    if (seed == -1) {\n"
"        seed = arc4random();\n"
"        printf(\"seed: %u\\en\", seed);\n"
"    }\n"
"\\&\n"
"    srand(seed);\n"
"    for (unsigned int j = 0; j E<lt> nloops; j++) {\n"
"        r =  rand();\n"
"        printf(\"%d\\en\", r);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<drand48>(3), B<random>(3)"
msgstr "B<drand48>(3), B<random>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"The functions B<rand>()  and B<srand>()  conform to SVr4, 4.3BSD, C99, "
"POSIX.1-2001.  The function B<rand_r>()  is from POSIX.1-2001.  POSIX.1-2008 "
"marks B<rand_r>()  as obsolete."
msgstr ""
"Die Funktionen B<rand>() und B<srand>() richten sich nach SVr4, 4.3BSD, C99 "
"und POSIX.1-2001. Die Funktion B<rand_r>() richtet sich nach POSIX.1-2001. "
"POSIX.1-2008 kennzeichnet B<rand_r>() als veraltet."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "static unsigned long next = 1;\n"
msgstr "static unsigned long next = 1;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"/* RAND_MAX assumed to be 32767 */\n"
"int myrand(void) {\n"
"    next = next * 1103515245 + 12345;\n"
"    return((unsigned)(next/65536) % 32768);\n"
"}\n"
msgstr ""
"/* es wird angenommen, dass RAND_MAX 32767 ist */\n"
"int myrand(void) {\n"
"    next = next * 1103515245 + 12345;\n"
"    return((unsigned)(next/65536) % 32768);\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"void mysrand(unsigned int seed) {\n"
"    next = seed;\n"
"}\n"
msgstr ""
"void mysrand(unsigned int seed) {\n"
"    next = seed;\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int           r;\n"
"    unsigned int  seed, nloops;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int           r;\n"
"    unsigned int  seed, nloops;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>seedE<gt> E<lt>nloopsE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Aufruf: %s E<lt>seedE<gt> E<lt>nloopsE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    seed = atoi(argv[1]);\n"
"    nloops = atoi(argv[2]);\n"
msgstr ""
"    seed = atoi(argv[1]);\n"
"    nloops = atoi(argv[2]);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (seed == -1) {\n"
"        seed = arc4random();\n"
"        printf(\"seed: %u\\en\", seed);\n"
"    }\n"
msgstr ""
"    if (seed == -1) {\n"
"        seed = arc4random();\n"
"        printf(\"seed: %u\\en\", seed);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    srand(seed);\n"
"    for (unsigned int j = 0; j E<lt> nloops; j++) {\n"
"        r =  rand();\n"
"        printf(\"%d\\en\", r);\n"
"    }\n"
msgstr ""
"    srand(seed);\n"
"    for (unsigned int j = 0; j E<lt> nloops; j++) {\n"
"        r =  rand();\n"
"        printf(\"%d\\en\", r);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20. Juli 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
