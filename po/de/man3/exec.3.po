# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Schulze <joey@infodrom.org>, 2001.
# Roland Krause <Rokrause@aol.com>, 2001.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
# Helge Kreutzmann <debian@helgefjell.de>, 2017, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-03-01 16:55+0100\n"
"PO-Revision-Date: 2023-05-07 06:45+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "exec"
msgstr "exec"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "execl, execlp, execle, execv, execvp, execvpe - execute a file"
msgstr "execl, execlp, execle, execv, execvp, execvpe - führt eine Datei aus"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<extern char **environ;>\n"
msgstr "B<extern char **environ;>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int execl(const char *>I<pathname>B<, const char *>I<arg>B<, ...>\n"
"B</*, (char *) NULL */);>\n"
"B<int execlp(const char *>I<file>B<, const char *>I<arg>B<, ...>\n"
"B</*, (char *) NULL */);>\n"
"B<int execle(const char *>I<pathname>B<, const char *>I<arg>B<, ...>\n"
"B<                /*, (char *) NULL, char *const >I<envp>B<[] */);>\n"
"B<int execv(const char *>I<pathname>B<, char *const >I<argv>B<[]);>\n"
"B<int execvp(const char *>I<file>B<, char *const >I<argv>B<[]);>\n"
"B<int execvpe(const char *>I<file>B<, char *const >I<argv>B<[], char *const >I<envp>B<[]);>\n"
msgstr ""
"B<int execl(const char *>I<pathname>B<, const char *>I<arg>B<, …>\n"
"B</*, (char *) NULL */);>\n"
"B<int execlp(const char *>I<file>B<, const char *>I<arg>B<, …>\n"
"B</*, (char *) NULL */);>\n"
"B<int execle(const char *>I<pathname>B<, const char *>I<arg>B<, …>\n"
"B<                /*, (char *) NULL, char *const >I<envp>B<[] */);>\n"
"B<int execv(const char *>I<pathname>B<, char *const >I<argv>B<[]);>\n"
"B<int execvp(const char *>I<file>B<, char *const >I<argv>B<[]);>\n"
"B<int execvpe(const char *>I<file>B<, char *const >I<argv>B<[], char *const >I<envp>B<[]);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<execvpe>():"
msgstr "B<execvpe>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<exec>()  family of functions replaces the current process image with a "
"new process image.  The functions described in this manual page are layered "
"on top of B<execve>(2).  (See the manual page for B<execve>(2)  for further "
"details about the replacement of the current process image.)"
msgstr ""
"Die B<exec>()-Funktionsfamilie ersetzt den aktuellen Programmcode im "
"Speicher mit einem neuen Prozessabbild. Die in dieser Handbuchseite "
"beschriebenen Bibliotheksfunktionen sind über diejenigen der Systemfunktion "
"B<execve>(2) gelegt. (Siehe die Handbuchseite von B<execve>(2) für weitere "
"Details über das Ersetzen des aktuellen Prozessabbilds.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The initial argument for these functions is the name of a file that is to be "
"executed."
msgstr ""
"Das erste Argument dieser Funktionen ist der Name der Datei, die ausgeführt "
"werden soll."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The functions can be grouped based on the letters following the \"exec\" "
"prefix."
msgstr ""
"Die Funktionen können, basierend auf den Buchstaben, die dem Namensanfang "
"»exec« folgen, gruppiert werden."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "l - execl(), execlp(), execle()"
msgstr "l - execl(), execlp(), execle()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<const char\\ *arg> and subsequent ellipses can be thought of as "
"I<arg0>, I<arg1>, \\&..., I<argn>.  Together they describe a list of one or "
"more pointers to null-terminated strings that represent the argument list "
"available to the executed program.  The first argument, by convention, "
"should point to the filename associated with the file being executed.  The "
"list of arguments I<must> be terminated by a null pointer, and, since these "
"are variadic functions, this pointer must be cast I<(char\\ *) NULL>."
msgstr ""
"Der Ausdruck I<const char\\ *arg> und die nachfolgenden Auslassungspunkte "
"(»…«) sind als eine Liste mit einer unbestimmten Anzahl von Parametern "
"I<arg0>, I<arg1>, …, I<argn> zu verstehen. Zusammen stellen sie eine Liste "
"mit einem oder mehreren Zeigern auf mit einem Nullbyte (»\\e0«) "
"abgeschlossene Zeichenketten dar, die dem aufgerufenen Programm als "
"Argumentliste verfügbar ist. Der erste Eintrag sollte konventionsgemäß ein "
"Zeiger auf den Dateinamen des aufgerufenen Programms sein. Die "
"Parameterliste I<muss> mit einem Nullzeiger abgeschlossen werden und weil es "
"variadische Funktionen sind, muss für diesen Zeiger eine Typumwandlung "
"(cast) zu I<(char\\ *) NULL> durchgeführt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By contrast with the 'l' functions, the 'v' functions (below) specify the "
"command-line arguments of the executed program as a vector."
msgstr ""
"Im Gegensatz zu den »l«-Funktionen legen die »v«-Funktionen (unten) die "
"Befehlszeilenargumente des ausgeführten Programmes als Vektor fest."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "v - execv(), execvp(), execvpe()"
msgstr "v - execv(), execvp(), execvpe()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<char\\ *const argv[]> argument is an array of pointers to null-"
"terminated strings that represent the argument list available to the new "
"program.  The first argument, by convention, should point to the filename "
"associated with the file being executed.  The array of pointers I<must> be "
"terminated by a null pointer."
msgstr ""
"Die Argument I<char\\ *const argv[]> ist ein im Folgeprogramm verfügbares "
"Feld von Zeigern auf mit einem Nullbyte abgeschlossene Zeichenketten, die "
"die Argumentenliste darstellen. Das erste Argument sollte konventionsgemäß "
"auf den Namen der auszuführenden Datei weisen. Der Feld von Zeigern I<muss> "
"mit einem Nullzeiger als letztem Eintrag abgeschlossen werden."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "e - execle(), execvpe()"
msgstr "e - execle(), execvpe()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The environment of the new process image is specified via the argument "
"I<envp>.  The I<envp> argument is an array of pointers to null-terminated "
"strings and I<must> be terminated by a null pointer."
msgstr ""
"Die Umgebung für das neue Prozessabbild wird mittels des Arguments I<envp> "
"festgelegt. Das Argument I<envp> ist ein Feld von Zeigern auf mit einem "
"Nullbyte abgeschlossene Zeichenketten und I<muss> mit einem Nullzeiger als "
"letztem Eintrag abgeschlossen werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All other B<exec>()  functions (which do not include 'e' in the suffix)  "
"take the environment for the new process image from the external variable "
"I<environ> in the calling process."
msgstr ""
"Alle anderen B<exec>()-Funktionen (die kein »e« in der Endung enthalten) "
"übernehmen die Umgebungsvariablen für den neuen Prozess von der externen "
"Variablen I<environ> im aufrufenden Prozess."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "p - execlp(), execvp(), execvpe()"
msgstr "p - execlp(), execvp(), execvpe()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions duplicate the actions of the shell in searching for an "
"executable file if the specified filename does not contain a slash (/) "
"character.  The file is sought in the colon-separated list of directory "
"pathnames specified in the B<PATH> environment variable.  If this variable "
"isn't defined, the path list defaults to a list that includes the "
"directories returned by I<confstr(_CS_PATH)> (which typically returns the "
"value \"/bin:/usr/bin\")  and possibly also the current working directory; "
"see NOTES for further details."
msgstr ""
"Diese Funktionen suchen ebenso wie die Shell nach einem ausführbaren "
"Programm, wenn der angegebene Dateiname keinen Schrägstrich (/) enthält. Die "
"Datei wird in der durch Doppelpunkte getrennten Liste von Verzeichnis-"
"Pfadnamen in der Umgebungsvariablen B<PATH> gesucht. Wenn diese Variable "
"nicht definiert ist, ist die Pfadliste standardmäßig eine Liste, die die von "
"I<confstr(_CS_PATH)> (das typischerweise den Wert »/bin:/usr/bin« "
"zurückliefert) zurückgelieferten Verzeichnisse enthält und mglicherweise "
"auch das aktuelle Arbeitsverzeichnis. Lesen Sie ANMERKUNGEN für weitere "
"Details."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<execvpe>()  searches for the program using the value of B<PATH> from the "
"caller's environment, not from the I<envp> argument."
msgstr ""
"B<execvpe>() sucht nach dem Programm mittels des Werts von B<PATH> aus der "
"Umgebung des Aufrufenden, nicht aus dem Argument I<envp>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the specified filename includes a slash character, then B<PATH> is "
"ignored, and the file at the specified pathname is executed."
msgstr ""
"Falls der angegebene Dateiname einen Schrägstrich enthält, wird B<PATH> "
"ignoriert und die Datei mit dem angegebenen Pfadnamen ausgeführt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "In addition, certain errors are treated specially."
msgstr "Zusätzlich werden bestimmte Fehler speziell behandelt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If permission is denied for a file (the attempted B<execve>(2)  failed with "
"the error B<EACCES>), these functions will continue searching the rest of "
"the search path.  If no other file is found, however, they will return with "
"I<errno> set to B<EACCES>."
msgstr ""
"Falls die Ausführung einer gefundenen Datei verweigert wird (die versuchte "
"Ausführung von B<execve>(2) führte zum Fehler B<EACCES>), werden diese "
"Funktionen im restlichen Suchpfad weitersuchen. Wenn aber keine andere Datei "
"gefunden wird, kehren diese Funktionen zurück und setzen I<errno> auf "
"B<EACCES>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the header of a file isn't recognized (the attempted B<execve>(2)  failed "
"with the error B<ENOEXEC>), these functions will execute the shell (I</bin/"
"sh>)  with the path of the file as its first argument.  (If this attempt "
"fails, no further searching is done.)"
msgstr ""
"Wenn der Header einer Datei nicht erkannt wird (die versuchte Ausführung von "
"B<execve>(2) führte zum Fehler B<ENOEXEC>), starten diese Funktionen die "
"Shell (I</bin/sh>) mit dem Pfadnamen der Datei als erstes Argument. (Wenn "
"dieser Versuch fehlschlägt, wird die Suche abgebrochen.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All other B<exec>()  functions (which do not include 'p' in the suffix)  "
"take as their first argument a (relative or absolute) pathname that "
"identifies the program to be executed."
msgstr ""
"Alle anderen B<exec>()-Funktionen (die kein »p« in der Endung enthalten) "
"akzeptieren als ihr erstes Argument einen (relativen oder absoluten) "
"Pfadnamen, der das auszuführende Programm identifiziert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<exec>()  functions return only if an error has occurred.  The return "
"value is -1, and I<errno> is set to indicate the error."
msgstr ""
"Die B<exec>()-Funktionen kehren nur in das aufrufende Programm zurück, wenn "
"ein Fehler aufgetreten ist. Der Rückgabewert ist -1 und I<errno> wird auf "
"die entsprechende Fehlerkennung gesetzt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All of these functions may fail and set I<errno> for any of the errors "
"specified for B<execve>(2)."
msgstr ""
"Alle diese Funktionen können fehlschlagen und I<errno> auf jeden möglichen "
"Fehler setzen, der für B<execve>(2) angegeben ist."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<execl>(),\n"
"B<execle>(),\n"
"B<execv>()"
msgstr ""
"B<execl>(),\n"
"B<execle>(),\n"
"B<execv>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Sicher"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<execlp>(),\n"
"B<execvp>(),\n"
"B<execvpe>()"
msgstr ""
"B<execlp>(),\n"
"B<execvp>(),\n"
"B<execvpe>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env"
msgstr "MT-Sicher env"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#.  glibc commit 1eb8930608705702d5746e5491bab4e4429fcb83
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The default search path (used when the environment does not contain the "
"variable B<PATH>)  shows some variation across systems.  It generally "
"includes I</bin> and I</usr/bin> (in that order) and may also include the "
"current working directory.  On some other systems, the current working is "
"included after I</bin> and I</usr/bin>, as an anti-Trojan-horse measure.  "
"The glibc implementation long followed the traditional default where the "
"current working directory is included at the start of the search path.  "
"However, some code refactoring during the development of glibc 2.24 caused "
"the current working directory to be dropped altogether from the default "
"search path.  This accidental behavior change is considered mildly "
"beneficial, and won't be reverted."
msgstr ""
"Der Standardsuchpfad (wird verwandt, wenn die Umgebung nicht die Variable "
"B<PATH> enthält), zeigt zwischen Systemen einige Variationen. Im Allgemeinen "
"enthält es I</bin> und I</usr/bin> (in dieser Reihenfolge) und kann auch das "
"aktuelle Arbeitsverzeichnis enthalten. Auf einigen Systemen ist das aktuelle "
"Arbeitsverzeichnis nach I</bin> und I</usr/bin> enthalten, um Trojanische "
"Pferde zu vermeiden. Die Glibc-Implementierung folgte lange der "
"traditionellen Vorgabe, bei der das aktuelle Arbeitsverzeichnis am Anfang "
"des Suchpfades enthalten ist. Aufgrund einiger Code-Überarbeitungen während "
"der Entwicklung der Glibc 2.24 wurde das aktuelle Arbeitsverzeichnis aus dem "
"Standard-Suchpfad komplett entfernt. Diese versehentliche Verhaltensänderung "
"wird leicht nützlich eingeschätzt und wird nicht zurückgenommen."

# Was ist ein Hard Error?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The behavior of B<execlp>()  and B<execvp>()  when errors occur while "
"attempting to execute the file is historic practice, but has not "
"traditionally been documented and is not specified by the POSIX standard.  "
"BSD (and possibly other systems) do an automatic sleep and retry if "
"B<ETXTBSY> is encountered.  Linux treats it as a hard error and returns "
"immediately."
msgstr ""
"Das Fehlerverhalten von B<execlp>() und B<execvp>() beim Versuch Programme "
"zu starten ist historische Praxis und traditionell undokumentiert. Daher ist "
"dieses Verhalten auch nicht durch den POSIX-Standard spezifiziert. BSD (und "
"möglicherweise andere Systeme) schlafen automatisch und wiederholen den "
"Versuch, wenn B<ETXTBSY> angetroffen wird. Linux behandelt es wie einen "
"harten Fehler und kehrt sofort zurück."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Traditionally, the functions B<execlp>()  and B<execvp>()  ignored all "
"errors except for the ones described above and B<ENOMEM> and B<E2BIG>, upon "
"which they returned.  They now return if any error other than the ones "
"described above occurs."
msgstr ""
"Traditionell ignorierten die Funktionen B<execlp>() und B<execvp>() alle "
"Fehler bis auf die oben beschriebenen sowie B<ENOMEM> und B<E2BIG>, bei "
"deren Auftreten sie ins Hauptprogramm zurückkehrten. Sie kehren jetzt ins "
"Hauptprogramm zurück, wenn ein anderer Fehler als die oben beschriebenen "
"auftritt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<environ>"
msgstr "B<environ>"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<execl>()"
msgstr "B<execl>()"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<execlp>()"
msgstr "B<execlp>()"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<execle>()"
msgstr "B<execle>()"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<execv>()"
msgstr "B<execv>()"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<execvp>()"
msgstr "B<execvp>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<execvpe>()"
msgstr "B<execvpe>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.11."
msgstr "Glibc 2.11."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#.  https://sourceware.org/bugzilla/show_bug.cgi?id=19534
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before glibc 2.24, B<execl>()  and B<execle>()  employed B<realloc>(3)  "
"internally and were consequently not async-signal-safe, in violation of the "
"requirements of POSIX.1.  This was fixed in glibc 2.24."
msgstr ""
"Vor Glibc 2.24 verwandten B<execl>() und B<execle>() intern B<realloc>(3) "
"und waren daher nicht asynchron-signal-sicher. Dies verletzte die "
"Anforderungen von POSIX.1. Dies wurde in Glibc 2.24 korrigiert."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Architecture-specific details"
msgstr "Architekturspezifische Details"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On sparc and sparc64, B<execv>()  is provided as a system call by the kernel "
"(with the prototype shown above)  for compatibility with SunOS.  This "
"function is I<not> employed by the B<execv>()  wrapper function on those "
"architectures."
msgstr ""
"Unter Sparc und Sparc64 wird B<execv>() zur Kompatibilität mit SunOS durch "
"den Kernel als ein Systemaufruf (mit dem oben gezeigten Prototypen) "
"bereitgestellt. Diese Funktion wird durch den B<execv>()-Wrapper auf diesen "
"Architekturen I<nicht> eingesetzt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sh>(1), B<execve>(2), B<execveat>(2), B<fork>(2), B<ptrace>(2), "
"B<fexecve>(3), B<system>(3), B<environ>(7)"
msgstr ""
"B<sh>(1), B<execve>(2), B<execveat>(2), B<fork>(2), B<ptrace>(2), "
"B<fexecve>(3), B<system>(3), B<environ>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr "7. Januar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "The B<execvpe>()  function first appeared in glibc 2.11."
msgstr "Die Funktion B<execvpe>() kam erstmals in Glibc 2.11 vor."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid "The B<execvpe>()  function is a GNU extension."
msgstr "Die Funktion B<execvpe>() ist eine GNU-Erweiterung."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20. Juli 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
