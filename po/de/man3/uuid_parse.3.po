# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21\n"
"POT-Creation-Date: 2024-02-09 17:14+0100\n"
"PO-Revision-Date: 2024-01-07 20:20+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "UUID_PARSE"
msgstr "UUID_PARSE"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Programmer\\(aqs Manual"
msgstr "Programmierhandbuch"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "uuid_parse - convert an input UUID string into binary representation"
msgstr ""
"uuid_parse - eine eingegebene UUID-Zeichenkette in Binärdarstellung umwandeln"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<#include E<lt>uuid.hE<gt>>"
msgstr "B<#include E<lt>uuid.hE<gt>>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<int uuid_parse(char *>I<in>B<, uuid_t >I<uu>B<);>"
msgstr "B<int uuid_parse(char *>I<in>B<, uuid_t >I<uu>B<);>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<int uuid_parse_range(char *>I<in_start>B<, char *>I<in_end>B<, uuid_t "
">I<uu>B<);>"
msgstr ""
"B<int uuid_parse_range(char *>I<in_Start>B<, char *>I<in_Ende>B<, uuid_t "
">I<uu>B<);>"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<uuid_parse>() function converts the UUID string given by I<in> into "
"the binary representation. The input UUID is a string of the form "
"1b4e28ba-2fa1-11d2-883f-b9a761bde3fb (in B<printf>(3) format \"%08x-%04x-"
"%04x-%04x-%012x\", 36 bytes plus the trailing \\(aq\\(rs0\\(aq)."
msgstr ""
"Die Funktion B<uuid_parse>() wandelt die von I<in> angegebene UID-"
"Zeichenkette in deren binäre Darstellung um. Die Eingabe-UUID ist eine "
"Zeichenkette der Form 1b4e28ba-2fa1-11d2-883f-b9a761bde3fb (im B<printf>(3)-"
"Format »%08x-%04x-%04x-%04x-%012x«, 36 Byte plus das angehängte »\\(rs0«)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<uuid_parse_range>() function works like B<uuid_parse>() but parses "
"only range in string specified by I<in_start> and I<in_end> pointers."
msgstr ""
"Die Funktion B<uuid_parse_range>() arbeitet wie B<uuid_parse>(), aber wertet "
"nur den Bereich aus, der in der durch die Zeiger I<in_Start> und I<in_Ende> "
"angegebenen Zeichenkette bezeichnet ist."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Upon successfully parsing the input string, 0 is returned, and the UUID is "
"stored in the location pointed to by I<uu>, otherwise -1 is returned."
msgstr ""
"Bei erfolgreicher Auswertung der Eingabezeichenkette wird 0 zurückgegeben "
"und die UUID an dem Ort gespeichert, auf den I<uu> zeigt, anderenfalls wird "
"-1 zurückgegeben."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This library parses UUIDs compatible with OSF DCE 1.1, and hash based UUIDs "
"V3 and V5 compatible with"
msgstr ""
"Diese Bibliothek wertet zu OSF DCE 1.1 kompatible UUIDs und Hash-basierte "
"UUIDs der Versionen 4 und 5 aus, die kompatible sind zu"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Theodore Y. Ts\\(cqo"
msgstr "Theodore Y. Ts\\(cqo"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<uuid>(3), B<uuid_clear>(3), B<uuid_compare>(3), B<uuid_copy>(3), "
"B<uuid_generate>(3), B<uuid_is_null>(3), B<uuid_time>(3), B<uuid_unparse>(3)"
msgstr ""
"B<uuid>(3), B<uuid_clear>(3), B<uuid_compare>(3), B<uuid_copy>(3), "
"B<uuid_generate>(3), B<uuid_is_null>(3), B<uuid_time>(3), B<uuid_unparse>(3)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<libuuid> library is part of the util-linux package since version "
"2.15.1. It can be downloaded from"
msgstr ""
"B<libuuid> ist seit Version 2.15.1 Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-01-06"
msgstr "6. Januar 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
