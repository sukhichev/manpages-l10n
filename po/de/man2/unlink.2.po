# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joern Vehoff <joern@vehoff.net>, 2001.
# Martin Schulze <joey@infodrom.org>, 2001.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
# Helge Kreutzmann <debian@helgefjell.de>, 2012, 2014.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013-2014, 2021, 2023.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.3\n"
"POT-Creation-Date: 2024-03-01 17:12+0100\n"
"PO-Revision-Date: 2023-05-03 09:59+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unlink"
msgstr "unlink"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "unlink, unlinkat - delete a name and possibly the file it refers to"
msgstr ""
"unlink, unlinkat - löscht einen Namen und unter Umständen die Datei, auf die "
"dieser verweist"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int unlink(const char *>I<pathname>B<);>\n"
msgstr "B<int unlink(const char *>I<Pfadname>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of B<AT_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition der B<AT_*>-Konstanten */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int unlinkat(int >I<dirfd>B<, const char *>I<pathname>B<, int >I<flags>B<);>\n"
msgstr "B<int unlinkat(int >I<Verzdd>B<, const char *>I<Pfadname>B<, int >I<Schalter>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<unlinkat>():"
msgstr "B<unlinkat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""
"    Seit Glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Vor Glibc 2.10:\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<unlink>()  deletes a name from the filesystem.  If that name was the last "
"link to a file and no processes have the file open, the file is deleted and "
"the space it was using is made available for reuse."
msgstr ""
"B<unlink>() löscht einen Namen aus dem Dateisystem. Falls dieser Name der "
"letzte Link auf eine Datei war und kein Prozess die Datei geöffnet hält, "
"wird sie gelöscht und der von ihr belegte Speicherplatz wird für die erneute "
"Verwendung verfügbar gemacht."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the name was the last link to a file but any processes still have the "
"file open, the file will remain in existence until the last file descriptor "
"referring to it is closed."
msgstr ""
"Falls dieser Name der letzte Link auf die Datei war, aber diverse Prozesse "
"die Datei noch geöffnet haben, bleibt die Datei bestehen, bis der letzte auf "
"sie weisende Dateideskriptor gelöscht wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If the name referred to a symbolic link, the link is removed."
msgstr ""
"Falls der referenzierte Name ein symbolischer Link ist, wird der Link "
"entfernt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the name referred to a socket, FIFO, or device, the name for it is "
"removed but processes which have the object open may continue to use it."
msgstr ""
"Falls der Name auf einen Socket, FIFO oder Gerät verwies, so wird der Name "
"dafür entfernt, aber Prozesse, die das Objekt geöffnet haben, können es "
"weiterhin benutzen."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unlinkat()"
msgstr "unlinkat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<unlinkat>()  system call operates in exactly the same way as either "
"B<unlink>()  or B<rmdir>(2)  (depending on whether or not I<flags> includes "
"the B<AT_REMOVEDIR> flag)  except for the differences described here."
msgstr ""
"Der Systemaufruf B<unlinkat>() funktioniert genau wie entweder B<unlink>() "
"oder B<rmdir>(2) (abhängig davon, ob I<Schalter> den Schalter "
"B<AT_REMOVEDIR> enthält), außer den hier beschriebenen Unterschieden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<unlink>()  and B<rmdir>(2)  for a relative "
"pathname)."
msgstr ""
"Falls der in I<Pfadname> übergebene Pfadname relativ ist wird er relativ zu "
"dem im Dateideskriptor I<Verzdd> referenzierten Verzeichnis interpretiert "
"(statt relativ zum aktuellen Arbeitsverzeichnis des aufrufenden Prozesses, "
"wie es bei B<unlink>() und B<rmdir>(2) für einen relativen Pfadnamen der "
"Fall ist)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative and I<dirfd> is the special "
"value B<AT_FDCWD>, then I<pathname> is interpreted relative to the current "
"working directory of the calling process (like B<unlink>()  and B<rmdir>(2))."
msgstr ""
"Falls der in I<Pfadname> übergebene Pfadname relativ ist und I<Verzdd> den "
"besonderen Wert B<AT_FDCWD> enthält wird I<Pfadname> relativ zum aktuellen "
"Arbeitsverzeichnis des aufrufenden Prozesses interpretiert (wie bei "
"B<unlink>() und B<rmdir>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is absolute, then I<dirfd> is ignored."
msgstr ""
"Falls der in I<Pfadname> übergebene Pfadname absolut ist, wird I<Verzdd> "
"ignoriert."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<flags> is a bit mask that can either be specified as 0, or by ORing "
"together flag values that control the operation of B<unlinkat>().  "
"Currently, only one such flag is defined:"
msgstr ""
"I<Schalter> ist eine Bitmaske, die entweder als 0 angegeben werden kann, "
"oder mit durch logisches ODER verknüpften Werten der Schalter, die die "
"Wirkung von B<unlinkat>() steuern. Gegenwärtig wird nur ein einziger "
"Schalter unterstützt:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_REMOVEDIR>"
msgstr "B<AT_REMOVEDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By default, B<unlinkat>()  performs the equivalent of B<unlink>()  on "
"I<pathname>.  If the B<AT_REMOVEDIR> flag is specified, then performs the "
"equivalent of B<rmdir>(2)  on I<pathname>."
msgstr ""
"Standardmäßig führt B<unlinkat>() das Äquivalent von B<unlink>() auf "
"I<Pfadname> aus. Falls der Schalter B<AT_REMOVEDIR> angegeben ist, führt es "
"das Äquivalent von B<rmdir>(2) auf I<Pfadname> aus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<unlinkat>()."
msgstr ""
"Lesen Sie B<openat>(2) für eine Beschreibung der Notwendigkeit von "
"B<unlinkat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Bei Erfolg wird Null zurückgegeben. Bei einem Fehler wird -1 zurückgegeben "
"und I<errno> gesetzt, um den Fehler anzuzeigen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Write access to the directory containing I<pathname> is not allowed for the "
"process's effective UID, or one of the directories in I<pathname> did not "
"allow search permission.  (See also B<path_resolution>(7).)"
msgstr ""
"Schreibzugriff auf das Verzeichnis, das I<Pfadname> enthält, ist für die "
"aktuell effektive UID des Prozesses nicht erlaubt oder eines der "
"Verzeichnisse in I<Pfadname> gewährt keinen Suchzugriff (siehe auch "
"B<path_resolution>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file I<pathname> cannot be unlinked because it is being used by the "
"system or another process; for example, it is a mount point or the NFS "
"client software created it to represent an active but otherwise nameless "
"inode (\"NFS silly renamed\")."
msgstr ""
"Für die Datei I<Pfadname> kann der Link nicht gelöst werden, weil er vom "
"System oder einem anderen Prozess genutzt wird; beispielsweise, wenn er ein "
"Einhängepunkt ist oder die NFS-Client-Software ihn erstellte, um einen "
"aktiven aber ansonsten namenlosen Inode zu repräsentieren (»NFS silly "
"renamed«)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> points outside your accessible address space."
msgstr "I<Pfadname> zeigt aus dem für Sie zugänglichen Adressraum heraus."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "Es ist ein E/A-Fehler (engl. I/O) aufgetreten."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> refers to a directory.  (This is the non-POSIX value returned "
"since Linux 2.1.132.)"
msgstr ""
"I<Pfadname> verweist auf ein Verzeichnis. (Dies ist ein nicht-POSIX-Wert, "
"der von Linux seit 2.1.132 zurückgegeben wird.)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

# Hier schrottet wahrscheinlich die Übersetzung von Pathname das Kompendium?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in translating I<pathname>."
msgstr ""
"Bei der Auflösung von I<Pfadname> wurden zu viele symbolische Verknüpfungen "
"gefunden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> was too long."
msgstr "I<Pfadname> war zu lang."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component in I<pathname> does not exist or is a dangling symbolic link, or "
"I<pathname> is empty."
msgstr ""
"Eine Verzeichniskomponente von I<Pfadname> existiert nicht oder ist ein "
"toter symbolischer Link oder I<Pfadname> ist leer."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Es war nicht genügend Kernelspeicher verfügbar."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<pathname> is not, in fact, a directory."
msgstr ""
"Eine als Verzeichnis benutzte Komponente von I<Pfadname> ist kein "
"Verzeichnis."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system does not allow unlinking of directories, or unlinking of "
"directories requires privileges that the calling process doesn't have.  "
"(This is the POSIX prescribed error return; as noted above, Linux returns "
"B<EISDIR> for this case.)"
msgstr ""
"Das System verweigert die Aufhebung von Links zu Verzeichnissen oder fordert "
"Privilegien, die der aufrufende Prozess nicht hat. (Dies ist die von POSIX "
"vorgeschriebene Fehlerrückgabe. Wie oben erwähnt gibt Linux in diesem Fall "
"B<EISDIR> zurück."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> (Linux only)"
msgstr "B<EPERM> (nur Linux)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The filesystem does not allow unlinking of files."
msgstr "Das Dateisystem verwehrt das Lösen von Dateilinks."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> or B<EACCES>"
msgstr "B<EPERM> oder B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The directory containing I<pathname> has the sticky bit (B<S_ISVTX>)  set "
"and the process's effective UID is neither the UID of the file to be deleted "
"nor that of the directory containing it, and the process is not privileged "
"(Linux: does not have the B<CAP_FOWNER> capability)."
msgstr ""
"Das Verzeichnis, welches I<Pfadname> enthält, hat das Sticky Bit "
"(B<S_ISVTX>) gesetzt und die effektive UID des Prozesses ist weder die UID "
"der zu löschenden Datei noch die UID des Verzeichnisses, das die Datei "
"enthält und der Prozess ist nicht privilegiert (Linux: ihm fehlt die "
"B<CAP_FOWNER>-Capability)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The file to be unlinked is marked immutable or append-only.  (See "
"B<ioctl_iflags>(2).)"
msgstr ""
"Die Datei, deren Link gelöst werden soll, ist unveränderlich oder nur-"
"anhängbar (siehe B<ioctl_iflags>(2))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> refers to a file on a read-only filesystem."
msgstr ""
"I<Pfadname> bezieht sich auf eine Datei auf einem schreibgeschützten "
"Dateisystem."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The same errors that occur for B<unlink>()  and B<rmdir>(2)  can also occur "
"for B<unlinkat>().  The following additional errors can occur for "
"B<unlinkat>():"
msgstr ""
"Die gleichen Fehler, die bei B<unlink>() und B<rmdir>(2) auftreten können, "
"können auch für B<unlinkat>() auftreten. Die folgenden zusätzlichen Fehler "
"können für B<unlinkat>() auftreten:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> nor a valid file "
"descriptor."
msgstr ""
"I<Pfadname> ist relativ, aber I<Verzdd> ist weder B<AT_FDCWD> noch ein "
"gültiger Dateideskriptor."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An invalid flag value was specified in I<flags>."
msgstr "In I<Schalter> wurde ein ungültiger Schalterwert angegeben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> refers to a directory, and B<AT_REMOVEDIR> was not specified in "
"I<flags>."
msgstr ""
"I<Pfadname> bezieht sich auf ein Verzeichnis und B<AT_REMOVEDIR> war in "
"I<Schalter> nicht angegeben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> is relative and I<dirfd> is a file descriptor referring to a "
"file other than a directory."
msgstr ""
"I<Pfadname> ist relativ und I<Verzdd> ist ein Dateideskriptor, der sich auf "
"eine Datei bezieht, die kein Verzeichnis ist."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<unlink>()"
msgstr "B<unlink>()"

#.  SVr4 documents additional error
#.  conditions EINTR, EMULTIHOP, ETXTBSY, ENOLINK.
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<unlinkat>()"
msgstr "B<unlinkat>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008.  Linux 2.6.16, glibc 2.4."
msgstr "POSIX.1-2008. Linux 2.6.16, Glibc 2.4."

#. type: SS
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "glibc"
msgstr "Glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On older kernels where B<unlinkat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<unlink>()  or B<rmdir>(2).  When "
"I<pathname> is a relative pathname, glibc constructs a pathname based on the "
"symbolic link in I</proc/self/fd> that corresponds to the I<dirfd> argument."
msgstr ""
"Wenn in älteren Kerneln B<unlinkat>() nicht verfügbar ist, weicht die Glibc-"
"Wrapper-Funktion auf B<unlink>() oder B<rmdir>(2) aus. Wenn I<Pfadname> "
"relativ ist, konstruiert die Glibc einen Pfadnamen, der auf dem symbolischen "
"Link in I</proc/self/fd> basiert, der dem I<Verzdd>-Argument entspricht."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Infelicities in the protocol underlying NFS can cause the unexpected "
"disappearance of files which are still being used."
msgstr ""
"Unzulänglichkeiten in dem NFS unterliegenden Protokoll können das "
"unerwartete Verschwinden von Dateien, welche noch verwandt werden, "
"verursachen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<rm>(1), B<unlink>(1), B<chmod>(2), B<link>(2), B<mknod>(2), B<open>(2), "
"B<rename>(2), B<rmdir>(2), B<mkfifo>(3), B<remove>(3), "
"B<path_resolution>(7), B<symlink>(7)"
msgstr ""
"B<rm>(1), B<unlink>(1), B<chmod>(2), B<link>(2), B<mknod>(2), B<open>(2), "
"B<rename>(2), B<rmdir>(2), B<mkfifo>(3), B<remove>(3), "
"B<path_resolution>(7), B<symlink>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<unlinkat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""
"B<unlinkat>() wurde zu Linux in Version 2.6.16 hinzugefügt; "
"Bibliotheksunterstützung wurde in Glibc in Version 2.4 hinzugefügt."

#.  SVr4 documents additional error
#.  conditions EINTR, EMULTIHOP, ETXTBSY, ENOLINK.
#. type: Plain text
#: debian-bookworm
msgid "B<unlink>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<unlink>(): SVr4, 4.3BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid "B<unlinkat>(): POSIX.1-2008."
msgstr "B<unlinkat>(): POSIX.1-2008."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "glibc notes"
msgstr "Anmerkungen zur Glibc"

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
