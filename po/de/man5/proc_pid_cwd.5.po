# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2011-2012.
# Dr. Tobias Quathamer <toddy@debian.org>, 2016.
# Chris Leick <c.leick@vollbio.de>, 2017.
# Erik Pfannenstein <debianignatz@gmx.de>, 2017.
# Helge Kreutzmann <debian@helgefjell.de>, 2012-2017, 2019-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.22.0\n"
"POT-Creation-Date: 2024-03-23 08:14+0100\n"
"PO-Revision-Date: 2024-03-23 15:28+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "proc_pid_cwd"
msgstr "proc_pid_cwd"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr "15. August 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "/proc/pid/cwd - symbolic link to current working directory"
msgstr "/proc/pid/cwd - symbolischer Link auf das aktuelle Arbeitsverzeichnis"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/>pidI</cwd>"
msgstr "I</proc/>PIDI</cwd>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This is a symbolic link to the current working directory of the process.  To "
"find out the current working directory of process 20, for instance, you can "
"do this:"
msgstr ""
"Dies ist ein symbolischer Link auf das aktuelle Arbeitsverzeichnis des "
"Prozesses. Um dieses z.B. für den Prozess 20 herauszufinden, geben Sie die "
"folgenden Befehle ein:"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "$B< cd /proc/20/cwd; pwd -P>\n"
msgstr "$B< cd /proc/20/cwd; pwd -P>\n"

#.  The following was still true as at kernel 2.6.13
#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"In a multithreaded process, the contents of this symbolic link are not "
"available if the main thread has already terminated (typically by calling "
"B<pthread_exit>(3))."
msgstr ""
"In einem Multithread-Prozess ist der Inhalt dieses symbolischen Links nicht "
"mehr verfügbar, wenn der Haupt-Thread schon beendet ist (typischerweise "
"durch einen Aufruf von B<pthread_exit>(3))."

#
#
#
#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Permission to dereference or read (B<readlink>(2))  this symbolic link is "
"governed by a ptrace access mode B<PTRACE_MODE_READ_FSCREDS> check; see "
"B<ptrace>(2)."
msgstr ""
"Die Rechte, diesen symbolischen Link zu dereferenzieren oder zu lesen "
"(B<readlink>(2)), werden von einer Ptrace-Zugriffsmodusprüfung "
"B<PTRACE_MODE_READ_FSCREDS> gesteuert; siehe B<ptrace>(2)."

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "B<proc>(5)"
msgstr "B<proc>(5)"
