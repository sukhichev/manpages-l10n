# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Norbert Kümin <norbert.kuemin@lugs.ch>, 1996.
# Andreas Braukmann <andy@abra.de>, 1998.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2011.
# Dr. Tobias Quathamer <toddy@debian.org>, 2017.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-03-01 17:00+0100\n"
"PO-Revision-Date: 2023-02-21 21:31+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mailaddr"
msgstr "mailaddr"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31. Oktober 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mailaddr - mail addressing description"
msgstr "mailaddr - beschreibt E-Mail-Adressen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page gives a brief introduction to SMTP mail addresses, as used "
"on the Internet.  These addresses are in the general format"
msgstr ""
"Diese Handbuchseite gibt eine kurze Einführung in SMTP-Mailadressen, wie sie "
"im Internet verwendet werden. Diese Adressen haben allgemein das Format"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "user@domain\n"
msgstr "benutzer@domain\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"where a domain is a hierarchical dot-separated list of subdomains.  These "
"examples are valid forms of the same address:"
msgstr ""
"wobei Domain eine Liste von durch Punkten getrennten Unter-Domains ist. Die "
"folgenden Beispiele sind gültige Formen der gleichen Adresse:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"john.doe@monet.example.com\n"
"John Doe E<lt>john.doe@monet.example.comE<gt>\n"
"john.doe@monet.example.com (John Doe)\n"
msgstr ""
"john.doe@monet.example.com\n"
"John Doe E<lt>john.doe@monet.example.comE<gt>\n"
"john.doe@monet.example.com (John Doe)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The domain part (\"monet.example.com\") is a mail-accepting domain.  It can "
"be a host and in the past it usually was, but it doesn't have to be.  The "
"domain part is not case sensitive."
msgstr ""
"Der Domain-Anteil (»monet.example.com«) steht für eine Domain, die E-Mails "
"annimmt. Das kann ein Rechner sein, wie es in der Vergangenheit "
"üblicherweise war, muss es aber nicht. Der Domain-Anteil beachtet Groß-/"
"Kleinschreibung nicht."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The local part (\"john.doe\") is often a username, but its meaning is "
"defined by the local software.  Sometimes it is case sensitive, although "
"that is unusual.  If you see a local-part that looks like garbage, it is "
"usually because of a gateway between an internal e-mail system and the net, "
"here are some examples:"
msgstr ""
"Der lokale Anteil (»john.doe«) ist oft ein Benutzername, aber die Bedeutung "
"wird von der lokalen Software definiert. Manchmal wird die Groß-/"
"Kleinschreibung berücksichtigt. Sieht der lokale Teil nach Unfug aus, ist "
"der Grund oft eine Schnittstelle zwischen dem Internet und einem internen "
"Mailsystem. Hier ein paar Beispiele:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"\"surname/admd=telemail/c=us/o=hp/prmd=hp\"@some.where\n"
"USER%SOMETHING@some.where\n"
"machine!machine!name@some.where\n"
"I2461572@some.where\n"
msgstr ""
"\"nachname/admd=telemail/c=us/o=hp/prmd=hp\"@irgend.wo\n"
"BENUTZER%ETWAS@irgend.wo\n"
"rechner!rechner!name@irgend.wo\n"
"I2461572@irgend.wo\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(These are, respectively, an X.400 gateway, a gateway to an arbitrary "
"internal mail system that lacks proper internet support, an UUCP gateway, "
"and the last one is just boring username policy.)"
msgstr ""
"(Das sind, der Reihe nach, ein X.400-Gateway, ein Gateway zu einem "
"beliebigen, internen Mailsystem, welches das Internet schlecht unterstützt, "
"ein UUCP-Gateway und zuletzt eine dröge Vergabe von Benutzernamen.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The real-name part (\"John Doe\") can either be placed before E<lt>E<gt>, or "
"in () at the end.  (Strictly speaking the two aren't the same, but the "
"difference is beyond the scope of this page.)  The name may have to be "
"quoted using \"\", for example, if it contains \".\":"
msgstr ""
"Der wirkliche Name (»John Doe«) kann entweder zuerst, außerhalb der "
"E<lt>E<gt>, oder zuletzt innerhalb von () erscheinen. (Streng genommen sind "
"diese beiden Formate nicht dasselbe, aber das würde den Rahmen dieser "
"Handbuchseite sprengen.) Der Name sollte mit \"\" maskiert werden, wenn er "
"allgemeine Adresszeichen verwendet (\".\"):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\"John Q. Doe\" E<lt>john.doe@monet.example.comE<gt>\n"
msgstr "\"John Q. Doe\" E<lt>john.doe@monet.example.comE<gt>\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Abbreviation"
msgstr "Abgekürzte Domain-Namen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some mail systems let users abbreviate the domain name.  For instance, users "
"at example.com may get away with \"john.doe@monet\" to send mail to John "
"Doe.  I<This behavior is deprecated.> Sometimes it works, but you should not "
"depend on it."
msgstr ""
"Einige Mailsysteme ermöglichen den Benutzern, den Domain-Namen abzukürzen. "
"Zum Beispiel könnten Benutzer der Domain example.com John Doe unter der "
"Adresse »john.doe@monet« erreichen. I<Dieses Verhalten wird missbilligt.> "
"Manchmal funktioniert es, aber Sie sollten sich nicht darauf verlassen."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Route-addrs"
msgstr "Geroutete Adressen (route-addrs)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the past, sometimes one had to route a message through several hosts to "
"get it to its final destination.  Addresses which show these relays are "
"termed \"route-addrs\".  These use the syntax:"
msgstr ""
"In der Vergangenheit musste manchmal eine Nachricht über mehrere Rechner bis "
"zum endgültigen Bestimmungsort weitergeleitet werden. Adressen, die diese "
"Relais zeigen, werden als geroutete Adressen bezeichnet. Sie verwenden das "
"folgende Format:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "E<lt>@hosta,@hostb:user@hostcE<gt>\n"
msgstr "E<lt>@rechnera,@rechnerb:benutzer@rechnercE<gt>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This specifies that the message should be sent to hosta, from there to "
"hostb, and finally to hostc.  Many hosts disregard route-addrs and send "
"directly to hostc."
msgstr ""
"Damit wird festgelegt, dass die Nachricht zu Rechner A, von dort zu Rechner "
"B und schließlich an den Rechner C gesendet werden soll. Viele Rechner "
"ignorieren den vorgegebenen Weg und schicken die Nachricht direkt an Rechner "
"C."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Route-addrs are very unusual now.  They occur sometimes in old mail "
"archives.  It is generally possible to ignore all but the \"user@hostc\" "
"part of the address to determine the actual address."
msgstr ""
"Geroutete Adressen sind heute extrem selten. Hin und wieder sind sie in "
"alten Mailarchiven anzutreffen. Es ist generell möglich, alles außer "
"»benutzer@rechnerc« zu ignorieren, um die Zieladresse festzulegen."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Postmaster"
msgstr "Postmaster"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Every site is required to have a user or user alias designated "
"\"postmaster\" to which problems with the mail system may be addressed.  The "
"\"postmaster\" address is not case sensitive."
msgstr ""
"Für jede Site muss es einen Benutzer (oder Alias) »postmaster« geben, an den "
"Nachrichten über Probleme mit dem Mailsystem gesendet werden können. Die "
"»postmaster«-Adresse wird durch Groß-/Kleinschreibung nicht beeinflusst."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</etc/aliases>"
msgstr "I</etc/aliases>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<\\[ti]/.forward>"
msgstr "I<\\[ti]/.forward>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mail>(1), B<aliases>(5), B<forward>(5), B<sendmail>(8)"
msgstr "B<mail>(1), B<aliases>(5), B<forward>(5), B<sendmail>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.UR http://www.ietf.org\\:/rfc\\:/rfc5322.txt> IETF RFC\\ 5322 E<.UE>"
msgstr ""
"E<.UR http://www.ietf.org\\:/rfc\\:/rfc5322.txt> IETF RFC\\ 5322 E<.UE>"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"
