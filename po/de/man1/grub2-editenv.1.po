# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-03-01 16:57+0100\n"
"PO-Revision-Date: 2024-03-01 22:12+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-EDITENV"
msgstr "GRUB-EDITENV"

#. type: TH
#: fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "February 2024"
msgstr "Februar 2024"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-editenv - edit GRUB environment block"
msgstr "grub-editenv - GRUB-Umgebungsblock bearbeiten"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-editenv> [I<\\,OPTION\\/>...] I<\\,FILENAME COMMAND\\/>"
msgstr "B<grub-editenv> [I<\\,OPTION\\/> …] I<\\,DATEINAME BEFEHL\\/>"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Tool to edit environment block."
msgstr "Werkzeug zum Bearbeiten des Umgebungsblocks."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Commands:"
msgstr "Befehle:"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "create"
msgstr "create"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Create a blank environment block file."
msgstr "erstellt eine leere Umgebungsblock-Datei."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "incr [NAME ...]"
msgstr "incr [NAME …]"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron
msgid "Increase value of integer variables."
msgstr "erhöht den Wert ganzzahliger Variablen."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "list"
msgstr "list"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "List the current variables."
msgstr "listet die aktuellen Variablen auf."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "set [NAME=VALUE ...]"
msgstr "set [NAME=WERT …]"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Set variables."
msgstr "setzt Variablen."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "unset [NAME ...]"
msgstr "unset [NAME …]"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Delete variables."
msgstr "löscht Variablen."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Options:"
msgstr "Optionen:"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "gibt ausführliche Meldungen aus."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"If FILENAME is `-', the default value I<\\,/boot/grub2/grubenv\\/> is used."
msgstr ""
"Falls »-« als DATEINAME angegeben wird, dann wird der Vorgabewert I<\\,/boot/"
"grub2/grubenv\\/> verwendet."

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm /boot/grub2/grubenv'."
msgstr ""
"Es gibt keinen »delete«-Befehl; falls Sie die gesamte Umgebung löschen "
"wollen, verwenden Sie »rm /boot/grub2/grubenv«."

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-grub@gnu.org> E<.ME .>"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-reboot>(8), B<grub-set-default>(8)"
msgstr "B<grub-reboot>(8), B<grub-set-default>(8)"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-editenv> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-editenv> programs are properly installed "
"at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-editenv> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-editenv> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-editenv>"
msgstr "B<info grub-editenv>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "Januar 2024"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "grub-editenv (GRUB2) 2.12"
msgstr "grub-editenv (GRUB2) 2.12"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"
