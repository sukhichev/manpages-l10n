# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2018, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-08-27 17:06+0200\n"
"PO-Revision-Date: 2022-03-30 20:39+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "LSMEM"
msgstr "LSMEM"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "lsmem - list the ranges of available memory with their online status"
msgstr ""
"lsmem - Bereiche verfügbaren Speichers mit deren Online-Status auflisten"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<lsmem> [options]"
msgstr "B<lsmem> [Optionen]"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<lsmem> command lists the ranges of available memory with their online "
"status. The listed memory blocks correspond to the memory block "
"representation in sysfs. The command also shows the memory block size and "
"the amount of memory in online and offline state."
msgstr ""
"Der Befehl B<lsmem> listet die Bereiche des verfügbaren Speichers zusammen "
"mit dessen Online-Status auf. Die aufgelisteten Speicherblöcke entsprechen "
"der Darstellung der Speicherblöcke in Sysfs. Der Befehl zeigt außerdem die "
"Größe der Speicherblöcke sowie die Menge des Speichers im Online- "
"beziehungsweise Offline-Status an."

#. type: Plain text
#: debian-bookworm
msgid ""
"The default output is compatible with original implementation from s390-"
"tools, but it\\(cqs strongly recommended to avoid using default outputs in "
"your scripts. Always explicitly define expected columns by using the B<--"
"output> option together with a columns list in environments where a stable "
"output is required."
msgstr ""
"Die standardmäßige Ausgabe ist kompatibel zur ursprünglichen Implementation "
"für S390-tools, aber es wird dringend empfohlen, standardmäßige Ausgaben in "
"Ihren Skripten zu vermeiden. Definieren Sie in Umgebungen, in denen eine "
"stabile Ausgabe erwartet wird, die erwarteten Spalten stets explizit mit der "
"Option B<--output> zusammen mit einer Liste der Spalten."

# FIXME first sentence
#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<lsmem> command lists a new memory range always when the current memory "
"block distinguish from the previous block by some output column. This "
"default behavior is possible to override by the B<--split> option (e.g., "
"B<lsmem --split=ZONES>). The special word \"none\" may be used to ignore all "
"differences between memory blocks and to create as large as possible "
"continuous ranges. The opposite semantic is B<--all> to list individual "
"memory blocks."
msgstr ""
"Der Befehl B<lsmem> listet einen neuen Speicherbereich auf, und zwar immer "
"dann, wenn sich der aktuelle Speicherblock von dem vorigen in der gleichen "
"Ausgabespalte unterscheidet. Dieses Standardverhalten lässt sich bei Bedarf "
"durch die Option B<--split> (z.B. B<lsmem --split=ZONEN>) außer Kraft "
"setzen. Das spezielle Schlüsselwort »none« können Sie dazu verwenden, alle "
"Unterschiede zwischen Speicherblöcken zu ignorieren und somit größtmögliche "
"zusammenhängende Bereiche zu erzeugen. Die gegenteilige Wirkung erzielt B<--"
"all>, wodurch individuelle Speicherblöcke aufgelistet werden."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that some output columns may provide inaccurate information if a split "
"policy forces B<lsmem> to ignore differences in some attributes. For example "
"if you merge removable and non-removable memory blocks to the one range than "
"all the range will be marked as non-removable on B<lsmem> output."
msgstr ""
"Beachten Sie, dass einige Ausgabespalten ungenaue Informationen anzeigen "
"könnten, wenn eine Teilungsregel B<lsmem> zwingt, die Unterschiede in "
"einigen Attributen zu ignorieren. Wenn Sie beispielsweise löschbare und "
"nicht löschbare Speicherblöcke in einem Bereich zusammenfassen, dann wird in "
"der Ausgabe von B<lsmem> der gesamte Bereich als nicht löschbar markiert."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Not all columns are supported on all systems. If an unsupported column is "
"specified, B<lsmem> prints the column but does not provide any data for it."
msgstr ""
"Nicht alle Spalten werden auf allen Systemen unterstützt. Wenn eine nicht "
"unterstützte Spalte angegeben wird, zeigt B<lsmem> die Spalte zwar an, aber "
"stellt dafür keinerlei Daten bereit."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use the B<--help> option to see the columns description."
msgstr ""
"Verwenden Sie die Option B<--help> für eine Anzeige der Spaltenbeschreibung."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"List each individual memory block, instead of combining memory blocks with "
"similar attributes."
msgstr ""
"listet jeden individuellen Speicherblock auf, anstatt Speicherblöcke anhand "
"ähnlicher Attribute zu kombinieren."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr "gibt die Größen in Byte anstelle eines menschenlesbaren Formats aus."

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""
"Standardmäßig werden die Größen in Byte ausgedrückt und die Präfixe sind "
"Potenzen der Form 2^10 (1024). Die Abkürzungen der Symbole werden zur "
"besseren Lesbarkeit abgeschnitten, indem jeweils nur der erste Buchstabe "
"dargestellt wird. Beispiele: »1 KiB« und »1 MiB« werden als »1 K« bzw. »1 M« "
"dargestellt. Die Erwähnung des »iB«-Anteils, der Teil der Abkürzung ist, "
"entfällt absichtlich."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use JSON output format."
msgstr "verwendet das JSON-Ausgabeformat."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Do not print a header line."
msgstr "unterdrückt die Ausgabe einer Kopfzeile."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<Liste>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns. The default list of columns may be extended if I<list> is "
"specified in the format B<+>I<list> (e.g., B<lsmem -o +NODE>)."
msgstr ""
"gibt an, welche Spalten ausgegeben werden sollen. Mit B<--help> erhalten Sie "
"eine Liste aller unterstützten Spalten. Die Standardliste der Spalten kann "
"erweitert werden, wenn die I<Liste> im Format B<+>I<Liste> (z.B. B<lsmem -o "
"+NODE>) vorliegt."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--output-all>"
msgstr "B<--output-all>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Output all available columns."
msgstr "gibt alle verfügbaren Spalten aus."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-P>, B<--pairs>"
msgstr "B<-P>, B<--pairs>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>)."
msgstr ""
"formatiert die Ausgabe als »Schlüssel=\"Wert\"«-Paare. Alle potenziell "
"unsicheren Wert-Zeichen werden hexadezimal maskiert (\\(rsxE<lt>CodeE<gt>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Produce output in raw format. All potentially unsafe characters are hex-"
"escaped (\\(rsxE<lt>codeE<gt>)."
msgstr ""
"erstellt die Ausgabe im rohen Format. Alle potenziell unsicheren Zeichen "
"werden hexadezimal maskiert (\\(rsxE<lt>CodeE<gt>)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-S>, B<--split> I<list>"
msgstr "B<-S>, B<--split> I<Liste>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Specify which columns (attributes) use to split memory blocks to ranges. The "
"supported columns are STATE, REMOVABLE, NODE and ZONES, or \"none\". The "
"other columns are silently ignored. For more details see B<DESCRIPTION> "
"above."
msgstr ""
"gibt an, welche Spalten (Attribute) zum Teilen von Speicherblöcken in "
"Bereiche verwendet werden. Unterstützte Spalten sind STATE, REMOVABLE, NODE "
"und ZONES, oder »none«. Die anderen Spalten werden stillschweigend "
"ignoriert. Weitere Details finden Sie oben in der B<BESCHREIBUNG>."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--sysroot> I<directory>"
msgstr "B<-s>, B<--sysroot> I<Verzeichnis>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Gather memory data for a Linux instance other than the instance from which "
"the B<lsmem> command is issued. The specified I<directory> is the system "
"root of the Linux instance to be inspected."
msgstr ""
"sammelt Speicherdaten für eine andere Linux-Instanz als jener, von der aus "
"der Befehl B<lsmem> aufgerufen wurde. Das angegebene I<Verzeichnis> ist die "
"Systemwurzel der zu untersuchenden Linux-Instanz."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--summary>[=I<when>]"
msgstr "B<--summary>[=I<wann>]"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option controls summary lines output. The optional argument I<when> can "
"be B<never>, B<always> or B<only>. If the I<when> argument is omitted, it "
"defaults to B<\"only\">. The summary output is suppressed for B<--raw>, B<--"
"pairs> and B<--json>."
msgstr ""
"steuert die Ausgabe der Zeilen der Zusammenfassung. Das optionale Argument "
"I<wann> kann B<never>, B<always> oder B<only> sein. Wenn das Argument "
"I<wann> ausgelassen wird, wird die Vorgabe B<only> verwendet. Die Ausgabe "
"der Zusammenfassung wird für B<--raw>, B<--pairs> und B<--json> unterdrückt."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<lsmem> was originally written by Gerald Schaefer for s390-tools in Perl. "
"The C version for util-linux was written by Clemens von Mann, Heiko Carstens "
"and Karel Zak."
msgstr ""
"B<lsmem> wurde ursprünglich von Gerald Schaefer für S390-tools in Perl "
"geschrieben. Die C-Version für Util-linux wurde von Clemens von Mann, Heiko "
"Carstens und Karel Zak geschrieben."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<chmem>(8)"
msgstr "B<chmem>(8)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<lsmem> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<lsmem> ist Teil des Pakets util-linux, welches heruntergeladen "
"werden kann von:"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

# FIXME: s/output compatible/output is compatible/
#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The default output compatible with original implementation from s390-tools, "
"but it\\(cqs strongly recommended to avoid using default outputs in your "
"scripts. Always explicitly define expected columns by using the B<--output> "
"option together with a columns list in environments where a stable output is "
"required."
msgstr ""
"Die standardmäßige Ausgabe ist kompatibel zur ursprünglichen Implementation "
"für S390-tools, aber es wird dringend empfohlen, standardmäßige Ausgaben in "
"Ihren Skripten zu vermeiden. Definieren Sie in Umgebungen, in denen eine "
"stabile Ausgabe erwartet wird, die erwarteten Spalten stets explizit mit der "
"Option B<--output> zusammen mit einer Liste der Spalten."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Print the SIZE column in bytes rather than in a human-readable format."
msgstr ""
"gibt die SIZE-Spalte in Byte anstelle eines menschenlesbaren Formats aus."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Specify which columns (attributes) use to split memory blocks to ranges. The "
"supported columns are STATE, REMOVABLE, NODE and ZONES, or \"none\". The "
"other columns are silently ignored. For more details see DESCRIPTION above."
msgstr ""
"gibt an, welche Spalten (Attribute) zum Teilen von Speicherblöcken in "
"Bereiche verwendet werden. Unterstützte Spalten sind STATE, REMOVABLE, NODE "
"und ZONES, oder »none«. Die anderen Spalten werden stillschweigend "
"ignoriert. Weitere Details finden Sie oben in der BESCHREIBUNG."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."
