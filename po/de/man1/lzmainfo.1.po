# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-08-27 17:06+0200\n"
"PO-Revision-Date: 2017-01-12 10:43+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "LZMAINFO"
msgstr "LZMAINFO"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2013-06-30"
msgstr "30. Juni 2013"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Tukaani"
msgstr "Tukaani"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "XZ Utils"
msgstr "XZ-Dienstprogramme"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid "lzmainfo - show information stored in the .lzma file header"
msgstr "lzmainfo - im .lzma-Dateikopf enthaltene Informationen anzeigen"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<lzmainfo> [B<--help>] [B<--version>] [I<file...>]"
msgstr "B<lzmainfo> [B<--help>] [B<--version>] [I<Datei …>]"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<lzmainfo> shows information stored in the B<.lzma> file header.  It reads "
"the first 13 bytes from the specified I<file>, decodes the header, and "
"prints it to standard output in human readable format.  If no I<files> are "
"given or I<file> is B<->, standard input is read."
msgstr ""
"B<lzmainfo> zeigt die im B<.lzma>-Dateikopf enthaltenen Informationen an. Es "
"liest die ersten 13 Bytes aus der angegebenen I<Datei>, dekodiert den "
"Dateikopf und gibt das Ergebnis in die Standardausgabe in einem "
"menschenlesbaren Format aus. Falls keine I<Datei>en angegeben werden oder "
"die I<Datei> als B<-> übergeben wird, dann wird aus der Standardeingabe "
"gelesen."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Usually the most interesting information is the uncompressed size and the "
"dictionary size.  Uncompressed size can be shown only if the file is in the "
"non-streamed B<.lzma> format variant.  The amount of memory required to "
"decompress the file is a few dozen kilobytes plus the dictionary size."
msgstr ""
"In der Regel sind die unkomprimierte Größe der Daten und die Größe des "
"Wörterbuchs am bedeutsamsten. Die unkomprimierte Größe kann nur dann "
"angezeigt werden, wenn die Datei im B<.lzma>-Format kein Datenstrom ist. Die "
"Größe des für die Dekomprimierung nötigen Speichers beträgt einige Dutzend "
"Kilobyte zuzüglich der Größe des Inhaltsverzeichnisses."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<lzmainfo> is included in XZ Utils primarily for backward compatibility "
"with LZMA Utils."
msgstr ""
"B<lzmainfo> ist in den XZ-Dienstprogrammen hauptsächlich zur Kompatibilität "
"zu den LZMA-Dienstprogrammen enthalten."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr "EXIT-STATUS"

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "All is good."
msgstr "Alles ist in Ordnung."

#. type: TP
#: opensuse-leap-15-6
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "An error occurred."
msgstr "Ein Fehler ist aufgetreten."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<lzmainfo> uses B<MB> while the correct suffix would be B<MiB> (2^20 "
"bytes).  This is to keep the output compatible with LZMA Utils."
msgstr ""
"B<lzmainfo> verwendet B<MB>, während das korrekte Suffix B<MiB> (2^20 Bytes) "
"wäre. Damit wird die Kompatibilität zu den LZMA-Dienstprogrammen "
"gewährleistet."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<xz>(1)"
msgstr "B<xz>(1)"
