# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: exif-man\n"
"POT-Creation-Date: 2024-02-15 17:58+0100\n"
"PO-Revision-Date: 2021-12-12 21:41+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "exif"
msgstr "exif"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2012-07-13"
msgstr "13. Juli 2012"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "exif 0.6.21.1"
msgstr "exif 0.6.21.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "command line front-end to libexif"
msgstr "Befehlszeilenoberfläche zu libexif"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif - shows EXIF information in JPEG files"
msgstr "exif - zeigt EXIF-Informationen in JPEG-Dateien an"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<exif [ >I<OPTION>B< ] [ >I<file...>B< ]>"
msgstr "B<exif [ >I<OPTION>B< ] [ >I<Datei …>B< ]>"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<exif> is a small command-line utility to show and change EXIF information "
"in JPEG files."
msgstr ""
"B<exif> ist ein kleines Befehlszeilenwerkzeug zum Anzeigen und Bearbeiten "
"der EXIF-Informationen in JPEG-Dateien."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Most digital cameras produce EXIF files, which are JPEG files with extra "
"tags that contain information about the image. The B<exif> command-line "
"utility allows you to read EXIF information from and write EXIF information "
"to those files.  B<exif> internally uses the B<libexif> library."
msgstr ""
"Die meisten Digitalkameras erstellen EXIF-Dateien. Das sind JPEG-Dateien mit "
"zusätzlichen Tags, die Informationen zum Bild enthalten. Das "
"Befehlszeilenwerkzeug B<exif> ermöglicht Ihnen das Lesen der EXIF-"
"Informationen in der Datei und das Speichern von EXIF-Informationen in "
"solche Dateien. Intern benutzt B<exif> die Bibliothek B<libexif>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Each input file given on the command line is acted upon in turn, using all "
"the options given. Execution will be aborted immediately if one file is not "
"readable or does not contain EXIF tags."
msgstr ""
"Jede der auf der Befehlszeile übergebenen Dateien wird mit allen angegebenen "
"Optionen bearbeitet. Die Ausführung wird sofort abgebrochen, wenn eine der "
"Dateien nicht lesbar ist oder keine EXIF-Markierungen enthält."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"As EXIF tags are read, any unknown ones are discarded and known ones are "
"automatically converted into the correct format, if they aren't already.  "
"Corrupted MakerNote tags are also removed, but no format changes are made."
msgstr ""
"Beim Lesen der EXIF-Markierungen werden die unbekannten verworfen und die "
"bekannten automatisch in das korrekte Format umgewandelt, sofern das nicht "
"bereits geschehen ist. Beschädigte MakerNote-Markierungen werden ebenfalls "
"entfernt, aber keine Formatänderungen vorgenommen."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v, --version>"
msgstr "B<-v, --version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display the B<exif> version number."
msgstr "zeigt die Versionsnummer von B<exif> an."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i, --ids>"
msgstr "B<-i, --ids>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Show ID numbers instead of tag names."
msgstr "zeigt Kennungen anstelle von Markierungsnamen an."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t, --tag=>I<TAG>"
msgstr "B<-t, --tag=>I<MARKIERUNG>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Select only this I<TAG>.  I<TAG> is the tag title, the short tag name, or "
"the tag number (hexadecimal numbers are prefixed with 0x), from the IFD "
"specified with --ifd.  The tag title is dependent on the current locale, "
"whereas name and number are locale-independent."
msgstr ""
"wählt nur diese I<MARKIERUNG>. I<MARKIERUNG> ist der Titel der Markierung, "
"deren Kurzbezeichnung oder deren Nummer (hexadezimalen Nummern wird ein 0x "
"vorangestellt) aus der mit --ifd angegebenen IFD. Der Titel der Markierung "
"ist von der gegenwärtigen Spracheinstellung abhängig, wobei Name und Nummer "
"allerdings nicht von der Spracheinstellung beeinflusst werden."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--ifd=>I<IFD>"
msgstr "B<--ifd=>I<IFD>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Select a tag or tags from this I<IFD>.  Valid IFDs are \"0\", \"1\", "
"\"EXIF\", \"GPS\", and \"Interoperability\".  Defaults to \"0\"."
msgstr ""
"wählt eine oder mehrere Markierungen aus dieser I<IFD>. Gültige IFDs sind "
"»0«, »1«, »EXIF«, »GPS« und »Interoperability«. Vorgabe ist »0«."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l, --list-tags>"
msgstr "B<-l, --list-tags>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"List all known EXIF tags and IFDs.  A JPEG image must be provided, and those "
"tags which appear in the file are shown with an asterisk in the "
"corresponding position in the list."
msgstr ""
"listet alle bekannten EXIF-Markierungen und IFDs auf. Ein JPEG-Bild muss "
"angegeben werden. Jene Tags, die in dieser Datei vorkommen, werden mit einem "
"Sternchen an der entsprechenden Position in der Liste markiert."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-|, --show-mnote>"
msgstr "B<-|, --show-mnote>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Show the contents of the MakerNote tag.  The contents of this tag are "
"nonstandard (and often undocumented) and may therefore not be recognized, or "
"if they are recognized they may not necessarily be interpreted correctly."
msgstr ""
"zeigt den Inhalt der MakerNote-Markierung an. Die Inhalte dieser Markierung "
"sind nicht standardisiert (und oft auch nicht dokumentiert) und könnten "
"deswegen nicht erkannt werden. Werden sie jedoch erkannt, heißt das nicht "
"unbedingt, dass sie auch richtig interpretiert werden."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--remove>"
msgstr "B<--remove>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Remove the tag or (if no tag is specified) the entire IFD."
msgstr ""
"entfernt die Markierung oder (wenn keine Markierung angegeben ist) die "
"gesamte IFD."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s, --show-description>"
msgstr "B<-s, --show-description>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Show description of tag.  The --tag option must also be given."
msgstr ""
"zeigt die Beschreibung der Markierung an. Die Option »--tag« muss ebenfalls "
"angegeben werden."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-e, --extract-thumbnail>"
msgstr "B<-e, --extract-thumbnail>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Extract the thumbnail, writing the thumbnail image to the file specified "
"with --output."
msgstr ""
"entpackt das Vorschaubild und speichert es in der Datei, die mit --output "
"angegeben ist."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r, --remove-thumbnail>"
msgstr "B<-r, --remove-thumbnail>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Remove the thumbnail from the image, writing the new image to the file "
"specified with --output."
msgstr ""
"entfernt das Vorschaubild aus dem Bild, wobei das neue Bild in die Datei "
"geschrieben wird, die mit --output angegeben wird."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n, --insert-thumbnail=>I<FILE>"
msgstr "B<-n, --insert-thumbnail=>I<DATEI>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Insert I<FILE> as thumbnail.  No attempt is made to ensure that the contents "
"of I<FILE> are in a valid thumbnail format."
msgstr ""
"fügt I<DATEI> als Vorschaubild ein. Es wird dabei nicht überprüft, ob der "
"Inhalt der I<DATEI> tatsächlich in einem gültigen Vorschauformat vorliegt."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-fixup>"
msgstr "B<--no-fixup>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Do not attempt to fix EXIF specification violations when reading tags.  When "
"used in conjunction with --create-exif, this option inhibits the creation of "
"the mandatory tags.  B<exif> will otherwise remove illegal or unknown tags, "
"add some mandatory tags using default values, and change the data type of "
"some tags to match that required by the specification."
msgstr ""
"verhindert den Versuch, die inkorrekte Anwendung der EXIF-Spezifikation beim "
"Lesen von Markierungen zu reparieren. Wenn diese Option zusammen mit --"
"create-exif verwendet wird, dann werden die obligatorischen Markierungen "
"nicht erstellt. B<exif> entfernt ansonsten ungültige oder unbekannte "
"Markierungen und fügt Standardwerte für einige obligatorische Markierungen "
"hinzu. Weiterhin wird der Datentyp einiger Markierungen so geändert, dass "
"dieser der Spezifikation entspricht."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o, --output=>I<FILE>"
msgstr "B<-o, --output=>I<DATEI>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Write output image to I<FILE>.  If this option is not given and an image "
"file must be written, the name used is the same as the input file with the "
"suffix \".modified.jpeg\"."
msgstr ""
"schreibt das Ausgabebild in I<DATEI> Wenn diese Option nicht angegeben ist, "
"aber eine Bilddatei geschrieben werden muss, wird für die Ausgabedatei der "
"Name der Eingabedatei verwendet und das Suffix ».modified.jpg« angehängt."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--set-value=>I<VALUE>"
msgstr "B<--set-value=>I<WERT>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the data for the tag specified with --tag and --ifd to I<VALUE>.  "
"Compound values consisting of multiple components are separated with spaces."
msgstr ""
"setzt die Daten für die mit »--tag« und »--ifd« bezeichnete Markierung auf "
"I<WERT>. Verkettete Wertangaben für mehrere Komponenten sind möglich, indem "
"die einzelnen Komponenten durch Leerzeichen getrennt werden."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c, --create-exif>"
msgstr "B<-c, --create-exif>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Create EXIF data if it does not exist. Mandatory tags are created with "
"default values unless the --no-fixup option is given.  This option can be "
"used instead of specifying an input file name in most cases, to operate on "
"the default values of the mandatory set of EXIF tags.  In this case, the --"
"output option has no effect and no file is written."
msgstr ""
"erstellt EXIF-Daten, falls diese noch nicht existieren. Wenn die Option --no-"
"fixup nicht angegeben ist, werden obligatorische Markierungen mit "
"Standardwerten erstellt. Diese Option kann in den meisten Fällen als Ersatz "
"für einen Eingabedateinamen verwendet werden, um mit dem Satz "
"obligatorischer EXIF-Markierungen zu arbeiten. In diesem Fall hat die Option "
"--output keinen Effekt und es wird keine Datei geschrieben."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m, --machine-readable>"
msgstr "B<-m, --machine-readable>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Produce output in a machine-readable (tab-delimited) format.  The --xml-"
"output and --machine-readable options are mutually exclusive."
msgstr ""
"erzeugt die Ausgabe in einem maschinenlesbaren (durch Tabulatoren "
"getrennten) Format. Die Optionen --xml-output und --machine-readable "
"schließen sich gegenseitig aus."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w, --width=>I<N>"
msgstr "B<-w, --width=>I<N>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the maximum width of the output to N characters (default 80). This does "
"not apply to some output formats (e.g. XML)."
msgstr ""
"setzt die maximale Breite der Ausgabe auf N Zeichen (Vorgabe ist 80). Dies "
"wirkt sich auf einige Ausgabeformate nicht aus, zum Beispiel XML."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x, --xml-output>"
msgstr "B<-x, --xml-output>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Produce output in an XML format (when possible).  The --xml-output and --"
"machine-readable options are mutually exclusive.  Note that the XML schema "
"changes with the locale, and it sometimes produces invalid XML. This option "
"is not recommended."
msgstr ""
"erstellt die Ausgabe in XML, wenn möglich. Die Optionen --xml-output und --"
"machine-readable schließen sich gegenseitig aus. Beachten Sie, dass sich das "
"XML-Schema mit der Spracheinstellung ändert und gelegentlich ungültiges XML "
"erzeugt. Diese Option wird nicht empfohlen."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d, --debug>"
msgstr "B<-d, --debug>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Show debugging messages. Also, when processing a file that contains "
"corrupted data, this option causes B<exif> to attempt to continue "
"processing. Normally, corrupted data causes an abort."
msgstr ""
"zeigt Debugging-Meldungen zur Fehlerdiagnose an. Außerdem veranlasst diese "
"Option B<exif>, bei einer Datei, deren Daten beschädigt sind, dennoch weiter "
"zu arbeiten. Normalerweise wird abgebrochen, wenn beschädigte Daten erkannt "
"werden."

#. type: SS
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Help options"
msgstr "Hilfeoptionen"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-?, --help>"
msgstr "B<-?, --help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Show help message."
msgstr "zeigt eine kurze Hilfe an."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display brief usage message."
msgstr "zeigt einen kurzen Bedienungshinweis an."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display all recognized EXIF tags in an image and the tag contents, with bad "
"tags fixed:"
msgstr ""
"Alle in einem Bild erkannten EXIF-Markierungen und deren Inhalte anzeigen, "
"wobei inkorrekte Markierungen repariert werden:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif image.jpg"
msgstr "exif Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display a table listing all known EXIF tags and whether each one exists in "
"the given image:"
msgstr ""
"Eine Tabelle aller bekannten EXIF-Markierungen anzeigen und dabei für jede "
"Markierung angeben, ob sie in dem Bild enthalten ist:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif --list-tags --no-fixup image.jpg"
msgstr "exif --list-tags --no-fixup Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display details on all XResolution tags found in the given image:"
msgstr "Details aller XResolution-Markierungen im angegebenen Bild anzeigen:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif --tag=XResolution --no-fixup image.jpg"
msgstr "exif --tag=XResolution --no-fixup Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display the raw contents of the \"Model\" tag in the given image (with a "
"newline character appended):"
msgstr ""
"Den unverarbeiteten Inhalt der »Model«-Markierung im angegebenen Bild "
"anzeigen (mit einem angehängten Zeilenumbruch):"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif --ifd=0 --tag=Model --machine-readable image.jpg"
msgstr "exif --ifd=0 --tag=Model --machine-readable Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Extract the thumbnail into the file thumbnail.jpg:"
msgstr "Das Vorschaubild in der Datei Vorschaubild.jpg speichern:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif --extract-thumbnail --output=thumbnail.jpg image.jpg"
msgstr "exif --extract-thumbnail --output=Vorschaubild.jpg Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display a list of the numeric values of only the EXIF tags in the thumbnail "
"IFD (IFD 1) and the tag values:"
msgstr ""
"Eine Liste der numerischen Werte nur aus den EXIF-Markierungen im "
"Vorschaubild IFD (IFD 1) und den Werten der Tags anzeigen:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif --ids --ifd=1 --no-fixup image.jpg"
msgstr "exif --ids --ifd=1 --no-fixup Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display the meaning of tag 0x9209 in the \"EXIF\" IFD according to the EXIF "
"specification:"
msgstr ""
"Den Inhalt der Markierung 0x9209 in der »EXIF«-IFD entsprechend der EXIF-"
"Spezifikation anzeigen:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif --show-description --ifd=EXIF --tag=0x9209"
msgstr "exif --show-description --ifd=EXIF --tag=0x9209"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Add an Orientation tag with value \"Bottom-left\" (4) to an existing image, "
"leaving the existing tags untouched:"
msgstr ""
"Eine Ausrichtungs-Markierung mit dem Wert »Bottom-left« (4) zu einem "
"existierenden Bild hinzufügen, wobei die vorhandenen Markierungen nicht "
"verändert werden:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"exif --output=new.jpg --ifd=0 --tag=0x0112 --set-value=4 --no-fixup image.jpg"
msgstr ""
"exif --output=neu.jpg --ifd=0 --tag=0x0112 --set-value=4 --no-fixup Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Add a YCbCr Sub-Sampling tag with value 2,1 (a.k.a YCbCr 4:2:2) to an "
"existing image and fix the existing tags, if necessary:"
msgstr ""
"Eine »YCbCr Sub-Sampling«-Markierung mit dem wert 2,1 (YCbCr 4:2:2) zu einem "
"existierenden Bild hinzufügen und die vorhandenen Markierungen reparieren, "
"falls nötig:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"exif --output=new.jpg --tag=YCbCrSubSampling --ifd=0 --set-value='2 1' image."
"jpg"
msgstr ""
"exif --output=neu.jpg --tag=YCbCrSubSampling --ifd=0 --set-value='2 1' Bild."
"jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Remove the \"User Comment\" tag from an image:"
msgstr "Die »Anmerkung des Nutzers«-Markierung von einem Bild entfernen:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"exif --output=new.jpg --remove --tag=\"User Comment\" --ifd=EXIF image.jpg"
msgstr ""
"exif --output=new.jpg --remove --tag=\"Anmerkung des Nutzers\" --ifd=EXIF "
"Bild.jpg"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display a table with all known EXIF tags, highlighting mandatory ones:"
msgstr ""
"Eine Tabelle aller bekannten EXIF-Markierungen anzeigen, wobei die "
"obligatorischen hervorgehoben werden:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "exif -cl"
msgstr "exif -cl"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<exif> was written by Lutz Mueller E<lt>lutz@users.sourceforge.netE<gt> and "
"numerous contributors.  This man page is Copyright \\(co 2002-2012 Thomas "
"Pircher, Dan Fandrich and others."
msgstr ""
"B<exif> wurde von Lutz Mueller E<lt>lutz@users.sourceforge.netE<gt> und "
"einigen anderen Mitwirkenden entwickelt. Copyright für diese Handbuchseite "
"\\(co 2002-2012 Thomas Pircher, Dan Fandrich und weitere."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.UR \"https://libexif.github.io/\"> B<https://libexif.github.io/>"
msgstr "E<.UR \"https://libexif.github.io/\"> B<https://libexif.github.io/>"
