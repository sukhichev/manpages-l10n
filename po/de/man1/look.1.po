# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-08-27 17:06+0200\n"
"PO-Revision-Date: 2022-02-11 15:40+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "LOOK"
msgstr "LOOK"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "look - display lines beginning with a given string"
msgstr ""
"look - Zeilen anzeigen, die mit einer angegebenen Zeichenkette beginnen"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<look> [options] I<string> [I<file>]"
msgstr "B<look> [Optionen] I<Zeichenkette> [I<Datei>]"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<look> utility displays any lines in I<file> which contain I<string> as "
"a prefix. As B<look> performs a binary search, the lines in I<file> must be "
"sorted (where B<sort>(1) was given the same options B<-d> and/or B<-f> that "
"B<look> is invoked with)."
msgstr ""
"Das Dienstprogramm B<look> zeigt alle Zeilen in einer I<Datei> an, welche "
"die I<Zeichenkette> als Präfix enthalten. Da B<look> eine Binärsuche "
"durchführt, müssen die Zeilen in der I<Datei> sortiert sein (wobei "
"B<sort>(1) die selben Optionen B<-d> und/oder B<-f> übergeben werden müssen, "
"mit denen B<look> aufgerufen wird)."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If I<file> is not specified, the file I</usr/share/dict/words> is used, only "
"alphanumeric characters are compared and the case of alphabetic characters "
"is ignored."
msgstr ""
"Falls keine I<Datei> angegeben ist, wird die Datei I</usr/share/dict/words> "
"verwendet. Es werden dann nur alphanumerische Zeichen verglichen und die "
"Groß-/Kleinschreibung von Buchstaben wird ignoriert."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--alternative>"
msgstr "B<-a>, B<--alternative>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use the alternative dictionary file."
msgstr "verwendet die alternative Wörterbuchdatei."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--alphanum>"
msgstr "B<-d>, B<--alphanum>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Use normal dictionary character set and order, i.e., only blanks and "
"alphanumeric characters are compared. This is on by default if no file is "
"specified."
msgstr ""
"verwendet den normalen Zeichensatz und die Reihenfolge des Wörterbuchs, d.h. "
"nur Leerräume und alphanumerische Zeichen werden verglichen. Dies ist "
"standardmäßig aktiviert, wenn keine Datei angegeben ist."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Note that blanks have been added to dictionary character set for "
"compatibility with B<sort -d> command since version 2.28."
msgstr ""
"Beachten Sie, dass mit Version 2.28 zwecks Kompatibilität zum Befehl B<sort -"
"d> zum Zeichensatz des Wörterbuchs Leerräume hinzugefügt wurden."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-f>, B<--ignore-case>"
msgstr "B<-f>, B<--ignore-case>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Ignore the case of alphabetic characters. This is on by default if no file "
"is specified."
msgstr ""
"ignoriert die Groß-/Kleinschreibung von Buchstaben. Dies ist standardmäßig "
"aktiviert, wenn keine Datei angegeben wird."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-t>, B<--terminate> I<character>"
msgstr "B<-t>, B<--terminate> I<Zeichen>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify a string termination character, i.e., only the characters in "
"I<string> up to and including the first occurrence of I<character> are "
"compared."
msgstr ""
"gibt ein Zeichen zur Markierung des Zeichenkettenendes an, d.h. es werden "
"nur die Zeichen in der I<Zeichenkette> bis zu einschließlich des ersten "
"Vorkommens des angegebenen I<Zeichens> verglichen."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<look> utility exits 0 if one or more lines were found and displayed, 1 "
"if no lines were found, and E<gt>1 if an error occurred."
msgstr ""
"Das Dienstprogramm B<look> gibt 0 zurück, wenn eine oder mehrere Zeilen "
"gefunden und angezeigt wurden, 1, wenn keine Zeilen gefunden wurden, und "
"E<gt>1, wenn ein Fehler aufgetreten ist."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<WORDLIST>"
msgstr "B<WORDLIST>"

#. type: Plain text
#: debian-bookworm
msgid ""
"Path to a dictionary file. The environment variable has greater priority "
"than the dictionary path defined in the B<FILES> segment."
msgstr ""
"Pfad zu einer Wörterbuchdatei. Die Umgebungsvariable hat eine höhere "
"Priorität als der im Abschnitt B<DATEIEN> definierte Wörterbuchpfad."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I</usr/share/dict/words>"
msgstr "I</usr/share/dict/words>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "the dictionary"
msgstr "das Wörterbuch"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I</usr/share/dict/web2>"
msgstr "I</usr/share/dict/web2>"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "the alternative dictionary"
msgstr "das alternative Wörterbuch"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The B<look> utility appeared in Version 7 AT&T Unix."
msgstr "Das Dienstprogramm B<look> erschien in Version 7 von AT&T UNIX."

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"sort -d /etc/passwd -o /tmp/look.dict\n"
"look -t: root:foobar /tmp/look.dict\n"
msgstr ""
"sort -d /etc/passwd -o /tmp/look.dict\n"
"look -t: root:foobar /tmp/look.dict\n"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<grep>(1), B<sort>(1)"
msgstr "B<grep>(1), B<sort>(1)"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<look> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<look> ist Teil des Pakets util-linux, welches heruntergeladen "
"werden kann von:"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<look> utility displays any lines in I<file> which contain I<string>. "
"As B<look> performs a binary search, the lines in I<file> must be sorted "
"(where B<sort>(1) was given the same options B<-d> and/or B<-f> that B<look> "
"is invoked with)."
msgstr ""
"Das Dienstprogramm B<look> zeigt alle Zeilen in einer I<Datei> an, welche "
"die I<Zeichenkette> enthalten. Da B<look> eine Binärsuche durchführt, müssen "
"die Zeilen in der I<Datei> sortiert sein (wobei B<sort>(1) die selben "
"Optionen B<-d> und/oder B<-f> übergeben werden müssen, mit denen B<look> "
"aufgerufen wird)."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Path to a dictionary file. The environment variable has greater priority "
"than the dictionary path defined in FILES segment."
msgstr ""
"Pfad zu einer Wörterbuchdatei. Die Umgebungsvariable hat eine höhere "
"Priorität als der im Abschnitt DATEIEN definierte Wörterbuchpfad."
