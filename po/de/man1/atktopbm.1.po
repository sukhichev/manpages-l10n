# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-03-09 15:31+0100\n"
"PO-Revision-Date: 2024-03-09 18:41+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Atktopbm User Manual"
msgstr "Atktopbm-Benutzerhandbuch"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "26 September 1991"
msgstr "26. September 1991"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr "Netpbm-Dokumentation"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

# FIXME PBM → PBM file? (as in other similar pages)
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "atktopbm - convert Andrew Toolkit raster object to PBM"
msgstr "atktopbm - konvertiert ein Andrew-Toolkit-Rasterobjekt in PBM"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<atktopbm> [I<atkfile>]"
msgstr "B<atktopbm> [I<Atkdatei>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME B<Netpbm> → B<netpbm>
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr "Dieses Programm ist Teil von B<netpbm>(1)\\&."

# FIXME input.  and → input and
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<atktopbm> reads an Andrew Toolkit raster object as input.  and produces a "
"PBM image as output."
msgstr ""
"B<atktopbm> liest ein Andrew-Toolkit-Rasterobjekt als Eingabe und erstellt "
"ein PBM-Bild als Ausgabe."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

# FIXME Remove hard line breaks
# FIXME See → see
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"There are no command line options defined specifically\n"
"for B<atktopbm>, but it recognizes the options common to all\n"
"programs based on libnetpbm (See \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&.)\n"
msgstr ""
"Für B<atktopbm> gibt es keine spezifischen Befehlszeilenoptionen, aber es\n"
"werden die gemeinsamen Optionen für alle auf libnetpbm basierenden Programme\n"
"akzeptiert (siehe E<.UR index.html#commonoptions>Common OptionsE<.UE>\\&.)\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: IP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<pbmtoatk>(1)  \\&"
msgstr "B<pbmtoatk>(1)\\&"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "B<pbm>(1)  \\&"
msgstr "B<pbm>(1)\\&"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copyright (C) 1991 by Bill Janssen."
msgstr "Copyright (C) 1991 Bill Janssen."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr "URSPRUNG DES DOKUMENTS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""
"Diese Handbuchseite wurde vom Netpbm-Werkzeug »makeman« aus der HTML-Quelle "
"erstellt. Das Master-Dokument befindet sich unter"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/atktopbm.html>"
msgstr "B<http://netpbm.sourceforge.net/doc/atktopbm.html>"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<pbm>(5)  \\&"
msgstr "B<pbm>(5)\\&"
