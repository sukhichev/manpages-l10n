# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2020,2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-03-01 17:03+0100\n"
"PO-Revision-Date: 2024-02-09 19:46+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "PAM_SYSTEMD_HOME"
msgstr "PAM_SYSTEMD_HOME"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "pam_systemd_home"
msgstr "pam_systemd_home"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"pam_systemd_home - Authenticate users and mount home directories via systemd-"
"homed\\&.service"
msgstr ""
"pam_systemd_home - Mittels systemd-homed\\&.service Benutzer "
"authentifizieren und Home-Verzeichnisse einhängen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "pam_systemd_home\\&.so"
msgstr "pam_systemd_home\\&.so"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<pam_systemd_home> ensures that home directories managed by B<systemd-homed."
"service>(8)  are automatically activated (mounted) on user login, and are "
"deactivated (unmounted) when the last session of the user ends\\&. For such "
"users, it also provides authentication (when per-user disk encryption is "
"used, the disk encryption key is derived from the authentication credential "
"supplied at login time), account management (the \\m[blue]B<JSON user "
"record>\\m[]\\&\\s-2\\u[1]\\d\\s+2 embedded in the home store contains "
"account details), and implements the updating of the encryption password "
"(which is also used for user authentication)\\&."
msgstr ""
"B<pam_systemd_home> stellt sicher, dass durch B<systemd-homed.service>(8) "
"verwaltete Home-Verzeichnisse bei der Benutzeranmeldung automatisch "
"aktiviert (eingehängt) und bei der Beendigung der letzten Sitzung des "
"Benutzers deaktiviert (ausgehängt) werden\\&. Für solche Benutzer stellt es "
"auch eine Authentifizierung (wenn benutzerbezogene Plattenverschlüsselung "
"verwandt wird, wird der Platten-Verschlüsselungsschlüssel aus den "
"Authentifizierungs-Zugangsberechtigungen abgeleitet, die beim Anmelden "
"bereitgestellt wurden) und Kontenverwaltung bereit (der in die Home-"
"Speicherung eingebettete \\m[blue]B<JSON-"
"Benutzerdatensatz>\\m[]\\&\\s-2\\u[1]\\d\\s+2 enthält Kontendetails) und "
"implementiert die Aktualisierung des Verschlüsselungspassworts (das auch für "
"die Benutzer-Authentifizierung verwandt wird)\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The following options are understood:"
msgstr "Die folgenden Optionen werden verstanden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<suspend=>"
msgstr "I<suspend=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Takes a boolean argument\\&. If true, the home directory of the user will be "
"suspended automatically during system suspend; if false it will remain "
"active\\&. Automatic suspending of the home directory improves security "
"substantially as secret key material is automatically removed from memory "
"before the system is put to sleep and must be re-acquired (through user re-"
"authentication) when coming back from suspend\\&. It is recommended to set "
"this parameter for all PAM applications that have support for automatically "
"re-authenticating via PAM on system resume\\&. If multiple sessions of the "
"same user are open in parallel the user\\*(Aqs home directory will be left "
"unsuspended on system suspend as long as at least one of the sessions does "
"not set this parameter to on\\&. Defaults to off\\&."
msgstr ""
"Akzeptiert ein logisches Argument\\&. Falls wahr, wird das Home-Verzeichnis "
"des Benutzers bei der Systemsuspendierung automatisch suspendiert; falls "
"falsch, wird es aktiv bleiben\\&. Automatische Suspendierung des Home-"
"Verzeichnisses erhöht die Sicherheit deutlich, da geheimes Schlüsselmaterial "
"automatisch aus dem Speicher entfernt wird, bevor das System in den "
"Schlafzustand versetzt wird und neues erlangt werden muss (mittels "
"Benutzerauthentifizierung), wenn des System aus der Suspendierung "
"zurückkehrt\\&. Es wird empfohlen, diesen Parameter für alle PAM-Anwendungen "
"zu setzen, die automatische Reauthentifizierung beim Systemaufwachen "
"unterstützen\\&. Falls mehrere Sitzungen des gleichen Benutzers parallel "
"offen sind, wird des Home-Verzeichnis des Benutzers nicht suspendiert, "
"solange mindestens eine dieser Sitzungen den Parameter nicht auf »on« "
"setzt\\&. Standardmäßig aus\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that TTY logins generally do not support re-authentication on system "
"resume\\&. Re-authentication on system resume is primarily a concept "
"implementable in graphical environments, in the form of lock screens brought "
"up automatically when the system goes to sleep\\&. This means that if a user "
"concurrently uses graphical login sessions that implement the required re-"
"authentication mechanism and console logins that do not, the home directory "
"is not locked during suspend, due to the logic explained above\\&. That "
"said, it is possible to set this field for TTY logins too, ignoring the fact "
"that TTY logins actually don\\*(Aqt support the re-authentication "
"mechanism\\&. In that case the TTY sessions will appear hung until the user "
"logs in on another virtual terminal (regardless if via another TTY session "
"or graphically) which will resume the home directory and unblock the "
"original TTY session\\&. (Do note that lack of screen locking on TTY "
"sessions means even though the TTY session appears hung, keypresses can "
"still be queued into it, and the existing screen contents be read without re-"
"authentication; this limitation is unrelated to the home directory "
"management B<pam_systemd_home> and systemd-homed\\&.service implement\\&.)"
msgstr ""
"Beachten Sie, dass TTY-Anmeldungen im Allgemeinen beim Aufwachen keine "
"erneute Authentifizierung unterstützen\\&. Die erneute Authentifizierung "
"beim Aufwachen des Systems ist ein Konzept, das primär von graphischen "
"Umgebungen unterstützt wird, bei denen beim Schlafenlegen automatisch "
"Sperrbildschirme hochgebracht werden\\&. Dies bedeutet, dass das Home-"
"Verzeichnis beim Suspendieren aufgrund der oben beschriebenen Logik nicht "
"gesperrt wird, wenn der Benutzer parallel graphische Anmeldungen, die die "
"benötigte erneute Authentifizierung unterstützen, und Konsole-Anmeldungen, "
"bei denen dies nicht der Fall ist, verwendet\\&. Trotzdem ist es möglich, "
"dieses Feld auch für TTY-Anmeldungen zu verwenden und die Tatsache zu "
"ignorieren, dass TTY-Anmeldungen den Mechanismus der erneuten "
"Authentifizierung nicht unterstützen\\&. In diesem Fall erscheint die TTY-"
"Sitzung aufgehangen, bis der Benutzer sich in einem anderen virtuellen "
"Terminal anmeldet (unabhängig davon, ob dies eine andere TTY-Sitzung oder "
"graphisch erfolgt), wodurch das Home-Verzeichnis wieder aufgeweckt wird und "
"die Blockade der ursprünglichen TTY-Sitzung aufgehoben wird\\. (Beachten "
"Sie, dass das Fehlen von Bildschirmsperren bei TTY-Sitzungen bedeutet, dass "
"Tastenanschläge weiterhin in die Warteschlange gestellt werden können, "
"obwohl die TTY-Sitzung aufgehangen erscheint, und die bestehenden "
"Bildschirminhalte ohne erneute Authentifizierung gelesen werden können; "
"diese Einschränkung steht nicht in Bezug zu der Verwaltung des Home-"
"Verzeichnisses durch B<pam_systemd_home> und systemd-homed\\&.service\\&.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Turning this option on by default is highly recommended for all sessions, "
"but only if the service managing these sessions correctly implements the "
"aforementioned re-authentication\\&. Note that the re-authentication must "
"take place from a component running outside of the user\\*(Aqs context, so "
"that it does not require access to the user\\*(Aqs home directory for "
"operation\\&. Traditionally, most desktop environments do not implement "
"screen locking this way, and need to be updated accordingly\\&."
msgstr ""
"Es wird für alle Sitzungen nachdrücklich empfohlen, diese Option "
"einzuschalten, aber nur, falls der Dienst, der diese Sitzungen verwaltet, "
"die vorab erwähnte erneute Authentifizierung korrekt implementiert\\&. "
"Beachten Sie, dass die erneute Authentifizierung durch eine Komponente "
"stattfinden muss, die außerhalb des Kontextes des Benutzers läuft, so dass "
"sie keinen Zugriff auf das Home-Verzeichnis für diese Aktion benötigt\\&. "
"Traditionell implementieren die meisten Desktop-Umgebungen Bildschirmschoner "
"nicht auf diese Art und müssen entsprechend aktualisiert werden\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This setting may also be controlled via the I<$SYSTEMD_HOME_SUSPEND> "
"environment variable (see below), which B<pam_systemd_home> reads during "
"initialization and sets for sessions\\&. If both the environment variable is "
"set and the module parameter specified the latter takes precedence\\&."
msgstr ""
"Diese Einstellung kann auch mittels der Umgebungsvariable "
"I<$SYSTEMD_HOME_SUSPEND> (siehe unten) gesteuert werden, die "
"B<pam_systemd_home> während der Initialisierung liest und für Sitzungen "
"setzt\\&. Falls sowohl die Umgebungsvariable gesetzt als auch der "
"Modulparameter angegeben ist, hat der Modulparameter Vorrang\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "Added in version 245\\&."
msgstr "Hinzugefügt in Version 245\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<debug>[=]"
msgstr "I<debug>[=]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Takes an optional boolean argument\\&. If yes or without the argument, the "
"module will log debugging information as it operates\\&."
msgstr ""
"Akzeptiert ein optionales logisches Argument\\&. Falls »yes« oder ohne "
"Argument, wird das Modul Fehlersuchinformationen beim Betrieb "
"protokollieren\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "MODULE TYPES PROVIDED"
msgstr "BEREITGESTELLTE MODULTYPEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The module implements all four PAM operations: B<auth> (to allow "
"authentication using the encrypted data), B<account> (because users with "
"systemd-homed\\&.service user accounts are described in a \\m[blue]B<JSON "
"user record>\\m[]\\&\\s-2\\u[1]\\d\\s+2 and may be configured in more detail "
"than in the traditional Linux user database), B<session> (because user "
"sessions must be tracked in order to implement automatic release when the "
"last session of the user is gone), B<password> (to change the encryption "
"password \\(em also used for user authentication \\(em through PAM)\\&."
msgstr ""
"Dieses Modul implementiert alle vier PAM-Aktionen: B<auth> "
"(Authentifizierung erlauben, um die verschlüsselten Daten zu benutzen), "
"B<account> (da Benutzer mit systemd-homed\\&.service-Benutzerkonten in einem "
"\\m[blue]B<JSON-Benutzerdatensatz>\\m[]\\&\\s-2\\u[1]\\d\\s+2 beschrieben "
"und detaillierter konfiguriert werden können, als in der traditionellen "
"Linux-Benutzerdatenbank), B<session> (da Benutzersitzungen nachverfolgt "
"werden müssen, um automatische Freigaben zu implementieren, wenn die letzte "
"Sitzung eines Benutzers verschwunden ist), B<password> (um das "
"Verschlüsselungspasswort über PAM zu ändern \\(en auch für die "
"Authentifizierung verwandt)\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The following environment variables are initialized by the module and "
"available to the processes of the user\\*(Aqs session:"
msgstr ""
"Die folgenden Umgebungsvariablen werden durch das Modul initialisiert und "
"stehen den Prozessen der Sitzung des Benutzers zur Verfügung:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_HOME=1>"
msgstr "I<$SYSTEMD_HOME=1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Indicates that the user\\*(Aqs home directory is managed by systemd-homed\\&."
"service\\&."
msgstr ""
"Zeigt an, dass das Home-Verzeichnis des Benutzers durch systemd-homed\\&."
"service verwaltet wird\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<$SYSTEMD_HOME_SUSPEND=>"
msgstr "I<$SYSTEMD_HOME_SUSPEND=>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Indicates whether the session has been registered with the suspend mechanism "
"enabled or disabled (see above)\\&. The variable\\*(Aqs value is either "
"\"0\" or \"1\"\\&. Note that the module both reads the variable when "
"initializing, and sets it for sessions\\&."
msgstr ""
"Zeigt an, ob die Sitzung mit dem Suspendierungs-Mechanismus aktiviert oder "
"deaktiviert registriert wurde (siehe oben)\\&. Der Wert der Variablen ist "
"entweder »0« oder »1«\\&. Beachten Sie, dass das Modul sowohl bei der "
"Initialisierung den Wert liest und ihn dann auch für Sitzungen setzt\\&."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "Added in version 246\\&."
msgstr "Hinzugefügt in Version 246\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Here\\*(Aqs an example PAM configuration fragment that permits users managed "
"by systemd-homed\\&.service to log in:"
msgstr ""
"Es folgt ein Beispiel-PAM-Konfigurationsfragment, das die Verwendung von "
"durch systemd-homed\\&.service verwalteten Benutzern bei der Anmeldung "
"erlaubt:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#%PAM-1\\&.0\n"
"auth      sufficient pam_unix\\&.so\n"
"B<-auth     sufficient pam_systemd_home\\&.so>\n"
"auth      required   pam_deny\\&.so\n"
msgstr ""
"#%PAM-1\\&.0\n"
"auth      sufficient pam_unix\\&.so\n"
"B<-auth     sufficient pam_systemd_home\\&.so>\n"
"auth      required   pam_deny\\&.so\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"account   required   pam_nologin\\&.so\n"
"B<-account  sufficient pam_systemd_home\\&.so>\n"
"account   sufficient pam_unix\\&.so\n"
"account   required   pam_permit\\&.so\n"
msgstr ""
"account   required   pam_nologin\\&.so\n"
"B<-account  sufficient pam_systemd_home\\&.so>\n"
"account   sufficient pam_unix\\&.so\n"
"account   required   pam_permit\\&.so\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<-password sufficient pam_systemd_home\\&.so>\n"
"password  sufficient pam_unix\\&.so sha512 shadow try_first_pass\n"
"password  required   pam_deny\\&.so\n"
msgstr ""
"B<-password sufficient pam_systemd_home\\&.so>\n"
"password  sufficient pam_unix\\&.so sha512 shadow try_first_pass\n"
"password  required   pam_deny\\&.so\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"-session  optional   pam_keyinit\\&.so revoke\n"
"-session  optional   pam_loginuid\\&.so\n"
"B<-session  optional   pam_systemd_home\\&.so>\n"
"-session  optional   pam_systemd\\&.so\n"
"session   required   pam_unix\\&.so\n"
msgstr ""
"-session  optional   pam_keyinit\\&.so revoke\n"
"-session  optional   pam_loginuid\\&.so\n"
"B<-session  optional   pam_systemd_home\\&.so>\n"
"-session  optional   pam_systemd\\&.so\n"
"session   required   pam_unix\\&.so\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-homed.service>(8), B<homed.conf>(5), B<homectl>(1), "
"B<pam_systemd>(8), B<pam.conf>(5), B<pam.d>(5), B<pam>(8)"
msgstr ""
"B<systemd>(1), B<systemd-homed.service>(8), B<homed.conf>(5), B<homectl>(1), "
"B<pam_systemd>(8), B<pam.conf>(5), B<pam.d>(5), B<pam>(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "JSON user record"
msgstr "JSON-Benutzerdatensatz"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "\\%https://systemd.io/USER_RECORD/"
msgstr "\\%https://systemd.io/USER_RECORD/"

#. type: TH
#: debian-bookworm opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"B<-password sufficient pam_systemd_home\\&.so>\n"
"password  sufficient pam_unix\\&.so sha512 shadow try_first_pass use_authtok\n"
"password  required   pam_deny\\&.so\n"
msgstr ""
"B<-password sufficient pam_systemd_home\\&.so>\n"
"password  sufficient pam_unix\\&.so sha512 shadow try_first_pass use_authtok\n"
"password  required   pam_deny\\&.so\n"
