# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2024-03-01 16:55+0100\n"
"PO-Revision-Date: 2023-05-23 09:02+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DNF-DOWNLOAD"
msgstr "DNF-DOWNLOAD"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Jan 22, 2023"
msgstr "22. Januar 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "4.3.1"
msgstr "4.3.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "dnf-plugins-core"
msgstr "dnf-plugins-core"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "dnf-download - DNF download Plugin"
msgstr "reposync - Download-Plugin von DNF"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Download binary or source packages."
msgstr "Binär- oder Quellpakete herunterladen."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dnf download [options] E<lt>pkg-specE<gt>...>"
msgstr "B<dnf download [Optionen] E<lt>PaketE<gt> …>"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ARGUMENTS"
msgstr "ARGUMENTE"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<E<lt>pkg-specE<gt>>"
msgstr "B<E<lt>PaketE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Package specification for the package to download.  Local RPMs can be "
"specified as well. This is useful with the B<--source> option or if you want "
"to download the same RPM again."
msgstr ""
"Angabe des herunterzuladenden Pakets. Auch lokale RPM-Pakete können "
"angegeben werden. Dies ist mit der Option B<--source> nützlich oder wenn Sie "
"ein RPM-Paket erneut herunterladen wollen."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""
"Alle allgemeinen DNF-Optionen werden akzeptiert, siehe I<Optionen> in "
"B<dnf>(8) für Details."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help-cmd>"
msgstr "B<--help-cmd>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Show this help."
msgstr "zeigt die Hilfe an."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--arch E<lt>archE<gt>[,E<lt>archE<gt>...]>"
msgstr "B<--arch E<lt>ArchitekturE<gt>[,E<lt>ArchitekturE<gt> …]>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Limit the query to packages of given architectures (default is all "
"compatible architectures with your system). To download packages with arch "
"incompatible with your system use B<--forcearch=E<lt>archE<gt>> option to "
"change basearch."
msgstr ""
"begrenzt die Abfrage auf Pakete der angegebenen Architekturen (standardmäßig "
"werden alle Architekturen berücksichtigt, die zu Ihrem System kompatibel "
"sind). Um Pakete herunterzuladen, deren Architektur zu Ihrem System "
"inkompatibel ist, verwenden Sie die Option B<--"
"forcearch=E<lt>ArchitekturE<gt>>, um die Basisarchitektur zu ändern."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--source>"
msgstr "B<--source>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Download the source rpm. Enables source repositories of all enabled binary "
"repositories."
msgstr ""
"lädt das Quell-RPM-Paket herunter. Dadurch werden die Quell-Paketquellen "
"aller aktivierten Binärpaketquellen zusätzlich aktiviert."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--debuginfo>"
msgstr "B<--debuginfo>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Download the debuginfo rpm. Enables debuginfo repositories of all enabled "
"binary repositories."
msgstr ""
"lädt das Debuginfo-Paket herunter. Dadurch werden die Debuginfo-Paketquellen "
"aller aktivierten Binärpaketquellen zusätzlich aktiviert."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--downloaddir>"
msgstr "B<--downloaddir>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Download directory, default is the current directory (the directory must "
"exist)."
msgstr ""
"gibt das Downloadverzeichnis an. Standardmäßig ist dies das aktuelle "
"Verzeichnis (das Verzeichnis muss existieren)."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--url>"
msgstr "B<--url>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Instead of downloading, print list of urls where the rpms can be downloaded."
msgstr ""
"listet nur die URLs auf, von denen RPM-Pakete heruntergeladen werden können, "
"lädt aber keine RPMs herunter."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--urlprotocol>"
msgstr "B<--urlprotocol>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Limit the protocol of the urls output by the –url option. Options are http, "
"https, rsync, ftp."
msgstr ""
"begrenzt das Protokoll der mit der Option B<--url> ausgegebenen URLs. "
"Optionen sind http, https, rsync, ftp."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--resolve>"
msgstr "B<--resolve>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Resolves dependencies of specified packages and downloads missing "
"dependencies in the system."
msgstr ""
"löst die Abhängigkeiten der angegebenen Pakete auf und lädt im System "
"fehlende Abhängigkeiten herunter."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--alldeps>"
msgstr "B<--alldeps>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When used with B<--resolve>, download all dependencies (do not skip already "
"installed ones)."
msgstr ""
"lädt bei Verwendung mit B<--resolve> alle Abhängigkeiten herunter (und "
"überspringt dabei bereits installierte Pakete nicht)."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<dnf download dnf>"
msgstr "B<dnf download dnf>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Download the latest dnf package to the current directory."
msgstr "lädt das aktuellste dnf-Paket in das aktuelle Verzeichnis herunter."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<dnf download --url dnf>"
msgstr "B<dnf download --url dnf>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Just print the remote location url where the dnf rpm can be downloaded from."
msgstr ""
"gibt nur die ferne URL aus, von der das dnf-Paket heruntergeladen werden "
"kann."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<dnf download --url --urlprotocols=https --urlprotocols=rsync dnf>"
msgstr "B<dnf download --url --urlprotocols=https --urlprotocols=rsync dnf>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Same as above, but limit urls to https or rsync urls."
msgstr ""
"ist das Gleiche wie oben, aber begrenzt die URLs auf die Protokolle https "
"oder rsync."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<dnf download dnf --destdir /tmp/dnl>"
msgstr "B<dnf download dnf --destdir /tmp/dnl>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Download the latest dnf package to the /tmp/dnl directory (the directory "
"must exist)."
msgstr ""
"lädt das aktuellste dnf-Paket in das Verzeichnis /tmp/dnl herunter (das "
"Verzeichnis muss existieren)."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<dnf download dnf --source>"
msgstr "B<dnf download dnf --source>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Download the latest dnf source package to the current directory."
msgstr ""
"lädt das aktuellste dnf-Quellpaket in das aktuelle Verzeichnis herunter."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<dnf download rpm --debuginfo>"
msgstr "B<dnf download rpm --debuginfo>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Download the latest rpm-debuginfo package to the current directory."
msgstr ""
"lädt das aktuellste rpm-Debuginfo-Paket in das aktuelle Verzeichnis herunter."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<dnf download btanks --resolve>"
msgstr "B<dnf download btanks --resolve>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Download the latest btanks package and the uninstalled dependencies to the "
"current directory."
msgstr ""
"lädt das aktuellste btanks-Paket und die noch nicht installierten "
"Abhängigkeiten in das aktuelle Verzeichnis herunter."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr "Siehe AUTHORS im Paket der Core DNF Plugins."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr "2023, Red Hat, lizenziert unter GPLv2+"

#. type: TH
#: fedora-40 fedora-rawhide
#, no-wrap
msgid "Feb 08, 2024"
msgstr "08. Februar 2024"

#. type: TH
#: fedora-40 fedora-rawhide
#, no-wrap
msgid "4.5.0"
msgstr "4.5.0"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Limit the protocol of the urls output by the --url option. Options are http, "
"https, rsync, ftp."
msgstr ""
"begrenzt das Protokoll der mit der Option B<--url> ausgegebenen URLs. "
"Optionen sind http, https, rsync, ftp."

#.  Generated by docutils manpage writer.
#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-tumbleweed
msgid "2024, Red Hat, Licensed under GPLv2+"
msgstr "2024, Red Hat, lizenziert unter GPLv2+"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Jan 30, 2024"
msgstr "30. Januar 2024"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "4.4.3"
msgstr "4.4.3"

#.  Generated by docutils manpage writer.
#. type: Plain text
#: mageia-cauldron
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr "2014, Red Hat, lizenziert unter GPLv2+"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Nov 03, 2021"
msgstr "3. November 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "4.0.24"
msgstr "4.0.24"

#.  Generated by docutils manpage writer.
#. type: Plain text
#: opensuse-leap-15-6
msgid "2021, Red Hat, Licensed under GPLv2+"
msgstr "2021, Red Hat, lizenziert unter GPLv2+"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Feb 06, 2024"
msgstr "6. Februar 2024"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "4.4.4"
msgstr "4.4.4"
