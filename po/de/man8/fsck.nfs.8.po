# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2023-06-27 19:27+0200\n"
"PO-Revision-Date: 2022-04-22 17:46+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FSCK.NFS"
msgstr "FSCK.NFS"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "May 2004"
msgstr "Mai 2004"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Initscripts"
msgstr "Initscripts"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

# FIXME No full stop in NAME
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "fsck.nfs - Dummy fsck.nfs script that always returns success."
msgstr "fsck.nfs - Pseudo-fsck.nfs-Skript, das immer Erfolg zurückmeldet"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<fsck.nfs>"
msgstr "B<fsck.nfs>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Debian GNU/Linux need this for when the root file system is on NFS: there is "
"no way to find out if root is NFS mounted and we really want to do a \"fsck -"
"a /\"."
msgstr ""
"Debian GNU/Linux benötigt dies für den Fall, dass sich das Wurzeldateisystem "
"auf NFS befindet, da es keine Möglichkeit gibt, herauszufinden, ob die "
"Wurzel ein eingehängtes NFS ist und ein »fsck -a« wirklich immer "
"durchgeführt werden sollte."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "EXIT CODE"
msgstr "EXIT-CODE"

# FIXME B<mount.nfs> → B<fsck.nfs>?
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The exit code returned by B<mount.nfs> is always zero, meaning successful "
"completion."
msgstr ""
"Der von B<mount.nfs> zurückgelieferte Exit-Code ist immer Null, was "
"erfolgreichen Abschluss bedeutet."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<fsck>(8), B<fstab>(5)."
msgstr "B<fsck>(8), B<fstab>(5)."
