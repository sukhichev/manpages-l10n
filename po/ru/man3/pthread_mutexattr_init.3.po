# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Kogan, Darima <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:04+0100\n"
"PO-Revision-Date: 2019-10-12 08:58+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "See B<pthread_attr_init>(3)."
msgid "pthread_mutexattr_init"
msgstr "Смотрите B<pthread_attr_init>(3)."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"pthread_mutexattr_init, pthread_mutexattr_destroy - initialize and destroy a "
"mutex attributes object"
msgstr ""
"pthread_mutexattr_init, pthread_mutexattr_destroy - инициализирует и "
"уничтожает объект атрибутов мьютекса"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr "B<#include E<lt>pthread.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_mutexattr_init(pthread_mutexattr_t *>I<attr>B<);>\n"
"B<int pthread_mutexattr_destroy(pthread_mutexattr_t *>I<attr>B<);>\n"
msgstr ""
"B<int pthread_mutexattr_init(pthread_mutexattr_t *>I<attr>B<);>\n"
"B<int pthread_mutexattr_destroy(pthread_mutexattr_t *>I<attr>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The B<pthread_mutexattr_init>()  function initializes the mutex attributes "
"object pointed to by I<attr> with default values for all attributes defined "
"by the implementation."
msgstr ""
"Функция B<pthread_mutexattr_init>() инициализирует объект атрибутов "
"мьютекса, на который указывает I<attr>, значениями по умолчанию для всех "
"атрибутов, определённых реализацией."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The results of initializing an already initialized mutex attributes object "
"are undefined."
msgstr ""
"Если для инициализации указан уже инициализированный объект атрибутов "
"мьютекса, то результат не определён."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The B<pthread_mutexattr_destroy>()  function destroys a mutex attribute "
"object (making it uninitialized).  Once a mutex attributes object has been "
"destroyed, it can be reinitialized with B<pthread_mutexattr_init>()."
msgstr ""
"Функция B<pthread_mutexattr_destroy>() уничтожает объект атрибутов мьютекса "
"(делает его не инициализированным). После уничтожения объекта атрибутов "
"мьютекса, его можно инициализировать с помощью B<pthread_mutexattr_init>()."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The results of destroying an uninitialized mutex attributes object are "
"undefined."
msgstr ""
"Если для уничтожения указан не инициализированный объект атрибутов мьютекса, "
"то результат не определён."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"On success, these functions return 0.  On error, they return a positive "
"error number."
msgstr ""
"При успешном выполнении эти функции возвращают 0. При ошибке возвращается "
"положительный номер ошибки."

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Subsequent changes to a mutex attributes object do not affect mutex that "
"have already been initialized using that object."
msgstr ""
"Изменения объекта атрибутов мьютекса не влияют на мьютексы, которые уже были "
"инициализированы с помощью этого объекта."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<pthread_mutex_init>(3), B<pthread_mutexattr_getpshared>(3), "
"B<pthread_mutexattr_getrobust>(3), B<pthreads>(7)"
msgstr ""
"B<pthread_mutex_init>(3), B<pthread_mutexattr_getpshared>(3), "
"B<pthread_mutexattr_getrobust>(3), B<pthreads>(7)"

#. type: TH
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "PTHREAD_MUTEXATTR_INIT"
msgid "PTHREAD_MUTEXATTR"
msgstr "PTHREAD_MUTEXATTR_INIT"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "LinuxThreads"
msgstr "LinuxThreads"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "pthread_mutexattr_init, pthread_mutexattr_destroy - initialize and "
#| "destroy a mutex attributes object"
msgid ""
"pthread_mutexattr_init, pthread_mutexattr_destroy, "
"pthread_mutexattr_settype, pthread_mutexattr_gettype - mutex creation "
"attributes"
msgstr ""
"pthread_mutexattr_init, pthread_mutexattr_destroy - инициализирует и "
"уничтожает объект атрибутов мьютекса"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<#include E<lt>pthread.hE<gt>>"
msgstr "B<#include E<lt>pthread.hE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid "B<int pthread_mutex_consistent(pthread_mutex_t *>I<mutex>B<);>\n"
msgid "B<int pthread_mutexattr_init(pthread_mutexattr_t *>I<attr>B<);>"
msgstr "B<int pthread_mutex_consistent(pthread_mutex_t *>I<mutex>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid "B<int pthread_mutex_consistent(pthread_mutex_t *>I<mutex>B<);>\n"
msgid "B<int pthread_mutexattr_destroy(pthread_mutexattr_t *>I<attr>B<);>"
msgstr "B<int pthread_mutex_consistent(pthread_mutex_t *>I<mutex>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "B<int pthread_setschedprio(pthread_t >I<thread>B<, int >I<prio>B<);>\n"
msgid ""
"B<int pthread_mutexattr_settype(pthread_mutexattr_t *>I<attr>B<, int "
">I<kind>B<);>"
msgstr "B<int pthread_setschedprio(pthread_t >I<thread>B<, int >I<prio>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "B<int pthread_mutex_consistent(const pthread_mutex_t *>I<mutex>B<);>\n"
msgid ""
"B<int pthread_mutexattr_gettype(const pthread_mutexattr_t *>I<attr>B<, int "
"*>I<kind>B<);>"
msgstr "B<int pthread_mutex_consistent(const pthread_mutex_t *>I<mutex>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Mutex attributes can be specified at mutex creation time, by passing a mutex "
"attribute object as second argument to B<pthread_mutex_init>(3).  Passing "
"B<NULL> is equivalent to passing a mutex attribute object with all "
"attributes set to their default values."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "The B<pthread_mutexattr_init>()  function initializes the mutex "
#| "attributes object pointed to by I<attr> with default values for all "
#| "attributes defined by the implementation."
msgid ""
"B<pthread_mutexattr_init> initializes the mutex attribute object I<attr> and "
"fills it with default values for the attributes."
msgstr ""
"Функция B<pthread_mutexattr_init>() инициализирует объект атрибутов "
"мьютекса, на который указывает I<attr>, значениями по умолчанию для всех "
"атрибутов, определённых реализацией."

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "The B<pthread_mutexattr_destroy>()  function destroys a mutex attribute "
#| "object (making it uninitialized).  Once a mutex attributes object has "
#| "been destroyed, it can be reinitialized with B<pthread_mutexattr_init>()."
msgid ""
"B<pthread_mutexattr_destroy> destroys a mutex attribute object, which must "
"not be reused until it is reinitialized. B<pthread_mutexattr_destroy> does "
"nothing in the LinuxThreads implementation."
msgstr ""
"Функция B<pthread_mutexattr_destroy>() уничтожает объект атрибутов мьютекса "
"(делает его не инициализированным). После уничтожения объекта атрибутов "
"мьютекса, его можно инициализировать с помощью B<pthread_mutexattr_init>()."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"LinuxThreads supports only one mutex attribute: the mutex kind, which is "
"either B<PTHREAD_MUTEX_FAST_NP> for ``fast'' mutexes, "
"B<PTHREAD_MUTEX_RECURSIVE_NP> for ``recursive'' mutexes, or "
"B<PTHREAD_MUTEX_ERRORCHECK_NP> for ``error checking'' mutexes.  As the B<NP> "
"suffix indicates, this is a non-portable extension to the POSIX standard and "
"should not be employed in portable programs."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The mutex kind determines what happens if a thread attempts to lock a mutex "
"it already owns with B<pthread_mutex_lock>(3). If the mutex is of the "
"``fast'' kind, B<pthread_mutex_lock>(3) simply suspends the calling thread "
"forever.  If the mutex is of the ``error checking'' kind, "
"B<pthread_mutex_lock>(3) returns immediately with the error code "
"B<EDEADLK>.  If the mutex is of the ``recursive'' kind, the call to "
"B<pthread_mutex_lock>(3) returns immediately with a success return code. The "
"number of times the thread owning the mutex has locked it is recorded in the "
"mutex. The owning thread must call B<pthread_mutex_unlock>(3) the same "
"number of times before the mutex returns to the unlocked state."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "The default mutex kind is ``fast'', that is, B<PTHREAD_MUTEX_FAST_NP>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "B<pthread_mutexattr_setpshared>()  sets the value of the process-shared "
#| "attribute of the mutex attributes object referred to by I<attr> to the "
#| "value specified in B<pshared>."
msgid ""
"B<pthread_mutexattr_settype> sets the mutex kind attribute in I<attr> to the "
"value specified by I<kind>."
msgstr ""
"Функция B<pthread_mutexattr_setpshared>() изменяет значения общепроцессного "
"атрибута объекта атрибутов мьютекса, на который указывает I<attr>, на "
"значение, заданное в B<pshared>."

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "B<pthread_mutexattr_getpshared>()  places the value of the process-shared "
#| "attribute of the mutex attributes object referred to by I<attr> in the "
#| "location pointed to by I<pshared>."
msgid ""
"B<pthread_mutexattr_gettype> retrieves the current value of the mutex kind "
"attribute in I<attr> and stores it in the location pointed to by I<kind>."
msgstr ""
"Функция B<pthread_mutexattr_getpshared>() помещает значения общепроцессного "
"атрибута объекта атрибутов мьютекса, на который указывает I<attr>, в "
"расположение, указанное в I<pshared>."

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "B<pthread_mutex_init>(3), B<pthread_mutexattr_getpshared>(3), "
#| "B<pthread_mutexattr_getrobust>(3), B<pthreads>(7)"
msgid ""
"B<pthread_mutexattr_init>, B<pthread_mutexattr_destroy> and "
"B<pthread_mutexattr_gettype> always return 0."
msgstr ""
"B<pthread_mutex_init>(3), B<pthread_mutexattr_getpshared>(3), "
"B<pthread_mutexattr_getrobust>(3), B<pthreads>(7)"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid "The function returns zero on success, and a nonzero value on error."
msgid ""
"B<pthread_mutexattr_settype> returns 0 on success and a non-zero error code "
"on error."
msgstr ""
"Функция возвращает ноль при успешном выполнении и ненулевое значение при "
"ошибке."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "B<pthread_mutexattr_setpshared>()  can fail with the following errors:"
msgid ""
"On error, B<pthread_mutexattr_settype> returns the following error code:"
msgstr ""
"Функция B<pthread_mutexattr_setpshared>() может завершаться со следующими "
"ошибками:"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<kind> is neither B<PTHREAD_MUTEX_FAST_NP> nor "
"B<PTHREAD_MUTEX_RECURSIVE_NP> nor B<PTHREAD_MUTEX_ERRORCHECK_NP>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОРЫ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"
msgstr "Xavier Leroy E<lt>Xavier.Leroy@inria.frE<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
#| msgid ""
#| "B<pthread_mutex_init>(3), B<pthread_mutex_lock>(3), "
#| "B<pthread_spin_lock>(3), B<pthread_spin_unlock>(3), B<pthreads>(7)"
msgid ""
"B<pthread_mutex_init>(3), B<pthread_mutex_lock>(3), "
"B<pthread_mutex_unlock>(3)."
msgstr ""
"B<pthread_mutex_init>(3), B<pthread_mutex_lock>(3), B<pthread_spin_lock>(3), "
"B<pthread_spin_unlock>(3), B<pthreads>(7)"

#. type: TH
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"
