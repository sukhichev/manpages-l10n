# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2012-2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:52+0100\n"
"PO-Revision-Date: 2019-08-11 08:40+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<atexit>()"
msgid "atexit"
msgstr "B<atexit>()"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "atexit - register a function to be called at normal process termination"
msgstr ""
"atexit - регистрирует функцию, вызываемую при обычном завершении программы"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int atexit(void (*>I<function>B<)(void));>\n"
msgstr "B<int atexit(void (*>I<function>B<)(void));>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<atexit>()  function registers the given I<function> to be called at "
"normal process termination, either via B<exit>(3)  or via return from the "
"program's I<main>().  Functions so registered are called in the reverse "
"order of their registration; no arguments are passed."
msgstr ""
"Функция B<atexit>() регистрирует передаваемую функцию I<function> в качестве "
"функции, вызываемой при нормальном завершении работы программы, например, с "
"помощью B<exit>(3), или при завершении работы функции I<main>. "
"Зарегистрированные функции вызываются в порядке, обратном регистрации; "
"никаких аргументов им не передаётся."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The same function may be registered multiple times: it is called once for "
"each registration."
msgstr ""
"Одна функция может быть зарегистрирована несколько раз: она вызывается один "
"раз для каждой регистрации."

#.  POSIX.1-2001, POSIX.1-2008
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 requires that an implementation allow at least B<ATEXIT_MAX> (32) "
"such functions to be registered.  The actual limit supported by an "
"implementation can be obtained using B<sysconf>(3)."
msgstr ""
"В POSIX.1 требуется, чтобы реализация позволяла регистрировать не менее "
"B<ATEXIT_MAX> (32) таких функций. Установленный лимит, поддерживаемый "
"реализацией, можно определить с помощью B<sysconf>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When a child process is created via B<fork>(2), it inherits copies of its "
"parent's registrations.  Upon a successful call to one of the B<exec>(3)  "
"functions, all registrations are removed."
msgstr ""
"Когда дочерний процесс создаётся через B<fork>(2), он наследует копии "
"регистраций родителя. При успешном выполнении одной из функций B<exec>(3) "
"все регистрации снимаются."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<atexit>()  function returns the value 0 if successful; otherwise it "
"returns a nonzero value."
msgstr ""
"Функция B<atexit>() при успешном выполнении возвращает 0, в противном случае "
"— ненулевое значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<atexit>()"
msgstr "B<atexit>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#.  POSIX.1-2001, POSIX.1-2008
#.  This can happen on OpenBSD 4.2 for example, and is documented
#.  as occurring on FreeBSD as well.
#.  glibc does "the Right Thing" -- invocation of the remaining
#.  exit handlers carries on as normal.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 says that the result of calling B<exit>(3)  more than once (i.e., "
"calling B<exit>(3)  within a function registered using B<atexit>())  is "
"undefined.  On some systems (but not Linux), this can result in an infinite "
"recursion; portable programs should not invoke B<exit>(3)  inside a function "
"registered using B<atexit>()."
msgstr ""
"В POSIX.1 указано, что при многократном вызове B<exit>(3) (т. е., вызов "
"B<exit>(3) из функций, зарегистрированных B<atexit>()) состояние программы "
"не определено. В некоторых системах (не в Linux), это приводит к бесконечной "
"рекурсии; переносимые программы не должны вызывать B<exit>(3) из функций, "
"зарегистрированных через B<atexit>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, C99, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Functions registered using B<atexit>()  (and B<on_exit>(3))  are not called "
"if a process terminates abnormally because of the delivery of a signal."
msgstr ""
"Функции, зарегистрированные с помощью B<atexit>() (и B<on_exit>(3)) не "
"вызываются, если процесс завершился некорректно, например из-за полученного "
"сигнала."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If one of the registered functions calls B<_exit>(2), then any remaining "
"functions are not invoked, and the other process termination steps performed "
"by B<exit>(3)  are not performed."
msgstr ""
"Если одна из зарегистрированных функций вызывает B<_exit>(2), то оставшиеся "
"функции не вызываются, и другие шаги по завершению процесса, производимые "
"B<exit>(3), не выполняются."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<atexit>()  and B<on_exit>(3)  functions register functions on the same "
"list: at normal process termination, the registered functions are invoked in "
"reverse order of their registration by these two functions."
msgstr ""
"Функции B<atexit>() и B<on_exit>(3) регистрируют функции в едином списке: "
"при нормальном завершении процесса зарегистрированные функции вызываются в "
"порядке обратном регистрации с помощью данных функций."

#.  In glibc, things seem to be handled okay
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"According to POSIX.1, the result is undefined if B<longjmp>(3)  is used to "
"terminate execution of one of the functions registered using B<atexit>()."
msgstr ""
"Согласно POSIX.1 результат является не определённым, если при завершении "
"выполнения одной из функций, зарегистрированной с помощью B<atexit>(), "
"используется B<longjmp>(3)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux notes"
msgstr "Замечания, касающиеся Linux"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since glibc 2.2.3, B<atexit>()  (and B<on_exit>(3))  can be used within a "
"shared library to establish functions that are called when the shared "
"library is unloaded."
msgstr ""
"Начиная с glibc 2.2.3, функция B<atexit>() (и B<on_exit>(3)) может быть "
"использована в общей библиотеке для указания функций, который вызываются при "
"выгрузке общей библиотеки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"void\n"
"bye(void)\n"
"{\n"
"    printf(\"That was all, folks\\en\");\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    long a;\n"
"    int i;\n"
"\\&\n"
"    a = sysconf(_SC_ATEXIT_MAX);\n"
"    printf(\"ATEXIT_MAX = %ld\\en\", a);\n"
"\\&\n"
"    i = atexit(bye);\n"
"    if (i != 0) {\n"
"        fprintf(stderr, \"cannot set exit function\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<_exit>(2), B<dlopen>(3), B<exit>(3), B<on_exit>(3)"
msgstr "B<_exit>(2), B<dlopen>(3), B<exit>(3), B<on_exit>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"void\n"
"bye(void)\n"
"{\n"
"    printf(\"That was all, folks\\en\");\n"
"}\n"
msgstr ""
"void\n"
"bye(void)\n"
"{\n"
"    printf(\"Но вот и всё\\en\");\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    long a;\n"
"    int i;\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    long a;\n"
"    int i;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    a = sysconf(_SC_ATEXIT_MAX);\n"
"    printf(\"ATEXIT_MAX = %ld\\en\", a);\n"
msgstr ""
"    a = sysconf(_SC_ATEXIT_MAX);\n"
"    printf(\"ATEXIT_MAX = %ld\\en\", a);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    i = atexit(bye);\n"
"    if (i != 0) {\n"
"        fprintf(stderr, \"cannot set exit function\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    i = atexit(bye);\n"
"    if (i != 0) {\n"
"        fprintf(stderr, \"не удалось установить функцию при выходе\\en\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
