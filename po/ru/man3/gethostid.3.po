# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:57+0100\n"
"PO-Revision-Date: 2019-10-05 08:06+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<gethostid>()"
msgid "gethostid"
msgstr "B<gethostid>()"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"gethostid, sethostid - get or set the unique identifier of the current host"
msgstr ""
"gethostid, sethostid - возвращает или назначает уникальный идентификатор "
"текущего узла"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sethostid(long >I<hostid>B<);>"
msgid ""
"B<long gethostid(void);>\n"
"B<int sethostid(long >I<hostid>B<);>\n"
msgstr "B<int sethostid(long >I<hostid>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<gethostid>():"
msgstr "B<gethostid>():"

#.         || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#.         || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.21:\n"
#| "        _DEFAULT_SOURCE\n"
#| "    In glibc 2.19 and 2.20:\n"
#| "        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
#| "    Up to and including glibc 2.19:\n"
#| "        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
msgid ""
"    Since glibc 2.20:\n"
"        _DEFAULT_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
"    Up to and including glibc 2.19:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    начиная с glibc 2.21:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc 2.19 и 2.20:\n"
"        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"
"    до glibc 2.19, включительно:\n"
"        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE\\ E<lt>\\ 500)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sethostid>():"
msgstr "B<sethostid>():"

#.              commit 266865c0e7b79d4196e2cc393693463f03c90bd8
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.21:\n"
"        _DEFAULT_SOURCE\n"
"    In glibc 2.19 and 2.20:\n"
"        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE E<lt> 500)\n"
"    Up to and including glibc 2.19:\n"
"        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE E<lt> 500)\n"
msgstr ""
"    начиная с glibc 2.21:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc 2.19 и 2.20:\n"
"        _DEFAULT_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE E<lt> 500)\n"
"    до glibc 2.19, включительно:\n"
"        _BSD_SOURCE || (_XOPEN_SOURCE && _XOPEN_SOURCE E<lt> 500)\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<gethostid>()  and B<sethostid>()  respectively get or set a unique 32-"
#| "bit identifier for the current machine.  The 32-bit identifier is "
#| "intended to be unique among all UNIX systems in existence.  This normally "
#| "resembles the Internet address for the local machine, as returned by "
#| "B<gethostbyname>(3), and thus usually never needs to be set."
msgid ""
"B<gethostid>()  and B<sethostid>()  respectively get or set a unique 32-bit "
"identifier for the current machine.  The 32-bit identifier was intended to "
"be unique among all UNIX systems in existence.  This normally resembles the "
"Internet address for the local machine, as returned by B<gethostbyname>(3), "
"and thus usually never needs to be set."
msgstr ""
"Функции B<gethostid>() и B<sethostid>(), соответственно, возвращают и "
"устанавливают уникальный 32-битный идентификатор текущей машины. Данный 32-"
"битный идентификатор считается уникальным среди всех существующих систем "
"UNIX. Обычно это напоминает Интернет-адрес локальной машины, возвращаемый "
"B<gethostbyname>(3), и поэтому, как правило, его не нужно изменять."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<sethostid>()  call is restricted to the superuser."
msgstr "Только суперпользователь может вызывать B<sethostid>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<gethostid>()  returns the 32-bit identifier for the current host as set by "
"B<sethostid>()."
msgstr ""
"Функция B<gethostid>() возвращает 32-битный идентификатор текущего узла, "
"установленный B<sethostid>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<sethostid>()  returns 0; on error, -1 is returned, and "
"I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<sethostid>() возвращает 0; при ошибке — -1, а в "
"I<errno> задаётся код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sethostid>()  can fail with the following errors:"
msgstr "Вызов B<sethostid>() может завершиться со следующими ошибками:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The caller did not have permission to write to the file used to store the "
"host ID."
msgstr ""
"Вызывающий не имеет прав на запись в файл, используемый для хранения ID узла."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The calling process's effective user or group ID is not the same as its "
"corresponding real ID."
msgstr ""
"Эффективный идентификатор пользователя или группы вызывающего процесса не "
"совпадает с его соответствующим реальным идентификатором."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. #-#-#-#-#  archlinux: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<gethostid>()"
msgstr "B<gethostid>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe hostid env locale"
msgstr "MT-Safe hostid env locale"

#. #-#-#-#-#  archlinux: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-40: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-6: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-tumbleweed: gethostid.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<sethostid>()"
msgstr "B<sethostid>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe const:hostid"
msgstr "MT-Unsafe const:hostid"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#.  libc5 used /etc/hostid; libc4 didn't have these functions
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In the glibc implementation, the I<hostid> is stored in the file I</etc/"
#| "hostid>.  (In glibc versions before 2.2, the file I</var/adm/hostid> was "
#| "used.)"
msgid ""
"In the glibc implementation, the I<hostid> is stored in the file I</etc/"
"hostid>.  (Before glibc 2.2, the file I</var/adm/hostid> was used.)"
msgstr ""
"В реализации glibc I<hostid> сохраняет значение в файле I</etc/hostid> (в "
"glibc до версии 2.2 используется файл I</var/adm/hostid>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the glibc implementation, if B<gethostid>()  cannot open the file "
"containing the host ID, then it obtains the hostname using "
"B<gethostname>(2), passes that hostname to B<gethostbyname_r>(3)  in order "
"to obtain the host's IPv4 address, and returns a value obtained by bit-"
"twiddling the IPv4 address.  (This value may not be unique.)"
msgstr ""
"В реализации glibc, если B<gethostid>() не может открыть файл, содержащий ID "
"узла, она получает имя узла с помощью B<gethostname>(2), передаёт это имя "
"B<gethostbyname_r>(3) для получения адреса IPv4 узла, и возвращает значение, "
"получаемое преобразованием битов полученного адреса IPv4 (такое значение "
"может быть не уникально)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "4.2BSD; these functions were dropped in 4.4BSD.  SVr4 includes "
#| "B<gethostid>()  but not B<sethostid>()."
msgid ""
"4.2BSD; dropped in 4.4BSD.  SVr4 and POSIX.1-2001 include B<gethostid>()  "
"but not B<sethostid>()."
msgstr ""
"42BSD; эти функции удалены в 4.4BSD. В SVr4 содержится B<gethostid>(), но "
"отсутствует B<sethostid>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "It is impossible to ensure that the identifier is globally unique."
msgstr ""
"Невозможно достоверно сказать, что идентификатор является глобально "
"уникальным."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<hostid>(1), B<gethostbyname>(3)"
msgstr "B<hostid>(1), B<gethostbyname>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"4.2BSD; these functions were dropped in 4.4BSD.  SVr4 includes "
"B<gethostid>()  but not B<sethostid>()."
msgstr ""
"42BSD; эти функции удалены в 4.4BSD. В SVr4 содержится B<gethostid>(), но "
"отсутствует B<sethostid>()."

#. type: Plain text
#: debian-bookworm
msgid ""
"POSIX.1-2001 and POSIX.1-2008 specify B<gethostid>()  but not B<sethostid>()."
msgstr ""
"В POSIX.1-2001 и POSIX.1-2008 определена B<gethostid>(), но отсутствует "
"B<sethostid>()."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
