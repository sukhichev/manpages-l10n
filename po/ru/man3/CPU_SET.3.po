# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:54+0100\n"
"PO-Revision-Date: 2019-10-05 07:56+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CPU_SET"
msgstr "CPU_SET"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"CPU_SET, CPU_CLR, CPU_ISSET, CPU_ZERO, CPU_COUNT, CPU_AND, CPU_OR, CPU_XOR, "
"CPU_EQUAL, CPU_ALLOC, CPU_ALLOC_SIZE, CPU_FREE, CPU_SET_S, CPU_CLR_S, "
"CPU_ISSET_S, CPU_ZERO_S, CPU_COUNT_S, CPU_AND_S, CPU_OR_S, CPU_XOR_S, "
"CPU_EQUAL_S - macros for manipulating CPU sets"
msgstr ""
"CPU_SET, CPU_CLR, CPU_ISSET, CPU_ZERO, CPU_COUNT, CPU_AND, CPU_OR, CPU_XOR, "
"CPU_EQUAL, CPU_ALLOC, CPU_ALLOC_SIZE, CPU_FREE, CPU_SET_S, CPU_CLR_S, "
"CPU_ISSET_S, CPU_ZERO_S, CPU_COUNT_S, CPU_AND_S, CPU_OR_S, CPU_XOR_S, "
"CPU_EQUAL_S - макросы для работы с наборами ЦП"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>sched.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* Смотрите feature_test_macros(7) */\n"
"B<#include E<lt>sched.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<void CPU_ZERO(cpu_set_t *>I<set>B<);>\n"
msgstr "B<void CPU_ZERO(cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_SET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
msgstr ""
"B<void CPU_SET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_COUNT(cpu_set_t *>I<set>B<);>\n"
msgstr "B<int  CPU_COUNT(cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_AND(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
msgstr ""
"B<void CPU_AND(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_EQUAL(cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"
msgstr "B<int  CPU_EQUAL(cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<cpu_set_t *CPU_ALLOC(int >I<num_cpus>B<);>\n"
"B<void CPU_FREE(cpu_set_t *>I<set>B<);>\n"
"B<size_t CPU_ALLOC_SIZE(int >I<num_cpus>B<);>\n"
msgstr ""
"B<cpu_set_t *CPU_ALLOC(int >I<num_cpus>B<);>\n"
"B<void CPU_FREE(cpu_set_t *>I<set>B<);>\n"
"B<size_t CPU_ALLOC_SIZE(int >I<num_cpus>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<void CPU_ZERO_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
msgstr "B<void CPU_ZERO_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_SET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
msgstr ""
"B<void CPU_SET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_COUNT_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
msgstr "B<int  CPU_COUNT_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_AND_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
msgstr ""
"B<void CPU_AND_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_EQUAL_S(size_t >I<setsize>B<, cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"
msgstr "B<int  CPU_EQUAL_S(size_t >I<setsize>B<, cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<cpu_set_t> data structure represents a set of CPUs.  CPU sets are used "
"by B<sched_setaffinity>(2)  and similar interfaces."
msgstr ""
"Набор ЦП описывается структурой данных I<cpu_set_t>. Набор ЦП используется в "
"B<sched_setaffinity>(2) и подобных интерфейсах."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<cpu_set_t> data type is implemented as a bit mask.  However, the data "
"structure should be treated as opaque: all manipulation of CPU sets should "
"be done via the macros described in this page."
msgstr ""
"Тип данных I<cpu_set_t> реализован в виде битовой маски. Однако структуру "
"данных следует считать «чёрным ящиком»: все действия с наборами ЦП следует "
"выполнять с помощью макросов, описанных на этой странице."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following macros are provided to operate on the CPU set I<set>:"
msgstr "Для работы с набором ЦП I<set> предоставляются следующие макросы:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ZERO>()"
msgstr "B<CPU_ZERO>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Clears I<set>, so that it contains no CPUs."
msgstr "Очищает I<set>, после этого он не содержит ЦП."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_SET>()"
msgstr "B<CPU_SET>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Add CPU I<cpu> to I<set>."
msgstr "Добавляет ЦП I<cpu> в I<set>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_CLR>()"
msgstr "B<CPU_CLR>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Remove CPU I<cpu> from I<set>."
msgstr "Удаляет ЦП I<cpu> из I<set>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ISSET>()"
msgstr "B<CPU_ISSET>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Test to see if CPU I<cpu> is a member of I<set>."
msgstr "Проверяет, является ли ЦП I<cpu> членом I<set>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_COUNT>()"
msgstr "B<CPU_COUNT>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Return the number of CPUs in I<set>."
msgstr "Возвращает количество ЦП в I<set>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Where a I<cpu> argument is specified, it should not produce side effects, "
"since the above macros may evaluate the argument more than once."
msgstr ""
"Макросы, имеющие аргумент I<cpu>, не вызывают побочных эффектов, все "
"перечисленные выше макросы могут использовать аргумент более одного раза."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The first CPU on the system corresponds to a I<cpu> value of 0, the next CPU "
"corresponds to a I<cpu> value of 1, and so on.  No assumptions should be "
"made about particular CPUs being available, or the set of CPUs being "
"contiguous, since CPUs can be taken offline dynamically or be otherwise "
"absent.  The constant B<CPU_SETSIZE> (currently 1024) specifies a value one "
"greater than the maximum CPU number that can be stored in I<cpu_set_t>."
msgstr ""
"Значение первого ЦП системы в I<cpu> обозначается как 0, значение следующего "
"ЦП в I<cpu> равно 1 и т. д. Нельзя предполагать о доступности каких-либо ЦП, "
"или что набор ЦП непрерывен, так как ЦП могут динамически отключаться или "
"вообще отсутствовать. Константой B<CPU_SETSIZE> (в настоящее время 1024) "
"задаётся значение на 1 большее, чем максимальный номер ЦП, который можно "
"хранить в I<cpu_set_t>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following macros perform logical operations on CPU sets:"
msgstr "Следующие макросы выполняют логические операции над наборами ЦП:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_AND>()"
msgstr "B<CPU_AND>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Store the intersection of the sets I<srcset1> and I<srcset2> in I<destset> "
"(which may be one of the source sets)."
msgstr ""
"Сохраняет пересечение наборов I<srcset1> и I<srcset2> в I<destset> (который "
"может быть одним из наборов-аргументов)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_OR>()"
msgstr "B<CPU_OR>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Store the union of the sets I<srcset1> and I<srcset2> in I<destset> (which "
"may be one of the source sets)."
msgstr ""
"Сохраняет объединение наборов I<srcset1> и I<srcset2> в I<destset> (который "
"может быть одним из наборов-аргументов)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_XOR>()"
msgstr "B<CPU_XOR>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Store the XOR of the sets I<srcset1> and I<srcset2> in I<destset> (which may "
"be one of the source sets).  The XOR means the set of CPUs that are in "
"either I<srcset1> or I<srcset2>, but not both."
msgstr ""
"Сохраняет результат XOR наборов I<srcset1> и I<srcset2> в I<destset> "
"(который может быть одним из наборов-аргументов). Операция XOR возвращает "
"набор из ЦП, которые есть в одном из I<srcset1> или I<srcset2>, но не в "
"обоих одновременно."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_EQUAL>()"
msgstr "B<CPU_EQUAL>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Test whether two CPU set contain exactly the same CPUs."
msgstr "Проверяет, что два набора ЦП содержат одинаковые ЦП."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Dynamically sized CPU sets"
msgstr "Динамически изменяемые наборы ЦП"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Because some applications may require the ability to dynamically size CPU "
"sets (e.g., to allocate sets larger than that defined by the standard "
"I<cpu_set_t> data type), glibc nowadays provides a set of macros to support "
"this."
msgstr ""
"Так как некоторым приложениям может потребоваться наборы ЦП динамически "
"изменяемых размеров (например, для выделения наборов больше, чем определено "
"стандартным типом данных I<cpu_set_t>), glibc предоставляет набор макросов "
"для этого."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following macros are used to allocate and deallocate CPU sets:"
msgstr "Следующие макросы используются для выделения и удаления наборов ЦП:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ALLOC>()"
msgstr "B<CPU_ALLOC>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Allocate a CPU set large enough to hold CPUs in the range 0 to I<num_cpus-1>."
msgstr ""
"Выделяет набор ЦП, достаточный для хранения ЦП в диапазоне от 0 до "
"I<num_cpus-1>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ALLOC_SIZE>()"
msgstr "B<CPU_ALLOC_SIZE>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return the size in bytes of the CPU set that would be needed to hold CPUs in "
"the range 0 to I<num_cpus-1>.  This macro provides the value that can be "
"used for the I<setsize> argument in the B<CPU_*_S>()  macros described below."
msgstr ""
"Возвращает размер набора ЦП в байтах, который необходим для хранения ЦП в "
"диапазоне от 0 до I<num_cpus-1>. Данный макрос предоставляет значение, "
"которое можно использовать в аргументе I<setsize> в макросах B<CPU_*_S>(), "
"описанных далее."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_FREE>()"
msgstr "B<CPU_FREE>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Free a CPU set previously allocated by B<CPU_ALLOC>()."
msgstr "Освобождает набор ЦП, выделенный ранее с помощью B<CPU_ALLOC>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The macros whose names end with \"_S\" are the analogs of the similarly "
"named macros without the suffix.  These macros perform the same tasks as "
"their analogs, but operate on the dynamically allocated CPU set(s) whose "
"size is I<setsize> bytes."
msgstr ""
"Макросы, чьи имена заканчиваются на «_S», аналогичны макросам с тем же "
"именем без суффикса. Эти макросы выполняют те же задачи что и их аналоги, но "
"они работают с динамически выделяемыми наборами ЦП, размер которых равен "
"I<setsize> байт."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_ISSET>()  and B<CPU_ISSET_S>()  return nonzero if I<cpu> is in I<set>; "
"otherwise, it returns 0."
msgstr ""
"Макросы B<CPU_ISSET>() и B<CPU_ISSET_S>() возвращают ненулевое значение, "
"если I<cpu> есть в I<set>; в противном случае возвращается 0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_COUNT>()  and B<CPU_COUNT_S>()  return the number of CPUs in I<set>."
msgstr ""
"Макросы B<CPU_COUNT>() и B<CPU_COUNT_S>() возвращают количество ЦП в I<set>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_EQUAL>()  and B<CPU_EQUAL_S>()  return nonzero if the two CPU sets are "
"equal; otherwise they return 0."
msgstr ""
"Макросы B<CPU_EQUAL>() и B<CPU_EQUAL_S>() возвращают ненулевое значение, "
"если два набора ЦП одинаковы; в противном случае возвращается 0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_ALLOC>()  returns a pointer on success, or NULL on failure.  (Errors "
"are as for B<malloc>(3).)"
msgstr ""
"При успешном выполнении макрос B<CPU_ALLOC>() возвращает указатель, при "
"ошибке возвращается NULL (ошибки те же, что и у B<malloc>(3))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_ALLOC_SIZE>()  returns the number of bytes required to store a CPU set "
"of the specified cardinality."
msgstr ""
"Макрос B<CPU_ALLOC_SIZE>() возвращает количество байт, требуемое для "
"хранения набора ЦП указанных элементов."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The other functions do not return a value."
msgstr "Другие функции не возвращают никаких значений."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<CPU_ZERO>(), B<CPU_SET>(), B<CPU_CLR>(), and B<CPU_ISSET>()  macros "
"were added in glibc 2.3.3."
msgstr ""
"Макросы B<CPU_ZERO>(), B<CPU_SET>(), B<CPU_CLR>() и B<CPU_ISSET>() добавлены "
"в glibc версии 2.3.3."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<CPU_COUNT>()  first appeared in glibc 2.6."
msgstr "Макрос B<CPU_COUNT>() впервые появилась в glibc версии 2.6."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_AND>(), B<CPU_OR>(), B<CPU_XOR>(), B<CPU_EQUAL>(), B<CPU_ALLOC>(), "
"B<CPU_ALLOC_SIZE>(), B<CPU_FREE>(), B<CPU_ZERO_S>(), B<CPU_SET_S>(), "
"B<CPU_CLR_S>(), B<CPU_ISSET_S>(), B<CPU_AND_S>(), B<CPU_OR_S>(), "
"B<CPU_XOR_S>(), and B<CPU_EQUAL_S>()  first appeared in glibc 2.7."
msgstr ""
"Макросы B<CPU_AND>(), B<CPU_OR>(), B<CPU_XOR>(), B<CPU_EQUAL>(), "
"B<CPU_ALLOC>(), B<CPU_ALLOC_SIZE>(), B<CPU_FREE>(), B<CPU_ZERO_S>(), "
"B<CPU_SET_S>(), B<CPU_CLR_S>(), B<CPU_ISSET_S>(), B<CPU_AND_S>(), "
"B<CPU_OR_S>(), B<CPU_XOR_S>() и B<CPU_EQUAL_S>() впервые появились в glibc "
"версии 2.7."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To duplicate a CPU set, use B<memcpy>(3)."
msgstr "Для создания копии набора ЦП используйте B<memcpy>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since CPU sets are bit masks allocated in units of long words, the actual "
"number of CPUs in a dynamically allocated CPU set will be rounded up to the "
"next multiple of I<sizeof(unsigned long)>.  An application should consider "
"the contents of these extra bits to be undefined."
msgstr ""
"Так как наборы ЦП это битовые маски, выделяемые в единицах длинных слов, "
"реальное количество ЦП в динамически выделенном наборе ЦП будет округлено до "
"следующего кратного I<sizeof(unsigned long)>. Приложение должно считать "
"содержимое этих дополнительных бит неопределённым."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Notwithstanding the similarity in the names, note that the constant "
"B<CPU_SETSIZE> indicates the number of CPUs in the I<cpu_set_t> data type "
"(thus, it is effectively a count of the bits in the bit mask), while the "
"I<setsize> argument of the B<CPU_*_S>()  macros is a size in bytes."
msgstr ""
"Несмотря на похожесть имён, заметим, что константа B<CPU_SETSIZE> определяет "
"количество ЦП в типе данных I<cpu_set_t> (то есть, эффективное количество "
"битов в битовой маске), в то время как аргумент I<setsize> макросов "
"B<CPU_*_S>() — это размер в байтах."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The data types for arguments and return values shown in the SYNOPSIS are "
"hints what about is expected in each case.  However, since these interfaces "
"are implemented as macros, the compiler won't necessarily catch all type "
"errors if you violate the suggestions."
msgstr ""
"Типы данных параметров и возвращаемых значений, показанных в ОБЗОРЕ — "
"подсказки, что ожидается в каждом случае. Однако так как эти интерфейсы "
"реализованы как макросы, компилятор не обязательно поймает все ошибки "
"приведения типов, если вы нарушите предположения."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#.  http://sourceware.org/bugzilla/show_bug.cgi?id=7029
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On 32-bit platforms with glibc 2.8 and earlier, B<CPU_ALLOC>()  allocates "
"twice as much space as is required, and B<CPU_ALLOC_SIZE>()  returns a value "
"twice as large as it should.  This bug should not affect the semantics of a "
"program, but does result in wasted memory and less efficient operation of "
"the macros that operate on dynamically allocated CPU sets.  These bugs are "
"fixed in glibc 2.9."
msgstr ""
"На 32-разрядных платформах в glibc 2.8 и ранее B<CPU_ALLOC> () выделяет "
"вдвое больше пространства, как требуется, и B<CPU_ALLOC_SIZE>() возвращает "
"значение вдвое большее, чем должно. Эта ошибка не должна влиять на семантику "
"программы, но приводит к трате впустую памяти и менее эффективной работе "
"макросов для динамически выделяемых наборов ЦП. Эти ошибки исправлены в "
"glibc версии 2.9."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following program demonstrates the use of some of the macros used for "
"dynamically allocated CPU sets."
msgstr ""
"Следующая программа показывает использование некоторых макросов, работающих "
"с динамически выделяемыми наборами ЦП."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>sched.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    cpu_set_t *cpusetp;\n"
"    size_t size, num_cpus;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>num-cpusE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    num_cpus = atoi(argv[1]);\n"
"\\&\n"
"    cpusetp = CPU_ALLOC(num_cpus);\n"
"    if (cpusetp == NULL) {\n"
"        perror(\"CPU_ALLOC\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    size = CPU_ALLOC_SIZE(num_cpus);\n"
"\\&\n"
"    CPU_ZERO_S(size, cpusetp);\n"
"    for (size_t cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
"        CPU_SET_S(cpu, size, cpusetp);\n"
"\\&\n"
"    printf(\"CPU_COUNT() of set:    %d\\en\", CPU_COUNT_S(size, cpusetp));\n"
"\\&\n"
"    CPU_FREE(cpusetp);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sched_setaffinity>(2), B<pthread_attr_setaffinity_np>(3), "
"B<pthread_setaffinity_np>(3), B<cpuset>(7)"
msgstr ""
"B<sched_setaffinity>(2), B<pthread_attr_setaffinity_np>(3), "
"B<pthread_setaffinity_np>(3), B<cpuset>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-09"
msgstr "9 октября 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid "These interfaces are Linux-specific."
msgstr "Данные интерфейсы есть только в Linux."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "#define _GNU_SOURCE\n"
#| "#include E<lt>pthread.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>errno.hE<gt>\n"
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>sched.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>pthread.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "#include E<lt>assert.hE<gt>\n"
msgstr "#include E<lt>assert.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "int\n"
#| "main(int argc, char *argv[])\n"
#| "{\n"
#| "    cpu_set_t *cpusetp;\n"
#| "    size_t size;\n"
#| "    int num_cpus, cpu;\n"
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    cpu_set_t *cpusetp;\n"
"    size_t size, num_cpus;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    cpu_set_t *cpusetp;\n"
"    size_t size;\n"
"    int num_cpus, cpu;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>num-cpusE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Использование: %s E<lt>количество-ЦПE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    num_cpus = atoi(argv[1]);\n"
msgstr "    num_cpus = atoi(argv[1]);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    cpusetp = CPU_ALLOC(num_cpus);\n"
"    if (cpusetp == NULL) {\n"
"        perror(\"CPU_ALLOC\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    cpusetp = CPU_ALLOC(num_cpus);\n"
"    if (cpusetp == NULL) {\n"
"        perror(\"CPU_ALLOC\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    size = CPU_ALLOC_SIZE(num_cpus);\n"
msgstr "    size = CPU_ALLOC_SIZE(num_cpus);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "    CPU_ZERO_S(size, cpusetp);\n"
#| "    for (cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
#| "        CPU_SET_S(cpu, size, cpusetp);\n"
msgid ""
"    CPU_ZERO_S(size, cpusetp);\n"
"    for (size_t cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
"        CPU_SET_S(cpu, size, cpusetp);\n"
msgstr ""
"    CPU_ZERO_S(size, cpusetp);\n"
"    for (cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
"        CPU_SET_S(cpu, size, cpusetp);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    printf(\"CPU_COUNT() of set:    %d\\en\", CPU_COUNT_S(size, cpusetp));\n"
msgstr "    printf(\"CPU_COUNT() набора:    %d\\en\", CPU_COUNT_S(size, cpusetp));\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    CPU_FREE(cpusetp);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    CPU_FREE(cpusetp);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-05-03"
msgstr "3 мая 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
