# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:07+0100\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sem_wait"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sem_wait, sem_timedwait, sem_trywait - lock a semaphore"
msgstr "sem_wait, sem_timedwait, sem_trywait - блокирует семафор"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>semaphore.hE<gt>>\n"
msgstr "B<#include E<lt>semaphore.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int semtimedop(int >I<semid>B<, struct sembuf *>I<sops>B<, size_t >I<nsops>B<,>\n"
#| "B<               const struct timespec *>I<timeout>B<);>\n"
msgid ""
"B<int sem_wait(sem_t *>I<sem>B<);>\n"
"B<int sem_trywait(sem_t *>I<sem>B<);>\n"
"B<int sem_timedwait(sem_t *restrict >I<sem>B<,>\n"
"B<                  const struct timespec *restrict >I<abs_timeout>B<);>\n"
msgstr ""
"B<int semtimedop(int >I<semid>B<, struct sembuf *>I<sops>B<, size_t >I<nsops>B<,>\n"
"B<               const struct timespec *>I<timeout>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "B<sigtimedwait>(2)"
msgid "B<sem_timedwait>():"
msgstr "B<sigtimedwait>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sem_wait>()  decrements (locks) the semaphore pointed to by I<sem>.  If "
"the semaphore's value is greater than zero, then the decrement proceeds, and "
"the function returns, immediately.  If the semaphore currently has the value "
"zero, then the call blocks until either it becomes possible to perform the "
"decrement (i.e., the semaphore value rises above zero), or a signal handler "
"interrupts the call."
msgstr ""
"Функция B<sem_wait>() уменьшает (блокирует) семафор, на который указывает "
"I<sem>. Если значение семафор больше нуля, то выполняется уменьшение и "
"функция сразу завершается. Если значение семафора равно нулю, то вызов "
"блокируется до тех пор, пока не станет возможным выполнить уменьшение (т. "
"е., значение семафора не станет больше нуля), или пока не вызовется "
"обработчик сигнала."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sem_trywait>()  is the same as B<sem_wait>(), except that if the decrement "
"cannot be immediately performed, then call returns an error (I<errno> set to "
"B<EAGAIN>)  instead of blocking."
msgstr ""
"Функция B<sem_trywait>() подобна B<sem_wait>(), за исключением того, что "
"если уменьшение нельзя выполнить сразу, то вызов завершается с ошибкой "
"(I<errno> становится равным B<EAGAIN>), а не блокируется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sem_timedwait>()  is the same as B<sem_wait>(), except that "
#| "I<abs_timeout> specifies a limit on the amount of time that the call "
#| "should block if the decrement cannot be immediately performed.  The "
#| "I<abs_timeout> argument points to a structure that specifies an absolute "
#| "timeout in seconds and nanoseconds since the Epoch, 1970-01-01 00:00:00 "
#| "+0000 (UTC).  This structure is defined as follows:"
msgid ""
"B<sem_timedwait>()  is the same as B<sem_wait>(), except that I<abs_timeout> "
"specifies a limit on the amount of time that the call should block if the "
"decrement cannot be immediately performed.  The I<abs_timeout> argument "
"points to a B<timespec>(3)  structure that specifies an absolute timeout in "
"seconds and nanoseconds since the Epoch, 1970-01-01 00:00:00 +0000 (UTC)."
msgstr ""
"Функция B<sem_timedwait>() подобна B<sem_wait>(), за исключением того, что в "
"I<abs_timeout> задаётся ограничение по количеству времени, на которое вызов "
"должен заблокироваться, если уменьшение невозможно выполнить сразу. Аргумент "
"I<abs_timeout> указывает на структуру, в которой задаётся абсолютное время "
"ожидания в секундах и наносекундах, начиная с эпохи, 1970-01-01 00:00:00 "
"+0000 (UTC). Эта структура определена следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the timeout has already expired by the time of the call, and the "
"semaphore could not be locked immediately, then B<sem_timedwait>()  fails "
"with a timeout error (I<errno> set to B<ETIMEDOUT>)."
msgstr ""
"Если на момент вызова время ожидания уже истекло и семафор нельзя "
"заблокировать сразу, то B<sem_timedwait>() завершается с ошибкой просрочки "
"(I<errno> становится равным B<ETIMEDOUT>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the operation can be performed immediately, then B<sem_timedwait>()  "
"never fails with a timeout error, regardless of the value of "
"I<abs_timeout>.  Furthermore, the validity of I<abs_timeout> is not checked "
"in this case."
msgstr ""
"Если операцию можно выполнить сразу, то B<sem_timedwait>() никогда не "
"завершится с ошибкой просрочки, независимо от значения I<abs_timeout>. Кроме "
"того, в этом случае не проверяется корректность I<abs_timeout>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All of these functions return 0 on success; on error, the value of the "
"semaphore is left unchanged, -1 is returned, and I<errno> is set to indicate "
"the error."
msgstr ""
"При успешном выполнении все функции возвращают 0; при ошибке значение "
"семафора не изменяется, возвращается -1, а в I<errno> указывается причина "
"ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The operation could not be performed without blocking (i.e., the "
#| "semaphore currently has the value zero)."
msgid ""
"(B<sem_trywait>())  The operation could not be performed without blocking (i."
"e., the semaphore currently has the value zero)."
msgstr ""
"Операция не может быть выполнена без блокировки (т. е., значение семафор "
"равно нулю)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The call was interrupted by a signal handler; see B<signal>(7)."
msgstr "Вызов был прерван обработчиком сигнала; смотрите B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<sem> is not a valid semaphore."
msgstr "Значение I<sem> не является корректным для семафора."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The value of I<abs_timeout.tv_nsecs> is less than 0, or greater than or "
#| "equal to 1000 million."
msgid ""
"(B<sem_timedwait>())  The value of I<abs_timeout.tv_nsecs> is less than 0, "
"or greater than or equal to 1000 million."
msgstr ""
"Значение I<abs_timeout.tv_nsecs> меньше 0, или больше или равно 1000 "
"миллионов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ETIMEDOUT>"
msgstr "B<ETIMEDOUT>"

#.  POSIX.1-2001 also allows EDEADLK -- "A deadlock condition
#.  was detected", but this does not occur on Linux(?).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The call timed out before the semaphore could be locked."
msgid ""
"(B<sem_timedwait>())  The call timed out before the semaphore could be "
"locked."
msgstr "Истёк период ожидания в вызове раньше возможности блокировки семафора."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<sem_wait>(),\n"
"B<sem_trywait>(),\n"
"B<sem_timedwait>()"
msgstr ""
"B<sem_wait>(),\n"
"B<sem_trywait>(),\n"
"B<sem_timedwait>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The (somewhat trivial) program shown below operates on an unnamed "
"semaphore.  The program expects two command-line arguments.  The first "
"argument specifies a seconds value that is used to set an alarm timer to "
"generate a B<SIGALRM> signal.  This handler performs a B<sem_post>(3)  to "
"increment the semaphore that is being waited on in I<main()> using "
"B<sem_timedwait>().  The second command-line argument specifies the length "
"of the timeout, in seconds, for B<sem_timedwait>().  The following shows "
"what happens on two different runs of the program:"
msgstr ""
"Программа (несколько упрощённая), показанная ниже, работает с безымянным "
"семафором. Она ожидает два аргумента командной строки. В первом аргументе "
"задаётся значение в секундах, которое используется в будильнике для "
"генерации сигнала B<SIGALRM>. Этот обработчик выполняет B<sem_post>(3) для "
"увеличения семафора, которого ждёт в I<main()> вызов B<sem_timedwait>(). Во "
"втором аргументе задаётся период ожидания в секундах для B<sem_timedwait>(). "
"Далее показано что происходит в двух разных запусках программы:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out 2 3>\n"
"About to call sem_timedwait()\n"
"sem_post() from handler\n"
"sem_timedwait() succeeded\n"
"$B< ./a.out 2 1>\n"
"About to call sem_timedwait()\n"
"sem_timedwait() timed out\n"
msgstr ""
"$B< ./a.out 2 3>\n"
"About to call sem_timedwait()\n"
"sem_post() из обработчика\n"
"sem_timedwait() выполнена успешно\n"
"$B< ./a.out 2 1>\n"
"About to call sem_timedwait()\n"
"истекло время ожидания sem_timedwait()\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"sem_t sem;\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() from handler\\en\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() failed\\en\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>alarm-secsE<gt> E<lt>wait-secsE<gt>\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"
"\\&\n"
"    /* Establish SIGALRM handler; set alarm timer using argv[1]. */\n"
"\\&\n"
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"
"\\&\n"
"    alarm(atoi(argv[1]));\n"
"\\&\n"
"    /* Calculate relative interval as current time plus\n"
"       number of seconds given argv[2]. */\n"
"\\&\n"
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
"\\&\n"
"    ts.tv_sec += atoi(argv[2]);\n"
"\\&\n"
"    printf(\"%s() about to call sem_timedwait()\\en\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Restart if interrupted by handler. */\n"
"\\&\n"
"    /* Check what happened. */\n"
"\\&\n"
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() timed out\\en\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() succeeded\\en\");\n"
"\\&\n"
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<clock_gettime>(2), B<sem_getvalue>(3), B<sem_post>(3), "
#| "B<sem_overview>(7), B<time>(7)"
msgid ""
"B<clock_gettime>(2), B<sem_getvalue>(3), B<sem_post>(3), B<timespec>(3), "
"B<sem_overview>(7), B<time>(7)"
msgstr ""
"B<clock_gettime>(2), B<sem_getvalue>(3), B<sem_post>(3), B<sem_overview>(7), "
"B<time>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "#include E<lt>stdlib.hE<gt>\n"
#| "#include E<lt>stdint.hE<gt>\n"
#| "#include E<lt>unistd.hE<gt>\n"
#| "#include E<lt>stdio.hE<gt>\n"
#| "#include E<lt>sys/types.hE<gt>\n"
#| "#include E<lt>fcntl.hE<gt>\n"
msgid ""
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>semaphore.hE<gt>\n"
"#include E<lt>signal.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>time.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "#include E<lt>assert.hE<gt>\n"
msgstr "#include E<lt>assert.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "sem_t sem;\n"
msgstr "sem_t sem;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() from handler\\en\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"sem_post() failed\\en\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"
msgstr ""
"static void\n"
"handler(int sig)\n"
"{\n"
"    write(STDOUT_FILENO, \"sem_post() из обработчика\\en\", 24);\n"
"    if (sem_post(&sem) == -1) {\n"
"        write(STDERR_FILENO, \"ошибка sem_post()\\en\", 18);\n"
"        _exit(EXIT_FAILURE);\n"
"    }\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    struct sigaction sa;\n"
"    struct timespec ts;\n"
"    int s;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>alarm-secsE<gt> E<lt>wait-secsE<gt>\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Использование: %s E<lt>alarm-secsE<gt> E<lt>wait-secsE<gt>\\en\",\n"
"                argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"
msgstr ""
"    if (sem_init(&sem, 0, 0) == -1)\n"
"        handle_error(\"sem_init\");\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "    /* Establish SIGALRM handler; set alarm timer using argv[1] */\n"
msgid "    /* Establish SIGALRM handler; set alarm timer using argv[1]. */\n"
msgstr ""
"    /* установка обработчика SIGALRM; зададим таймер будильника,\n"
"       используя argv[1] */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"
msgstr ""
"    sa.sa_handler = handler;\n"
"    sigemptyset(&sa.sa_mask);\n"
"    sa.sa_flags = 0;\n"
"    if (sigaction(SIGALRM, &sa, NULL) == -1)\n"
"        handle_error(\"sigaction\");\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    alarm(atoi(argv[1]));\n"
msgstr "    alarm(atoi(argv[1]));\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "    /* Calculate relative interval as current time plus\n"
#| "       number of seconds given argv[2] */\n"
msgid ""
"    /* Calculate relative interval as current time plus\n"
"       number of seconds given argv[2]. */\n"
msgstr ""
"    /* вычисляем относительный интервал как текущее время плюс\n"
"       количество секунд из argv[2] */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"
msgstr ""
"    if (clock_gettime(CLOCK_REALTIME, &ts) == -1)\n"
"        handle_error(\"clock_gettime\");\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    ts.tv_sec += atoi(argv[2]);\n"
msgstr "    ts.tv_sec += atoi(argv[2]);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "    printf(\"main() about to call sem_timedwait()\\en\");\n"
#| "    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
#| "        continue;       /* Restart if interrupted by handler */\n"
msgid ""
"    printf(\"%s() about to call sem_timedwait()\\en\", __func__);\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* Restart if interrupted by handler. */\n"
msgstr ""
"    printf(\"в main() вызывается sem_timedwait()\\en\");\n"
"    while ((s = sem_timedwait(&sem, &ts)) == -1 && errno == EINTR)\n"
"        continue;       /* перезапускаем, если прервано обработчиком */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "    /* Check what happened */\n"
msgid "    /* Check what happened. */\n"
msgstr "    /* проверяем  что произошло */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"sem_timedwait() timed out\\en\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() succeeded\\en\");\n"
msgstr ""
"    if (s == -1) {\n"
"        if (errno == ETIMEDOUT)\n"
"            printf(\"истекло время ожидания sem_timedwait()\\en\");\n"
"        else\n"
"            perror(\"sem_timedwait\");\n"
"    } else\n"
"        printf(\"sem_timedwait() выполнена успешно\\en\");\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"
msgstr ""
"    exit((s == 0) ? EXIT_SUCCESS : EXIT_FAILURE);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 июля 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
