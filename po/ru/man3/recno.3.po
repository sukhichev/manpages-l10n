# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:05+0100\n"
"PO-Revision-Date: 2019-10-06 09:20+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "recno"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "recno - record number database access method"
msgstr "recno - метод доступа к базам нумерованных данных"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>db.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>db.hE<gt>>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Note well>: This page documents interfaces provided in glibc up until "
#| "version 2.1.  Since version 2.2, glibc no longer provides these "
#| "interfaces.  Probably, you are looking for the APIs provided by the "
#| "I<libdb> library instead."
msgid ""
"I<Note well>: This page documents interfaces provided up until glibc 2.1.  "
"Since glibc 2.2, glibc no longer provides these interfaces.  Probably, you "
"are looking for the APIs provided by the I<libdb> library instead."
msgstr ""
"I<Примечание>: В этой странице описаны интерфейсы, предоставляемые glibc до "
"версии 2.1. Начиная с версии 2.2, glibc больше не поддерживает эти "
"интерфейсы. Вероятно, вы ищите API, предоставляемое библиотекой I<libdb>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The routine B<dbopen>(3)  is the library interface to database files.  One "
"of the supported file formats is record number files.  The general "
"description of the database access methods is in B<dbopen>(3), this manual "
"page describes only the recno-specific information."
msgstr ""
"Функция B<dbopen>(3) — это библиотечный интерфейс к файлам баз данных. Один "
"из поддерживаемых форматов файлов — нумерованные данные. Общее описание "
"методов доступа к базам данных находится в B<dbopen>(3). Эта справочная "
"страница содержит только информацию, относящуюся к нумерованным данным."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The record number data structure is either variable or fixed-length records "
"stored in a flat-file format, accessed by the logical record number.  The "
"existence of record number five implies the existence of records one through "
"four, and the deletion of record number one causes record number five to be "
"renumbered to record number four, as well as the cursor, if positioned after "
"record number one, to shift down one record."
msgstr ""
"Структура нумерованных данных может описывать записи переменной или "
"фиксированной длины, хранящиеся в файле в «плоском» формате, и доступные по "
"логическому номеру записи. Существование записи с номером пять подразумевает "
"существование записей от одного до четырёх и удаление записи с номером один "
"вызывает перенумеровывание записи с номером пять в запись с номером четыре, "
"а также сдвиг курсора вниз на одну запись, если он указывает на запись после "
"номера один."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The recno access-method-specific data structure provided to B<dbopen>(3)  is "
"defined in the I<E<lt>db.hE<gt>> include file as follows:"
msgstr ""
"Специальная структура метода доступа к данным recno, предоставляемая "
"B<dbopen>(3), определена в I<E<lt>db.hE<gt>> следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"typedef struct {\n"
"    unsigned long flags;\n"
"    unsigned int  cachesize;\n"
"    unsigned int  psize;\n"
"    int           lorder;\n"
"    size_t        reclen;\n"
"    unsigned char bval;\n"
"    char         *bfname;\n"
"} RECNOINFO;\n"
msgstr ""
"typedef struct {\n"
"    unsigned long flags;\n"
"    unsigned int  cachesize;\n"
"    unsigned int  psize;\n"
"    int           lorder;\n"
"    size_t        reclen;\n"
"    unsigned char bval;\n"
"    char         *bfname;\n"
"} RECNOINFO;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "The elements of this structure are defined as follows:"
msgstr "Элементы этой структуры определены так:"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<flags>"
msgstr "I<flags>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "The flag value is specified by ORing any of the following values:"
msgstr "Значение флага определяется логическим ИЛИ следующих значений:"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<R_FIXEDLEN>"
msgstr "B<R_FIXEDLEN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The records are fixed-length, not byte delimited.  The structure element "
"I<reclen> specifies the length of the record, and the structure element "
"I<bval> is used as the pad character.  Any records, inserted into the "
"database, that are less than I<reclen> bytes long are automatically padded."
msgstr ""
"Записи фиксированной длины без разделительного байта. Полем I<reclen> "
"определяется длина записи, а поле I<bval> используется как дополняющий "
"символ. Все вставляемые в базу данных записи, размер которых меньше "
"I<reclen> байт, автоматически дополняются."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<R_NOKEY>"
msgstr "B<R_NOKEY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"In the interface specified by B<dbopen>(3), the sequential record retrieval "
"fills in both the caller's key and data structures.  If the B<R_NOKEY> flag "
"is specified, the I<cursor> routines are not required to fill in the key "
"structure.  This permits applications to retrieve records at the end of "
"files without reading all of the intervening records."
msgstr ""
"В интерфейсе, определённом B<dbopen>(3), последовательная выборка записей "
"заполняет структуры данных и ключа вызывающего. Если указан флаг B<R_NOKEY>, "
"то процедурам I<cursor> не обязательно заполнять структуру ключа. Это "
"позволяет приложениям извлекать записи из конца файлов без чтения всех "
"промежуточных записей."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<R_SNAPSHOT>"
msgstr "B<R_SNAPSHOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"This flag requires that a snapshot of the file be taken when B<dbopen>(3)  "
"is called, instead of permitting any unmodified records to be read from the "
"original file."
msgstr ""
"Этим флагом указывается, чтобы в момент вызова B<dbopen>(3) был сделан "
"снимок (snapshot) файла, а не выполнялось чтение каких-либо не изменённых "
"записей из оригинального файла."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<cachesize>"
msgstr "I<cachesize>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"A suggested maximum size, in bytes, of the memory cache.  This value is "
"B<only> advisory, and the access method will allocate more memory rather "
"than fail.  If I<cachesize> is 0 (no size is specified), a default cache is "
"used."
msgstr ""
"Предполагаемый максимальный размер кэша памяти в байтах. Эта величина "
"I<только рекомендация>, метод доступа скорее выделит больше памяти, чем "
"завершится с ошибкой. Если значение I<cachesize> равно 0 (размер не указан), "
"то используется кэш по умолчанию."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<psize>"
msgstr "I<psize>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The recno access method stores the in-memory copies of its records in a "
"btree.  This value is the size (in bytes) of the pages used for nodes in "
"that tree.  If I<psize> is 0 (no page size is specified), a page size is "
"chosen based on the underlying filesystem I/O block size.  See B<btree>(3)  "
"for more information."
msgstr ""
"Метод доступа recno хранит копии своих записей в памяти в виде дерева btree. "
"Это значение задаёт размер страниц (в байтах), используемых для узлов в этом "
"дереве. Если значение I<psize> равно 0 (размер страницы не указан), то "
"размер выбирается на основе размера блока ввода-вывода нижележащей файловой "
"системы. Подробней смотрите B<btree>(3)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<lorder>"
msgstr "I<lorder>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The byte order for integers in the stored database metadata.  The number "
"should represent the order as an integer; for example, big endian order "
"would be the number 4,321.  If I<lorder> is 0 (no order is specified), the "
"current host order is used."
msgstr ""
"Порядок расположения байтов, используемый при хранении целых чисел в "
"метаданных базы данных. Число должно отражать порядок размещения в виде "
"целого значения; например, порядок «big  endian» должен обозначаться числом "
"4321. Если I<lorder> равно 0 (порядок не определён), то используется "
"значение по умолчанию, принятое на машине."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<reclen>"
msgstr "I<reclen>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "The length of a fixed-length record."
msgstr "Длина записи фиксированной длины."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<bval>"
msgstr "I<bval>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The delimiting byte to be used to mark the end of a record for variable-"
"length records, and the pad character for fixed-length records.  If no value "
"is specified, newlines (\"\\en\") are used to mark the end of variable-"
"length records and fixed-length records are padded with spaces."
msgstr ""
"Разделительный байт, для использования в качестве отметки конца записи с "
"переменной длиной и как заполняющий символ для записи с фиксированной "
"длиной. Если значение не указано, то для отметки конца записи с переменной "
"длиной используется символ новой строки («\\en»), а записи с фиксированной "
"длиной дополняются пробелом."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<bfname>"
msgstr "I<bfname>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The recno access method stores the in-memory copies of its records in a "
"btree.  If I<bfname> is non-NULL, it specifies the name of the btree file, "
"as if specified as the filename for a B<dbopen>(3)  of a btree file."
msgstr ""
"Метод доступа recno хранит в памяти копии своих записей в дереве btree. Если "
"значение I<bfname> не равно NULL, то им задаётся имя btree-файла в том же "
"виде, что и при доступе B<dbopen>(3) для доступа к btree-файлу."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The data part of the key/data pair used by the I<recno> access method is the "
"same as other access methods.  The key is different.  The I<data> field of "
"the key should be a pointer to a memory location of type I<recno_t>, as "
"defined in the I<E<lt>db.hE<gt>> include file.  This type is normally the "
"largest unsigned integral type available to the implementation.  The I<size> "
"field of the key should be the size of that type."
msgstr ""
"Часть «данные» из пары ключ/данные, используемая методом доступа I<recno>, "
"является такой же, как в других методах доступа. Ключ используется иначе. "
"Как следует из определения заголовочного файла I<E<lt>db.hE<gt>>, поле ключа "
"I<data> должно быть указателем на расположение памяти типа I<recno_t>. Этот "
"тип, обычно, является наибольшим беззнаковым целочисленным типом данных, "
"применяемым в реализации. Значение поля ключа I<size> должен задавать размер "
"этого типа."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Because there can be no metadata associated with the underlying recno access "
"method files, any changes made to the default values (e.g., fixed record "
"length or byte separator value) must be explicitly specified each time the "
"file is opened."
msgstr ""
"Так как в файлах с данными в формате recno могут отсутствовать метаданные, "
"то любые изменения значений по умолчанию (например, длина фиксированной "
"записи или разделяющий байт) должны явно указываться каждый раз при открытии "
"файла."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"In the interface specified by B<dbopen>(3), using the I<put> interface to "
"create a new record will cause the creation of multiple, empty records if "
"the record number is more than one greater than the largest record currently "
"in the database."
msgstr ""
"В интерфейсе, определённым B<dbopen>(3), использование интерфейса I<put> для "
"создания новой записи повлечёт за собой создание множества пустых записей, "
"если номер записи больше, чем номер наибольшей записи, имеющейся в базе "
"данных."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The I<recno> access method routines may fail and set I<errno> for any of the "
"errors specified for the library routine B<dbopen>(3)  or the following:"
msgstr ""
"Функции метода доступа I<recno> могут завершиться с ошибкой и присвоить "
"I<errno> любое значение из определённых для библиотеки функций B<dbopen>(3), "
"а также:"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"An attempt was made to add a record to a fixed-length database that was too "
"large to fit."
msgstr ""
"Попытка добавить слишком большую запись в базу данных фиксированной длины."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Only big and little endian byte order is supported."
msgstr "Поддерживаются значения только с прямым и обратным порядком байт."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<btree>(3), B<dbopen>(3), B<hash>(3), B<mpool>(3)"
msgstr "B<btree>(3), B<dbopen>(3), B<hash>(3), B<mpool>(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"I<Document Processing in a Relational Database System>, Michael Stonebraker, "
"Heidi Stettner, Joseph Kalash, Antonin Guttman, Nadene Lynn, Memorandum No. "
"UCB/ERL M82/32, May 1982."
msgstr ""
"I<Document Processing in a Relational Database System>, Michael Stonebraker, "
"Heidi Stettner, Joseph Kalash, Antonin Guttman, Nadene Lynn, Memorandum No. "
"UCB/ERL M82/32, May 1982."

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
