# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexey, 2016.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014-2017.
# kogamatranslator49 <r.podarov@yandex.ru>, 2015.
# Kogan, Darima <silverdk99@gmail.com>, 2014.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:04+0100\n"
"PO-Revision-Date: 2019-10-12 08:58+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "pread"
msgstr "pread"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"pread, pwrite - read from or write to a file descriptor at a given offset"
msgstr ""
"pread, pwrite - чтение или запись информации из файлового дескриптора "
"согласно заданному смещению"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<ssize_t pread(int >I<fd>B<, void *>I<buf>B<, size_t >I<count>B<, off_t >I<offset>B<);>\n"
#| "B<ssize_t pwrite(int >I<fd>B<, const void *>I<buf>B<, size_t >I<count>B<, off_t >I<offset>B<);>\n"
msgid ""
"B<ssize_t pread(int >I<fd>B<, void >I<buf>B<[.>I<count>B<], size_t >I<count>B<,>\n"
"B<              off_t >I<offset>B<);>\n"
"B<ssize_t pwrite(int >I<fd>B<, const void >I<buf>B<[.>I<count>B<], size_t >I<count>B<,>\n"
"B<              off_t >I<offset>B<);>\n"
msgstr ""
"B<ssize_t pread(int >I<fd>B<, void *>I<buf>B<, size_t >I<count>B<, off_t >I<offset>B<);>\n"
"B<ssize_t pwrite(int >I<fd>B<, const void *>I<buf>B<, size_t >I<count>B<, off_t >I<offset>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<pread>(), B<pwrite>():"
msgstr "B<pread>(), B<pwrite>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* начиная с glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pread>()  reads up to I<count> bytes from file descriptor I<fd> at offset "
"I<offset> (from the start of the file) into the buffer starting at I<buf>.  "
"The file offset is not changed."
msgstr ""
"B<pread>() читает максимум I<count> байтов из файлового дескриптора I<fd>, "
"начиная со смещения I<offset> (от начала файла), в буфер, начиная с I<buf>. "
"Текущая позиция файла не изменяется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pwrite>()  writes up to I<count> bytes from the buffer starting at I<buf> "
"to the file descriptor I<fd> at offset I<offset>.  The file offset is not "
"changed."
msgstr ""
"B<pwrite>() записывает максимум I<count> байтов из буфера I<buf> в файловый "
"дескриптор I<fd> , начиная со смещения I<offset>. Текущая позиция файла не "
"изменяется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file referenced by I<fd> must be capable of seeking."
msgstr "Файл, заданный в I<fd>, должен позволять изменение смещения."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<pread>()  returns the number of bytes read (a return of zero "
"indicates end of file)  and B<pwrite>()  returns the number of bytes written."
msgstr ""
"При успешном выполнении B<pread>() возвращается количество считанных байт "
"(ноль указывает на конец файла), а B<pwrite>() — количество записанных байт."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that it is not an error for a successful call to transfer fewer bytes "
"than requested (see B<read>(2)  and B<write>(2))."
msgstr ""
"Заметим, что для успешного выполнения не считается ошибкой передача меньшего "
"количества байт чем запрошено (смотрите B<read>(2) и B<write>(2))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On error, -1 is returned and I<errno> is set to indicate the error."
msgstr ""
"В случае ошибки возвращается -1, а I<errno> устанавливается в значение "
"ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pread>()  can fail and set I<errno> to any error specified for B<read>(2)  "
"or B<lseek>(2).  B<pwrite>()  can fail and set I<errno> to any error "
"specified for B<write>(2)  or B<lseek>(2)."
msgstr ""
"Вызов B<pread>() может завершиться неудачно и записать в I<errno> один из "
"кодов ошибки, определённых для B<read>(2) или B<lseek>(2). Вызов B<pwrite>() "
"может завершиться неудачно и записать в I<errno> один из кодов ошибки, "
"определённых для B<write>(2) или B<lseek>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<pread>()  and B<pwrite>()  system calls were added to Linux in "
#| "version 2.1.60; the entries in the i386 system call table were added in "
#| "2.1.69.  C library support (including emulation using B<lseek>(2)  on "
#| "older kernels without the system calls) was added in glibc 2.1."
msgid ""
"Added in Linux 2.1.60; the entries in the i386 system call table were added "
"in Linux 2.1.69.  C library support (including emulation using B<lseek>(2)  "
"on older kernels without the system calls) was added in glibc 2.1."
msgstr ""
"Системные вызовы B<pread>() и B<pwrite>() были внесены в ядро Linux, начиная "
"с версии 2.1.60; как элементы таблицы системных вызовов i386 были добавлены "
"в ядро версии 2.1.69. Поддержка в библиотеке С (включая эмуляцию с помощью "
"B<lseek>(2) в старых ядрах, не имеющих соответствующих системных вызовов) "
"была добавлена в glibc 2.1."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr "Отличия между библиотекой C и ядром"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On Linux, the underlying system calls were renamed in kernel 2.6: "
#| "B<pread>()  became B<pread64>(), and B<pwrite>()  became B<pwrite64>().  "
#| "The system call numbers remained the same.  The glibc B<pread>()  and "
#| "B<pwrite>()  wrapper functions transparently deal with the change."
msgid ""
"On Linux, the underlying system calls were renamed in Linux 2.6: B<pread>()  "
"became B<pread64>(), and B<pwrite>()  became B<pwrite64>().  The system call "
"numbers remained the same.  The glibc B<pread>()  and B<pwrite>()  wrapper "
"functions transparently deal with the change."
msgstr ""
"В Linux нижележащие системные вызовы переименованы в ядре версии 2.6: "
"B<pread>() стал называться B<pread64>(), а B<pwrite>() \\(em B<pwrite64>(). "
"Номера системных вызовов остались прежними. Обёрточные функции B<pread>() и "
"B<pwrite>() в glibc скрывают данные переименование."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On some 32-bit architectures, the calling signature for these system calls "
"differ, for the reasons described in B<syscall>(2)."
msgstr ""
"На некоторых 32-битных архитектурах интерфейс этих системных вызовов "
"отличается от описанного выше по причинам, указанным в B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pread>()  and B<pwrite>()  system calls are especially useful in "
"multithreaded applications.  They allow multiple threads to perform I/O on "
"the same file descriptor without being affected by changes to the file "
"offset by other threads."
msgstr ""
"Системные вызовы B<pread>() и B<pwrite>()  особенно полезны в многонитевых "
"приложениях. Они позволяют нескольким нитям выполнять ввод-вывод в один "
"файловый дескриптор не учитывая изменений файлового смещения, сделанного "
"другими нитями."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#.  FIXME . https://bugzilla.kernel.org/show_bug.cgi?id=43178
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX requires that opening a file with the B<O_APPEND> flag should have no "
"effect on the location at which B<pwrite>()  writes data.  However, on "
"Linux, if a file is opened with B<O_APPEND>, B<pwrite>()  appends data to "
"the end of the file, regardless of the value of I<offset>."
msgstr ""
"Согласно POSIX требуется, чтобы открытие файла с флагом B<O_APPEND> не "
"влияло на расположение, по которому B<pwrite>() записывает данные. Однако в "
"Linux, если файл открывается с флагом B<O_APPEND>, B<pwrite>() добавляет "
"данные в конец файла, независимо от значения I<offset>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<lseek>(2), B<read>(2), B<readv>(2), B<write>(2)"
msgstr "B<lseek>(2), B<read>(2), B<readv>(2), B<write>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "The B<pread>()  and B<pwrite>()  system calls were added to Linux in "
#| "version 2.1.60; the entries in the i386 system call table were added in "
#| "2.1.69.  C library support (including emulation using B<lseek>(2)  on "
#| "older kernels without the system calls) was added in glibc 2.1."
msgid ""
"The B<pread>()  and B<pwrite>()  system calls were added in Linux 2.1.60; "
"the entries in the i386 system call table were added in Linux 2.1.69.  C "
"library support (including emulation using B<lseek>(2)  on older kernels "
"without the system calls) was added in glibc 2.1."
msgstr ""
"Системные вызовы B<pread>() и B<pwrite>() были внесены в ядро Linux, начиная "
"с версии 2.1.60; как элементы таблицы системных вызовов i386 были добавлены "
"в ядро версии 2.1.69. Поддержка в библиотеке С (включая эмуляцию с помощью "
"B<lseek>(2) в старых ядрах, не имеющих соответствующих системных вызовов) "
"была добавлена в glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
