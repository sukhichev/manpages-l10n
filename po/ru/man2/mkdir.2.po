# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Alexey <a.chepugov@gmail.com>, 2015.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# ITriskTI <ITriskTI@gmail.com>, 2013.
# Max Is <ismax799@gmail.com>, 2016.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
# Малянов Евгений Викторович <maljanow@outlook.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:01+0100\n"
"PO-Revision-Date: 2019-10-06 08:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mkdir"
msgstr "mkdir"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mkdir, mkdirat - create a directory"
msgstr "mkdir, mkdirat - создаёт каталог"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#.  .B #include <unistd.h>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr "B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int mkdir(const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"
msgstr "B<int mkdir(const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* определения констант AT_* */\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int mkdirat(int >I<dirfd>B<, const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"
msgstr "B<int mkdirat(int >I<dirfd>B<, const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mkdirat>():"
msgstr "B<mkdirat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""
"    Начиная с glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    До glibc 2.10:\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mkdir>()  attempts to create a directory named I<pathname>."
msgstr "Функция B<mkdir>() пытается создать каталог с именем I<pathname>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The argument I<mode> specifies the mode for the new directory (see "
#| "B<inode>(7)).  It is modified by the process's I<umask> in the usual way: "
#| "in the absence of a default ACL, the mode of the created directory is "
#| "(I<mode> & \\(tiI<umask> & 0777).  Whether other I<mode> bits are honored "
#| "for the created directory depends on the operating system.  For Linux, "
#| "see NOTES below."
msgid ""
"The argument I<mode> specifies the mode for the new directory (see "
"B<inode>(7)).  It is modified by the process's I<umask> in the usual way: in "
"the absence of a default ACL, the mode of the created directory is (I<mode> "
"& \\[ti]I<umask> & 0777).  Whether other I<mode> bits are honored for the "
"created directory depends on the operating system.  For Linux, see NOTES "
"below."
msgstr ""
"В аргументе I<mode> задаются права доступа к новому каталогу (смотрите "
"B<inode>(7)). Эти права стандартным образом изменяются с помощью согласно "
"I<umask> процесса: при отсутствии списка доступа по умолчанию права на "
"созданный каталог будут рассчитаны как (I<mode> & \\(tiI<umask> & 0777). "
"Другие биты I<mode> на создаваемый каталог зависят от операционной системы. "
"Для Linux они описаны в ЗАМЕЧАНИЯХ."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The newly created directory will be owned by the effective user ID of the "
"process.  If the directory containing the file has the set-group-ID bit set, "
"or if the filesystem is mounted with BSD group semantics (I<mount -o "
"bsdgroups> or, synonymously I<mount -o grpid>), the new directory will "
"inherit the group ownership from its parent; otherwise it will be owned by "
"the effective group ID of the process."
msgstr ""
"Создаваемый каталог будет принадлежать фактическому владельцу процесса. Если "
"у родительского каталога установлен флаг set-group-ID, или файловая система "
"смонтирована с семантикой групп в стиле BSD (I<mount -o bsdgroups> или, что "
"одно и тоже, I<mount -o grpid>), то новый каталог унаследует группу "
"владельца от своего родительского каталога; в противном случае группой "
"владельцем станет фактическая группа процесса."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the parent directory has the set-group-ID bit set, then so will the newly "
"created directory."
msgstr ""
"Если у родительского каталога установлен бит set-group-ID, то он будет "
"установлен также и у создаваемого каталога."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mkdirat()"
msgstr "mkdirat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<mkdirat>()  system call operates in exactly the same way as "
"B<mkdir>(), except for the differences described here."
msgstr ""
"Системный вызов B<mkdirat>() работает также как системный вызов B<mkdir>(), "
"за исключением случаев, описанных здесь."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<mkdir>()  for a relative pathname)."
msgstr ""
"Если в I<pathname> задан относительный путь, то он считается относительно "
"каталога, на который ссылается файловый дескриптор I<dirfd> (а не "
"относительно текущего рабочего каталога вызывающего процесса, как это "
"делается в B<mkdir>())."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<pathname> is relative and I<dirfd> is the special value B<AT_FDCWD>, "
"then I<pathname> is interpreted relative to the current working directory of "
"the calling process (like B<mkdir>())."
msgstr ""
"Если в I<pathname> задан относительный путь и I<dirfd> равно специальному "
"значению B<AT_FDCWD>, то I<pathname> рассматривается относительно текущего "
"рабочего каталога вызывающего процесса (как B<mkdir>())."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<pathname> is absolute, then I<dirfd> is ignored."
msgstr "Если в I<pathname> задан абсолютный путь, то I<dirfd> игнорируется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<mkdirat>()."
msgstr "Смотрите в B<openat>(2) объяснение необходимости B<mkdirat>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, B<mbind>()  returns 0; on error, -1 is returned and I<errno> "
#| "is set to indicate the error."
msgid ""
"B<mkdir>()  and B<mkdirat>()  return zero on success.  On error, -1 is "
"returned and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении B<mbind>() возвращается 0. При ошибке возвращается "
"-1, а в I<errno> содержится код ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The parent directory does not allow write permission to the process, or one "
"of the directories in I<pathname> did not allow search permission.  (See "
"also B<path_resolution>(7).)"
msgstr ""
"У процесса нет прав на запись в родительский каталог, или в одном из "
"каталогов в I<pathname> не разрешён поиск (смотрите также "
"B<path_resolution>(7))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<mkdirat>())  I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> "
"nor a valid file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The user's quota of disk blocks or inodes on the filesystem has been "
"exhausted."
msgstr ""
"Исчерпана пользовательская квота на дисковые блоки или иноды файловой "
"системы."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<pathname> already exists (not necessarily as a directory).  This includes "
"the case where I<pathname> is a symbolic link, dangling or not."
msgstr ""
"I<pathname> уже существует (и необязательно как каталог). В этом случае "
"I<pathname> может быть символьной ссылкой, повисшей или нет."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> points outside your accessible address space."
msgstr ""
"Аргумент I<pathname> указывает за пределы доступного адресного пространства."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The final component (\"basename\") of the new directory's I<pathname> is "
"invalid (e.g., it contains characters not permitted by the underlying "
"filesystem)."
msgstr ""
"Последний компонент («основная часть» (basename)) нового каталога "
"I<pathname> некорректен (например, содержит недопустимые в нижележащей "
"файловой системе символы)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in resolving I<pathname>."
msgstr ""
"Во время определения I<pathname> встретилось слишком много символьных ссылок."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EMLINK>"
msgstr "B<EMLINK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The number of links to the parent directory would exceed B<LINK_MAX>."
msgstr "Количество ссылок на родительский каталог превысило бы B<LINK_MAX>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> was too long."
msgstr "I<pathname> слишком длинен."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A directory component in I<pathname> does not exist or is a dangling "
"symbolic link."
msgstr ""
"Один из каталогов в I<pathname> не существует или является повисшей "
"символьной ссылкой."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Недостаточное количество памяти ядра."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The device containing I<pathname> has no room for the new directory."
msgstr ""
"На устройстве, содержащем I<pathname>, нет места для создания нового "
"каталога."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The new directory cannot be created because the user's disk quota is "
"exhausted."
msgstr ""
"Новый каталог не может быть создан, так как превышена пользовательская "
"дисковая квота."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<pathname> is not, in fact, a directory."
msgstr ""
"Компонент пути, использованный как каталог в I<pathname>, в действительности "
"таковым не является."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<mkdirat>())  I<pathname> is relative and I<dirfd> is a file descriptor "
"referring to a file other than a directory."
msgstr ""
"(B<mkdirat>())  Значение I<pathname> содержит относительный путь и I<dirfd> "
"содержит файловый дескриптор, указывающий на файл, а не на каталог."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The filesystem containing I<pathname> does not support the creation of "
"directories."
msgstr ""
"Файловая система, содержащая I<pathname>, не поддерживает создание каталогов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> refers to a file on a read-only filesystem."
msgstr ""
"I<pathname> указывает на файл в файловой системе, доступной только для "
"чтения."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Under Linux, apart from the permission bits, the B<S_ISVTX> I<mode> bit is "
"also honored."
msgstr "В Linux кроме битов прав, в I<mode> учитывается также бит B<S_ISVTX>."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "glibc notes"
msgstr "Замечания по glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On older kernels where B<mkdirat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<mkdir>().  When I<pathname> is a "
"relative pathname, glibc constructs a pathname based on the symbolic link in "
"I</proc/self/fd> that corresponds to the I<dirfd> argument."
msgstr ""
"В старых ядрах, где B<mkdirat>() отсутствует, обёрточная функция glibc "
"использует B<mkdir>(). Если I<pathname> является относительным путём, то "
"glibc собирает путь относительно символической ссылки в I</proc/self/fd>, "
"которая соответствует аргументу I<dirfd>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<mkdir>(2)"
msgid "B<mkdir>()"
msgstr "B<mkdir>(2)"

#.  SVr4 documents additional EIO, EMULTIHOP
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgid "SVr4, BSD, POSIX.1-2001."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<mkdirat>():"
msgid "B<mkdirat>()"
msgstr "B<mkdirat>():"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 2.6.16, glibc 2.4."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"There are many infelicities in the protocol underlying NFS.  Some of these "
"affect B<mkdir>()."
msgstr ""
"В протоколе, на котором работает NFS, есть множество недоработок. Некоторые "
"из них влияют на B<mkdir>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mkdir>(1), B<chmod>(2), B<chown>(2), B<mknod>(2), B<mount>(2), "
"B<rmdir>(2), B<stat>(2), B<umask>(2), B<unlink>(2), B<acl>(5), "
"B<path_resolution>(7)"
msgstr ""
"B<mkdir>(1), B<chmod>(2), B<chown>(2), B<mknod>(2), B<mount>(2), "
"B<rmdir>(2), B<stat>(2), B<umask>(2), B<unlink>(2), B<acl>(5), "
"B<path_resolution>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "B<mkdirat>()  was added to Linux in kernel 2.6.16; library support was "
#| "added to glibc in version 2.4."
msgid ""
"B<mkdirat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""
"Вызов B<mkdirat>() был добавлен в ядро Linux версии 2.6.16; поддержка в "
"glibc доступна с версии 2.4."

#.  SVr4 documents additional EIO, EMULTIHOP
#. type: Plain text
#: debian-bookworm
msgid "B<mkdir>(): SVr4, BSD, POSIX.1-2001, POSIX.1-2008."
msgstr "B<mkdir>(): SVr4, BSD, POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid "B<mkdirat>(): POSIX.1-2008."
msgstr "B<mkdirat>(): POSIX.1-2008."

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
