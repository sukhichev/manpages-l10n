# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2014.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:54+0100\n"
"PO-Revision-Date: 2019-10-05 07:56+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "copy_file_range"
msgstr "copy_file_range"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "copy_file_range - Copy a range of data from one file to another"
msgstr "copy_file_range - копирует часть данных из одного файла в другой"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#define _GNU_SOURCE>\n"
#| "B<#include E<lt>unistd.hE<gt>>\n"
msgid ""
"B<#define _GNU_SOURCE>\n"
"B<#define _FILE_OFFSET_BITS 64>\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<ssize_t copy_file_range(int >I<fd_in>B<, off64_t *>I<off_in>B<,>\n"
#| "B<                        int >I<fd_out>B<, off64_t *>I<off_out>B<,>\n"
#| "B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"
msgid ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off_t *_Nullable >I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off_t *_Nullable >I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"
msgstr ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off64_t *>I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off64_t *>I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<copy_file_range>()  system call performs an in-kernel copy between two "
"file descriptors without the additional cost of transferring data from the "
"kernel to user space and then back into the kernel.  It copies up to I<len> "
"bytes of data from the source file descriptor I<fd_in> to the target file "
"descriptor I<fd_out>, overwriting any data that exists within the requested "
"range of the target file."
msgstr ""
"Системный вызов B<copy_file_range>()  выполняет внутриядерное копирование "
"между двумя файловыми дескрипторами без дополнительных накладных расходов по "
"передаче данных из ядра в пользовательское пространство и затем обратно в "
"ядро. Он копирует до I<len> байт данных из файлового дескриптора I<fd_in> "
"источника в файловый дескриптор I<fd_out> приёмника, перезаписывая "
"существующие данные внутри запрашиваемой области файла назначения."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following semantics apply for I<off_in>, and similar statements apply to "
"I<off_out>:"
msgstr ""
"Следующая семантика применяется к I<off_in> и подобная ей к I<off_out>:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<off_in> is NULL, then bytes are read from I<fd_in> starting from the "
"file offset, and the file offset is adjusted by the number of bytes copied."
msgstr ""
"Если I<off_in> равно NULL, то байты читаются из I<fd_in> начиная с файлового "
"смещения, а файловое смещение корректируется на количество скопированных "
"байт."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<off_in> is not NULL, then I<off_in> must point to a buffer that "
"specifies the starting offset where bytes from I<fd_in> will be read.  The "
"file offset of I<fd_in> is not changed, but I<off_in> is adjusted "
"appropriately."
msgstr ""
"Если I<off_in> не равно NULL, то I<off_in> должно указывать на буфер, "
"задающий начальное смещение в I<fd_in>, из которого будут читаться байты. "
"Файловое смещение I<fd_in> не изменяется, но I<off_in> изменяется "
"соответствующим образом."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd_in> and I<fd_out> can refer to the same file.  If they refer to the "
"same file, then the source and target ranges are not allowed to overlap."
msgstr ""
"Значения I<fd_in> и I<fd_out> могут ссылаться на один и тот же файл. Если "
"это так, то диапазонам источника и приёмника нельзя перекрываться."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<flags> argument is provided to allow for future extensions and "
"currently must be set to 0."
msgstr ""
"Аргумент I<flags> предназначен для будущих расширений, а пока его значение "
"должно быть равно 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Upon successful completion, B<copy_file_range>()  will return the number of "
"bytes copied between files.  This could be less than the length originally "
"requested.  If the file offset of I<fd_in> is at or past the end of file, no "
"bytes are copied, and B<copy_file_range>()  returns zero."
msgstr ""
"При успешном выполнении B<copy_file_range>() возвращает количество "
"скопированных между файлами байт. Оно может быть меньше запрашиваемой длины. "
"Если файловое смещение I<fd_in> в конце или за концом файла, то байты не "
"копирются и B<copy_file_range>() возвращает ноль."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On error, B<copy_file_range>()  returns -1 and I<errno> is set to indicate "
"the error."
msgstr ""
"В случае ошибки B<copy_file_range>() возвращает -1, а I<errno> "
"устанавливается в соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "One or more file descriptors are not valid."
msgstr "Один или оба файловых дескриптора недействительны."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd_in> is not open for reading; or I<fd_out> is not open for writing."
msgstr ""
"Дескриптор I<fd_in> не открыт на чтение или дескриптор I<fd_out> не открыт "
"на запись."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<O_APPEND> flag is set for the open file description (see B<open>(2))  "
"referred to by the file descriptor I<fd_out>."
msgstr ""
"В открытом файловом описании, на которое ссылается файловый дескриптор "
"I<fd_out>, установлен флаг B<O_APPEND> (смотрите B<open>(2))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFBIG>"
msgstr "B<EFBIG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An attempt was made to write at a position past the maximum file offset the "
"kernel supports."
msgstr ""
"Попытка записать в позицию вне максимально поддерживаемого ядром файлового "
"смещения."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An attempt was made to write a range that exceeds the allowed maximum file "
"size.  The maximum file size differs between filesystem implementations and "
"can be different from the maximum allowed file offset."
msgstr ""
"Попытка записи диапазона, который превышает разрешённый максимальный размер "
"файла. Максимальный размер файла различается в реализациях файловых систем и "
"может отличаться от разрешённого максимального файлового смещения."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An attempt was made to write beyond the process's file size resource limit.  "
"This may also result in the process receiving a B<SIGXFSZ> signal."
msgstr ""
"Попытка записи, выходящее за ограничение ресурса процесса на размер файла. "
"Также это может вызвать получение процессом сигнала B<SIGXFSZ>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The I<flags> argument is not 0."
msgstr "Аргумент I<flags> не равен 0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd_in> and I<fd_out> refer to the same file and the source and target "
"ranges overlap."
msgstr ""
"Значения I<fd_in> и I<fd_out> ссылаются на один и тот же файл и диапазоны "
"источника и приёмника перекрываются."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Either I<fd_in> or I<fd_out> is not a regular file."
msgstr "Значение I<fd_in> или I<fd_out> указывает на необычный файл."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A low-level I/O error occurred while copying."
msgstr "Во время копирования возникла низкоуровневая ошибка ввода-вывода."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Either I<fd_in> or I<fd_out> refers to a directory."
msgstr "Значение I<fd_in> или I<fd_out> указывает на каталог."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Out of memory."
msgstr "Не хватает памяти."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"There is not enough space on the target filesystem to complete the copy."
msgstr ""
"Недостаточно места на файловой системе назначения для завершения копирования."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<EOPNOTSUPP> (since Linux 5.12)"
msgid "B<EOPNOTSUPP> (since Linux 5.19)"
msgstr "B<EOPNOTSUPP> (начиная с Linux 5.12)"

#.  commit 868f9f2f8e004bfe0d3935b1976f625b2924893b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "The filesystem does not support this call."
msgid "The filesystem does not support this operation."
msgstr "Файловая система не поддерживает данный вызов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EOVERFLOW>"
msgstr "B<EOVERFLOW>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The requested source or destination range is too large to represent in the "
"specified data types."
msgstr ""
"Запрошенный диапазон источника и приёмника слишком большой для представления "
"в указанном типе данных."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd_out> refers to an immutable file."
msgstr ""
"Значение I<fd_out> ссылается на файл с постоянными данными (immutable)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ETXTBSY>"
msgstr "B<ETXTBSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Either I<fd_in> or I<fd_out> refers to an active swap file."
msgstr "Значение I<fd_in> или I<fd_out> указывает на активный файл подкачки."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<EINVAL> (since Linux 4.3)"
msgid "B<EXDEV> (before Linux 5.3)"
msgstr "B<EINVAL> (начиная с Linux 4.3)"

#.  commit 5dae222a5ff0c269730393018a5539cc970a4726
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The files referred to by I<file_in> and I<file_out> are not on the same "
#| "mounted filesystem (pre Linux 5.3)."
msgid ""
"The files referred to by I<fd_in> and I<fd_out> are not on the same "
"filesystem."
msgstr ""
"Файлы, на которые ссылаются I<file_in> и I<file_out>, находятся не в одной "
"смонтированной файловой системе (до Linux 5.3)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<EXDEV> (since Linux 5.12)"
msgid "B<EXDEV> (since Linux 5.19)"
msgstr "B<EXDEV> (начиная с Linux 5.12)"

#.  commit 868f9f2f8e004bfe0d3935b1976f625b2924893b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The files referred to by I<file_in> and I<file_out> are not on the same "
#| "mounted filesystem (pre Linux 5.3)."
msgid ""
"The files referred to by I<fd_in> and I<fd_out> are not on the same "
"filesystem, and the source and target filesystems are not of the same type, "
"or do not support cross-filesystem copy."
msgstr ""
"Файлы, на которые ссылаются I<file_in> и I<file_out>, находятся не в одной "
"смонтированной файловой системе (до Linux 5.3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "A major rework of the kernel implementation occurred in 5.3.  Areas of "
#| "the API that weren't clearly defined were clarified and the API bounds "
#| "are much more strictly checked than on earlier kernels.  Applications "
#| "should target the behaviour and requirements of 5.3 kernels."
msgid ""
"A major rework of the kernel implementation occurred in Linux 5.3.  Areas of "
"the API that weren't clearly defined were clarified and the API bounds are "
"much more strictly checked than on earlier kernels."
msgstr ""
"В ядре версии 5.3 произошли значительные перемены. Уточнены области API, "
"которые были нечётно определены, и границы API теперь строже проверяются, по "
"сравнению с ранними ядрами. Приложения должны следовать поведению и "
"требованиям ядер 5.3."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 5.19, cross-filesystem copies can be achieved when both "
"filesystems are of the same type, and that filesystem implements support for "
"it.  See BUGS for behavior prior to Linux 5.19."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Applications should target the behaviour and requirements of Linux 5.19, "
"that was also backported to earlier stable kernels."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Linux"
msgid "Linux, GNU."
msgstr "Linux"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#.  https://sourceware.org/git/?p=glibc.git;a=commit;f=posix/unistd.h;h=bad7a0c81f501fbbcc79af9eaa4b8254441c4a1f
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<copy_file_range>()  system call first appeared in Linux 4.5, but "
#| "glibc 2.27 provides a user-space emulation when it is not available."
msgid ""
"Linux 4.5, but glibc 2.27 provides a user-space emulation when it is not "
"available."
msgstr ""
"Системный вызов B<copy_file_range>() впервые появился в Linux 4.5, но если "
"он недоступен, в glibc 2.27 предоставляется эмуляция в пользовательском "
"пространстве."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<fd_in> is a sparse file, then B<copy_file_range>()  may expand any "
"holes existing in the requested range.  Users may benefit from calling "
"B<copy_file_range>()  in a loop, and using the B<lseek>(2)  B<SEEK_DATA> and "
"B<SEEK_HOLE> operations to find the locations of data segments."
msgstr ""
"Если файл I<fd_in> является разреженным (sparse), то B<copy_file_range>() "
"может расширить дыры, существующие в запрашиваемой области. Пользователи "
"могут получить преимущество от вызова B<copy_file_range>() в цикле, и "
"используя операции B<lseek>(2) B<SEEK_DATA> и B<SEEK_HOLE> для поиска "
"расположений сегментов данных."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<copy_file_range>()  gives filesystems an opportunity to implement \"copy "
"acceleration\" techniques, such as the use of reflinks (i.e., two or more "
"inodes that share pointers to the same copy-on-write disk blocks)  or server-"
"side-copy (in the case of NFS)."
msgstr ""
"Вызов B<copy_file_range>() даёт файловым системам возможность реализовать "
"«ускорение копирования», например, использовать ссылочные связи (т. е., две "
"или более инод, использующих общие указатели для одного копирования-при-"
"записи дисковых блоков) или копирование-на-сервере (server-side-copy, в "
"случае использования NFS)."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<_FILE_OFFSET_BITS> should be defined to be 64 in code that uses non-null "
"I<off_in> or I<off_out> or that takes the address of B<copy_file_range>, if "
"the code is intended to be portable to traditional 32-bit x86 and ARM "
"platforms where B<off_t>'s width defaults to 32 bits."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In Linux 5.3 to Linux 5.18, cross-filesystem copies were implemented by the "
"kernel, if the operation was not supported by individual filesystems.  "
"However, on some virtual filesystems, the call failed to copy, while still "
"reporting success."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#define _FILE_OFFSET_BITS 64\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off_t        len, ret;\n"
"    struct stat  stat;\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    len = stat.st_size;\n"
"\\&\n"
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"
"\\&\n"
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<lseek>(2), B<sendfile>(2), B<splice>(2)"
msgstr "B<lseek>(2), B<sendfile>(2), B<splice>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid ""
#| "B<ssize_t copy_file_range(int >I<fd_in>B<, off64_t *>I<off_in>B<,>\n"
#| "B<                        int >I<fd_out>B<, off64_t *>I<off_out>B<,>\n"
#| "B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"
msgid ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off64_t *_Nullable >I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off64_t *_Nullable >I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"
msgstr ""
"B<ssize_t copy_file_range(int >I<fd_in>B<, off64_t *>I<off_in>B<,>\n"
"B<                        int >I<fd_out>B<, off64_t *>I<off_out>B<,>\n"
"B<                        size_t >I<len>B<, unsigned int >I<flags>B<);>\n"

#.  https://sourceware.org/git/?p=glibc.git;a=commit;f=posix/unistd.h;h=bad7a0c81f501fbbcc79af9eaa4b8254441c4a1f
#. type: Plain text
#: debian-bookworm
msgid ""
"The B<copy_file_range>()  system call first appeared in Linux 4.5, but glibc "
"2.27 provides a user-space emulation when it is not available."
msgstr ""
"Системный вызов B<copy_file_range>() впервые появился в Linux 4.5, но если "
"он недоступен, в glibc 2.27 предоставляется эмуляция в пользовательском "
"пространстве."

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<copy_file_range>()  system call is a nonstandard Linux and GNU "
"extension."
msgstr ""
"Системный вызов B<copy_file_range>() является нестандартным расширением "
"Linux и GNU."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/stat.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off64_t      len, ret;\n"
"    struct stat  stat;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int          fd_in, fd_out;\n"
"    off64_t      len, ret;\n"
"    struct stat  stat;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>sourceE<gt> E<lt>destinationE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Использование: %s E<lt>источникE<gt> E<lt>приёмникE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"open (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    fd_in = open(argv[1], O_RDONLY);\n"
"    if (fd_in == -1) {\n"
"        perror(\"открытие (argv[1])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (fstat(fd_in, &stat) == -1) {\n"
"        perror(\"fstat\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    len = stat.st_size;\n"
msgstr "    len = stat.st_size;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"open (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    fd_out = open(argv[2], O_CREAT | O_WRONLY | O_TRUNC, 0644);\n"
"    if (fd_out == -1) {\n"
"        perror(\"открытие (argv[2])\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
msgstr ""
"    do {\n"
"        ret = copy_file_range(fd_in, NULL, fd_out, NULL, len, 0);\n"
"        if (ret == -1) {\n"
"            perror(\"copy_file_range\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"
msgstr ""
"        len -= ret;\n"
"    } while (len E<gt> 0 && ret E<gt> 0);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    close(fd_in);\n"
"    close(fd_out);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-15"
msgstr "15 июля 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
