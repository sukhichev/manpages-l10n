# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:59+0100\n"
"PO-Revision-Date: 2019-10-05 08:17+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ioctl"
msgstr "ioctl"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ioctl - control device"
msgstr "ioctl - управляет устройством"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/ioctl.hE<gt>>\n"
msgstr "B<#include E<lt>sys/ioctl.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\\f[R]  /* glibc, BSD */\\fR\n"
"B<int ioctl(int >I<fd>B<, int >I<request>B<, ...);>\\f[R]            /* musl, other UNIX */\\fR\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<ioctl>()  system call manipulates the underlying device parameters of "
"special files.  In particular, many operating characteristics of character "
"special files (e.g., terminals) may be controlled with B<ioctl>()  "
"requests.  The argument I<fd> must be an open file descriptor."
msgstr ""
"Системный вызов B<ioctl>() изменяет параметры нижележащего устройства "
"специальных файлов. В частности, через запросы B<ioctl>() можно управлять "
"многими оперативными характеристиками специальных символьных файлов "
"(например, терминалов). В качестве аргумента I<fd> должен быть указан "
"открытый файловый дескриптор."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The second argument is a device-dependent request code.  The third argument "
"is an untyped pointer to memory.  It's traditionally B<char *>I<argp> (from "
"the days before B<void *> was valid C), and will be so named for this "
"discussion."
msgstr ""
"Второй аргумент является кодом запроса, значение которого зависит от "
"устройства. Третий аргумент является нетипизированным указателем на память. "
"Обычно, это B<char *>I<argp> (было до тех пор, пока в C не появился vB<void "
"*>) и далее он будет называться именно так."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "An B<ioctl>()  I<request> has encoded in it whether the argument is an "
#| "I<in> parameter or I<out> parameter, and the size of the argument I<argp> "
#| "in bytes.  Macros and defines used in specifying an B<ioctl>()  "
#| "I<request> are located in the file I<E<lt>sys/ioctl.hE<gt>>."
msgid ""
"An B<ioctl>()  I<request> has encoded in it whether the argument is an I<in> "
"parameter or I<out> parameter, and the size of the argument I<argp> in "
"bytes.  Macros and defines used in specifying an B<ioctl>()  I<request> are "
"located in the file I<E<lt>sys/ioctl.hE<gt>>.  See NOTES."
msgstr ""
"В значении I<request> функции B<ioctl>() кодируется информация является ли "
"параметр I<входным> или I<выходным> и размер аргумента I<argp> в байтах. "
"Макросы и определения, используемые при указании в B<ioctl>() запросах "
"I<request>, определены в файле I<E<lt>sys/ioctl.hE<gt>>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Usually, on success zero is returned.  A few B<ioctl>()  requests use the "
#| "return value as an output parameter and return a nonnegative value on "
#| "success.  On error, -1 is returned, and I<errno> is set appropriately."
msgid ""
"Usually, on success zero is returned.  A few B<ioctl>()  requests use the "
"return value as an output parameter and return a nonnegative value on "
"success.  On error, -1 is returned, and I<errno> is set to indicate the "
"error."
msgstr ""
"Обычно, при успешном завершении возвращается ноль. В некоторых B<ioctl>() "
"запросах возвращаемое значение считается выходным параметром и при успешном "
"завершении возвращается неотрицательное значение. В случае ошибки "
"возвращается -1 и значение I<errno> устанавливается соответствующим образом."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not a valid file descriptor."
msgstr "Значение I<fd> не является правильным файловым дескриптором."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<argp> references an inaccessible memory area."
msgstr "I<argp> ссылается на недоступную область памяти."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<request> or I<argp> is not valid."
msgstr "Неправильное значение I<request> или I<argp>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTTY>"
msgstr "B<ENOTTY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not associated with a character special device."
msgstr "Значение I<fd> не связано со специальным символьным устройством."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The specified request does not apply to the kind of object that the file "
"descriptor I<fd> references."
msgstr ""
"Указанный запрос не применяется к типу объекта, на который ссылается "
"файловый дескриптор I<fd>."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "No single standard.  Arguments, returns, and semantics of B<ioctl>()  "
#| "vary according to the device driver in question (the call is used as a "
#| "catch-all for operations that don't cleanly fit the UNIX stream I/O "
#| "model)."
msgid ""
"Arguments, returns, and semantics of B<ioctl>()  vary according to the "
"device driver in question (the call is used as a catch-all for operations "
"that don't cleanly fit the UNIX stream I/O model)."
msgstr ""
"Нет единого стандарта. Аргументы, возвращаемые значения и семантика "
"B<ioctl>() варьируются в соответствии с драйвером устройства (вызов, "
"используемый как всеохватывающий, не полностью соответствует потоковой "
"модели ввода/вывода в UNIX)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "Version\\~7 AT&T UNIX has"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"
msgid "B<ioctl(int >I<fildes>B<, int >I<request>B<, struct sgttyb *>I<argp>B<);>\n"
msgstr "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"(where B<struct sgttyb> has historically been used by B<stty>(2)  and "
"B<gtty>(2), and is polymorphic by request type (like a B<void *> would be, "
"if it had been available))."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "SysIII documents I<arg> without a type at all."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "4.3BSD."
msgid "4.3BSD has"
msgstr "4.3BSD."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"
msgid "B<ioctl(int >I<d>B<, unsigned long >I<request>B<, char *>I<argp>B<);>\n"
msgstr "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "(with B<char *> similarly in for B<void *>)."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "SysVr4 has"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"
msgid "B<int ioctl(int >I<fildes>B<, int >I<request>B<, ... /* >I<arg>B< */);>\n"
msgstr "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In order to use this call, one needs an open file descriptor.  Often the "
"B<open>(2)  call has unwanted side effects, that can be avoided under Linux "
"by giving it the B<O_NONBLOCK> flag."
msgstr ""
"Чтобы использовать этот вызов требуется открытый файловый дескриптор. Часто "
"вызов B<open>(2) приводит к нежелательным эффектам, которых в Linux можно "
"избежать указав флаг B<O_NONBLOCK>."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ioctl structure"
msgstr "Структура ioctl"

#.  added two sections - aeb
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Ioctl command values are 32-bit constants.  In principle these constants are "
"completely arbitrary, but people have tried to build some structure into "
"them."
msgstr ""
"Значения команд ioctl являются 32-битными константами. В принципе, эти "
"константы являются полностью случайными, но некоторые люди пытаются увидеть "
"в них определённую закономерность."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The old Linux situation was that of mostly 16-bit constants, where the "
#| "last byte is a serial number, and the preceding byte(s) give a type "
#| "indicating the driver.  Sometimes the major number was used: 0x03 for the "
#| "B<HDIO_*> ioctls, 0x06 for the B<LP*> ioctls.  And sometimes one or more "
#| "ASCII letters were used.  For example, B<TCGETS> has value 0x00005401, "
#| "with 0x54 = \\(aqT\\(aq indicating the terminal driver, and "
#| "B<CYGETTIMEOUT> has value 0x00435906, with 0x43 0x59 = \\(aqC\\(aq "
#| "\\(aqY\\(aq indicating the cyclades driver."
msgid ""
"The old Linux situation was that of mostly 16-bit constants, where the last "
"byte is a serial number, and the preceding byte(s) give a type indicating "
"the driver.  Sometimes the major number was used: 0x03 for the B<HDIO_*> "
"ioctls, 0x06 for the B<LP*> ioctls.  And sometimes one or more ASCII letters "
"were used.  For example, B<TCGETS> has value 0x00005401, with 0x54 = "
"\\[aq]T\\[aq] indicating the terminal driver, and B<CYGETTIMEOUT> has value "
"0x00435906, with 0x43 0x59 = \\[aq]C\\[aq] \\[aq]Y\\[aq] indicating the "
"cyclades driver."
msgstr ""
"В старом Linux, в основном, были 16-битные константы, в которых последний "
"байт является серийным номером, а предшествующим байт(ами) указывался "
"драйвер. Иногда использовался старший номер устройства: 0x03 для ioctl "
"вызовов B<HDIO_*>, 0x06 для ioctl вызовов B<LP*>. Иногда использовались одна "
"или несколько букв ASCII. Например, B<TCGETS> имеет значение 0x00005401, с "
"буквой 0x54 = \\(aqT\\(aq, указывающей на драйвер терминала и "
"B<CYGETTIMEOUT> имеет значение 0x00435906, с буквами 0x43 0x59 = \\(aqC\\(aq "
"\\(aqY\\(aq, указывающими на драйвер cyclades."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Later (0.98p5) some more information was built into the number.  One has 2 "
"direction bits (00: none, 01: write, 10: read, 11: read/write)  followed by "
"14 size bits (giving the size of the argument), followed by an 8-bit type "
"(collecting the ioctls in groups for a common purpose or a common driver), "
"and an 8-bit serial number."
msgstr ""
"Позже (0.98p5) в номер была встроена дополнительная информация. Появилось 2 "
"бита направления (00: нет, 01: запись, 10: чтение, 11: чтение/запись), за "
"которыми следовали 14 бит размера (указывают размер аргумента), за которыми "
"следовали 8 бит типа (собирающих вызовы ioctl в группы по назначению или "
"общему драйверу) и, наконец, 8 бит серийного номера."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The macros describing this structure live in I<E<lt>asm/ioctl.hE<gt>> and "
"are B<_IO(type,nr)> and B<{_IOR,_IOW,_IOWR}(type,nr,size)>.  They use "
"I<sizeof(size)> so that size is a misnomer here: this third argument is a "
"data type."
msgstr ""
"Макросы, описывающие эту структуру, расположены в I<E<lt>asm/ioctl.hE<gt>>, "
"B<_IO(type,nr)> и B<{_IOR,_IOW,_IOWR}(type,nr,size)>. Они используют "
"I<sizeof(size)>, так что говорить о размере здесь является неправильным "
"\\(em это третий параметр типа данных."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the size bits are very unreliable: in lots of cases they are "
"wrong, either because of buggy macros using I<sizeof(sizeof(struct))>, or "
"because of legacy values."
msgstr ""
"Заметим, что биты размера очень ненадёжны: во многих случаях они ошибочны "
"или потому что ошибочный макрос использует I<sizeof(sizeof(struct))> или "
"потому что таковы унаследованные значение."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Thus, it seems that the new structure only gave disadvantages: it does not "
"help in checking, but it causes varying values for the various architectures."
msgstr ""
"Таким образом, мы видим, что новая структура имеет только недостатки: она не "
"помогает в проверке, и приводит к различным значениям у разных архитектур."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#.  .BR mt (4),
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<execve>(2), B<fcntl>(2), B<ioctl_console>(2), B<ioctl_fat>(2), "
"B<ioctl_ficlone>(2), B<ioctl_ficlonerange>(2), B<ioctl_fideduperange>(2), "
"B<ioctl_fslabel>(2), B<ioctl_getfsmap>(2), B<ioctl_iflags>(2), "
"B<ioctl_ns>(2), B<ioctl_tty>(2), B<ioctl_userfaultfd>(2), B<open>(2), "
"B<sd>(4), B<tty>(4)"
msgstr ""
"B<execve>(2), B<fcntl>(2), B<ioctl_console>(2), B<ioctl_fat>(2), "
"B<ioctl_ficlone>(2), B<ioctl_ficlonerange>(2), B<ioctl_fideduperange>(2), "
"B<ioctl_fslabel>(2), B<ioctl_getfsmap>(2), B<ioctl_iflags>(2), "
"B<ioctl_ns>(2), B<ioctl_tty>(2), B<ioctl_userfaultfd>(2), B<open>(2), "
"B<sd>(4), B<tty>(4)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#.  POSIX says 'request' is int, but glibc has the above
#.  See https://bugzilla.kernel.org/show_bug.cgi?id=42705
#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"
msgstr "B<int ioctl(int >I<fd>B<, unsigned long >I<request>B<, ...);>\n"

#. type: Plain text
#: debian-bookworm
msgid ""
"No single standard.  Arguments, returns, and semantics of B<ioctl>()  vary "
"according to the device driver in question (the call is used as a catch-all "
"for operations that don't cleanly fit the UNIX stream I/O model)."
msgstr ""
"Нет единого стандарта. Аргументы, возвращаемые значения и семантика "
"B<ioctl>() варьируются в соответствии с драйвером устройства (вызов, "
"используемый как всеохватывающий, не полностью соответствует потоковой "
"модели ввода/вывода в UNIX)."

#. type: Plain text
#: debian-bookworm
msgid "The B<ioctl>()  system call appeared in Version 7 AT&T UNIX."
msgstr ""

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: Plain text
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
msgid "Version\\~7 AT&T UNIX."
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
