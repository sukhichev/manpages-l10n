# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:59+0100\n"
"PO-Revision-Date: 2019-10-05 08:17+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ioctl_ficlonerange"
msgstr "ioctl_ficlonerange"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"ioctl_ficlonerange, ioctl_ficlone - share some the data of one file with "
"another file"
msgstr ""
"ioctl_ficlonerange, ioctl_ficlone - сделать некоторые данные одного файла "
"общими с другим файлом"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/fs.hE<gt>>        /* Definition of B<FICLONE*> constants */\n"
"B<#include E<lt>sys/ioctl.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/fs.hE<gt>>        /* определения констант B<FICLONE*> */\n"
"B<#include E<lt>sys/ioctl.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int ioctl(int >I<dest_fd>B<, FICLONERANGE, struct file_clone_range *>I<arg>B<);>\n"
"B<int ioctl(int >I<dest_fd>B<, FICLONE, int >I<src_fd>B<);>\n"
msgstr ""
"B<int ioctl(int >I<dest_fd>B<, FICLONERANGE, struct file_clone_range *>I<arg>B<);>\n"
"B<int ioctl(int >I<dest_fd>B<, FICLONE, int >I<src_fd>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a filesystem supports files sharing physical storage between multiple "
"files (\"reflink\"), this B<ioctl>(2)  operation can be used to make some of "
"the data in the I<src_fd> file appear in the I<dest_fd> file by sharing the "
"underlying storage, which is faster than making a separate physical copy of "
"the data.  Both files must reside within the same filesystem.  If a file "
"write should occur to a shared region, the filesystem must ensure that the "
"changes remain private to the file being written.  This behavior is commonly "
"referred to as \"copy on write\"."
msgstr ""
"Если файловая система поддерживает общее файловое физическое хранилище для "
"файлов («reflink»), то эту операцию B<ioctl>(2) можно использовать для того, "
"чтобы часть данных файла I<src_fd> появилась в файле I<dest_fd> в виде "
"общего пространства на носителе, что быстрее по сравнению с созданием "
"отдельной физической копии данных. Оба файла должны располагаться в одной "
"файловой системе. Если выполняется запись в эту общую область, то файловая "
"система должна гарантировать, что изменения будут видны только в файле, в "
"который производится запись. Такое поведение часто называют как «копирование "
"при записи» (copy on write)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This ioctl reflinks up to I<src_length> bytes from file descriptor I<src_fd> "
"at offset I<src_offset> into the file I<dest_fd> at offset I<dest_offset>, "
"provided that both are files.  If I<src_length> is zero, the ioctl reflinks "
"to the end of the source file.  This information is conveyed in a structure "
"of the following form:"
msgstr ""
"Данный ioctl создаёт ссылку (reflink) на не более I<src_length> байт из "
"файлового дескриптора I<src_fd> по смещению I<src_offset> в файл I<dest_fd> "
"по смещению I<dest_offset>, предоставляя её обоим файлам. Если I<src_length> "
"равно нулю, то ioctl делает ссылку на конец файла-источника. Данная "
"информация передаётся в структуре следующего вида:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct file_clone_range {\n"
"    __s64 src_fd;\n"
"    __u64 src_offset;\n"
"    __u64 src_length;\n"
"    __u64 dest_offset;\n"
"};\n"
msgstr ""
"struct file_clone_range {\n"
"    __s64 src_fd;\n"
"    __u64 src_offset;\n"
"    __u64 src_length;\n"
"    __u64 dest_offset;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Clones are atomic with regards to concurrent writes, so no locks need to be "
"taken to obtain a consistent cloned copy."
msgstr ""
"Клоны атомарны для одновременной записи, поэтому для получения корректной "
"копии не нужно получать блокировки."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<FICLONE> ioctl clones entire files."
msgstr "Операция B<FICLONE> ioctl клонирует файлы полностью."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"В случае ошибки возвращается -1, а I<errno> устанавливается в значение "
"ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Error codes can be one of, but are not limited to, the following:"
msgstr "Возможные коды ошибок (помимо прочих):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<src_fd> is not open for reading; I<dest_fd> is not open for writing or is "
"open for append-only writes; or the filesystem which I<src_fd> resides on "
"does not support reflink."
msgstr ""
"Дескриптор I<src_fd> не открыт на чтение; I<dest_fd> не открыт на запись или "
"открыт только для добавления; файловая система, в которой находится "
"I<src_fd>, не поддерживает reflink."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The filesystem does not support reflinking the ranges of the given files.  "
"This error can also appear if either file descriptor represents a device, "
"FIFO, or socket.  Disk filesystems generally require the offset and length "
"arguments to be aligned to the fundamental block size.  XFS and Btrfs do not "
"support overlapping reflink ranges in the same file."
msgstr ""
"Файловая система не поддерживает создание ссылок диапазонов в заданных "
"файлах. Эта ошибка также может возникнуть, если файловый дескриптор "
"представляет устройство, FIFO или сокет. Обычно, для дисковых файловых "
"систем требуются, чтобы аргументы смещения и длины были выровнены по "
"основному размеру блока. В XFS и Btrfs нет поддержки ссылок перекрывающихся "
"диапазонов в одном и том же файле."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EISDIR>"
msgstr "B<EISDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"One of the files is a directory and the filesystem does not support shared "
"regions in directories."
msgstr ""
"Один из файлов является каталогом и файловая система не поддерживает "
"диапазоны для каталогов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This can appear if the filesystem does not support reflinking either file "
"descriptor, or if either file descriptor refers to special inodes."
msgstr ""
"Может возникать, если файловая система не поддерживает ссылки для файловых "
"дескрипторов или когда файловый дескриптор ссылается на специальные иноды."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<dest_fd> is immutable."
msgstr "Дескриптор I<dest_fd> является неизменным."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ETXTBSY>"
msgstr "B<ETXTBSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "One of the files is a swap file.  Swap files cannot share storage."
msgstr ""
"Один из файлов является файлом подкачки. Файлы подкачки не могут содержаться "
"в совместных хранилищах."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EXDEV>"
msgstr "B<EXDEV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<dest_fd> and I<src_fd> are not on the same mounted filesystem."
msgstr ""
"Дескрипторы I<dest_fd> и I<src_fd> находятся на разных смонтированных "
"файловых системах."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Linux"
msgid "Linux 4.5."
msgstr "Linux"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "These ioctl operations first appeared in Linux 4.5.  They were previously "
#| "known as B<BTRFS_IOC_CLONE> and B<BTRFS_IOC_CLONE_RANGE>, and were "
#| "private to Btrfs."
msgid ""
"They were previously known as B<BTRFS_IOC_CLONE> and "
"B<BTRFS_IOC_CLONE_RANGE>, and were private to Btrfs."
msgstr ""
"Данные операции ioctl появились в Linux 4.5. Раньше они назывались "
"B<BTRFS_IOC_CLONE> и B<BTRFS_IOC_CLONE_RANGE> и работали только в Btrfs."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Because a copy-on-write operation requires the allocation of new storage, "
"the B<fallocate>(2)  operation may unshare shared blocks to guarantee that "
"subsequent writes will not fail because of lack of disk space."
msgstr ""
"Так как для операции копирования-при-записи требуется выделение нового "
"пространства в хранилище, операция B<fallocate>(2) может отменить совместное "
"использование общих блоков для гарантии того, что последующие операции "
"записи не завершатся ошибкой из-за нехватки места на диске."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ioctl>(2)"
msgstr "B<ioctl>(2)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr "30 октября 2022 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm
msgid ""
"These ioctl operations first appeared in Linux 4.5.  They were previously "
"known as B<BTRFS_IOC_CLONE> and B<BTRFS_IOC_CLONE_RANGE>, and were private "
"to Btrfs."
msgstr ""
"Данные операции ioctl появились в Linux 4.5. Раньше они назывались "
"B<BTRFS_IOC_CLONE> и B<BTRFS_IOC_CLONE_RANGE> и работали только в Btrfs."

#. type: Plain text
#: debian-bookworm
msgid "This API is Linux-specific."
msgstr "Данный программный интерфейс существует только в Linux."

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
