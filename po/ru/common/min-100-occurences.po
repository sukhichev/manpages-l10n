# Common msgids
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:15+0100\n"
"PO-Revision-Date: 2024-01-28 06:47+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed mageia-cauldron archlinux debian-bullseye
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОРЫ"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРЫ"

#: opensuse-leap-15-6 archlinux debian-bookworm debian-unstable fedora-40
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНОСТЬ"

#: mageia-cauldron debian-bookworm fedora-40 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "Апрель 2022"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-40 fedora-rawhide
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#: archlinux fedora-40 fedora-rawhide mageia-cauldron debian-bookworm
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АВТОРСКИЕ ПРАВА"

#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторские права \\(co 2020 Free Software Foundation, Inc.  Лицензия GPLv3+: "
"GNU GPL версии 3 или выше E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: debian-bookworm mageia-cauldron archlinux fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторские права \\(co 2022 Free Software Foundation, Inc.  Лицензия GPLv3+: "
"GNU GPL версии 3 или выше E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed archlinux
#: mageia-cauldron
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Авторские права \\(co 2023 Free Software Foundation, Inc.  Лицензия GPLv3+: "
"GNU GPL версии 3 или выше E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed archlinux mageia-cauldron debian-bullseye
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#: opensuse-leap-15-6 debian-bookworm fedora-rawhide mageia-cauldron
msgid "Display help text and exit."
msgstr ""

#: opensuse-leap-15-6 archlinux debian-bookworm debian-unstable fedora-40
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Display version information and exit."
msgstr ""

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed mageia-cauldron archlinux
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ОКРУЖЕНИЕ"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#: debian-bookworm debian-unstable mageia-cauldron archlinux fedora-40
#: fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "КОД РЕЗУЛЬТАТА"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed mageia-cauldron archlinux debian-bullseye
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#: opensuse-leap-15-6 debian-bookworm fedora-rawhide mageia-cauldron
msgid "For bug reports, use the issue tracker at"
msgstr ""

#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#: debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed archlinux
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Онлайн-справка GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ИСТОРИЯ"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#: debian-unstable mageia-cauldron fedora-40 fedora-rawhide opensuse-tumbleweed
#: archlinux opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "январь 2024 г."

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed debian-bookworm
msgid "Linux."
msgstr "Linux."

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed archlinux mageia-cauldron debian-bullseye
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed archlinux mageia-cauldron debian-bullseye
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРЫ"

#: opensuse-leap-15-6 mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "Октябрь 2021"

#: debian-bookworm archlinux debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed debian-bookworm mageia-cauldron
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#: debian-bookworm opensuse-leap-15-6 archlinux fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed debian-unstable
msgid "Print version and exit."
msgstr ""

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-40 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ИНФОРМАЦИЯ ОБ ОШИБКАХ"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed archlinux mageia-cauldron
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Сообщайте обо всех ошибках перевода по адресу E<lt>https://"
"translationproject.org/team/ru.htmlE<gt>"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed archlinux mageia-cauldron debian-bullseye
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#: debian-bookworm debian-unstable archlinux fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed archlinux mageia-cauldron debian-bullseye
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Сентябрь 2022"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Это свободное программное обеспечение: вы можете изменять и распространять "
"его. БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, в пределах, разрешенных законом."

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-40 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Команды пользователя"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid "display this help and exit"
msgstr "выводит информацию о версии и заканчивает работу"

#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed archlinux
msgid "output version information and exit"
msgstr "выводит справку и заканчивает работу"

#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed fedora-40 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "можно получить доступ к полному справочнику."

#: debian-bookworm opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr "systemd 255"

#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"
