# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2012-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-09 15:36+0100\n"
"PO-Revision-Date: 2019-09-05 21:56+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "hd"
msgstr "hd"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "hd - MFM/IDE hard disk devices"
msgstr "hd - устройства для жёстких дисков MFM/IDE"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<hd*> devices are block devices to access MFM/IDE hard disk drives in "
"raw mode.  The master drive on the primary IDE controller (major device "
"number 3) is B<hda>; the slave drive is B<hdb>.  The master drive of the "
"second controller (major device number 22)  is B<hdc> and the slave is "
"B<hdd>."
msgstr ""
"Устройства B<hd*> — это блочные устройства для прямого доступа к жёстким "
"дискам MFM/IDE. Для главного (master) диска на первичном контроллере IDE "
"(старший номер 3) назначается имя B<hda>; для зависимого (slave) диска — "
"B<hdb>. Для главного диска на вторичном контроллере (старший номер 22) — "
"B<hdc>, а для зависимого — B<hdd>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "General IDE block device names have the form B<hd>I<X>, or B<hd>I<XP>, "
#| "where I<X> is a letter denoting the physical drive, and I<P> is a number "
#| "denoting the partition on that physical drive.  The first form, "
#| "B<hd>I<X>, is used to address the whole drive.  Partition numbers are "
#| "assigned in the order the partitions are discovered, and only nonempty, "
#| "nonextended partitions get a number.  However, partition numbers 1\\(en4 "
#| "are given to the four partitions described in the MBR (the \"primary\" "
#| "partitions), regardless of whether they are unused or extended.  Thus, "
#| "the first logical partition will be B<hd>I<X>B<5>\\&.  Both DOS-type "
#| "partitioning and BSD-disklabel partitioning are supported.  You can have "
#| "at most 63 partitions on an IDE disk."
msgid ""
"General IDE block device names have the form B<hd>I<X> , or B<hd>I<XP> , "
"where I<X> is a letter denoting the physical drive, and I<P> is a number "
"denoting the partition on that physical drive.  The first form, B<hd>I<X> , "
"is used to address the whole drive.  Partition numbers are assigned in the "
"order the partitions are discovered, and only nonempty, nonextended "
"partitions get a number.  However, partition numbers 1\\[en]4 are given to "
"the four partitions described in the MBR (the \"primary\" partitions), "
"regardless of whether they are unused or extended.  Thus, the first logical "
"partition will be B<hd>I<X>B<5> \\&.  Both DOS-type partitioning and BSD-"
"disklabel partitioning are supported.  You can have at most 63 partitions on "
"an IDE disk."
msgstr ""
"Названия блочных устройств IDE имеют вид B<hd>I<X> или B<hd>I<XP>, где I<X> "
"— буква, указывающая на физический диск, а I<P> — номер, указывающий на "
"раздел этого физического диска. Первая форма, B<hd>I<X>, используется для "
"обращения ко всему диску. Номера разделов назначаются в порядке их "
"обнаружения, и номера присваиваются только «не пустым», не расширенным "
"разделам. Однако, номера с первого по четвертый присваиваются разделам, "
"описанным в MBR («основным» (primary) разделам), независимо от того, "
"используются ли они или являются ли расширенными (extended). Поэтому первым "
"логическим разделом будет B<hd>I<X>B<5>\\&. Поддерживается как таблица "
"разделов DOS, так и BSD-disklabel. На IDE-диске может быть не более 63-х "
"разделов."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For example, I</dev/hda> refers to all of the first IDE drive in the system; "
"and I</dev/hdb3> refers to the third DOS \"primary\" partition on the second "
"one."
msgstr ""
"Например, I</dev/hda> ссылается на весь первый физический диск IDE, а I</dev/"
"hdb3> ссылается на третий, \"основной\", раздел DOS второго физического "
"диска."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "They are typically created by:"
msgstr "Файлы устройств, обычно, создаются следующим образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"mknod -m 660 /dev/hda b 3 0\n"
"mknod -m 660 /dev/hda1 b 3 1\n"
"mknod -m 660 /dev/hda2 b 3 2\n"
"\\&...\n"
"mknod -m 660 /dev/hda8 b 3 8\n"
"mknod -m 660 /dev/hdb b 3 64\n"
"mknod -m 660 /dev/hdb1 b 3 65\n"
"mknod -m 660 /dev/hdb2 b 3 66\n"
"\\&...\n"
"mknod -m 660 /dev/hdb8 b 3 72\n"
"chown root:disk /dev/hd*\n"
msgstr ""
"mknod -m 660 /dev/hda b 3 0\n"
"mknod -m 660 /dev/hda1 b 3 1\n"
"mknod -m 660 /dev/hda2 b 3 2\n"
"\\&...\n"
"mknod -m 660 /dev/hda8 b 3 8\n"
"mknod -m 660 /dev/hdb b 3 64\n"
"mknod -m 660 /dev/hdb1 b 3 65\n"
"mknod -m 660 /dev/hdb2 b 3 66\n"
"\\&...\n"
"mknod -m 660 /dev/hdb8 b 3 72\n"
"chown root:disk /dev/hd*\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/hd*>"
msgstr "I</dev/hd*>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chown>(1), B<mknod>(1), B<sd>(4), B<mount>(8)"
msgstr "B<chown>(1), B<mknod>(1), B<sd>(4), B<mount>(8)"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
