# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Kozlov <yuray@komyakino.ru>, 2012-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:55+0100\n"
"PO-Revision-Date: 2019-08-29 09:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "dsp56k"
msgstr "dsp56k"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 октября 2023 г."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "dsp56k - DSP56001 interface device"
msgstr "dsp56k - устройство интерфейса DSP56001"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>asm/dsp56k.hE<gt>>\n"
msgstr "B<#include E<lt>asm/dsp56k.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<ssize_t read(int >I<fd>B<, void *>I<data>B<, size_t >I<length>B<);>\n"
"B<ssize_t write(int >I<fd>B<, void *>I<data>B<, size_t >I<length>B<);>\n"
msgstr ""
"B<ssize_t read(int >I<fd>B<, void *>I<data>B<, size_t >I<length>B<);>\n"
"B<ssize_t write(int >I<fd>B<, void *>I<data>B<, size_t >I<length>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int ioctl(int >I<fd>B<, DSP56K_UPLOAD, struct dsp56k_upload *>I<program>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_SET_TX_WSIZE, int >I<wsize>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_SET_RX_WSIZE, int >I<wsize>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_HOST_FLAGS, struct dsp56k_host_flags *>I<flags>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_HOST_CMD, int >I<cmd>B<);>\n"
msgstr ""
"B<int ioctl(int >I<fd>B<, DSP56K_UPLOAD, struct dsp56k_upload *>I<program>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_SET_TX_WSIZE, int >I<wsize>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_SET_RX_WSIZE, int >I<wsize>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_HOST_FLAGS, struct dsp56k_host_flags *>I<flags>B<);>\n"
"B<int ioctl(int >I<fd>B<, DSP56K_HOST_CMD, int >I<cmd>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "НАСТРОЙКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<dsp56k> device is a character device with major number 55 and minor "
#| "number 0."
msgid ""
"The I<dsp56k> device is a character device with major number 55 and minor "
"number 0."
msgstr ""
"Устройство B<dsp56k> является символьным устройством с старшим номером 55 и "
"младшим номером 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The Motorola DSP56001 is a fully programmable 24-bit digital signal "
"processor found in Atari Falcon030-compatible computers.  The I<dsp56k> "
"special file is used to control the DSP56001, and to send and receive data "
"using the bidirectional handshaked host port."
msgstr ""
"Motorola DSP56001 \\(em это полностью программируемый 24-битный цифровой "
"сигнальный процессор, используемый в компьютерах Atari Falcon030 и "
"совместимых с ними. Специальный файл I<dsp56k> используется для управления "
"DSP56001 и для посылки и получения данных через выбранный на узле (host) "
"двунаправленный порт."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To send a data stream to the signal processor, use B<write>(2)  to the "
"device, and B<read>(2)  to receive processed data.  The data can be sent or "
"received in 8, 16, 24, or 32-bit quantities on the host side, but will "
"always be seen as 24-bit quantities in the DSP56001."
msgstr ""
"Для отправки потока данных сигнальному процессору, используйте B<write>(2), "
"а для получения обработанных данных \\(em B<read>(2). Данные могут быть "
"посылаться и получаться узлом по 8, 16, 24 или 32 бита, а сигнальный "
"процессор способен обрабатывать только по 24 бита."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following B<ioctl>(2)  calls are used to control the I<dsp56k> device:"
msgstr ""
"Для управления устройством I<dsp56k> используются следующие вызовы "
"B<ioctl>(2):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<DSP56K_UPLOAD>"
msgstr "B<DSP56K_UPLOAD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"resets the DSP56001 and uploads a program.  The third B<ioctl>(2)  argument "
"must be a pointer to a I<struct dsp56k_upload> with members I<bin> pointing "
"to a DSP56001 binary program, and I<len> set to the length of the program, "
"counted in 24-bit words."
msgstr ""
"Обнуляет DSP56001 и загружает в него программу. Третьим аргументом "
"B<ioctl>(2) должен быть указатель на I<struct dsp56k_upload>, в которое поле "
"I<bin> указывает на двоичную программу DSP56001, а в I<len> задаётся длина "
"программы (значение должно быть кратно 24-битным словам)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<DSP56K_SET_TX_WSIZE>"
msgstr "B<DSP56K_SET_TX_WSIZE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "sets the transmit word size.  Allowed values are in the range 1 to 4, and "
#| "is the number of bytes that will be sent at a time to the DSP56001.  "
#| "These data quantities will either be padded with zero bytes, or truncated "
#| "to fit the native 24-bit data format of the DSP56001."
msgid ""
"sets the transmit word size.  Allowed values are in the range 1 to 4, and is "
"the number of bytes that will be sent at a time to the DSP56001.  These data "
"quantities will either be padded with bytes containing zero, or truncated to "
"fit the native 24-bit data format of the DSP56001."
msgstr ""
"Устанавливает размер передаваемых слов. Допустимое значение находится в "
"пределах от 1 до 4, и является числом байт, посылаемых за один раз в "
"DSP56001. Эти частички информации будут заполнены нулевыми байтами или "
"преобразованы в формат 24-битных данных DSP56001."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<DSP56K_SET_RX_WSIZE>"
msgstr "B<DSP56K_SET_RX_WSIZE>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "sets the receive word size.  Allowed values are in the range 1 to 4, and "
#| "is the number of bytes that will be received at a time from the "
#| "DSP56001.  These data quantities will either truncated, or padded with a "
#| "null byte (\\(aq\\e0\\(aq) to fit the native 24-bit data format of the "
#| "DSP56001."
msgid ""
"sets the receive word size.  Allowed values are in the range 1 to 4, and is "
"the number of bytes that will be received at a time from the DSP56001.  "
"These data quantities will either truncated, or padded with a null byte "
"(\\[aq]\\e0\\[aq]), to fit the native 24-bit data format of the DSP56001."
msgstr ""
"Устанавливает размер принимаемых слов. Допустимое значение находится в "
"диапазоне от 1 до 4, и является числом байт, принимаемых за один раз от "
"процессора DSP56001. Эти частички информации будут урезаны или дополнены "
"байтом null (\\(aq\\e0\\(aq), чтобы соответствовать 24-битному формату "
"DSP56001."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<DSP56K_HOST_FLAGS>"
msgstr "B<DSP56K_HOST_FLAGS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"read and write the host flags.  The host flags are four general-purpose bits "
"that can be read by both the hosting computer and the DSP56001.  Bits 0 and "
"1 can be written by the host, and bits 2 and 3 can be written by the "
"DSP56001."
msgstr ""
"Считывает и записывает флаги узла. Флаги узла \\(em это четыре бита общего "
"назначения, они могут быть считаны как узлом, так и процессором DSP56001. "
"Нулевой и первый биты могут записываться узлом, а второй и третий пишутся "
"устройством DSP56001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To access the host flags, the third B<ioctl>(2)  argument must be a pointer "
"to a I<struct dsp56k_host_flags>.  If bit 0 or 1 is set in the I<dir> "
"member, the corresponding bit in I<out> will be written to the host flags.  "
"The state of all host flags will be returned in the lower four bits of the "
"I<status> member."
msgstr ""
"Для получения доступа к флагам узла третий аргумент B<ioctl>(2) должен быть "
"указателем на I<struct dsp56k_host_flags>. Если нулевой и первый поля I<dir> "
"установлены в единичное значение, то соответствующий бит в поле I<out> будет "
"записан в флаг узла. Состояние всех флагов узла будет возвращено в младших "
"четырёх битах поля I<status>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<DSP56K_HOST_CMD>"
msgstr "B<DSP56K_HOST_CMD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"sends a host command.  Allowed values are in the range 0 to 31, and is a "
"user-defined command handled by the program running in the DSP56001."
msgstr ""
"Посылает команды узла. Допустимые величины находятся в диапазоне от 0 до 31, "
"это определяемые пользователем команды, обрабатываемые программой, "
"выполняющейся на DSP56001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#.  .SH AUTHORS
#.  Fredrik Noring <noring@nocrew.org>, lars brinkhoff <lars@nocrew.org>,
#.  Tomas Berndtsson <tomas@nocrew.org>.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/dsp56k>"
msgstr "I</dev/dsp56k>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<linux/include/asm-m68k/dsp56k.h>, I<linux/drivers/char/dsp56k.c>, E<.UR "
"http://dsp56k.nocrew.org/> E<.UE ,> DSP56000/DSP56001 Digital Signal "
"Processor User's Manual"
msgstr ""
"I<linux/include/asm-m68k/dsp56k.h>, I<linux/drivers/char/dsp56k.c>, E<.UR "
"http://dsp56k.nocrew.org/> E<.UE ,> DSP56000/DSP56001 Digital Signal "
"Processor User's Manual"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "sets the receive word size.  Allowed values are in the range 1 to 4, and "
#| "is the number of bytes that will be received at a time from the "
#| "DSP56001.  These data quantities will either truncated, or padded with a "
#| "null byte (\\(aq\\e0\\(aq) to fit the native 24-bit data format of the "
#| "DSP56001."
msgid ""
"sets the receive word size.  Allowed values are in the range 1 to 4, and is "
"the number of bytes that will be received at a time from the DSP56001.  "
"These data quantities will either truncated, or padded with a null byte "
"(\\[aq]\\e0\\[aq]) to fit the native 24-bit data format of the DSP56001."
msgstr ""
"Устанавливает размер принимаемых слов. Допустимое значение находится в "
"диапазоне от 1 до 4, и является числом байт, принимаемых за один раз от "
"процессора DSP56001. Эти частички информации будут урезаны или дополнены "
"байтом null (\\(aq\\e0\\(aq), чтобы соответствовать 24-битному формату "
"DSP56001."

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-08"
msgstr "8 марта 2023 г."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
