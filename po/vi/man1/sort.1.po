# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-15 18:11+0100\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SORT"
msgstr "SORT"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "January 2024"
msgstr "Tháng 1 năm 2024"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sort - sort lines of text files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sort> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<sort> [I<\\,TÙY_CHỌN\\/>]… [I<\\,TẬP_TIN\\/>]…"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sort> [I<\\,OPTION\\/>]... I<\\,--files0-from=F\\/>"
msgstr "B<sort> [I<\\,TÙY_CHỌN\\/>]… I<\\,--files0-from=F\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Write sorted concatenation of all FILE(s) to standard output."
msgstr "Ghi ra đầu ra tiêu chuẩn bản ghép nối đã sắp xếp của tất cả TẬP_TIN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"Không chỉ ra TẬP_TIN, hoặc khi TẬP_TIN là “-”, thì đọc từ đầu vào tiêu chuẩn."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too.  "
"Ordering options:"
msgstr ""
"Tùy chọn dài yêu cầu đối số thì tùy chọn ngắn cũng vậy. Tùy chọn sắp xếp:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--ignore-leading-blanks>"
msgstr "B<-b>, B<--ignore-leading-blanks>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ignore leading blanks"
msgstr "bỏ qua những khoảng trắng ở đầu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--dictionary-order>"
msgstr "B<-d>, B<--dictionary-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "consider only blanks and alphanumeric characters"
msgstr "tính chỉ các dấu cách và các ký tự chữ-cái/số"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--ignore-case>"
msgstr "B<-f>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fold lower case to upper case characters"
msgstr "tính chữ thường là chữ hoa"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-g>, B<--general-numeric-sort>"
msgstr "B<-g>, B<--general-numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare according to general numerical value"
msgstr "so sánh theo giá trị thuộc số kiểu chung"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-nonprinting>"
msgstr "B<-i>, B<--ignore-nonprinting>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "consider only printable characters"
msgstr "tính chỉ các ký tự có thể in được"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-M>, B<--month-sort>"
msgstr "B<-M>, B<--month-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare (unknown) E<lt> 'JAN' E<lt> ... E<lt> 'DEC'"
msgstr "so sánh (không hiểu) E<lt> “Th1” E<lt> … E<lt> “Th12”"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-numeric-sort>"
msgstr "B<-h>, B<--human-numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare human readable numbers (e.g., 2K 1G)"
msgstr "so sánh các số mà người đọc được (v.d. 2K 1G)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--numeric-sort>"
msgstr "B<-n>, B<--numeric-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compare according to string numerical value"
msgstr "so sánh theo giá trị thuộc số kiểu chuỗi"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--random-sort>"
msgstr "B<-R>, B<--random-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "shuffle, but group identical keys.  See shuf(1)"
msgid "shuffle, but group identical keys.  See B<shuf>(1)"
msgstr "xáo trộn, nhưng nhóm các khóa định danh. Xem B<shuf>(1)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--random-source>=I<\\,FILE\\/>"
msgstr "B<--random-source>=I<\\,TẬP_TIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "get random bytes from FILE"
msgstr "lấy các byte ngẫu nhiên từ tập tin này"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--reverse>"
msgstr "B<-r>, B<--reverse>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "reverse the result of comparisons"
msgstr "đảo ngược kết quả so sánh"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--sort>=I<\\,WORD\\/>"
msgstr "B<--sort>=I<\\,TỪ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid ""
"sort according to WORD: general-numeric B<-g>, human-numeric B<-h>, month B<-"
"M>, numeric B<-n>, random B<-R>, version B<-V>"
msgstr ""
"sắp xếp theo TỪ:\n"
"                               * general-numeric -g\t\tthuộc số kiểu chung\n"
"                               * human-numeric -h\t\tthuộc số kiểu người\n"
"                               * month -M\t\t\t\ttháng\n"
"                               * numeric -n\t\t\tthuộc số\n"
"                               * random -R\t\t\tngẫu nhiên\n"
"                               * version -V\t\t\tphiên bản"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version-sort>"
msgstr "B<-V>, B<--version-sort>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "natural sort of (version) numbers within text"
msgstr "sắp xếp các số thứ tự (phiên bản) một cách tự nhiên bên trong văn bản"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Other options:"
msgstr "Những tùy chọn khác:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--batch-size>=I<\\,NMERGE\\/>"
msgstr "B<--batch-size>=I<\\,SỐ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "merge at most NMERGE inputs at once; for more use temp files"
msgstr ""
"gộp lại đồng thời nhiều nhất SỐ đầu vào; nếu cần hơn, hãy sử dụng các tập "
"tin tạm thời"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--check>, B<--check>=I<\\,diagnose-first\\/>"
msgstr "B<-c>, B<--check>, B<--check>=I<\\,diagnose-first\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "check for sorted input; do not sort"
msgstr "kiểm tra có đầu vào đã sắp xếp; không nên sắp xếp"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--check>=I<\\,quiet\\/>, B<--check>=I<\\,silent\\/>"
msgstr "B<-C>, B<--check>=I<\\,quiet\\/>, B<--check>=I<\\,silent\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "like B<-c>, but do not report first bad line"
msgstr "giống B<-c>, nhưng không thông báo dòng sai thứ nhất"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<--diff-program>=I<PROGRAM>"
msgid "B<--compress-program>=I<\\,PROG\\/>"
msgstr "B<--compress-program>=I<\\,PROG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compress temporaries with PROG; decompress them with PROG B<-d>"
msgstr "nén các đồ tạm thời bằng chương trình này; để giải nén cũng đặt B<-d>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--debug>"
msgstr "B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"annotate the part of the line used to sort, and warn about questionable "
"usage to stderr"
msgstr ""
"diễn giải thành phần của dòng được dùng để sắp xếp, và cảnh báo về các dùng "
"đáng ngờ ra đầu ra lỗi chuẩn"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--files0-from>=I<\\,F\\/>"
msgstr "B<--files0-from>=I<\\,F\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"read input from the files specified by NUL-terminated names in file F; If F "
"is - then read names from standard input"
msgstr ""
"đọc dữ liệu vào từ những tập tin chỉ ra bởi các tập tin chấm dứt rỗng (NUL) "
"trong tập tin F; Nếu F là “-” thì đọc các tên từ đầu vào tiêu chuẩn"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--key>=I<\\,KEYDEF\\/>"
msgstr "B<-k>, B<--key>=I<\\,KEYDEF\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sort via a key; KEYDEF gives location and type"
msgstr "sắp xếp qua khóa, KEYDEF chỉ ra vị trí và kiểu"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--merge>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "merge already sorted files; do not sort"
msgstr "hòa trộn các tập tin đã sắp xếp rồi; chứ không sắp xếp"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,TẬP_TIN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write result to FILE instead of standard output"
msgstr "ghi kết quả vào tập tin này thay cho đầu ra tiêu chuẩn"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--stable>"
msgstr "B<-s>, B<--stable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "stabilize sort by disabling last-resort comparison"
msgstr "ổn định sắp xếp bằng việc tắt so sánh sắp xếp lại cuối cùng"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--buffer-size>=I<\\,SIZE\\/>"
msgstr "B<-S>, B<--buffer-size>=I<\\,CỠ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use SIZE for main memory buffer"
msgstr "dùng CỠ cho bộ nhớ đệm chính"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--field-separator>=I<\\,SEP\\/>"
msgstr "B<-t>, B<--field-separator>=I<\\,SEP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "use SEP instead of non-blank to blank transition"
msgstr "phân cách các trường bằng SEP này thay ký tự trống"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--temporary-directory>=I<\\,DIR\\/>"
msgstr "B<-T>, B<--temporary-directory>=I<\\,TMỤC\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"use DIR for temporaries, not $TMPDIR or I<\\,/tmp\\/>; multiple options "
"specify multiple directories"
msgstr ""
"dùng TMỤC làm thư mục tạm thời, không phải $TMPDIR hay I<\\,/tmp\\/>; có thể "
"chỉ định nhiều thư mục"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--parallel>=I<\\,N\\/>"
msgstr "B<--parallel>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "change the number of sorts run concurrently to N"
msgstr "thay đổi số sắp xếp chạy đồng thời là N"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unique>"
msgstr "B<-u>, B<--unique>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"with B<-c>, check for strict ordering; without B<-c>, output only the first "
"of an equal run"
msgstr ""
"nếu có B<-c>, kiểm tra có thứ tự chặt chẽ; khi không có B<-c>, chỉ xuất kết "
"quả chạy đầu tiên"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "bộ phân tách dòng là NUL, không phải ký tự dòng mới"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"KEYDEF is F[.C][OPTS][,F[.C][OPTS]] for start and stop position, where F is "
"a field number and C a character position in the field; both are origin 1, "
"and the stop position defaults to the line's end.  If neither B<-t> nor B<-"
"b> is in effect, characters in a field are counted from the beginning of the "
"preceding whitespace.  OPTS is one or more single-letter ordering options "
"[bdfgiMhnRrV], which override global ordering options for that key.  If no "
"key is given, use the entire line as the key.  Use B<--debug> to diagnose "
"incorrect key usage."
msgstr ""
"KEYDEF  là F[.C][TÙY_CHỌN…], [F[.C][TÙY_CHỌN…]] cho điểm đầu và cuối, trong "
"đó F là số thứ tự trường và C là vị trí của ký tự trong trường; cả hai có "
"gốc bắt đầu là 1, và điểm kết thúc mặc định là cuối dòng. Nếu hoặc B<-t> "
"hoặc B<-b>  chịu tác động, các ký tự trong trường được đếm từ trước khoảng "
"trắng. TÙY_CHỌN là một hoặc vài ký tự đơn xếp theo thứ tự [bdfgiMhnRrV], các "
"tùy chọn này sẽ đè lên các tùy chọn toàn cục. Khi không đưa ra một khóa, thì "
"dùng cả dòng làm khóa. Dùng B<--debug> để chuẩn đoán cách dùng khóa sai."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SIZE may be followed by the following multiplicative suffixes: % 1% of "
#| "memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y."
msgid ""
"SIZE may be followed by the following multiplicative suffixes: % 1% of "
"memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y, R, Q."
msgstr ""
"CỠ có thể theo bởi hậu tố là bội số của: % 1% của bộ nhớ, b 1, K 1024 (mặc "
"định), và tương tự như vậy với M, G, T, P, E, Z, Y."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"*** WARNING *** The locale specified by the environment affects sort order.  "
"Set LC_ALL=C to get the traditional sort order that uses native byte values."
msgstr ""
"*** CẢNH BÁO ***  Biến môi trường địa phương ảnh hưởng đến thứ tự sắp xếp. "
"Đặt “LC_ALL=C” để dùng thứ tự sắp xếp truyền thống theo giá trị byte gốc."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Mike Haertel and Paul Eggert."
msgstr "Viết bởi Mike Haertel và Paul Eggert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Trợ giúp trực tuyến GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Report %s translation bugs to <https://translationproject.org/team/>\n"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Hãy thông báo lỗi dịch “%s” cho <https://translationproject.org/team/vi."
"html>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "shuf(1), uniq(1)"
msgid "B<shuf>(1), B<uniq>(1)"
msgstr "B<shuf>(1), B<uniq>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/sortE<gt>"
msgstr ""
"Tài liệu đầy đủ có tại: E<lt>https://www.gnu.org/software/coreutils/sortE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) sort invocation\\(aq"
msgstr ""
"hoặc sẵn có nội bộ thông qua: info \\(aq(coreutils) sort invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Tháng 9 năm 2022"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm mageia-cauldron opensuse-leap-15-6
msgid ""
"SIZE may be followed by the following multiplicative suffixes: % 1% of "
"memory, b 1, K 1024 (default), and so on for M, G, T, P, E, Z, Y."
msgstr ""
"CỠ có thể theo bởi hậu tố là bội số của: % 1% của bộ nhớ, b 1, K 1024 (mặc "
"định), và tương tự như vậy với M, G, T, P, E, Z, Y."

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Tháng 4 năm 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Tháng 10 năm 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "shuffle, but group identical keys.  See shuf(1)"
msgstr "xáo trộn, nhưng nhóm các khóa định danh. Xem B<shuf>(1)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "shuf(1), uniq(1)"
msgstr "B<shuf>(1), B<uniq>(1)"
