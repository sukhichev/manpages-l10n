# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-09 16:59+0100\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MACBLESS"
msgstr "GRUB-MACBLESS"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "Tháng 12 năm 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-1"
msgstr "GRUB 2:2.12-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Tiện ích quản trị hệ thống"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-macbless - bless a mac file/directory"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-macbless> [I<\\,OPTION\\/>...] I<\\,--ppc PATH|--x86 FILE\\/>"
msgstr ""
"B<grub-macbless> [I<\\,TÙY_CHỌN\\/>…] I<\\,--ppc ĐƯỜNG_DẪN|--x86 TẬP_TIN\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Mac-style bless on HFS or HFS+"
msgstr "Đặt phân vùng khởi động kiểu Mac trên HFS hay HFS+"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-p>, B<--ppc>"
msgstr "B<-p>, B<--ppc>"

# bless -- set volume bootability and startup disk
# Xem https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man8/bless.8.html
#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "bless for ppc-based macs"
msgstr ""
"đặt phân vùng có khả năng khởi động và đĩa khởi động trên máy macs chạy bộ "
"vi xử lý ppc"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "hiển thị thông tin chi tiết."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-x>, B<--x86>"
msgstr "B<-x>, B<--x86>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "bless for x86-based macs"
msgstr ""
"đặt phân vùng có khả năng khởi động và đĩa khởi động trên máy macs chạy bộ "
"vi xử lý x86"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "hiển thị trợ giúp này"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "hiển thị cách sử dụng dạng ngắn gọn"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "in ra phiên bản chương trình"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Hãy thông báo lỗi cho  E<lt>bug-grub@gnu.orgE<gt>. Thông báo lỗi dịch cho: "
"E<lt>http://translationproject.org/team/vi.htmlE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-install>(1)"
msgstr "B<grub-install>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-macbless> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-macbless> programs are properly installed "
"at your site, the command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<grub-macbless> được bảo trì dưới dạng một sổ "
"tay Texinfo.  Nếu chương trình B<info> và B<grub-macbless> được cài đặt đúng "
"ở địa chỉ của bạn thì câu lệnh"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-macbless>"
msgstr "B<info grub-macbless>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "October 2023"
msgstr "Tháng 10 năm 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13+deb12u1"
msgstr "GRUB 2.06-13+deb12u1"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "January 2024"
msgstr "Tháng 1 năm 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-1"
msgstr "GRUB 2.12-1"
