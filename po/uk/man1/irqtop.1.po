# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andriy Rysin <arysin@gmail.com>, 2022.
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-08-27 17:03+0200\n"
"PO-Revision-Date: 2023-07-06 20:31+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "IRQTOP"
msgstr "IRQTOP"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "March 2018"
msgstr "Березень 2018 року"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "irqtop"
msgstr "irqtop"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr "Команди користувача"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "irqtop - Observe IRQ and SoftIRQ in a top-like fashion"
msgstr "irqtop — спостереження за IRQ і SoftIRQ у top-подібному вигляді"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Shows interrupt rates (per second) per cpu.  Also shows irq affinity ('.' "
"for disabled cpus), and rps/xps affinity ('+' rx, '-' tx, '*' tx/rx).  Can "
"show packet rate per eth queue."
msgstr ""
"Вивести частоти переривань (за секунду) за процесорами. Також вивести "
"спорідненість irq («.» для вимкнених процесорів) і спорідненість rps/xps "
"(«+» — отримання, «-» — надсилання, «*» — надсилання/отримання). Можливий "
"показ частоти пакетів за чергами eth."

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "irqtop [-h] [-d] [-b] [-t|-x] [-i|-s] [-r]"
msgstr "irqtop [-h] [-d] [-b] [-t|-x] [-i|-s] [-r]"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--delay>=I<\\,n\\/>"
msgstr "B<-d>, B<--delay>=I<\\,n\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "refresh interval"
msgstr "інтервал оновлення"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--softirq>"
msgstr "B<-s>, B<--softirq>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "select softirqs only"
msgstr "вибрати лише softirq"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-i>, B<--irq>"
msgstr "B<-i>, B<--irq>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "select hardware irqs only"
msgstr "вибрати лише апаратні irq"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-e>, B<--eth>"
msgstr "B<-e>, B<--eth>"

#. type: Plain text
#: debian-bookworm
msgid "show extra eth stats (from ethtool)"
msgstr "показати додаткові статистичні дані eth (з ethtool)"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-R>, B<--rps>"
msgstr "B<-R>, B<--rps>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "enable display of rps/xps"
msgstr "увімкнути показ rps/xps"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-x>, B<--table>"
msgstr "B<-x>, B<--table>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "output in table mode (default)"
msgstr "вивести дані у табличному режимі (типова поведінка)"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-t>, B<--top>"
msgstr "B<-t>, B<--top>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "output in flat top mode"
msgstr "вивести дані у простому режимі top"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-b>, B<--batch>"
msgstr "B<-b>, B<--batch>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "output non-interactively"
msgstr "виводити дані неінтерактивно"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--reverse>"
msgstr "B<-r>, B<--reverse>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "reverse sort order"
msgstr "обернений порядок записів"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--nocolor>"
msgstr "B<-C>, B<--nocolor>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "disable colors"
msgstr "вимкнути розфарбовування"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NOTES"
msgstr "ПРИМІТКИ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Rates marked as '.' is forbidden by smp_affinity mask."
msgstr "Оцінки, позначені як «.», є забороненим маскою smp_affinity."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "htop(1), top(1), ntop(1)"
msgstr "htop(1), top(1), ntop(1)"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR AND LICENSE"
msgstr "АВТОР І ЛІЦЕНЗУВАННЯ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<irqtop> was written by ABC E<lt>abc@openwall.comE<gt> and is licensed "
"under the GNU GPL."
msgstr ""
"I<irqtop> було написано ABC E<lt>abc@openwall.comE<gt> і ліцензовано за умов "
"дотримання GNU GPL."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This man page was written by Axel Beckert E<lt>abe@debian.orgE<gt> for the "
"Debian GNU/Linux distribution (but it may be used by others)."
msgstr ""
"Цю сторінку підручника написав Axel Beckert E<lt>abe@debian.orgE<gt> для "
"дистрибутива Debian GNU/Linux (але може використовуватися й на інших)."

#. type: Plain text
#: debian-unstable
msgid "show extra eth stats (from ethtool(8))"
msgstr "показати додаткові статистичні дані eth (з ethtool(8))"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-6
msgid "irqtop - utility to display kernel interrupt information"
msgstr "irqtop — допоміжна програма для показу даних щодо переривань ядра"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<irqtop> [options]"
msgstr "B<irqtop> [параметри]"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display kernel interrupt counter information in B<top>(1) style view."
msgstr "Показ даних лічильника переривань ядра у стилі показу B<top>(1)."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The default output is subject to change. So whenever possible, you should "
"avoid using default outputs in your scripts. Always explicitly define "
"expected columns by using B<--output>."
msgstr ""
"Типовий формат виведення може бути змінено авторами програми. Тому, коли це "
"можливо, вам слід уникати обробки типових виведених даних у ваших скриптах. "
"Завжди явно визначайте очікувані стовпчики за допомогою параметра B<--"
"output>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<список>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns. The default list of columns may be extended if list is "
"specified in the format I<+list>."
msgstr ""
"Вказати, які стовпчики слід вивести. Скористайтеся параметром B<--help>, щоб "
"переглянути опис стовпчиків. Типовий список стовпчиків може бути розширено, "
"якщо список вказано у форматі I<+список>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-d>, B<--delay> I<seconds>"
msgstr "B<-d>, B<--delay> I<секунди>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Update interrupt output every I<seconds> intervals."
msgstr ""
"Оновлювати виведення щодо переривань з інтервалом у вказану кількість "
"I<секунд>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-s>, B<--sort> I<column>"
msgstr "B<-s>, B<--sort> I<стовпчик>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Specify sort criteria by column name. See B<--help> output to get column "
"names. The sort criteria may be changes in interactive mode."
msgstr ""
"Вказати критерій упорядковування за назвою стовпчика. Див. дані, які "
"виведено B<--help>, щоб дізнатися назви стовпчиків. Критерії упорядковування "
"можна змінювати в інтерактивному режимі."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-S>, B<--softirq>"
msgstr "B<-S>, B<--softirq>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Show softirqs information."
msgstr "Вивести дані щодо softirq."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "INTERACTIVE MODE KEY COMMANDS"
msgstr "КЛАВІАТУРНІ КОМАНДИ ІНТЕРАКТИВНОГО РЕЖИМУ"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<i>"
msgstr "B<i>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "sort by short irq name or number field"
msgstr "упорядкувати за короткою назвою irq або полем кількості"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<t>"
msgstr "B<t>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "sort by total count of interrupts (the default)"
msgstr "упорядкувати за загальною кількістю переривань (типово)"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<d>"
msgstr "B<d>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "sort by delta count of interrupts"
msgstr "упорядкувати за лічильником різниці переривань"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<n>"
msgstr "B<n>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "sort by long descriptive name field"
msgstr "упорядкувати за полем довгої описової назви"

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<q Q>"
msgstr "B<q Q>"

#. type: Plain text
#: opensuse-leap-15-6
msgid "stop updates and exit program"
msgstr "припинити оновлення і вийти з програми"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<irqtop> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<irqtop> є частиною пакунка util-linux, який можна отримати з"
