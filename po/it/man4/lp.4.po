# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Giovanni Bortolozzo <borto@dei.unipd.it>, 1996.
# Alessandro Rubini <rubini@linux.it>, 1997.
# Giulio Daprelà <giulio@pluto.it>, 2005.
# Marco Curreli <marcocurreli@tiscali.it>, 2013, 2018, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2024-03-01 17:00+0100\n"
"PO-Revision-Date: 2021-04-07 00:10+0200\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "lp"
msgstr "lp"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 febbraio 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "lp - line printer devices"
msgstr "lp - file speciali per le stampanti"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/lp.hE<gt>>\n"
msgstr "B<#include E<lt>linux/lp.hE<gt>>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURAZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<lp>[0\\[en]2] are character devices for the parallel line printers; they "
"have major number 6 and minor number 0\\[en]2.  The minor numbers correspond "
"to the printer port base addresses 0x03bc, 0x0378, and 0x0278.  Usually they "
"have mode 220 and are owned by user I<root> and group I<lp>.  You can use "
"printer ports either with polling or with interrupts.  Interrupts are "
"recommended when high traffic is expected, for example, for laser printers.  "
"For typical dot matrix printers, polling will usually be enough.  The "
"default is polling."
msgstr ""
"B<lp>[0\\[en]2] sono dispositivi a carattere per le stampanti connesse sulla "
"porta parallela; hanno numero primario 6 e numero secondario 0\\[en]2.  I "
"numeri secondari corrispondono agli indirizzi base delle porte della "
"stampante 0x03bc, 0x0378 e 0x0278.  Si solito hanno modo di accesso 220 e "
"sono di proprietà dell'utente  I<root> e gruppo I<lp>.  Si può usare la "
"porta della stampante sia in polling che con interrupt.  Gli interrupt sono "
"raccomandati quando è previsto molto traffico, per esempio per le stampanti "
"laser.  Per le tipiche stampanti ad aghi di solito è sufficiente il polling. "
"Il default è il polling."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following B<ioctl>(2)  calls are supported:"
msgstr "Sono supportate le seguenti chiamate B<ioctl>(2):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPTIME, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPTIME, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sets the amount of time that the driver sleeps before rechecking the printer "
"when the printer's buffer appears to be filled to I<arg>.  If you have a "
"fast printer, decrease this number; if you have a slow printer, then "
"increase it.  This is in hundredths of a second, the default 2 being 0.02 "
"seconds.  It influences only the polling driver."
msgstr ""
"Pone uguale ad I<arg> l'ammontare di tempo che il driver deve aspettare "
"prima di ricontrollare la stampante quando il buffer della stampante sembra "
"essere pieno. Se si ha un stampante veloce, si può diminuire questo numero; "
"se si ha una stampante lenta conviene aumentarlo. Il valore è espresso in "
"centesimi di secondo, e il default 2 significa 0.02 secondi. Questo valore "
"influenza solo il driver di tipo \"polling\"."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPCHAR, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPCHAR, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sets the maximum number of busy-wait iterations which the polling driver "
"does while waiting for the printer to get ready for receiving a character to "
"I<arg>.  If printing is too slow, increase this number; if the system gets "
"too slow, decrease this number.  The default is 1000.  It influences only "
"the polling driver."
msgstr ""
"Assenga ad I<arg> il numero massimo di iterazioni in busy-wait che il driver "
"di tipo \"polling\" effetua aspettando che la stampante sia pronta a "
"ricevere un carattere. Se la stampa è troppo lenta, si può incrementare "
"questo numero; se il sistema va troppo piano lo si decrementi. Il default è "
"1000. Il valore ha influenza solo sulla sequenza di interrogazione dei "
"driver."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPABORT, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPABORT, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, the printer driver will retry on errors, otherwise it will "
"abort.  The default is 0."
msgstr ""
"Se I<arg> è 0, il driver della stampante riproverà a trasmettere nel caso si "
"verifichino errori, altrimenti interromperà la stampa. Il default è 0."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPABORTOPEN, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPABORTOPEN, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, B<open>(2)  will be aborted on error, otherwise error will "
"be ignored.  The default is to ignore it."
msgstr ""
"Se I<arg> è 0, B<open>(2) sarà terminato in caso un errore, altrimenti "
"l'errore sarà ignorato. Il default è di ignorarlo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPCAREFUL, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPCAREFUL, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<arg> is 0, then the out-of-paper, offline, and error signals are "
"required to be false on all writes, otherwise they are ignored.  The default "
"is to ignore them."
msgstr ""
"Se I<arg> è 0, i segnali out-of-paper, offline ed error devono essere falsi "
"durante tutte le operazioni di scrittura, altrimenti vengono ignorati. Il "
"default è di ignorarli."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPWAIT, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPWAIT, int >argB<)>"

#.  FIXME . Actually, since Linux 2.2, the default is 1
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sets the number of busy waiting iterations to wait before strobing the "
"printer to accept a just-written character, and the number of iterations to "
"wait before turning the strobe off again, to I<arg>.  The specification says "
"this time should be 0.5 microseconds, but experience has shown the delay "
"caused by the code is already enough.  For that reason, the default value is "
"0.  This is used for both the polling and the interrupt driver."
msgstr ""
"Assegna ad I<arg> il numero di iterazioni in busy-wait da aspettare prima di "
"abilitare (strobe) la stampante ad accettare il carattere appena scritto, e "
"il numero di iterazioni da attendere prima di togliere ancora "
"l'abilitazione. Le specifiche dicono che questo tempo dovrebbe essere di 0.5 "
"microsecondi, ma l'esperienza ha mostrato che il ritardo causato dal codice "
"è già sufficiente. Per questa ragione, il valore di default è 0. Questi "
"valori vengono usati sia per la modalità interrogazione che quella "
"coninterrupt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPSETIRQ, int >argB<)>"
msgstr "B<int ioctl(int >fdB<, LPSETIRQ, int >argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This B<ioctl>(2)  requires superuser privileges.  It takes an I<int> "
"containing the new IRQ as argument.  As a side effect, the printer will be "
"reset.  When I<arg> is 0, the polling driver will be used, which is also "
"default."
msgstr ""
"Questa B<ioctl>(2) richiede i privilegi del superutente. Prende come "
"argomento un I<intero> contenente il nuovo numero di IRQ. Come effetto "
"collaterale, la stampante sarà reimpostata. Quando I<arg> è 0, questa "
"chiamata seleziona il driver di tipo \"polling\", che è anche quello di "
"default."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPGETIRQ, int *>argB<)>"
msgstr "B<int ioctl(int >fdB<, LPGETIRQ, int *>argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Stores the currently used IRQ in I<arg>."
msgstr "Salva l'IRQ correntemente usato in I<arg>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPGETSTATUS, int *>argB<)>"
msgstr "B<int ioctl(int >fdB<, LPGETSTATUS, int *>argB<)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Stores the value of the status port in I<arg>.  The bits have the following "
"meaning:"
msgstr ""
"Salva il valore della porta di stato della stampante in I<arg>. I bit hanno "
"i seguenti significati:"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LP_PBUSY"
msgstr "LP_PBUSY"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "inverted busy input, active high"
msgstr "input busy invertito, attivo alto"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LP_PACK"
msgstr "LP_PACK"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unchanged acknowledge input, active low"
msgstr "input acknowledge invariato, attivo basso"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LP_POUTPA"
msgstr "LP_POUTPA"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unchanged out-of-paper input, active high"
msgstr "input \"out-of-paper\" invariato, attivo alto"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LP_PSELECD"
msgstr "LP_PSELECD"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unchanged selected input, active high"
msgstr "input \"selected\" invariato, attivo alto"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LP_PERRORP"
msgstr "LP_PERRORP"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "unchanged error input, active low"
msgstr "input \"error\" invariato, attivo basso"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Refer to your printer manual for the meaning of the signals.  Note that "
"undocumented bits may also be set, depending on your printer."
msgstr ""
"Si faccia riferimento al manuale della propria stampante per ilsignificato "
"dei segnali.  Si noti che possono essere impostati anche ibit non "
"documentati, ma questo dipende dalla stampante usata."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int ioctl(int >fdB<, LPRESET)>"
msgstr "B<int ioctl(int >fdB<, LPRESET)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Resets the printer.  No argument is used."
msgstr "Reimposta la stampante. Non è necessario alcun argomento."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FILE"

#.  .SH AUTHORS
#.  The printer driver was originally written by Jim Weigand and Linus
#.  Torvalds.
#.  It was further improved by Michael K.\& Johnson.
#.  The interrupt code was written by Nigel Gamble.
#.  Alan Cox modularized it.
#.  LPCAREFUL, LPABORT, LPGETSTATUS were added by Chris Metcalf.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I</dev/lp*>"
msgstr "I</dev/lp*>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<chmod>(1), B<chown>(1), B<mknod>(1), B<lpcntl>(8), B<tunelp>(8)"
msgstr "B<chmod>(1), B<chown>(1), B<mknod>(1), B<lpcntl>(8), B<tunelp>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
