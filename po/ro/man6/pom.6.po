# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:06+0100\n"
"PO-Revision-Date: 2024-02-06 12:42+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Dd
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "January 9, 1999"
msgstr "9 ianuarie 1999"

#. type: Dt
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "POM 6"
msgstr "POM 6"

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Nm pom>"
msgstr "E<.Nm pom>"

#. type: Nd
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "display the phase of the moon"
msgstr "afișează faza lunii"

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Nm> E<.Op [[[[[cc]yy]mm]dd]HH]>"
msgstr "E<.Nm> E<.Op [[[[[secolsecol]aa]ll]zz]HH]>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The E<.Nm> utility displays the current phase of the moon.  Useful for "
"selecting software completion target dates and predicting managerial "
"behavior."
msgstr ""
"Instrumentul E<.Nm> afișează faza actuală a lunii.  Util pentru selectarea "
"datelor țintă de finalizare a software-ului și pentru a prezice "
"comportamentul managerial."

#. type: It
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Ar [[[[[cc]yy]mm]dd]HH]"
msgstr "Ar [[[[[secolsecol]aa]ll]zz]HH]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Display the phase of the moon for a given time.  The format is similar to "
"the canonical representation used by E<.Xr date 1>."
msgstr ""
"Afișează faza lunii pentru o anumită dată și oră. Formatul este similar cu "
"reprezentarea canonică utilizată de E<.Xr data 1>."

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Xr date 1>"
msgstr "E<.Xr date 1>"

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Nm> was written by E<.An Keith E. Brandt>."
msgstr "E<.Nm> a fost scris de E<.An Keith E. Brandt>."

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Times must be within range of the E<.Ux> epoch."
msgstr "Datele trebuie să se încadreze în intervalul de timp al epocii E<.Ux>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This program does not allow for the difference between the TDT and UTC "
"timescales (about one minute at the time of writing)."
msgstr ""
"Acest program nu ține cont de diferența dintre scara de timp TDT și UTC "
"(aproximativ un minut la momentul scrierii acestei pagini)."

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ACKNOWLEDGEMENTS"
msgstr "MULȚUMIRI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This program is based on algorithms from E<.%B Practical Astronomy with Your "
"Calculator, Third Edition> by Peter Duffett-Smith E<.Aq pjds@mrao.cam.ac.uk>."
msgstr ""
"Acest program se bazează pe algoritmii din E<.%B Practical Astronomy with "
"Your Calculator, Third Edition> de Peter Duffett-Smith E<.Aq pjds@mrao.cam."
"ac.uk>."
