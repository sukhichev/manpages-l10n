# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2023-11-05 09:37+0100\n"
"PO-Revision-Date: 2023-11-04 20:40+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "mkfs.reiser4"
msgstr "mkfs.reiser4"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "02 Oct, 2002"
msgstr "2 octombrie 2002"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "reiser4progs"
msgstr "reiser4progs"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "reiser4progs manual"
msgstr "manualul reiser4progs"

#.  Please adjust this date whenever revising the manpage.
#.  Some roff macros, for reference:
#.  .nh        disable hyphenation
#.  .hy        enable hyphenation
#.  .ad l      left justify
#.  .ad b      justify to both left and right margins
#.  .nf        disable filling
#.  .fi        enable filling
#.  .br        insert line break
#.  .sp <n>    insert n+1 empty lines
#.  for manpage-specific macros, see man(7)
#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "mkfs.reiser4 - the program for creating reiser4 filesystem."
msgstr "mkfs.reiser4 - programul pentru crearea sistemului de fișiere reiser4."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "B<mkfs.reiser4> [ options ] FILE1 FILE2 ... [ size[K|M|G] ]"
msgstr "B<mkfs.reiser4> [ opțiuni ] FIȘIER1 FIȘIER2 ... [ dimensiune[K|M|G] ]"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"B<mkfs.reiser4> is reiser4 filesystem creation program. It is based on new "
"libreiser4 library. Since libreiser4 is fully plugin-based, we have the "
"potential to create not just reiser4 partitions, but any filesystem or "
"database format, which is based on balanced trees."
msgstr ""
"B<mkfs.reiser4> este programul de creare a sistemului de fișiere reiser4. "
"Acesta se bazează pe noua bibliotecă libreiser4. Deoarece libreiser4 se "
"bazează în totalitate pe module, avem potențialul de a crea nu doar partiții "
"reiser4, ci orice format de sistem de fișiere sau de bază de date, care se "
"bazează pe arbori echilibrați."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COMMON OPTIONS"
msgstr "OPȚIUNI COMUNE"

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-V, --version>"
msgstr "B<-V, --version>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "prints program version."
msgstr "afișează versiunea programului."

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-?, -h, --help>"
msgstr "B<-?, -h, --help>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "prints program help."
msgstr "afișează ajutorul programului."

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-y, --yes>"
msgstr "B<-y, --yes>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "assumes an answer 'yes' to all questions."
msgstr "consideră un răspuns „da” la toate întrebările."

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-f, --force>"
msgstr "B<-f, --force>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "forces mkfs to use whole disk, not block device or mounted partition."
msgstr ""
"forțează «mkfs» să utilizeze întregul disc, nu dispozitivul bloc sau "
"partiția montată."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "MKFS OPTIONS"
msgstr "OPȚIUNI MKFS"

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-b, --block-size N>"
msgstr "B<-b, --block-size N>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "block size to be used (architecture page size by default)"
msgstr ""
"dimensiunea blocului care urmează să fie utilizată (dimensiunea paginii de "
"arhitectură în mod implicit)"

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-L, --label LABEL>"
msgstr "B<-L, --label ETICHETA>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "volume label to be used"
msgstr "eticheta de volum care urmează să fie utilizată"

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-U, --uuid UUID>"
msgstr "B<-U, --uuid UUID>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "universally unique identifier to be used"
msgstr "identificatorul unic universal care urmează să fie utilizat"

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-s, --lost-found>"
msgstr "B<-s, --lost-found>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "forces mkfs to create lost+found directory."
msgstr "forțează «mkfs» să creeze directorul „lost+found”."

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-d, --discard>"
msgstr "B<-d, --discard>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"tells mkfs to discard given device before creating the filesystem (for solid "
"state drives)."
msgstr ""
"îi indică lui «mkfs» să renunțe la un anumit dispozitiv înainte de a crea "
"sistemul de fișiere (pentru unitățile de stocare solidă)."

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "PLUGIN OPTIONS"
msgstr "OPȚIUNI MODUL"

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-p, --print-profile>"
msgstr "B<-p, --print-profile>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"prints the plugin profile. This is the set of default plugins used for all "
"parts of a filesystem -- format, nodes, files, directories, hashes, etc. If "
"--override is specified, then prints modified plugins."
msgstr ""
"afișează profilul modulului. Acesta este setul de module implicite utilizate "
"pentru toate părțile unui sistem de fișiere - format, noduri, fișiere, "
"directoare, sume de control, etc. Dacă se specifică „--override”, atunci se "
"afișează modulele modificate."

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-l, --print-plugins>"
msgstr "B<-l, --print-plugins>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "prints all plugins libreiser4 know about."
msgstr "afișează toate modulele pe care „libreiser4” le cunoaște."

#. type: TP
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-o, --override TYPE=PLUGIN, ...>"
msgstr "B<-o, --override TYPE=MODUL, ...>"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"overrides the default plugin of the type \"TYPE\" by the plugin \"PLUGIN\" "
"in the plugin profile."
msgstr ""
"înlocuiește modulul implicit de tipul „TIP” cu modulul „MODUL” din profilul "
"modulului."

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "Examples:"
msgstr "Exemple:"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid ""
"assign short key plugin to \"key\" field in order to create filesystem with "
"short keys policy:"
msgstr ""
"atribuie modulul „short key” (de chei scurte) la câmpul „key” pentru a crea "
"un sistem de fișiere cu o politică de chei scurte:"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "mkfs.reiser4 -yf -o key=key_short /dev/hda2"
msgstr "mkfs.reiser4 -yf -o key=key_short /dev/hda2"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "Report bugs to E<lt>reiserfs-devel@vger.kernel.orgE<gt>"
msgstr "Raportați erorile la E<.MT reiserfs-devel@vger.kernel.org>E<.ME .>"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "B<measurefs.reiser4(8),> B<debugfs.reiser4(8),> B<fsck.reiser4(8)>"
msgstr "B<measurefs.reiser4(8),> B<debugfs.reiser4(8),> B<fsck.reiser4(8)>"

#. type: SH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
msgid "This manual page was written by Yury Umanets E<lt>umka@namesys.comE<gt>"
msgstr ""
"Această pagină de manual a fost scrisă de Yury Umanets E<lt>umka@namesys."
"comE<gt>"
