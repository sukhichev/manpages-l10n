# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2023-08-27 16:54+0200\n"
"PO-Revision-Date: 2023-11-04 15:38+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DEBUGREISERFS"
msgstr "DEBUGREISERFS"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "January 2009"
msgstr "ianuarie 2009"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "Reiserfsprogs 3.6.27"
msgstr "Reiserfsprogs 3.6.27"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "debugreiserfs - The debugging tool for the ReiserFS filesystem."
msgstr ""
"debugreiserfs - instrument de depanare pentru sistemul de fișiere ReiserFS."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<debugreiserfs> [ B<-dDJmoqpuSV> ] [ B<-j >I<device> ] [ B<-B >I<file> ] "
"[ B<-1 >I<N> ]"
msgstr ""
"B<debugreiserfs> [ B<-dDJmoqpuSV> ] [ B<-j >I<dispozitiv> ] [ B<-B "
">I<fișier> ] [ B<-1 >I<N> ]"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "I<device>"
msgstr "I<dispozitiv>"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<debugreiserfs> sometimes helps to solve problems with reiserfs "
"filesystems.  When run without options it prints the super block of the "
"ReiserFS filesystem found on the I<device>."
msgstr ""
"B<debugreiserfs> ajută uneori la rezolvarea problemelor cu sistemele de "
"fișiere reiserfs.  Atunci când este rulat fără opțiuni, imprimă superblocul "
"sistemului de fișiere ReiserFS găsit pe I<dispozitiv>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"is the special file corresponding to the device (e.g /dev/hdXX for an IDE "
"disk partition or /dev/sdXX for a SCSI disk partition)."
msgstr ""
"este fișierul special care corespunde dispozitivului (de exemplu, /dev/hdXX "
"pentru o partiție de disc IDE sau /dev/sdXX pentru o partiție de disc SCSI)."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-j> I<device>"
msgstr "B<-j> I<dispozitiv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"prints the contents of the journal. The option -p allows it to pack the "
"journal with other metadata into the archive."
msgstr ""
"afișează conținutul jurnalului. Opțiunea „-p” permite împachetarea "
"jurnalului cu alte metadate în arhivă."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-J>"
msgstr "B<-J>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "prints the journal header."
msgstr "afișează antetul jurnalului."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "prints the formatted nodes of the internal tree of the filesystem."
msgstr ""
"afișează nodurile formatate ale arborelui intern al sistemului de fișiere."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-D>"
msgstr "B<-D>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "prints the formatted nodes of all used blocks of the filesystem."
msgstr ""
"afișează nodurile formatate ale tuturor blocurilor utilizate din sistemul de "
"fișiere."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "prints the contents of the bitmap (slightly useful)."
msgstr "afișează conținutul hărții de biți (puțin util)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-o>"
msgstr "B<-o>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "prints the objectid map (slightly useful)."
msgstr "afișează harta identificatorilor de obiecte (puțin utilă)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-B> I<file>"
msgstr "B<-B> I<fișier>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"takes the list of bad blocks stored in the internal ReiserFS tree and "
"translates it into an ascii list written to the specified file."
msgstr ""
"preia lista de blocuri defecte stocate în arborele intern ReiserFS și o "
"traduce într-o listă ascii scrisă în fișierul specificat."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-1> I<blocknumber>"
msgstr "B<-1> I<număr-bloc>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "prints the specified block of the filesystem."
msgstr "afișează blocul specificat din sistemul de fișiere."

#.  \fB-s
#.  scans the partition and prints a line when any kind of reiserfs
#.  formatted nodes found. Can be used to find specific key in the filesystem.
#.  .TP
#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"extracts the filesystem's metadata with B<debugreiserfs> -p /dev/xxx | gzip -"
"c E<gt> xxx.gz. None of your data are packed unless a filesystem corruption "
"presents when the whole block having this corruption is packed. You send us "
"the output, and we use it to create a filesystem with the same strucure as "
"yours using B<debugreiserfs -u>.  When the data file is not too large, this "
"usually allows us to quickly reproduce and debug the problem."
msgstr ""
"extrage metadatele sistemului de fișiere cu «B<debugreiserfs> -p /dev/xxx | "
"gzip -c E<gt> xxx.gz». Niciuna dintre datele dvs. nu este împachetată, cu "
"excepția cazului în care se prezintă o corupție a sistemului de fișiere, "
"când întregul bloc care are această corupție este împachetat. Ne trimiteți "
"rezultatul, iar noi îl folosim pentru a crea un sistem de fișiere cu aceeași "
"structură ca a dumneavoastră folosind B<debugreiserfs -u>.  Atunci când "
"fișierul de date nu este prea mare, acest lucru ne permite, de obicei, să "
"reproducem și să depanăm rapid problema."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"builds the ReiserFS filesystem image with gunzip -c xxx.gz | "
"B<debugreiserfs> -u /dev/image of the previously packed metadata with "
"B<debugreiserfs -p>. The result image is not the same as the original "
"filesystem, because mostly only metadata were packed with B<debugreiserfs -"
"p>, but the filesystem structure is completely recreated."
msgstr ""
"construiește imaginea sistemului de fișiere ReiserFS cu gunzip -c xxx.gz | "
"B<debugreiserfs> -u /dev/image din metadatele împachetate anterior cu "
"B<debugreiserfs -p>. Imaginea rezultată nu este aceeași cu sistemul de "
"fișiere original, deoarece în mare parte doar metadatele au fost împachetate "
"cu B<debugreiserfs -p>, dar structura sistemului de fișiere este complet "
"recreată."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-S>"
msgstr "B<-S>"

#.  and -s 
#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"When -S is not specified -p deals with blocks marked used in the filesystem "
"bitmap only. With this option set B<debugreiserfs> will work with the entire "
"device."
msgstr ""
"Atunci când nu se specifică „-S”, „-p” se ocupă numai de blocurile marcate "
"utilizate în harta de biți a sistemului de fișiere. Cu această opțiune "
"setată B<debugreiserfs> va lucra cu întregul dispozitiv."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-q>"
msgstr "B<-q>"

#.  -s or 
#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "When -p is in use, suppress showing the speed of progress."
msgstr "Atunci când se utilizează „-p”, nu se afișează viteza de progres."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This version of B<debugreiserfs> has been written by Vitaly Fertman "
"E<lt>vitaly@namesys.comE<gt>."
msgstr ""
"Această versiune de B<debugreiserfs> a fost scrisă de Vitaly Fertman "
"E<lt>vitaly@namesys.comE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Please report bugs to the ReiserFS developers E<lt>reiserfs-devel@vger."
"kernel.orgE<gt>, providing as much information as possible--your hardware, "
"kernel, patches, settings, all printed messages; check the syslog file for "
"any related information."
msgstr ""
"Raportați erorile către dezvoltatorii ReiserFS E<lt>reiserfs-devel@vger."
"kernel.orgE<gt>, furnizând cât mai multe informații posibile - componentele "
"calculatorului, nucleul, corecțiile, configurările, toate mesajele "
"imprimate; verificați fișierul syslog pentru orice informații conexe."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<reiserfsck>(8), B<mkreiserfs>(8)"
msgstr "B<reiserfsck>(8), B<mkreiserfs>(8)"
