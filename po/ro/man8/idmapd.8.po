# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:01+0100\n"
"PO-Revision-Date: 2024-01-30 13:57+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Dd
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "February 3, 2003"
msgstr "3 februarie 2003"

#. type: Dt
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RPC.IDMAPD 8"
msgstr "RPC.IDMAPD 8"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Nm rpc.idmapd>"
msgstr "E<.Nm rpc.idmapd>"

#. type: Nd
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NFSv4 ID E<lt>-E<gt> Name Mapper"
msgstr "corelare NFSv4 ID E<lt>-E<gt> nume"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#.  For a program:  program [-abc] file ...
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"E<.Nm rpc.idmapd> E<.Op Fl h> E<.Op Fl f> E<.Op Fl v> E<.Op Fl C> E<.Op Fl "
"S> E<.Op Fl p Ar path> E<.Op Fl c Ar path>"
msgstr ""
"E<.Nm rpc.idmapd> E<.Op Fl h> E<.Op Fl f> E<.Op Fl v> E<.Op Fl C> E<.Op Fl "
"S> E<.Op Fl p Ar ruta> E<.Op Fl c Ar ruta>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"E<.Nm> is the NFSv4 ID E<lt>-E<gt> name mapping daemon.  It provides "
"functionality to the NFSv4 kernel client and server, to which it "
"communicates via upcalls, by translating user and group IDs to names, and "
"vice versa."
msgstr ""
"E<.Nm> este demonul de corelare NFSv4 ID E<lt>-E<gt> nume. Acesta oferă "
"funcționalitate pentru clientul și serverul nucleului NFSv4, cu care "
"comunică prin apeluri ascendente, prin traducerea ID-urilor de utilizator și "
"de grup în nume și invers."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The system derives the E<.Em user> part of the string by performing a "
"password or group lookup.  The lookup mechanism is configured in E<.Pa /etc/"
"idmapd.conf>"
msgstr ""
"Sistemul obține partea E<.Em utilizator> a șirului prin căutarea unei parole "
"sau a unui grup. Mecanismul de căutare este configurat în E<.Pa /etc/idmapd."
"conf>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"By default, the E<.Em domain> part of the string is the system's DNS domain "
"name.  It can also be specified in E<.Pa /etc/idmapd.conf> if the system is "
"multi-homed, or if the system's DNS domain name does not match the name of "
"the system's Kerberos realm."
msgstr ""
"În mod implicit, partea E<.Em domeniu> a șirului este numele de domeniu DNS "
"al sistemului. De asemenea, poate fi specificată în E<.Pa /etc/idmapd.conf> "
"dacă sistemul este multi-conexiune „multi-homed” (conectat la două sau mai "
"multe rețele) sau dacă numele de domeniu DNS al sistemului nu se potrivește "
"cu numele domeniului Kerberos al sistemului."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When the domain is not specified in /etc/idmapd.conf the local DNS server "
"will be queried for the E<.Sy _nfsv4idmapdomain > text record. If the record "
"exists that will be used as the domain. When the record does not exist, the "
"domain part of the DNS domain will used."
msgstr ""
"Când domeniul nu este specificat în /etc/idmapd.conf, serverul DNS local va "
"fi interogat pentru înregistrarea text E<.Sy _nfsv4idmapdomain >. În cazul "
"în care înregistrarea există, aceasta va fi utilizată ca domeniu. În cazul "
"în care înregistrarea nu există, se va utiliza partea de domeniu din "
"domeniul DNS."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that on more recent kernels only the NFSv4 server uses E<.Nm>.  The "
"NFSv4 client instead uses E<.Xr nfsidmap 8>, and only falls back to E<.Nm > "
"if there was a problem running the E<.Xr nfsidmap 8> program."
msgstr ""
"Rețineți că pe nucleele mai recente numai serverul NFSv4 utilizează E<.Nm>. "
"În schimb, clientul NFSv4 utilizează E<.Xr nfsidmap 8> și revine la E<.Nm > "
"numai dacă a existat o problemă la rularea programului E<.Xr nfsidmap 8>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The options are as follows:"
msgstr "Opțiunile sunt următoarele:"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl h"
msgstr "Fl h"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Display usage message."
msgstr "Afișează mesajul de utilizare."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl v"
msgstr "Fl v"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Increases the verbosity level (can be specified multiple times)."
msgstr ""
"Mărește nivelul de detaliere al mesajelor informative de la ieșire (poate fi "
"specificată de mai multe ori)."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl f"
msgstr "Fl f"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Runs E<.Nm> in the foreground and prints all output to the terminal."
msgstr "Rulează E<.Nm> în prim-plan și afișează toată ieșirea în terminal."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl p Ar path"
msgstr "Fl p Ar ruta"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Specifies the location of the RPC pipefs to be E<.Ar path>.  The default "
"value is \\&\"/var/lib/nfs/rpc_pipefs\\&\"."
msgstr ""
"Specifică locația conductelor RPC care urmează să fie E<.Ar ruta>. Valoarea "
"implicită este \\&„/var/lib/nfs/rpc_pipefs\\&”."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl c Ar path"
msgstr "Fl c Ar ruta"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Use configuration file E<.Ar path>.  This option is deprecated."
msgstr ""
"Utilizează fișierul de configurare E<.Ar ruta>. Această opțiune este "
"depreciată."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl C"
msgstr "Fl C"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Client-only: perform no idmapping for any NFS server, even if one is "
"detected."
msgstr ""
"Doar pentru client: nu se efectuează nicio corelare de identificare pentru "
"niciun server NFS, chiar dacă este detectat vreunul."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Fl S"
msgstr "Fl S"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Server-only: perform no idmapping for any NFS client, even if one is "
"detected."
msgstr ""
"Doar pentru server: nu se efectuează nicio corelare de identificare pentru "
"niciun client NFS, chiar dacă este detectat vreunul."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION FILES"
msgstr "FIȘIERELE DE CONFIGURARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"E<.Nm> recognizes the following value from the E<.Sy [general]> section of "
"the E<.Pa /etc/nfs.conf> configuration file:"
msgstr ""
"E<.Nm> recunoaște următoarea valoare din secțiunea E<.Sy [general]> a "
"fișierului de configurare E<.Pa /etc/nfs.conf>:"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Sy pipefs-directory"
msgstr "Sy pipefs-directory"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Equivalent to E<.Sy -p>."
msgstr "Echivalentă cu E<.Sy -p>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"All other settings related to id mapping are found in the E<.Pa /etc/idmapd."
"conf> configuration file."
msgstr ""
"Toate celelalte valori legate de corelarea id se găsesc în fișierul de "
"configurare E<.Pa /etc/idmapd.conf>."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Cm rpc.idmapd -f -vvv>"
msgstr "E<.Cm rpc.idmapd -f -vvv>"

#.  This next request is for sections 2 and 3 function return values only.
#.  .Sh RETURN VALUES
#.  The next request is for sections 2 and 3 error and signal handling only.
#.  .Sh ERRORS
#.  This next request is for section 4 only.
#.  .Sh DIAGNOSTICS
#.  This next request is for sections 1, 6, 7 & 8 only.
#.  .Sh ENVIRONMENT
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Runs E<.Nm> printing all messages to console, and with a verbosity level of "
"3."
msgstr ""
"Rulează E<.Nm> afișând toate mesajele în consolă și cu un nivel de detaliere "
"al informațiilor de 3."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "E<.Pa /etc/idmapd.conf>, E<.Pa /etc/nfs.conf>"
msgstr "E<.Pa /etc/idmapd.conf>, E<.Pa /etc/nfs.conf>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. .Sh SEE ALSO
#. .Xr nylon.conf 4
#.  .Sh COMPATIBILITY
#. .Sh STANDARDS
#. .Sh ACKNOWLEDGEMENTS
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "E<.Xr idmapd.conf 5>, E<.Xr nfs.conf 5>, E<.Xr nfsidmap 8>"
msgstr "E<.Xr idmapd.conf 5>, E<.Xr nfs.conf 5>, E<.Xr nfsidmap 8>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The E<.Nm> software has been developed by Marius Aamodt Eriksen E<.Aq "
"marius@citi.umich.edu>."
msgstr ""
"Software-ul E<.Nm> a fost dezvoltat de Marius Aamodt Eriksen E<.Aq "
"marius@citi.umich.edu>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The system derives the I<user> part of the string by performing a password "
"or group lookup.  The lookup mechanism is configured in E<.Pa /etc/idmapd."
"conf>"
msgstr ""
"Sistemul obține partea I<utilizator> a șirului prin căutarea unei parole sau "
"a unui grup. Mecanismul de căutare este configurat în E<.Pa /etc/idmapd."
"conf>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"By default, the I<domain> part of the string is the system's DNS domain "
"name.  It can also be specified in E<.Pa /etc/idmapd.conf> if the system is "
"multi-homed, or if the system's DNS domain name does not match the name of "
"the system's Kerberos realm."
msgstr ""
"În mod implicit, partea I<domeniu> a șirului este numele de domeniu DNS al "
"sistemului. De asemenea, poate fi specificată în E<.Pa /etc/idmapd.conf> "
"dacă sistemul este multi-conexiune „multi-homed” (conectat la două sau mai "
"multe rețele) sau dacă numele de domeniu DNS al sistemului nu se potrivește "
"cu numele domeniului Kerberos al sistemului."

#. type: Plain text
#: opensuse-leap-15-6
msgid "Use configuration file E<.Ar path>."
msgstr "Utilizează fișierul de configurare E<.Ar ruta>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "E<.Pa /etc/idmapd.conf>"
msgstr "E<.Pa /etc/idmapd.conf>"

#. .Sh SEE ALSO
#. .Xr nylon.conf 4
#.  .Sh COMPATIBILITY
#. .Sh STANDARDS
#. .Sh ACKNOWLEDGEMENTS
#. type: Plain text
#: opensuse-leap-15-6
msgid "E<.Xr idmapd.conf 5>, E<.Xr nfsidmap 8>"
msgstr "E<.Xr idmapd.conf 5>, E<.Xr nfsidmap 8>"
