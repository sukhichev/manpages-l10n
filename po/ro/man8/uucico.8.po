# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:15+0100\n"
"PO-Revision-Date: 2024-03-09 20:49+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "uucico"
msgstr "uucico"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Taylor UUCP 1.07"
msgstr "Taylor UUCP 1.07"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "uucico - UUCP file transfer daemon"
msgstr "uucico - demon de transfer de fișiere UUCP"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<uucico> [ options ]"
msgstr "B<uucico> [ opțiuni ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<uucico> daemon processes file transfer requests queued by I<uucp> (1) "
"and I<uux> (1).  It is started when I<uucp> or I<uux> is run (unless they "
"are given the B<-r> option).  It is also typically started periodically "
"using entries in the I<crontab> table(s)."
msgstr ""
"Demonul I<uucico> procesează cererile de transfer de fișiere puse în coadă "
"de I<uucp> (1) și I<uux> (1). Acesta este pornit atunci când este rulat "
"I<uucp> sau I<uux> (cu excepția cazului în care acestea primesc opțiunea B<-"
"r>). De asemenea, este de obicei pornit periodic, folosind intrările din "
"tabelele I<crontab>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When invoked with B<-r1,> B<--master,> B<-s,> B<--system,> or B<-S,> the "
"daemon will place a call to a remote system, running in master mode.  "
"Otherwise the daemon will start in slave mode, accepting a call from a "
"remote system.  Typically a special login name will be set up for UUCP which "
"automatically invokes I<uucico> when a call is made."
msgstr ""
"Atunci când este invocat cu B<-r1,> B<--master,> B<-s,> B<--system,> sau B<-"
"S,>, demonul va efectua un apel către un sistem la distanță, care rulează în "
"modul master. În caz contrar, demonul va porni în modul sclav, acceptând un "
"apel de la un sistem de la distanță. De obicei, se va configura un nume de "
"utilizator special pentru UUCP, care va invoca automat I<uucico> atunci când "
"se efectuează un apel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When I<uucico> terminates, it invokes the I<uuxqt> (8) daemon, unless the B<-"
"q> or B<--nouuxqt> option is given; I<uuxqt> (8) executes any work orders "
"created by I<uux> (1) on a remote system, and any work orders created "
"locally which have received remote files for which they were waiting."
msgstr ""
"Când I<uucico> se termină, invocă demonul I<uuxqt> (8), cu excepția cazului "
"în care se dă opțiunea B<-q> sau B<--nouuxqt>; I<uuxqt> (8) execută orice "
"ordine de lucru create de I<uux> (1) pe un sistem la distanță și orice "
"ordine de lucru create local care au primit fișiere de la distanță pe care "
"le așteptau."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"If a call fails, I<uucico> will normally refuse to retry the call until a "
"certain (configurable) amount of time has passed.  This may be overriden by "
"the B<-f,> B<--force,> or B<-S> option."
msgstr ""
"Dacă un apel eșuează, I<uucico> va refuza în mod normal să reia apelul până "
"când trece o anumită perioadă de timp (configurabilă). Acest lucru poate fi "
"anulat prin opțiunea B<-f,> B<--force,> sau B<-S>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<-l,> B<--prompt,> B<-e,> or B<--loop> options may be used to force "
"I<uucico> to produce its own prompts of \"login: \" and \"Password:\".  When "
"another daemon calls in, it will see these prompts and log in as usual.  The "
"login name and password will normally be checked against a separate list "
"kept specially for I<uucico> rather than the I</etc/passwd> file; it is "
"possible on some systems to direct I<uucico> to use the I</etc/passwd> "
"file.  The B<-l> or B<--prompt> option will prompt once and then exit; in "
"this mode the UUCP administrator or the superuser may use the B<-u> or B<--"
"login> option to force a login name, in which case I<uucico> will not prompt "
"for one.  The B<-e> or B<--loop> option will prompt again after the first "
"session is over; in this mode I<uucico> will permanently control a port."
msgstr ""
"Opțiunile B<-l,> B<--prompt,> B<-e,> sau B<--loop> pot fi folosite pentru a "
"forța I<uucico> să producă propriile prompts de tipul „utilizator:” și "
"„Parola:”. Atunci când un alt demon apelează, acesta va vedea aceste "
"solicitări și se va autentifica ca de obicei. În mod normal, numele de "
"utilizator și parola de conectare vor fi verificate în raport cu o listă "
"separată păstrată special pentru I<uucico>, mai degrabă decât cu fișierul I</"
"etc/passwd>; este posibil ca pe unele sisteme să se direcționeze I<uucico> "
"pentru a utiliza fișierul I</etc/passwd>. Opțiunea B<-l> sau B<--prompt> va "
"solicita o singură dată și apoi va ieși; în acest mod, administratorul UUCP "
"sau superutilizatorul poate utiliza opțiunea B<-u> sau B<--login> pentru a "
"forța un nume de utilizator, caz în care I<uucico> nu va solicita unul. "
"Opțiunea B<-e> sau B<--loop> va solicita din nou după terminarea primei "
"sesiuni; în acest mod, I<uucico> va controla permanent un port."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<uucico> receives a SIGQUIT, SIGTERM or SIGPIPE signal, it will cleanly "
"abort any current conversation with a remote system and exit.  If it "
"receives a SIGHUP signal it will abort any current conversation, but will "
"continue to place calls to (if invoked with B<-r1> or B<--master)> and "
"accept calls from (if invoked with B<-e> or B<--loop)> other systems.  If it "
"receives a SIGINT signal it will finish the current conversation, but will "
"not place or accept any more calls."
msgstr ""
"Dacă I<uucico> primește un semnal SIGQUIT, SIGTERM sau SIGPIPE, va întrerupe "
"în mod clar orice conversație curentă cu un sistem la distanță și va ieși. "
"Dacă primește un semnal SIGHUP, va întrerupe orice conversație curentă, dar "
"va continua să efectueze apeluri către (dacă este invocat cu B<-r1> sau B<--"
"master>) și să accepte apeluri de la (dacă este invocat cu B<-e> sau B<--"
"loop>) alte sisteme. În cazul în care primește un semnal SIGINT, va încheia "
"conversația curentă, dar nu va mai efectua sau accepta alte apeluri."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following options may be given to I<uucico.>"
msgstr "Următoarele opțiuni pot fi date lui I<uucico>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r1, --master>"
msgstr "B<-r1, --master>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Start in master mode (call out to a system); implied by B<-s,> B<--system,> "
"or B<-S.> If no system is specified, call any system for which work is "
"waiting to be done."
msgstr ""
"Pornește în modul master (apel către un sistem); implicită prin B<-s,> B<--"
"sistem,> sau B<-S>. Dacă nu este specificat nici un sistem, apelează orice "
"sistem pentru care se așteaptă să se lucreze."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r0, --slave>"
msgstr "B<-r0, --slave>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Start in slave mode.  This is the default."
msgstr "Pornește în modul sclav. Acesta este modul implicit."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-s system, --system system>"
msgstr "B<-s sistem, --system sistem>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Call the named system."
msgstr "Apelează sistemul numit."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-S system>"
msgstr "B<-S sistem>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Call the named system, ignoring any required wait.  This is equivalent to B<-"
"s system -f.>"
msgstr ""
"Apelează sistemul numit, ignorând orice așteptare necesară. Acest lucru este "
"echivalent cu B<-s sistem -f>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-f, --force>"
msgstr "B<-f, --force>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Ignore any required wait for any systems to be called."
msgstr "Ignoră orice așteptare necesară pentru apelarea oricărui sistem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l, --prompt>"
msgstr "B<-l, --prompt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Prompt for login name and password using \"login: \" and \"Password:\".  "
"This allows I<uucico> to be easily run from I<inetd> (8).  The login name "
"and password are checked against the UUCP password file, which probably has "
"no connection to the file I</etc/passwd.> The B<--login> option may be used "
"to force a login name, in which cause I<uucico> will only prompt for a "
"password."
msgstr ""
"Solicită numele de utilizator și parola de conectare folosind „utilizator:” "
"și „Parola:”. Acest lucru permite ca I<uucico> să fie rulat cu ușurință din "
"I<inetd> (8). Numele de utilizator și parola sunt verificate în funcție de "
"fișierul de parole UUCP, care probabil nu are nicio legătură cu fișierul I</"
"etc/passwd>. Opțiunea B<--login> poate fi utilizată pentru a forța un nume "
"de utilizator, caz în care I<uucico> va solicita doar o parolă."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p port, --port port>"
msgstr "B<-p port, --port port>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Specify a port to call out on or to listen to."
msgstr "Specifică un port de apelare sau de ascultare."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-e, --loop>"
msgstr "B<-e, --loop>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Enter endless loop of login/password prompts and slave mode daemon "
"execution.  The program will not stop by itself; you must use I<kill> (1) to "
"shut it down."
msgstr ""
"Introduce o buclă nesfârșită de solicitări de utilizator/parolă și de "
"execuție a demonului în modul sclav. Programul nu se va opri de la sine; "
"trebuie să utilizați I<kill> (1) pentru a-l opri."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w, --wait>"
msgstr "B<-w, --wait>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"After calling out (to a particular system when B<-s,> B<--system,> or B<-S> "
"is specifed, or to all systems which have work when just B<-r1> or B<--"
"master> is specifed), begin an endless loop as with B<--loop.>"
msgstr ""
"După apelarea (către un anumit sistem atunci când este specificat B<-s,> B<--"
"sistem,> sau B<-S>, sau către toate sistemele care au de lucru atunci când "
"este specificat doar B<-r1> sau B<--master>), începe o buclă nesfârșită ca "
"și cu B<--loop>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-q, --nouuxqt>"
msgstr "B<-q, --nouuxqt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Do not start the I<uuxqt> (8) daemon when finished."
msgstr "Nu lansează demonul I<uuxqt> (8) când a terminat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c, --quiet>"
msgstr "B<-c, --quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If no calls are permitted at this time, then don't make the call, but also "
"do not put an error message in the log file and do not update the system "
"status (as reported by I<uustat> (1)).  This can be convenient for automated "
"polling scripts, which may want to simply attempt to call every system "
"rather than worry about which particular systems may be called at the "
"moment.  This option also suppresses the log message indicating that there "
"is no work to be done."
msgstr ""
"Dacă nu sunt permise apeluri în acest moment, atunci nu efectuează apelul, "
"dar nici nu introduce un mesaj de eroare în fișierul jurnal și nu "
"actualizează starea sistemului (așa cum a raportat I<uustat> (1)). Acest "
"lucru poate fi convenabil pentru scripturile automate de interogare, care ar "
"putea dori să încerce pur și simplu să apeleze fiecare sistem, mai degrabă "
"decât să își facă griji cu privire la sistemele particulare care pot fi "
"apelate în acest moment. Această opțiune suprimă, de asemenea, mesajul de "
"jurnal care indică faptul că nu există nicio activitate de efectuat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C, --ifwork>"
msgstr "B<-C, --ifwork>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Only call the system named by B<-s,> B<--system> or B<-S> if there is work "
"for that system."
msgstr ""
"Apelează sistemul numit de B<-s,> B<--system> sau B<-S> numai dacă există "
"lucrări pentru acel sistem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-D, --nodetach>"
msgstr "B<-D, --nodetach>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Do not detach from the controlling terminal.  Normally I<uucico> detaches "
"from the terminal before each call out to another system and before invoking "
"I<uuxqt.> This option prevents this."
msgstr ""
"Nu se detașează de terminalul de control. În mod normal, I<uucico> se "
"detașează de terminal înainte de fiecare apel către un alt sistem și înainte "
"de a invoca I<uuxqt>. Această opțiune previne acest lucru."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-u name, --login name>"
msgstr "B<-u nume, --login nume>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the login name to use instead of that of the invoking user.  This option "
"may only be used by the UUCP administrator or the superuser.  If used with "
"B<--prompt,> this will cause I<uucico> to prompt only for the password, not "
"the login name."
msgstr ""
"Stabilește numele de utilizator care trebuie utilizat în locul celui al "
"utilizatorului care face apelul. Această opțiune poate fi utilizată numai de "
"către administratorul UUCP sau superutilizator. Dacă este utilizată împreună "
"cu B<--prompt,> aceasta va face ca I<uucico> să ceară doar parola, nu și "
"numele de utilizator."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z, --try-next>"
msgstr "B<-z, --try-next>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a call fails after the remote system is reached, try the next alternate "
"rather than simply exiting."
msgstr ""
"Dacă un apel eșuează după ce s-a ajuns la sistemul de la distanță, încearcă "
"următoarea variantă alternativă în loc să iasă pur și simplu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-i type, --stdin type>"
msgstr "B<-i tip, --stdin tip>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the type of port to use when using standard input.  The only support "
"port type is TLI, and this is only available on machines which support the "
"TLI networking interface.  Specifying B<-iTLI> causes I<uucico> to use TLI "
"calls to perform I/O."
msgstr ""
"Stabilește tipul de port care trebuie utilizat atunci când se utilizează "
"intrarea standard. Singurul tip de port acceptat este TLI, iar acesta este "
"disponibil numai pe mașinile care acceptă interfața de rețea TLI. "
"Specificarea B<-iTLI> face ca I<uucico> să utilizeze apeluri TLI pentru a "
"efectua operațiile de In/Ieș."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x type, -X type, --debug type>"
msgstr "B<-x tip, -X tip, --debug tip>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Turn on particular debugging types.  The following types are recognized: "
"abnormal, chat, handshake, uucp-proto, proto, port, config, spooldir, "
"execute, incoming, outgoing."
msgstr ""
"Activează anumite tipuri de depanare. Sunt recunoscute următoarele tipuri: "
"abnormal, chat, handshake, uucp-proto, proto, port, config, spooldir, "
"execute, incoming, outgoing."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Multiple types may be given, separated by commas, and the B<--debug> option "
"may appear multiple times.  A number may also be given, which will turn on "
"that many types from the foregoing list; for example, B<--debug 2> is "
"equivalent to B<--debug abnormal,chat.>"
msgstr ""
"Se pot indica mai multe tipuri, separate prin virgule, iar opțiunea B<--"
"debug> poate apărea de mai multe ori. De asemenea, se poate indica un număr, "
"care va activa atâtea tipuri din lista de mai sus; de exemplu, B<--debug 2> "
"este echivalent cu B<--debug abnormal,chat>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The debugging output is sent to the debugging file, which may be printed "
"using I<uulog -D.>"
msgstr ""
"Ieșirea de depanare este trimisă în fișierul de depanare, care poate fi "
"afișat folosind I<uulog -D.>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-I file, --config file>"
msgstr "B<-I fișier, --config fișier>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set configuration file to use.  This option may not be available, depending "
"upon how I<uucico> was compiled."
msgstr ""
"Stabilește fișierul de configurare care urmează să fie utilizat. Este "
"posibil ca această opțiune să nu fie disponibilă, în funcție de modul în "
"care a fost compilat I<uucico>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v, --version>"
msgstr "B<-v, --version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Report version information and exit."
msgstr "Comunică informațiile despre versiune și iese."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print a help message and exit."
msgstr "Imprimă un mesaj de ajutor și iese."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "kill(1), uucp(1), uux(1), uustat(1), uuxqt(8)"
msgstr "kill(1), uucp(1), uux(1), uustat(1), uuxqt(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Ian Lance Taylor E<lt>ian@airs.comE<gt>"
msgstr "Ian Lance Taylor E<lt>ian@airs.comE<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a call fails, I<uucico> will normally refuse to retry the call until a "
"certain (configurable) amount of time has passed.  This may be overridden by "
"the B<-f,> B<--force,> or B<-S> option."
msgstr ""
"În cazul în care un apel eșuează, I<uucico> va refuza în mod normal să reia "
"apelul până când trece o anumită perioadă de timp (configurabilă). Acest "
"lucru poate fi anulat prin opțiunea B<-f,> B<--force,> sau B<-S>."

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"After calling out (to a particular system when B<-s,> B<--system,> or B<-S> "
"is specified, or to all systems which have work when just B<-r1> or B<--"
"master> is specified), begin an endless loop as with B<--loop.>"
msgstr ""
"După apelarea (către un anumit sistem atunci când este specificat B<-s,> B<--"
"sistem,> sau B<-S>, sau către toate sistemele care au de lucru atunci când "
"este specificat doar B<-r1> sau B<--master>), începe o buclă nesfârșită ca "
"și cu B<--loop>."

#. type: TP
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-g, --grade>"
msgstr "B<-g, --grade>"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Limit outgoing call to a given grade."
msgstr "Limitează apelurile de ieșire la nivelul dat."
