# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-01 16:58+0100\n"
"PO-Revision-Date: 2024-02-17 13:54+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "iconvconfig"
msgstr "iconvconfig"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "iconvconfig - create iconv module configuration cache"
msgstr ""
"iconvconfig - creează un fișier de configurare cu încărcare rapidă pentru "
"modulele iconv"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<iconvconfig> [I<options>] [I<directory>]..."
msgstr "B<iconvconfig> [I<opțiuni>] [I<director>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<iconv>(3)  function internally uses I<gconv> modules to convert to and "
"from a character set.  A configuration file is used to determine the needed "
"modules for a conversion.  Loading and parsing such a configuration file "
"would slow down programs that use B<iconv>(3), so a caching mechanism is "
"employed."
msgstr ""
"Funcția B<iconv>(3) utilizează în mod intern modulele I<gconv> pentru a "
"converti la și de la un set de caractere.  Un fișier de configurare este "
"utilizat pentru a determina modulele necesare pentru o conversie.  "
"Încărcarea și analizarea unui astfel de fișier de configurare ar încetini "
"programele care utilizează B<iconv>(3), astfel încât se folosește un "
"mecanism de memorare într-un fișier de prestocare a datelor (cache)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<iconvconfig> program reads iconv module configuration files and writes "
"a fast-loading gconv module configuration cache file."
msgstr ""
"Programul B<iconvconfig> citește fișierele de configurare a modulelor iconv "
"și scrie un fișierului de prestocare a datelor de configurare a modulelor "
"gconv care se încarcă rapid."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In addition to the system provided gconv modules, the user can specify "
"custom gconv module directories with the environment variable "
"B<GCONV_PATH>.  However, iconv module configuration caching is used only "
"when the environment variable B<GCONV_PATH> is not set."
msgstr ""
"În plus față de modulele gconv furnizate de sistem, utilizatorul poate "
"specifica directoare personalizate pentru modulele gconv cu ajutorul "
"variabilei de mediu B<GCONV_PATH>.  Cu toate acestea, fișierului de "
"prestocare a datelor de configurare a modulelor gconv este utilizat numai "
"atunci când variabila de mediu B<GCONV_PATH> nu este definită."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--nostdlib>"
msgstr "B<--nostdlib>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Do not search the system default gconv directory, only the directories "
"provided on the command line."
msgstr ""
"Nu caută în directorul gconv implicit al sistemului, ci doar în directoarele "
"furnizate în linia de comandă."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--output=>I<outputfile>"
msgstr "B<--output=>I<fișier-ieșire>"

#. type: TQ
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-o\\~>I<outputfile>"
msgstr "B<-o\\~>I<fișier-ieșire>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use I<outputfile> for output instead of the system default cache location."
msgstr ""
"Utilizează I<fișier-ieșire> pentru ieșire în loc de locația implicită a "
"fișierului de prestocare a datelor din sistem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--prefix=>I<pathname>"
msgstr "B<--prefix=>I<nume-rută>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the prefix to be prepended to the system pathnames.  See FILES, below.  "
"By default, the prefix is empty.  Setting the prefix to I<foo>, the gconv "
"module configuration would be read from I<foo/usr/lib/gconv/gconv-modules> "
"and the cache would be written to I<foo/usr/lib/gconv/gconv-modules.cache>."
msgstr ""
"Stabilește prefixul care urmează să fie adăugat în prealabil la numele "
"rutelor sistemului.  A se vedea FIȘIERE, mai jos.  În mod implicit, prefixul "
"este gol.  Dacă se stabilește prefixul la I<foo>, configurația modulului "
"gconv ar fi citită din I<foo/usr/lib/gconv/gconv-modules>, iar fișierul de "
"prestocare a datelor (cache) ar fi scris în I<foo/usr/lib/gconv/gconv/gconv-"
"modules.cache>."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: TQ
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-?>"
msgstr "B<-?>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print a usage summary and exit."
msgstr "Afișează un rezumat al utilizării și iese."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print a short usage summary and exit."
msgstr "Imprimă un mesaj scurt de utilizare și iese."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: TQ
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print the version number, license, and disclaimer of warranty for B<iconv>."
msgstr ""
"Afișează numărul versiunii, licența și declarația de renunțare la garanție "
"pentru B<iconv>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "STARE DE IEȘIRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Zero on success, nonzero on errors."
msgstr "Returnează 0 în caz de succes, diferit de zero în caz de eșec."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib/gconv>"
msgstr "I</usr/lib/gconv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Usual default gconv module path."
msgstr "Ruta implicită obișnuită a modulelor gconv."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib/gconv/gconv-modules>"
msgstr "I</usr/lib/gconv/gconv-modules>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Usual system default gconv module configuration file."
msgstr ""
"Fișierul obișnuit de configurare a modulului gconv implicit al sistemului."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib/gconv/gconv-modules.cache>"
msgstr "I</usr/lib/gconv/gconv-modules.cache>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Usual system gconv module configuration cache."
msgstr "Fișierul cache obișnuit de configurare a modulelor gconv din sistem."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Depending on the architecture, the above files may instead be located at "
"directories with the path prefix I</usr/lib64>."
msgstr ""
"În funcție de arhitectură, fișierele de mai sus pot fi în schimb localizate "
"în directoare cu prefixul de rută I</usr/lib64>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<iconv>(1), B<iconv>(3)"
msgstr "B<iconv>(1), B<iconv>(3)"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>I< outputfile>B<, --output=>I<outputfile>"
msgstr "B<-o>I< fișier-ieșire>B<, --output=>I<fișier-ieșire>"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-?>, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
