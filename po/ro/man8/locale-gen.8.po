# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:35+0200\n"
"PO-Revision-Date: 2023-07-15 09:44+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "May 5, 2022"
msgstr "5 mai 2022"

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "LOCALE-GEN 8"
msgstr "LOCALE-GEN 8"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm locale-gen>"
msgstr "E<.Nm locale-gen>"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "generate localisation files from templates"
msgstr "generează fișiere de configurații regionale din șabloane"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm> E<.Op Fl -keep-existing>"
msgstr "E<.Nm> E<.Op Fl -keep-existing>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"As compiled locales are large, only templates are distributed in the default "
"E<.Sy locales> package, and only the desired locales are compiled on the "
"target system."
msgstr ""
"Deoarece configurațiile regionale compilate sunt mari, numai șabloanele sunt "
"distribuite în pachetul implicit E<.Sy locales> și numai configurațiile "
"regionale dorite sunt compilate pe sistemul țintă."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"After selecting the locales into E<.Pa /etc/locale.gen> E<.Pq via Nm dpkg No "
"package configuration, for example>, E<.Nm> is run to compile them via E<.Xr "
"localedef 1>."
msgstr ""
"După selectarea configurațiilor regionale în E<.Pa /etc/locale.gen> E<.Pq cu "
"ajutorul configurării pachetului cu Nm dpkg No, de exemplu>, E<.Nm> este "
"rulat pentru a le compila prin intermediul E<.Xr localedef 1>."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Fl -keep-existing"
msgstr "Fl -keep-existing"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Do not remove E<.Pa /usr/lib/locale/locale-archive>, only compiling new "
"locales."
msgstr ""
"Nu elimină E<.Pa /usr/lib/locale/locale-archive>, ci doar compilează noile "
"configurații regionale."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: It
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Pa /etc/locale.gen"
msgstr "Pa /etc/locale.gen"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Whitespace-separated newline-delimited E<.Ar locale charset> list of locales "
"to build with E<.Li #> start-of-line comments."
msgstr ""
"Listă de E<.Ar seturi de caractere regionale> delimitată prin linii noi, "
"separată prin spații albe, de configurații regionale care urmează să fie "
"construite, cu marcaje de comentarii la început de linie E<.Li #>."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Xr locale 1>, E<.Xr localedef 1>, E<.Xr locale.gen 5>"
msgstr "E<.Xr locale 1>, E<.Xr localedef 1>, E<.Xr locale.gen 5>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Pa /usr/share/i18n/SUPPORTED> E<.Pq Pa /usr/local/share/i18n/SUPPORTED> "
"\\(em list of all supported locales on the current system."
msgstr ""
"E<.Pa /usr/share/i18n/SUPPORTED> E<.Pq Pa /usr/local/share/i18n/SUPPORTED> "
"\\(em lista tuturor configurațiilor regionale acceptate pe sistemul curent."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Sy locales-all> package, which contains all supported locales in "
"compiled form."
msgstr ""
"Pachetul E<.Sy locales-all>, care conține toate configurațiile regionale "
"acceptate în formă compilată."
