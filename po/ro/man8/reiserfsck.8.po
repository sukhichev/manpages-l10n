# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2023-08-27 17:16+0200\n"
"PO-Revision-Date: 2024-03-10 19:49+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "REISERFSCK"
msgstr "REISERFSCK"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "January 2009"
msgstr "ianuarie 2009"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "Reiserfsprogs-3.6.27"
msgstr "Reiserfsprogs-3.6.27"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "reiserfsck - The checking tool for the ReiserFS filesystem."
msgstr ""
"reiserfsck - instrumentul de verificare pentru sistemul de fișiere ReiserFS"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. [ \fB-i\fR | \fB--interactive\fR ]
#. [ \fB-b\fR | \fB--scan-marked-in-bitmap \fIbitmap-filename\fR ]
#. [ \fB-h\fR | \fB--hash \fIhash-name\fR ]
#. [ \fB-g\fR | \fB--background\fR ]
#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<reiserfsck> [ B<-aprVy> ] [ B<--rebuild-sb> | B<--check> | B<--fix-"
"fixable> | B<--rebuild-tree> | B<--clean-attributes> ] [ B<-j> | B<--"
"journal> I<device> ] [ B<-z> | B<--adjust-size> ] [ B<-n> | B<--nolog> ] "
"[ B<-B> | B<--badblocks >I<file> ] [ B<-l> | B<--logfile >I<file> ] [ B<-q> "
"| B<--quiet> ] [ B<-y> | B<--yes> ] [ B<-f> | B<--force> ] [ B<-S> | B<--"
"scan-whole-partition> ] [ B<--no-journal-available> ] I<device>"
msgstr ""
"B<reiserfsck> [ B<-aprVy> ] [ B<--rebuild-sb> | B<--check> | B<--fix-"
"fixable> | B<--rebuild-tree> | B<--clean-attributes> ] [ B<-j> | B<--"
"journal> I<dispozitiv> ] [ B<-z> | B<--adjust-size> ] [ B<-n> | B<--nolog> ] "
"[ B<-B> | B<--badblocks >I<filșier> ] [ B<-l> | B<--logfile >I<fișier> ] "
"[ B<-q> | B<--quiet> ] [ B<-y> | B<--yes> ] [ B<-f> | B<--force> ] [ B<-S> | "
"B<--scan-whole-partition> ] [ B<--no-journal-available> ] I<dispozitiv>"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<Reiserfsck> searches for a Reiserfs filesystem on a device, replays any "
"necessary transactions, and either checks or repairs the file system."
msgstr ""
"B<reiserfsck> caută un sistem de fișiere Reiserfs pe un dispozitiv, reia "
"toate tranzacțiile necesare și verifică sau repară sistemul de fișiere."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "I<device>"
msgstr "I<dispozitiv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"is the special file corresponding to a device or to a partition (e.g /dev/"
"hdXX for an IDE disk partition or /dev/sdXX for a SCSI disk partition)."
msgstr ""
"este fișierul special care corespunde unui dispozitiv sau unei partiții (de "
"exemplu, /dev/hdXX pentru o partiție de disc IDE sau /dev/sdXX pentru o "
"partiție de disc SCSI)."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--rebuild-sb>"
msgstr "B<--rebuild-sb>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option recovers the superblock on a Reiserfs partition.  Normally you "
"only need this option if mount reports \"read_super_block: can't find a "
"reiserfs file system\" and you are sure that a Reiserfs file system is "
"there. But remember that if you have used some partition editor program and "
"now you cannot find a filesystem, probably something has gone wrong while "
"repartitioning and the start of the partition has been changed. If so, "
"instead of rebuilding the super block on a wrong place you should find the "
"correct start of the partition first."
msgstr ""
"Această opțiune recuperează superblocul de pe o partiție Reiserfs. În mod "
"normal, aveți nevoie de această opțiune numai dacă «mount» raportează "
"„read_super_block: can't find a reiserfs file system” și sunteți sigur că "
"există un sistem de fișiere Reiserfs. Dar nu uitați că, dacă ați folosit un "
"program de editare a partițiilor și acum nu puteți găsi un sistem de "
"fișiere, probabil că ceva nu a mers bine în timpul repartiționării și "
"începutul partiției a fost schimbat. În acest caz, în loc să reconstruiți "
"superblocul pe un loc greșit, ar trebui să găsiți mai întâi începutul corect "
"al partiției."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--check>"
msgstr "B<--check>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This default action checks filesystem consistency and reports, but does not "
"repair any corruption that it finds. This option may be used on a read-only "
"file system mount."
msgstr ""
"Această acțiune implicită verifică consistența sistemului de fișiere și "
"raportează, dar nu repară nicio deteriorare pe care o găsește. Această "
"opțiune poate fi utilizată la o montare a unui sistem de fișiere numai "
"pentru citire."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--fix-fixable>"
msgstr "B<--fix-fixable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option recovers certain kinds of corruption that do not require "
"rebuilding the entire file system tree (B<--rebuild-tree>). Normally you "
"only need this option if the B<--check> option reports \"corruption that can "
"be fixed with B<--fix-fixable>\". This includes: zeroing invalid data-block "
"pointers, correcting st_size and st_blocks for directories, and deleting "
"invalid directory entries."
msgstr ""
"Această opțiune recuperează anumite tipuri de corupție care nu necesită "
"reconstruirea întregului arbore al sistemului de fișiere (B<--rebuild-"
"tree>). În mod normal, aveți nevoie de această opțiune numai dacă opțiunea "
"B<--check> raportează „corruption that can be fixed with B<--fix-fixable>” "
"(corupție care poate fi reparată cu B<--fix-fixable>). Aceasta include: "
"reducerea la zero a indicatorilor de blocuri de date nevalide, corectarea "
"st_size și st_blocks pentru directoare și ștergerea intrărilor de directoare "
"nevalide."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--rebuild-tree>"
msgstr "B<--rebuild-tree>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option rebuilds the entire filesystem tree using leaf nodes found on "
"the device.  Normally you only need this option if the B<reiserfsck --check> "
"reports \"Running with B<--rebuild-tree> is required\". You are strongly "
"encouraged to make a backup copy of the whole partition before attempting "
"the B<--rebuild-tree> option. Once B<reiserfsck --rebuild-tree> is started "
"it must finish its work (and you should not interrupt it), otherwise the "
"filesystem will be left in the unmountable state to avoid subsequent data "
"corruptions."
msgstr ""
"Această opțiune reconstruiește întregul arbore al sistemului de fișiere "
"utilizând nodurile de frunze găsite pe dispozitiv. În mod normal, aveți "
"nevoie de această opțiune numai dacă B<reiserfsck --check> raportează "
"„Running with B<--rebuild-tree> is required” (Este necesară rularea cu B<--"
"rebuild-tree>.). Vă recomandăm insistent să faceți o copie de rezervă a "
"întregii partiții înainte de a încerca opțiunea B<--rebuild-tree>. Odată ce "
"B<reiserfsck --rebuild-tree> a fost pornit, acesta trebuie să-și termine "
"activitatea (și nu trebuie să-l întrerupeți), altfel sistemul de fișiere va "
"fi lăsat în starea de ne-montabil pentru a evita corupțiile ulterioare de "
"date."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--clean-attributes>"
msgstr "B<--clean-attributes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option cleans reserved fields of Stat-Data items. There were days when "
"there were no extended attributes in reiserfs. When they were implemented "
"old partitions needed to be cleaned first -- reiserfs code in the kernel did "
"not care about not used fields in its strutures. Thus if you have used one "
"of the old (pre-attrbutes) kernels with a ReiserFS filesystem and you want "
"to use extented attribues there, you should clean the filesystem first."
msgstr ""
"Această opțiune curăță câmpurile rezervate ale elementelor Stat-Data. Au "
"fost zile când nu existau atribute extinse în reiserfs. Când au fost "
"implementate, partițiile vechi trebuiau curățate mai întâi -- codul reiserfs "
"din nucleu nu se interesa de câmpurile nefolosite în structurile sale. "
"Astfel, dacă ați folosit unul dintre nucleele vechi (pre-atribute) cu un "
"sistem de fișiere ReiserFS și doriți să folosiți atributele extinse acolo, "
"trebuie să curățați mai întâi sistemul de fișiere."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--journal >I<device >, B<-j >I<device >"
msgstr "B<--journal >I<dispozitiv >, B<-j >I<dispozitiv >"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option supplies the device name of the current file system journal.  "
"This option is required when the journal resides on a separate device from "
"the main data device (although it can be avoided with the expert option B<--"
"no-journal-available>)."
msgstr ""
"Această opțiune furnizează numele de dispozitiv al jurnalului sistemului de "
"fișiere curent. Această opțiune este necesară atunci când jurnalul se află "
"pe un dispozitiv separat de dispozitivul principal de date (deși poate fi "
"evitată cu opțiunea de expert B<--no-journal-available>)."

#. .B --interactive, -i
#. This makes \fBreiserfsck\fR to stop after each pass completed.
#. .TP
#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--adjust-size, -z>"
msgstr "B<--adjust-size, -z>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option causes B<reiserfsck> to correct file sizes that are larger than "
"the offset of the last discovered byte.  This implies that holes at the end "
"of a file will be removed.  File sizes that are smaller than the offset of "
"the last discovered byte are corrected by B<--fix-fixable>."
msgstr ""
"Această opțiune face ca B<reiserfsck> să corecteze dimensiunile fișierelor "
"care sunt mai mari decât decalajul ultimului octet descoperit. Aceasta "
"implică faptul că vor fi eliminate găurile de la sfârșitul unui fișier. "
"Dimensiunile fișierelor care sunt mai mici decât decalajul ultimului octet "
"descoperit sunt corectate de B<--fix-fixable>."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--badblocks >I<file>, B<-B >I< file>"
msgstr "B<--badblocks >I<fișier>, B<-B >I< fișier>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option sets the badblock list to be the list of blocks specified in the "
"given `file`. The filesystem badblock list is cleared before the new list is "
"added. It can be used with B<--fix-fixable> to fix the list of badblocks "
"(see B<debugreiserfs -B>). If the device has bad blocks, every time it must "
"be given with the B<--rebuild-tree> option."
msgstr ""
"Această opțiune stabilește lista de blocuri defectuoase ca fiind lista de "
"blocuri specificate în fișierul „fișier” dat. Lista de blocuri defectuoase "
"din sistemul de fișiere este curățată înainte de adăugarea noii liste. Poate "
"fi utilizată împreună cu B<--fix-fixable> pentru a fixa lista de blocuri "
"defectuoase (a se vedea B<debugreiserfs -B>). În cazul în care dispozitivul "
"are blocuri defectuoase, de fiecare dată trebuie să fie dată cu opțiunea B<--"
"rebuild-tree>."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--logfile >I<file>, B<-l >I< file>"
msgstr "B<--logfile >I<fișier>, B<-l >I< fișier>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option causes B<reiserfsck> to report any corruption it finds to the "
"specified log file rather than to stderr."
msgstr ""
"Această opțiune face ca B<reiserfsck> să raporteze orice corupție pe care o "
"găsește în fișierul jurnal specificat, mai degrabă decât la ieșirea de "
"eroare standard."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--nolog, -n>"
msgstr "B<--nolog, -n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option prevents B<reiserfsck> from reporting any kinds of corruption."
msgstr ""
"Această opțiune previne ca B<reiserfsck> să raporteze orice tip de corupție."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--quiet, -q>"
msgstr "B<--quiet, -q>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "This option prevents B<reiserfsck> from reporting its rate of progress."
msgstr ""
"Această opțiune împiedică B<reiserfsck> să raporteze rata sa de progres."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--yes, -y>"
msgstr "B<--yes, -y>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option inhibits B<reiserfsck> from asking you for confirmation after "
"telling you what it is going to do. It will assuem you confirm. For safety, "
"it does not work with the B<--rebuild-tree> option."
msgstr ""
"Această opțiune împiedică B<reiserfsck> să vă ceară confirmarea după ce vă "
"spune ce urmează să facă. Acesta presupune că ați dat confirmarea. Din "
"motive de siguranță, această opțiune nu funcționează cu opțiunea B<--rebuild-"
"tree>."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-a>, B<-p>"
msgstr "B<-a>, B<-p>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"These options are usually passed by fsck -A during the automatic checking of "
"those partitions listed in /etc/fstab. These options cause B<reiserfsck> to "
"print some information about the specified filesystem, to check if error "
"flags in the superblock are set and to do some light-weight checks. If these "
"checks reveal a corruption or the flag indicating a (possibly fixable)  "
"corruption is found set in the superblock, then B<reiserfsck> switches to "
"the fix-fixable mode. If the flag indicating a fatal corruption is found set "
"in the superblock, then B<reiserfsck> finishes with an error."
msgstr ""
"Aceste opțiuni sunt de obicei transmise de fsck -A în timpul verificării "
"automate a partițiilor listate în /etc/fstab. Aceste opțiuni determină "
"B<reiserfsck> să afișeze unele informații despre sistemul de fișiere "
"specificat, să verifice dacă sunt activate fanioanele de eroare din super-"
"bloc și să efectueze unele verificări ușoare. În cazul în care aceste "
"verificări relevă o corupție sau dacă se găsește activat în super-bloc "
"fanionul care indică o corupție (posibil rezolvabilă), atunci B<reiserfsck> "
"trece în modul „fix-fixable”. În cazul în care se găsește un fanion care "
"indică o corupție fatală activat în super-bloc, atunci B<reiserfsck> se "
"termină cu o eroare."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--force, -f>"
msgstr "B<--force, -f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Force checking even if the file system seems clean."
msgstr "Forțează verificarea chiar dacă sistemul de fișiere pare curat."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "This option prints the reiserfsprogs version and then exit."
msgstr "Această opțiune afișează versiunea de «reiserfsprogs» și apoi iese."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option does nothing at all; it is provided only for backwards "
"compatibility."
msgstr ""
"Această opțiune nu are niciun efect; este furnizată doar pentru "
"compatibilitate retroactivă."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "EXPERT OPTIONS"
msgstr "OPȚIUNI PENTRU EXPERȚI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"DO NOT USE THESE OPTIONS UNLESS YOU KNOW WHAT YOU ARE DOING.  WE ARE NOT "
"RESPONSIBLE IF YOU LOSE DATA AS A RESULT OF THESE OPTIONS."
msgstr ""
"NU UTILIZAȚI ACESTE OPȚIUNI DECÂT DACĂ ȘTIȚI CE FACEȚI. NU SUNTEM "
"RESPONSABILI DACĂ PIERDEȚI DATE CA URMARE A ACESTOR OPȚIUNI."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--no-journal-available>"
msgstr "B<--no-journal-available>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option allows B<reiserfsck> to proceed when the journal device is not "
"available. This option has no effect when the journal is located on the main "
"data device. NOTE: after this operation you must use B<reiserfstune> to "
"specify a new journal device."
msgstr ""
"Această opțiune îi permite lui B<reiserfsck> să continue atunci când "
"dispozitivul de jurnal nu este disponibil. Această opțiune nu are niciun "
"efect atunci când jurnalul este localizat pe dispozitivul principal de date. "
"NOTĂ: după această operație, trebuie să utilizați B<reiserfstune> pentru a "
"specifica un nou dispozitiv de jurnal."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--scan-whole-partition, -S>"
msgstr "B<--scan-whole-partition, -S>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This option causes B<--rebuild-tree> to scan the whole partition but not "
"only the used space on the partition."
msgstr ""
"Această opțiune face ca B<--rebuild-tree> să scaneze întreaga partiție, nu "
"doar spațiul utilizat de pe partiție."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "AN EXAMPLE OF USING reiserfsck"
msgstr "UN EXEMPLU DE UTILIZARE a reiserfsck"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"1. You think something may be wrong with a reiserfs partition on /dev/hda1 "
"or you would just like to perform a periodic disk check."
msgstr ""
"1. Credeți că ceva nu este în regulă cu o partiție reiserfs de pe „/dev/"
"hda1” sau doriți doar să efectuați o verificare periodică a discului."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"2. Run B<reiserfsck --check --logfile check.log /dev/hda1>. If B<reiserfsck "
"--check> exits with status 0 it means no errors were discovered."
msgstr ""
"2. Rulați B<reiserfsck --check --logfile check.log /dev/hda1>. Dacă "
"B<reiserfsck --check> iese cu starea 0, înseamnă că nu au fost descoperite "
"erori."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"3. If B<reiserfsck --check> exits with status 1 (and reports about fixable "
"corruptions) it means that you should run B<reiserfsck --fix-fixable --"
"logfile fixable.log /dev/hda1>."
msgstr ""
"3. Dacă B<reiserfsck --check> iese cu starea 1 (și raportează despre "
"corupții reparabile), înseamnă că trebuie să executați B<reiserfsck ---"
"fixable --logfile fixable.log /dev/hda1>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"4. If B<reiserfsck --check> exits with status 2 (and reports about fatal "
"corruptions) it means that you need to run B<reiserfsck --rebuild-tree>.  If "
"B<reiserfsck --check> fails in some way you should also run B<reiserfsck --"
"rebuild-tree>, but we also encourage you to submit this as a bug report."
msgstr ""
"4. Dacă B<reiserfsck --check> iese cu starea 2 (și raportează despre "
"corupții fatale) înseamnă că trebuie să executați B<reiserfsck --rebuild-"
"tree>. Dacă B<reiserfsck --check> eșuează în vreun fel, ar trebui să rulați "
"și B<reiserfsck --rebuild-tree>, dar vă încurajăm, de asemenea, să trimiteți "
"acest lucru ca un raport de eroare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"5. Before running B<reiserfsck --rebuild-tree>, please make a backup of the "
"whole partition before proceeding. Then run B<reiserfsck --rebuild-tree --"
"logfile rebuild.log /dev/hda1>."
msgstr ""
"5. Înainte de a rula B<reiserfsck --rebuild-tree>, vă rugăm să faceți o "
"copie de rezervă a întregii partiții înainte de a continua. Apoi rulați "
"B<reiserfsck --rebuild-tree --logfile rebuild.log /dev/hda1>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"6. If the B<reiserfsck --rebuild-tree> step fails or does not recover what "
"you expected, please submit this as a bug report. Try to provide as much "
"information as possible including your platform and Linux kernel version. We "
"will try to help solve the problem."
msgstr ""
"6. Dacă pasul B<reiserfsck --rebuild-tree> nu reușește sau nu recuperează "
"ceea ce vă așteptați, vă rugăm să trimiteți acest lucru ca raport de eroare. "
"Încercați să furnizați cât mai multe informații posibile, inclusiv platforma "
"și versiunea nucleului Linux. Vom încerca să vă ajutăm să rezolvați problema."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "EXIT CODES"
msgstr "CODURI DE IEȘIRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<reiserfsck> uses the following exit codes:"
msgstr "B<reiserfsck> utilizează următoarele coduri de ieșire:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\ I<0> -\\ No errors."
msgstr "\\ I<0> -\\ Nu sunt erori."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\ I<1> -\\ File system errors corrected."
msgstr "\\ I<1> -\\ Erori ale sistemului de fișiere corectate."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\ I<2> -\\ Reboot is needed."
msgstr "\\ I<2> -\\ Este necesară repornirea."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\ I<4> -\\ File system fatal errors left uncorrected,"
msgstr "\\ I<4> -\\ Erori fatale ale sistemului de fișiere lăsate necorectate,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\\t B<reiserfsck --rebuild-tree> needs to be launched."
msgstr "\\\t B<reiserfsck --rebuild-tree> trebuie să fie lansat.."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\ I<6> -\\ File system fixable errors left uncorrected,"
msgstr ""
"\\ I<6> -\\ Erori corectabile ale sistemului de fișiere lăsate necorectate,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\\t B<reiserfsck --fix-fixable> needs to be launched."
msgstr "\\\t B<reiserfsck --fix-fixable> trebuie să fie lansat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\ I<8> -\\ Operational error."
msgstr "\\ I<8> -\\ Eroare de operare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "\\ I<16> -\\ Usage or syntax error."
msgstr "\\ I<16> -\\ Eroare de utilizare sau de sintaxă."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This version of B<reiserfsck> has been written by Vitaly Fertman "
"E<lt>vitaly@namesys.comE<gt>."
msgstr ""
"Această versiune de B<reiserfsck> a fost scrisă de Vitaly Fertman "
"E<lt>vitaly@namesys.comE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr "ERORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Please report bugs to the ReiserFS developers E<lt>reiserfs-devel@vger."
"kernel.orgE<gt>, providing as much information as possible--your hardware, "
"kernel, patches, settings, all printed messages, the logfile; check the "
"syslog file for any related information."
msgstr ""
"Raportați erorile către dezvoltatorii ReiserFS E<lt>reiserfs-devel@vger."
"kernel.orgE<gt>, furnizând cât mai multe informații posibile - componentele "
"calculatorului, nucleul, corecțiile, configurările, toate mesajele "
"imprimate, fișierul dde jurnal; verificați fișierul syslog pentru orice "
"informații conexe."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "TODO"
msgstr "DE FĂCUT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Faster recovering, signal handling."
msgstr "Recuperare mai rapidă, gestionarea semnalelor."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<mkreiserfs>(8), B<reiserfstune>(8)  B<resize_reiserfs>(8), "
"B<debugreiserfs>(8),"
msgstr ""
"B<mkreiserfs>(8), B<reiserfstune>(8)  B<resize_reiserfs>(8), "
"B<debugreiserfs>(8),"
