# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-01 16:59+0100\n"
"PO-Revision-Date: 2023-11-05 12:25+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "io_setup"
msgstr "io_setup"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "io_setup - create an asynchronous I/O context"
msgstr "io_setup - creează un context de In/Ieș asincron"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Alternatively, Asynchronous I/O library (I<libaio>, I<-laio>); see VERSIONS."
msgstr ""
"Alternativ, biblioteca de In/Ieș asincronă (I<libaio>, I<-laio>); a se vedea "
"VERSIUNI."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/aio_abi.hE<gt>>          /* Defines needed types */\n"
msgstr "B<#include E<lt>linux/aio_abi.hE<gt>>          /* Definește tipurile necesare */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<long io_setup(unsigned int >I<nr_events>B<, aio_context_t *>I<ctx_idp>B<);>\n"
msgstr "B<long io_setup(unsigned int >I<nr_events>B<, aio_context_t *>I<ctx_idp>B<);>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<Note>: There is no glibc wrapper for this system call; see VERSIONS."
msgstr ""
"I<Notă>: Nu există nicio funcție învăluitoare glibc pentru acest apel de "
"sistem; a se vedea VERSIUNI."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: this page describes the raw Linux system call interface.  The "
"wrapper function provided by I<libaio> uses a different type for the "
"I<ctx_idp> argument.  See VERSIONS."
msgstr ""
"I<Notă>: această pagină descrie interfața brută de apelare a sistemului "
"Linux.  Funcția de învăluire furnizată de I<libaio> utilizează un tip "
"diferit pentru argumentul I<ctx_idp>.  A se vedea VERSIUNI."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<io_setup>()  system call creates an asynchronous I/O context suitable "
"for concurrently processing I<nr_events> operations.  The I<ctx_idp> "
"argument must not point to an AIO context that already exists, and must be "
"initialized to 0 prior to the call.  On successful creation of the AIO "
"context, I<*ctx_idp> is filled in with the resulting handle."
msgstr ""
"Apelul de sistem B<io_setup>() creează un context de intrare/ieșire asincron "
"adecvat pentru procesarea simultană a operațiunilor I<nr_events>.  "
"Argumentul I<ctx_idp> nu trebuie să indice un context AIO care există deja "
"și trebuie inițializat la 0 înainte de apel.  La crearea cu succes a "
"contextului AIO, I<*ctx_idp> este completat cu gestionarul rezultat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<io_setup>()  returns 0.  For the failure return, see VERSIONS."
msgstr ""
"În caz de succes, B<io_setup>() returnează 0.  Pentru returnarea în caz de "
"eșec, a se vedea VERSIUNI."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN>"
msgstr "B<EAGAIN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The specified I<nr_events> exceeds the limit of available events, as defined "
"in I</proc/sys/fs/aio-max-nr> (see B<proc>(5))."
msgstr ""
"Valoarea I<nr_events> specificată depășește limita evenimentelor "
"disponibile, așa cum este definită în I</proc/sys/fs/aio-max-nr> (a se vedea "
"B<proc>(5))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An invalid pointer is passed for I<ctx_idp>."
msgstr "Un indicator nevalid este transmis pentru I<ctx_idp>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<ctx_idp> is not initialized, or the specified I<nr_events> exceeds "
"internal limits.  I<nr_events> should be greater than 0."
msgstr ""
"I<ctx_idp> nu este inițializat sau I<nr_events> specificat depășește "
"limitele interne.  I<nr_events> trebuie să fie mai mare decât 0."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel resources are available."
msgstr "Nu au fost disponibile suficiente resurse pentru nucleu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<io_setup>()  is not implemented on this architecture."
msgstr "B<io_setup>() nu este implementată pe această arhitectură."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#.  http://git.fedorahosted.org/git/?p=libaio.git
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"glibc does not provide a wrapper for this system call.  You could invoke it "
"using B<syscall>(2).  But instead, you probably want to use the "
"B<io_setup>()  wrapper function provided by I<libaio>."
msgstr ""
"Glibc nu oferă o funcție de învăluire pentru acest apel de sistem.  Puteți "
"să-l apelați folosind B<syscall>(2).  Dar, în schimb, probabil că doriți să "
"folosiți funcția de învăluire B<io_setup>() furnizată de I<libaio>."

#.  But glibc is confused, since <libaio.h> uses 'io_context_t' to declare
#.  the system call.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the I<libaio> wrapper function uses a different type "
"(I<io_context_t\\ *>)  for the I<ctx_idp> argument.  Note also that the "
"I<libaio> wrapper does not follow the usual C library conventions for "
"indicating errors: on error it returns a negated error number (the negative "
"of one of the values listed in ERRORS).  If the system call is invoked via "
"B<syscall>(2), then the return value follows the usual conventions for "
"indicating an error: -1, with I<errno> set to a (positive) value that "
"indicates the error."
msgstr ""
"Rețineți că funcția de învăluire I<libaio> utilizează un tip diferit "
"(I<io_context_t\\ *>) pentru argumentul I<ctx_idp>.  Rețineți, de asemenea, "
"că funcția de învăluire I<libaio> nu respectă convențiile obișnuite ale "
"bibliotecii C pentru indicarea erorilor: în caz de eroare, aceasta "
"returnează un număr de eroare negat (negativul uneia dintre valorile "
"enumerate în ERORI).  Dacă apelul de sistem este invocat prin B<syscall>(2), "
"atunci valoarea de returnare urmează convențiile obișnuite pentru indicarea "
"unei erori: -1, cu I<errno> configurată la o valoare (pozitivă) care indică "
"eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 2.5."
msgstr "Linux 2.5."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<io_cancel>(2), B<io_destroy>(2), B<io_getevents>(2), B<io_submit>(2), "
"B<aio>(7)"
msgstr ""
"B<io_cancel>(2), B<io_destroy>(2), B<io_getevents>(2), B<io_submit>(2), "
"B<aio>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"Alternatively, Asynchronous I/O library (I<libaio>, I<-laio>); see NOTES."
msgstr ""
"Alternativ, biblioteca de In/Ieș asincronă (I<libaio>, I<-laio>); a se vedea "
"NOTE."

#. type: Plain text
#: debian-bookworm
msgid "I<Note>: There is no glibc wrapper for this system call; see NOTES."
msgstr ""
"I<Notă>: Nu există nicio funcție învăluitoare (wrapper) glibc pentru acest "
"apel de sistem; a se vedea NOTE."

#. type: Plain text
#: debian-bookworm
msgid ""
"I<Note>: this page describes the raw Linux system call interface.  The "
"wrapper function provided by I<libaio> uses a different type for the "
"I<ctx_idp> argument.  See NOTES."
msgstr ""
"I<Notă>: această pagină descrie interfața brută de apelare a sistemului "
"Linux.  Funcția de învăluire furnizată de I<libaio> utilizează un tip "
"diferit pentru argumentul I<ctx_idp>.  A se vedea NOTE."

#. type: Plain text
#: debian-bookworm
msgid ""
"On success, B<io_setup>()  returns 0.  For the failure return, see NOTES."
msgstr ""
"În caz de succes, B<io_setup>() returnează 0.  Pentru returnarea în caz de "
"eșec, a se vedea NOTE."

#. type: Plain text
#: debian-bookworm
msgid "The asynchronous I/O system calls first appeared in Linux 2.5."
msgstr ""
"Apelurile sistemului de In/Ieș asincrone au apărut pentru prima dată în "
"Linux 2.5."

#. type: Plain text
#: debian-bookworm
msgid ""
"B<io_setup>()  is Linux-specific and should not be used in programs that are "
"intended to be portable."
msgstr ""
"B<io_setup>() este specific Linux și nu ar trebui să fie utilizată în "
"programe care sunt destinate să fie portabile."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
