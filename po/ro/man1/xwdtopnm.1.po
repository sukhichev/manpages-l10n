# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-09 15:52+0100\n"
"PO-Revision-Date: 2024-03-10 11:46+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Xwdtopnm User Manual"
msgstr "Manualul utilizatorului xwmdtoppm"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "08 January 2010"
msgstr "8 ianuarie 2010"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "netpbm documentation"
msgstr "documentația netpbm"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "xwdtopnm - convert an X11 or X10 window dump file to a PNM image"
msgstr ""
"xwdtopnm - convertește un fișier de captură de ferestre X11 sau X10 într-o "
"imagine PNM"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<xwdtopnm> [B<-verbose>] [B<-headerdump>] [I<xwdfile>]"
msgstr "B<xwdtopnm> [B<-verbose>] [B<-headerdump>] [I<fișier-xwd>]"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "This program is part of B<Netpbm>(1)  \\&."
msgstr "Acest program face parte din B<Netpbm>(1)\\&."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<xwdtopnm> reads an X11 or X10 window dump file as input and produces a PNM "
"image as output.  The type of the output image depends on the input file - "
"if it's black and white, the output is PBM.  If it's grayscale, the output "
"is PGM.  Otherwise, it's PPM.  The program tells you which type it is "
"writing."
msgstr ""
"B<xwdtopnm> citește ca intrare un fișier de captură al ferestrelor X11 sau "
"X10 și produce o imagine PNM ca ieșire. Tipul imaginii de ieșire depinde de "
"fișierul de intrare - dacă este alb-negru, imaginea de ieșire este PBM. Dacă "
"este în tonuri de gri, ieșirea este PGM. În caz contrar, este PPM. Programul "
"vă spune ce tip scrie."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Using this program, you can convert anything you can display on an X "
"workstation's screen into a PNM image.  Just display whatever you're "
"interested in, run the B<xwd> program to capture the contents of the window, "
"run it through B<xwdtopnm>, and then use B<pamcut> to select the part you "
"want."
msgstr ""
"Cu ajutorul acestui program, puteți converti orice lucru pe care îl puteți "
"afișa pe ecranul unei stații de lucru X într-o imagine PNM. Trebuie doar să "
"afișați ceea ce vă interesează, să rulați programul B<xwd> pentru a captura "
"conținutul ferestrei, să îl treceți prin B<xwdtopnm> și apoi să utilizați "
"B<pamcut> pentru a selecta partea pe care o doriți."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that a pseudocolor XWD image (typically what you get when you make a "
"dump of a pseudocolor X window) has maxval 65535, which means the PNM file "
"that B<xwdtopnm> generates has maxval 65535.  Many older image processing "
"programs (that aren't part of the Netpbm package and don't use the Netpbm "
"programming library) don't know how to handle a PNM image with maxval "
"greater than 255 (because there are two bytes instead of one for each sample "
"in the image).  So you may want to run the output of B<xwdtopnm> through "
"B<pamdepth> before feeding it to one of these old programs."
msgstr ""
"Rețineți că o imagine XWD pseudocolorată (de obicei ceea ce obțineți atunci "
"când efectuați o descărcare a unei ferestre X pseudocolorate) are val.max. "
"65535, ceea ce înseamnă că fișierul PNM pe care îl generează B<xwdtopnm> are "
"val.max. 65535. Multe programe mai vechi de procesare a imaginilor (care nu "
"fac parte din pachetul Netpbm și nu utilizează biblioteca de programare "
"Netpbm) nu știu cum să gestioneze o imagine PNM cu o val.max. mai mare de "
"255 (deoarece există doi octeți în loc de unul pentru fiecare eșantion din "
"imagine). Așadar, este posibil să doriți să rulați ieșirea din B<xwdtopnm> "
"prin B<pamdepth> înainte de a o introduce în unul dintre aceste programe "
"vechi."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<xwdtopnm> can't convert every kind of XWD image (which essentially means "
"it can't convert an XWD created from every kind of X display "
"configuration).  In particular, it cannot convert one with more than 24 bits "
"per pixel."
msgstr ""
"B<xwdtopnm> nu poate converti orice tip de imagine XWD (ceea ce înseamnă, în "
"esență, că nu poate converti un XWD creat din orice tip de configurație de "
"afișare X). În special, nu poate converti una cu mai mult de 24 de biți pe "
"pixel."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"In addition to the options common to all programs based on libnetpbm\n"
"(most notably B<-quiet>, see \n"
"E<.UR index.html#commonoptions>\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<xwdtopnm> recognizes the following\n"
"command line options:\n"
msgstr ""
"În plus față de opțiunile comune tuturor programelor bazate pe libnetpbm\n"
"(în special B<-quiet>, a se vedea \n"
"E<.UR index.html#commonoptions>.\n"
" Common Options\n"
"E<.UE>\n"
"\\&), B<xwdtopnm> recunoaște următoarea\n"
"opțiune de linie de comandă:\n"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-verbose>"
msgstr "B<-verbose>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This option causes B<xwdtopnm> to display handy information about the input "
"image and the conversion process"
msgstr ""
"Această opțiune face ca B<xwdtopnm> să afișeze informații utile despre "
"imaginea de intrare și procesul de conversie"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-headerdump>"
msgstr "B<-headerdump>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This option causes B<xwdtopnm> to display the contents of the X11 header.  "
"It has no effect when the input is X10.  This option was new in Netpbm 10.26 "
"(December 2004)."
msgstr ""
"Această opțiune face ca B<xwdtopnm> să afișeze conținutul antetului X11.  Nu "
"are niciun efect atunci când intrarea este X10.  Această opțiune a fost nouă "
"în Netpbm 10.26 (decembrie 2004)."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: SS
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Two Byte Samples"
msgstr "Două eșantioane de octeți"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<xwdtopnm> sometimes produces output with a maxval greater than 255, which "
"means the maximum value of a sample (one intensity value, e.g. the red "
"component of a pixel) is greater than 255 and therefore each sample takes 2 "
"bytes to represent.  This can be a problem because some programs expect "
"those bytes in a different order from what the Netpbm format specs say, "
"which is what B<xwdtopnm> produces, which means they will see totally "
"different colors than they should.  B<xv> is one such program."
msgstr ""
"B<xwdtopnm> produce uneori o ieșire cu val.max. mai mare de 255, ceea ce "
"înseamnă că valoarea maximă a unui eșantion (o valoare de intensitate, de "
"exemplu, componenta roșie a unui pixel) este mai mare de 255 și, prin "
"urmare, fiecare eșantion necesită 2 octeți pentru a fi reprezentat. Acest "
"lucru poate fi o problemă deoarece unele programe se așteaptă ca acei octeți "
"să fie într-o ordine diferită de ceea ce spun specificațiile formatului "
"Netpbm, ceea ce produce B<xwdtopnm>, ceea ce înseamnă că vor vedea culori "
"total diferite față de cum ar trebui. B<xv> este un astfel de program."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If this is a problem (e.g. you want to look at the output of B<xwdtopnm> "
"with B<xv>), there are two ways to fix it:"
msgstr ""
"Dacă aceasta este o problemă (de exemplu, doriți să vă uitați la ieșirea lui "
"B<xwdtopnm> cu B<xv>), există două moduri de a rezolva problema:"

#. type: IP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Pass the output through B<pamendian> to produce the format the program "
"expects."
msgstr ""
"Treceți ieșirea prin B<pamendian> pentru a produce formatul pe care îl "
"așteaptă programul."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Pass the output through B<pamdepth> to reduce the maxval below 256 so there "
"is only one byte per sample."
msgstr ""
"Treceți ieșirea prin B<pamdepth> pentru a reduce valoarea maximă sub 256, "
"astfel încât să existe doar un octet pe eșantion."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Often, there is no good reason to have a maxval greater than 255.  It "
"happens because in XWD, but not PNM, each color component of a pixel can "
"have different resolution, for example 5 bits for blue (maxval 31), 5 bits "
"for red (maxval 31), and 6 bits for green (maxval 63), for a total of 16 "
"bits per pixel.  In order to reproduce the colors as closely as possible, "
"B<xwdtopnm> has to use a large maxval.  In this example, it would use 31 * "
"63 = 1953, and use 48 bits per pixel."
msgstr ""
"De multe ori, nu există niciun motiv întemeiat pentru a avea o valoare "
"maximă mai mare de 255. Acest lucru se întâmplă deoarece în XWD, dar nu și "
"în PNM, fiecare componentă de culoare a unui pixel poate avea o rezoluție "
"diferită, de exemplu 5 biți pentru albastru (val.max. 31), 5 biți pentru "
"roșu (val.max. 31) și 6 biți pentru verde (val.max. 63), pentru un total de "
"16 biți pe pixel. Pentru a reproduce culorile cât mai fidel posibil, "
"B<xwdtopnm> trebuie să utilizeze un maxval mare. În acest exemplu, ar trebui "
"să folosească 31 * 63 = 1953 și să utilizeze 48 de biți pe pixel."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Because this is a common and frustrating problem when using B<xwdtopnm>, the "
"program issues a warning whenever it generates output with two byte "
"samples.  You can quiet this warning with the B<-quiet> E<.UR index."
"html#commonoptions> common option E<.UE> \\&.  The warning was new in Netpbm "
"10.46 (March 2009)."
msgstr ""
"Deoarece aceasta este o problemă comună și frustrantă atunci când se "
"utilizează B<xwdtopnm>, programul emite un avertisment ori de câte ori "
"generează o ieșire cu eșantioane de doi octeți. Puteți să reduceți acest "
"avertisment la tăcere cu opțiunea comună B<-quiet> E<.UR index."
"html#commonoptions> E<.UE> \\&. Avertismentul a fost nou în Netpbm 10.46 "
"(martie 2009)."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"B<pnmtoxwd>(1)  \\&, B<pamendian>(1)  \\&, B<pamdepth>(1)  \\&, B<pnm>(1)  "
"\\&, B<xwd> man page"
msgstr ""
"paginile de manual: B<pnmtoxwd>(1)  \\&, B<pamendian>(1)  \\&, "
"B<pamdepth>(1)  \\&, B<pnm>(1)  \\&, B<xwd>"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copyright (C) 1989, 1991 by Jef Poskanzer."
msgstr "Drepturi de autor © 1989, 1991 pentru Jef Poskanzer."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DOCUMENT SOURCE"
msgstr "SURSA DOCUMENTULUI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This manual page was generated by the Netpbm tool 'makeman' from HTML "
"source.  The master documentation is at"
msgstr ""
"Această pagină de manual a fost generată de instrumentul Netpbm «makeman» "
"din sursa HTML. Documentația principală este la"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<http://netpbm.sourceforge.net/doc/xwdtopnm.html>"
msgstr "B<http://netpbm.sourceforge.net/doc/xwdtopnm.html>"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "8 January 2010"
msgstr "8 ianuarie 2010"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<xwdtopnm> sometimes produces output with a maxval greater than 255, which "
"means the maximum value of a sample (one intensity value, e.g. the red "
"component of a pixel) is greater than 255 and therefore each sample takes 2 "
"bytes to represent.  This can be a problem because some programs expect "
"those bytes in a different order from what the Netpbm format specs say, "
"which is what B<xwdtopnm> produces, which means they will see totally "
"different colors that they should.  B<xv> is one such program."
msgstr ""
"B<xwdtopnm> produce uneori o ieșire cu val.max. mai mare de 255, ceea ce "
"înseamnă că valoarea maximă a unui eșantion (o valoare de intensitate, de "
"exemplu, componenta roșie a unui pixel) este mai mare de 255 și, prin "
"urmare, fiecare eșantion necesită 2 octeți pentru a fi reprezentat. Acest "
"lucru poate fi o problemă deoarece unele programe se așteaptă ca acești "
"octeți să fie într-o ordine diferită de cea prevăzută în specificațiile "
"formatului Netpbm, ceea ce produce B<xwdtopnm>, ceea ce înseamnă că vor "
"vedea culori total diferite de cele care ar trebui. B<xv> este un astfel de "
"program."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Often, there is no good reason to have a maxval greater than 255.  It "
"happens because in XWD, byte not PNM, each color component of a pixel can "
"have different resolution, for example 5 bits for blue (maxval 31), 5 bits "
"for red (maxval 31), and 6 bits for green (maxval 63), for a total of 16 "
"bits per pixel.  In order to reproduce the colors as closely as possible, "
"B<xwdtopnm> has to use a large maxval.  In this example, it would use 31 * "
"63 = 1953, and use 48 bits per pixel."
msgstr ""
"De multe ori, nu există niciun motiv întemeiat pentru a avea o valoare "
"maximă mai mare de 255. Acest lucru se întâmplă deoarece în XWD, nu și în "
"PNM, fiecare componentă de culoare a unui pixel poate avea o rezoluție "
"diferită, de exemplu 5 biți pentru albastru (val.max. 31), 5 biți pentru "
"roșu (val.max. 31) și 6 biți pentru verde (val.max. 63), pentru un total de "
"16 biți pe pixel. Pentru a reproduce culorile cât mai fidel posibil, "
"B<xwdtopnm> trebuie să utilizeze o val.max. mare. În acest exemplu, ar "
"trebui să folosească 31 * 63 = 1953 și să utilizeze 48 de biți pe pixel."

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pnmtoxwd>(1)  \\&, B<pamendian>(1)  \\&, B<pamdepth>(1)  \\&, B<pnm>(5)  "
"\\&, B<xwd> man page"
msgstr ""
"paginile de manual: B<pnmtoxwd>(1)  \\&, B<pamendian>(1)  \\&, "
"B<pamdepth>(1)  \\&, B<pnm>(5)  \\&, B<xwd>"
