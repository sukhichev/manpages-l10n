# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2021-12-25 19:36+0100\n"
"PO-Revision-Date: 2024-02-07 19:32+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: ds C+
#: archlinux
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#. type: ds :
#: archlinux
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"
msgstr "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"

#. type: ds 8
#: archlinux
#, no-wrap
msgid "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"

#. type: ds o
#: archlinux
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"
msgstr "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"

#. type: ds d-
#: archlinux
#, no-wrap
msgid "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"

#. type: ds D-
#: archlinux
#, no-wrap
msgid "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"
msgstr "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"

#. type: ds th
#: archlinux
#, no-wrap
msgid "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"
msgstr "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"

#. type: ds Th
#: archlinux
#, no-wrap
msgid "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"
msgstr "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"

#. type: ds ae
#: archlinux
#, no-wrap
msgid "a\\h'-(\\w'a'u*4/10)'e"
msgstr "a\\h'-(\\w'a'u*4/10)'e"

#. type: ds Ae
#: archlinux
#, no-wrap
msgid "A\\h'-(\\w'A'u*4/10)'E"
msgstr "A\\h'-(\\w'A'u*4/10)'E"

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "Title"
msgstr "Titlu"

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "PACCHECK 1"
msgstr "PACCHECK 1"

#. type: TH
#: archlinux
#, no-wrap
msgid "PACCHECK"
msgstr "PACCHECK"

#. type: TH
#: archlinux
#, no-wrap
msgid "2021-08-14"
msgstr "14 august 2021"

#. type: TH
#: archlinux
#, no-wrap
msgid "pacutils"
msgstr "pacutils"

#. type: TH
#: archlinux
#, no-wrap
msgid "paccheck"
msgstr "paccheck"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid "paccheck - check installed packages"
msgstr "paccheck - verifică pachetele instalate"

#. type: IX
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: IX
#: archlinux
#, no-wrap
msgid "Header"
msgstr "Antet"

#. type: Plain text
#: archlinux
msgid ""
"\\& paccheck [options] [E<lt>packageE<gt>]...  \\& paccheck (--help|--"
"version)"
msgstr ""
"\\& paccheck [opțiuni] [E<lt>pachetE<gt>]...  \\& paccheck (--help|--version)"

#. type: IX
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid ""
"Check installed packages.  Additional packages may be specified on "
"I<stdin>.  If no package are provided, all installed packages will be "
"checked.  By default only package dependencies and basic file information "
"will checked."
msgstr ""
"Verifică pachetele instalate. Pachete suplimentare pot fi specificate la "
"I<intrarea standard>. Dacă nu se furnizează niciun pachet, vor fi verificate "
"toate pachetele instalate. În mod implicit, vor fi verificate doar "
"dependențele pachetelor și informațiile de bază despre fișiere."

#. type: IX
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--config>=I<path>"
msgstr "B<--config>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "Item"
msgstr "Element"

#. type: IX
#: archlinux
#, no-wrap
msgid "--config=path"
msgstr "--config=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate configuration file path."
msgstr "Stabilește o rută alternativă pentru fișierul de configurare."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--dbpath>=I<path>"
msgstr "B<--dbpath>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--dbpath=path"
msgstr "--dbpath=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate database path."
msgstr "Stabilește o rută alternativă pentru baza de date."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--root>=I<path>"
msgstr "B<--root>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--root=path"
msgstr "--root=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate installation root."
msgstr "Stabilește o rădăcină de instalare alternativă."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--sysroot>=I<path>"
msgstr "B<--sysroot>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--sysroot=path"
msgstr "--sysroot=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate system root.  See B<pacutils-sysroot>\\|(7)."
msgstr ""
"Stabilește o rădăcină alternativă a sistemului. A se vedea B<pacutils-"
"sysroot>\\|(7)."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--null>[=I<sep>]"
msgstr "B<--null>[=I<separator>]"

#. type: IX
#: archlinux
#, no-wrap
msgid "--null[=sep]"
msgstr "--null[=separator]"

#. type: Plain text
#: archlinux
msgid ""
"Set an alternate separator for values parsed from I<stdin>.  By default a "
"newline CW<\\*(C`\\en\\*(C'> is used as the separator.  If B<--null> is used "
"without specifying I<sep> CW<\\*(C`NUL\\*(C'> will be used."
msgstr ""
"Stabilește un separator alternativ pentru valorile analizate din I<intrarea-"
"standard>. În mod implicit, ca separator se utilizează o nouă linie "
"CW<\\*(C`\\en\\*(C'>. Dacă se utilizează B<--null> fără a se specifica "
"I<separator>, se va folosi CW<\\*(C`NUL\\*(C'>."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--list-broken>"
msgstr "B<--list-broken>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--list-broken"
msgstr "--list-broken"

#. type: Plain text
#: archlinux
msgid "Only print the names of packages that fail the selected checks."
msgstr ""
"Afișează numai numele pachetelor care nu au trecut verificările selectate."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--quiet>"
msgstr "B<--quiet>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--quiet"
msgstr "--quiet"

#. type: Plain text
#: archlinux
msgid "Only display messages if a problem is found."
msgstr "Afișează mesaje numai dacă se găsește o problemă."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--recursive>"
msgstr "B<--recursive>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--recursive"
msgstr "--recursive"

#. type: Plain text
#: archlinux
msgid "Recursively perform checks on packages' dependencies as well."
msgstr ""
"Efectuează în mod recurent verificări și asupra dependențelor pachetelor."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--depends>"
msgstr "B<--depends>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--depends"
msgstr "--depends"

#. type: Plain text
#: archlinux
msgid "Check that all package dependencies are satisfied."
msgstr "Verifică dacă toate dependențele pachetului sunt îndeplinite."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--opt-depends>"
msgstr "B<--opt-depends>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--opt-depends"
msgstr "--opt-depends"

#. type: Plain text
#: archlinux
msgid "Check that all package optional dependencies are satisfied."
msgstr ""
"Verifică dacă toate dependențele opționale ale pachetului sunt îndeplinite."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--files>"
msgstr "B<--files>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--files"
msgstr "--files"

#. type: Plain text
#: archlinux
msgid "Check package files against the local database."
msgstr "Verifică fișierele de pachete în baza de date locală."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--file-properties>"
msgstr "B<--file-properties>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--file-properties"
msgstr "--file-properties"

#. type: Plain text
#: archlinux
msgid "Check package files against \\s-1MTREE\\s0 data."
msgstr "Verifică fișierele pachetului în raport cu datele din „MTREE”."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--md5sum>"
msgstr "B<--md5sum>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--md5sum"
msgstr "--md5sum"

#. type: Plain text
#: archlinux
msgid "Check file md5sums against \\s-1MTREE\\s0 data."
msgstr "Verifică sumele md5 ale fișierelor cu datele din „MTREE”."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--sha256sum>"
msgstr "B<--sha256sum>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--sha256sum"
msgstr "--sha256sum"

#. type: Plain text
#: archlinux
msgid "Check file sha256sums against \\s-1MTREE\\s0 data."
msgstr "Verifică sumele sha256 ale fișierelor cu datele din „MTREE”."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--require-mtree>"
msgstr "B<--require-mtree>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--require-mtree"
msgstr "--require-mtree"

#. type: Plain text
#: archlinux
msgid ""
"Treat missing \\s-1MTREE\\s0 data as an error for B<--db-files> and/or "
"\\&B<--file-properties>."
msgstr ""
"Tratați datele lipsă din „MTREE” ca o eroare pentru B<--db-files> și/sau "
"\\&B<--file-properties>."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--db-files>"
msgstr "B<--db-files>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--db-files"
msgstr "--db-files"

#. type: Plain text
#: archlinux
msgid ""
"Include database files in B<--files> and B<--file-properties> checks.  "
"\\&B<--files> will test for the existence of I<desc>, I<files>, and I<mtree> "
"(with \\&B<--require-mtree>) files in the package database entry.  B<--file-"
"properties> will check I<install> and I<changelog> files in the package "
"database where applicable."
msgstr ""
"Include fișierele de baze de date în verificările B<--files> și B<--file-"
"properties>. \\&B<--files> va testa existența fișierelor I<desc>, I<files> "
"și I<mtree> (cu \\&B<--require-mtree>) în intrarea în baza de date a "
"pachetului. B<--file-properties> va verifica fișierele I<install> și "
"I<changelog> din baza de date a pachetului, dacă este cazul."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--backup>"
msgstr "B<--backup>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--backup"
msgstr "--backup"

#. type: Plain text
#: archlinux
msgid "Include backup files in file modification checks."
msgstr ""
"Include fișierele de copie de rezervă în verificările de modificare a "
"fișierelor."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--noextract>"
msgstr "B<--noextract>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--noextract"
msgstr "--noextract"

#. type: Plain text
#: archlinux
msgid "Include NoExtract files in file modification checks."
msgstr ""
"Include fișierele NoExtract în verificările de modificare a fișierelor."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--noupgrade>"
msgstr "B<--noupgrade>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--noupgrade"
msgstr "--noupgrade"

#. type: Plain text
#: archlinux
msgid "Include NoUpgrade files in file modification checks."
msgstr ""
"Include fișierele NoUpgrade în verificările de modificare a fișierelor."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--help"
msgstr "--help"

#. type: Plain text
#: archlinux
msgid "Display usage information and exit."
msgstr "Afișează informațiile de utilizare și iese."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--version"
msgstr "--version"

#. type: Plain text
#: archlinux
msgid "Display version information and exit."
msgstr "Afișează informațiile despre versiune și iese."

#. type: IX
#: archlinux
#, no-wrap
msgid "CAVEATS"
msgstr "AVERTISMENTE"

#. type: Plain text
#: archlinux
msgid ""
"\\&B<paccheck> determines whether or not to read packages from I<stdin> "
"based on a naive check using B<isatty>\\|(3).  If B<paccheck> is called in "
"an environment, such as a shell function or script being used in a pipe, "
"where I<stdin> is not connected to a terminal but does not contain packages "
"to check, B<paccheck> should be called with I<stdin> closed.  For POSIX-"
"compatible shells, this can be done with CW<\\*(C`E<lt>&-\\*(C'>."
msgstr ""
"\\&B<paccheck> determină dacă trebuie sau nu să citească pachete de la "
"I<intrarea standard> pe baza unei verificări simple folosind B<isatty>\\|"
"(3). Dacă B<paccheck> este apelat într-un mediu, cum ar fi o funcție de "
"shell sau un script utilizat într-o conductă, în care I<intrarea standard> "
"nu este conectată la un terminal, dar nu conține pachete de verificat, "
"B<paccheck> trebuie apelat cu I<intrarea standard> închisă. Pentru shell-"
"urile compatibile POSIX, acest lucru se poate face cu CW<\\*(C`E<lt>&-"
"\\*(C'>."
