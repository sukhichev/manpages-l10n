# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-02-09 16:59+0100\n"
"PO-Revision-Date: 2023-05-23 09:08+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MKPASSWD-PBKDF2"
msgstr "GRUB-MKPASSWD-PBKDF2"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "decembrie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-1"
msgstr "GRUB 2:2.12-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-mkpasswd-pbkdf2 - generate hashed password for GRUB"
msgstr ""
"grub-mkpasswd-pbkdf2 - generează parolă tip sumă de control pentru GRUB"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-mkpasswd-pbkdf2> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-mkpasswd-pbkdf2> [I<\\,OPȚIUNE\\/>...] [I<\\,OPȚIUNI\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Generate PBKDF2 password hash."
msgstr "Generează suma de control pentru parola PBKDF2."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--iteration-count>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--iteration-count>=I<\\,NUMĂR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Number of PBKDF2 iterations"
msgstr "Numărul de iterații PBKDF2"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-l>, B<--buflen>=I<\\,NUM\\/>"
msgstr "B<-l>, B<--buflen>=I<\\,NUMĂR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Length of generated hash"
msgstr "Lungimea sumei de control generată"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--salt>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--salt>=I<\\,NUMĂR\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Length of salt"
msgstr "Lungimea „salt”-ului"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Raportați erorile la E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mkpasswd-pbkdf2> is maintained as a "
"Texinfo manual.  If the B<info> and B<grub-mkpasswd-pbkdf2> programs are "
"properly installed at your site, the command"
msgstr ""
"Documentația completă pentru B<grub-mkpasswd-pbkdf2> este menținută ca un "
"manual Texinfo. Dacă programele B<info> și B<grub-mkpasswd-pbkdf2> sunt "
"instalate corect în sistemul dvs., comanda"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-mkpasswd-pbkdf2>"
msgstr "B<info grub-mkpasswd-pbkdf2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "October 2023"
msgstr "octombrie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13+deb12u1"
msgstr "GRUB 2.06-13+deb12u1"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "January 2024"
msgstr "ianuarie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-1"
msgstr "GRUB 2.12-1"
