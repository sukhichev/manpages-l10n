# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2021-12-25 19:36+0100\n"
"PO-Revision-Date: 2023-12-15 12:04+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: ds C+
#: archlinux
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#. type: ds :
#: archlinux
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"
msgstr "\\k:\\h'-(\\n(.wu*8/10-\\*(#H+.1m+\\*(#F)'\\v'-\\*(#V'\\z.\\h'.2m+\\*(#F'.\\h'|\\n:u'\\v'\\*(#V'"

#. type: ds 8
#: archlinux
#, no-wrap
msgid "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(*b\\h'-\\*(#H'"

#. type: ds o
#: archlinux
#, no-wrap
msgid "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"
msgstr "\\k:\\h'-(\\n(.wu+\\w'\\(de'u-\\*(#H)/2u'\\v'-.3n'\\*(#[\\z\\(de\\v'.3n'\\h'|\\n:u'\\*(#]"

#. type: ds d-
#: archlinux
#, no-wrap
msgid "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"
msgstr "\\h'\\*(#H'\\(pd\\h'-\\w'~'u'\\v'-.25m'I<\\(hy>\\v'.25m'\\h'-\\*(#H'"

#. type: ds D-
#: archlinux
#, no-wrap
msgid "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"
msgstr "D\\k:\\h'-\\w'D'u'\\v'-.11m'\\z\\(hy\\v'.11m'\\h'|\\n:u'"

#. type: ds th
#: archlinux
#, no-wrap
msgid "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"
msgstr "\\*(#[\\v'.3m'\\s+1I\\s-1\\v'-.3m'\\h'-(\\w'I'u*2/3)'\\s-1o\\s+1\\*(#]"

#. type: ds Th
#: archlinux
#, no-wrap
msgid "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"
msgstr "\\*(#[\\s+2I\\s-2\\h'-\\w'I'u*3/5'\\v'-.3m'o\\v'.3m'\\*(#]"

#. type: ds ae
#: archlinux
#, no-wrap
msgid "a\\h'-(\\w'a'u*4/10)'e"
msgstr "a\\h'-(\\w'a'u*4/10)'e"

#. type: ds Ae
#: archlinux
#, no-wrap
msgid "A\\h'-(\\w'A'u*4/10)'E"
msgstr "A\\h'-(\\w'A'u*4/10)'E"

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "Title"
msgstr "Titlu"

#.  ========================================================================
#. type: IX
#: archlinux
#, no-wrap
msgid "PACREPAIRFILE 1"
msgstr "PACREPAIRFILE 1"

#. type: TH
#: archlinux
#, no-wrap
msgid "PACREPAIRFILE"
msgstr "PACREPAIRFILE"

#. type: TH
#: archlinux
#, no-wrap
msgid "2021-08-14"
msgstr "14 august 2021"

#. type: TH
#: archlinux
#, no-wrap
msgid "pacutils"
msgstr "pacutils"

#. type: TH
#: archlinux
#, no-wrap
msgid "pacrepairfile"
msgstr "pacrepairfile"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid "pacrepairfile - reset properties on alpm-managed files"
msgstr ""
"pacrepairfile - restabilește proprietățile fișierelor gestionate de alpm"

#. type: IX
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: IX
#: archlinux
#, no-wrap
msgid "Header"
msgstr "Antet"

#. type: Plain text
#: archlinux
msgid ""
"\\& pacrepairfile [options] (--gid|--mode|--mtime|--uid)... "
"E<lt>fileE<gt>...  \\& pacrepairfile (--help|--version)"
msgstr ""
"\\& pacrepairfile [opțiuni] (--gid|--mode|--mtime|--uid)... "
"E<lt>fișierE<gt>...  \\& pacrepairfile (--help|--version)"

#. type: IX
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid ""
"Resets file properties for alpm-managed files based on \\s-1MTREE\\s0 data."
msgstr ""
"Restabilește proprietățile fișierelor pentru fișierele gestionate de alpm pe "
"baza datelor furnizate de \\s-1MTREE\\s0."

#. type: Plain text
#: archlinux
msgid ""
"If I<stdin> is not connected to a terminal, files will be read from I<stdin>."
msgstr ""
"Dacă I<stdin> (intrarea standard) nu este conectată la un terminal, "
"pachetele vor fi citite de la I<stdin>."

#. type: IX
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--config>=I<path>"
msgstr "B<--config>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "Item"
msgstr "Element"

#. type: IX
#: archlinux
#, no-wrap
msgid "--config=path"
msgstr "--config=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate configuration file path."
msgstr "Stabilește o rută alternativă pentru fișierul de configurare."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--dbpath>=I<path>"
msgstr "B<--dbpath>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--dbpath=path"
msgstr "--dbpath=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate database path."
msgstr "Stabilește o rută alternativă pentru baza de date."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--root>=I<path>"
msgstr "B<--root>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--root=path"
msgstr "--root=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate installation root."
msgstr "Stabilește o rădăcină de instalare alternativă."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--sysroot>=I<path>"
msgstr "B<--sysroot>=I<ruta>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--sysroot=path"
msgstr "--sysroot=ruta"

#. type: Plain text
#: archlinux
msgid "Set an alternate system root.  See B<pacutils-sysroot>\\|(7)."
msgstr ""
"Stabilește o rădăcină alternativă a sistemului. A se vedea B<pacutils-"
"sysroot>\\|(7)."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--quiet>"
msgstr "B<--quiet>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--quiet"
msgstr "--quiet"

#. type: Plain text
#: archlinux
msgid "Do not display progress information."
msgstr "Nu afișează informații privind progresul."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--package>=I<pkgname>"
msgstr "B<--package>=I<nume-pachet>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--package=pkgname"
msgstr "--package=nume-pachet"

#. type: Plain text
#: archlinux
msgid ""
"Search I<pkgname> for file properties.  May be specified multiple times.  If "
"\\&B<--package> is not specified, all installed packages will be searched."
msgstr ""
"Caută I<nume-pachet> pentru proprietățile fișierelor.  Poate fi specificată "
"de mai multe ori.  Dacă \\&B<--pachet> nu este specificat, vor fi căutate "
"toate pachetele instalate."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--help"
msgstr "--help"

#. type: Plain text
#: archlinux
msgid "Display usage information and exit."
msgstr "Afișează informațiile de utilizare și iese."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--version"
msgstr "--version"

#. type: Plain text
#: archlinux
msgid "Display version information and exit."
msgstr "Afișează informațiile despre versiune și iese."

#. type: IX
#: archlinux
#, no-wrap
msgid "Fields"
msgstr "Câmpuri"

#. type: IX
#: archlinux
#, no-wrap
msgid "Subsection"
msgstr "Subsecțiune"

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--gid>"
msgstr "B<--gid>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--gid"
msgstr "--gid"

#. type: Plain text
#: archlinux
msgid "Reset file owner group id."
msgstr "Restabilește ID-ul grupului proprietar al fișierului."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--mode>"
msgstr "B<--mode>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--mode"
msgstr "--mode"

#. type: Plain text
#: archlinux
msgid "Reset file permissions."
msgstr "Restabilește permisiunile fișierelor."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--mtime>"
msgstr "B<--mtime>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--mtime"
msgstr "--mtime"

#. type: Plain text
#: archlinux
msgid "Reset file modification time."
msgstr "Restabilește timpul de modificare a fișierului."

#. type: IP
#: archlinux
#, no-wrap
msgid "B<--uid>"
msgstr "B<--uid>"

#. type: IX
#: archlinux
#, no-wrap
msgid "--uid"
msgstr "--uid"

#. type: Plain text
#: archlinux
msgid "Reset file owner user id."
msgstr "Restabilește ID-ul de utilizator al proprietarului fișierului."

#. type: IX
#: archlinux
#, no-wrap
msgid "CAVEATS"
msgstr "AVERTISMENTE"

#. type: Plain text
#: archlinux
msgid ""
"\\&B<pacrepairfile> determines whether or not to read files from I<stdin> "
"based on a naive check using B<isatty>\\|(3).  If B<pacrepairfile> is called "
"in an environment, such as a shell function or script being used in a pipe, "
"where \\&I<stdin> is not connected to a terminal but does not contain files "
"to reset, \\&B<pacrepairfile> should be called with I<stdin> closed.  For "
"POSIX-compatible shells, this can be done with CW<\\*(C`E<lt>&-\\*(C'>."
msgstr ""
"\\&B<pacrepairfile> determină dacă trebuie sau nu să citească fișiere din "
"I<stdin> pe baza unei verificări sumare folosind B<isatty>\\|(3). Dacă "
"B<pacrepairfile> este apelat într-un mediu, cum ar fi o funcție de shell sau "
"un script utilizat într-o conductă, în care \\&I<stdin> nu este conectat la "
"un terminal, dar nu conține fișiere de restabilit, \\&B<pacrepairfile> "
"trebuie apelat cu I<stdin> închis. Pentru shell-urile compatibile POSIX, "
"acest lucru se poate face cu CW<\\*(C`E<lt>&-\\*(C'>."

#. type: Plain text
#: archlinux
msgid ""
"In order for B<pacrepairfile> to reset a file's properties, the package "
"which owns the file must have \\s-1MTREE\\s0 data."
msgstr ""
"Pentru ca B<pacrepairfile> să restabilească proprietățile unui fișier, "
"pachetul care deține fișierul trebuie să aibă date \\s-1MTREE\\s0."
