# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:58+0200\n"
"PO-Revision-Date: 2023-06-28 11:26+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "TARCAT"
msgstr "TARCAT"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "tarcat - concatenates the pieces of a GNU tar multi-volume archive"
msgstr "tarcat - concatenează părțile unei arhive multi-volum GNU tar"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<tarcat> files ..."
msgstr "B<tarcat> fișiere ..."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<tarcat> command simply concatenates the files from a GNU tar multi-"
"volume archive into a single tar archive."
msgstr ""
"Comanda B<tarcat> concatenează pur și simplu fișierele dintr-o arhivă multi-"
"volum GNU tar într-o singură arhivă tar."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<tar>(1)."
msgstr "B<tar>(1)."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The B<tarcat> script was written by Bruno Haible E<lt>bruno@clisp.orgE<gt> "
"and Sergey Poznyakoff E<lt>gray@gnu.org.uaE<gt>."
msgstr ""
"Scriptul B<tarcat> a fost scris de Bruno Haible E<lt>bruno@clisp.orgE<gt> și "
"Sergey Poznyakoff E<lt>gray@gnu.org.uaE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"This document was written by Bdale Garbee E<lt>bdale@gag.comE<gt> for Debian."
msgstr ""
"Acest document a fost scris de Bdale Garbee E<lt>bdale@gag.comE<gt> pentru "
"Debian."
