# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-11-05 09:31+0100\n"
"PO-Revision-Date: 2023-06-22 00:46+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "FIXFMPS"
msgstr "FIXFMPS"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "fixfmps - filter to fix Framemaker documents so PSUtils work"
msgstr ""
"fixfmps - filtru pentru a repara documentele Framemaker astfel încât PSUtils "
"să funcționeze cu acestea"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "B<fixfmps> E<lt> I<Framemaker.ps> E<gt> I<Fixed.ps>"
msgstr "B<fixfmps> E<lt> I<Framemaker.ps> E<gt> I<Remediat.ps>"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid ""
"I<Fixfmps> is a I<perl> filter which \"fixes\" PostScript from Framemaker so "
"that it works correctly with Angus Duggan's B<psutils> package."
msgstr ""
"I<Fixfmps> este un filtru I<perl> care „repară” PostScript din Framemaker "
"astfel încât să funcționeze corect cu pachetul B<psutils> al lui Angus "
"Duggan."

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Drepturi de autor © Angus J. C. Duggan 1991-1995"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "TRADEMARKS"
msgstr "MĂRCI ÎNREGISTRATE"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> este o marcă înregistrată a Adobe Systems Incorporated."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixmacps(1), fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), "
"fixwpps(1), fixwwps(1), extractres(1), includeres(1)"
msgstr ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixmacps(1), fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), "
"fixwpps(1), fixwwps(1), extractres(1), includeres(1)"
