# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-02-09 17:07+0100\n"
"PO-Revision-Date: 2023-07-02 19:07+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "RSH"
msgstr "RSH"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "decembrie 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GNU inetutils 2.5"
msgstr "GNU inetutils 2.5"

#. type: TH
#: archlinux
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux
msgid "rsh - Remote shell client"
msgstr "rsh - client de shell la distanță"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux
msgid ""
"B<rsh> [I<\\,OPTION\\/>...] [I<\\,USER@\\/>]I<\\,HOST \\/>[I<\\,COMMAND \\/"
">[I<\\,ARG\\/>...]]"
msgstr ""
"B<rsh> [I<\\,OPȚIUNE\\/>...] [I<\\,UTILIZATOR@\\/>]I<\\,GAZDA \\/>[I<\\,"
"COMANDA \\/>[I<\\,ARG\\/>...]]"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux
msgid "remote shell"
msgstr "shell la distanță"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-4>, B<--ipv4>"
msgstr "B<-4>, B<--ipv4>"

#. type: Plain text
#: archlinux
msgid "use only IPv4"
msgstr "utilizează doar ipv4"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-6>, B<--ipv6>"
msgstr "B<-6>, B<--ipv6>"

#. type: Plain text
#: archlinux
msgid "use only IPv6"
msgstr "utilizează doar ipv6"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-8>, B<--8-bit>"
msgstr "B<-8>, B<--8-bit>"

#. type: Plain text
#: archlinux
msgid "allows an eight-bit input data path at all times"
msgstr "permite o cale de date de intrare pe opt biți în orice moment"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--debug>"
msgstr "B<-d>, B<--debug>"

#. type: Plain text
#: archlinux
msgid "turns on socket debugging (see setsockopt(2))"
msgstr "activează depanarea soclului ( a se vedea setsockopt(2))"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-e>, B<--escape>=I<\\,CHAR\\/>"
msgstr "B<-e>, B<--escape>=I<\\,CARACTER\\/>"

#. type: Plain text
#: archlinux
msgid "allows user specification of the escape character (``~'' by default)"
msgstr ""
"permite specificarea de către utilizator a caracterului de eludare, („~'” în "
"mod implicit)."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-l>, B<--user>=I<\\,USER\\/>"
msgstr "B<-l>, B<--user>=I<\\,UTILIZATOR\\/>"

#. type: Plain text
#: archlinux
msgid "run as USER on the remote system"
msgstr "rulează ca UTILIZATOR pe sistemul de la distanță"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-n>, B<--no-input>"
msgstr "B<-n>, B<--no-input>"

#. type: Plain text
#: archlinux
msgid "use I<\\,/dev/null\\/> as input"
msgstr "utilizează I<\\,/dev/null\\/> ca intrare"

#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "oferă această listă de ajutor"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "oferă un mesaj de utilizare scurt"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "afișează versiunea programului"

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Argumentele obligatorii sau opționale pentru opțiunile lungi sunt "
"obligatorii sau opționale și pentru opțiunile corespunzătoare scurte."

#. type: SH
#: archlinux
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux
msgid "Written by many authors."
msgstr "Scris de mai mulți autori."

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-inetutils@gnu.orgE<gt>."
msgstr "Raportați erorile la: E<lt>bug-inetutils@gnu.orgE<gt>."

#. type: SH
#: archlinux
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: archlinux
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2023 Free Software Foundation, Inc. Licența GPLv3+: "
"GNU GPL versiunea 3 sau ulterioară E<lt>https://gnu.org/licenses/gpl."
"htmlE<gt>."

#. type: Plain text
#: archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux
msgid "rshd(1)"
msgstr "rshd(1)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<rsh> is maintained as a Texinfo manual.  If the "
"B<info> and B<rsh> programs are properly installed at your site, the command"
msgstr ""
"Documentația completă pentru B<rsh> este menținută ca un manual Texinfo. "
"Dacă programele B<info> și B<rsh> sunt instalate corect pe sistemul "
"dumneavoastră, comanda"

#. type: Plain text
#: archlinux
msgid "B<info rsh>"
msgstr "B<info rsh>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."
