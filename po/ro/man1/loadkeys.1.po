# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:02+0100\n"
"PO-Revision-Date: 2024-02-13 17:20+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LOADKEYS"
msgstr "LOADKEYS"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "6 Feb 1994"
msgstr "6 februarie 1994"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "loadkeys - load keyboard translation tables"
msgstr "loadkeys - încarcă tabelele de conversie a tastaturii"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<loadkeys> [I<\\,OPTION\\/>]... I<\\,FILENAME\\/>..."
msgstr "B<loadkeys> [I<\\,OPȚIUNE\\/>]... I<\\,NUME_FIȘIER\\/>..."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<loadkeys> I<--default>"
msgstr "B<loadkeys> I<--default>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<loadkeys> I<--mktable>"
msgstr "B<loadkeys> I<--mktable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<loadkeys> I<--bkeymap>"
msgstr "B<loadkeys> I<--bkeymap>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<loadkeys> I<--parse>"
msgstr "B<loadkeys> I<--parse>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: IX
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "loadkeys command"
msgstr "comanda loadkeys"

#. type: IX
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "\\fLloadkeys\\fR command"
msgstr "comanda «loadkeys»"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The program B<loadkeys> reads the file or files specified by I<FILENAME..."
">.  Its main purpose is to load the kernel keymap for the console.  You can "
"specify console device by the I<-C> (or I<--console> ) option."
msgstr ""
"Programul B<loadkeys> citește fișierul sau fișierele specificate de "
"I<NUME_FIȘIER...>. Scopul său principal este de a încărca schema de taste a "
"nucleului pentru consolă. Puteți specifica dispozitivul de consolă prin "
"opțiunea I<-C> (sau I<--console> )."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RESET TO DEFAULT"
msgstr "REVENIREA LA VALORILE IMPLICITE"

#. type: Plain text
#: archlinux opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the I<-d> (or I<--default> ) option is given, B<loadkeys> loads a default "
"keymap, probably the file I<defkeymap.map> either in I</usr/share/kbd/"
"keymaps> or in I</usr/src/linux/drivers/char>.  (Probably the former was "
"user-defined, while the latter is a qwerty keyboard map for PCs - maybe not "
"what was desired.)  Sometimes, with a strange keymap loaded (with the minus "
"on some obscure unknown modifier combination) it is easier to type `loadkeys "
"defkeymap'."
msgstr ""
"Dacă este dată opțiunea I<-d> (sau I<--default> ), B<loadkeys> încarcă o "
"schemă de taste implicită, probabil fișierul I<defkeymap.map> fie din I</usr/"
"share/kbd/keymaps>, fie din I</usr/src/linux/drivers/char>; (probabil că "
"primul a fost definit de utilizator, în timp ce al doilea este o schemă de "
"tastatură „qwerty” pentru PC-uri - poate nu ceea ce s-a dorit). Uneori, cu o "
"schemă de taste ciudată încărcată (cu minus pe o combinație obscură de "
"modificatori necunoscută), este mai ușor să se tasteze «loadkeys defkeymap»."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LOAD KERNEL KEYMAP"
msgstr "ÎNCĂRCAREA SCHEMEI DE TASTE A NUCLEULUI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The main function of B<loadkeys> is to load or modify the keyboard driver's "
"translation tables.  When specifying the file names, standard input can be "
"denoted by dash (-). If no file is specified, the data is read from the "
"standard input."
msgstr ""
"Funcția principală a B<loadkeys> este de a încărca sau modifica tabelele de "
"conversie ale controlorului de tastatură. La specificarea numelor de "
"fișiere, intrarea standard poate fi indicată prin liniuță (-). Dacă nu se "
"specifică niciun fișier, datele sunt citite de la intrarea standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For many countries and keyboard types appropriate keymaps are available "
"already, and a command like `loadkeys uk' might do what you want. On the "
"other hand, it is easy to construct one's own keymap. The user has to tell "
"what symbols belong to each key. She can find the keycode for a key by use "
"of B<showkey>(1), while the keymap format is given in B<keymaps>(5)  and can "
"also be seen from the output of B<dumpkeys>(1)."
msgstr ""
"Pentru multe țări și tipuri de tastaturi sunt deja disponibile scheme de "
"taste adecvate, iar o comandă precum «loadkeys ro» ar putea face ceea ce vă "
"doriți. Pe de altă parte, este ușor să vă construiți propria schemă de "
"taste. Utilizatorul trebuie să spună ce simboluri aparțin fiecărei taste. El "
"poate afla codul unei taste folosind B<showkey>(1), în timp ce formatul "
"schemei de taste este dat în B<keymaps>(5) și poate fi văzut, de asemenea, "
"din ieșirea lui B<dumpkeys>(1)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LOAD KERNEL ACCENT TABLE"
msgstr "ÎNCĂRCAREA TABELULUI DE ACCENTE (DIACRITICE) AL NUCLEULUI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the input file does not contain any compose key definitions, the kernel "
"accent table is left unchanged, unless the I<-c> (or I<--clearcompose> ) "
"option is given, in which case the kernel accent table is emptied.  If the "
"input file does contain compose key definitions, then all old definitions "
"are removed, and replaced by the specified new entries.  The kernel accent "
"table is a sequence of (by default 68) entries describing how dead "
"diacritical signs and compose keys behave.  For example, a line"
msgstr ""
"În cazul în care fișierul de intrare nu conține definiții ale tastelor de "
"compunere, tabelul de accente din nucleu este lăsat neschimbat, cu excepția "
"cazului în care se furnizează opțiunea I<-c> (sau I<--clearcompose> ), caz "
"în care tabelul de accente din nucleu este golit. În cazul în care fișierul "
"de intrare conține definiții ale tastelor de compunere, toate definițiile "
"vechi sunt eliminate și înlocuite cu noile intrări specificate. Tabelul de "
"accente din nucleu este o secvență de intrări (în mod implicit 68) care "
"descriu modul în care se comportă semnele diacritice moarte și cheile de "
"compunere. De exemplu, o linie"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "compose ',' 'c' to ccedilla"
msgstr "compose ',' 'c' to ccedilla"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"means that E<lt>ComposeKeyE<gt>E<lt>,E<gt>E<lt>cE<gt> must be combined to "
"E<lt>ccedillaE<gt>.  The current content of this table can be see using "
"`dumpkeys --compose-only'."
msgstr ""
"înseamnă că tasta de compunere E<lt>ComposeKeyE<gt>E<lt>,E<gt>E<lt>cE<gt> "
"trebuie să fie combinată cu E<lt>ccedillaE<gt>. Conținutul actual al acestui "
"tabel poate fi văzut folosind «dumpkeys --compose-only»."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LOAD KERNEL STRING TABLE"
msgstr "ÎNCĂRCAREA TABELULUI DE ȘIRURI AL NUCLEULUI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The option I<-s> (or I<--clearstrings> ) clears the kernel string table. If "
"this option is not given, B<loadkeys> will only add or replace strings, not "
"remove them.  (Thus, the option -s is required to reach a well-defined "
"state.)  The kernel string table is a sequence of strings with names like "
"F31. One can make function key F5 (on an ordinary PC keyboard) produce the "
"text `Hello!', and Shift+F5 `Goodbye!' using lines"
msgstr ""
"Opțiunea I<-s> (sau I<--clearstrings> ) șterge tabelul de șiruri al "
"nucleului. Dacă această opțiune nu este dată, B<loadkeys> va adăuga sau "
"înlocui doar șirurile, nu le va șterge; (astfel, opțiunea -s este necesară "
"pentru a ajunge la o stare bine definită). Tabelul de șiruri al nucleului "
"este o secvență de șiruri cu nume precum F31. Se poate face ca tasta de "
"funcție F5 (pe o tastatură obișnuită de PC) să producă textul „Salut!”, iar "
"Shift+F5 „La revedere!” folosind liniile"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "keycode 63 = F70 F71"
msgstr "keycode 63 = F70 F71"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "string F70 = \"Hello!\""
msgstr "string F70 = \"Salut!\""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "string F71 = \"Goodbye!\""
msgstr "string F71 = \"La revedere!\""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"in the keymap.  The default bindings for the function keys are certain "
"escape sequences mostly inspired by the VT100 terminal."
msgstr ""
"în schema de taste.  Legăturile implicite pentru tastele de funcții sunt "
"anumite secvențe de control inspirate în principal de terminalul VT100."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CREATE KERNEL SOURCE TABLE"
msgstr "CREAREA TABELULUI SURSĂ AL NUCLEULUI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the I<-m> (or I<--mktable> ) option is given B<loadkeys> prints to the "
"standard output a file that may be used as I</usr/src/linux\\%/drivers\\%/"
"char\\%/defkeymap.c,> specifying the default key bindings for a kernel (and "
"does not modify the current keymap)."
msgstr ""
"Dacă este dată opțiunea I<-m> (sau I<--mktable> ) B<loadkeys> imprimă la "
"ieșirea standard un fișier care poate fi utilizat ca I</usr/src/linux\\%/"
"drivers\\%/char\\%/defkeymap.c>, specificând combinațiile de taste implicite "
"pentru nucleu (și nu modifică schema de taste curentă)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CREATE BINARY KEYMAP"
msgstr "CREAREA UNEI SCHEME DE TASTE BINARĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the I<-b> (or I<--bkeymap> ) option is given B<loadkeys> prints to the "
"standard output a file that may be used as a binary keymap as expected by "
"Busybox B<loadkmap> command (and does not modify the current keymap)."
msgstr ""
"Dacă se dă opțiunea I<-b> (sau I<--bkeymap> ), B<loadkeys> imprimă la "
"ieșirea standard un fișier care poate fi utilizat ca o schemă de taste "
"binară, așa cum se așteaptă comanda Busybox B<loadkmap> (și nu modifică "
"schema de taste curentă)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "UNICODE MODE"
msgstr "MODUL UNICODE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<loadkeys> automatically detects whether the console is in Unicode or ASCII "
"(XLATE) mode.  When a keymap is loaded, literal keysyms (such as "
"B<section>)  are resolved accordingly; numerical keysyms are converted to "
"fit the current console mode, regardless of the way they are specified "
"(decimal, octal, hexadecimal or Unicode)."
msgstr ""
"B<loadkeys> detectează automat dacă consola se află în modul Unicode sau "
"ASCII (XLATE). Atunci când se încarcă o schemă de taste, simbolurile de "
"taste literale (cum ar fi B<section> (secțiune)) sunt rezolvate în "
"consecință; simbolurile de taste numerice sunt convertite pentru a se "
"potrivi modului curent al consolei, indiferent de modul în care sunt "
"specificate (zecimal, octal, hexazecimal sau Unicode)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<-u> (or I<--unicode>)  switch forces B<loadkeys> to convert all "
"keymaps to Unicode.  If the keyboard is in a non-Unicode mode, such as "
"XLATE, B<loadkeys> will change it to Unicode for the time of its execution.  "
"A warning message will be printed in this case."
msgstr ""
"Opțiunea I<-u> (sau I<--unicode>) forțează B<loadkeys> să convertească toate "
"schemele de taste în Unicode. Dacă tastatura se află într-un mod non-"
"Unicode, cum ar fi XLATE, B<loadkeys> o va schimba în Unicode pe durata "
"execuției sale. În acest caz, se va afișa un mesaj de avertizare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"It is recommended to run B<kbd_mode>(1)  before B<loadkeys> instead of using "
"the I<-u> option."
msgstr ""
"Se recomandă să executați B<kbd_mode>(1) înainte de B<loadkeys> în loc să "
"folosiți opțiunea I<-u>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OTHER OPTIONS"
msgstr "ALTE OPȚIUNI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a --ascii>"
msgstr "B<-a --ascii>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Force conversion to ASCII."
msgstr "Forțează conversia la ASCII."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-h --help>"
msgstr "B<-h --help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<loadkeys> prints its version number and a short usage message to the "
"programs standard error output and exits."
msgstr ""
"B<loadkeys> afișează numărul versiunii sale și un scurt mesaj de utilizare "
"la ieșirea de eroare standard a programului și iese."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-p --parse>"
msgstr "B<-p --parse>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<loadkeys> searches and parses keymap without action."
msgstr "B<loadkeys> caută și analizează schema de taste fără a acționa."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-q --quiet>"
msgstr "B<-q --quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<loadkeys> suppresses all normal output."
msgstr "B<loadkeys> suprimă toate mesajele normale."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V --version>"
msgstr "B<-V --version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<loadkeys> prints version number and exits."
msgstr "B<loadkeys> afișează numărul versiunii și iese."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "WARNING"
msgstr "AVERTISMENT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that anyone having read access to B</dev/console> can run B<loadkeys> "
"and thus change the keyboard layout, possibly making it unusable. Note that "
"the keyboard translation table is common for all the virtual consoles, so "
"any changes to the keyboard bindings affect all the virtual consoles "
"simultaneously."
msgstr ""
"Rețineți că orice persoană care are acces de citire la B</dev/console> poate "
"rula B<loadkeys> și astfel poate modifica aranjamentul tastaturii, făcându-l "
"posibil inutilizabil. Rețineți că tabelul de conversie a tastaturii este "
"comun pentru toate consolele virtuale, astfel încât orice modificare a "
"combinațiilor de taste afectează simultan toate consolele virtuale."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that because the changes affect all the virtual consoles, they also "
"outlive your session. This means that even at the login prompt the key "
"bindings may not be what the user expects."
msgstr ""
"Rețineți că, deoarece modificările afectează toate consolele virtuale, "
"acestea vor supraviețui și sesiunii dumneavoastră. Aceasta înseamnă că, până "
"și la solicitarea de autentificare, este posibil ca asocierile "
"(combinațiile) de taste să nu fie cele la care se așteaptă utilizatorul."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/kbd/keymaps>"
msgstr "I</usr/share/kbd/keymaps>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "default directory for keymaps."
msgstr "directorul implicit pentru schemele de taste."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/src/linux/drivers/char/defkeymap.map>"
msgstr "I</usr/src/linux/drivers/char/defkeymap.map>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "default kernel keymap."
msgstr "schema de taste implicită a nucleului."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dumpkeys>(1), B<keymaps>(5)"
msgstr "B<dumpkeys>(1), B<keymaps>(5)"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If the I<-d> (or I<--default> ) option is given, B<loadkeys> loads a default "
"keymap, probably the file I<defkeymap.map> either in I</usr/share/keymaps> "
"or in I</usr/src/linux/drivers/char>.  (Probably the former was user-"
"defined, while the latter is a qwerty keyboard map for PCs - maybe not what "
"was desired.)  Sometimes, with a strange keymap loaded (with the minus on "
"some obscure unknown modifier combination) it is easier to type `loadkeys "
"defkeymap'."
msgstr ""
"Dacă este dată opțiunea I<-d> (sau I<--default> ), B<loadkeys> încarcă o "
"schemă de taste implicită, probabil fișierul I<defkeymap.map> fie din I</usr/"
"share/keymaps>, fie din I</usr/src/linux/drivers/char>; (probabil că primul "
"a fost definit de utilizator, în timp ce al doilea este o schemă de "
"tastatură „qwerty” pentru PC-uri - poate nu ceea ce s-a dorit). Uneori, cu o "
"schemă de taste ciudată încărcată (cu minus pe o combinație obscură de "
"modificatori necunoscută), este mai ușor să se tasteze «loadkeys defkeymap»."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/share/keymaps>"
msgstr "I</usr/share/keymaps>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"If the I<-d> (or I<--default> ) option is given, B<loadkeys> loads a default "
"keymap, probably the file I<defkeymap.map> either in I</usr/lib/kbd/keymaps> "
"or in I</usr/src/linux/drivers/char>.  (Probably the former was user-"
"defined, while the latter is a qwerty keyboard map for PCs - maybe not what "
"was desired.)  Sometimes, with a strange keymap loaded (with the minus on "
"some obscure unknown modifier combination) it is easier to type `loadkeys "
"defkeymap'."
msgstr ""
"Dacă este dată opțiunea I<-d> (sau I<--default> ), B<loadkeys> încarcă o "
"schemă de taste implicită, probabil fișierul I<defkeymap.map> fie din I</usr/"
"lib/kbd/keymaps>, fie din I</usr/src/linux/drivers/char>; (probabil că "
"primul a fost definit de utilizator, în timp ce al doilea este o schemă de "
"tastatură „qwerty” pentru PC-uri - poate nu ceea ce s-a dorit). Uneori, cu o "
"schemă de taste ciudată încărcată (cu minus pe o combinație obscură de "
"modificatori necunoscută), este mai ușor să se tasteze «loadkeys defkeymap»."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</usr/lib/kbd/keymaps>"
msgstr "I</usr/lib/kbd/keymaps>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<loadkeys> [ I<-a --ascii> ] [ I<-b --bkeymap> ] [ I<-c --clearcompose> ] "
"[ I<-C 'E<lt>FILEE<gt>'> | I<--console=E<lt>FILEE<gt>> ] [ I<-d --default> ] "
"[ I<-h --help> ] [ I<-m --mktable> ] [ I<-p --parse> ] [ I<-q --quiet> ] "
"[ I<-s --clearstrings> ] [ I<-u --unicode> ] [ I<-v --verbose> ] [ I<-V --"
"version> ] [ I<filename...> ]"
msgstr ""
"B<loadkeys> [ I<-a --ascii> ] [ I<-b --bkeymap> ] [ I<-c --clearcompose> ] "
"[ I<-C 'E<lt>FIȘIERE<gt>'> | I<--console=E<lt>FILEE<gt>> ] [ I<-d --"
"default> ] [ I<-h --help> ] [ I<-m --mktable> ] [ I<-p --parse> ] [ I<-q --"
"quiet> ] [ I<-s --clearstrings> ] [ I<-u --unicode> ] [ I<-v --verbose> ] "
"[ I<-V --version> ] [ I<nume-fișier...> ]"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The program B<loadkeys> reads the file or files specified by I<filename..."
">.  Its main purpose is to load the kernel keymap for the console.  You can "
"specify console device by the I<-C> (or I<--console> ) option."
msgstr ""
"Programul B<loadkeys> citește fișierul sau fișierele specificate de I<nume-"
"fișier...>. Scopul său principal este de a încărca schema de taste a "
"nucleului pentru consolă. Puteți specifica dispozitivul de consolă prin "
"opțiunea I<-C> (sau I<--console> )."
