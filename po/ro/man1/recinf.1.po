# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2024-03-01 17:05+0100\n"
"PO-Revision-Date: 2023-07-12 09:59+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "RECINF"
msgstr "RECINF"

#. type: TH
#: debian-bookworm fedora-40 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "aprilie 2022"

#. type: TH
#: debian-bookworm fedora-40 fedora-rawhide
#, no-wrap
msgid "recinf 1.9"
msgstr "recinf 1.9"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Comenzi utilizator"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "recinf - print information about a recfile"
msgstr "recinf - tipărește informații despre un fișier „rec” (de înregistrări)"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "B<recinf> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<recinf> [I<\\,OPȚIUNE\\/>]... [I<\\,FIȘIER\\/>]..."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Print information about the types of records stored in the input."
msgstr ""
"Afișează informații despre tipurile de înregistrări stocate în datele de "
"intrare."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,RECORD_TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,TIP_ÎNREGISTRARE\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "print information on the records having the specified type."
msgstr "afișează informații despre înregistrările care au tipul specificat."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<-d>, B<--descriptor>"
msgstr "B<-d>, B<--descriptor>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "include the full record descriptors."
msgstr "include descriptorii compleți ai înregistrărilor."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<-n>, B<--names-only>"
msgstr "B<-n>, B<--names-only>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "output just the names of the record files found in the input."
msgstr ""
"afișează doar numele fișierelor de înregistrare găsite în datele de intrare."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "print a help message and exit."
msgstr "imprimă un mesaj de ajutor și iese."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "show version and exit."
msgstr "afișează versiunea și iese."

#. type: SS
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "Special options:"
msgstr "Opțiuni speciale:"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<-S>, B<--print-sexps>"
msgstr "B<-S>, B<--print-sexps>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "print the data in sexps instead of rec format."
msgstr "tipărește datele în format „sexps” în loc de „rec”."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Written by Jose E. Marchesi."
msgstr "Scris de Jose E. Marchesi."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPORTAREA ERORILOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Report bugs to: bug-recutils@gnu.org"
msgstr "Raportați erorile la: E<.MT bug-recutils@gnu.org>E<.ME>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"GNU recutils home page: E<lt>https://www.gnu.org/software/recutils/E<gt>"
msgstr ""
"Pagina principală a GNU recutils: E<lt>https://www.gnu.org/software/recutils/"
"E<gt>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Ajutor general pentru utilizarea software-ului GNU: E<lt>http://www.gnu.org/"
"gethelp/E<gt>"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr "DREPTURI DE AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Copyright \\(co 2010-2020 Jose E. Marchesi.  License GPLv3+: GNU GPL version "
"3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Drepturi de autor \\(co 2010-2020 Jose E. Marchesi. Licența GPLv3+: GNU GPL "
"versiunea 3 sau ulterioară E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Acesta este software liber: sunteți liber să-l modificați și să-l "
"redistribuiți. Nu există NICIO GARANȚIE, în limitele prevăzute de lege."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"The full documentation for B<recinf> is maintained as a Texinfo manual.  If "
"the B<info> and B<recinf> programs are properly installed at your site, the "
"command"
msgstr ""
"Documentația completă pentru B<recinf> este menținută ca un manual Texinfo.  "
"Dacă programele B<info> și B<> sunt instalate corect în sistemul dvs., "
"comanda"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "B<info recutils>"
msgstr "B<info recutils>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "ar trebui să vă permită accesul la manualul complet."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "February 2024"
msgstr "februarie 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GNU recutils 1.9"
msgstr "GNU recutils 1.9"
