# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-01 16:53+0100\n"
"PO-Revision-Date: 2023-11-26 13:08+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "bsearch"
msgstr "bsearch"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "bsearch - binary search of a sorted array"
msgstr "bsearch - căutare binară a unei matrice sortate"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *bsearch(const void >I<key>B<[.>I<size>B<], const void >I<base>B<[.>I<size>B< * .>I<nmemb>B<],>\n"
"B<              size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<              int (*>I<compar>B<)(const void [.>I<size>B<], const void [.>I<size>B<]));>\n"
msgstr ""
"B<void *bsearch(const void >I<key>B<[.>I<size>B<], const void >I<base>B<[.>I<size>B< * .>I<nmemb>B<],>\n"
"B<              size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<              int (*>I<compar>B<)(const void [.>I<size>B<], const void [.>I<size>B<]));>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<bsearch>()  function searches an array of I<nmemb> objects, the "
"initial member of which is pointed to by I<base>, for a member that matches "
"the object pointed to by I<key>.  The size of each member of the array is "
"specified by I<size>."
msgstr ""
"Funcția B<bsearch>() caută într-o matrice de obiecte I<nmemb>, al cărei "
"membru inițial este indicat de I<base>, un membru care se potrivește cu "
"obiectul indicat de I<key>.  Dimensiunea fiecărui membru al matricei este "
"specificată de I<size>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The contents of the array should be in ascending sorted order according to "
"the comparison function referenced by I<compar>.  The I<compar> routine is "
"expected to have two arguments which point to the I<key> object and to an "
"array member, in that order, and should return an integer less than, equal "
"to, or greater than zero if the I<key> object is found, respectively, to be "
"less than, to match, or be greater than the array member."
msgstr ""
"Conținutul matricei ar trebui să fie în ordine crescătoare în conformitate "
"cu funcția de comparație la care face referire I<compar>.  Se așteaptă ca "
"rutina I<compar> să aibă două argumente care indică obiectul I<key> și un "
"membru al matricei, în această ordine, și ar trebui să returneze un număr "
"întreg mai mic, egal sau mai mare decât zero dacă obiectul I<key> este "
"găsit, respectiv, mai mic, egal sau mai mare decât membrul matricei."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<bsearch>()  function returns a pointer to a matching member of the "
"array, or NULL if no match is found.  If there are multiple elements that "
"match the key, the element returned is unspecified."
msgstr ""
"Funcția B<bsearch>() returnează un indicator către un membru al matricei "
"care corespunde sau NULL dacă nu se găsește nicio potrivire.  În cazul în "
"care există mai multe elemente care corespund cheii (key), elementul "
"returnat este nespecificat."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<bsearch>()"
msgstr "B<bsearch>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, C99, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The example below first sorts an array of structures using B<qsort>(3), then "
"retrieves desired elements using B<bsearch>()."
msgstr ""
"Exemplul de mai jos sortează mai întâi o matrice de structuri utilizând "
"B<qsort>(3), apoi extrage elementele dorite utilizând B<bsearch>()."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"
"\\&\n"
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"
"\\&\n"
"static struct mi  months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"
"\\&\n"
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"
"\\&\n"
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"
"\\&\n"
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"
"\\&\n"
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"
"\\&\n"
"static struct mi  months[] = {\n"
"    { 1, \"ian\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"mai\" }, { 6, \"iun\" }, { 7, \"iul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"noi\" }, {12, \"dec\" }\n"
"};\n"
"\\&\n"
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"
"\\&\n"
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"
"\\&\n"
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: lună necunoscută\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: luna nr.%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<hsearch>(3), B<lsearch>(3), B<qsort>(3), B<tsearch>(3)"
msgstr "B<hsearch>(3), B<lsearch>(3), B<qsort>(3), B<tsearch>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"
msgstr "#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"
msgstr ""
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static struct mi  months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"
msgstr ""
"static struct mi  months[] = {\n"
"    { 1, \"ian\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"mai\" }, { 6, \"iun\" }, { 7, \"iul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"noi\" }, {12, \"dec\" }\n"
"};\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"
msgstr ""
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
msgstr ""
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: lună necunoscută\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: luna nr.%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 iulie 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
