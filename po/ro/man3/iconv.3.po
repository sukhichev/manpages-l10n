# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-01 16:58+0100\n"
"PO-Revision-Date: 2023-12-12 23:11+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "iconv"
msgstr "iconv"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "iconv - perform character set conversion"
msgstr "iconv - efectuează conversia setului de caractere"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>iconv.hE<gt>>\n"
msgstr "B<#include E<lt>iconv.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<size_t iconv(iconv_t >I<cd>B<,>\n"
"B<             char **restrict >I<inbuf>B<, size_t *restrict >I<inbytesleft>B<,>\n"
"B<             char **restrict >I<outbuf>B<, size_t *restrict >I<outbytesleft>B<);>\n"
msgstr ""
"B<size_t iconv(iconv_t >I<cd>B<,>\n"
"B<             char **restrict >I<inbuf>B<, size_t *restrict >I<inbytesleft>B<,>\n"
"B<             char **restrict >I<outbuf>B<, size_t *restrict >I<outbytesleft>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<iconv>()  function converts a sequence of characters in one character "
"encoding to a sequence of characters in another character encoding.  The "
"I<cd> argument is a conversion descriptor, previously created by a call to "
"B<iconv_open>(3); the conversion descriptor defines the character encodings "
"that B<iconv>()  uses for the conversion.  The I<inbuf> argument is the "
"address of a variable that points to the first character of the input "
"sequence; I<inbytesleft> indicates the number of bytes in that buffer.  The "
"I<outbuf> argument is the address of a variable that points to the first "
"byte available in the output buffer; I<outbytesleft> indicates the number of "
"bytes available in the output buffer."
msgstr ""
"Funcția B<iconv>() convertește o secvență de caractere dintr-o codificare de "
"caractere într-o secvență de caractere dintr-o altă codificare de caractere. "
"Argumentul I<cd> este un descriptor de conversie, creat anterior printr-un "
"apel la B<iconv_open>(3); descriptorul de conversie definește codificările "
"de caractere pe care B<iconv>() le utilizează pentru conversie. Argumentul "
"I<inbuf> este adresa unei variabile care indică primul caracter din secvența "
"de intrare; I<inbytesleft> indică numărul de octeți din memoria tampon "
"respectivă. Argumentul I<outbuf> este adresa unei variabile care indică "
"primul octet disponibil în memoria tampon de ieșire; I<outbytesleft> indică "
"numărul de octeți disponibili în memoria tampon de ieșire."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The main case is when I<inbuf> is not NULL and I<*inbuf> is not NULL.  In "
"this case, the B<iconv>()  function converts the multibyte sequence starting "
"at I<*inbuf> to a multibyte sequence starting at I<*outbuf>.  At most "
"I<*inbytesleft> bytes, starting at I<*inbuf>, will be read.  At most "
"I<*outbytesleft> bytes, starting at I<*outbuf>, will be written."
msgstr ""
"Cazul principal este atunci când I<inbuf> nu este NULL și I<*inbuf> nu este "
"NULL. În acest caz, funcția B<iconv>() convertește secvența de mai mulți "
"octeți care începe la I<*inbuf> într-o secvență de mai mulți octeți care "
"începe la I<*outbuf>. Vor fi citiți cel mult I<*inbytesleft> octeți, "
"începând de la I<*inbuf>. Se vor scrie cel mult I<*outbytesleft> octeți, "
"începând de la I<*outbuf>."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"The B<iconv>()  function converts one multibyte character at a time, and for "
"each character conversion it increments I<*inbuf> and decrements "
"I<*inbytesleft> by the number of converted input bytes, it increments "
"I<*outbuf> and decrements I<*outbytesleft> by the number of converted output "
"bytes, and it updates the conversion state contained in I<cd>.  If the "
"character encoding of the input is stateful, the B<iconv>()  function can "
"also convert a sequence of input bytes to an update to the conversion state "
"without producing any output bytes; such input is called a I<shift "
"sequence>.  The conversion can stop for five reasons:"
msgstr ""
"Funcția B<iconv>() convertește câte un caracter multi-octet la un moment dat "
"și, pentru fiecare convertire de caracter, mărește I<*inbuf> și descrește "
"I<*inbytesleft> cu numărul de octeți de intrare convertiți, mărește "
"I<*outbuf> și descrește I<*outbytesleft> cu numărul de octeți de ieșire "
"convertiți și actualizează starea de conversie conținută în I<cd>. În cazul "
"în care codificarea caracterelor de intrare este de tip statutar, funcția "
"B<iconv>() poate, de asemenea, să convertească o secvență de octeți de "
"intrare într-o actualizare a stării de conversie fără a produce niciun octet "
"de ieșire; o astfel de intrare se numește I<secvență „shift”>. Conversia se "
"poate opri din cinci motive:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An invalid multibyte sequence is encountered in the input.  In this case, it "
"sets I<errno> to B<EILSEQ> and returns I<(size_t)\\ -1>.  I<*inbuf> is left "
"pointing to the beginning of the invalid multibyte sequence."
msgstr ""
"O secvență multi-octet nevalidă este întâlnită la intrare.  În acest caz, "
"configurează I<errno> la B<EILSEQ> și returnează I<(size_t)\\ -1>.  "
"I<*inbuf> rămâne îndreptat spre începutul secvenței multi-octet nevalide."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"A multibyte sequence is encountered that is valid but that cannot be "
"translated to the character encoding of the output.  This condition depends "
"on the implementation and on the conversion descriptor.  In the GNU C "
"library and GNU libiconv, if I<cd> was created without the suffix B<//"
"TRANSLIT> or B<//IGNORE>, the conversion is strict: lossy conversions "
"produce this condition.  If the suffix B<//TRANSLIT> was specified, "
"transliteration can avoid this condition in some cases.  In the musl C "
"library, this condition cannot occur because a conversion to "
"B<\\[aq]*\\[aq]> is used as a fallback.  In the FreeBSD, NetBSD, and Solaris "
"implementations of B<iconv>(), this condition cannot occur either, because a "
"conversion to B<\\[aq]?\\[aq]> is used as a fallback.  When this condition "
"is met, B<iconv>()  sets I<errno> to B<EILSEQ> and returns I<(size_t)\\ "
"-1>.  I<*inbuf> is left pointing to the beginning of the unconvertible "
"multibyte sequence."
msgstr ""
"Se întâlnește o secvență multi-octet care este validă, dar care nu poate fi "
"convertită în codificarea caracterelor de ieșire. Această condiție depinde "
"de implementare și de descriptorul de conversie. În biblioteca GNU C și GNU "
"libiconv, dacă I<cd> a fost creat fără sufixul B<//TRANSLIT> sau B<//"
"IGNORE>, conversia este strictă: conversiile cu pierderi produc această "
"condiție. Dacă a fost specificat sufixul B<//TRANSLIT>, transliterarea poate "
"evita această condiție în unele cazuri. În biblioteca musl C, această "
"condiție nu poate apărea deoarece se utilizează o conversie în "
"B<\\[aq]*\\[aq]> ca soluție de rezervă. Nici în implementările FreeBSD, "
"NetBSD și Solaris ale B<iconv>() nu poate apărea această condiție, deoarece "
"se utilizează o conversie în B<\\[aq]?\\[aq]> ca soluție de rezervă. Atunci "
"când această condiție este îndeplinită, B<iconv>() stabilește I<errno> la "
"B<EILSEQ> și returnează I<(size_t)\\ -1>. I<*inbuf> este lăsată indicând "
"începutul secvenței multi-octet neconvertibile."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The input byte sequence has been entirely converted, that is, "
"I<*inbytesleft> has gone down to 0.  In this case, B<iconv>()  returns the "
"number of nonreversible conversions performed during this call."
msgstr ""
"Secvența de octeți de intrare a fost convertită în întregime, adică "
"I<*inbytesleft> a coborât la 0. În acest caz, B<iconv>() returnează numărul "
"de conversii nereversibile efectuate în timpul acestui apel."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An incomplete multibyte sequence is encountered in the input, and the input "
"byte sequence terminates after it.  In this case, it sets I<errno> to "
"B<EINVAL> and returns I<(size_t)\\ -1>.  I<*inbuf> is left pointing to the "
"beginning of the incomplete multibyte sequence."
msgstr ""
"O secvență de mai mulți octeți incompletă este întâlnită la intrare, iar "
"secvența de octeți de intrare se termină după ea. În acest caz, se "
"stabilește I<errno> la B<EINVAL> și se returnează I<(size_t)\\ -1>.  "
"I<*inbuf> rămâne îndreptat spre începutul secvenței multi-octet incomplete."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The output buffer has no more room for the next converted character.  In "
"this case, it sets I<errno> to B<E2BIG> and returns I<(size_t)\\ -1>."
msgstr ""
"Memoria tampon de ieșire nu mai are loc pentru următorul caracter "
"convertit.  În acest caz, configurează I<errno> la B<E2BIG> și returnează "
"I<(size_t)\\ -1>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A different case is when I<inbuf> is NULL or I<*inbuf> is NULL, but "
"I<outbuf> is not NULL and I<*outbuf> is not NULL.  In this case, the "
"B<iconv>()  function attempts to set I<cd>'s conversion state to the initial "
"state and store a corresponding shift sequence at I<*outbuf>.  At most "
"I<*outbytesleft> bytes, starting at I<*outbuf>, will be written.  If the "
"output buffer has no more room for this reset sequence, it sets I<errno> to "
"B<E2BIG> and returns I<(size_t)\\ -1>.  Otherwise, it increments I<*outbuf> "
"and decrements I<*outbytesleft> by the number of bytes written."
msgstr ""
"Un caz diferit este atunci când I<inbuf> este NULL sau I<*inbuf> este NULL, "
"dar I<outbuf> nu este NULL și I<*outbuf> nu este NULL. În acest caz, funcția "
"B<iconv>() încearcă să fixeze starea de conversie a lui I<cd> la starea "
"inițială și să stocheze o secvență de transformare corespunzătoare în "
"I<*outbuf>. Se vor scrie cel mult I<*outbytesleft> octeți, începând de la "
"I<*outbuf>. Dacă memoria tampon de ieșire nu mai are loc pentru această "
"secvență de reinițializare, configurează I<errno> la B<E2BIG> și returnează "
"I<(size_t)\\ -1>.  În caz contrar, se incrementează I<*outbuf> și se "
"decrementează I<*outbytesleft> cu numărul de octeți scriși."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A third case is when I<inbuf> is NULL or I<*inbuf> is NULL, and I<outbuf> is "
"NULL or I<*outbuf> is NULL.  In this case, the B<iconv>()  function sets "
"I<cd>'s conversion state to the initial state."
msgstr ""
"Un al treilea caz este atunci când I<inbuf> este NULL sau I<*inbuf> este "
"NULL, iar I<outbuf> este NULL sau I<*outbuf> este NULL. În acest caz, "
"funcția B<iconv>() stabilește starea de conversie a lui I<cd> la starea "
"inițială."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<iconv>()  function returns the number of characters converted in a "
"nonreversible way during this call; reversible conversions are not counted.  "
"In case of error, B<iconv>()  returns I<(size_t)\\ -1> and sets I<errno> to "
"indicate the error."
msgstr ""
"Funcția B<iconv>() returnează numărul de caractere convertite într-un mod "
"nereversibil în timpul acestui apel; conversiile reversibile nu sunt luate "
"în considerare. În caz de eroare, B<iconv>() returnează I<(size_t)\\ -1> și "
"configurează I<errno> pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERORI-IEȘIRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following errors can occur, among others:"
msgstr "Pot apărea, printre altele, următoarele erori:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<E2BIG>"
msgstr "B<E2BIG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "There is not sufficient room at I<*outbuf>."
msgstr "Nu există spațiu suficient la I<*outbuf>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EILSEQ>"
msgstr "B<EILSEQ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An invalid multibyte sequence has been encountered in the input."
msgstr "O secvență multi-octet nevalidă a fost întâlnită la intrare."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An incomplete multibyte sequence has been encountered in the input."
msgstr "O secvență multi-octet incompletă a fost întâlnită la intrare."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<iconv>()"
msgstr "B<iconv>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:cd"
msgstr "MT-Safe race:cd"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<iconv>()  function is MT-Safe, as long as callers arrange for mutual "
"exclusion on the I<cd> argument."
msgstr ""
"Funcția B<iconv>() este MT-Safe, atâta timp cât apelanții iau măsuri de "
"excludere reciprocă pentru argumentul I<cd>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.1.  POSIX.1-2001."
msgstr "glibc 2.1.  POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In each series of calls to B<iconv>(), the last should be one with I<inbuf> "
"or I<*inbuf> equal to NULL, in order to flush out any partially converted "
"input."
msgstr ""
"În fiecare serie de apeluri către B<iconv>(), ultimul ar trebui să fie unul "
"cu I<inbuf> sau I<*inbuf> egal cu NULL, pentru a elimina orice intrare "
"parțial convertită."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Although I<inbuf> and I<outbuf> are typed as I<char\\ **>, this does not "
"mean that the objects they point can be interpreted as C strings or as "
"arrays of characters: the interpretation of character byte sequences is "
"handled internally by the conversion functions.  In some encodings, a zero "
"byte may be a valid part of a multibyte character."
msgstr ""
"Deși I<inbuf> și I<outbuf> sunt tipizate ca I<char\\ **>, acest lucru nu "
"înseamnă că obiectele pe care le indică pot fi interpretate ca șiruri de "
"caractere C sau ca matrice de caractere: interpretarea secvențelor de octeți "
"de caractere este gestionată în mod intern de către funcțiile de conversie. "
"În unele codificări, un octet zero poate fi o parte validă a unui caracter "
"multi-octet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The caller of B<iconv>()  must ensure that the pointers passed to the "
"function are suitable for accessing characters in the appropriate character "
"set.  This includes ensuring correct alignment on platforms that have tight "
"restrictions on alignment."
msgstr ""
"Cel care apelează B<iconv>() trebuie să se asigure că indicatorii trecuți în "
"funcție sunt adecvați pentru accesarea caracterelor din setul de caractere "
"corespunzător. Aceasta include asigurarea alinierii corecte pe platformele "
"care au restricții stricte privind alinierea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<iconv_close>(3), B<iconv_open>(3), B<iconvconfig>(8)"
msgstr "B<iconv_close>(3), B<iconv_open>(3), B<iconvconfig>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<iconv>()  function converts one multibyte character at a time, and for "
"each character conversion it increments I<*inbuf> and decrements "
"I<*inbytesleft> by the number of converted input bytes, it increments "
"I<*outbuf> and decrements I<*outbytesleft> by the number of converted output "
"bytes, and it updates the conversion state contained in I<cd>.  If the "
"character encoding of the input is stateful, the B<iconv>()  function can "
"also convert a sequence of input bytes to an update to the conversion state "
"without producing any output bytes; such input is called a I<shift "
"sequence>.  The conversion can stop for four reasons:"
msgstr ""
"Funcția B<iconv>() convertește câte un caracter multi-octet la un moment dat "
"și, pentru fiecare convertire de caracter, mărește I<*inbuf> și descrește "
"I<*inbytesleft> cu numărul de octeți de intrare convertiți, mărește "
"I<*outbuf> și descrește I<*outbytesleft> cu numărul de octeți de ieșire "
"convertiți și actualizează starea de conversie conținută în I<cd>. În cazul "
"în care codificarea caracterelor de intrare este de tip statutar, funcția "
"B<iconv>() poate, de asemenea, să convertească o secvență de octeți de "
"intrare într-o actualizare a stării de conversie fără a produce niciun octet "
"de ieșire; o astfel de intrare se numește I<secvență „shift”>. Conversia se "
"poate opri din patru motive:"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: debian-bookworm
msgid "This function is available since glibc 2.1."
msgstr "Această funcție este disponibilă începând cu glibc 2.1."

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 iulie 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
