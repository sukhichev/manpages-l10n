# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.20.0\n"
"POT-Creation-Date: 2024-03-01 16:55+0100\n"
"PO-Revision-Date: 2023-10-31 23:17+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ecvt"
msgstr "ecvt"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octombrie 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pagini de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "ecvt, fcvt - convert a floating-point number to a string"
msgstr ""
"ecvt, fcvt - convertește un număr în virgulă mobilă într-un șir de caractere"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] char *ecvt(double >I<number>B<, int >I<ndigits>B<,>\n"
"B<                          int *restrict >I<decpt>B<, int *restrict >I<sign>B<);>\n"
"B<[[deprecated]] char *fcvt(double >I<number>B<, int >I<ndigits>B<,>\n"
"B<                          int *restrict >I<decpt>B<, int *restrict >I<sign>B<);>\n"
msgstr ""
"B<[[deprecated]] char *ecvt(double >I<number>B<, int >I<ndigits>B<,>\n"
"B<                          int *restrict >I<decpt>B<, int *restrict >I<sign>B<);>\n"
"B<[[deprecated]] char *fcvt(double >I<number>B<, int >I<ndigits>B<,>\n"
"B<                          int *restrict >I<decpt>B<, int *restrict >I<sign>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Cerințe pentru macrocomenzi de testare a caracteristicilor pentru glibc "
"(consultați B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ecvt>(), B<fcvt>():"
msgstr "B<ecvt>(), B<fcvt>():"

#.         || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.17\n"
"        (_XOPEN_SOURCE E<gt>= 500 && ! (_POSIX_C_SOURCE E<gt>= 200809L))\n"
"            || /* glibc E<gt>= 2.20 */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19 */ _SVID_SOURCE\n"
"    glibc 2.12 to glibc 2.16:\n"
"        (_XOPEN_SOURCE E<gt>= 500 && ! (_POSIX_C_SOURCE E<gt>= 200112L))\n"
"            || _SVID_SOURCE\n"
"    Before glibc 2.12:\n"
"        _SVID_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"    Începând cu glibc 2.17\n"
"        (_XOPEN_SOURCE E<gt>= 500 && ! (_POSIX_C_SOURCE E<gt>= 200809L))\n"
"            || /* glibc E<gt>= 2.20 */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19 */ _SVID_SOURCE\n"
"    de la glibc 2.12 la glibc 2.16:\n"
"        (_XOPEN_SOURCE E<gt>= 500 && ! (_POSIX_C_SOURCE E<gt>= 200112L))\n"
"            || _SVID_SOURCE\n"
"    Înainte de glibc 2.12:\n"
"        _SVID_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<ecvt>()  function converts I<number> to a null-terminated string of "
"I<ndigits> digits (where I<ndigits> is reduced to a system-specific limit "
"determined by the precision of a I<double>), and returns a pointer to the "
"string.  The high-order digit is nonzero, unless I<number> is zero.  The low "
"order digit is rounded.  The string itself does not contain a decimal point; "
"however, the position of the decimal point relative to the start of the "
"string is stored in I<*decpt>.  A negative value for I<*decpt> means that "
"the decimal point is to the left of the start of the string.  If the sign of "
"I<number> is negative, I<*sign> is set to a nonzero value, otherwise it is "
"set to 0.  If I<number> is zero, it is unspecified whether I<*decpt> is 0 or "
"1."
msgstr ""
"Funcția B<ecvt>() convertește I<number> într-un șir de cifre I<ndigits> cu "
"terminație nulă (unde I<ndigits> este redus la o limită specifică sistemului "
"determinată de precizia unui I<double>) și returnează un indicator la șir.  "
"Cifra de ordinul superior este diferită de zero, cu excepția cazului în care "
"I<number> este zero.  Cifra de ordin inferior este rotunjită.  Șirul în sine "
"nu conține o virgulă zecimală; cu toate acestea, poziția virgulei zecimale "
"în raport cu începutul șirului este stocată în I<*decpt>.  O valoare "
"negativă pentru I<*decpt> înseamnă că punctul zecimal se află la stânga față "
"de începutul șirului.  În cazul în care semnul lui I<number> este negativ, "
"I<*sign> este setat la o valoare diferită de zero, în caz contrar este setat "
"la 0. Dacă I<number> este zero, nu se specifică dacă I<*decpt> este 0 sau 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<fcvt>()  function is identical to B<ecvt>(), except that I<ndigits> "
"specifies the number of digits after the decimal point."
msgstr ""
"Funcția B<fcvt>() este identică cu B<ecvt>(), cu excepția faptului că "
"I<ndigits> specifică numărul de cifre după virgulă."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Both the B<ecvt>()  and B<fcvt>()  functions return a pointer to a static "
"string containing the ASCII representation of I<number>.  The static string "
"is overwritten by each call to B<ecvt>()  or B<fcvt>()."
msgstr ""
"Atât funcțiile B<ecvt>(), cât ș i B<fcvt>() returnează un indicator la un "
"șir static care conține reprezentarea ASCII a lui I<number>.  Șirul static "
"este suprascris de fiecare apel la B<ecvt>() sau B<fcvt>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pentru o explicație a termenilor folosiți în această secțiune, a se vedea "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfață"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valoare"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ecvt>()"
msgstr "B<ecvt>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Siguranța firelor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:ecvt"
msgstr "MT-Unsafe race:ecvt"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<fcvt>()"
msgstr "B<fcvt>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:fcvt"
msgstr "MT-Unsafe race:fcvt"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr "Niciunul."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"SVr2; marked as LEGACY in POSIX.1-2001.  POSIX.1-2008 removes the "
"specifications of B<ecvt>()  and B<fcvt>(), recommending the use of "
"B<sprintf>(3)  instead (though B<snprintf>(3)  may be preferable)."
msgstr ""
"SVr2; marcat ca „LEGACY” în POSIX.1-2001.  POSIX.1-2008 elimină "
"specificațiile B<ecvt>() și B<fcvt>(), recomandând în schimb utilizarea "
"B<sprintf>(3) (deși B<snprintf>(3) poate fi preferabil)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#.  Linux libc4 and libc5 specified the type of
#.  .I ndigits
#.  as
#.  .IR size_t .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Not all locales use a point as the radix character (\"decimal point\")."
msgstr ""
"Nu toate configurațiile regionale utilizează un punct drept caracter "
"separator zecimal („punct zecimal”) în cadrul numerelor reale."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ecvt_r>(3), B<gcvt>(3), B<qecvt>(3), B<setlocale>(3), B<sprintf>(3)"
msgstr "B<ecvt_r>(3), B<gcvt>(3), B<qecvt>(3), B<setlocale>(3), B<sprintf>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 iulie 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pagini de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"
