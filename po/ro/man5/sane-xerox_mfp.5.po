# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:09+0100\n"
"PO-Revision-Date: 2023-12-18 11:09+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sane-xerox_mfp"
msgstr "sane-xerox_mfp"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "15 Dec 2008"
msgstr "15 decembrie 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sane-xerox_mfp - SANE backend for Xerox Phaser 3200MFP device et al."
msgstr ""
"sane-xerox_mfp - controlor SANE pentru dispozitivul Xerox Phaser 3200MFP și "
"altele"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<sane-xerox_mfp> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to several Samsung-based Samsung, Xerox, and "
"Dell scanners.  Please see full list of supported devices at I<http://www."
"sane-project.org/sane-supported-devices.html>."
msgstr ""
"Biblioteca B<sane-xerox_mfp> implementează un controlor SANE (Scanner Access "
"Now Easy) care oferă acces la mai multe scanere Samsung, Xerox și Dell. Vă "
"rugăm să consultați lista completă a dispozitivelor acceptate la I<http://"
"www.sane-project.org/sane-supported-devices.html>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/sane.d/xerox_mfp.conf>"
msgstr "I</etc/sane.d/xerox_mfp.conf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "USB scanners do not need any configuration."
msgstr "Scanerele USB nu au nevoie de nicio configurare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For SCX-4500W in network mode you need to specify"
msgstr "Pentru SCX-4500W în modul rețea, trebuie să specificați"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<tcp host_address [port]>"
msgstr "B<tcp adresa_gazdei [port]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<host_address> is passed through resolver, thus can be a dotted quad or "
"a name from I</etc/hosts> or resolvable through DNS."
msgstr ""
"Adresa B<adresa_gazdei> este transmisă prin „resolver”, deci poate fi un "
"cvadruplu punctat (XXX.XXX.XXX.XXX) sau un nume din I</etc/hosts> sau poate "
"fi rezolvat prin DNS."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The backend configuration file. By default all scanner types/models are "
"enabled, you may want to comment out unwanted entries."
msgstr ""
"Fișierul de configurare a controlorului. În mod implicit, toate tipurile/"
"modelele de scaner sunt activate; este posibil să doriți să comentați "
"intrările nedorite."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-xerox_mfp.a>"
msgstr "I</usr/lib/sane/libsane-xerox_mfp.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-xerox_mfp.so>"
msgstr "I</usr/lib/sane/libsane-xerox_mfp.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_XEROX_MFP>"
msgstr "B<SANE_DEBUG_XEROX_MFP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. "
"Nivelurile mai mari de depanare cresc cantitatea de detalii informative a "
"ieșirii."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Example: export SANE_DEBUG_XEROX_MFP=4"
msgstr "Exemplu: export SANE_DEBUG_XEROX_MFP=4"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIMITATIONS"
msgstr "LIMITĂRI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Multicast autoconfiguration for LAN scanners is not implemented yet. IPv6 "
"addressing has never been tested."
msgstr ""
"Autoconfigurarea Multicast pentru scanerele LAN nu este încă implementată. "
"Adresarea IPv6 nu a fost niciodată testată."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS AND SUPPORT"
msgstr "ERORI ȘI ASISTENȚĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If you have found a bug or need support please follow open-source way of "
"acquiring support via mail-lists I<http://www.sane-project.org/mailing-lists."
"html> or SANE bug tracker I<http://www.sane-project.org/bugs.html>."
msgstr ""
"Dacă ați găsit o eroare sau aveți nevoie de asistență, vă rugăm să urmați "
"metoda „open-source” de obținere a asistenței prin intermediul listelor de e-"
"mail I<http://www.sane-project.org/mailing-lists.html> sau al sistemului de "
"urmărire a erorilor SANE I<http://www.sane-project.org/bugs.html>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Alex Belkin E<lt>I<abc@telekom.ru>E<gt>."
msgstr "Alex Belkin E<lt>I<abc@telekom.ru>E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Samsung SCX-4500W scan over network support by Alexander Kuznetsov "
"E<lt>I<acca(at)cpan.org>E<gt>."
msgstr ""
"Samsung SCX-4500W scanare prin intermediul suportului de rețea de Alexander "
"Kuznetsov E<lt>I<acca(at)cpan.org>E<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Color scanning on Samsung M2870 model and Xerox Cognac 3215 & 3225 models by "
"Laxmeesh Onkar Markod E<lt>I<m.laxmeesh@samsung.com>E<gt>."
msgstr ""
"Scanarea color pe modelul Samsung M2870 și modelele Xerox Cognac 3215 & 3225 "
"de Laxmeesh Onkar Markod E<lt>I<m.laxmeesh@samsung.com>E<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sane>(7), B<sane-usb>(5)"
msgstr "B<sane>(7), B<sane-usb>(5)"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-xerox_mfp.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-xerox_mfp.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-xerox_mfp.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-xerox_mfp.so>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-xerox_mfp.a>"
msgstr "I</usr/lib64/sane/libsane-xerox_mfp.a>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-xerox_mfp.so>"
msgstr "I</usr/lib64/sane/libsane-xerox_mfp.so>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<sane-xerox_mfp> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to several Samsung-based Samsung, Xerox, and "
"Dell scanners.  Please see full list of supported devices at http://www.sane-"
"project.org/sane-supported-devices.html"
msgstr ""
"Biblioteca B<sane-xerox_mfp> implementează un controlor SANE (Scanner Access "
"Now Easy) care oferă acces la mai multe scanere Samsung, Xerox și Dell. Vă "
"rugăm să consultați lista completă a dispozitivelor acceptate la I<http://"
"www.sane-project.org/sane-supported-devices.html>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Multicast autoconfiguration for LAN scanners is not implemented yet. IPv6 "
"addressing never been tested."
msgstr ""
"Autoconfigurarea Multicast pentru scanerele LAN nu este încă implementată. "
"Adresarea IPv6 nu a fost niciodată testată."
