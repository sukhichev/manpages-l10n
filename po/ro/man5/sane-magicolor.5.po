# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:09+0100\n"
"PO-Revision-Date: 2024-02-24 01:12+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sane-magicolor"
msgstr "sane-magicolor"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "10 Jan 2011"
msgstr "10 ianuarie 2011"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sane-magicolor - SANE backend for KONICA MINOLTA magicolor scanners"
msgstr ""
"sane-magicolor - controlor SANE pentru scanerele KONICA MINOLTA magicolor"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sane-magicolor> backend supports KONICA MINOLTA magicolor scanners "
"connected via USB or LAN. Currently, only the magicolor 1690MF device is "
"supported, no other devices with the same scanning protocol are known."
msgstr ""
"Controlorul B<sane-magicolor> acceptă scanerele KONICA MINOLTA magicolor "
"conectate prin USB sau LAN. În prezent, este acceptat doar dispozitivul "
"magicolor 1690MF, nu sunt cunoscute alte dispozitive cu același protocol de "
"scanare."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SUPPORTED DEVICES"
msgstr "DISPOZITIVE COMPATIBILE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following scanner should work with this backend:"
msgstr "Următorul scaner ar trebui să funcționeze cu acest controlor:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Device Details"
msgstr "Detalii dispozitiv"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "-----------------------------"
msgstr "-----------------------------"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Vendor: KONICA MINOLTA"
msgstr "Fabricant: KONICA MINOLTA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Model: magicolor 1690MF"
msgstr "Model: magicolor 1690MF"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This section describes the backend's configuration file entries. The file is "
"located at:"
msgstr ""
"Această secțiune descrie intrările din fișierul de configurare al "
"controlorului. Fișierul este localizat la:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/sane.d/magicolor.conf>"
msgstr "I</etc/sane.d/magicolor.conf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For a proper setup, at least one of the following entries are needed:"
msgstr ""
"Pentru o configurare corectă, este necesară cel puțin una dintre următoarele "
"intrări:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<net autodiscovery>"
msgstr "I<net autodiscovery>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<net [IP ADDRESS] [DEVICE-ID]>"
msgstr "I<net [ADRESA IP] [ID-DISPOZITIV]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<usb>"
msgstr "I<usb>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<usb [VENDOR-ID] [DEVICE-ID]>"
msgstr "I<usb [ID-FABRICANT] [ID-DISPOZITIV]>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The backend configuration file"
msgstr "Fișierul de configurare al controlorului"

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-magicolor.a>"
msgstr "I</usr/lib/sane/libsane-magicolor.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-magicolor.so>"
msgstr "I</usr/lib/sane/libsane-magicolor.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_CONFIG_DIR>"
msgstr "B<SANE_CONFIG_DIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  On *NIX systems, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d>.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I</tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Această variabilă de mediu specifică lista de directoare care pot conține "
"fișierul de configurare. În sistemele *NIX, directoarele sunt separate prin "
"două puncte („:”), în OS/2, ele sunt separate prin punct și virgulă („;”). "
"Dacă această variabilă nu este definită, fișierul de configurare este căutat "
"în două directoare implicite: mai întâi, în directorul de lucru curent („.”) "
"și apoi în I</etc/sane.d>. Dacă valoarea variabilei de mediu se termină cu "
"caracterul separator de directoare, atunci directoarele implicite sunt "
"căutate după directoarele specificate explicit. De exemplu, dacă se "
"stabilește B<SANE_CONFIG_DIR> la „/tmp/config:”, vor fi căutate directoarele "
"I</tmp/config>, I<.> și I</etc/sane.d> (în această ordine)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_MAGICOLOR>"
msgstr "B<SANE_DEBUG_MAGICOLOR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. "
"Nivelurile mai mari de depanare cresc cantitatea de detalii informative a "
"ieșirii."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Example: export SANE_DEBUG_MAGICOLOR=127"
msgstr "Exemplu: export SANE_DEBUG_MAGICOLOR=127"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To obtain debug messages from the backend, set this environment variable "
"before calling your favorite frontend (e.g.  B<xscanimage>(1))."
msgstr ""
"Pentru a obține mesaje de depanare de la controlor, definiți această "
"variabilă de mediu înainte de a apela interfața preferată (de exemplu, "
"B<xscanimage>(1))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Example: SANE_DEBUG_MAGICOLOR=65 xscanimage"
msgstr "Exemplu: SANE_DEBUG_MAGICOLOR=65 xscanimage"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "KNOWN BUGS AND RESTRICTIONS"
msgstr "ERORI ȘI RESTRICȚII CUNOSCUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Large color scans may sometimes timeout due to unknown reasons (the scanner "
"simply stops returning data)."
msgstr ""
"Este posibil ca scanările color de mari dimensiuni să se termine uneori din "
"motive necunoscute (scanerul încetează pur și simplu să mai trimită date)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Cancelling large scans may lock up the scanner."
msgstr "Anularea scanărilor mari poate bloca scanerul."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sane>(7),"
msgstr "B<sane>(7),"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<http://wiki.kainhofer.com/hardware/magicolor_scan>"
msgstr "I<http://wiki.kainhofer.com/hardware/magicolor_scan>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Reinhold Kainhofer E<lt>I<reinhold@kainhofer.com>E<gt>"
msgstr "Reinhold Kainhofer E<lt>I<reinhold@kainhofer.com>E<gt>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-magicolor.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-magicolor.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-magicolor.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-magicolor.so>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-magicolor.a>"
msgstr "I</usr/lib64/sane/libsane-magicolor.a>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-magicolor.so>"
msgstr "I</usr/lib64/sane/libsane-magicolor.so>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  Under UNIX, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d>.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I</tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Această variabilă de mediu specifică lista de directoare care pot conține "
"fișierul de configurare. În UNIX, directoarele sunt separate prin două "
"puncte („:”), în OS/2, ele sunt separate prin punct și virgulă („;”). Dacă "
"această variabilă nu este definită, fișierul de configurare este căutat în "
"două directoare implicite: mai întâi, în directorul de lucru curent („.”) și "
"apoi în I</etc/sane.d>. Dacă valoarea variabilei de mediu se termină cu "
"caracterul separator de directoare, atunci directoarele implicite sunt "
"căutate după directoarele specificate explicit. De exemplu, dacă se "
"definește B<SANE_CONFIG_DIR> la „/tmp/config:”, se vor căuta (în această "
"ordine) directoarele I<tmp/config>, I<.> și I</etc/sane.d>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Large color scans may sometimes timeout due to unknown reasons (the scanner "
"simply stops returning data)"
msgstr ""
"Scanările color de mari dimensiuni se pot opri uneori din motive necunoscute "
"(scanerul încetează pur și simplu să mai returneze date)."
