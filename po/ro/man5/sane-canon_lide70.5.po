# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:08+0100\n"
"PO-Revision-Date: 2024-02-19 00:49+0100\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sane-canon_lide70"
msgstr "sane-canon_lide70"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "22 Aug 2020"
msgstr "22 august 2020"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"sane-canon_lide70 - SANE backend for the Canon LiDE 70 and 600(F) USB "
"flatbed scanners"
msgstr ""
"sane-canon_lide70 - Backend SANE pentru scanerele plate USB Canon LiDE 70 și "
"600(F)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<canon_lide70> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to the Canon Inc. CanoScan LiDE 70 and 600(F)  "
"flatbed scanners. The film unit of the LiDE 600F is not supported."
msgstr ""
"Biblioteca B<canon_lide70> implementează un controlor SANE (Scanner Access "
"Now Easy) care oferă acces la Canon Inc. CanoScan LiDE 70 și 600(F). "
"Unitatea de film de la LiDE 600F nu este compatibilă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Due to Canon's unwillingness to provide scanner documentation, this software "
"was developed by analyzing the USB traffic of the Windows XP driver. The "
"precise meaning of the individual commands that are sent to the scanner is "
"known only to a very limited extent. Some sophistication present in the "
"Windows XP driver has been left out. There is, for example, no active "
"calibration."
msgstr ""
"Din cauza refuzului Canon de a furniza documentația scanerului, acest "
"software a fost dezvoltat prin analiza traficului USB al controlorului "
"Windows XP. Semnificația exactă a comenzilor individuale care sunt trimise "
"către scaner este cunoscută doar într-o măsură foarte limitată. Unele "
"sofisticări prezente în controlorul Windows XP au fost omise. Nu există, de "
"exemplu, nicio calibrare activă."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Testers and reviewers are welcome. Send your bug reports and comments to the "
"sane-devel mailing list I<E<lt>sane-devel@alioth-lists.debian.netE<gt>>."
msgstr ""
"Testatorii și revizorii sunt bineveniți. Trimiteți rapoartele de erori și "
"comentariile dvs. la lista de discuții sane-devel I<E<lt>sane-devel@alioth-"
"lists.debian.netE<gt>>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURARE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I</etc/sane.d/canon_lide70.conf> file identifies the LiDE 70 by its "
"vendor code 0x04a9 and its product code 0x2225. For the LiDE 600(f) the "
"product code is 0x2224."
msgstr ""
"Fișierul I</etc/sane.d/canon_lide70.conf> identifică LiDE 70 prin codul de "
"furnizor 0x04a9 și codul de produs 0x2225. Pentru LiDE 600(f), codul "
"produsului este 0x2224."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BACKEND SPECIFIC OPTIONS"
msgstr "OPȚIUNI SPECIFICE CONTROLORULUI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<Scan Mode:>"
msgstr "B<Mod scanare:>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--resolution 75|150|300|600|1200 [default 600]>"
msgstr "B<--resolution 75|150|300|600|1200 [implicit 600]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Sets the resolution of the scanned image in dots per inch. Scanning at 1200 "
"dpi is not available on the LiDE 600(F) and it is very slow on the LiDE 70."
msgstr ""
"Stabilește rezoluția imaginii scanate în puncte pe inci. Scanarea la 1200 "
"dpi nu este disponibilă pe LiDE 600(F) și este foarte lentă pe LiDE 70."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--mode Color|Gray|Lineart [default: Color]>"
msgstr "B<--mode Color|Gray|Lineart [implicilt: Color]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Selects the scan mode. Lineart means fully black and fully white pixels only."
msgstr ""
"Selectează modul de scanare. Linear înseamnă numai pixeli complet negri și "
"complet albi."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--threshold 0..100 (in steps of 1) [default 75]>"
msgstr "B<--threshold 0..100 (in steps of 1) [implicit 75]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Select minimum-brightness percentage to get a white point, relevant only for "
"Lineart"
msgstr ""
"Selectați procentul minim de luminozitate pentru a obține un punct alb, "
"relevant doar pentru Lineart"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--non-blocking[=(yes|no)] [inactive]>"
msgstr "B<--non-blocking[=(yes|no)] [inactive]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This option has not yet been implemented. Scans are captured in a temporary "
"file with a typical size of 100MB."
msgstr ""
"Această opțiune nu a fost încă pusă în aplicare. Scanările sunt capturate "
"într-un fișier temporar cu o dimensiune tipică de 100Mo."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<Geometry:>"
msgstr "B<Geometrie:>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l 0..216.069 [default 0]>"
msgstr "B<-l 0..216.069 [implicit 0]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Top-left x position of scan area in millimeters."
msgstr "Poziția x din stânga-sus a zonei de scanare în milimetri."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t 0..297 [default 0]>"
msgstr "B<-t 0..297 [implicit 0]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Top-left y position of scan area in millimeters."
msgstr "Poziția y din stânga-sus a zonei de scanare în milimetri."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x 0..216.069 [default 80]>"
msgstr "B<-x 0..216.069 [implicit 80]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Width of scan-area in millimeters."
msgstr "Lățimea zonei de scanare în milimetri."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-y 0..297 [default 100]>"
msgstr "B<-y 0..297 [implicit 100]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Height of scan-area in millimeters."
msgstr "Înălțimea zonei de scanare în milimetri."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/sane.d/canon_lide70.conf>"
msgstr "I</etc/sane.d/canon_lide70.conf>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The backend configuration file"
msgstr "Fișierul de configurare al controlorului"

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-canon_lide70.a>"
msgstr "I</usr/lib/sane/libsane-canon_lide70.a>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Biblioteca statică care implementează acest controlor."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-canon_lide70.so>"
msgstr "I</usr/lib/sane/libsane-canon_lide70.so>"

# R-GC, scrie:
# pentru motivul traducerii:
# backend = controlor, a se vedea
# pagina:
# <https://en.wikipedia.org/wiki/
# Scanner_Access_Now_Easy>
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Biblioteca partajată care implementează acest controlor (prezentă pe "
"sistemele care acceptă încărcare dinamică)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MEDIU"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_CANON_LIDE70>"
msgstr "B<SANE_DEBUG_CANON_LIDE70>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Dacă biblioteca a fost compilată cu suportul de depanare activat, această "
"variabilă de mediu controlează nivelul de depanare pentru acest controlor. "
"Nivelurile mai mari de depanare cresc cantitatea de detalii informative a "
"ieșirii."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Example:"
msgstr "Exemplu:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "SANE_DEBUG_CANON_LIDE70=128 scanimage E<gt> /dev/null"
msgstr "SANE_DEBUG_CANON_LIDE70=128 scanimage E<gt> /dev/null"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "KNOWN PROBLEMS"
msgstr "PROBLEME CUNOSCUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At low resolutions (75 and 150 dpi, implying high slider speeds)  the LiDE "
"70 misses the top one millimeter of the scan area. This can be remedied by "
"shifting the document one millimeter downward, in cases where such precision "
"matters. Note that B<xsane>(1)  uses the 75 dpi mode for prescans. The "
"problem is worse on the LiDE 600(F), where the offset is five millimeters."
msgstr ""
"La rezoluții mici (75 și 150 dpi, ceea ce implică viteze mari de rulare), "
"LiDE 70 ratează ultimul milimetru al zonei de scanare. Acest lucru poate fi "
"remediat prin deplasarea documentului cu un milimetru în jos, în cazurile în "
"care o astfel de precizie contează. Rețineți că B<xsane>(1) utilizează modul "
"de 75 dpi pentru prescanare. Problema este mai gravă pe LiDE 600(F), unde "
"decalajul este de cinci milimetri."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sane>(7), B<sane-usb>(5), B<sane-find-scanner>(1), B<scanimage>(1), "
"B<xsane>(1),"
msgstr ""
"B<sane>(7), B<sane-usb>(5), B<sane-find-scanner>(1), B<scanimage>(1), "
"B<xsane>(1),"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "http://www.juergen-ernst.de/info_sane.html"
msgstr "http://www.juergen-ernst.de/info_sane.html"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "pimvantend, building upon pioneering work by Juergen Ernst."
msgstr "pimvantend, bazându-se pe munca de pionierat a lui Juergen Ernst."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-canon_lide70.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-canon_lide70.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-canon_lide70.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-canon_lide70.so>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-canon_lide70.a>"
msgstr "I</usr/lib64/sane/libsane-canon_lide70.a>"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-canon_lide70.so>"
msgstr "I</usr/lib64/sane/libsane-canon_lide70.so>"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"It is recommended that in B<xsane>(1)  the gamma value be set to "
"approximately 1.7 to get more realistic colors. This also wipes out some "
"artifacts caused by the lack of real calibration."
msgstr ""
"Se recomandă ca în B<xsane>(1) valoarea gamma să fie stabilită la "
"aproximativ 1,7 pentru a obține culori mai realiste. Acest lucru înlătură, "
"de asemenea, unele artefacte cauzate de lipsa unei calibrări reale."
