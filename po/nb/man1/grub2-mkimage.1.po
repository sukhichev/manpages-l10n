# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2024-03-01 16:58+0100\n"
"PO-Revision-Date: 2022-07-22 14:39+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKIMAGE"
msgstr "GRUB-MKIMAGE"

#. type: TH
#: fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "February 2024"
msgstr "Februar 2024"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-mkimage - make a bootable image of GRUB"
msgstr "grub-mkimage - lag et bilde av GRUB som maskinen kan starte opp"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<grub-mkimage> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,MODULES\\/>]"
msgstr ""
"B<grub-mkimage> [I<\\,VALG\\/>...] [I<\\,VALG\\/>]... [I<\\,MODULER\\/>]"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Make a bootable image of GRUB."
msgstr "Lag et bilde av GRUB som maskinen kan starte opp."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--config>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--config>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "embed FILE as an early config"
msgstr "legg inn valgt FIL som et tidlig oppsett"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--compression=>(xz|none|auto)"
msgstr "B<-C>, B<--compression=>(xz|none|auto)"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "choose the compression to use for core image"
msgstr "velg komprimeringsmetoden som skal brukes på kjernebildet"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "disable shim_lock verifier"
msgstr ""

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--dtb>=I<\\,FILE\\/>"
msgstr "B<-D>, B<--dtb>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "embed FILE as a device tree (DTB)"
msgstr "bygg inn FIL som enhetstre (DTB)"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid "embed FILE as public key for signature checking"
msgid "embed FILE as public key for PGP signature checking"
msgstr "bruk valgt FIL som offentlig nøkkel for signaturkontroll"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>,                              B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<-m>,                              B<--memdisk>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid "use FILE as memdisk"
msgid "embed FILE as a memdisk image"
msgstr "bruk valgt FIL som minnedisk"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Implies `-p (memdisk)/boot/grub' and overrides"
msgstr ""

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "any prefix supplied previously, but the prefix"
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "itself can be overridden by later options"
msgstr ""

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--note>"
msgstr "B<-n>, B<--note>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "add NOTE segment for CHRP IEEE1275"
msgstr "legg til NOTE-segment for CHRP IEEE1275"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "output a generated image to FILE [default=stdout]"
msgstr "skriv ut et generert bilde til FIL (standard=stdout)"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-O>, B<--format>=I<\\,FORMAT\\/>"
msgstr "B<-O>, B<--format>=I<\\,FORMAT\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"
msgstr ""
"lag et bilde i valgt FORMAT; tilgjengelige formater: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--prefix>=I<\\,DIR\\/>"
msgstr "B<-p>, B<--prefix>=I<\\,MAPPE\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "set prefix directory"
msgstr "velg prefiks-mappe"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--sbat>=I<\\,FILE\\/>"
msgstr "B<-s>, B<--sbat>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "SBAT metadata"
msgstr ""

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--appended-signature-size>=I<\\,SIZE\\/>"
msgstr "B<-S>, B<--appended-signature-size>=I<\\,STØRR\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Add a note segment reserving SIZE bytes for an appended signature"
msgstr ""

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print verbose messages."
msgstr "skriv ut detaljerte meldinger"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--x509>=I<\\,FILE\\/>"
msgstr "B<-x>, B<--x509>=I<\\,FIL\\/>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
#| msgid "embed FILE as public key for signature checking"
msgid "embed FILE as an x509 certificate for appended signature checking"
msgstr "bruk valgt FIL som offentlig nøkkel for signaturkontroll"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "vis denne hjelpelista"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "vis en kortfattet bruksanvisning"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "skriv ut programversjon"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Valg er enten obligatoriske både for fullstendige valg og tilsvarende "
"forkortede valg."

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Rapporter feil til E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"
msgstr "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkimage> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkimage> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullstendige dokumentasjonen for B<grub-mkimage> opprettholdes som en "
"Texinfo manual. Dersom B<info> og B<grub-mkimage> programmene er riktig "
"installert på ditt sted burde kommandoen"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-mkimage>"
msgstr "B<info grub-mkimage>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "gi deg tilgang til hele manualen."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "September 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "Januar 2024"

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "grub-mkimage (GRUB2) 2.06"
msgid "grub-mkimage (GRUB2) 2.12"
msgstr "grub-mkimage (GRUB2) 2.06"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"use images and modules under DIR [default=/usr/share/grub2/"
"E<lt>platformE<gt>]"
msgstr ""
"use images and modules under DIR [standard=/usr/share/grub2/"
"E<lt>platformE<gt>]"

#. type: Plain text
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, riscv32-"
"efi, riscv64-efi"
msgstr ""
"lag et bilde i valgt FORMAT; tilgjengelige formater: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, riscv32-"
"efi, riscv64-efi"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "GRUB2 2.12"
