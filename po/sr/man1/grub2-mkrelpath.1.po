# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2024-03-01 16:58+0100\n"
"PO-Revision-Date: 2022-07-23 16:31+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKRELPATH"
msgstr "GRUB-MKRELPATH"

#. type: TH
#: fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "February 2024"
msgstr "Фебруара 2024"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "ГРУБ 2.06"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "grub-mkrelpath - make a system path relative to its root"
msgstr ""

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-mkrelpath> [I<\\,OPTION\\/>...] I<\\,PATH\\/>"
msgstr "B<grub-mkrelpath> [I<\\,ОПЦИЈА\\/>...] I<\\,ПУТАЊА\\/>"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Transform a system filename into GRUB one."
msgstr "Претвара састемски назив датотеке у онај ГРУБ-ов."

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--relative>"
msgstr "B<-r>, B<--relative>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "use relative path on btrfs"
msgstr ""

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "приказује овај списак помоћи"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "приказује кратку поруку коришћења"

#. type: TP
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "print program version"
msgstr "исписује издање програма"

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Грешке пријавите на: E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkrelpath> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkrelpath> programs are properly "
"installed at your site, the command"
msgstr ""
"Потпуна документација за B<grub-mkrelpath> је одржавана као Тексинфо "
"упутство.  Ако су B<info> и B<grub-mkrelpath> исправно инсталирани на вашем "
"сајту, наредба"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<info grub-mkrelpath>"
msgstr "B<info grub-mkrelpath>"

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2023"
msgstr "Септембра 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "January 2024"
msgstr "јануара 2024"

#. type: TH
#: opensuse-leap-15-6
#, fuzzy, no-wrap
#| msgid "grub-mkrelpath (GRUB2) 2.06"
msgid "grub-mkrelpath (GRUB2) 2.12"
msgstr "grub-mkrelpath (ГРУБ2) 2.06"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.12"
msgstr "ГРУБ 2.12"
