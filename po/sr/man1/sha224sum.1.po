# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2024-02-15 18:10+0100\n"
"PO-Revision-Date: 2021-09-03 20:06+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SHA224SUM"
msgstr "SHA224SUM"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "January 2024"
msgstr "јануара 2024"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "ГНУ coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sha224sum - compute and check SHA224 message digest"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<sha224sum> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<sha224sum> [I<\\,ОПЦИЈА\\/>]... [I<\\,ДАТОТЕКА\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print or check SHA224 (224-bit) checksums."
msgstr "Исписује или проверава „SHA224“ (224-бит) суме провере."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Без ДАТОТЕКЕ, или када је ДАТОТЕКА -, чита стандардни улаз."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--binary>"
msgstr "B<-b>, B<--binary>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read in binary mode"
msgstr "чита у извршном режиму"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--check>"
msgstr "B<-c>, B<--check>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "read SHA224 sums from the FILEs and check them"
msgid "read checksums from the FILEs and check them"
msgstr "чита суме „SHA224“ из ДАТОТЕКЕ и проверава их"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--tag>"
msgstr "B<--tag>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "create a BSD-style checksum"
msgstr "ствара суму провере у БСД стилу"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--text>"
msgstr "B<-t>, B<--text>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read in text mode (default)"
msgstr "чита у текстуалном режиму (основно)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"end each output line with NUL, not newline, and disable file name escaping"
msgstr ""
"завршава сваки излазни ред са НИШТА, не новим редом и искључује прескакање "
"назива датотеке"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "The following five options are useful only when verifying checksums:"
msgstr "Следеће пет опције су корисне само приликом проверавања сума провере:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-missing>"
msgstr "B<--ignore-missing>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't fail or report status for missing files"
msgstr "не урушава се и не извештава о стању за датотеке које недостају"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--quiet>"
msgstr "B<--quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't print OK for each successfully verified file"
msgstr "не исписује У реду за сваку успешно проверену датотеку"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--status>"
msgstr "B<--status>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't output anything, status code shows success"
msgstr "не даје никакав излаз, код стања приказује успех"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--strict>"
msgstr "B<--strict>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "exit non-zero for improperly formatted checksum lines"
msgstr "излази са не-нулом за неисправно обликоване редове суме провере"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--warn>"
msgstr "B<-w>, B<--warn>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "warn about improperly formatted checksum lines"
msgstr "упозорава о неисправно обликованим редовима суме провере"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "приказује ову помоћ и излази"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "исписује податке о издању и излази"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The sums are computed as described in RFC 3874.  When checking, the input "
#| "should be a former output of this program.  The default mode is to print "
#| "a line with checksum, a space, a character indicating input mode ('*' for "
#| "binary, \\&' ' for text or where binary is insignificant), and name for "
#| "each FILE."
msgid ""
"The sums are computed as described in RFC 3874.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with: checksum, a space, a character indicating input mode ('*' for "
"binary, ' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""
"Суме се израчунавају као што је описано у „РФЦ 3874“.  Приликом провере, "
"улаз треба да буде бивши излаз тог програма.  Основни режим је исписивање "
"реда сумом провере, размаком, знаком који указује на улазни режим (* за "
"извршне, размак размак за текст или где је извршни безначајан), и назив за "
"сваку ДАТОТЕКУ."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note: There is no difference between binary mode and text mode on GNU "
"systems."
msgstr ""
"Напомена: Не постоји разлика између бинарног и текстуалног режима на ГНУ "
"системима."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АУТОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Ulrich Drepper, Scott Miller, and David Madore."
msgstr "Написали су Улрих Дрепер, Скот Милер и Дејвид Мадоре."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Помоћ на мрежи за ГНУ coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Грешке у преводу пријавите на E<lt>https://translationproject.org/team/sr."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АУТОРСКА ПРАВА"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ово је слободан софтвер: слободни сте да га мењате и расподељујете. Не "
"постоји НИКАКВА ГАРАНЦИЈА, у оквирима дозвољеним законом."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<cksum>(1)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/sha224sumE<gt>"
msgstr ""
"Сва документација се налази на E<lt>https://www.gnu.org/software/coreutils/"
"sha224sumE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) sha2 utilities\\(aq"
msgstr ""
"или је доступна на рачунару путем наредбе „info \\(aq(coreutils) sha2 "
"utilities\\(aq“"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "Септембра 2022"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "ГНУ coreutils 9.1"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Априла 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Октобра 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "ГНУ coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "read SHA224 sums from the FILEs and check them"
msgstr "чита суме „SHA224“ из ДАТОТЕКЕ и проверава их"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The sums are computed as described in RFC 3874.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with checksum, a space, a character indicating input mode ('*' for "
"binary, \\&' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""
"Суме се израчунавају као што је описано у „РФЦ 3874“.  Приликом провере, "
"улаз треба да буде бивши излаз тог програма.  Основни режим је исписивање "
"реда сумом провере, размаком, знаком који указује на улазни режим (* за "
"извршне, размак размак за текст или где је извршни безначајан), и назив за "
"сваку ДАТОТЕКУ."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
