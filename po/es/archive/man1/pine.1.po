# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Ángel Rojas Aquino <mrojas_aquino@mail.flashmail.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-01-22 20:56+01:00\n"
"PO-Revision-Date: 1999-04-20 19:53+0200\n"
"Last-Translator: Miguel Ángel Rojas Aquino <mrojas_aquino@mail.flashmail."
"com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: man1/pine.1:1
#, no-wrap
msgid "pine"
msgstr "pine"

#. type: TH
#: man1/pine.1:1
#, no-wrap
msgid "Version 4.10"
msgstr "Versión 4.10"

#. type: SH
#: man1/pine.1:2
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: man1/pine.1:4
msgid "pine - a Program for Internet News and Email"
msgstr "pine - un Programa para Noticias de Internet y Correo Electrónico"

#. type: SH
#: man1/pine.1:4
#, no-wrap
msgid "SYNTAX"
msgstr "SINTAXIS"

#. type: Plain text
#: man1/pine.1:14
msgid "B<pine> [ I<options> ] [ I<address> , I<address> ]"
msgstr "B<pine> [ I<opciones> ] [ I<dirección> , I<dirección> ]"

#. type: Plain text
#: man1/pine.1:23
msgid "B<pinef> [ I<options> ] [ I<address> , I<address> ]"
msgstr "B<pinef> [ I<opciones> ] [ I<dirección> , I<dirección> ]"

#. type: SH
#: man1/pine.1:23
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: man1/pine.1:33
msgid ""
"Pine is a screen-oriented message-handling tool.  In its default "
"configuration, Pine offers an intentionally limited set of functions geared "
"toward the novice user, but it also has a growing list of optional \"power-"
"user\" and personal-preference features.  I<pinef> is a variant of Pine that "
"uses function keys rather than mnemonic single-letter commands.  Pine's basic "
"feature set includes:"
msgstr ""
"Pine es una herramienta de manejo de mensajes orientada a la pantalla. En su "
"configuración por defecto, Pine ofrece un conjunto de funciones "
"intencionalmente limitado orientado al usuario novel, pero también cuenta con "
"una creciente lista de opciones para los usuarios avanzados y características "
"de preferencias personales.  I<pinef> es una variante de Pine que utiliza las "
"teclas de función en lugar de comandos mnemónicos de una sola letra.  El "
"conjunto de características básicas de Pine incluyen:"

#. type: Plain text
#: man1/pine.1:35
msgid "View, Save, Export, Delete, Print, Reply and Forward messages."
msgstr ""
"Ver (View), Guardar (Save), Exportar (Export), Borrar (Delete), Imprimir "
"(Print), Responder (Reply) y Canalizar (Forward) mensajes."

#. type: Plain text
#: man1/pine.1:38
msgid ""
"Compose messages in a simple editor (Pico) with word-wrap and a spelling "
"checker.  Messages may be postponed for later completion."
msgstr ""
"Prepara mensajes en un editor sencillo (Pico) con acomodamiento de palabras y "
"verificación ortográfica. Los mensajes se puede posponer para su posterior "
"terminación."

#. type: Plain text
#: man1/pine.1:40
msgid "Full-screen selection and management of message folders."
msgstr "Selección y manejo a pantalla completa de las carpetas de mensajes."

#. type: Plain text
#: man1/pine.1:45
msgid ""
"Address book to keep a list of long or frequently-used addresses.  Personal "
"distribution lists may be defined.  Addresses may be taken into the address "
"book from incoming mail without retyping them."
msgstr ""
"Libro de direcciones para mantener una lista de direcciones largas o de uso "
"frecuente. Es posible definir listas personales de distribución. Las "
"direcciones se pueden tomar del correo entrante sin necesidad de volverlas a "
"teclear."

#. type: Plain text
#: man1/pine.1:48
msgid ""
"New mail checking and notification occurs automatically every 2.5 minutes and "
"after certain commands, e.g. refresh-screen (Ctrl-L)."
msgstr ""
"La verificación y notificación de nuevo correo ocurre automáticamente cada "
"2.5 minutos y después de ciertos comando, por ejemplo actualizar pantalla "
"(ctrl-L)"

#. type: Plain text
#: man1/pine.1:50
msgid "On-line, context-sensitive help screens."
msgstr "Pantallas de ayuda en línea sensibles al contexto."

#. type: Plain text
#: man1/pine.1:65
msgid ""
"Pine supports MIME (Multipurpose Internet Mail Extensions), an Internet "
"Standard for representing multipart and multimedia data in email.  Pine "
"allows you to save MIME objects to files, and in some cases, can also "
"initiate the correct program for viewing the object.  It uses the system's "
"I<mailcap> configuration file to determine what program can process a "
"particular MIME object type.  Pine's message composer does not have integral "
"multimedia capability, but any type of data file --including multimedia-- can "
"be attached to a text message and sent using MIME's encoding rules.  This "
"allows any group of individuals with MIME-capable mail software (e.g. Pine, "
"PC-Pine, or many other programs) to exchange formatted documents, spread-"
"sheets, image files, etc, via Internet email."
msgstr ""
"Pine soporta MIME (Multipurpose Internet Mail Extensions), una norma de "
"Internet para representar datos multipartes y multimedia en el correo "
"electrónico. Pine permite guardar objetos MIME en un fichero, y en algunos "
"casos, puede iniciar el programa adecuado para visualizar el objeto. Esto "
"utiliza el fichero I<mailcap> de configuración del sistema para determinar "
"que programa puede procesar un tipo de objeto MIME particular.  El compositor "
"de mensajes de Pine no cuenta con capacidades multimedia integrales, pero "
"cualquier tipo de fichero de datos --incluyendo multimedia-- puede ser "
"añadido a un mensaje de texto y ser enviado utilizando las reglas de "
"codificación MIME. Esto permite a cualquier grupo de individuos con programas "
"de correo con capacidades MIME (por ejemplo Pine, Pine-PC, y varios otros "
"programas) intercambiar documentos formateados, hojas de cálculo, ficheros de "
"imagen, etc., por medio del correo electrónico de Internet."

#. type: Plain text
#: man1/pine.1:78
msgid ""
"Pine uses the I<c-client> messaging API to access local and remote mail "
"folders. This library provides a variety of low-level message-handling "
"functions, including drivers for a variety of different mail file formats, as "
"well as routines to access remote mail and news servers, using IMAP (Internet "
"Message Access Protocol) and NNTP (Network News Transport Protocol).  "
"Outgoing mail is usually handed-off to the Unix I<sendmail>, program but it "
"can optionally be posted directly via SMTP (Simple Mail Transfer Protocol)."
msgstr ""
"Pine utiliza la API de mensajería I<c-client> para acceder a carpetas de "
"correo locales y remotas. Esta biblioteca provee una variedad de funciones de "
"bajo nivel para el manejo de mensajes, incluyendo manejadores para una "
"variedad de diferentes formatos de ficheros de correo, así como rutinas para "
"acceder a servidores remotos de correo y noticias, utilizando IMAP (Internet "
"Message Access Protocol) y NNTP (Network News Transport Protocol).  El correo "
"de salida generalmente se transfiere al programa Unix I<sendmail>, pero "
"opcionalmente puede ser enviado por medio de SMTP (Simple Mail Transfer "
"Protocol)."

#. type: SH
#: man1/pine.1:78
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: Plain text
#: man1/pine.1:83
msgid "The command line options/arguments are:"
msgstr "Las opciones/argumentos de la línea de comandos son:"

#. type: IP
#: man1/pine.1:83
#, no-wrap
msgid "I<address>"
msgstr "I<dirección>"

#. type: Plain text
#: man1/pine.1:87
msgid ""
"Send mail to I<address.> This will cause Pine to go directly into the message "
"composer."
msgstr ""
"Enviar correo a I<dirección.> Esto hace que Pine vaya directamente al "
"compositor de mensajes."

#. type: IP
#: man1/pine.1:87
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: man1/pine.1:89
msgid "Special anonymous mode for UWIN*"
msgstr "Modo especial anónimo para UWIN*"

#. type: IP
#: man1/pine.1:89
#, no-wrap
msgid "B<-attach\\ >I<file>"
msgstr "B<-attach\\ >I<fichero>"

#. type: Plain text
#: man1/pine.1:93
msgid "Send mail with the listed I<file> as an attachment."
msgstr "Enviar correo con el I<fichero> listado como un anexo."

#. type: IP
#: man1/pine.1:93
#, no-wrap
msgid "B<-attach_and_delete\\ >I<file>"
msgstr "B<-attach_and_delete\\ >I<fichero>"

#. type: Plain text
#: man1/pine.1:98
msgid ""
"Send mail with the listed I<file> as an attachment, and remove the file after "
"the message is sent."
msgstr ""
"Enviar correo con el I<fichero> como un anexo, y eliminar el fichero después "
"de enviar el mensaje."

#. type: IP
#: man1/pine.1:98
#, no-wrap
msgid "B<-attachlist\\ >I<file-list>"
msgstr "B<-attachlist\\ >I<lista-ficheros>"

#. type: Plain text
#: man1/pine.1:102
msgid "Send mail with the listed I<file-list> as an attachments."
msgstr "Enviar correo con la I<lista-ficheros> como un anexo."

#. type: IP
#: man1/pine.1:102
#, no-wrap
msgid "B<-c\\ >I<context-number>"
msgstr "B<-c\\ >I<número-contexto>"

#. type: Plain text
#: man1/pine.1:109
msgid ""
"context-number is the number corresponding to the folder-collection to which "
"the I<-f> command line argument should be applied.  By default the I<-f> "
"argument is applied to the first defined folder-collection."
msgstr ""
"número contexto es el número correspondiente a la colección de carpetas a la "
"cual el argumento I<-f> de la línea de comandos debe ser aplicado. Por "
"defecto, el argumento I<-f> se aplica a la primera colección definida de "
"carpetas."

#. type: IP
#: man1/pine.1:109
#, no-wrap
msgid "B<-d\\ >I<debug-level>"
msgstr "B<-d\\ >I<nivel-depuración>"

#. type: Plain text
#: man1/pine.1:117
msgid ""
"Output diagnostic info at I<debug-level> (0-9) to the current I<.pine-"
"debug[1-4]> file.  A value of 0 turns debugging off and suppresses the I<."
"pine-debug> file."
msgstr ""
"Mandar información de diagnóstico en el I<nivel-depuración> (0-9) al fichero "
"I<.pine-debug[1-4]> actual.  Un valor de 0 desactiva la depuración y elimina "
"el fichero I<.pine-debug>."

#. type: IP
#: man1/pine.1:117
#, no-wrap
msgid "B<-d\\ >I<key[=val]>"
msgstr "B<-d\\ >I<clave[=valor]>"

#. type: Plain text
#: man1/pine.1:126
msgid ""
"Fine tuned output of diagnostic messages where \"flush\" causes debug file "
"writing without buffering, \"timestamp\" appends each message with a "
"timestamp, \"imap=n\" where n is between 0 and 4 representing none to verbose "
"IMAP telemetry reporting, \"numfiles=n\" where n is between 0 and 31 "
"corresponding to the number of debug files to maintain, and \"verbose=n\" "
"where n is between 0 and 9 indicating an inverse threshold for message output."
msgstr ""
"Salida afinada de mensajes de diagnóstico, en donde \"flush\" provoca que el "
"fichero de depuración se escriba sin buffering, \"timestamp agrega a cada "
"mensaje un indicador de fecha, \"imap=n\" donde n está entre 0 y 4 representa "
"de nada a salida completa en el reporte de telemetría IMAP, \"numfiles=n\" "
"donde n está entre 0 y 31 corresponde al número de ficheros de depuración que "
"se desea mantener, y \"verbose=n\" donde n está entre 0 y 9 indica un límite "
"inverso para la salida de mensajes."

#. type: IP
#: man1/pine.1:126
#, no-wrap
msgid "B<-f\\ >I<folder>"
msgstr "B<-f\\ >I<carpeta>"

#. type: Plain text
#: man1/pine.1:132
msgid ""
"Open I<folder> (in first defined folder collection, use I<-c n> to specify "
"another collection) instead of INBOX."
msgstr ""
"Abrir I<carpeta> (en la primer colección de carpetas definida, utilizar I<-c "
"n> para especificar otra colección) en lugar de INBOX."

#. type: IP
#: man1/pine.1:132
#, no-wrap
msgid "B<-F\\ >I<file>"
msgstr "B<-F\\ >I<fichero>"

#. type: Plain text
#: man1/pine.1:134
msgid "Open named text file and view with Pine's browser."
msgstr "Abrir el fichero de texto nombrado con el navegador de Pine."

#. type: IP
#: man1/pine.1:134
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: man1/pine.1:136
msgid "Help: list valid command-line options."
msgstr "Ayuda: listar las opciones de válidas de la línea de comandos."

#. type: IP
#: man1/pine.1:136
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: man1/pine.1:138
msgid "Start up in the FOLDER INDEX screen."
msgstr "Iniciar en la pantalla FOLDER INDEX."

#. type: IP
#: man1/pine.1:138
#, no-wrap
msgid "B<-I\\ >I<keystrokes>"
msgstr "B<-I\\ >I<teclas>"

#. type: Plain text
#: man1/pine.1:141
msgid ""
"Initial (comma separated list of) keystrokes which Pine should execute on "
"startup."
msgstr ""
"Lista de teclas (separadas por comas) que Pine debe ejecutar al iniciarse."

#. type: IP
#: man1/pine.1:141
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: man1/pine.1:144
msgid ""
"Use function keys for commands. This is the same as running the command "
"I<pinef>."
msgstr ""
"Utilizar las teclas de función para los comandos. Es igual a ejecutar el "
"comando I<pinef>."

#. type: IP
#: man1/pine.1:144
#, no-wrap
msgid "B<-n\\ >I<number>"
msgstr "B<-n\\ >I<número>"

#. type: Plain text
#: man1/pine.1:147
msgid "Start up with current message-number set to I<number.>"
msgstr "Iniciar con el número de mensaje actual establecido en I<número.>"

#. type: IP
#: man1/pine.1:147
#, no-wrap
msgid "B<-nr>"
msgstr "B<-nr>"

#. type: Plain text
#: man1/pine.1:149
msgid "Special mode for UWIN*"
msgstr "Modo especial para UWIN*"

#. type: IP
#: man1/pine.1:149
#, no-wrap
msgid "B<-o>"
msgstr "B<-o>"

#. type: Plain text
#: man1/pine.1:151
msgid "Open first folder read-only."
msgstr "Abrir la primer carpeta como de sólo lectura"

#. type: IP
#: man1/pine.1:151
#, no-wrap
msgid "B<-p\\ >I<config-file>"
msgstr "B<-p\\ >I<fichero-configuración>"

#. type: Plain text
#: man1/pine.1:156
msgid ""
"Use I<config-file> as the personal configuration file instead of the default "
"I<.pinerc>."
msgstr ""
"Utilizar I<fichero-configuración> como el fichero de configuración personal "
"en lugar de I<.pinerc>."

#. type: IP
#: man1/pine.1:156
#, no-wrap
msgid "B<-P\\ >I<config-file>"
msgstr "B<-P\\ >I<fichero-configuración>"

#. type: Plain text
#: man1/pine.1:162
msgid ""
"Use I<config-file> as the configuration file instead of default system-wide "
"configuration file I<pine.conf>."
msgstr ""
"Utilizar I<fichero-configuración> en lugar del fichero de configuración "
"general del sistema I<pine.conf>."

#. type: IP
#: man1/pine.1:162
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: man1/pine.1:167
msgid ""
"Use restricted/demo mode.  I<Pine> will only send mail to itself and "
"functions like save and export are restricted."
msgstr ""
"Utilizar el modo restringido/demostración.  I<Pine> se mandará correo a sí "
"mismo únicamente y las funciones como guardar o exportar están restringidas."

#. type: IP
#: man1/pine.1:167
#, no-wrap
msgid "B<-url\\ >I<url>"
msgstr "B<-url\\ >I<url>"

#. type: Plain text
#: man1/pine.1:175
msgid ""
"Open the given I<url.> Cannot be used with I<-f, -F,> or I<-attach> options."
msgstr ""
"Abrir el I<url> especificado.  No se puede usar con combinación con las "
"opciones I<-f>, I<-F> o I<-attach>."

#. type: IP
#: man1/pine.1:175
#, no-wrap
msgid "B<-z>"
msgstr "B<-z>"

#. type: Plain text
#: man1/pine.1:177
msgid "Enable ^Z and SIGTSTP so pine may be suspended."
msgstr "Habilita ^Z y SIGTSTP de manera que pine puede ser suspendido."

#. type: IP
#: man1/pine.1:177
#, no-wrap
msgid "B<-conf>"
msgstr "B<-conf>"

#. type: Plain text
#: man1/pine.1:184
msgid ""
"Produce a sample/fresh copy of the system-wide configuration file, I<pine."
"conf,> on the standard output. This is distinct from the per-user I<.pinerc> "
"file."
msgstr ""
"Produce una copia/muestra reciente del fichero de configuración del sistema, "
"I<pine.conf>, en la salida estándar. Este es diferente del fichero I<.pinerc> "
"de cada usuario individual."

#. type: IP
#: man1/pine.1:184
#, no-wrap
msgid "B<-create_lu\\ >I<addrbook\\ sort-order>"
msgstr "B<-create_lu\\ >I<agenda\\ orden>"

#. type: Plain text
#: man1/pine.1:209
msgid ""
"Creates auxiliarly index (look-up) file for I<addrbook> and sorts I<addrbook> "
"in I<sort-order>, which may be I<dont-sort>, I<nickname>, I<fullname>, "
"I<nickname-with-lists-last>, or I<fullname-with-lists-last>.  Useful when "
"creating global or shared address books.  After creating the index file in "
"this way, the file should be moved or copied in a way which preserves the "
"mtime of the address book file.  The mtime of the address book file at the "
"time the index file was built is stored inside the index file and a "
"comparison between that stored value and the current mtime of the address "
"book file is done when somebody runs pine.  If the mtime has changed since "
"the index file was made, then pine will want to rebuild the index file.  In "
"other words, don't build the index file with this option and then copy the "
"address book to its final destination in a way which changes the file's mtime."
msgstr ""
"Crea un fichero índice auxiliar para I<agenda> y ordena I<agenda> de acuerdo "
"a I<orden>, el cual puede ser I<dont-sort> (no ordenar), I<nickname> (apodo), "
"I<fullname> (nombre completo), I<nickname-with-lists-last> o I<fullname-with-"
"lists-last>.  Útil cuando se crean agendas globales o compartidas.  Después "
"de crear un fichero índice por este método, el fichero debe ser movido o "
"copiado en una forma en que conserve el mtime del fichero de agenda. El mtime "
"del fichero de agenda al momento en que el fichero índice fue creado se "
"almacena en el fichero índice y se hace una comparación entre el valor "
"almacenado y el mtime actual del fichero de agenda cuando alguien ejecuta "
"pine. Si el mtime ha cambiado desde que el fichero índice fue creado, "
"entonces pine reconstruiría el fichero índice.  En otras palabras, no se debe "
"generar el fichero índice con esta opción y luego cambiar la agenda a su "
"destino final en una forma en que modifique el mtime del fichero."

#. type: IP
#: man1/pine.1:209
#, no-wrap
msgid "B<-pinerc\\ >I<file>"
msgstr "B<-pinerc\\ >I<fichero>"

#. type: Plain text
#: man1/pine.1:212
msgid "Output fresh pinerc configuration to I<file.>"
msgstr "Vacía una copia reciente de pinerc al I<fichero.>"

#. type: IP
#: man1/pine.1:212
#, no-wrap
msgid "B<-sort\\ >I<order>"
msgstr "B<-sort\\ >I<orden>"

#. type: Plain text
#: man1/pine.1:225
msgid ""
"Sort the FOLDER INDEX display in one of the following orders: I<arrival, "
"subject, from, date, size, orderedsubj> or I<reverse. Arrival> order is the "
"default.  The OrderedSubj choice simulates a threaded sort.  Any sort may be "
"reversed by adding I</reverse> to it.  I<Reverse> by itself is the same as "
"I<arrival/reverse>."
msgstr ""
"Ordena la pantalla FOLDER INDEX en alguna de las siguientes formas: "
"I<arrival> (llegada), I<subject> (asunto), I<from> (de), I<date> (fecha), "
"I<size> (tamaño), I<orderedsubj> (asunto ordenado)  o I<reverse> (inverso).  "
"I<Arrival> es la opción por defecto.  La opción OrderedSubj simula un orden "
"por hilos.  Cualquier formato de ordenamiento puede ser invertido agregándole "
"I</reverse>.  I<Reverse> Por si misma es igual a I<arrival/reverse>."

#. type: IP
#: man1/pine.1:225
#, no-wrap
msgid "I<-option\\=value>"
msgstr "I<-option\\=valor>"

#. type: Plain text
#: man1/pine.1:232
msgid ""
"Assign I<value> to the config option I<option> e.g. -signature-file=sig1 or -"
"feature-list=signature-at-bottom (Note: feature-list values are additive)"
msgstr ""
"Asignar I<valor> a la opción de configuración I<opción> por ejemplo -"
"signature-file=sig1 o -feature-list=signature-at-bottom (Nota: los valores "
"feature-list son aditivos)"

#. type: Plain text
#: man1/pine.1:234
msgid "* UWIN = University of Washington Information Navigator"
msgstr "* UWIN = University of Washington Information Navigator"

#. type: SH
#: man1/pine.1:234
#, no-wrap
msgid "CONFIGURATION"
msgstr "CONFIGURACIÓN"

#. type: Plain text
#: man1/pine.1:239
msgid ""
"There are several levels of Pine configuration.  Configuration values at a "
"given level over-ride corresponding values at lower levels.  In order of "
"increasing precedence:"
msgstr ""
"Existen varios niveles de configuración en Pine. Los valores de configuración "
"en un determinado nivel se imponen a los valores en niveles inferiores. En "
"orden ascendente de precedencia:"

#. type: Plain text
#: man1/pine.1:241
#, no-wrap
msgid " o built-in defaults.\n"
msgstr " o interconstruidos por defecto.\n"

#. type: Plain text
#: man1/pine.1:245
#, no-wrap
msgid ""
" o system-wide \n"
"I<pine.conf>\n"
"file.\n"
msgstr ""
" o fichero  \n"
"I<pine.conf>\n"
"de configuración global del sistema.\n"

#. type: Plain text
#: man1/pine.1:249
#, no-wrap
msgid ""
" o personal \n"
"I<.pinerc>\n"
"file (may be set via built-in Setup/Config menu.)\n"
msgstr ""
" o fichero \n"
"I<.pinerc>\n"
"personal (se puede establecer por medio del menú Setup/Config.)\n"

#. type: Plain text
#: man1/pine.1:251
#, no-wrap
msgid " o command-line options.\n"
msgstr " o opciones de la línea de comandos.\n"

#. type: Plain text
#: man1/pine.1:255
#, no-wrap
msgid ""
" o system-wide \n"
"I<pine.conf.fixed>\n"
"file.\n"
msgstr ""
" o fichero \n"
"I<pine.conf.fixed>\n"
"de configuración global del sistema.\n"

#. type: Plain text
#: man1/pine.1:261
msgid ""
"There is one exception to the rule that configuration values are replaced by "
"the value of the same option in a higher-precedence file: the feature-list "
"variable has values that are additive, but can be negated by prepending \"no-"
"\" in front of an individual feature name. Unix Pine also uses the following "
"environment variables:"
msgstr ""
"Hay una excepción a la regla de que los valores de configuración son "
"reemplazados por el valor de la misma opción en un nivel de precedencia más "
"alto: la variable feature-list tiene valores que son aditivos, pero pueden "
"ser negados añadiendo \"no-\" antes de un nombre individual de "
"característica. Así mismo, Pine para Unix utiliza las siguientes variable de "
"ambiente:"

#. type: Plain text
#: man1/pine.1:263
#, no-wrap
msgid "  TERM\n"
msgstr "  TERM\n"

#. type: Plain text
#: man1/pine.1:265
#, no-wrap
msgid "  DISPLAY     (determines if Pine can display IMAGE attachments.)\n"
msgstr "  DISPLAY     (determina si Pine puede presentar anexos IMAGE.)\n"

#. type: Plain text
#: man1/pine.1:267
#, no-wrap
msgid "  SHELL       (if not set, default is /bin/sh )\n"
msgstr "  SHELL       (si no está establecida, por defecto es /bin/sh )\n"

#. type: Plain text
#: man1/pine.1:269
#, no-wrap
msgid "  MAILCAPS    (semicolon delimited list of path names to mailcap files)\n"
msgstr ""
"  MAILCAPS    (lista de nombres de ruta separada por punto y coma a los\n"
"ficheros mailcap)\n"

#. type: SH
#: man1/pine.1:269
#, no-wrap
msgid "FILES"
msgstr "FICHEROS"

#. type: Plain text
#: man1/pine.1:274
msgid "/usr/spool/mail/xxxx\tDefault folder for incoming mail."
msgstr "/usr/spool/mail/xxxx\tCarpeta por defecto para el correo entrante."

#. type: Plain text
#: man1/pine.1:276
msgid "~/mail\tDefault directory for mail folders."
msgstr "~/mail\tDirectorio por defecto para las carpetas de correo."

#. type: Plain text
#: man1/pine.1:278
msgid "~/.addressbook\tDefault address book file."
msgstr "~/.addressbook\tFichero agenda por defecto."

#. type: Plain text
#: man1/pine.1:280
msgid "~/.addressbook.lu\tDefault address book index file."
msgstr "~/.addressbook.lu\tFichero índice de agenda por defecto."

#. type: Plain text
#: man1/pine.1:282
msgid "~/.pine-debug[1-4]\tDiagnostic log for debugging."
msgstr "~/.pine-debug[1-4]\tRegistro de diagnóstico para depuración."

#. type: Plain text
#: man1/pine.1:284
msgid "~/.pinerc\tPersonal pine config file."
msgstr "~/.pinerc\tFichero personal de configuración de Pine."

#. type: Plain text
#: man1/pine.1:286
msgid "~/.newsrc\tNews subscription/state file."
msgstr "~/.newsrc\tFichero de suscripción/estado de News."

#. type: Plain text
#: man1/pine.1:288
msgid "~/.signature\tDefault signature file."
msgstr "~/.signature\tFichero de firmas por defecto."

#. type: Plain text
#: man1/pine.1:290
msgid "~/.mailcap\tPersonal mail capabilities file."
msgstr "~/.mailcap\tFichero personal de capacidades de correo."

#. type: Plain text
#: man1/pine.1:292
msgid "~/.mime.types\tPersonal file extension to MIME type mapping"
msgstr "~/.mime.types\tFichero personal de extensiones de mapeo de tipos MIME."

#. type: Plain text
#: man1/pine.1:294
msgid "/etc/mailcap\tSystem-wide mail capabilities file."
msgstr "/etc/mailcap\tFichero general de sistema de capacidades de correo."

#. type: Plain text
#: man1/pine.1:296
msgid "/etc/mime.types\tSystem-wide file ext. to MIME type mapping"
msgstr ""
"/etc/mime.types\tFichero general de sistema de mapeo de extensiones de tipos "
"MIME."

#. type: Plain text
#: man1/pine.1:298
msgid "/usr/local/lib/pine.info\tLocal pointer to system administrator."
msgstr "/usr/local/lib/pine.info\tPuntero local al administrador del sistema."

#. type: Plain text
#: man1/pine.1:300
msgid "/usr/local/lib/pine.conf\tSystem-wide configuration file."
msgstr "/usr/local/lib/pine.conf\tFichero de configuración global del sistema."

#. type: Plain text
#: man1/pine.1:302
msgid "/usr/local/lib/pine.conf.fixed\t Non-overridable configuration file."
msgstr "/usr/local/lib/pine.conf.fixed\t Fichero de configuración no excluible."

#. type: Plain text
#: man1/pine.1:304
msgid "/tmp/.\\eusr\\espool\\email\\exxxx\tPer-folder mailbox lock files."
msgstr ""
"/tmp/.\\eusr\\espool\\email\\exxxx\tficheros de bloqueo de buzones por "
"carpeta."

#. type: Plain text
#: man1/pine.1:306
msgid "~/.pine-interrupted-mail\tMessage which was interrupted."
msgstr "~/.pine-interrupted-mail\tMensaje que fue interrumpido."

#. type: Plain text
#: man1/pine.1:308
msgid "~/mail/postponed-msgs\tFor postponed messages."
msgstr "~/mail/postponed-msgs\tPara mensajes pospuestos."

#. type: Plain text
#: man1/pine.1:310
msgid "~/mail/sent-mail\tOutgoing message archive (FCC)."
msgstr "~/mail/sent-mail\tArchivo de mensajes salientes."

#. type: Plain text
#: man1/pine.1:312
msgid "~/mail/saved-messages\tDefault destination for Saving messages."
msgstr "~/mail/saved-messages\tDestino por defecto para guardar mensajes."

#. type: SH
#: man1/pine.1:312
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: man1/pine.1:315
msgid ""
"pico(1), binmail(1), aliases(5), mailaddr(7), sendmail(8), spell(1), imapd(8)"
msgstr ""
"pico(1), binmail(1), aliases(5), mailaddr(7), sendmail(8), spell(1), imapd(8)"

#. type: Plain text
#: man1/pine.1:318
msgid "Newsgroup: comp.mail.pine"
msgstr "Newsgroup: comp.mail.pine"

#. type: Plain text
#: man1/pine.1:320
msgid "Pine Information Center: http://www.washington.edu/pine"
msgstr "Centro de Información de Pine: http://www.washington.edu/pine"

#. type: Plain text
#: man1/pine.1:322
msgid "Source distribution: ftp://ftp.cac.washington.edu/pine/pine.tar.Z"
msgstr "Distribución fuente: ftp://ftp.cac.washington.edu/pine/pine.tar.Z"

#. type: Plain text
#: man1/pine.1:324
msgid "Pine Technical Notes, included in the source distribution."
msgstr "Notas Técnicas de Pine, incluidas en la distribución fuente."

#. type: Plain text
#: man1/pine.1:326
msgid "C-Client messaging API library, included in the source distribution."
msgstr ""
"Biblioteca API de mensajería C-Client, incluida en la distribución fuente."

#. type: SH
#: man1/pine.1:326
#, no-wrap
msgid "ACKNOWLEDGMENTS"
msgstr "RECONOCIMIENTOS"

#. type: Plain text
#: man1/pine.1:332
#, no-wrap
msgid ""
"The University of Washington Pine development team (part of the UW Office \n"
"of Computing & Communications) includes:\n"
msgstr ""
"El equipo de desarrollo de Pine de la Universidad de Washington (parte de la\n"
"Oficina de Computación y Comunicaciones de la UW) incluye a:\n"

#. type: Plain text
#: man1/pine.1:346
#, no-wrap
msgid ""
" Project Leader:           Mike Seibel\n"
" Principal authors:        Mike Seibel, Steve Hubert, Laurence Lundblade*\n"
" C-Client library & IMAPd: Mark Crispin\n"
" Pico, the PIne COmposer:  Mike Seibel\n"
" Documentation:            Many people!\n"
" PC-Pine for Windows:      Tom Unger, Mike Seibel\n"
" Project oversight:        Terry Gray, Lori Stevens\n"
" Principal Patrons:        Ron Johnson, Mike Bryant\n"
" Additional support:       NorthWestNet\n"
" Initial Pine code base:   Elm, by Dave Taylor & USENET Community Trust\n"
" Initial Pico code base:   MicroEmacs 3.6, by Dave G. Conroy\n"
" User Interface design:    Inspired by UCLA's \"Ben\" mailer for MVS\n"
" Suggestions/fixes/ports:  Folks from all over!\n"
msgstr ""
" Líder de proyecto:                 Mike Seibel\n"
" Autores principales:               Mike Seibel, Steve Hubert, Laurence Lundblade*\n"
" Biblioteca C-Client & IMAPd:       Mark Crispin\n"
" Pico, el PIne COmposer:            Mike Seibel\n"
" Documentación:                     Varias personas!\n"
" PC-Pine para Windows:              Tom Unger, Mike Seibel\n"
" Supervisión de proyecto:           Terry Gray, Lori Stevens\n"
" Jefes Principales:                 Ron Johnson, Mike Bryant\n"
" Soporte adicional:                 NorthWestNet\n"
" Código base inicial de Pine:       Elm, por Dave Taylor & USENET Community Trust\n"
" Código base inicial de Pico:       MicroEmacs 3.6, por Dave G. Conroy\n"
" Diseño de Interfaz de Usuario:     Inspirado en el UCLA's \"Ben\" mailer para MVS\n"
" Sugerencias/correcciones/porteos:  Gente de todos lados!\n"

#. type: Plain text
#: man1/pine.1:348
#, no-wrap
msgid "   *Emeritus\n"
msgstr "   *Emeritus\n"

#. type: Plain text
#: man1/pine.1:351
#, no-wrap
msgid ""
"Copyright 1989-1999 by the University of Washington.\n"
"Pine and Pico are trademarks of the University of Washington.\n"
msgstr ""
"Copyright 1989-1999 por la Universidad de Washington.\n"
"Pine y Pico son marcas registradas de la Universidad de Washington.\n"

#. type: Plain text
#: man1/pine.1:352
#, no-wrap
msgid "$Date: 1999/02/04 18:41:58 $\n"
msgstr "$Date: 1999/02/04 18:41:58 $\n"
