# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2024-03-01 17:03+0100\n"
"PO-Revision-Date: 2021-09-03 21:25+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nptl"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "nptl - Native POSIX Threads Library"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"NPTL (Native POSIX Threads Library)  is the GNU C library POSIX threads "
"implementation that is used on modern Linux systems."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NPTL and signals"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"NPTL makes internal use of the first two real-time signals (signal numbers "
"32 and 33).  One of these signals is used to support thread cancelation and "
"POSIX timers (see B<timer_create>(2)); the other is used as part of a "
"mechanism that ensures all threads in a process always have the same UIDs "
"and GIDs, as required by POSIX.  These signals cannot be used in "
"applications."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To prevent accidental use of these signals in applications, which might "
"interfere with the operation of the NPTL implementation, various glibc "
"library functions and system call wrapper functions attempt to hide these "
"signals from applications, as follows:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<SIGRTMIN> is defined with the value 34 (rather than 32)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigwaitinfo>(2), B<sigtimedwait>(2), and B<sigwait>(3)  interfaces "
"silently ignore requests to wait for these two signals if they are specified "
"in the signal set argument of these calls."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigprocmask>(2)  and B<pthread_sigmask>(3)  interfaces silently ignore "
"attempts to block these two signals."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sigaction>(2), B<pthread_kill>(3), and B<pthread_sigqueue>(3)  "
"interfaces fail with the error B<EINVAL> (indicating an invalid signal "
"number) if these signals are specified."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sigfillset>(3)  does not include these two signals when it creates a full "
"signal set."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NPTL and process credential changes"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At the Linux kernel level, credentials (user and group IDs) are a per-thread "
"attribute.  However, POSIX requires that all of the POSIX threads in a "
"process have the same credentials.  To accommodate this requirement, the "
"NPTL implementation wraps all of the system calls that change process "
"credentials with functions that, in addition to invoking the underlying "
"system call, arrange for all other threads in the process to also change "
"their credentials."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The implementation of each of these system calls involves the use of a real-"
"time signal that is sent (using B<tgkill>(2))  to each of the other threads "
"that must change its credentials.  Before sending these signals, the thread "
"that is changing credentials saves the new credential(s) and records the "
"system call being employed in a global buffer.  A signal handler in the "
"receiving thread(s) fetches this information and then uses the same system "
"call to change its credentials."
msgstr ""

#.  FIXME .
#.  Maybe say something about vfork() not being serialized wrt set*id() APIs?
#.  https://sourceware.org/bugzilla/show_bug.cgi?id=14749
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Wrapper functions employing this technique are provided for B<setgid>(2), "
"B<setuid>(2), B<setegid>(2), B<seteuid>(2), B<setregid>(2), B<setreuid>(2), "
"B<setresgid>(2), B<setresuid>(2), and B<setgroups>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For details of the conformance of NPTL to the POSIX standard, see "
"B<pthreads>(7)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#.  See POSIX.1-2008 specification of pthread_mutexattr_init()
#.  See sysdeps/x86/bits/pthreadtypes.h
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX says that any thread in any process with access to the memory "
"containing a process-shared (B<PTHREAD_PROCESS_SHARED>)  mutex can operate "
"on that mutex.  However, on 64-bit x86 systems, the mutex definition for "
"x86-64 is incompatible with the mutex definition for i386, meaning that 32-"
"bit and 64-bit binaries can't share mutexes on x86-64 systems."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<credentials>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
