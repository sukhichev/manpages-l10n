# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Fidel García <fidelgq@dinamic.net>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-15 18:04+0100\n"
"PO-Revision-Date: 2021-06-07 16:25+0200\n"
"Last-Translator: Fidel García <fidelgq@dinamic.net>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MEV"
msgstr "MEV"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "February 1995"
msgstr "Febrero 1995"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mev - a program to report mouse events"
msgstr "mev - un programa para informar de los eventos del ratón"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<mev> [ I<options> ]"
msgstr "B<mev> [ I<opciones> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The `mev' program is part of the gpm package.  The information below is "
"extracted from the texinfo file, which is the preferred source of "
"information."
msgstr ""
"El programa ``mev'' es parte del paquete gpm. La información de más abajo "
"está extraída del fichero texinfo, la cual es la fuente de información "
"preferida."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The mev program is modeled after xev. It prints to stdout the mouse console "
"events it gets."
msgstr ""
"El programa `mev' se ha realizado tomando como modelo `xev'. Imprime en la "
"salida estándar (stdout) de la consola los eventos que recibe, tanto del "
"telclado como del ratón."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"mev's default behaviour is to get anything, but command line switches can be "
"used to set the various fields in the Gpm_Connect structure, in order to "
"customize the program's behaviour. I'm using mev to handle mouse events to "
"Emacs."
msgstr ""
"El comportamiento por defecto de `mev' es capturar cualquier cosa, pero se "
"pueden usar las opciones de la línea de órdenes para establecer diferentes "
"campos en la estructura `Gpm_Connect', para personalizar el comportamiento "
"del programa.  Estoy usando `mev' para manejar los eventos del ratón en "
"emacs."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Command line switches for mev are the following:"
msgstr "Las opciones de la línea de órdenes para `mev' son las siguientes:"

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-C B<number>"
msgstr "-C B<número>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Select a virtual console to get events from.  This is intended to be used "
"for debugging."
msgstr ""
"Selecciona una consola virtual desde la que recibir eventos.  Esto está "
"pensado para ser usado para depuración."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-d B<number>"
msgstr "-d B<número>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Choose a default mask. By default the server gets any events not belonging "
"to the event mask. The mask can be provided either as a decimal number, or "
"as a symbolic string."
msgstr ""
"Selecciona una máscara por defecto. Por defecto el servidor recibe cualquier "
"evento no perteneciente a la máscara de eventos. La máscara puede ser "
"suministrada bien como un número decimal o una cadena simbólica."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-e B<number>"
msgstr "-e B<número>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Choose the event mask. By default any event is received. The mask can be "
"provided either as a decimal number, or as a symbolic string."
msgstr ""
"Selecciona la máscara de eventos. Por defecto cualquier evento es recibido. "
"La máscara puede ser suministrada bien como un número decimal o una cadena "
"simbólica."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-E"
msgstr "-E"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Enter emacs mode. In emacs mode events are reported as lisp forms rather "
"than numbers. This is the format used by the t-mouse package within emacs."
msgstr ""
"Pasa a modo emacs. En modo emacs los eventos son informados en formato lisp "
"en lugar de números. Este es el formato usado por el paquete t-mouse dentro "
"de emacs."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-f"
msgstr "-f"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
msgid ""
"Fit events inside the screen before reporting them. This options re-fits "
"drag events, which are allowed to exit the screen border,"
msgstr ""
"Adapta eventos dentro de la pantalla antes de informar de ellos.  Estas "
"opciones readaptan los eventos de arrastre, los cuales están autorizados "
"para salir del borde de la pantalla, márgenes."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-i"
msgstr "-i"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Interactive. Accepts input from stdin to change connection parameters."
msgstr ""
"Modo interactivo. Acepta entradas desde la entrada estándar para cambiar "
"parámetros de conexión."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-m B<number>"
msgstr "-m B<número>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Choose the minimum modifier mask. Any event with fewer modifiers will not be "
"reported to mev. It defaults to 0.  The mask must be provided either as a "
"decimal number, or as a symbolic string."
msgstr ""
"Selecciona la máscara de modificadores mínimos. Cualquier evento menos "
"modificadores de los indicados no serán informado a mev. Por defecto es 0. "
"La máscara debe ser suministrada bien como un número decimal o una cadena "
"simbólica."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "-M B<number>"
msgstr "-M B<número>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Choose the maximum modifier mask. Any event with more modifier than "
"specified will not be reported to mev.  It defaults to \\~~0, i.e. all "
"events are received.  The mask must be provided either as a decimal number, "
"or as a symbolic string."
msgstr ""
"Selecciona la máscara de modificadores máximos. Cualquier evento con más "
"modificadores que los indicados no será informado a mev.  Por defecto es 0, "
"es decir, todos los eventos serán recibidos.  La máscara debe ser "
"suministrada bien como un número decimal o una cadena simbólica."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "-p"
msgstr "-p"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Requests to draw the pointer during drags. This option is used by emacs to "
"avoid invoking ioctl() from lisp code."
msgstr ""
"Solicita dibujar el puntero mientras se arrastra. Esta opción es usada por "
"emacs para evitar invocar a ioctl() desde el código lisp."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"When the arguments are not decimal integers, they are considered lists of "
"alphanumeric characters, separated by a single non-alphanumeric character. I "
"use the comma (,), but any will do."
msgstr ""
"Cuando los argumento no son decimales enteros, son considerados listas de "
"caracteres alfanuméricos, separados por un único carácter no alfanumérico. "
"Yo uso la coma ``,'', pero cualquier otro funcionará."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Allowed names for events are move, drag, down or press, up or release, "
"motion (which is both move and drag), and hard."
msgstr ""
"Los nombres permitidos para eventos son `move', `drag', `down' o `press', "
"`up' o `release', `motion' (el cual es `move' y `drag') y `hard'."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Allowed names for modifiers are shift, leftAlt, rightAlt, anyAlt (one or the "
"other), control."
msgstr ""
"Los nombres permitidos para modificadores son `shift', `leftAlt', "
"`rightAlt', `anyAlt' (uno o el otro), `control'."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"When the -i switch is specified, mev looks at its standard input as command "
"lines rather than events. The input lines are parsed, and the commands push "
"and pop are recognized."
msgstr ""
"Cuando se especificada la opción `-i', `mev' mira a su entrada estándar como "
"una línea de órdenes en lugar de eventos. Las líneas de entrada son "
"analizadas, y las órdenes push y pop son reconocidas."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The push command, then, accepts the options -d, -e, -m and -M, with the same "
"meaning described above. Unspecified options retain the previous value and "
"the resulting masks are used to reopen the connection with the server. pop "
"is used to pop the connection stack. If an empty stack is popped the program "
"exits."
msgstr ""
"La orden `push', entonces, acepta las opciones `-d', `-e', `-m' y `-M', con "
"el mismo significado descrito anteriormente. Las opciones no especificadas "
"mantienen los valores previos y las máscaras resultante son usadas para "
"reabrir la conexión con el servidor.  `pop' es usado para desapilar de la "
"pila la conexión. Si se intenta desapilar de una pila vacía, el programa "
"terminará."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"Other commands recognized are info, used to return the stack depth; quit to "
"prematurely terminate the program; and snapshot to get some configuration "
"information from the server."
msgstr ""
"Otras órdenes reconocidos son `info', usada para devolver el tamaño de la "
"pila, y quit para terminar prematuramente el programa"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Beginning with release 1.16, B<mev> no longer works under xterm.  Please use "
"the B<rmev> program (provided in the B<sample> directory) to watch gpm "
"events under xterm or rxvt.  B<rmev> also displays keyboard events besides "
"mouse events."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Alessandro Rubini E<lt>rubini@linux.itE<gt>"
msgstr "Alessandro Rubini E<lt>rubini@linux.itE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Ian Zimmerman E<lt>itz@speakeasy.orgE<gt>"
msgstr "Ian Zimmerman E<lt>itz@speakeasy.orgE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "/dev/gpmctl The socket used to connect to gpm.\n"
msgstr "/dev/gpmctl En enchufe (socket) usado para conectarse a gpm.\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
" B<gpm(8) >      The mouse server\n"
" B<gpm-root(1) > An handler for Control-Mouse events.\n"
msgstr ""
" B<gpm(8) >      El servidor del ratón\n"
" B<gpm-root(1) > Un manejador de eventos para Control-Mouse.\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The info file about `gpm', which gives more complete information and "
"explains how to write a gpm client."
msgstr ""
"El archivo info sobre `gpm', el cual da una completa información y explica "
"como escribir un cliente gpm."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The `mev' program is modeled after `xev'. It prints to `stdout' the mouse "
"console events it gets."
msgstr ""
"El programa `mev' se ha realizado tomando como modelo `xev'. Imprime en la "
"salida estándar (stdout) de la consola los eventos que recibe, tanto del "
"telclado como del ratón."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"`mev''s default behaviour is to get anything, but command line switches can "
"be used to set the various fields in the `Gpm_Connect' structure, in order "
"to customize the program's behaviour. I'm using `mev' to handle mouse events "
"to Emacs."
msgstr ""
"El comportamiento por defecto de `mev' es capturar cualquier cosa, pero se "
"pueden usar las opciones de la línea de órdenes para establecer diferentes "
"campos en la estructura `Gpm_Connect', para personalizar el comportamiento "
"del programa.  Estoy usando `mev' para manejar los eventos del ratón en "
"emacs."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Command line switches for `mev' are the following:"
msgstr "Las opciones de la línea de órdenes para `mev' son las siguientes:"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-C I<number>"
msgstr "-C I<número>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-d I<number>"
msgstr "-d I<número>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-e I<number>"
msgstr "-e I<número>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Interactive. Accepts input from `stdin' to change connection parameters."
msgstr ""
"Modo interactivo. Acepta entradas desde la entrada estándar para cambiar "
"parámetros de conexión."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-m I<number>"
msgstr "-m I<número>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Choose the minimum modifier mask. Any event with fewer modifiers will not be "
"reported to `mev'. It defaults to `0'.  The mask must be provided either as "
"a decimal number, or as a symbolic string."
msgstr ""
"Selecciona la máscara de modificadores mínimos. Cualquier evento menos "
"modificadores de los indicados no serán informado a mev. Por defecto es 0. "
"La máscara debe ser suministrada bien como un número decimal o una cadena "
"simbólica."

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "-M I<number>"
msgstr "-M I<número>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Choose the maximum modifier mask. Any event with more modifier than "
"specified will not be reported to `mev'.  It defaults to `\\~~0', i.e. all "
"events are received.  The mask must be provided either as a decimal number, "
"or as a symbolic string."
msgstr ""
"Selecciona la máscara de modificadores máximos. Cualquier evento con más "
"modificadores que los indicados no será informado a mev.  Por defecto es 0, "
"es decir, todos los eventos serán recibidos.  La máscara debe ser "
"suministrada bien como un número decimal o una cadena simbólica."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Requests to draw the pointer during drags. This option is used by emacs to "
"avoid invoking `ioctl()' from lisp code."
msgstr ""
"Solicita dibujar el puntero mientras se arrastra. Esta opción es usada por "
"emacs para evitar invocar a ioctl() desde el código lisp."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"When the arguments are not decimal integers, they are considered lists of "
"alphanumeric characters, separated by a single non-alphanumeric character. I "
"use the comma (`,'), but any will do."
msgstr ""
"Cuando los argumento no son decimales enteros, son considerados listas de "
"caracteres alfanuméricos, separados por un único carácter no alfanumérico. "
"Yo uso la coma ``,'', pero cualquier otro funcionará."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Allowed names for events are `move', `drag', `down' or `press', `up' or "
"`release', `motion' (which is both `move' and `drag'), and `hard'."
msgstr ""
"Los nombres permitidos para eventos son `move', `drag', `down' o `press', "
"`up' o `release', `motion' (el cual es `move' y `drag') y `hard'."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Allowed names for modifiers are `shift', `leftAlt', `rightAlt', "
"`anyAlt' (one or the other), `control'."
msgstr ""
"Los nombres permitidos para modificadores son `shift', `leftAlt', "
"`rightAlt', `anyAlt' (uno o el otro), `control'."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"When the `-i' switch is specified, `mev' looks at its standard input as "
"command lines rather than events. The input lines are parsed, and the "
"commands `push' and `pop' are recognized."
msgstr ""
"Cuando se especificada la opción `-i', `mev' mira a su entrada estándar como "
"una línea de órdenes en lugar de eventos. Las líneas de entrada son "
"analizadas, y las órdenes push y pop son reconocidas."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The `push' command, then, accepts the options `-d', `-e', `-m' and `-M', "
"with the same meaning described above. Unspecified options retain the "
"previous value and the resulting masks are used to reopen the connection "
"with the server. `pop' is used to pop the connection stack. If an empty "
"stack is popped the program exits."
msgstr ""
"La orden 'push', entonces, acepta las opciones `-d', `-e', `-m' y `-M', con "
"el mismo significado descrito anteriormente. Las opciones no especificadas "
"mantienen los valores previos y las máscaras resultante son usadas para "
"reabrir la conexión con el servidor.  `pop' es usado para desapilar de la "
"pila la conexión. Si se intenta desapilar de una pila vacía, el programa "
"terminará."

#. type: Plain text
#: debian-bookworm debian-unstable
#, fuzzy
msgid ""
"Other commands recognized are `info', used to return the stack depth; `quit' "
"to prematurely terminate the program; and `snapshot' to get some "
"configuration information from the server."
msgstr ""
"Otras órdenes reconocidas son `info', usado para devolver el tamaño de la "
"pila, y quit para terminar prematuramente el programa"

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid " B<gpm(8) >      The mouse server\n"
msgstr " B<gpm(8) >      El servidor del ratón\n"

#. type: Plain text
#: mageia-cauldron
#, no-wrap
msgid ""
" B<gpm(8) >       The mouse server\n"
" B<gpm-types(7) > Description of mouse types supported by gpm.\n"
msgstr ""
" B<gpm(8) >       El servidor del ratón\n"
" B<gpm-types(7) > Un manejador de eventos para Control-Mouse.\n"
