# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Enrique Ferrero Puchades <enferpuc@olemail.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-15 18:01+0100\n"
"PO-Revision-Date: 1999-05-29 19:53+0200\n"
"Last-Translator: Enrique Ferrero Puchades <enferpuc@olemail.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "INCLUDERES"
msgstr "INCLUDERES"

#. type: TH
#: archlinux fedora-40 fedora-rawhide
#, no-wrap
msgid "February 2023"
msgstr "Febrero de 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide
#, fuzzy, no-wrap
#| msgid "includeres 2.04"
msgid "includeres 2.10"
msgstr "includeres 2.04"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "includeres - include resources in a PostScript document"
msgstr "includeres - incluir recursos dentro de un documento PostScript"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<includeres> [I<\\,OPTION\\/>...] [I<\\,INFILE \\/>[I<\\,OUTFILE\\/>]]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Include resources in a PostScript document."
msgstr "Incluir recursos dentro de un documento PostScript."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display this help and exit"
msgstr "muestra la ayuda y finaliza"

#. type: TP
#: archlinux fedora-40 fedora-rawhide
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display version information and exit"
msgstr "mostrar información de versión y finalizar"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Includeres> includes resources (fonts, procsets, patterns, files, etc) "
#| "in place of I<%%IncludeResource> comments in a PostScript document. The "
#| "resources are searched for in the current directory and the system "
#| "default directory under the resource name, and with an appropriate "
#| "extension.  The pipeline"
msgid ""
"B<includeres> includes resources (fonts, procsets, patterns, files, etc.) in "
"place of B<%%IncludeResource> comments in a PostScript document.  The "
"resources are searched for under the resource name, and with an appropriate "
"extension.  The pipeline"
msgstr ""
"I<Includeres> incluye recursos (fuentes, procsets, patrones, ficheros, etc) "
"en el lugar de los comentarios I<%%IncludeResource> en un documento "
"PostScript. Los recursos se buscan en el directorio actual y en el "
"directorio por defecto del sistema bajo el nombre del recurso, y con una "
"extensión apropiada.  La tubería"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "extractres file.ps | includeres E<gt>out.ps"
msgstr "extractres file.ps | includeres E<gt>out.ps"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies.  The output file can then be put through page re-"
"arrangement filters such as B<psnup> or B<pstops> safely."
msgstr ""
"mueve al prólogo todos los recursos que aparecen en el documento, quitando "
"copias redundantes. De esta manera el archivo de salida se puede meter a "
"través de filtros de reordenación de páginas tales como B<psnup> o B<pstops> "
"de una manera segura."

#. type: SS
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Estado de salida:"

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "if OK,"
msgstr "si todo fue bien,"

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if arguments or options are incorrect, or there is some other problem "
"starting up,"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if there is some problem during processing, typically an error reading or "
"writing an input or output file."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Written by Angus J. C. Duggan and Reuben Thomas."
msgstr "Escrito por Angus J. C. Duggan y Reuben Thomas."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<includeres> does not alter the B<%%DocumentNeededResources> comments."
msgstr "B<includeres> no altera los comentarios B<%%DocumentNeededResources>."

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide
#, fuzzy
#| msgid "Copyright \\(co Reuben Thomas 2012-2019."
msgid "Copyright \\(co Reuben Thomas 2012-2022."
msgstr "Copyright \\(co Reuben Thomas 2012-2019."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Copyright \\(co Angus J. C. Duggan 1991-1997."
msgstr "Copyright \\(co Angus J. C. Duggan 1991-1997."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "TRADEMARKS"
msgstr "MARCAS REGISTRADAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> es una marca registrada de Adobe Systems Incorporated."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<psutils>(1), B<paper>(1)"
msgstr "B<psutils>(1), B<paper>(1)"

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "includeres - filter to include resources in a PostScript document"
msgstr ""
"includeres - filtro para incluir recursos dentro de un documento PostScript"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "B<includeres> E<lt> I<document.ps> E<gt> I<output.ps>"
msgstr "B<includeres> E<lt> I<document.ps> E<gt> I<output.ps>"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid ""
"I<Includeres> includes resources (fonts, procsets, patterns, files, etc) in "
"place of I<%%IncludeResource> comments in a PostScript document. The "
"resources are searched for in the current directory and the system default "
"directory under the resource name, and with an appropriate extension.  The "
"pipeline"
msgstr ""
"I<Includeres> incluye recursos (fuentes, procsets, patrones, ficheros, etc) "
"en el lugar de los comentarios I<%%IncludeResource> en un documento "
"PostScript. Los recursos se buscan en el directorio actual y en el "
"directorio por defecto del sistema bajo el nombre del recurso, y con una "
"extensión apropiada.  La tubería"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid ""
"will move all resources appearing in a document to the document prologue, "
"removing redundant copies. The output file can then be put through page re-"
"arrangement filters such as I<psnup> or I<pstops> safely."
msgstr ""
"mueve al prólogo todos los recursos que aparecen en el documento, quitando "
"copias redundantes. De esta manera el archivo de salida se puede meter a "
"través de filtros de reordenación de páginas tales como I<psnup> o I<pstops> "
"de una manera segura."

#. type: SH
#: debian-bookworm debian-unstable opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "/usr/lib/psutils - system resource directory."
msgstr "/usr/lib/psutils - directorio de recurso del sistema."

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Copyright (C) Angus J. C. Duggan 1991-1995"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"B<psbook>(1), B<psselect>(1), B<pstops>(1), B<epsffit>(1), B<psnup>(1), "
"B<psresize>(1), B<psmerge>(1), B<fixscribeps>(1), B<getafm>(1), "
"B<fixdlsrps>(1), B<fixfmps>(1), B<fixpsditps>(1), B<fixpspps>(1), "
"B<fixtpps>(1), B<fixwfwps>(1), B<fixwpps>(1), B<fixwwps>(1), "
"B<extractres>(1), B<includeres>(1), B<showchar>(1)"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6
msgid "I<includeres> does not alter the I<%%DocumentNeededResources> comments."
msgstr "I<includeres> no altera los comentarios I<%%DocumentNeededResources>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "Octubre de 2021"

#. type: TH
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid "includeres 2.04"
msgid "includeres 2.07"
msgstr "includeres 2.04"

#. type: TP
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: mageia-cauldron
#, fuzzy
#| msgid ""
#| "I<Includeres> includes resources (fonts, procsets, patterns, files, etc) "
#| "in place of I<%%IncludeResource> comments in a PostScript document. The "
#| "resources are searched for in the current directory and the system "
#| "default directory under the resource name, and with an appropriate "
#| "extension.  The pipeline"
msgid ""
"B<Includeres> includes resources (fonts, procsets, patterns, files, etc.) in "
"place of B<%%IncludeResource> comments in a PostScript document.  The "
"resources are searched for under the resource name, and with an appropriate "
"extension.  The pipeline"
msgstr ""
"I<Includeres> incluye recursos (fuentes, procsets, patrones, ficheros, etc) "
"en el lugar de los comentarios I<%%IncludeResource> en un documento "
"PostScript. Los recursos se buscan en el directorio actual y en el "
"directorio por defecto del sistema bajo el nombre del recurso, y con una "
"extensión apropiada.  La tubería"

#. type: Plain text
#: mageia-cauldron opensuse-tumbleweed
msgid "Copyright \\(co Reuben Thomas 2012-2019."
msgstr "Copyright \\(co Reuben Thomas 2012-2019."

#. type: Plain text
#: opensuse-leap-15-6
#, fuzzy
#| msgid "/usr/lib/psutils - system resource directory."
msgid "/usr/share/psutils - system resource directory."
msgstr "/usr/lib/psutils - directorio de recurso del sistema."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixmacps(1), fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), "
"fixwpps(1), fixwwps(1), extractres(1), includeres(1)"
msgstr ""
"B<psbook(1)>, B<psselect(1)>, B<pstops(1)>, B<epsffit(1)>, B<psnup(1)>, "
"B<psresize(1)>, B<psmerge(1)>, B<fixscribeps(1)>, B<getafm(1)>, "
"B<fixdlsrps(1)>, B<fixfmps(1)>, B<fixmacps(1)>, B<fixpsditps(1)>, "
"B<fixpspps(1)>, B<fixtpps(1)>, B<fixwfwps(1)>, B<fixwpps(1)>, B<fixwwps(1)>, "
"B<extractres(1)>, B<includeres(1)>"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr "Diciembre de 2021"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "includeres 2.04"
msgid "includeres 2.08"
msgstr "includeres 2.04"
