# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-15 18:10+0100\n"
"PO-Revision-Date: 2023-01-16 00:00+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SETKEYCODES"
msgstr "SETKEYCODES"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "8 Nov 1994"
msgstr "8 de Noviembre de 1994"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setkeycodes - load kernel scancode-to-keycode mapping table entries"
msgstr ""
"setkeycodes - carga las entradas de la tabla del núcleo de códigos de "
"rastreo a códigos de teclas"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<setkeycodes> I<scancode keycode ...>"
msgstr "B<setkeycodes> I<codigo-rastreo código-tecla ...>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<setkeycodes> command reads its arguments two at a time, each pair of "
"arguments consisting of a scancode (given in hexadecimal) and a keycode "
"(given in decimal). For each such pair, it tells the kernel keyboard driver "
"to map the specified scancode to the specified keycode."
msgstr ""
"La orden I<setkeycodes> lee sus argumentos de dos en dos, consistiendo cada "
"par de ellos en un código de rastreo (dado en hexadecimal) y un código de "
"tecla (en base 10). Para cada par, le dice al controlador de teclado del "
"núcleo que asocie el código de rastreo especificado al código de tecla "
"correspondiente."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This command is useful only for people with slightly unusual keyboards, that "
"have a few keys which produce scancodes that the kernel does not recognize."
msgstr ""
"Esta orden es útil solo para gente con teclados ligeramente distintos de lo "
"normal, que tengan unas cuantas teclas que produzcan códigos de rastreo que "
"el núcleo no reconozca."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "THEORY"
msgstr "TEORÍA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The usual PC keyboard produces a series of scancodes for each key press "
#| "and key release. (Scancodes are shown by B<showkey -s>, see showkey(1).)  "
#| "The kernel parses this stream of scancodes, and converts it to a stream "
#| "of keycodes (key press/release events).  (Keycodes are shown by "
#| "B<showkey>.)  Apart from a few scancodes with special meaning, and apart "
#| "from the sequence produced by the Pause key, and apart from shiftstate "
#| "related scancodes, and apart from the key up/down bit, the stream of "
#| "scancodes consists of unescaped scancodes xx (7 bits) and escaped "
#| "scancodes e0 xx (8+7 bits).  It is hardwired in the current kernel that "
#| "in the range 1-88 (0x01-0x58) keycode equals scancode. For the remaining "
#| "scancodes (0x59-0x7f) or scancode pairs (0xe0 0x00 - 0xe0 0x7f) a "
#| "corresponding keycode can be assigned (in the range 1-127).  For example, "
#| "if you have a Macro key that produces e0 6f according to showkey(1), the "
#| "command"
msgid ""
"The usual PC keyboard produces a series of scancodes for each key press and "
"key release. (Scancodes are shown by B<showkey -s>, see B<showkey>(1)  ) The "
"kernel parses this stream of scancodes, and converts it to a stream of "
"keycodes (key press/release events).  (Keycodes are shown by B<showkey>.)  "
"Apart from a few scancodes with special meaning, and apart from the sequence "
"produced by the Pause key, and apart from shiftstate related scancodes, and "
"apart from the key up/down bit, the stream of scancodes consists of "
"unescaped scancodes xx (7 bits) and escaped scancodes e0 xx (8+7 bits).  To "
"these scancodes or scancode pairs, a corresponding keycode can be assigned "
"(in the range 1-127).  For example, if you have a Macro key that produces e0 "
"6f according to B<showkey>(1), the command"
msgstr ""
"El teclado usual de un PC produce una serie de códigos de rastreo para cada "
"pulsación y liberación de tecla. (Los códigos de rastreo se ven con la orden "
"B<showkey -s>, vea showkey(1).)  El núcleo analiza este flujo de códigos de "
"rastreo, y lo convierte en un flujo de códigos de tecla (eventos de "
"pulsación/liberación de tecla).  (Los códigos de tecla se ven con "
"B<showkey>.)  Aparte de unos pocos códigos de rastreo con significado "
"especial, y aparte de la secuencia producida por la tecla Pausa, y aparte de "
"los códigos de rastreo relacionados con estados de cambios, y aparte de lo "
"de las teclas arriba/abajo, el flujo de códigos de rastreo consiste en "
"códigos de rastreo sin escape xx (de 7 bits) y códigos de rastreo con escape "
"e0 xx (8+7 bits).  Está codificado dentro del núcleo actual que en el rango "
"1-88 (0x01-0x58) los códigos de tecla son iguales que los de rastreo. Para "
"los códigos de rastreo restantes (0x59-0x7f) o pares de códigos de rastreo "
"(0xe0 0x00 - 0xe0 0x7f) se puede asignar un código de tecla correspondiente "
"(en el rango 1-127).  Por ejemplo, si uno tiene una tecla Macro que produce "
"e0 6f de acuerdo con showkey(1), la orden"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<setkeycodes e06f 112>"
msgstr "B<setkeycodes e06f 112>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"will assign the keycode 112 to it, and then B<loadkeys>(1)  can be used to "
"define the function of this key."
msgstr ""
"asignará el código de tecla 112 a ella, y entonces puede emplearse "
"B<loadkeys>(1) para definir la función de esta tecla."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some older kernels might hardwire a low scancode range to the equivalent "
"keycodes; setkeycodes will fail when you try to remap these."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2.6 KERNELS"
msgstr "NÚCLEOS 2.6"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In 2.6 kernels key codes lie in the range 1-255, instead of 1-127.  (It "
"might be best to confine oneself to the range 1-239.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In 2.6 kernels raw mode, or scancode mode, is not very raw at all.  The code "
"returned by showkey -s will change after use of setkeycodes.  A kernel bug. "
"See also B<showkey>(1)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr "Ninguno."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The keycodes of X have nothing to do with those of Linux.  Unusual keys can "
"be made visible under Linux, but not under X."
msgstr ""
"Los códigos de teclas de X no tienen nada que ver con los de Linux.  Las "
"teclas inusuales pueden hacerse visible bajo Linux, pero no bajo X."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<dumpkeys>(1), B<loadkeys>(1), B<showkey>(1), B<getkeycodes>(8)"
msgstr "B<dumpkeys>(1), B<loadkeys>(1), B<showkey>(1), B<getkeycodes>(8)"

#. type: Plain text
#: fedora-40 fedora-rawhide
msgid ""
"USB keyboards have standardized keycodes and B<setkeycodes> doesn't affect "
"them at all."
msgstr ""
"Los teclados USB tienen codificaciones de teclas estandarizados, por los que "
"B<setkeycodes> no les afecta en absoluto."

#. type: Plain text
#: fedora-40 fedora-rawhide
msgid ""
"B<setkeycodes> affects only the \"first\" input device that has modifiable "
"scancode-to-keycode mapping.  If there is more than one such device, "
"B<setkeycodes> cannot change the mapping of other devices than the \"first\" "
"one."
msgstr ""
