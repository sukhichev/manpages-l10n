# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:05+0100\n"
"PO-Revision-Date: 1999-04-12 19:53+0200\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "recno"
msgstr ""

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "recno - record number database access method"
msgstr "recno - método de acceso a bases de datos con registros numerados"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>db.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>db.hE<gt>>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"I<Note well>: This page documents interfaces provided up until glibc 2.1.  "
"Since glibc 2.2, glibc no longer provides these interfaces.  Probably, you "
"are looking for the APIs provided by the I<libdb> library instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The routine B<dbopen>(3)  is the library interface to database files.  One "
"of the supported file formats is record number files.  The general "
"description of the database access methods is in B<dbopen>(3), this manual "
"page describes only the recno-specific information."
msgstr ""
"La rutina B<dbopen>(3) es la interfaz de biblioteca para los ficheros de "
"bases de datos. Uno de los formatos de fichero soportados es el de los "
"ficheros con registros numerados (que llamaremos regnum). La descripción "
"general de los métodos de acceso a bases de datos se encuentra en la página "
"de manual de B<dbopen>(3); esta página de manual sólo describe información "
"específica de regnum."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The record number data structure is either variable or fixed-length records "
"stored in a flat-file format, accessed by the logical record number.  The "
"existence of record number five implies the existence of records one through "
"four, and the deletion of record number one causes record number five to be "
"renumbered to record number four, as well as the cursor, if positioned after "
"record number one, to shift down one record."
msgstr ""
"La estructura de datos de registros numerados está formada por registros de "
"longitud o fija o variable almacenados en un formato de fichero plano, "
"accedido por el número lógico de registro.  La existencia del registro "
"número cinco implica la existencia de los registros del 1 al 5, y la "
"eliminación del registro número 1 hace que el registro número 5 sea "
"renumerado al registro número 4, de la misma manera que el cursor se "
"desplazará un registro hacia abajo si se encuentra posicionado después del "
"registro número 1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The recno access-method-specific data structure provided to B<dbopen>(3)  is "
"defined in the I<E<lt>db.hE<gt>> include file as follows:"
msgstr ""
"La estructura de datos específica del método de acceso regnum proporcionada "
"a B<dbopen>(3) se define en el fichero cabecera I<E<lt>db.hE<gt>> como sigue:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"typedef struct {\n"
"    unsigned long flags;\n"
"    unsigned int  cachesize;\n"
"    unsigned int  psize;\n"
"    int           lorder;\n"
"    size_t        reclen;\n"
"    unsigned char bval;\n"
"    char         *bfname;\n"
"} RECNOINFO;\n"
msgstr ""
"typedef struct {\n"
"    unsigned long flags;\n"
"    unsigned int  cachesize;\n"
"    unsigned int  psize;\n"
"    int           lorder;\n"
"    size_t        reclen;\n"
"    unsigned char bval;\n"
"    char         *bfname;\n"
"} RECNOINFO;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "The elements of this structure are defined as follows:"
msgstr "Los elementos de esta estructura se definen de la siguiente manera:"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<flags>"
msgstr "I<flags>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "The flag value is specified by ORing any of the following values:"
msgstr ""
"El valor de las opciones se especifica mediante una operación I<O>-lógica de "
"cualquiera de los siguientes valores:"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<R_FIXEDLEN>"
msgstr "B<R_FIXEDLEN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The records are fixed-length, not byte delimited.  The structure element "
"I<reclen> specifies the length of the record, and the structure element "
"I<bval> is used as the pad character.  Any records, inserted into the "
"database, that are less than I<reclen> bytes long are automatically padded."
msgstr ""
"Los registros son de longitud fija, no delimitados por bytes.  El elemento "
"I<reclen> de la estructura especifica la longitud del registro y el elemento "
"I<bval> de la estructura se usa como carácter de relleno.  Todos los "
"registros, insertados en la base de datos, cuya longitud es más pequeña que "
"I<reclen> bytes, se completan automáticamente con el carácter I<bval>."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<R_NOKEY>"
msgstr "B<R_NOKEY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"In the interface specified by B<dbopen>(3), the sequential record retrieval "
"fills in both the caller's key and data structures.  If the B<R_NOKEY> flag "
"is specified, the I<cursor> routines are not required to fill in the key "
"structure.  This permits applications to retrieve records at the end of "
"files without reading all of the intervening records."
msgstr ""
"En la interfaz especificada por B<dbopen>(3), la recuperación secuencial de "
"registros rellena tanto la clave del invocador como las estructuras de "
"datos.  Si se especifica la opción B<R_NOKEY>, no se necesitarán las rutinas "
"con I<cursor> para rellenar la estructura de la clave.  Esto permite a las "
"aplicaciones recuperar registros al final de los ficheros sin leer todos los "
"registros que intervienen."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<R_SNAPSHOT>"
msgstr "B<R_SNAPSHOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"This flag requires that a snapshot of the file be taken when B<dbopen>(3)  "
"is called, instead of permitting any unmodified records to be read from the "
"original file."
msgstr ""
"Esta opción exige que se tome una instantanea del fichero cuando se llame a "
"B<dbopen>(3), en lugar de permitir que cualquier registro sin modificar sea "
"leído del fichero original."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<cachesize>"
msgstr "I<cachesize>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"A suggested maximum size, in bytes, of the memory cache.  This value is "
"B<only> advisory, and the access method will allocate more memory rather "
"than fail.  If I<cachesize> is 0 (no size is specified), a default cache is "
"used."
msgstr ""
"Tamaño máximo sugerido, en bytes, de la memoria caché.  Este valor B<sólo> "
"es consultivo y el método de acceso reservará más memoria antes que fallar.  "
"Si I<cachesize> es 0 (no se especifica un tamaño) se usa una caché por "
"defecto."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<psize>"
msgstr "I<psize>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The recno access method stores the in-memory copies of its records in a "
"btree.  This value is the size (in bytes) of the pages used for nodes in "
"that tree.  If I<psize> is 0 (no page size is specified), a page size is "
"chosen based on the underlying filesystem I/O block size.  See B<btree>(3)  "
"for more information."
msgstr ""
"El método de acceso regnum almacena las copias residentes en memoria de sus "
"registros en un árbolB (btree).  Este valor es el tamaño (en bytes) de las "
"páginas usadas para nodos del árbol.  Si I<psize> es 0 (no se especifica un "
"tamaño de página) se selecciona un tamaño de página basado en el tamaño de "
"bloque de E/S del sistema de ficheros subyacente.  Véase B<btree>(3)  para "
"más información."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<lorder>"
msgstr "I<lorder>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The byte order for integers in the stored database metadata.  The number "
"should represent the order as an integer; for example, big endian order "
"would be the number 4,321.  If I<lorder> is 0 (no order is specified), the "
"current host order is used."
msgstr ""
"El orden de los bytes para los enteros de los metadatos almacenados en la "
"base de datos. El número debería representar el orden como un entero; por "
"ejemplo, el orden `el byte de mayor peso el último' (orden ascendente)  "
"sería el número 4321.  Si I<lorder> es 0 (no se especifica un orden) se "
"utiliza el orden del anfitrión actual."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<reclen>"
msgstr "I<reclen>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "The length of a fixed-length record."
msgstr "El tamaño de un registro de tamaño fijo."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<bval>"
msgstr "I<bval>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The delimiting byte to be used to mark the end of a record for variable-"
"length records, and the pad character for fixed-length records.  If no value "
"is specified, newlines (\"\\en\") are used to mark the end of variable-"
"length records and fixed-length records are padded with spaces."
msgstr ""
"El byte delimitador a usar para marcar el final de un registro para los "
"registros de tamaño variable, y el carácter de relleno para los registros de "
"tamaño fijo.  Si no se especifica un valor, se utilizan caracteres `nueva "
"línea' (\"\\en\") para marcar el final de los registros de longitud variable "
"y los registros de longitud fija se completan con espacios."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "I<bfname>"
msgstr "I<bfname>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The recno access method stores the in-memory copies of its records in a "
"btree.  If I<bfname> is non-NULL, it specifies the name of the btree file, "
"as if specified as the filename for a B<dbopen>(3)  of a btree file."
msgstr ""
"El método de acceso regnum almacena las copias residentes en memoria de sus "
"registros en un árbolB.  Si I<bfname> no es NULL, especifica el nombre de un "
"fichero árbolB, como si se especificara el nombre de fichero para una "
"llamadada a B<dbopen>(3) de un fichero árbolB."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The data part of the key/data pair used by the I<recno> access method is the "
"same as other access methods.  The key is different.  The I<data> field of "
"the key should be a pointer to a memory location of type I<recno_t>, as "
"defined in the I<E<lt>db.hE<gt>> include file.  This type is normally the "
"largest unsigned integral type available to the implementation.  The I<size> "
"field of the key should be the size of that type."
msgstr ""
"La parte de datos del par clave/datos usado por el método de acceso I<recno> "
"es la misma que la de los otros métodos de acceso.  La clave es diferente.  "
"El campo I<data> de la clave debería ser un puntero a una posición de "
"memoria del tipo I<recno_t>, tal y como se define en el fichero cabecera "
"I<E<lt>db.hE<gt>>.  Normalmente, este tipo es el tipo entero sin signo más "
"grande disponible para la implementación.  El campo I<size> de la clave "
"debería ser el tamaño de ese tipo."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Because there can be no metadata associated with the underlying recno access "
"method files, any changes made to the default values (e.g., fixed record "
"length or byte separator value) must be explicitly specified each time the "
"file is opened."
msgstr ""
"Ya que puede no haber ningún metadato asociado con los ficheros subyacentes "
"del método de acceso regnum, cualquier cambio realizado a los valores por "
"defecto (por ejemplo, a la longitud de los registros de tamaño fijo o al "
"valor del separador de bytes) se debe especificar explícitamente cada vez "
"que se abra el fichero."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"In the interface specified by B<dbopen>(3), using the I<put> interface to "
"create a new record will cause the creation of multiple, empty records if "
"the record number is more than one greater than the largest record currently "
"in the database."
msgstr ""
"En la interfaz especificada por B<dbopen>(3), el uso de la interfaz I<put> "
"para crear un nuevo registro provocará la creación de varios registros "
"vacíos si el número de registro es mayor, en más de uno, que el número del "
"mayor registro actualmente en la base de datos."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"The I<recno> access method routines may fail and set I<errno> for any of the "
"errors specified for the library routine B<dbopen>(3)  or the following:"
msgstr ""
"Las rutinas del método de acceso I<regnum> pueden fallar y asignar a "
"I<errno> cualquiera de los errores especificados para la rutina de "
"biblioteca B<dbopen>(3)  o el siguiente:"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"An attempt was made to add a record to a fixed-length database that was too "
"large to fit."
msgstr ""
"Se ha intentado añadir un registro a una base de datos de registros de "
"tamaño fijo que s demasiado grande para caber."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Only big and little endian byte order is supported."
msgstr ""
"Sólo se soportan los órdenes de bytes ascedente (el byte de mayor peso el "
"último) y descendente (el byte de menor peso el último)."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<btree>(3), B<dbopen>(3), B<hash>(3), B<mpool>(3)"
msgstr "B<btree>(3), B<dbopen>(3), B<hash>(3), B<mpool>(3)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"I<Document Processing in a Relational Database System>, Michael Stonebraker, "
"Heidi Stettner, Joseph Kalash, Antonin Guttman, Nadene Lynn, Memorandum No. "
"UCB/ERL M82/32, May 1982."
msgstr ""
"I<Document Processing in a Relational Database System>, Michael Stonebraker, "
"Heidi Stettner, Joseph Kalash, Antonin Guttman, Nadene Lynn, Memorandum No. "
"UCB/ERL M82/32, May 1982."

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2022-12-04"
msgstr "4 Diciembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
