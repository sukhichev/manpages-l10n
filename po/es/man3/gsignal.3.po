# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:58+0100\n"
"PO-Revision-Date: 2004-08-06 19:53+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "gsignal"
msgstr "gsignal"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 Octubre 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Páginas de manual de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "gsignal, ssignal - software signal facility"
msgstr "gsignal, ssignal - utilidades para el manejo de señales"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<typedef void (*sighandler_t)(int);>\n"
msgstr "B<typedef void (*sighandler_t)(int);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int gsignal(int >I<signum>B<);>\n"
msgid "B<[[deprecated]] int gsignal(int >I<signum>B<);>\n"
msgstr "B<int gsignal(int >I<signum>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<sighandler_t ssignal(int >I<signum>B<, sighandler_t >I<action>B<);>\n"
msgid "B<[[deprecated]] sighandler_t ssignal(int >I<signum>B<, sighandler_t >I<action>B<);>\n"
msgstr "B<sighandler_t ssignal(int >I<signum>B<, sighandler_t >I<action>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<gsignal>(), B<ssignal>():"
msgstr "B<gsignal>(), B<ssignal>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"    A partir de glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    En glibc 2.19 y anteriores:\n"
"        _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Don't use these functions under Linux.  Due to a historical mistake, under "
"Linux these functions are aliases for B<raise>(3)  and B<signal>(2), "
"respectively."
msgstr ""
"No use estas funciones bajo Linux.  Debido a un error histórico, bajo Linux "
"estas funciones son sinónimos para B<raise>(3) y B<signal>(2), "
"respectivamente."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Elsewhere, on SYSV-like systems, these functions implement software "
#| "signalling, entirely independent of the classical signal and kill "
#| "functions. The function B<ssignal()> defines the action to take when the "
#| "software signal with number I<signum> is raised using the function "
#| "B<gsignal()>, and returns the previous such action or SIG_DFL.  The "
#| "function B<gsignal()> does the following: if no action (or the action "
#| "SIG_DFL) was specified for I<signum>, then it does nothing and returns "
#| "0.  If the action SIG_IGN was specified for I<signum>, then it does "
#| "nothing and returns 1.  Otherwise, it resets the action to SIG_DFL and "
#| "calls the action function with parameter I<signum>, and returns the value "
#| "returned by that function.  The range of possible values I<signum> varies "
#| "(often 1-15 or 1-17)."
msgid ""
"Elsewhere, on System V-like systems, these functions implement software "
"signaling, entirely independent of the classical B<signal>(2)  and "
"B<kill>(2)  functions.  The function B<ssignal>()  defines the action to "
"take when the software signal with number I<signum> is raised using the "
"function B<gsignal>(), and returns the previous such action or B<SIG_DFL>.  "
"The function B<gsignal>()  does the following: if no action (or the action "
"B<SIG_DFL>)  was specified for I<signum>, then it does nothing and returns "
"0.  If the action B<SIG_IGN> was specified for I<signum>, then it does "
"nothing and returns 1.  Otherwise, it resets the action to B<SIG_DFL> and "
"calls the action function with argument I<signum>, and returns the value "
"returned by that function.  The range of possible values I<signum> varies "
"(often 1\\[en]15 or 1\\[en]17)."
msgstr ""
"Por otra parte, en sistemas tipo SYSV, estas funciones implementan software "
"de manejo de señales, completamente independiente de las funciones típicas "
"signal y kill. La función B<ssignal()> define la acción que se llevará a "
"cabo cuando la señal software cuyo número es I<signum> sea provocada usando "
"la función B<gsignal()>, y devuelve la acción previa instalada o SIG_DFL.  "
"La función B<gsignal()> hace lo siguiente: si no se especifica ninguna "
"acción (o la acción SIG_DFL)  para I<signum>, no hace nada y devuelve 0.  Si "
"se especifica la acción SIG_IGN para I<signum>, no hace nada y devuelve 1.  "
"En otro caso, restablece la acción al valor SIG_DFL, llama a la función "
"especificada por la acción con el parámetro I<signum>, y devuelve el valor "
"retornado por dicha función.  El rango de posibles valores para I<signum> "
"varía (a menudo entre 1-15 o 1-17)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<gsignal>()"
msgstr "B<gsignal>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ssignal>()"
msgstr "B<ssignal>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "MT-Safe"
msgid "MT-Safe sigintr"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#.  Linux libc and
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "These functions are available under AIX, DG/UX, HP-UX, SCO, Solaris, "
#| "Tru64.  They are called obsolete under most of these systems, and are "
#| "broken under glibc.  Some systems also have B<gsignal_r>()  and "
#| "B<ssignal_r>()."
msgid ""
"AIX, DG/UX, HP-UX, SCO, Solaris, Tru64.  They are called obsolete under most "
"of these systems, and are broken under glibc.  Some systems also have "
"B<gsignal_r>()  and B<ssignal_r>()."
msgstr ""
"Estas funciones están disponibles bajo AIX, DG/UX, HP-UX, SCO, Solaris, "
"Tru64.  Son consideradas obsoletas en la mayoría de ellos, y no funcionan "
"correctamente bajo glibc.  Algunos sistemas también tienen B<gsignal_r>() y "
"B<ssignal_r>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<kill>(2), B<signal>(2), B<raise>(3)"
msgstr "B<kill>(2), B<signal>(2), B<raise>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#.  Linux libc and
#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are available under AIX, DG/UX, HP-UX, SCO, Solaris, Tru64.  "
"They are called obsolete under most of these systems, and are broken under "
"glibc.  Some systems also have B<gsignal_r>()  and B<ssignal_r>()."
msgstr ""
"Estas funciones están disponibles bajo AIX, DG/UX, HP-UX, SCO, Solaris, "
"Tru64.  Son consideradas obsoletas en la mayoría de ellos, y no funcionan "
"correctamente bajo glibc.  Algunos sistemas también tienen B<gsignal_r>() y "
"B<ssignal_r>()."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 ​​Julio 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Páginas de manual de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
