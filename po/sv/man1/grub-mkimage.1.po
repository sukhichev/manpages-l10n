# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-09 16:59+0100\n"
"PO-Revision-Date: 2022-07-22 22:21+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MKIMAGE"
msgstr "GRUB-MKIMAGE"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "december 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-1"
msgstr "GRUB 2:2.12-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-mkimage - make a bootable image of GRUB"
msgstr "grub-mkimage - gör en startbar avbildning av GRUB"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<grub-mkimage> [I<\\,OPTION\\/>...] [I<\\,OPTION\\/>]... [I<\\,MODULES\\/>]"
msgstr ""
"B<grub-mkimage> [I<\\,FLAGGA\\/>...] [I<\\,FLAGGA\\/>]... [I<\\,MODULER\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Make a bootable image of GRUB."
msgstr "Gör en startbar avbildning av GRUB."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--config>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--config>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as an early config"
msgstr "bädda in FIL som en tidig konfiguration"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--compression=>(xz|none|auto)"
msgstr "B<-C>, B<--compression=>(xz|none|auto)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "choose the compression to use for core image"
msgstr "välj komprimering att använda för kärnavbild"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--disable-shim-lock>"
msgstr "B<--disable-shim-lock>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "disable shim_lock verifier"
msgstr "inaktivera shim_lock verifier"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,KAT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"use images and modules under DIR [default=/usr/lib/grub/E<lt>platformE<gt>]"
msgstr ""
"använd avbilder och moduler under KAT [standard=/usr/lib/grub/"
"E<lt>platformE<gt>]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-D>, B<--dtb>=I<\\,FILE\\/>"
msgstr "B<-D>, B<--dtb>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as a device tree (DTB)"
msgstr "bädda in FIL som ett enhetsträd (DTB)"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-k>, B<--pubkey>=I<\\,FILE\\/>"
msgstr "B<-k>, B<--pubkey>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as public key for signature checking"
msgstr "bädda in FIL som öppen nyckel för signaturkontroll"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-m>,                              B<--memdisk>=I<\\,FILE\\/>"
msgstr "B<-m>,                              B<--memdisk>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "embed FILE as a memdisk image"
msgstr "bädda in FIL som en memdisk-avbild"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Implies `-p (memdisk)/boot/grub' and overrides"
msgstr "Implicerar ”-p (memdisk)/boot/grub” och åsidosätter"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "any prefix supplied previously, but the prefix"
msgstr "tidigare prefix, men prefixet själv kan bli åsidosatt"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "itself can be overridden by later options"
msgstr "med senare alternativ."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-n>, B<--note>"
msgstr "B<-n>, B<--note>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "add NOTE segment for CHRP IEEE1275"
msgstr "lägg till NOTE-segment för CHRP IEEE1275"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "output a generated image to FILE [default=stdout]"
msgstr "mata ut en genererad avbild till FIL [standard=stdut]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-O>, B<--format>=I<\\,FORMAT\\/>"
msgstr "B<-O>, B<--format>=I<\\,FORMAT\\/>"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, riscv32-"
"efi, riscv64-efi"
msgstr ""
"Generera en avbildning i FORMAT; tillgängliga format: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, loongarch64-efi, riscv32-"
"efi, riscv64-efi"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-p>, B<--prefix>=I<\\,DIR\\/>"
msgstr "B<-p>, B<--prefix>=I<\\,KATALOG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set prefix directory"
msgstr "sätt prefixkatalog"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--sbat>=I<\\,FILE\\/>"
msgstr "B<-s>, B<--sbat>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "SBAT metadata"
msgstr "SBAT-metadata"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "skriv ut informativa meddelanden."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "visa denna hjälplista"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "ge ett kort användningsmeddelande"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "skriv ut programversion"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriska eller valfria argument till långa flaggor är också "
"obligatoriska eller valfria för motsvarande korta flaggor."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"
msgstr "B<grub-install>(8), B<grub-mkrescue>(1), B<grub-mknetdir>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mkimage> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkimage> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-mkimage> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-mkimage> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-mkimage>"
msgstr "B<info grub-mkimage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "October 2023"
msgstr "oktober 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13+deb12u1"
msgstr "GRUB 2.06-13+deb12u1"

#. type: Plain text
#: debian-bookworm
msgid ""
"generate an image in FORMAT available formats: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"
msgstr ""
"Generera en avbildning i FORMAT; tillgängliga format: i386-coreboot, i386-"
"multiboot, i386-pc, i386-xen_pvh, i386-pc-pxe, i386-pc-eltorito, i386-efi, "
"i386-ieee1275, i386-qemu, x86_64-efi, i386-xen, x86_64-xen, mipsel-yeeloong-"
"flash, mipsel-fuloong2f-flash, mipsel-loongson-elf, powerpc-ieee1275, "
"sparc64-ieee1275-raw, sparc64-ieee1275-cdcore, sparc64-ieee1275-aout, ia64-"
"efi, mips-arc, mipsel-arc, mipsel-qemu_mips-elf, mips-qemu_mips-flash, "
"mipsel-qemu_mips-flash, mips-qemu_mips-elf, arm-uboot, arm-coreboot-"
"vexpress, arm-coreboot-veyron, arm-efi, arm64-efi, riscv32-efi, riscv64-efi"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "January 2024"
msgstr "januari 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-1"
msgstr "GRUB 2.12-1"
