# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-15 18:03+0100\n"
"PO-Revision-Date: 2023-01-10 10:20+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MD5SUM"
msgstr "MD5SUM"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "January 2024"
msgstr "januari 2024"

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "md5sum - compute and check MD5 message digest"
msgstr "md5sum — beräkna och kontrollera meddelandes MD5-kontrollsumma"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<md5sum> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<md5sum> [I<\\,FLAGGA\\/>]... [I<\\,FIL\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print or check MD5 (128-bit) checksums."
msgstr "Skriv eller kontrollera MD5 (128-bitars) kontrollsummor."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr "Utan FIL, eller när FIL är -, läs standard in."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--binary>"
msgstr "B<-b>, B<--binary>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read in binary mode"
msgstr "Läs i binärt läge."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--check>"
msgstr "B<-c>, B<--check>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "read checksums from the FILEs and check them"
msgstr "Läs kontrollsummor från FILerna och kontrollera dem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--tag>"
msgstr "B<--tag>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "create a BSD-style checksum"
msgstr "Skapa en kontrollsumma i BSD-stil."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--text>"
msgstr "B<-t>, B<--text>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read in text mode (default)"
msgstr "Läs i textläge (standard)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"end each output line with NUL, not newline, and disable file name escaping"
msgstr ""
"Avsluta varje utmatad rad med NOLL, inte nyrad, och avaktivera "
"kontrollsekvenser i filnamn."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "The following five options are useful only when verifying checksums:"
msgstr "De fem följande flaggorna är användbara enbart vid verifikation av kontrollsummor:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-missing>"
msgstr "B<--ignore-missing>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't fail or report status for missing files"
msgstr "Misslyckas inte eller rapportera status för saknade filer."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--quiet>"
msgstr "B<--quiet>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't print OK for each successfully verified file"
msgstr "Skriv inte OK för varje verifierad fil."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--status>"
msgstr "B<--status>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "don't output anything, status code shows success"
msgstr "Skriv inte ut något, statuskoden visar resultatet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--strict>"
msgstr "B<--strict>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "exit non-zero for improperly formatted checksum lines"
msgstr "Returnera nollskilt vid felformaterade kontrollsummerader."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--warn>"
msgstr "B<-w>, B<--warn>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "warn about improperly formatted checksum lines"
msgstr "Varna för felaktigt formaterade kontrollsummerader."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "visa denna hjälp och avsluta"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "visa versionsinformation och avsluta"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The sums are computed as described in RFC 1321.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with: checksum, a space, a character indicating input mode ('*' for "
"binary, ' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""
"Summorna beräknas så som beskrivs i RFC 1321. Vid kontroll skall indata vara "
"en tidigare utdata från detta program. Normalläget är att skriva en rad med: "
"kontrollsumma, ett mellanslag, ett tecken som indikerar indataläge (”*” för "
"binärt, ” ” för text eller där binärläge är betydelselöst) och namnet på "
"varje FIL."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note: There is no difference between binary mode and text mode on GNU "
"systems."
msgstr ""
"Obs: det är ingen skillnad mellan binärt läge och textläge på GNU-system."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Do not use the MD5 algorithm for security related purposes.  Instead, use an "
"SHA-2 algorithm, implemented in the programs B<sha224sum>(1), "
"B<sha256sum>(1), B<sha384sum>(1), B<sha512sum>(1), or the BLAKE2 algorithm, "
"implemented in B<b2sum>(1)"
msgstr ""
"Använd inte MD5-algoritmen för säkerhetsrelaterade ändamål. Använd istället "
"en SHA-2-algoritm, implementerad i programmen B<sha224sum>(1), "
"B<sha256sum>(1), B<sha384sum>(1), B<sha512sum>(1) eller BLAKE2-algoritmen, "
"implementerad i B<b2sum>(1)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Written by Ulrich Drepper, Scott Miller, and David Madore."
msgstr "Skrivet av Ulrich Drepper, Scott Miller och David Madore."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU coreutils hjälp på nätet: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportera anmärkningar på översättningen till E<lt>tp-sv@listor.tp-sv."
"seE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<cksum>(1)"
msgstr "B<cksum>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/md5sumE<gt>"
msgstr ""
"Fullständig dokumentation E<lt>https://www.gnu.org/software/coreutils/"
"md5sumE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) md5sum invocation\\(aq"
msgstr ""
"eller tillgängligt lokalt via: info \\(aq(coreutils) md5sum invocation\\(aq"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "april 2022"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid "read MD5 sums from the FILEs and check them"
msgstr "Läs MD5-summor från FILen och kontrollera dem."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The sums are computed as described in RFC 1321.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with checksum, a space, a character indicating input mode ('*' for "
"binary, \\&' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""
"Summorna beräknas så som beskrivs i RFC 1321. Vid kontroll skall indata vara "
"en tidigare utdata från detta program. Normalläget är att skriva en rad med: "
"kontrollsumma, ett mellanslag, ett tecken som indikerar indataläge (”*” för "
"binärt, ” ” för text eller där binärläge är betydelselöst) och namnet på "
"varje FIL."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Do not use the MD5 algorithm for security related purposes.  Instead, use an "
"SHA-2 algorithm, implemented in the programs sha224sum(1), sha256sum(1), "
"sha384sum(1), sha512sum(1), or the BLAKE2 algorithm, implemented in b2sum(1)"
msgstr ""
"Använd inte MD5-algoritmen för säkerhetsrelaterade ändamål. Använd istället "
"en SHA-2-algoritm, implementerad i programmen sha224sum(1), sha256sum(1), "
"sha384sum(1), sha512sum(1) eller BLAKE2-algoritmen, implementerad i b2sum(1)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
