# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Rafał Lewczuk <R.Lewczuk@elka.pw.edu.p>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2003.
# Robert Luberda <robert@debian.org>, 2013.
# Michał Kułach <michal.kulach@gmail.com>, 2013, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-03-01 17:08+0100\n"
"PO-Revision-Date: 2016-09-25 12:59+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "shmget"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "shmget - allocates a System V shared memory segment"
msgstr "shmget - utworzenie segmentu pamięci dzielonej Systemu V"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/shm.hE<gt>>\n"
msgstr "B<#include E<lt>sys/shm.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int shmget(key_t >I<key>B<, size_t >I<size>B<, int >I<shmflg>B<);>"
msgid "B<int shmget(key_t >I<key>B<, size_t >I<size>B<, int >I<shmflg>B<);>\n"
msgstr "B<int shmget(key_t >I<key>B<, size_t >I<size>B<, int >I<shmflg>B<);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<shmget>()  returns the identifier of the System\\ V shared memory "
#| "segment associated with the value of the argument I<key>.  A new shared "
#| "memory segment, with size equal to the value of I<size> rounded up to a "
#| "multiple of B<PAGE_SIZE>, is created if I<key> has the value "
#| "B<IPC_PRIVATE> or I<key> isn't B<IPC_PRIVATE>, no shared memory segment "
#| "corresponding to I<key> exists, and B<IPC_CREAT> is specified in "
#| "I<shmflg>."
msgid ""
"B<shmget>()  returns the identifier of the System\\ V shared memory segment "
"associated with the value of the argument I<key>.  It may be used either to "
"obtain the identifier of a previously created shared memory segment (when "
"I<shmflg> is zero and I<key> does not have the value B<IPC_PRIVATE>), or to "
"create a new set."
msgstr ""
"B<shmget>() zwraca identyfikator segmentu pamięci dzielonej Systemu\\ V, "
"skojarzonego z wartością (kluczem) przekazaną w parametrze I<key>. Nowy "
"segment, o rozmiarze równym wartości parametru I<size> zaokrąglonym w górę "
"do wielokrotności B<PAGE_SIZE>, zostanie utworzony, jeśli parametr I<key> "
"będzie mieć wartość B<IPC_PRIVATE> lub jeśli będzie mieć inną wartość oraz "
"segment skojarzony z I<key> nie istnieje, a w parametrze I<shmflg> zostanie "
"przekazany znacznik B<IPC_CREAT>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<shmget>()  returns the identifier of the System\\ V shared memory "
#| "segment associated with the value of the argument I<key>.  A new shared "
#| "memory segment, with size equal to the value of I<size> rounded up to a "
#| "multiple of B<PAGE_SIZE>, is created if I<key> has the value "
#| "B<IPC_PRIVATE> or I<key> isn't B<IPC_PRIVATE>, no shared memory segment "
#| "corresponding to I<key> exists, and B<IPC_CREAT> is specified in "
#| "I<shmflg>."
msgid ""
"A new shared memory segment, with size equal to the value of I<size> rounded "
"up to a multiple of B<PAGE_SIZE>, is created if I<key> has the value "
"B<IPC_PRIVATE> or I<key> isn't B<IPC_PRIVATE>, no shared memory segment "
"corresponding to I<key> exists, and B<IPC_CREAT> is specified in I<shmflg>."
msgstr ""
"B<shmget>() zwraca identyfikator segmentu pamięci dzielonej Systemu\\ V, "
"skojarzonego z wartością (kluczem) przekazaną w parametrze I<key>. Nowy "
"segment, o rozmiarze równym wartości parametru I<size> zaokrąglonym w górę "
"do wielokrotności B<PAGE_SIZE>, zostanie utworzony, jeśli parametr I<key> "
"będzie mieć wartość B<IPC_PRIVATE> lub jeśli będzie mieć inną wartość oraz "
"segment skojarzony z I<key> nie istnieje, a w parametrze I<shmflg> zostanie "
"przekazany znacznik B<IPC_CREAT>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<shmflg> specifies both B<IPC_CREAT> and B<IPC_EXCL> and a shared memory "
"segment already exists for I<key>, then B<shmget>()  fails with I<errno> set "
"to B<EEXIST>.  (This is analogous to the effect of the combination B<O_CREAT "
"| O_EXCL> for B<open>(2).)"
msgstr ""
"Jeżeli w parametrze I<shmflg> podano zarówno B<IPC_CREAT>, jak i B<IPC_EXCL> "
"oraz już istnieje segment w pamięci dzielonej o kluczu I<key>, to "
"B<shmget>() kończy się błędem, ustawiając I<errno> na wartość B<EEXIST>. "
"(Działa to analogicznie do B<O_CREAT | O_EXCL> w B<open>(2))."

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The value I<shmflg> is composed of:"
msgstr "Wartość I<shmflg> składa się z:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<IPC_CREAT>"
msgstr "B<IPC_CREAT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Create a new segment.  If this flag is not used, then B<shmget>()  will find "
"the segment associated with I<key> and check to see if the user has "
"permission to access the segment."
msgstr ""
"Tworzy nowy segment. Jeśli ten znacznik nie zostanie ustawiony, to "
"B<shmget>() spróbuje znaleźć segment skojarzony z I<key> i sprawdzić, czy "
"użytkownik ma uprawnienia dostępu do segmentu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<IPC_EXCL>"
msgstr "B<IPC_EXCL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This flag is used with B<IPC_CREAT> to ensure that this call creates the "
"segment.  If the segment already exists, the call fails."
msgstr ""
"Ta flaga przekazana łącznie z B<IPC_CREAT> zapewnia, że to wywołanie utworzy "
"segment. Jeśli segment już istnieje, wywołanie zawiedzie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHM_HUGETLB> (since Linux 2.6)"
msgstr "B<SHM_HUGETLB> (od Linuksa 2.6)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Allocate the segment using \"huge pages.\" See the Linux kernel source "
#| "file I<Documentation/vm/hugetlbpage.txt> for further information."
msgid ""
"Allocate the segment using \"huge\" pages.  See the Linux kernel source file "
"I<Documentation/admin-guide/mm/hugetlbpage.rst> for further information."
msgstr ""
"Dołącza segment używając \"wielkich stron\" (huge pages). Dalsze informacje "
"można znaleźć w pliku I<Documentation/vm/hugetlbpage.txt> w źródłach jądra "
"Linux."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<SHM_HUGE_2MB>"
msgstr ""

#. type: TQ
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<SHM_HUGETLB> (since Linux 2.6)"
msgid "B<SHM_HUGE_1GB> (since Linux 3.8)"
msgstr "B<SHM_HUGETLB> (od Linuksa 2.6)"

#.  See https://lwn.net/Articles/533499/
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Used in conjunction with B<SHM_HUGETLB> to select alternative hugetlb "
#| "page sizes (respectively, 2 MB and 1 GB)  on systems that support "
#| "multiple hugetlb page sizes."
msgid ""
"Used in conjunction with B<SHM_HUGETLB> to select alternative hugetlb page "
"sizes (respectively, 2\\ MB and 1\\ GB)  on systems that support multiple "
"hugetlb page sizes."
msgstr ""
"Używany w połączeniu z B<SHM_HUGETLB> do wybrania alternatywnych rozmiarów "
"stron hugetlb (odpowiednio, 2 MB i 1 GB) w systemach obsługujących wiele "
"rozmiarów stron hugetlb."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"More generally, the desired huge page size can be configured by encoding the "
"base-2 logarithm of the desired page size in the six bits at the offset "
"B<SHM_HUGE_SHIFT>.  Thus, the above two constants are defined as:"
msgstr ""
"Ogólniej, pożądany rozmiar dużej strony można skonfigurować kodując go w "
"sześciu bajtach na przesunięciu B<SHM_HUGE_SHIFT> za pomocą logarytmu o "
"podstawie 2. Dlatego, powyższe dwie stałe są zdefiniowane jako:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"#define SHM_HUGE_2MB    (21 E<lt>E<lt> SHM_HUGE_SHIFT)\n"
"#define SHM_HUGE_1GB    (30 E<lt>E<lt> SHM_HUGE_SHIFT)\n"
msgstr ""
"#define SHM_HUGE_2MB    (21 E<lt>E<lt> SHM_HUGE_SHIFT)\n"
"#define SHM_HUGE_1GB    (30 E<lt>E<lt> SHM_HUGE_SHIFT)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For some additional details, see the discussion of the similarly named "
"constants in B<mmap>(2)."
msgstr ""
"Dodatkowe informacje można odnaleźć przy omówieniu podobnie nazwanych "
"stałych w B<mmap>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHM_NORESERVE> (since Linux 2.6.15)"
msgstr "B<SHM_NORESERVE> (od Linuksa 2.6.15)"

#.  As at 2.6.17-rc2, this flag has no effect if SHM_HUGETLB was also
#.  specified.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This flag serves the same purpose as the B<mmap>(2)  B<MAP_NORESERVE> flag.  "
"Do not reserve swap space for this segment.  When swap space is reserved, "
"one has the guarantee that it is possible to modify the segment.  When swap "
"space is not reserved one might get B<SIGSEGV> upon a write if no physical "
"memory is available.  See also the discussion of the file I</proc/sys/vm/"
"overcommit_memory> in B<proc>(5)."
msgstr ""
"Ten znacznik stosuje się w takim samym celu jak znacznik B<MAP_NORESERVE> "
"funkcji B<mmap>(2). Nie rezerwuje przestrzeni wymiany dla tego segmentu. "
"Jeśli przestrzeń wymiany zostanie zarezerwowana, ma się gwarancję, że jest "
"możliwe zmodyfikowanie segmentu. Gdy przestrzeń wymiany nie jest "
"zarezerwowana, można otrzymać sygnał B<SIGSEGV> podczas próby zapisu do "
"segmentu, gdy zabraknie dostępnej fizycznej pamięci. Patrz także opis pliku "
"I</proc/sys/vm/overcommit_memory> w B<proc>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In addition to the above flags, the least significant 9 bits of I<shmflg> "
"specify the permissions granted to the owner, group, and others.  These bits "
"have the same format, and the same meaning, as the I<mode> argument of "
"B<open>(2).  Presently, execute permissions are not used by the system."
msgstr ""
"Oprócz powyższych flag, 9 najmniej znaczących bitów I<sgmflg> określa prawa "
"dostępu do segmentu dla jego właściciela, grupy oraz innych. Bity są w takim "
"samym formacie i mają takie samo znaczenie, jak parametr I<mode> wywołania "
"B<open>(2). Prawa uruchamiania nie są obecnie używane przez system."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When a new shared memory segment is created, its contents are initialized to "
"zero values, and its associated data structure, I<shmid_ds> (see "
"B<shmctl>(2)), is initialized as follows:"
msgstr ""
"Jeżeli tworzona jest nowa kolejka komunikatów, wywołanie to w następujący "
"sposób inicjuje strukturę danych I<msqid_ds> (patrz B<msgctl>(2)):"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<shm_perm.cuid> and I<shm_perm.uid> are set to the effective user ID of the "
"calling process."
msgstr ""
"I<shm_perm.cuid> i I<shm_perm.uid> przyjmują wartość efektywnego "
"identyfikatora właściciela procesu wywołującego."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<shm_perm.cgid> and I<shm_perm.gid> are set to the effective group ID of "
"the calling process."
msgstr ""
"I<shm_perm.cgid> i I<shm_perm.gid> przyjmują wartość efektywnego "
"identyfikatora grupy procesu wywołującego."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The least significant 9 bits of I<shm_perm.mode> are set to the least "
"significant 9 bit of I<shmflg>."
msgstr ""
"9 najmniej znaczących bitów pola I<shm_perm.mode> jest kopiowanych z 9 "
"najmniej znaczących bitów I<shmflg>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<shm_segsz> is set to the value of I<size>."
msgstr "I<shm_segsz> jest ustawiane na wartość parametru I<size>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<shm_lpid>, I<shm_nattch>, I<shm_atime>, and I<shm_dtime> are set to 0."
msgstr ""
"I<shm_lpid>, I<shm_nattch>, I<shm_atime> i I<shm_dtime> są ustawiane na 0."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<shm_ctime> is set to the current time."
msgstr "I<shm_ctime> jest ustawiane na bieżący czas."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the shared memory segment already exists, the permissions are verified, "
"and a check is made to see if it is marked for destruction."
msgstr ""
"Jeśli dany segment pamięci dzielonej już istnieje, to są weryfikowane "
"uprawnienia i jest sprawdzane, czy segment nie jest przeznaczony do "
"usunięcia."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, a valid shared memory identifier is returned.  On error, -1 is "
"returned, and I<errno> is set to indicate the error."
msgstr ""
"W przypadku powodzenia zwracany jest poprawny identyfikator pamięci "
"współdzielonej. W razie wystąpienia błędu zwracane jest -1 i ustawiana jest "
"I<errno> wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The user does not have permission to access the shared memory segment, "
#| "and does not have the B<CAP_IPC_OWNER> capability."
msgid ""
"The user does not have permission to access the shared memory segment, and "
"does not have the B<CAP_IPC_OWNER> capability in the user namespace that "
"governs its IPC namespace."
msgstr ""
"Użytkownik nie ma praw dostępu do żądanego segmentu pamięci dzielonej oraz "
"nie ma ustawionego atrybutu  B<CAP_IPC_OWNER>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<IPC_CREAT> and B<IPC_EXCL> were specified in I<shmflg>, but a shared "
"memory segment already exists for I<key>."
msgstr ""
"B<IPC_CREAT> i B<IPC_EXCL> były określone w I<shmflg>, lecz segment pamięci "
"dzielonej już istnieje dla I<key>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A new segment was to be created and I<size> is less than B<SHMMIN> or "
"greater than B<SHMMAX>."
msgstr ""
"Miał być utworzony nowy segment, a wartość I<size> jest mniejsza niż "
"B<SHMMIN> lub większa niż B<SHMMAX>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A segment for the given I<key> exists, but I<size> is greater than the size "
"of that segment."
msgstr ""
"Segment dla podanego I<key> istnieje, lecz I<size> jest większy niż rozmiar "
"tego segmentu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#.  [2.6.7] shmem_zero_setup()-->shmem_file_setup()-->get_empty_filp()
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"Zostało osiągnięte systemowe ograniczenie na całkowitą liczbę otwartych "
"plików."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"No segment exists for the given I<key>, and B<IPC_CREAT> was not specified."
msgstr ""
"Segment o zadanej wartości I<key> nie istnieje i nie ustawiono znacznika "
"B<IPC_CREAT>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "No memory could be allocated for segment overhead."
msgstr "Nie udało się przydzielić pamięci dla segmentu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All possible shared memory IDs have been taken (B<SHMMNI>), or allocating a "
"segment of the requested I<size> would cause the system to exceed the system-"
"wide limit on shared memory (B<SHMALL>)."
msgstr ""
"Wszystkie możliwe identyfikatory pamięci dzielonej zostały wykorzystane "
"(B<SHMMNI>) lub przydzielenie segmentu o żądanym rozmiarze I<size> "
"spowodowałoby przekroczenie systemowego ograniczenia na wielkość pamięci "
"dzielonej (B<SHMALL>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<SHM_HUGETLB> flag was specified, but the caller was not privileged "
#| "(did not have the B<CAP_IPC_LOCK> capability)."
msgid ""
"The B<SHM_HUGETLB> flag was specified, but the caller was not privileged "
"(did not have the B<CAP_IPC_LOCK> capability)  and is not a member of the "
"I<sysctl_hugetlb_shm_group> group; see the description of I</proc/sys/vm/"
"sysctl_hugetlb_shm_group> in B<proc>(5)."
msgstr ""
"Podano znacznik B<SHM_HUGETLB>, ale proces wywołujący nie był "
"uprzywilejowany (nie miał ustawionego atrybutu B<CAP_IPC_LOCK>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<SHM_HUGETLB> and B<SHM_NORESERVE> are Linux extensions."
msgstr "B<SHM_HUGETLB> i B<SHM_NORESERVE> są linuksowymi rozszerzeniami."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#.  SVr4 documents an additional error condition EEXIST.
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4."
msgstr "POSIX.1-2001, SVr4."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<IPC_PRIVATE> isn't a flag field but a I<key_t> type.  If this special "
"value is used for I<key>, the system call ignores all but the least "
"significant 9 bits of I<shmflg> and creates a new shared memory segment."
msgstr ""
"B<IPC_PRIVATE> nie jest znacznikiem, ale szczególną wartością typu I<key_t>. "
"Jeśli wartość ta zostanie użyta jako parametr I<key>, to system uwzględni "
"jedynie 9 najniższych bitów parametru I<shmflg> i utworzy nowy segment "
"pamięci dzielonej."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Shared memory limits"
msgstr "Limity pamięci dzielonej"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following limits on shared memory segment resources affect the "
"B<shmget>()  call:"
msgstr ""
"Następujące ograniczenia odnoszące się do zasobów pamięci dzielonej dotyczą "
"wywołania B<shmget>():"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHMALL>"
msgstr "B<SHMALL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"System-wide limit on the total amount of shared memory, measured in units of "
"the system page size."
msgstr ""
"Systemowy limit całkowitej wielkości pamięci dzielonej, mierzony w "
"jednostkach systemowego rozmiaru strony."

#.  commit 060028bac94bf60a65415d1d55a359c3a17d5c31
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, this limit can be read and modified via I</proc/sys/kernel/"
"shmall>.  Since Linux 3.16, the default value for this limit is:"
msgstr ""
"W Linuksie to ograniczenie można odczytać i zmienić, używając pliku I</proc/"
"sys/kernel/shmall>. Od Linuksa 3.16 domyślną wartością tego limitu jest:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "    ULONG_MAX - 2^24\n"
msgid "ULONG_MAX - 2\\[ha]24\n"
msgstr "    ULONG_MAX - 2^24\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The effect of this value (which is suitable for both 32-bit and 64-bit "
"systems)  is to impose no limitation on allocations.  This value, rather "
"than B<ULONG_MAX>, was chosen as the default to prevent some cases where "
"historical applications simply raised the existing limit without first "
"checking its current value.  Such applications would cause the value to "
"overflow if the limit was set at B<ULONG_MAX>."
msgstr ""
"Ta wartość (odpowiednia dla systemów 32 i 64-bitowych) skutkuje brakiem "
"limitów dla alokacji. Ta wartość, zamiast B<ULONG_MAX>, została wybrana jako "
"domyślna, aby zapobiec przypadkom gdy pewne stare aplikacje zwiększały "
"istniejący limit bez sprawdzania jego wartości bieżącej. Mogło to "
"doprowadzić do przepełnienia wartości, jeśli limit był ustawiony na "
"B<ULONG_MAX>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "From Linux 2.4 up to Linux 3.15, the default value for this limit was:"
msgstr "Od Linuksa 2.4 do 3.15 domyślną wartością limitu było:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "    SHMMAX / PAGE_SIZE * (SHMMNI / 16)\n"
msgid "SHMMAX / PAGE_SIZE * (SHMMNI / 16)\n"
msgstr "    SHMMAX / PAGE_SIZE * (SHMMNI / 16)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If B<SHMMAX> and B<SHMMNI> were not modified, then multiplying the result "
#| "of this formula by the page size (to get a value in bytes) yielded a "
#| "value of 8 GB as the limit on the total memory used by all shared memory "
#| "segments."
msgid ""
"If B<SHMMAX> and B<SHMMNI> were not modified, then multiplying the result of "
"this formula by the page size (to get a value in bytes) yielded a value of "
"8\\ GB as the limit on the total memory used by all shared memory segments."
msgstr ""
"Jeśli nie zmodyfikowano B<SHMMAX> i B<SHMMNI>, to przemnożenie wyniku tego "
"działania przez rozmiar strony (aby otrzymać wartość w bajtach) dawało "
"wartość 8 GB jako limit całkowitej pamięci używanej przez wszystkie segmenty "
"pamięci dzielonej."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHMMAX>"
msgstr "B<SHMMAX>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Maximum size in bytes for a shared memory segment."
msgstr "Maksymalny rozmiar segmentu pamięci dzielonej w bajtach."

#.  commit 060028bac94bf60a65415d1d55a359c3a17d5c31
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, this limit can be read and modified via I</proc/sys/kernel/"
"shmmax>.  Since Linux 3.16, the default value for this limit is:"
msgstr ""
"W Linuksie to ograniczenie można odczytać i zmienić, używając pliku I</proc/"
"sys/kernel/shmmax>. Od Linuksa 3.16 domyślną wartością tego limitu jest:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The effect of this value (which is suitable for both 32-bit and 64-bit "
"systems)  is to impose no limitation on allocations.  See the description of "
"B<SHMALL> for a discussion of why this default value (rather than "
"B<ULONG_MAX>)  is used."
msgstr ""
"Ta wartość (odpowiednia dla systemów 32 i 64-bitowych) skutkuje brakiem "
"limitów dla alokacji. Wyjaśnienie dlaczego jest to wartość domyślna (zamiast "
"B<ULONG_MAX>) znajduje się w opisie B<SHMALL>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "From Linux 2.2 up to Linux 3.15, the default value of this limit was "
#| "0x2000000 (32MB)."
msgid ""
"From Linux 2.2 up to Linux 3.15, the default value of this limit was "
"0x2000000 (32\\ MiB)."
msgstr ""
"Od Linuksa 2.2 do 3.15 domyślną wartością tego limitu było 0x2000000 (32 MB)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Because it is not possible to map just part of a shared memory segment, "
#| "the amount of virtual memory places another limit on the maximum size of "
#| "a usable segment: for example, on i386 the largest segments that can be "
#| "mapped have a size of around 2.8 GB, and on x86_64 the limit is around "
#| "127 TB."
msgid ""
"Because it is not possible to map just part of a shared memory segment, the "
"amount of virtual memory places another limit on the maximum size of a "
"usable segment: for example, on i386 the largest segments that can be mapped "
"have a size of around 2.8\\ GB, and on x86-64 the limit is around 127 TB."
msgstr ""
"Ponieważ nie da się przypisać jedynie fragmentu segmentu pamięci dzielonej, "
"wartość pamięci wirtualnej nakłada kolejne ograniczenie na maksymalny "
"rozmiar użytecznego segmentu np. na architekturze i386 największy segment "
"który można zmapować ma rozmiar około 2,8 GB, a na x86_64 limit ten wynosi "
"około 127 TB."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHMMIN>"
msgstr "B<SHMMIN>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Minimum size in bytes for a shared memory segment: implementation dependent "
"(currently 1 byte, though B<PAGE_SIZE> is the effective minimum size)."
msgstr ""
"Minimalny rozmiar (w bajtach) pojedynczego segmentu pamięci dzielonej: "
"zależny od implementacji (obecnie 1 bajt, ale efektywny minimalny rozmiar "
"wynosi B<PAGE_SIZE>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHMMNI>"
msgstr "B<SHMMNI>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"System-wide limit on the number of shared memory segments.  In Linux 2.2, "
"the default value for this limit was 128; since Linux 2.4, the default value "
"is 4096."
msgstr ""
"Systemowy limit liczby segmentów pamięci dzielonej. W Linuksie 2.2 domyślna "
"wartość tego limitu wynosiła 128; od Linuksa 2.4 domyślna wartość wynosi "
"4096."

#.  Kernels between Linux 2.4.x and Linux 2.6.8 had an off-by-one error
#.  that meant that we could create one more segment than SHMMNI -- MTK
#.  This /proc file is not available in Linux 2.2 and earlier -- MTK
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, this limit can be read and modified via I</proc/sys/kernel/shmmni>."
msgstr ""
"W Linuksie to ograniczenie można odczytać i zmienić, używając pliku I</proc/"
"sys/kernel/shmmni>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The implementation has no specific limits for the per-process maximum number "
"of shared memory segments (B<SHMSEG>)."
msgstr ""
"System Linux nie stawia ograniczeń dotyczących liczby segmentów pamięci "
"dzielonej dołączonych do jednego procesu (B<SHMSEG>)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux notes"
msgstr "Uwagi linuksowe"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Until version 2.3.30, Linux would return B<EIDRM> for a B<shmget>()  on a "
#| "shared memory segment scheduled for deletion."
msgid ""
"Until Linux 2.3.30, Linux would return B<EIDRM> for a B<shmget>()  on a "
"shared memory segment scheduled for deletion."
msgstr ""
"Do wersji 2.3.30 Linux zwracał B<EIDRM> dla B<shmget>() na segmencie pamięci "
"dzielonej przeznaczonym do usunięcia."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "USTERKI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The name choice B<IPC_PRIVATE> was perhaps unfortunate, B<IPC_NEW> would "
"more clearly show its function."
msgstr ""
"Nazwa B<IPC_PRIVATE> prawdopodobnie nie jest najszczęśliwsza. B<IPC_NEW> w "
"sposób bardziej przejrzysty odzwierciedlałoby rolę tej wartości."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<shmop>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<memfd_create>(2), B<shmat>(2), B<shmctl>(2), B<shmdt>(2), B<ftok>(3), "
#| "B<capabilities>(7), B<shm_overview>(7), B<svipc>(7)"
msgid ""
"B<memfd_create>(2), B<shmat>(2), B<shmctl>(2), B<shmdt>(2), B<ftok>(3), "
"B<capabilities>(7), B<shm_overview>(7), B<sysvipc>(7)"
msgstr ""
"B<memfd_create>(2), B<shmat>(2), B<shmctl>(2), B<shmdt>(2), B<ftok>(3), "
"B<capabilities>(7), B<shm_overview>(7), B<svipc>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-10"
msgstr "10 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<SHM_HUGE_2MB>, B<SHM_HUGE_1GB> (since Linux 3.8)"
msgstr "B<SHM_HUGE_2MB>, B<SHM_HUGE_1GB> (od Linuksa 3.8)"

#.  SVr4 documents an additional error condition EEXIST.
#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, SVr4."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4."

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
