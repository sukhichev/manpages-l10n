# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Michał Kułach <michal.kulach@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:54+0100\n"
"PO-Revision-Date: 2024-03-05 23:17+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "delete_module"
msgstr "delete_module"

#. type: TH
#: archlinux fedora-40 fedora-rawhide
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: archlinux fedora-40 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "delete_module - unload a kernel module"
msgstr "delete_module - usuwa (odłącza) moduł jądra"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>>            /* Definition of B<O_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>>            /* Definicja stałych B<O_*> */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definicja stałych B<SYS_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_delete_module, const char *>I<name>B<, unsigned int >I<flags>B<);>\n"
msgstr "B<int syscall(SYS_delete_module, const char *>I<name>B<, unsigned int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<delete_module>(), necessitating the "
"use of B<syscall>(2)."
msgstr ""
"I<Uwaga>: glibc nie udostępnia opakowania dla B<delete_module>(), co wymusza "
"użycie B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<delete_module>()  system call attempts to remove the unused loadable "
"module entry identified by I<name>.  If the module has an I<exit> function, "
"then that function is executed before unloading the module.  The I<flags> "
"argument is used to modify the behavior of the system call, as described "
"below.  This system call requires privilege."
msgstr ""
"Wywołanie systemowe B<delete_module>() próbuje usunąć nieużywany, ładowalny "
"wpis modułu określony nazwą I<name>. Jeśli moduł posiada funkcję I<exit>, to "
"jest ona wykonywana przed usunięciem modułu. Argument I<flags> służy do "
"modyfikacji zachowania wywołania systemowego, zgodnie z opisem poniżej. To "
"wywołanie systemowe wymaga uprzywilejowania."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Module removal is attempted according to the following rules:"
msgstr "Próba usunięcia modułu zachodzi zgodnie z poniższymi regułami:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "(1)"
msgstr "(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If there are other loaded modules that depend on (i.e., refer to symbols "
"defined in) this module, then the call fails."
msgstr ""
"Jeśli występują inne załadowane moduły zależące od tego modułu (tzn. "
"odniesienia do symboli w nim zdefiniowanych), to wywołanie zawodzi."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "(2)"
msgstr "(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Otherwise, if the reference count for the module (i.e., the number of "
"processes currently using the module)  is zero, then the module is "
"immediately unloaded."
msgstr ""
"W przeciwnym razie, jeśli licznik odniesień do modułu (tzn. liczba procesów "
"używających go aktualnie) wynosi zero, to moduł jest natychmiast usuwany."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "(3)"
msgstr "(3)"

#.   	O_TRUNC == KMOD_REMOVE_FORCE in kmod library
#.   	O_NONBLOCK == KMOD_REMOVE_NOWAIT in kmod library
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a module has a nonzero reference count, then the behavior depends on the "
"bits set in I<flags>.  In normal usage (see NOTES), the B<O_NONBLOCK> flag "
"is always specified, and the B<O_TRUNC> flag may additionally be specified."
msgstr ""
"Jeśli moduł ma niezerowy licznik odniesień, to zachowanie zależy od bitów "
"ustawionych w I<flags>. Przy zwykłym użyciu (zob. UWAGI), podany jest zawsze "
"znacznik B<O_NONBLOCK>, dodatkowo można też podać znacznik B<O_TRUNC>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The various combinations for I<flags> have the following effect:"
msgstr "Różne zestawienia I<flags> dają następujący rezultat:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<flags == O_NONBLOCK>"
msgstr "B<flags == O_NONBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The call returns immediately, with an error."
msgstr "Wywołanie natychmiast zwraca z błędem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<flags == (O_NONBLOCK | O_TRUNC)>"
msgstr "B<flags == (O_NONBLOCK | O_TRUNC)>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The module is unloaded immediately, regardless of whether it has a nonzero "
"reference count."
msgstr ""
"Moduł jest natychmiast usuwany, niezależnie od tego, czy posiada zerowy "
"licznik odniesień."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<(flags & O_NONBLOCK) == 0>"
msgstr "B<(flags & O_NONBLOCK) == 0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<flags> does not specify B<O_NONBLOCK>, the following steps occur:"
msgstr ""
"Jeśli we I<flags> nie podano B<O_NONBLOCK>, to zachodzą następujące kroki:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The module is marked so that no new references are permitted."
msgstr "Moduł jest oznaczany, dzięki czemu niedozwolone są nowe odniesienia."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the module's reference count is nonzero, the caller is placed in an "
"uninterruptible sleep state (B<TASK_UNINTERRUPTIBLE>)  until the reference "
"count is zero, at which point the call unblocks."
msgstr ""
"Jeśli licznik odniesień modułu jest niezerowy, to wywołujący jest "
"umieszczany w nieprzerywalnym stanie snu (B<TASK_UNINTERRUPTIBLE>) do "
"osiągnięcia przez licznik wartości zero, kiedy wywołanie jest odblokowywane."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The module is unloaded in the usual way."
msgstr "Moduł jest usuwany w zwykły sposób."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<O_TRUNC> flag has one further effect on the rules described above.  By "
"default, if a module has an I<init> function but no I<exit> function, then "
"an attempt to remove the module fails.  However, if B<O_TRUNC> was "
"specified, this requirement is bypassed."
msgstr ""
"Znacznik B<O_TRUNC> ma jeden dodatkowy skutek wobec powyższych reguł. "
"Domyślnie, jeśli moduł ma funkcję I<init>, lecz nie posiada funkcji I<exit>, "
"to próba usunięcia modułu zawodzi. Jeśli jednak podano B<O_TRUNC> to ten "
"warunek jest pomijany."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Using the B<O_TRUNC> flag is dangerous! If the kernel was not built with "
"B<CONFIG_MODULE_FORCE_UNLOAD>, this flag is silently ignored.  (Normally, "
"B<CONFIG_MODULE_FORCE_UNLOAD> is enabled.)  Using this flag taints the "
"kernel (TAINT_FORCED_RMMOD)."
msgstr ""
"Używanie znacznika B<O_TRUNC> jest niebezpieczne! Jeśli jądro nie zostało "
"zbudowane z B<CONFIG_MODULE_FORCE_UNLOAD>, znacznik jest po cichu ignorowany "
"(zwykle B<CONFIG_MODULE_FORCE_UNLOAD> jest włączony). Używanie tego "
"znacznika prowadzi do skażenia jądra (TAINT_FORCED_RMMOD)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned and I<errno> is set "
"to indicate the error."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"ustawiane jest I<errno> wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The module is not \"live\" (i.e., it is still being initialized or is "
"already marked for removal); or, the module has an I<init> function but has "
"no I<exit> function, and B<O_TRUNC> was not specified in I<flags>."
msgstr ""
"Moduł nie jest \\[Bq]żywy\\[rq] (tzn. wciąż jest inicjowany lub został już "
"oznaczony do usunięcia) lub moduł posiada funkcję I<init>, ale nie ma "
"funkcji I<exit> a we I<flags> nie podano B<O_TRUNC>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<name> refers to a location outside the process's accessible address space."
msgstr ""
"I<name> odnosi się do położenia, które jest poza dostępną dla procesu "
"przestrzenią adresową."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "No module by that name exists."
msgstr "Nie istnieje moduł o tej nazwie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The caller was not privileged (did not have the B<CAP_SYS_MODULE> "
"capability), or module unloading is disabled (see I</proc/sys/kernel/"
"modules_disabled> in B<proc>(5))."
msgstr ""
"Wywołujący nie był uprzywilejowany (nie posiadał przywileju (ang. "
"capability) B<CAP_SYS_MODULE>) lub usuwanie modułów jest wyłączone (zob.  I</"
"proc/sys/kernel/modules_disabled> w B<proc>(5))."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EWOULDBLOCK>"
msgstr "B<EWOULDBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Other modules depend on this module; or, B<O_NONBLOCK> was specified in "
"I<flags>, but the reference count of this module is nonzero and B<O_TRUNC> "
"was not specified in I<flags>."
msgstr ""
"Od tego modułu zależą inne moduły; albo we I<flags> podano B<O_NONBLOCK>, "
"lecz licznik odniesień tego modułu jest niezerowy i we I<flags> nie podano "
"B<O_TRUNC>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<delete_module>()  system call is not supported by glibc.  No "
"declaration is provided in glibc headers, but, through a quirk of history, "
"glibc versions before glibc 2.23 did export an ABI for this system call.  "
"Therefore, in order to employ this system call, it is (before glibc 2.23) "
"sufficient to manually declare the interface in your code; alternatively, "
"you can invoke the system call using B<syscall>(2)."
msgstr ""
"Wywołanie systemowe B<delete_module>() nie jest obsługiwane przez glibc. W "
"nagłówkach glibc nie ma jego deklaracji, ale z powodów pewnych zaszłości "
"historycznych wersje przed glibc 2.23 eksportowały ABI dla tego wywołania "
"systemowego. Z tego powodu, aby go użyć wystarczy (przed glibc 2.23) "
"manualnie zadeklarować interfejs w swoim kodzie; alternatywnie można wywołać "
"to wywołanie systemowe za pomocą B<syscall>(2)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Linux 2.4 and earlier"
msgstr "Linux 2.4 i wcześniejsze"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "In Linux 2.4 and earlier, the system call took only one argument:"
msgstr ""
"W Linuksie 2.4 i wcześniejszych, wywołanie systemowe przyjmowało jedynie "
"jeden argument:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B< int delete_module(const char *>I<name>B<);>"
msgstr "B< int delete_module(const char *>I<name>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<name> is NULL, all unused modules marked auto-clean are removed."
msgstr ""
"Gdy I<name> wynosi NULL, wszystkie nieużywane moduły oznaczone auto-clean są "
"usuwane."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some further details of differences in the behavior of B<delete_module>()  "
"in Linux 2.4 and earlier are I<not> currently explained in this manual page."
msgstr ""
"Pewne dalsze detale na temat różnic w zachowaniu B<delete_module>() w "
"Linuksie 2.4 i wcześniejszych I<nie> są obecnie wytłumaczone w niniejszym "
"podręczniku."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The uninterruptible sleep that may occur if B<O_NONBLOCK> is omitted from "
"I<flags> is considered undesirable, because the sleeping process is left in "
"an unkillable state.  As at Linux 3.7, specifying B<O_NONBLOCK> is optional, "
"but in future kernels it is likely to become mandatory."
msgstr ""
"Nieprzerywalny stan snu, który może się zdarzyć, gdy pominie się "
"B<O_NONBLOCK> z I<flags> jest uważany za niepożądany, ponieważ śpiący proces "
"pozostawiany jest w stanie nieśmiertelności. Według stanu na Linux 3.7, "
"podawanie B<O_NONBLOCK> jest opcjonalne, ale w przyszłych jądrach "
"prawdopodobnie stanie się to obowiązkowe."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<create_module>(2), B<init_module>(2), B<query_module>(2), B<lsmod>(8), "
"B<modprobe>(8), B<rmmod>(8)"
msgstr ""
"B<create_module>(2), B<init_module>(2), B<query_module>(2), B<lsmod>(8), "
"B<modprobe>(8), B<rmmod>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "B<delete_module>()  is Linux-specific."
msgstr "B<delete_module>() jest specyficzne dla Linuksa."

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
