# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2001.
# Robert Luberda <robert@debian.org>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-03-01 17:02+0100\n"
"PO-Revision-Date: 2017-02-12 23:48+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "nfsservctl"
msgstr "nfsservctl"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "nfsservctl - syscall interface to kernel nfs daemon"
msgstr ""
"nfsservctl - funkcja systemowa stanowiąca interfejs do demona NFS w jądrze"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/nfsd/syscall.hE<gt>>\n"
msgstr "B<#include E<lt>linux/nfsd/syscall.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long nfsservctl(int >I<cmd>B<, struct nfsctl_arg *>I<argp>B<,>\n"
"B<                union nfsctl_res *>I<resp>B<);>\n"
msgstr ""
"B<long nfsservctl(int >I<cmd>B<, struct nfsctl_arg *>I<argp>B<,>\n"
"B<                union nfsctl_res *>I<resp>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: Since Linux 3.1, this system call no longer exists.  It has been "
"replaced by a set of files in the I<nfsd> filesystem; see B<nfsd>(7)."
msgstr ""
"I<Uwaga>: Ta wywołanie systemowe zostało usunięte w Linuksie 3.1. Zostało "
"zastąpione przez pliki w systemie plików I<nfsd>; patrz B<nfsd>(7)."

# FIXME mountd(8) → B<mountd>(8)
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "/*\n"
#| " * These are the commands understood by nfsctl().\n"
#| " */\n"
#| "#define NFSCTL_SVC        0  /* This is a server process. */\n"
#| "#define NFSCTL_ADDCLIENT  1  /* Add an NFS client. */\n"
#| "#define NFSCTL_DELCLIENT  2  /* Remove an NFS client. */\n"
#| "#define NFSCTL_EXPORT     3  /* Export a filesystem. */\n"
#| "#define NFSCTL_UNEXPORT   4  /* Unexport a filesystem. */\n"
#| "#define NFSCTL_UGIDUPDATE 5  /* Update a client\\[aq]s UID/GID map\n"
#| "                                (only in Linux 2.4.x and earlier). */\n"
#| "#define NFSCTL_GETFH      6  /* Get a file handle (used by mountd(8))\n"
#| "                                (only in Linux 2.4.x and earlier). */\n"
msgid ""
"/*\n"
" * These are the commands understood by nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* This is a server process. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Add an NFS client. */\n"
"#define NFSCTL_DELCLIENT  2  /* Remove an NFS client. */\n"
"#define NFSCTL_EXPORT     3  /* Export a filesystem. */\n"
"#define NFSCTL_UNEXPORT   4  /* Unexport a filesystem. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Update a client\\[aq]s UID/GID map\n"
"                                (only in Linux 2.4.x and earlier). */\n"
"#define NFSCTL_GETFH      6  /* Get a file handle (used by mountd(8))\n"
"                                (only in Linux 2.4.x and earlier). */\n"
"\\&\n"
"struct nfsctl_arg {\n"
"    int                       ca_version;     /* safeguard */\n"
"    union {\n"
"        struct nfsctl_svc     u_svc;\n"
"        struct nfsctl_client  u_client;\n"
"        struct nfsctl_export  u_export;\n"
"        struct nfsctl_uidmap  u_umap;\n"
"        struct nfsctl_fhparm  u_getfh;\n"
"        unsigned int          u_debug;\n"
"    } u;\n"
"}\n"
"\\&\n"
"union nfsctl_res {\n"
"        struct knfs_fh          cr_getfh;\n"
"        unsigned int            cr_debug;\n"
"};\n"
msgstr ""
"/*\n"
" * To są polecenia rozumiane przez nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* Jest to proces serwera. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Dodanie klienta NFS. */\n"
"#define NFSCTL_DELCLIENT  2  /* Usunięcie klienta NFS. */\n"
"#define NFSCTL_EXPORT     3  /* Eksportowanie systemu plików. */\n"
"#define NFSCTL_UNEXPORT   4  /* Zaprzestanie eksportowania\n"
"                                systemu plików. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Uaktualnienie mapy UID/GIT klienta\n"
"                                (tylko w Linuksie 2.4 i wcześniejszych). */\n"
"#define NFSCTL_GETFH      6  /* otrzymanie fh (używane przez B<mountd>(8))\n"
"                                           (tylko w Linuksie 2.4 i wcześniejszych). */\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"ustawiane jest I<errno> wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This system call is present on Linux only up until kernel 2.4; it was "
#| "removed in Linux 2.6."
msgid "Removed in Linux 3.1.  Removed in glibc 2.28."
msgstr ""
"To wywołanie systemowe jest obecne w Linuksie tylko do wersji 2.4 jądra; "
"zostało usunięte w Linuksie 2.6."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<nfsd>(7)"
msgstr "B<nfsd>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

# FIXME mountd(8) → B<mountd>(8)
#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"/*\n"
" * These are the commands understood by nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* This is a server process. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Add an NFS client. */\n"
"#define NFSCTL_DELCLIENT  2  /* Remove an NFS client. */\n"
"#define NFSCTL_EXPORT     3  /* Export a filesystem. */\n"
"#define NFSCTL_UNEXPORT   4  /* Unexport a filesystem. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Update a client\\[aq]s UID/GID map\n"
"                                (only in Linux 2.4.x and earlier). */\n"
"#define NFSCTL_GETFH      6  /* Get a file handle (used by mountd(8))\n"
"                                (only in Linux 2.4.x and earlier). */\n"
msgstr ""
"/*\n"
" * To są polecenia rozumiane przez nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* Jest to proces serwera. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Dodanie klienta NFS. */\n"
"#define NFSCTL_DELCLIENT  2  /* Usunięcie klienta NFS. */\n"
"#define NFSCTL_EXPORT     3  /* Eksportowanie systemu plików. */\n"
"#define NFSCTL_UNEXPORT   4  /* Zaprzestanie eksportowania\n"
"                                systemu plików. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Uaktualnienie mapy UID/GIT klienta\n"
"                                (tylko w Linuksie 2.4 i wcześniejszych). */\n"
"#define NFSCTL_GETFH      6  /* otrzymanie fh (używane przez B<mountd>(8))\n"
"                                           (tylko w Linuksie 2.4 i wcześniejszych). */\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"struct nfsctl_arg {\n"
"    int                       ca_version;     /* safeguard */\n"
"    union {\n"
"        struct nfsctl_svc     u_svc;\n"
"        struct nfsctl_client  u_client;\n"
"        struct nfsctl_export  u_export;\n"
"        struct nfsctl_uidmap  u_umap;\n"
"        struct nfsctl_fhparm  u_getfh;\n"
"        unsigned int          u_debug;\n"
"    } u;\n"
"}\n"
msgstr ""
"struct nfsctl_arg {\n"
"    int                       ca_version;     /* zabezpieczenie */\n"
"    union {\n"
"        struct nfsctl_svc     u_svc;\n"
"        struct nfsctl_client  u_client;\n"
"        struct nfsctl_export  u_export;\n"
"        struct nfsctl_uidmap  u_umap;\n"
"        struct nfsctl_fhparm  u_getfh;\n"
"        unsigned int          u_debug;\n"
"    } u;\n"
"}\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"union nfsctl_res {\n"
"        struct knfs_fh          cr_getfh;\n"
"        unsigned int            cr_debug;\n"
"};\n"
msgstr ""
"union nfsctl_res {\n"
"        struct knfs_fh          cr_getfh;\n"
"        unsigned int            cr_debug;\n"
"};\n"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid ""
#| "This system call is present on Linux only up until kernel 2.4; it was "
#| "removed in Linux 2.6."
msgid ""
"This system call was removed in Linux 3.1.  Library support was removed in "
"glibc 2.28."
msgstr ""
"To wywołanie systemowe jest obecne w Linuksie tylko do wersji 2.4 jądra; "
"zostało usunięte w Linuksie 2.6."

#. type: Plain text
#: debian-bookworm
msgid "This call is Linux-specific."
msgstr "Ta funkcja jest specyficzna dla Linuksa."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-05-03"
msgstr "3 maja 2023 r."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
