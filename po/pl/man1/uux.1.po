# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Robert Luberda <robert@debian.org>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-02-15 18:15+0100\n"
"PO-Revision-Date: 2017-02-18 12:50+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "uux"
msgstr "uux"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Taylor UUCP 1.07"
msgstr "Taylor UUCP 1.07"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "uux - Remote command execution over UUCP"
msgstr "uux - Zdalne wywołanie polecenia poprzez UUCP"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<uux> [ options ] command"
msgstr "B<uux> [I<opcje>] I<polecenie>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<uux> command is used to execute a command on a remote system, or to "
"execute a command on the local system using files from remote systems.  The "
"command is not executed immediately; the request is queued until the "
"I<uucico> (8) daemon calls the system and executes it.  The daemon is "
"started automatically unless one of the B<-r> or B<--nouucico> options is "
"given."
msgstr ""
"I<uux> jest używane do wykonania zadanego polecenia na zdalnym systemie lub "
"do wykonania polecenia na systemie lokalnym z użyciem plików ze zdalnych "
"systemów. Podane polecenie nie jest wywoływane natychmiast; żądanie jest "
"kolejkowane do czasu, gdy demon I<uucico>(8) nie zadzwoni do odpowiedniego "
"systemu i jej nie wykona. Demon jest uruchomiany automatycznie, chyba że "
"zostanie podana opcja B<-r> lub B<--nouucico>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The actual command execution is done by the I<uuxqt> (8) daemon."
msgstr "Właściwe wykonanie polecenia jest dokonywane przez demona I<uuxqt>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"File arguments can be gathered from remote systems to the execution system, "
"as can standard input.  Standard output may be directed to a file on a "
"remote system."
msgstr ""
"Argumenty plikowe mogą być zebrane z systemów zdalnych, a także ze "
"standardowego wejścia. Standardowe wyjście może być przekierowane do pliku "
"na zdalnym systemie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The command name may be preceded by a system name followed by an exclamation "
"point if it is to be executed on a remote system.  An empty system name is "
"taken as the local system."
msgstr ""
"Nazwa polecenia może być poprzedzona nazwą systemu zakończoną wykrzyknikiem, "
"jeśli ma ono być wykonane na zdalnym systemie. Pusta nazwa systemu jest "
"uważana za nazwę systemu lokalnego."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Each argument that contains an exclamation point is treated as naming a "
"file.  The system which the file is on is before the exclamation point, and "
"the pathname on that system follows it.  An empty system name is taken as "
"the local system; this must be used to transfer a file to a command being "
"executed on a remote system.  If the path is not absolute, it will be "
"appended to the current working directory on the local system; the result "
"may not be meaningful on the remote system.  A pathname may begin with ~/, "
"in which case it is relative to the UUCP public directory (usually /usr/"
"spool/uucppublic or /var/spool/uucppublic) on the appropriate system.  A "
"pathname may begin with ~name/, in which case it is relative to the home "
"directory of the named user on the appropriate system."
msgstr ""
"Argumenty zawierające wykrzyknik są uważane za nazwy plików. Przed "
"wykrzyknikiem znajduje się nazwa systemu, na którym leży dany plik, a za "
"wykrzyknikiem - ścieżka do tego pliku. Pusta nazwa systemu oznacza system "
"lokalny; taki zapis musi być użyty przy transferowaniu pliku lokalnego do "
"polecenia wywoływanego na zdalnym systemie. Jeśli ścieżka nie jest ścieżką "
"bezwzględną, zostanie dołączona do bieżącego katalogu roboczego systemu "
"lokalnego; rezultat nie musi być znaczący dla zdalnego systemu. Ścieżka może "
"zaczynać się od B<~/> - wówczas jest względna w stosunku do publicznego "
"katalogu UUCP (zwykle /usr/spool/uucppublic lub /var/spool/uucppublic) na "
"odpowiednim systemie. Ścieżka może się zaczynać od B<~nazwa/>, wówczas jest "
"względna w stosunku do katalogu domowego użytkownika o podanej nazwie na "
"danym systemie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Standard input and output may be redirected as usual; the pathnames used may "
"contain exclamation points to indicate that they are on remote systems.  "
"Note that the redirection characters must be quoted so that they are passed "
"to I<uux> rather than interpreted by the shell.  Append redirection "
"(E<gt>E<gt>) does not work."
msgstr ""
"Standardowe wyjście i wejście można przekierowywać jak zwykle. Nazwy plików "
"z nimi kojarzonych mogą zawierać wykrzykniki, wskazujące system. Proszę "
"zauważyć, że znaki przekierowania podczas przekazywania do I<uux> muszą być "
"cytowane i nie powinno się dopuszczać do interpretowania ich przez powłokę. "
"Przekierowanie dopisujące (E<gt>E<gt>) nie działa."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All specified files are gathered together into a single directory before "
"execution of the command begins.  This means that each file must have a "
"distinct base name.  For example,"
msgstr ""
"Wszystkie podane pliki są przed wywołaniem polecenia zbierane do "
"pojedynczego katalogu. Znaczy to, że każdy plik musi mieć inną nazwę. Na "
"przykład"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "uux 'sys1!diff sys2!~user1/foo sys3!~user2/foo E<gt>!foo.diff'\n"
msgstr "uux 'sys1!diff sys2!~user1/foo sys3!~user2/foo E<gt>!foo.diff'\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"will fail because both files will be copied to sys1 and stored under the "
"name foo."
msgstr ""
"nie powiedzie się, ponieważ obydwa pliki zostaną skopiowane na sys1 i "
"zapisane pod tą samą nazwą foo."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Arguments may be quoted by parentheses to avoid interpretation of "
"exclamation points.  This is useful when executing the I<uucp> command on a "
"remote system."
msgstr ""
"Dla zapobieżenia interpretacji wykrzykników można cytować argumenty za "
"pomocą nawiasów. Przydaje się to podczas wywoływania polecenia I<uucp> na "
"zdalnym systemie."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A request to execute an empty command (e.g., I<uux sys!)> will create a poll "
"file for the specified system."
msgstr ""
"Żądanie wywołania pustego polecenia (np.  I<uux sys!)> nie stworzy pliku "
"poll'ującego na dany system."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The exit status of I<uux> is one of the codes found in the header file "
"B<sysexits.h.> In particular, B<EX_OK> ( B<0> ) indicates success, and "
"B<EX_TEMPFAIL> ( B<75> ) indicates a temporary failure."
msgstr ""
"Kodem wyjścia programu I<uux> jest jeden z kodów wymienionych w pliku "
"nagłówkowym B<sysexits.h>. W szczególności B<EX_OK> (B<0>) oznacza "
"powodzenie, a B<EX_TEMPFAIL> (B<75>) wskazuje na błąd tymczasowy."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following options may be given to I<uux.>"
msgstr "I<uux> przyjmuje następujące opcje:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-, -p, --stdin>"
msgstr "B<-, -p, --stdin>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Read standard input and use it as the standard input for the command to be "
"executed."
msgstr ""
"Odczytuje standardowe wejście i używa go jako standardowego wejścia dla "
"wywoływanego polecenia."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-c, --nocopy>"
msgstr "B<-c, --nocopy>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Do not copy local files to the spool directory.  This is the default.  If "
"they are removed before being processed by the I<uucico> (8) daemon, the "
"copy will fail.  The files must be readable by the I<uucico> (8) daemon, as "
"well as the by the invoker of I<uux.>"
msgstr ""
"Nie kopiuje plików lokalnych do katalogu kolejkowego. Tak jest domyślnie.  "
"Jeśli zostaną one usunięte przed przetworzeniem przez demon I<uucico>(8), "
"kopiowanie nie powiedzie się. Pliki muszą być odczytywalne przez demona "
"I<uucico>(8), a także przez użytkownika wywołującego I<uux>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-C, --copy>"
msgstr "B<-C, --copy>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Copy local files to the spool directory."
msgstr "Kopiuje pliki lokalne do katalogu kolejkowego."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-l, --link>"
msgstr "B<-l, --link>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Link local files into the spool directory.  If a file can not be linked "
"because it is on a different device, it will be copied unless one of the B<-"
"c> or B<--nocopy> options also appears (in other words, use of B<--link> "
"switches the default from B<--nocopy> to B<--copy).> If the files are "
"changed before being processed by the I<uucico> (8) daemon, the changed "
"versions will be used.  The files must be readable by the I<uucico> (8) "
"daemon, as well as by the invoker of I<uux.>"
msgstr ""
"Tworzy dowiązania do plików lokalnych w katalogu kolejkowym. Jeśli nie można "
"utworzyć dowiązania do pliku, ponieważ leży on na innym urządzeniu, zostanie "
"skopiowany, chyba że podano jedną z opcji B<-c> lub B<--nocopy> (innymi "
"słowy, użycie opcji B<--link> przełącza domyślny tryb z B<--nocopy> na B<--"
"copy>). Jeśli pliki zostaną zmienione przed przetworzeniem przez demona "
"I<uucico>(8), użyte zostaną wersje zmienione. Pliki muszą być odczytywalne "
"przez demona I<uucico>(8) oraz przez użytkownika wywołującego I<uux>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-g grade, --grade grade>"
msgstr "B<-g poziom, --grade poziom>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set the grade of the file transfer command.  Jobs of a higher grade are "
"executed first.  Grades run 0 ... 9 A ... Z a ... z from high to low."
msgstr ""
"Ustawia poziom polecenia transferu plików. Zadania wyższego stopnia są "
"wykonywane jako pierwsze. Poziomy przyjmują wartości 0 ... 9 A ... Z a ... z "
"od najwyższego do najniższego."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-n, --notification=no>"
msgstr "B<-n, --notification=no>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Do not send mail about the status of the job, even if it fails."
msgstr "Nie wysyła e-maili o stanie zadania, nawet jeśli się nie powiedzie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-z, --notification=error>"
msgstr "B<-z, --notification=error>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Send mail about the status of the job if an error occurs.  For many I<uuxqt> "
"daemons, including the Taylor UUCP I<uuxqt,> this is the default action; for "
"those, B<--notification=error> will have no effect.  However, some I<uuxqt> "
"daemons will send mail if the job succeeds unless the B<--"
"notification=error> option is used, and some other I<uuxqt> daemons will not "
"send mail if the job fails unless the B<--notification=error> option is used."
msgstr ""
"Wysyła informacje o błędach w zadaniu. Dla wielu demonów I<uuxqt>, łącznie z "
"Taylor UUCP I<uuxqt>, jest to akcja domyślna; dla nich opcja B<--"
"notification=error> nie daje żadnego rezultatu. Jednak niektóre demony "
"I<uuxqt> wysyłają pocztę, jeśli zadanie się powiedzie, chyba że użyje się "
"opcji B<--notification=error>, a parę innych demonów I<uuxqt> w ogóle nie "
"będzie wysyłać e-maili, jeśli zdanie się powiedzie, o ile nie będzie "
"ustawiona opcja B<--notification=error>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-r, --nouucico>"
msgstr "B<-r, --nouucico>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Do not start the I<uucico> (8) daemon immediately; merely queue up the "
"execution request for later processing."
msgstr ""
"Nie uruchamia demona I<uucico>(8)  natychmiast; zamiast tego po prostu "
"kolejkuje żądanie do dalszego przetworzenia."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-j, --jobid>"
msgstr "B<-j, --jobid>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Print jobids on standard output.  A jobid will be generated for each file "
"copy operation required to perform the operation.  These file copies may be "
"cancelled by passing the jobid to the B<--kill> switch of I<uustat> (1), "
"which will make the execution impossible to complete."
msgstr ""
"Wypisuje na standardowym wyjściu identyfikatory zadań. Identyfikatory są "
"generowane dla każdej operacji kopiowania plików, wymaganej do dokonania "
"operacji. Kopie te mogą być unieważniane przez przekazanie identyfikatorowi "
"zadania opcji B<--kill> programu I<uustat>(1), która uniemożliwi dokonanie "
"wywołania."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-a address, --requestor address>"
msgstr "B<-a adres, --requestor adres>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Report job status to the specified e-mail address."
msgstr "Zgłasza stan zadania na podany adres e-mail."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x type, --debug type>"
msgstr "B<-x typ, --debug typ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Turn on particular debugging types.  The following types are recognized: "
"abnormal, chat, handshake, uucp-proto, proto, port, config, spooldir, "
"execute, incoming, outgoing.  Only abnormal, config, spooldir and execute "
"are meaningful for I<uux.>"
msgstr ""
"Włącza dany rodzaj diagnostyki. Rozpoznawane są następujące: abnormal, chat, "
"handshake, uucp-proto, proto, port, config, spooldir, execute, incoming, "
"outgoing. Dla uux znaczenie mają tylko abnormal, config, spooldir i execute."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Multiple types may be given, separated by commas, and the B<--debug> option "
"may appear multiple times.  A number may also be given, which will turn on "
"that many types from the foregoing list; for example, B<--debug 2> is "
"equivalent to B<--debug abnormal,chat.>"
msgstr ""
"Można podawać wiele typów rozdzielonych przecinkami, a opcja B<--debug> może "
"pojawić się wiele razy. Można podać także liczbę, która włączy kolejne typy "
"powyższej listy; np, B<--debug 2> jest równoważne B<--debug abnormal,chat>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-I file, --config file>"
msgstr "B<-I file, --config pliki>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Set configuration file to use.  This option may not be available, depending "
"upon how I<uux> was compiled."
msgstr ""
"Wskazuje plik konfiguracyjny, z którego ma korzystać program.  Opcja ta może "
"być niedostępna, zależnie od tego, jak skompilowano I<uux>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-v, --version>"
msgstr "B<-v, --version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Report version information and exit."
msgstr "Pokazuje informację o wersji programu i kończy działanie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Print a help message and exit."
msgstr "Wyświetla komunikat pomocy i wychodzi."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "uux -z - sys1!rmail user1\n"
msgstr "uux -z - sys1!rmail user1\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Execute the command ``rmail user1'' on the system sys1, giving it as "
"standard input whatever is given to I<uux> as standard input.  If a failure "
"occurs, send a message using I<mail> (1)."
msgstr ""
"Wywołuje polecenie \"rmail user1\" na systemie sys1, podając za standardowe "
"wejście to, co podano I<uux> jako standardowe wejście. Jeśli pojawi się "
"błąd, wysyła wiadomość za pomocą klienta I<mail>(1)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "uux 'diff -c sys1!~user1/file1 sys2!~user2/file2 E<gt>!file.diff'\n"
msgstr "uux 'diff -c sys1!~user1/file1 sys2!~user2/file2 E<gt>!file.diff'\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Fetch the two named files from system sys1 and system sys2 and execute "
"I<diff> putting the result in file.diff in the current directory.  The "
"current directory must be writable by the I<uuxqt> (8) daemon for this to "
"work."
msgstr ""
"Ściąga dwa nazwane pliki z systemu sys1 i systemu sys2 i wywołuje "
"I<diff>(1), a następnie wkłada wynik do bieżącego katalogu. Bieżący katalog "
"musi być dostępny do zapisu dla demona I<uuxqt>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "uux 'sys1!uucp ~user1/file1 (sys2!~user2/file2)'\n"
msgstr "uux 'sys1!uucp ~user1/file1 (sys2!~user2/file2)'\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Execute I<uucp> on the system sys1 copying file1 (on system sys1) to sys2.  "
"This illustrates the use of parentheses for quoting."
msgstr ""
"Wywołuje na systemie sys1 I<uucp> i kopiuje plik file1 (z systemu sys1) na "
"sys2. Ten przykład pokazuje zastosowanie nawiasów do cytowania."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RESTRICTIONS"
msgstr "OGRANICZENIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The remote system may not permit you to execute certain commands.  Many "
"remote systems only permit the execution of I<rmail> and I<rnews.>"
msgstr ""
"Zdalny system może nie pozwalać na wywoływanie niektórych poleceń. Wiele "
"zdalnych systemów zezwala tylko na wywoływanie I<rmail> i I<rnews.>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some of the options are dependent on the capabilities of the I<uuxqt> (8) "
"daemon on the remote system."
msgstr ""
"Niektóre z opcji są zależne od zdolności demona I<uuxqt>(8) na zdalnym "
"systemie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mail(1), uustat(1), uucp(1), uucico(8), uuxqt(8)"
msgstr "mail(1), uustat(1), uucp(1), uucico(8), uuxqt(8)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "USTERKI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Files can not be referenced across multiple systems."
msgstr "Nie można odwoływać się do plików między wieloma systemami."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Too many jobids are output by B<--jobid,> and there is no good way to cancel "
"a local execution requiring remote files."
msgstr ""
"B<--jobid> tworzy zbyt wiele identyfikatorów i nie ma możliwości anulowania "
"lokalnego wywołania wymagającego zdalnych plików."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Ian Lance Taylor (ian@airs.com)"
msgstr "Ian Lance Taylor (ian@airs.com)"
