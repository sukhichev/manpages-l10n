# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2021, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-03-09 15:32+0100\n"
"PO-Revision-Date: 2024-03-15 19:32+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "clear"
msgstr "clear"

#. type: ds n
#: archlinux debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "5"
msgstr "5"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "B<clear> - clear the terminal screen"
msgstr "B<clear> - czyści ekran terminala"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "B<clear> [B<-T>I<type>] [B<-V>] [B<-x>]"
msgstr "B<clear> [B<-T>I<typ>] [B<-V>] [B<-x>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm
msgid ""
"B<clear> clears your terminal's screen if this is possible, including the "
"terminal's scrollback buffer (if the extended \\*(``E3\\*('' capability is "
"defined).  B<clear> looks in the environment for the terminal type given by "
"the environment variable B<TERM>, and then in the B<terminfo> database to "
"determine how to clear the screen."
msgstr ""
"B<clear> czyści ekran terminala (o ile to możliwe) w tym bufor przewijania "
"do tyłu (jeśli zdefiniowano rozszerzoną funkcję \\[Bq]E3\\[rq]). Sprawdza on "
"w środowisku typ terminala za pomocą zmiennej środowiskowej B<TERM>, a potem "
"w bazie terminfo szuka sposobu na wyczyszczenie ekranu."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"B<clear> writes to the standard output.  You can redirect the standard "
"output to a file (which prevents B<clear> from actually clearing the "
"screen), and later B<cat> the file to the screen, clearing it at that point."
msgstr ""
"B<clear> pisze na standardowe wyjście. Istnieje możliwość przekierowania "
"standardowego wyjścia do pliku (co zapobiega wyczyszczeniu ekranu), by "
"następnie wywołując B<cat> I<plik> wypisać jego zawartość, czyszcząc "
"terminal."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-T >I<type>"
msgstr "B<-T >I<typ>"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"indicates the I<type> of terminal.  Normally this option is unnecessary, "
"because the default is taken from the environment variable B<TERM>.  If B<-"
"T> is specified, then the shell variables B<LINES> and B<COLUMNS> will also "
"be ignored."
msgstr ""
"wskazuje I<typ> terminala. Zwykle ta opcja nie jest wymagana, ponieważ "
"wartość domyślna jest brana ze zmiennej środowiskowej B<TERM>. Jeśli B<-T> "
"został określony, zmienne środowiskowe B<LINES> oraz B<COLUMNS> również "
"zostaną zignorowane."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"reports the version of ncurses which was used in this program, and exits.  "
"The options are as follows:"
msgstr ""
"zwraca wersję ncurses, która została użyta w tym programie i kończy "
"działanie. Opcje są następujące:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>"
msgstr "B<-x>"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"do not attempt to clear the terminal's scrollback buffer using the extended "
"\\*(``E3\\*('' capability."
msgstr ""
"nie próbuje wyczyścić bufora przewijania terminala za pomocą rozszerzonej "
"funkcji \"E3\"."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"A B<clear> command appeared in 2.79BSD dated February 24, 1979.  Later that "
"was provided in Unix 8th edition (1985)."
msgstr ""
"Polecenie B<clear> pojawiło się w BSD 2.79 z 24 Stycznia 1979. Następnie "
"zostało dostarczone w 8 edycji Unixa (1985)."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"AT&T adapted a different BSD program (B<tset>) to make a new command "
"(B<tput>), and used this to replace the B<clear> command with a shell script "
"which calls B<tput clear>, e.g.,"
msgstr ""
"AT&T dostosowało inny program z BSD (B<tset>) by stworzyć nową komendę "
"(B<tput>), oraz użyła jej by zastąpić komendę B<clear> skryptem powłoki "
"wywołującym B<tput clear>, np.,"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "/usr/bin/tput ${1:+-T$1} clear 2E<gt> /dev/null exit"
msgstr "/usr/bin/tput ${1:+-T$1} clear 2E<gt> /dev/null exit"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"In 1989, when Keith Bostic revised the BSD B<tput> command to make it "
"similar to the AT&T B<tput>, he added a shell script for the B<clear> "
"command:"
msgstr ""
"W 1989 Keith Bostic poprawił na BSD komendę B<tput>, by działała podobnie do "
"B<tput> stworzonego przez AT&T, dodając skrypt powłoki dla komendy B<clear>:"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "exec tput clear"
msgstr "exec tput clear"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The remainder of the script in each case is a copyright notice."
msgstr "Reszta skryptu w obu przypadkach jest informacją o prawach autorskich."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"The ncurses B<clear> command began in 1995 by adapting the original BSD "
"B<clear> command (with terminfo, of course)."
msgstr ""
"Wersja B<clear> oparta o ncurses pojawiła się w 1995 przez adaptację "
"oryginalnej wersji komendy B<clear> z BSD (z wykorzystaniem terminfo)."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "The B<E3> extension came later:"
msgstr "Rozszerzenie B<E3> wyszło później:"

#. type: Plain text
#: archlinux debian-bookworm
msgid ""
"In June 1999, B<xterm> provided an extension to the standard control "
"sequence for clearing the screen.  Rather than clearing just the visible "
"part of the screen using"
msgstr ""
"W czerwcu 1999, B<xterm> dostarczył rozszerzenie do standardowej sekwencji "
"czyszczącej ekran. Zamiast czyścić tylko widoczną część ekranu używając"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "printf '\\e033[2J'"
msgstr "printf '\\e033[2J'"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "one could clear the I<scrollback> using"
msgstr "można wyczyścić I<bufor przewijania> używając"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid "printf '\\e033[B<3>J'"
msgstr "printf '\\e033[B<3>J'"

#. type: Plain text
#: archlinux debian-bookworm
msgid ""
"This is documented in I<XTerm Control Sequences> as a feature originating "
"with B<xterm>."
msgstr ""
"Jest to udokumentowane w I<XTerm Control Sequences> jako funkcjonalność "
"wprowadzona w B<xterm>."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"A few other terminal developers adopted the feature, e.g., PuTTY in 2006."
msgstr ""
"Kilku innych deweloperów terminali zaadoptowało tą funkcjonalność, np. PuTTy "
"w 2006."

#. type: Plain text
#: archlinux debian-bookworm
msgid ""
"In April 2011, a Red Hat developer submitted a patch to the Linux kernel, "
"modifying its console driver to do the same thing.  The Linux change, part "
"of the 3.0 release, did not mention B<xterm>, although it was cited in the "
"Red Hat bug report (#683733)  which led to the change."
msgstr ""
"W kwietniu 2011, deweloper Red Hat wypuścił łatkę do jądra Linuxa, "
"modyfikującą jego sterownik konsoli by wykorzystywał tą samą funkcjonalność. "
"Zmiany w Linuksie, część wydania 3.0, nie zawierały B<xterm>, jednakże "
"zostało to przytoczone w raporcie błędu Red Hat (#683733), co doprowadziło "
"do zmiany."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"Again, a few other terminal developers adopted the feature.  But the next "
"relevant step was a change to the B<clear> program in 2013 to incorporate "
"this extension."
msgstr ""
"Kilku kolejnych deweloperów terminali wdrożyło to rozwiązanie. Jednakże, "
"następnym znaczącym krokiem była zmiana z 2013 roku wprowadzająca to "
"rozszerzenie do programu B<clear>."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"In 2013, the B<E3> extension was overlooked in B<tput> with the "
"\\*(``clear\\*('' parameter.  That was addressed in 2016 by reorganizing "
"B<tput> to share its logic with B<clear> and B<tset>."
msgstr ""
"W 2013 roku rozszerzenie B<E3> zostało przeoczone w B<tput> przez parametr "
"\\[Bq]clear\\[rq]. W 2016 poprawiono B<tput> tak, aby zachowywał się "
"podobnie jak B<clear> i B<tset>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "PORTABILITY"
msgstr "PRZENOŚNOŚĆ"

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"Neither IEEE Std 1003.1/The Open Group Base Specifications Issue 7 "
"(POSIX.1-2008) nor X/Open Curses Issue 7 documents tset or reset."
msgstr ""
"Ani IEEE Std 1003.1/The Open Group Base Specifications Issue 7 "
"(POSIX.1-2008), ani X/Open Curses Issue 7 nie opisują tset oraz reset."

#. type: Plain text
#: archlinux debian-bookworm opensuse-leap-15-6
msgid ""
"The latter documents B<tput>, which could be used to replace this utility "
"either via a shell script or by an alias (such as a symbolic link) to run "
"B<tput> as B<clear>."
msgstr ""
"Ten drugi opisuje B<tput>, który można wykorzystać do zastąpienia tego "
"narzędzia za pomocą skryptu powłoki lub aliasu (np. łącza symbolicznego) "
"poprzez uruchamianie B<tput> jako B<clear>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux
msgid "B<tput>(1), B<xterm>(1), B<terminfo>(\\*n)."
msgstr "B<tput>(1), B<xterm>(1), B<terminfo>(\\*n)."

#. type: Plain text
#: archlinux
msgid "This describes B<ncurses> version 6.4 (patch 20230520)."
msgstr "Podręcznik odnosi się do wersji 6.4 B<ncurses> (łatka 20230520)."

#. type: Plain text
#: debian-bookworm
msgid "B<tput>(1), B<terminfo>(\\*n), B<xterm>(1)."
msgstr "B<tput>(1), B<terminfo>(\\*n), B<xterm>(1)."

#. type: Plain text
#: debian-bookworm
msgid "This describes B<ncurses> version 6.4 (patch 20221231)."
msgstr "Podręcznik odnosi się do wersji 6.4 B<ncurses> (łatka 20221231)."

#. type: TH
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-12-16"
msgstr "16 grudnia 2023 r."

#. type: TH
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ncurses 6.4"
msgstr "ncurses 6.4"

#. type: TH
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "User commands"
msgstr "Polecenia użytkownika"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<\\%clear> - clear the terminal screen"
msgstr "B<\\%clear> - czyści ekran terminala"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<clear> [B<-x>] [B<-T\\ > I<terminal-type>]"
msgstr "B<clear> [B<-x>] [B<-T\\ > I<typ-terminala>]"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<clear -V>"
msgstr "B<clear -V>"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<\\%clear> clears your terminal's screen and its scrollback buffer, if "
"any.  B<\\%clear> retrieves the terminal type from the environment variable "
"I<TERM>, then consults the I<terminfo> terminal capability database entry "
"for that type to determine how to perform these actions."
msgstr ""
"B<\\%clear> czyści ekran terminala i bufor przewijania do tyłu (jeśli "
"istnieje). B<\\%clear> pobiera typ terminala ze zmiennej środowiskowej "
"I<TERM>, a potem sprawdza odpowiedni wpis w bazie możliwości terminali "
"I<terminfo> aby ustalić, w jaki sposób przeprowadzić te działania."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The capabilities to clear the screen and scrollback buffer are named "
"\\*(``clear\\*('' and \\*(``E3\\*('', respectively.  The latter is a I<user-"
"defined capability>, applying an extension mechanism introduced in I<\\"
"%ncurses> 5.0 (1999)."
msgstr ""
"Możliwości czyszczenia ekranu terminala i bufora przewijania do tyłu są "
"nazwane odpowiednio: \\[Bq]clear\\[rq] i \\[Bq]E3\\[rq]. To ostatnie stanowi "
"I<możliwość zdefiniowaną przez użytkownika>, wykorzystując mechanizm "
"rozszerzeń wprowadzony w I<\\%ncurses> 5.0 (1999)."

#.  "-T type" + 2n
#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<\\%clear> recognizes the following options."
msgstr "B<\\%clear> rozpoznaje następujące opcje."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"produces instructions suitable for the terminal I<type>.  Normally, this "
"option is unnecessary, because the terminal type is inferred from the "
"environment variable I<TERM>.  If this option is specified, B<\\%clear> "
"ignores the environment variables I<LINES> and I<\\%COLUMNS> as well."
msgstr ""
"tworzy instrukcje odpowiednie do terminala podanego I<typu>.  Zwykle ta "
"opcja nie jest wymagana, ponieważ typ terminala jest określany za pomocą "
"zmiennej środowiskowej I<TERM>. Użycie tej opcji powoduje zignorowanie przez "
"B<\\%clear> również zmiennych środowiskowych I<LINES> (wiersze) i I<COLUMNS> "
"(kolumny)."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"reports the version of I<\\%ncurses> associated with this program and exits "
"with a successful status."
msgstr ""
"zwraca wersję I<\\%ncurses>, związaną z tym programem i kończy działanie z "
"kodem zakończenia oznaczającym sukces."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "prevents B<\\%clear> from attempting to clear the scrollback buffer."
msgstr ""
"zapobiega próbom wyczyszczenia bufora przewijania do tyłu przez B<\\%clear>."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Neither IEEE Std 1003.1/The Open Group Base Specifications Issue 7 "
"(POSIX.1-2008) nor X/Open Curses Issue 7 documents B<\\%clear>."
msgstr ""
"Ani IEEE Std 1003.1/The Open Group Base Specifications Issue 7 "
"(POSIX.1-2008), ani X/Open Curses Issue 7 nie opisują B<\\%clear>."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The latter documents B<tput>, which could be used to replace this utility "
"either via a shell script or by an alias (such as a symbolic link)  to run "
"B<\\%tput> as B<\\%clear>."
msgstr ""
"Ten drugi opisuje B<tput>, który można wykorzystać do zastąpienia tego "
"narzędzia za pomocą skryptu powłoki lub aliasu (np. dowiązania "
"symbolicznego) poprzez uruchamianie B<\\%tput> jako B<\\%clear>."

#.  https://minnie.tuhs.org/cgi-bin/utree.pl?file=2BSD/src/clear.c
#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"A B<clear> command using the I<termcap> database and library appeared in "
"2BSD (1979).  Eighth Edition Unix (1985) later included it."
msgstr ""
"Polecenie B<clear> używające biblioteki i bazy danych I<termcap> pojawiło "
"się w 2BSD (1979). Następnie istniało w ósmej edycji Uniksa (1985)."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The commercial Unix arm of AT&T adapted a different BSD program (B<tset>) to "
"make a new command, B<tput>, and replaced the B<clear> program with a shell "
"script that called \\*(``B<tput clear>\\*(''."
msgstr ""
"Oddział AT&T zajmujący się komercyjnym Uniksem dostosowało inny program z "
"BSD (B<tset>) by stworzyć nowe polecenie (B<tput>), oraz użył go do "
"zastąpienia programu B<clear> skryptem powłoki wywołującym B<tput clear>."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"/usr/bin/tput ${1:+-T$1} clear 2E<gt> /dev/null\n"
"exit\n"
msgstr ""
"/usr/bin/tput ${1:+-T$1} clear 2E<gt> /dev/null\n"
"exit\n"

#.  https://minnie.tuhs.org/cgi-bin/utree.pl?file=Net2/usr/src/usr.bin/#.    tput/clear.sh
#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"In 1989, when Keith Bostic revised the BSD B<tput> command to make it "
"similar to AT&T's B<tput>, he added a B<clear> shell script as well."
msgstr ""
"W 1989 Keith Bostic poprawił na BSD polecenie B<tput>, by działało podobnie "
"do B<tput> stworzonego przez AT&T, dodając również skrypt powłoki B<clear>."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "exec tput clear\n"
msgstr "exec tput clear\n"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"In 1995, I<\\%ncurses>'s B<clear> began by adapting BSD's original B<clear> "
"command to use I<terminfo>.  The B<E3> extension came later."
msgstr ""
"W 1995 B<clear> oparte o I<\\%ncurses> zaczęło od adaptacji oryginalnej "
"wersji polecenia B<clear> z BSD w celu wykorzystywania I<terminfo>. "
"Rozszerzenie B<E3> pojawiło się później."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"In June 1999, I<xterm> provided an extension to the standard control "
"sequence for clearing the screen.  Rather than clearing just the visible "
"part of the screen using"
msgstr ""
"W czerwcu 1999, I<xterm> dostarczył rozszerzenie do standardowej sekwencji "
"czyszczącej ekran. Zamiast czyścić tylko widoczną część ekranu używając"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "printf \\*'\\e033[2J\\*'\n"
msgstr "printf \\*'\\e033[2J\\*'\n"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "one could clear the scrollback buffer as well by using"
msgstr "można wyczyścić bufor przewijania do tyłu używając również"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "printf \\*'\\e033[B<3>J\\*'\n"
msgstr "printf \\*'\\e033[B<3>J\\*'\n"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"instead.  \\*(``XTerm Control Sequences\\*('' documents this feature as "
"originating with I<xterm>."
msgstr ""
"Jest to udokumentowane w \\[Bq]XTerm Control Sequences\\[rq] jako "
"funkcjonalność wprowadzona w I<xterm>."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "A few other terminal emulators adopted it, such as PuTTY in 2006."
msgstr ""
"Kilka innych emulatorów terminali zaadoptowało tę funkcjonalność, np. PuTTy "
"w 2006."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"In April 2011, a Red Hat developer submitted a patch to the Linux kernel, "
"modifying its console driver to do the same thing.  Documentation of this "
"change, appearing in Linux 3.0, did not mention I<xterm>, although that "
"program was cited in the Red Hat bug report (#683733)  motivating the "
"feature."
msgstr ""
"W kwietniu 2011, deweloper Red Hat wypuścił łatkę do jądra Linuxa, "
"modyfikującą jego sterownik konsoli by wykorzystywał tą samą funkcjonalność. "
"Dokumentacja tej zmiany, jaka pojawiła się w Linuksie 3.0 nie wspominała o "
"I<xterm>, jednakże zostało to przytoczone w raporcie błędu Red Hat "
"(#683733), motywującym tę funkcję."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Subsequently, more terminal developers adopted the feature.  The next "
"relevant step was to change the I<\\%ncurses> B<clear> program in 2013 to "
"incorporate this extension."
msgstr ""
"Kilku kolejnych deweloperów terminali wdrożyło to rozwiązanie. Następnym "
"znaczącym krokiem była zmiana z 2013 roku wprowadzająca to rozszerzenie do "
"programu B<clear> I<\\%ncurses>."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"In 2013, the B<E3> capability was not exercised by \\*(``B<\\%tput "
"clear>\\*(''.  That oversight was addressed in 2016 by reorganizing B<\\"
"%tput> to share its logic with B<\\%clear> and B<\\%tset>."
msgstr ""
"W 2013 roku rozszerzenie B<E3> nie zostało wykorzystane w B<\\%tput clear>. "
"To przeoczenie zostało poprawione w 2016 tak, aby B<tput> współdzielił swoją "
"logikę z B<\\%clear> i B<\\%tset>."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<\\%tput>(1), B<\\%xterm>(1), B<\\%terminfo>(5)"
msgstr "B<\\%tput>(1), B<\\%xterm>(1), B<\\%terminfo>(5)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"B<clear> clears your screen if this is possible, including its scrollback "
"buffer (if the extended \\*(``E3\\*('' capability is defined).  B<clear> "
"looks in the environment for the terminal type given by the environment "
"variable B<TERM>, and then in the B<terminfo> database to determine how to "
"clear the screen."
msgstr ""
"B<clear> czyści ekran terminala (o ile to możliwe) w tym bufor przewijania "
"do tyłu (jeśli zdefiniowano rozszerzoną funkcję \\[Bq]E3\\[rq]). Sprawdza on "
"w środowisku typ terminala za pomocą zmiennej środowiskowej B<TERM>, a potem "
"w bazie terminfo szuka sposobu na wyczyszczenie ekranu."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In June 1999, xterm provided an extension to the standard control sequence "
"for clearing the screen.  Rather than clearing just the visible part of the "
"screen using"
msgstr ""
"W czerwcu 1999, xterm dostarczył rozszerzenie do standardowej sekwencji "
"czyszczącej ekran. Zamiast czyścić tylko widoczną część ekranu używając"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"This is documented in I<XTerm Control Sequences> as a feature originating "
"with xterm."
msgstr ""
"Jest to udokumentowane w I<XTerm Control Sequences> jako funkcjonalność "
"wprowadzona w xterm."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"In April 2011, a Red Hat developer submitted a patch to the Linux kernel, "
"modifying its console driver to do the same thing.  The Linux change, part "
"of the 3.0 release, did not mention xterm, although it was cited in the Red "
"Hat bug report (#683733)  which led to the change."
msgstr ""
"W kwietniu 2011, deweloper Red Hat wypuścił łatkę do jądra Linuxa, "
"modyfikującą jego sterownik konsoli by wykorzystywał tą samą funkcjonalność. "
"Zmiany w Linuksie, część wydania 3.0, nie zawierały xterm, jednakże zostało "
"to przytoczone w raporcie błędu Red Hat (#683733), co doprowadziło do zmiany."

#. type: Plain text
#: opensuse-leap-15-6
msgid "B<tput>(1), B<terminfo>(\\*n)"
msgstr "B<tput>(1), B<terminfo>(\\*n)"

#. type: Plain text
#: opensuse-leap-15-6
msgid "This describes B<ncurses> version 6.1 (patch 20180317)."
msgstr "Podręcznik odnosi się do wersji 6.1 B<ncurses> (łatka 20180317)."
