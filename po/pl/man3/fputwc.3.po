# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2014, 2017, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2024-03-01 16:56+0100\n"
"PO-Revision-Date: 2019-08-16 21:10+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fputwc"
msgstr "fputwc"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 października 2023 r."

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "fputwc, putwc - write a wide character to a FILE stream"
msgstr "fputwc, putwc - zapisanie szerokiego znaku do strumienia FILE"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<wint_t fputwc(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwc(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
msgstr ""
"B<wint_t fputwc(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwc(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<fputwc>()  function is the wide-character equivalent of the "
"B<fputc>(3)  function.  It writes the wide character I<wc> to I<stream>.  If "
"I<ferror(stream)> becomes true, it returns B<WEOF>.  If a wide-character "
"conversion error occurs, it sets I<errno> to B<EILSEQ> and returns B<WEOF>.  "
"Otherwise, it returns I<wc>."
msgstr ""
"Funkcja B<fputwc>() jest szerokoznakowym odpowiednikiem funkcji B<fputc>(3). "
"Zapisuje szeroki znak I<wc> do I<stream>. Gdy I<ferror(stream)> stanie się "
"prawdziwe, to zwraca B<WEOF>. Jeśli wystąpi błąd konwersji szerokiego znaku, "
"ustawia I<errno> na B<EILSEQ> i również zwraca B<WEOF>. W pozostałych "
"przypadkach zwraca I<wc>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<putwc>()  function or macro functions identically to B<fputwc>().  It "
"may be implemented as a macro, and may evaluate its argument more than "
"once.  There is no reason ever to use it."
msgstr ""
"Funkcja lub makro B<putwc>() zachowuje się identycznie jak B<fgetwc>(). Może "
"być  zaimplementowane jako makro, więc może wskutek tego rozwijać swój "
"argument więcej niż raz. Nie ma żadnego powodu, aby tej funkcji/makra używać."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "For nonlocking counterparts, see B<unlocked_stdio>(3)."
msgstr ""
"Informacje o nieblokujących odpowiednikach znajdują się w "
"B<unlocked_stdio>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<fputwc>()  function returns I<wc> if no error occurred, or B<WEOF> "
#| "to indicate an error.  In the event of an error, I<errno> is set to "
#| "indicate the cause."
msgid ""
"On success, B<fputwc>()  function returns I<wc>.  Otherwise, B<WEOF> is "
"returned, and I<errno> is set to indicate the error."
msgstr ""
"Funkcja B<fputwc>() zwraca I<wc>, jeśli nie wystąpił żaden błąd, lub B<WEOF> "
"w przeciwnym wypadku. W przypadku błędu ustawiane jest I<errno>, wskazując "
"na przyczynę błędu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Apart from the usual ones, there is"
msgstr "Oprócz zwykłych jest jeszcze"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EILSEQ>"
msgstr "B<EILSEQ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Conversion of I<wc> to the stream's encoding fails."
msgstr ""
"Konwersja I<wc> do kodowania stosowanego w strumieniu nie powiodła się."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRYBUTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Informacje o pojęciach używanych w tym rozdziale można znaleźć w podręczniku "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfejs"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atrybut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wartość"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<fputwc>(),\n"
"B<putwc>()"
msgstr ""
"B<fputwc>(),\n"
"B<putwc>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Bezpieczeństwo wątkowe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-bezpieczne"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The behavior of B<fputwc>()  depends on the B<LC_CTYPE> category of the "
"current locale."
msgstr ""
"Zachowanie B<fputwc>() zależy od kategorii B<LC_CTYPE> bieżących ustawień "
"regionalnych."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the absence of additional information passed to the B<fopen>(3)  call, it "
"is reasonable to expect that B<fputwc>()  will actually write the multibyte "
"sequence corresponding to the wide character I<wc>."
msgstr ""
"W przypadku nieprzekazania dodatkowych informacji przy wywołaniu B<fopen>(3) "
"rozsądne jest oczekiwanie, że B<fputwc>() faktycznie zapisze wielobajtową "
"sekwencję odpowiadającą szerokiemu znakowi I<wc>. "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fgetwc>(3), B<fputws>(3), B<unlocked_stdio>(3)"
msgstr "B<fgetwc>(3), B<fputws>(3), B<unlocked_stdio>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr "15 grudnia 2022 r."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 lipca 2023 r."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
