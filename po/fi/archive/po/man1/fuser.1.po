# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joose Vettenranta <joose@iki.fi>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-09-24 11:22+0200\n"
"PO-Revision-Date: 1998-04-08 15:46+0200\n"
"Last-Translator: Joose Vettenranta <joose@iki.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: fuser.1:1
#, no-wrap
msgid "FUSER"
msgstr "FUSER"

#. type: TH
#: fuser.1:1
#, no-wrap
msgid "Feb 3, 1998"
msgstr "3. helmikuuta 1998"

#. type: TH
#: fuser.1:1
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: fuser.1:1
#, no-wrap
msgid "User Commands"
msgstr "Käyttäjän sovellusohjelmat"

#. type: SH
#: fuser.1:2
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: fuser.1:4
msgid "fuser - identify processes using files or sockets"
msgstr "fuser - Tunnistaa prosesseja, jotka käyttävät tiedostoja"

#. type: SH
#: fuser.1:4
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: fuser.1:17
msgid ""
"B<fuser> [B<-a>|B<-s>] [B<-n\\ >I<space>] [B<->I<signal>] [B<-kmuv>] "
"I<name ...> [B<->] [B<-n\\ >I<space>] [B<->I<signal>] [B<-kmuv>] I<name ...>"
msgstr ""
"B<fuser> [B<-a>|B<-s>] [B<-n\\ >I<nimiavaruus>] [B<->I<signaali>] [B<-kmuv>] "
"I<tiedoston nimi ...> [B<->] [B<-n\\ >I<nimiavaruus>] [B<->I<signaali>] [B<-"
"kmuv>] I<tiedoston nimi ...>"

#. type: Plain text
#: fuser.1:20
msgid "B<fuser> -l"
msgstr "B<fuser> -l"

#. type: Plain text
#: fuser.1:23
msgid "B<fuser> -V"
msgstr "B<fuser> -V"

#. type: SH
#: fuser.1:24
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: fuser.1:29
msgid ""
"B<fuser> displays the PIDs of processes using the specified files or file "
"systems.  In the default display mode, each file name is followed by a letter "
"denoting the type of access:"
msgstr ""
"B<fuser> näyttää sen prosessin PID:n, joka käyttää jotain tiettyä tiedostoa "
"tai tiedostojärjestelmää. Oletus näyttömoodissa jokaista tiedoston nimeä "
"seuraa kirjain merkitsee oikeuden tyyppiä:"

#. type: IP
#: fuser.1:30
#, no-wrap
msgid "B<c>"
msgstr "B<c>"

#. type: Plain text
#: fuser.1:32
msgid "current directory."
msgstr "Nykyinen hakemisto."

#. type: IP
#: fuser.1:32
#, no-wrap
msgid "B<e>"
msgstr "B<e>"

#. type: Plain text
#: fuser.1:34
msgid "executable being run."
msgstr "ajettava ohjelma ajossa."

#. type: IP
#: fuser.1:34
#, no-wrap
msgid "B<f>"
msgstr "B<f>"

#. type: Plain text
#: fuser.1:36
msgid "open file. B<f> is omitted in default display mode."
msgstr "aukinainen tiedosto. B<f>:ää ei mainita oletusnäyttömoodissa."

#. type: IP
#: fuser.1:36
#, no-wrap
msgid "B<r>"
msgstr "B<r>"

#. type: Plain text
#: fuser.1:38
msgid "root directory."
msgstr "juuri hakemisto."

#. type: IP
#: fuser.1:38
#, no-wrap
msgid "B<m>"
msgstr "B<m>"

#. type: Plain text
#: fuser.1:40
msgid "mmap'ed file or shared library."
msgstr "muistimapitettu tiedosto tai jaettu kirjasto."

#. type: Plain text
#: fuser.1:45
msgid ""
"B<fuser> returns a non-zero return code if none of the specified files is "
"accessed or in case of a fatal error. If at least one access has been found, "
"B<fuser> returns zero."
msgstr ""
"B<fuser> palauttaa ei-nolla paluu koodin, jos mikään mainituista tiedostoista "
"on käytössä tai vakavan virheen sattuessa. Mikäli vähintään yksi käytössä "
"oleva tiedosto on löytynyt, B<fuser> palauttaa nollan."

#. type: Plain text
#: fuser.1:50
msgid ""
"In order to look up processes using TCP and UDP sockets, the corresponding "
"name space has to be selected with the B<-n> option. Then the socket(s) can "
"be specified by the local and remote port, and the remote address. All fields "
"are optional, but commas in front of missing fields must be present:"
msgstr ""

#. type: Plain text
#: fuser.1:52
msgid "B<[>I<lcl_port>B<][>,B<[>I<rmt_host>B<][>,B<[>I<rmt_port>B<]]]>"
msgstr ""

#. type: Plain text
#: fuser.1:55
msgid ""
"Either symbolic or numeric values can be used for IP addresses and port "
"numbers."
msgstr ""

#. type: SH
#: fuser.1:55
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIOT"

#. type: IP
#: fuser.1:56
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: fuser.1:59
msgid ""
"Show all files specified on the command line. By default, only files that are "
"accessed by at least one process are shown."
msgstr ""
"Näyttää kaikki tiedostot, jotka ovat määritelty komentorivillä. Oletuksena "
"vain tiedostot, jotka ovat käytössä vähintään yhden prosessin, näytetään."

#. type: IP
#: fuser.1:59
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: fuser.1:64
msgid ""
"Kill processes accessing the file. Unless changed with B<->I<signal>, SIGKILL "
"is sent. An B<fuser> process never kills itself, but may kill other B<fuser> "
"processes. The effective user ID of the process executing B<fuser> is set to "
"its real user ID before attempting to kill."
msgstr ""
"Tappaa prosessin, joka käyttää tiedostoa. Mikäli ei muuteta B<->I<signaali>, "
"SIGKILL lähetetään. Itse B<fuser> prosessi ei koskaan tapa itseään, mutta voi "
"tappaa muita fuser prosesseja."

#. type: IP
#: fuser.1:64
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: fuser.1:66
msgid "List all known signal names."
msgstr "Listaa kaikki tunnetut signaalien nimet."

#. type: IP
#: fuser.1:66
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: fuser.1:72
msgid ""
"I<name> specifies a file on a mounted file system or a block device that is "
"mounted. All processes accessing files on that file system are listed.  If a "
"directory file is specified, it is automatically changed to I<name>/. to use "
"any file system that might be mounted on that directory."
msgstr ""
"I<tiedoston nimi> määrittelee tiedoston tai liitetyn tiedostojärjestelmän tai "
"block devicen, mikä on liitetty. Kaikki prosessit jotka käyttävät tiedostoja "
"ko.  tiedostojärjestelmässä, näytetään. Jos hakemisto on määritelty, "
"muutetaan se automaattisesti I<tiedoston nimeksi>/. käyttääkseen mitä tahansa "
"tiedostojärjestelmää, mikä voisi olla liitettynä hakemistoon."

#. type: IP
#: fuser.1:72
#, no-wrap
msgid "B<-n\\ >I<space>"
msgstr "B<-n\\ >I<nimiavaruus>"

#. type: Plain text
#: fuser.1:78
msgid ""
"Select a different name space. The name spaces B<file> (file names, the "
"default), B<udp> (local UDP ports), and B<tcp> (local TCP ports) are "
"supported.  For ports, either the port number or the symbolic name can be "
"specified. If there is no ambiguity, the shortcut notation I<name>B</"
">I<space>B< (e.g. >I<name>B</>I<proto>B<) can be used.>"
msgstr ""
"Valitsee toisen nimiavaruuden. Nimiavaruudet: B<tiedoston nimi> (tiedoston "
"nimi, oletus), B<udp> (paikallinen UDP portti) sekä B<tcp> (paikallinen TCP "
"portti) ovat tuettuja. Porteille, joko portinnumero tai nimi voidaan "
"määrittää. Nimiavaruuksille B<udp> sekä B<tcp> oikopolun merkintää "
"I<portti>B</>I<protokolla>B< voidaan käyttää.>"

#. type: IP
#: fuser.1:78
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: fuser.1:80
msgid "Silent operation. B<-a>, B<-u> and B<-v> are ignored in this mode."
msgstr ""
"Hiljainen operaatio. B<-a>, B<-u> and B<-v> ovat syrjäytetty tässä tilassa."

#. type: IP
#: fuser.1:80
#, no-wrap
msgid "B<->I<signal>"
msgstr "B<->I<signaali>"

#. type: Plain text
#: fuser.1:84
msgid ""
"Use the specified signal instead of SIGKILL when killing processes. Signals "
"can be specified either by name (e.g. B<-HUP>) or by number (e.g. B<-1>)."
msgstr ""
"Käytä määrättyä signaalia SIGKILL:n tilalla, kun prosesseja tapetaan.  "
"Signaalit voidaan määrittää nimen (esim. B<-HUP>) tai numeron (esim.  B<-1>) "
"perusteella."

#. type: IP
#: fuser.1:84
#, no-wrap
msgid "B<-u>"
msgstr "B<-u>"

#. type: Plain text
#: fuser.1:86
msgid "Append the user name of the process owner to each PID."
msgstr "Lisää käyttäjän nimen jokaisen prosessin omistajan PID:n eteen."

#. type: IP
#: fuser.1:86
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: fuser.1:91
#, fuzzy
msgid ""
"Verbose mode. Processes are shown in a B<ps>-like style. The fields PID, USER "
"and COMMAND are similar to B<ps>. ACCESS shows how the process accesses the "
"file. If the access is by the kernel (e.g. in the case of a mount point, a "
"swap file, etc.), B<kernel> is shown instead of the PID."
msgstr ""
"Monisanainen tila. Prosessit näytetään B<ps>-tyylillä. Kentät PID, USER ja "
"COMMAND ovat samankaltaisia kuin B<ps:llä>. ACCESS näyttää kuinka monta "
"prosessia käyttää tiedostoa."

#. type: IP
#: fuser.1:91
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: fuser.1:93
msgid "Display version information."
msgstr "Näyttää versio tiedon."

#. type: IP
#: fuser.1:93
#, no-wrap
msgid "B<->"
msgstr "B<->"

#. type: Plain text
#: fuser.1:95
msgid "Reset all options and set the signal back to SIGKILL."
msgstr ""
"Uudelleen asettaa kaikki optiot ja palauttaa signaalin takaisin SIGKILL:ksi."

#. type: SH
#: fuser.1:95
#, no-wrap
msgid "FILES"
msgstr "TIEDOSTOT"

#. type: Plain text
#: fuser.1:98
#, no-wrap
msgid "/proc\tlocation of the proc file system\n"
msgstr "/proc\tproc-tiedostojärjestelmän sijainti\n"

#. type: SH
#: fuser.1:99
#, no-wrap
msgid "EXAMPLES"
msgstr "ESIMERKKEJÄ"

#. type: Plain text
#: fuser.1:102
msgid ""
"B<fuser -km /home> kills all processes accessing the file system /home in any "
"way."
msgstr ""
"B<fuser -km /home> tappaa kaikki prosessit, jotka käyttävät "
"tiedostojärjestelmää /home jotenkin."

#. type: Plain text
#: fuser.1:105
msgid ""
"B<if fuser -s /dev/ttyS1; then :; else >I<something>B<; fi> invokes "
"I<something> if no other process is using /dev/ttyS1."
msgstr ""
"B<if fuser -s /dev/ttyS1; then :; else >I<jonkin>B<; fi> käynnistää I<jonkin> "
"mikäli mikään muu prosessi ei käytä /dev/ttyS1:stä."

#. type: Plain text
#: fuser.1:107
msgid "B<fuser telnet/tcp> shows all processes at the (local) TELNET port."
msgstr ""
"B<fuser telnet/tcp> näyttää kaikki prosessit paikallisessa TELNET portissa."

#. type: SH
#: fuser.1:107
#, no-wrap
msgid "RESTRICTIONS"
msgstr "RAJOITUKSET"

#. type: Plain text
#: fuser.1:110
msgid ""
"Processes accessing the same file or file system several times in the same "
"way are only shown once."
msgstr ""
"Prosessit jotka käyttävät samaa tiedostoa tai tiedostojärjestelmää usean "
"kerran samalla lailla, näytetään vain kerran."

#. type: Plain text
#: fuser.1:113
msgid ""
"If the same object is specified several times on the command line, some of "
"those entries may be ignored."
msgstr ""
"Jos sama kohde on määritelty useasti komentorivilla, jotkut niistä voi "
"mahdollisesti tulla syrjäytetyksi."

#. type: Plain text
#: fuser.1:117
msgid ""
"B<fuser> may only be able to gather partial information unless run with "
"privileges. As a consequence, files opened by processes belonging to other "
"users may not be listed and executables may be classified as mapped only."
msgstr ""
"B<fuser> voi kerätä vain osittaista tietoa, mikäli sitä ei ajeta oikeuksilla. "
"Seuraukseni, tiedostoja avannut prosessi, mikä kuuluu toiselle käyttäjälle ei "
"voida näyttää ja ajettavat tiedostot voidaan luokitella vain mapitetuksi."

#. type: Plain text
#: fuser.1:121
msgid ""
"Installing B<fuser> SUID root will avoid problems associated with partial "
"information, but may be undesirable for security and privacy reasons."
msgstr ""
"B<fuser> asentaminen pääkäyttäjän oikeudelle tuo kaiken tiedon näkyviin, "
"mutta voi olla ei-toivottua turvallisuus ja yksityisyys syistä."

#. type: Plain text
#: fuser.1:124
msgid ""
"B<udp> and B<tcp> name spaces, and UNIX domain sockets can't be searched with "
"kernels older than 1.3.78."
msgstr ""
"B<udp> ja B<tcp> nimiavaruutta, ja socket:a ei voida etsiä kerneleille, mitkä "
"ovat vanhempia kuin 1.3.78."

#. type: Plain text
#: fuser.1:126
msgid "B<udp> and B<tcp> currently only work for IPv4."
msgstr ""

#. type: Plain text
#: fuser.1:128
msgid "Accesses by the kernel are only shown with the B<-v> option."
msgstr ""

#. type: Plain text
#: fuser.1:131
msgid ""
"The B<-k> option only works on processes. If the user is the kernel, B<fuser> "
"will print an advice, but take no action beyond that."
msgstr ""

#. type: SH
#: fuser.1:131
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: fuser.1:133
msgid "Werner Almesberger E<lt>werner.almesberger@lrc.di.epfl.chE<gt>"
msgstr "Werner Almesberger E<lt>werner.almesberger@lrc.di.epfl.chE<gt>"

#. type: SH
#: fuser.1:133
#, no-wrap
msgid "SEE ALSO"
msgstr "KATSO MYÖS"

#. type: Plain text
#: fuser.1:134
msgid "kill(1), killall(1), ps(1), kill(2)"
msgstr "B<kill>(1), B<killall>(1), B<ps>(1), B<kill>(2)"
