# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# carmie
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2000.
# Éric Piel <eric.piel@tremplin-utc.net>, 2005.
# Gérard Delafond <gerard@delafond.org>, 2001, 2003.
# Danny <dannybrain@noos.fr>, 2001.
# Christophe Blaess <ccb@club-internet.fr>, 1997.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000-2002.
# François Wendling <frwendling@free.fr>, 2005.
# Philippe Batailler, 2000.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# David Prévot <david@tilapin.org>, 2010-2014.
# Romain Doumenc <rd6137@gmail.com>, 2011.
# Thomas Vincent <tvincent@debian.org>, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra\n"
"POT-Creation-Date: 2023-06-27 19:27+0200\n"
"PO-Revision-Date: 2014-03-20 18:34+0100\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FSCK.NFS"
msgstr "FSCK.NFS"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "May 2004"
msgstr "mai 2004"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Initscripts"
msgstr "Scripts de démarrage"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "fsck.nfs - Dummy fsck.nfs script that always returns success."
msgstr "fsck.nfs - Script fsck.nfs vide et n'échouant jamais"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<fsck.nfs>"
msgstr "B<fsck.nfs>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Debian GNU/Linux need this for when the root file system is on NFS: there is "
"no way to find out if root is NFS mounted and we really want to do a \"fsck -"
"a /\"."
msgstr ""
"A été ajouté à Debian GNU/Linux pour le cas où le système de fichiers racine "
"est de type NFS : il n'y a aucun moyen de deviner le système de fichiers et "
"il est important de toujours faire un « fsck -a »."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "EXIT CODE"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The exit code returned by B<mount.nfs> is always zero, meaning successful "
"completion."
msgstr ""
"Le code de sortie retourné par B<mount.nfs> est toujours zéro, indiquant que "
"tout s'est bien passé."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<fsck>(8), B<fstab>(5)."
msgstr "B<fsck>(8), B<fstab>(5)"
