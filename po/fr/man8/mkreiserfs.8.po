# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Guillaume Bour, 2002.
# Nicolas François <nicolas.francois@centraliens.net>, 2009.
# David Prévot <david@tilapin.org>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-reiserfsprogs\n"
"POT-Creation-Date: 2023-08-27 17:08+0200\n"
"PO-Revision-Date: 2013-12-31 13:15-0400\n"
"Last-Translator: Nicolas François <nicolas.francois@centraliens.net>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "MKREISERFS"
msgstr "MKREISERFS"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "January 2009"
msgstr "Janvier 2009"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "Reiserfsprogs-3.6.27"
msgstr "Reiserfsprogs-3.6.27"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "mkreiserfs - The create tool for the Linux ReiserFS filesystem."
msgstr "mkreiserfs - Créer une système de fichiers ReiserFS pour Linux"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<mkreiserfs> [ B<-dfV> ] [ B<-b> | B<--block-size >I<N> ] [ B<-h> | B<--"
"hash >I<HASH> ] [ B<-u> | B<--uuid >I<UUID> ] [ B<-l> | B<--label "
">I<LABEL> ] [ B<--format >I<FORMAT> ] [ B<-q> | B<--quiet> ] [ B<-j> | B<--"
"journal-device >I<FILE> ] [ B<-s> | B<--journal-size >I<N> ] [ B<-o> | B<--"
"journal-offset >I<N> ] [ B<-t> | B<--transaction-max-size> I<N> ] [ B<-B> | "
"B<--badblocks> I<file> ] I< device> [ I<filesystem-size> ]"
msgstr ""
"B<mkreiserfs> [ B<-dfV> ] [ B<-b> | B<--block-size >I<N> ] [ B<-h> | B<--"
"hash >I<HACHAGE> ] [ B<-u> | B<--uuid >I<UUID> ] [ B<-l> | B<--label "
">I<NOM> ] [ B<--format >I<FORMAT> ] [ B<-q> | B<--quiet> ] [ B<-j> | B<--"
"journal-device >I<FICHIER> ] [ B<-s> | B<--journal-size >I<N> ] [ B<-o> | "
"B<--journal-offset >I<N> ] [ B<-t> | B<--transaction-max-size> I<N> ] [ B<-"
"B> | B<--badblocks> I<fichier> ] I< périphérique> [ I<taille-du-système-de-"
"fichiers> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"B<mkreiserfs> creates a Linux ReiserFS filesystem on a device (usually a "
"disk partition)."
msgstr ""
"B<mkreiserfs> crée un système de fichiers ReiserFS pour Linux sur un "
"périphérique (habituellement la partition d'un disque dur). "

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "I<device>"
msgstr "I<périphérique>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"is the special file corresponding to a device or to a partition (e.g /dev/"
"hdXX for an IDE disk partition or /dev/sdXX for a SCSI disk partition)."
msgstr ""
"fichier spécial correspondant à un périphérique ou une partition (par "
"exemple /dev/hdXX, correspondant à une partition d'un disque IDE ou /dev/"
"sdXX pour celle d'un disque SCSI)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "I<filesystem-size>"
msgstr "I<taille-du-système-de-fichiers>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"is the size in blocks of the filesystem. If omitted, B<mkreiserfs> will "
"automatically set it."
msgstr ""
"taille du système de fichiers en blocs. Par défaut, B<mkreiserfs> la "
"déterminera automatiquement."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-b> | B<--block-size >I<N>"
msgstr "B<-b> | B<--block-size >I<N>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<N> is block size in bytes. It may only be set to a power of 2 within the "
"512-8192 interval."
msgstr ""
"N est la taille d'un bloc en octets. Cette valeur doit être une puissance de "
"deux dans l'intervalle 512-8192."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-h> | B<--hash >I<HASH>"
msgstr "B<-h> | B<--hash >I<HACHAGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<HASH> specifies which hash function will sort the names in the "
"directories.  Choose from r5, rupasov, or tea. r5 is the default one."
msgstr ""
"I<HACHAGE> indique la fonction de hachage qui sera utilisée pour trier les "
"noms de fichiers dans les répertoires. À choisir parmi r5, rupasov ou tea. "
"r5 est la valeur par défaut."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<--format >I<FORMAT>"
msgstr "B<--format >I<FORMAT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<FORMAT> specifies the format for the new filsystem. Choose format 3.5 or "
"3.6. If none is specified B<mkreiserfs> will create format 3.6 if running "
"kernel is 2.4 or higher, and format 3.5 if kernel 2.2 is running, and will "
"refuse creation under all other kernels."
msgstr ""
"FORMAT indique le format du nouveau système de fichiers. À choisir entre 3.5 "
"et 3.6. Par défaut, B<mkreiserfs> utilisera le format 3.6 si vous utilisez "
"un noyau Linux 2.4 ou ultérieur, le format 3.5 pour un noyau 2.2 et il "
"refusera de créer le système de fichiers pour toute autre version du noyau."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-u> | B<--uuid >I<UUID>"
msgstr "B<-u> | B<--uuid >I<UUID>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Sets the Universally Unique IDentifier of the filesystem to I<UUID> (see "
"also B<uuidgen(8)>).  The format of the I<UUID> is a series of hex digits "
"separated by hypthens, e.g.: \"c1b9d5a2-f162-11cf-9ece-0020afc76f16\".  If "
"the option is skipped, B<mkreiserfs> will by default generate a new I<UUID>."
msgstr ""
"Définit l'identifiant unique universel (« Universally Unique IDentifier », "
"ou UUID) du système de fichiers à UUID (voir aussi B<uuidgen>(8)). L'I<UUID> "
"est composé d'une série de chiffres hexadécimaux séparés par des tirets, "
"comme par exemple :« c1b9d5a2-f162-11cf-9ece-0020afc76f16 ». Par défaut "
"quand cette option n'et pas précisée, B<mkreiserfs> génère automatiquement "
"un nouvel identifiant I<UUID>."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-l> | B<--label >I<LABEL>"
msgstr "B<-l> | B<--label >I<NOM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Sets the volume label of the filesystem. I<LABEL> can at most be 16 "
"characters long; if it is longer than 16 characters, B<mkreiserfs> will "
"truncate it."
msgstr ""
"Définit le nom de volume du système de fichiers. I<NOM> fait au plus 16 "
"caractères de long. S'il est plus long, B<mkreiserfs> le tronquera "
"automatiquement."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-q> | B<--quiet >"
msgstr "B<-q> | B<--quiet >"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Sets B<mkreiserfs> to work quietly without producing messages, progress or "
"questions. It is useful, but only for use by end users, if you run "
"B<mkreiserfs> in a script."
msgstr ""
"Demande à B<mkreiserfs>  de travailler silencieusement sans produire de "
"messages, d'indications de progression ou de questions. C'est utile, mais "
"seulement par les utilisateurs finaux, si vous exécutez B<mkreiserfs> dans "
"un script."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-j> | B<--journal-device >I<FILE>"
msgstr "B<-j> | B<--journal-device >I<FICHIER>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<FILE> is the name of the block device on which is to be places the "
"filesystem journal."
msgstr ""
"I<FICHIER> est le nom du périphérique bloc destiné à contenir le journal du "
"système de fichiers."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-o> | B<--journal-offset >I<N>"
msgstr "B<-o> | B<--journal-offset >I<N>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<N> is the offset where the journal starts when it is to be on a separate "
"device. Default is 0. I<N> has no effect when the journal is to be on the "
"host device."
msgstr ""
"I<N> est le décalage à partir duquel commence le journal quand il se trouve "
"sur un périphérique différent. La valeur par défaut est 0. Cette option n'a "
"aucun effet quand le journal se situe sur le même périphérique que le "
"système de fichiers."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-s> | B<--journal-size >I<N>"
msgstr "B<-s> | B<--journal-size >I<N>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<N> is the size of the journal in blocks. When the journal is to be on a "
"separate device, its size defaults to the number of blocks that the device "
"has.  When journal is to be on the host device, its size defaults to 8193 "
"and the maximal possible size is 32749 (for blocksize 4k). The minimum size "
"is 513 blocks (whether the journal is on the host or on a separate device)."
msgstr ""
"I<N> indique la taille du journal en nombre de blocs. Quand le journal est "
"situé sur un périphérique différent de celui du système de fichiers, sa "
"taille par défaut est celle du périphérique. Quand le journal est sur le "
"même périphérique, sa taille par défaut est de 8193 blocs et sa taille "
"maximale possible est de 32749 (pour des blocs de 4 k). Sa taille minimale "
"est de 513 blocs (que le journal se trouve sur le même périphérique que le "
"système de fichiers ou non)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-t> | B<--transaction-max-size >I<N>"
msgstr "B<-t> | B<--transaction-max-size >I<N>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<N> is the maximum transaction size parameter for the journal. The default, "
"and max possible, value is 1024 blocks. It should be less than half the size "
"of the journal. If specified incorrectly, it will automatically be adjusted."
msgstr ""
"I<N> indique la taille maximale d'une transaction pour le journal. La valeur "
"par défaut, également valeur maximale, est de 1024 blocs. Cette valeur "
"devrait être de moins de la moitié de la taille du journal. Si la valeur est "
"incorrectement spécifiée, elle sera modifiée automatiquement."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-B> | B<--badblocks >I<file>"
msgstr "B<-B> | B<--badblocks >I<fichier>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"I<File> is the file name of the file that contains the list of blocks to be "
"marked as bad on the filesystem. This list can be created by B</sbin/"
"badblocks -b block-size device>."
msgstr ""
"I<fichier> est le nom du fichier qui contient la liste des blocs marqués "
"comme défectueux sur le système de fichiers. Cette liste peut être créée "
"avec B</sbin/badblocks -b taille_bloc périphérique>."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Forces B<mkreiserfs> to continue even when the device is the whole disk, "
"looks mounted, or is not a block device. If B<-f> is specified more than "
"once, it allows the user to avoid asking for confirmation."
msgstr ""
"Cette option force B<mkreiserfs> à continuer même si le périphérique est un "
"disque entier qui paraît monté ou n'est pas un périphérique en mode bloc. "
"Spécifier l'option B<-f> plus d'une fois permet de supprimer la demande de "
"confirmation."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Sets B<mkreiserfs> to print debugging information during B<mkreiserfs>."
msgstr ""
"Indique à B<mkreiserfs> d'afficher les informations de débogage pendant son "
"exécution."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "B<-V>"
msgstr "B<-V>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "Prints the version and then exits."
msgstr "Affiche la version, puis quitte."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"This version of B<mkreiserfs> has been written by Edward Shishkin "
"E<lt>edward@namesys.comE<gt>."
msgstr ""
"Cette version de B<mkreiserfs> a été écrite par Edward Shishkin "
"E<lt>edward@namesys.comE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid ""
"Please report bugs to the ReiserFS developers E<lt>reiserfs-devel@vger."
"kernel.orgE<gt>, providing as much information as possible--your hardware, "
"kernel, patches, settings, all printed messages; check the syslog file for "
"any related information."
msgstr ""
"Veuillez envoyer vos rapports de bogue aux développeurs ReiserFS "
"E<lt>reiserfs-devel@vger.kernel.orgE<gt>, en fournissant autant "
"d'informations que possibles : le matériel, le noyau, les patches, la "
"configuration, tous les messages affichés ; vérifiez si le fichier syslog "
"contient des informations qui sembles liées."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-6
msgid "B<reiserfsck>(8), B<debugreiserfs>(8), B<reiserfstune>(8)"
msgstr "B<reiserfsck>(8), B<debugreiserfs>(8), B<reiserfstune>(8)"
