# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Cédric Boutillier <cedric.boutillier@gmail.com>, 2011, 2012.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023-2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-03-01 16:54+0100\n"
"PO-Revision-Date: 2024-03-04 12:18+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CPU_SET"
msgstr "CPU_SET"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"CPU_SET, CPU_CLR, CPU_ISSET, CPU_ZERO, CPU_COUNT, CPU_AND, CPU_OR, CPU_XOR, "
"CPU_EQUAL, CPU_ALLOC, CPU_ALLOC_SIZE, CPU_FREE, CPU_SET_S, CPU_CLR_S, "
"CPU_ISSET_S, CPU_ZERO_S, CPU_COUNT_S, CPU_AND_S, CPU_OR_S, CPU_XOR_S, "
"CPU_EQUAL_S - macros for manipulating CPU sets"
msgstr ""
"CPU_SET, CPU_CLR, CPU_ISSET, CPU_ZERO, CPU_COUNT, CPU_AND, CPU_OR, CPU_XOR, "
"CPU_EQUAL, CPU_ALLOC, CPU_ALLOC_SIZE, CPU_FREE, CPU_SET_S, CPU_CLR_S, "
"CPU_ISSET_S, CPU_ZERO_S, CPU_COUNT_S, CPU_AND_S, CPU_OR_S, CPU_XOR_S, "
"CPU_EQUAL_S - macros de manipulation d'un «\\ ensemble de CPUs\\ »"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>sched.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* Consultez feature_test_macros(7) */\n"
"B<#include E<lt>sched.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<void CPU_ZERO(cpu_set_t *>I<set>B<);>\n"
msgstr "B<void CPU_ZERO(cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_SET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
msgstr ""
"B<void CPU_SET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET(int >I<cpu>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_COUNT(cpu_set_t *>I<set>B<);>\n"
msgstr "B<int  CPU_COUNT(cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_AND(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
msgstr ""
"B<void CPU_AND(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR(cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_EQUAL(cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"
msgstr "B<int  CPU_EQUAL(cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<cpu_set_t *CPU_ALLOC(int >I<num_cpus>B<);>\n"
"B<void CPU_FREE(cpu_set_t *>I<set>B<);>\n"
"B<size_t CPU_ALLOC_SIZE(int >I<num_cpus>B<);>\n"
msgstr ""
"B<cpu_set_t *CPU_ALLOC(int >I<num_cpus>B<);>\n"
"B<void CPU_FREE(cpu_set_t *>I<set>B<);>\n"
"B<size_t CPU_ALLOC_SIZE(int >I<num_cpus>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<void CPU_ZERO_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
msgstr "B<void CPU_ZERO_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_SET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
msgstr ""
"B<void CPU_SET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<void CPU_CLR_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
"B<int  CPU_ISSET_S(int >I<cpu>B<, size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_COUNT_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"
msgstr "B<int  CPU_COUNT_S(size_t >I<setsize>B<, cpu_set_t *>I<set>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void CPU_AND_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
msgstr ""
"B<void CPU_AND_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_OR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"
"B<void CPU_XOR_S(size_t >I<setsize>B<, cpu_set_t *>I<destset>B<,>\n"
"B<             cpu_set_t *>I<srcset1>B<, cpu_set_t *>I<srcset2>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int  CPU_EQUAL_S(size_t >I<setsize>B<, cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"
msgstr "B<int  CPU_EQUAL_S(size_t >I<setsize>B<, cpu_set_t *>I<set1>B<, cpu_set_t *>I<set2>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<cpu_set_t> data structure represents a set of CPUs.  CPU sets are used "
"by B<sched_setaffinity>(2)  and similar interfaces."
msgstr ""
"La structure de données I<cpu_set_t> représente un «\\ ensemble de CPUs\\ ». "
"Les «\\ ensembles de CPUs\\ » sont utilisés par B<sched_setaffinity>(2) et "
"les interfaces similaires."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<cpu_set_t> data type is implemented as a bit mask.  However, the data "
"structure should be treated as opaque: all manipulation of CPU sets should "
"be done via the macros described in this page."
msgstr ""
"Le type I<cpu_set_t> est implémenté comme un masque de bits. Cependant, la "
"structure de données traitée est considérée comme opaque\\ : toute "
"manipulation d'un «\\ ensemble de CPU\\ » devrait être effectuée avec les "
"macros décrites dans cette page."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following macros are provided to operate on the CPU set I<set>:"
msgstr ""
"Les macros suivantes sont fournies pour opérer sur l'ensemble I<set>\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ZERO>()"
msgstr "B<CPU_ZERO>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Clears I<set>, so that it contains no CPUs."
msgstr "Mettre à zéro I<set>, ainsi, il ne contient aucun CPU."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_SET>()"
msgstr "B<CPU_SET>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Add CPU I<cpu> to I<set>."
msgstr "Ajouter le CPU I<cpu> à I<set>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_CLR>()"
msgstr "B<CPU_CLR>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Remove CPU I<cpu> from I<set>."
msgstr "Supprimer le CPU I<cpu> de I<set>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ISSET>()"
msgstr "B<CPU_ISSET>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Test to see if CPU I<cpu> is a member of I<set>."
msgstr "Tester si le CPU I<cpu> est un membre de I<set>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_COUNT>()"
msgstr "B<CPU_COUNT>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Return the number of CPUs in I<set>."
msgstr "Renvoyer le nombre de CPU de I<set>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Where a I<cpu> argument is specified, it should not produce side effects, "
"since the above macros may evaluate the argument more than once."
msgstr ""
"Lorsque l'argument I<cpu> est spécifié, il ne devrait pas produire d'effet "
"de bord puisque les macros ci-dessus pourraient évaluer l'argument plus "
"d'une fois."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The first CPU on the system corresponds to a I<cpu> value of 0, the next CPU "
"corresponds to a I<cpu> value of 1, and so on.  No assumptions should be "
"made about particular CPUs being available, or the set of CPUs being "
"contiguous, since CPUs can be taken offline dynamically or be otherwise "
"absent.  The constant B<CPU_SETSIZE> (currently 1024) specifies a value one "
"greater than the maximum CPU number that can be stored in I<cpu_set_t>."
msgstr ""
"Le premier CPU disponible sur un système correspond à la valeur I<cpu> 0, le "
"CPU suivant à la valeur I<cpu> 1 et ainsi de suite. Aucune hypothèse ne "
"devrait être émise sur la disponibilité de CPU particuliers ou sur un "
"ensemble de CPU contigus, dans la mesure où des CPU peuvent être mis hors "
"ligne de façon dynamique ou être absents autrement. La constante "
"B<CPU_SETSIZE> (habituellement 1024) spécifie le nombre maximal de CPU qui "
"peut être enregistré dans I<cpu_set_t>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following macros perform logical operations on CPU sets:"
msgstr ""
"Les macros suivantes réalisent des opérations logiques sur les «\\ ensembles "
"de CPUs\\ »\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_AND>()"
msgstr "B<CPU_AND>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Store the intersection of the sets I<srcset1> and I<srcset2> in I<destset> "
"(which may be one of the source sets)."
msgstr ""
"Enregistre l'intersection (ET logique) des ensembles I<srcset1> et "
"I<srcset2> dans I<destset> (qui peut être un ensemble source)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_OR>()"
msgstr "B<CPU_OR>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Store the union of the sets I<srcset1> and I<srcset2> in I<destset> (which "
"may be one of the source sets)."
msgstr ""
"Enregistre l'union (OU logique) des ensembles I<srcset1> et I<srcset2> dans "
"I<destset> (qui peut être un ensemble source)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_XOR>()"
msgstr "B<CPU_XOR>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Store the XOR of the sets I<srcset1> and I<srcset2> in I<destset> (which may "
"be one of the source sets).  The XOR means the set of CPUs that are in "
"either I<srcset1> or I<srcset2>, but not both."
msgstr ""
"Enregistre le OU EXCLUSIF logique des ensembles I<srcset1> et I<srcset2> "
"dans I<destset> (qui peut être un ensemble source). Le OU EXCLUSIF signifie "
"que les ensembles appartiennent soit à I<srcset1>, soit à I<srcset2>, mais "
"pas aux deux à la fois."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_EQUAL>()"
msgstr "B<CPU_EQUAL>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Test whether two CPU set contain exactly the same CPUs."
msgstr "Tester si deux ensembles de CPUs contiennent les mêmes CPUs."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Dynamically sized CPU sets"
msgstr "Ensemble de CPUs de taille dynamique"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Because some applications may require the ability to dynamically size CPU "
"sets (e.g., to allocate sets larger than that defined by the standard "
"I<cpu_set_t> data type), glibc nowadays provides a set of macros to support "
"this."
msgstr ""
"Certaines applications nécessite des ensembles CPUs de taille dynamique (par "
"exemple, pour allouer des ensembles plus grands que ceux définis avec le "
"type I<cpu_set_t>), la glibc propose aujourd'hui un jeu de macro pour cette "
"fonctionnalité."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following macros are used to allocate and deallocate CPU sets:"
msgstr ""
"Les macros suivantes sont utilisées pour allouer et désallouer des ensembles "
"de CPUs\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ALLOC>()"
msgstr "B<CPU_ALLOC>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Allocate a CPU set large enough to hold CPUs in the range 0 to I<num_cpus-1>."
msgstr "Allouer un ensemble CPUs assez grand pour contenir I<num_cpus-1> CPU."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_ALLOC_SIZE>()"
msgstr "B<CPU_ALLOC_SIZE>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Return the size in bytes of the CPU set that would be needed to hold CPUs in "
"the range 0 to I<num_cpus-1>.  This macro provides the value that can be "
"used for the I<setsize> argument in the B<CPU_*_S>()  macros described below."
msgstr ""
"Renvoie la taille en octets de l'ensemble CPUs nécessaire pour contenir les "
"I<num_cpus-1> cpu. Cette macro fournit la valeur de l'argument I<setsize> "
"des macros B<CPU_*_S>() définies ci-dessous."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<CPU_FREE>()"
msgstr "B<CPU_FREE>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Free a CPU set previously allocated by B<CPU_ALLOC>()."
msgstr "Libérer un ensemble alloué avec B<CPU_ALLOC>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The macros whose names end with \"_S\" are the analogs of the similarly "
"named macros without the suffix.  These macros perform the same tasks as "
"their analogs, but operate on the dynamically allocated CPU set(s) whose "
"size is I<setsize> bytes."
msgstr ""
"Les macros dont le nom se termine par «\\ _S\\ » sont les macros "
"équivalentes aux macros sans «\\ _S\\ » qui opèrent sur les ensembles de "
"taille dynamique de taille I<setsize>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_ISSET>()  and B<CPU_ISSET_S>()  return nonzero if I<cpu> is in I<set>; "
"otherwise, it returns 0."
msgstr ""
"B<CPU_ISSET>()  et B<CPU_ISSET_S>() renvoient une valeur non nulle si I<cpu> "
"est présent dans I<set>, 0 sinon."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_COUNT>()  and B<CPU_COUNT_S>()  return the number of CPUs in I<set>."
msgstr ""
"B<CPU_COUNT>() et B<CPU_COUNT_S>() renvoient le nombre de CPUs présent dans "
"I<set>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_EQUAL>()  and B<CPU_EQUAL_S>()  return nonzero if the two CPU sets are "
"equal; otherwise they return 0."
msgstr ""
"B<CPU_EQUAL>() et B<CPU_EQUAL_S>() renvoient une valeur non nulle si les "
"deux ensembles de CPU sont égaux, B<0> sinon."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_ALLOC>()  returns a pointer on success, or NULL on failure.  (Errors "
"are as for B<malloc>(3).)"
msgstr ""
"B<CPU_ALLOC>() renvoie un pointeur en cas de succès et NULL en cas d'échec. "
"Les erreurs sont les mêmes que B<malloc>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_ALLOC_SIZE>()  returns the number of bytes required to store a CPU set "
"of the specified cardinality."
msgstr ""
"B<CPU_ALLOC_SIZE>() renvoie le nombre d'octets nécessaire pour sauvegarder "
"un ensemble avec une cardinalité spécifique."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The other functions do not return a value."
msgstr "Les autres fonctions ne renvoient pas de valeur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<CPU_ZERO>(), B<CPU_SET>(), B<CPU_CLR>(), and B<CPU_ISSET>()  macros "
"were added in glibc 2.3.3."
msgstr ""
"Les macros B<CPU_ZERO>(), B<CPU_SET>(), B<CPU_CLR>() et B<CPU_ISSET>() ont "
"été ajoutées dans la glibc\\ 2.3.3."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<CPU_COUNT>()  first appeared in glibc 2.6."
msgstr "B<CPU_COUNT>() est apparue dans le glibc2.6."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<CPU_AND>(), B<CPU_OR>(), B<CPU_XOR>(), B<CPU_EQUAL>(), B<CPU_ALLOC>(), "
"B<CPU_ALLOC_SIZE>(), B<CPU_FREE>(), B<CPU_ZERO_S>(), B<CPU_SET_S>(), "
"B<CPU_CLR_S>(), B<CPU_ISSET_S>(), B<CPU_AND_S>(), B<CPU_OR_S>(), "
"B<CPU_XOR_S>(), and B<CPU_EQUAL_S>()  first appeared in glibc 2.7."
msgstr ""
"B<CPU_AND>(), B<CPU_OR>(), B<CPU_XOR>(), B<CPU_EQUAL>(), B<CPU_ALLOC>(), "
"B<CPU_ALLOC_SIZE>(), B<CPU_FREE>(), B<CPU_ZERO_S>(), B<CPU_SET_S>(), "
"B<CPU_CLR_S>(), B<CPU_ISSET_S>(), B<CPU_AND_S>(), B<CPU_OR_S>(), "
"B<CPU_XOR_S>() et B<CPU_EQUAL_S>() sont apparues en premier dans la glibc\\ "
"2.7."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "To duplicate a CPU set, use B<memcpy>(3)."
msgstr "Pour dupliquer un ensemble, utilisez B<memcpy>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since CPU sets are bit masks allocated in units of long words, the actual "
"number of CPUs in a dynamically allocated CPU set will be rounded up to the "
"next multiple of I<sizeof(unsigned long)>.  An application should consider "
"the contents of these extra bits to be undefined."
msgstr ""
"Comme les ensembles de CPU sont des masques de bits alloués par unité de "
"mots de type I<long>, le nombre actuel de CPU dans un ensemble dynamique "
"doit être arrondi au multiple suivant de I<sizeof(unsigned long)>. Une "
"application doit considérer le contenu de ces bits non utilisés comme "
"indéfinis."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Notwithstanding the similarity in the names, note that the constant "
"B<CPU_SETSIZE> indicates the number of CPUs in the I<cpu_set_t> data type "
"(thus, it is effectively a count of the bits in the bit mask), while the "
"I<setsize> argument of the B<CPU_*_S>()  macros is a size in bytes."
msgstr ""
"Malgré la proximité des noms, notez que la constante B<CPU_SETSIZE> indique "
"le nombre de CPU dans le type de données I<cpu_set_t> (c'est en réalité un "
"comptage de bits dans le masque de bits) alors que l'argument I<setsize> des "
"macros B<CPU_*_S>() est une taille en octets."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The data types for arguments and return values shown in the SYNOPSIS are "
"hints what about is expected in each case.  However, since these interfaces "
"are implemented as macros, the compiler won't necessarily catch all type "
"errors if you violate the suggestions."
msgstr ""
"Les types de données des arguments et des valeurs de retour vues dans le "
"SYNOPSIS sont des suggestions sur ce qui est prévu dans chaque cas. "
"Cependant, puisque ces interfaces sont des macros, le compilateur ne va pas "
"nécessairement attraper toutes les erreurs de type si vous violez ces "
"suggestions."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#.  http://sourceware.org/bugzilla/show_bug.cgi?id=7029
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On 32-bit platforms with glibc 2.8 and earlier, B<CPU_ALLOC>()  allocates "
"twice as much space as is required, and B<CPU_ALLOC_SIZE>()  returns a value "
"twice as large as it should.  This bug should not affect the semantics of a "
"program, but does result in wasted memory and less efficient operation of "
"the macros that operate on dynamically allocated CPU sets.  These bugs are "
"fixed in glibc 2.9."
msgstr ""
"Sur une plate-forme 32\\ bits avec une glibc\\ 2.8 ou plus récente, "
"B<CPU_ALLOC>() alloue deux fois plus d'espace que nécessaire, et "
"B<CPU_ALLOC_SIZE>() renvoie une valeur deux fois plus grande que la valeur "
"attendue. Ce bogue ne devrait pas affecter la sémantique d'un programme mais "
"il provoque une surconsommation mémoire et les macros opérant sur un "
"ensemble dynamique sont moins performantes. Ce bogue est corrigé avec la "
"glibc\\ 2.9."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following program demonstrates the use of some of the macros used for "
"dynamically allocated CPU sets."
msgstr ""
"Le programme suivant est un exemple d'utilisation de macros dans le cas d'un "
"ensemble de CPUs dynamique."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>sched.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    cpu_set_t *cpusetp;\n"
"    size_t size, num_cpus;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>num-cpusE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    num_cpus = atoi(argv[1]);\n"
"\\&\n"
"    cpusetp = CPU_ALLOC(num_cpus);\n"
"    if (cpusetp == NULL) {\n"
"        perror(\"CPU_ALLOC\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    size = CPU_ALLOC_SIZE(num_cpus);\n"
"\\&\n"
"    CPU_ZERO_S(size, cpusetp);\n"
"    for (size_t cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
"        CPU_SET_S(cpu, size, cpusetp);\n"
"\\&\n"
"    printf(\"CPU_COUNT() of set:    %d\\en\", CPU_COUNT_S(size, cpusetp));\n"
"\\&\n"
"    CPU_FREE(cpusetp);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>sched.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#include E<lt>assert.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    cpu_set_t *cpusetp;\n"
"    size_t size, num_cpus;\n"
"\\&\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Utilisation : %s E<lt>num-cpusE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    num_cpus = atoi(argv[1]);\n"
"\\&\n"
"    cpusetp = CPU_ALLOC(num_cpus);\n"
"    if (cpusetp == NULL) {\n"
"        perror(\"CPU_ALLOC\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    size = CPU_ALLOC_SIZE(num_cpus);\n"
"\\&\n"
"    CPU_ZERO_S(size, cpusetp);\n"
"    for (size_t cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
"        CPU_SET_S(cpu, size, cpusetp);\n"
"\\&\n"
"    printf(\"CPU_COUNT() de l'ensemble :    %d\\en\", CPU_COUNT_S(size, cpusetp));\n"
"\\&\n"
"    CPU_FREE(cpusetp);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sched_setaffinity>(2), B<pthread_attr_setaffinity_np>(3), "
"B<pthread_setaffinity_np>(3), B<cpuset>(7)"
msgstr ""
"B<sched_setaffinity>(2), B<pthread_attr_setaffinity_np>(3), "
"B<pthread_setaffinity_np>(3), B<cpuset>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-09"
msgstr "9 octobre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm
msgid "These interfaces are Linux-specific."
msgstr "Ces interfaces sont spécifiques à Linux."

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>sched.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#define _GNU_SOURCE\n"
"#include E<lt>sched.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "#include E<lt>assert.hE<gt>\n"
msgstr "#include E<lt>assert.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    cpu_set_t *cpusetp;\n"
"    size_t size, num_cpus;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    cpu_set_t *cpusetp;\n"
"    size_t size, num_cpus;\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>num-cpusE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Utilisation : %s E<lt>num-cpusE<gt>\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    num_cpus = atoi(argv[1]);\n"
msgstr "    num_cpus = atoi(argv[1]);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    cpusetp = CPU_ALLOC(num_cpus);\n"
"    if (cpusetp == NULL) {\n"
"        perror(\"CPU_ALLOC\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    cpusetp = CPU_ALLOC(num_cpus);\n"
"    if (cpusetp == NULL) {\n"
"        perror(\"CPU_ALLOC\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    size = CPU_ALLOC_SIZE(num_cpus);\n"
msgstr "    size = CPU_ALLOC_SIZE(num_cpus);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    CPU_ZERO_S(size, cpusetp);\n"
"    for (size_t cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
"        CPU_SET_S(cpu, size, cpusetp);\n"
msgstr ""
"    CPU_ZERO_S(size, cpusetp);\n"
"    for (size_t cpu = 0; cpu E<lt> num_cpus; cpu += 2)\n"
"        CPU_SET_S(cpu, size, cpusetp);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    printf(\"CPU_COUNT() of set:    %d\\en\", CPU_COUNT_S(size, cpusetp));\n"
msgstr "    printf(\"CPU_COUNT() de l'ensemble :    %d\\en\", CPU_COUNT_S(size, cpusetp));\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    CPU_FREE(cpusetp);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    CPU_FREE(cpusetp);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-05-03"
msgstr "3 mai 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
