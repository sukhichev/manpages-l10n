# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Frédéric Hantrais <fhantrais@gmail.com>, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-03-01 16:56+0100\n"
"PO-Revision-Date: 2023-03-15 00:23+0100\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "flockfile"
msgstr "flockfile"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "flockfile, ftrylockfile, funlockfile - lock FILE for stdio"
msgstr ""
"flockfile, ftrylockfile, funlockfile - Verrouiller un flux FILE pour stdio"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void flockfile(FILE *>I<filehandle>B<);>\n"
"B<int ftrylockfile(FILE *>I<filehandle>B<);>\n"
"B<void funlockfile(FILE *>I<filehandle>B<);>\n"
msgstr ""
"B<void flockfile(FILE *>I<filehandle>B<);>\n"
"B<int ftrylockfile(FILE *>I<filehandle>B<);>\n"
"B<void funlockfile(FILE *>I<filehandle>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "All functions shown above:"
msgstr "Pour toutes les fonctions ci-dessus :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Since glibc 2.24: */ _POSIX_C_SOURCE E<gt>= 199309L\n"
"        || /* glibc E<lt>= 2.23: */ _POSIX_C_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    /* Depuis la glibc 2.24 : */ _POSIX_C_SOURCE E<gt>= 199309L\n"
"        || /* glibc E<lt>= 2.23 : */ _POSIX_C_SOURCE\n"
"        || /* glibc E<lt>= 2.19 : */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The stdio functions are thread-safe.  This is achieved by assigning to each "
"I<FILE> object a lockcount and (if the lockcount is nonzero)  an owning "
"thread.  For each library call, these functions wait until the I<FILE> "
"object is no longer locked by a different thread, then lock it, do the "
"requested I/O, and unlock the object again."
msgstr ""
"Les fonctions stdio peuvent être utilisées dans un contexte multithread. "
"Ceci est réalisé en affectant à chaque objet de type I<FILE> un «\\ compteur "
"de verrouillage\\ » et (si le «\\ compteur de verrouillage\\ » est non nul) "
"un thread propriétaire. Lors de chaque appel à la bibliothèque, ces "
"fonctions attendent jusqu'à ce que l'objet I<FILE> ne soit plus verrouillé "
"par un thread différent, puis elles le verrouillent, réalisent les entrées/"
"sorties demandées, et libèrent l'objet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(Note: this locking has nothing to do with the file locking done by "
"functions like B<flock>(2)  and B<lockf>(3).)"
msgstr ""
"(Remarque\\ : ce verrouillage n'a rien à voir avec le verrouillage de "
"fichier réalisé par des fonctions comme B<flock>(2) et B<lockf>(3).)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All this is invisible to the C-programmer, but there may be two reasons to "
"wish for more detailed control.  On the one hand, maybe a series of I/O "
"actions by one thread belongs together, and should not be interrupted by the "
"I/O of some other thread.  On the other hand, maybe the locking overhead "
"should be avoided for greater efficiency."
msgstr ""
"Tout ceci est invisible au programmeur en C, mais il existe deux raisons de "
"souhaiter un contrôle plus fin. D'une part, un thread peut réaliser une "
"série d'entrées/sorties interdépendantes, ces opérations ne devant pas être "
"interrompues par les entrées/sorties d'autres threads. D'autre part, on peut "
"désirer supprimer la surcharge induite par ce verrouillage afin d'obtenir de "
"meilleures performances."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"To this end, a thread can explicitly lock the I<FILE> object, then do its "
"series of I/O actions, then unlock.  This prevents other threads from coming "
"in between.  If the reason for doing this was to achieve greater efficiency, "
"one does the I/O with the nonlocking versions of the stdio functions: with "
"B<getc_unlocked>(3)  and B<putc_unlocked>(3)  instead of B<getc>(3)  and "
"B<putc>(3)."
msgstr ""
"À cette fin, un thread peut verrouiller explicitement un objet de type "
"I<FILE>, puis réaliser sa série d'entrées/sorties, et enfin, relâcher le "
"verrou. Cela empêche les autres threads d'intervenir sur le flux. Si la "
"motivation du verrouillage est la recherche de meilleures performances, on "
"peut réaliser l'entrée/sortie à l'aide des versions non bloquantes des "
"fonctions stdio\\ : avec B<getc_unlocked>(3) et B<putc_unlocked>(3) au lieu "
"de B<getc>(3) et B<putc>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<flockfile>()  function waits for I<*filehandle> to be no longer locked "
"by a different thread, then makes the current thread owner of "
"I<*filehandle>, and increments the lockcount."
msgstr ""
"La fonction B<flockfile>() attend jusqu'à ce que I<*filehandle> ne soit plus "
"verrouillé par un autre thread, puis affecte I<*filehandle> au thread "
"actuel, et incrémente le «\\ compteur de verrouillage\\ »."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<funlockfile>()  function decrements the lock count."
msgstr ""
"La fonction B<funlockfile>() décrémente le «\\ compteur de verrouillage\\ »."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<ftrylockfile>()  function is a nonblocking version of B<flockfile>().  "
"It does nothing in case some other thread owns I<*filehandle>, and it "
"obtains ownership and increments the lockcount otherwise."
msgstr ""
"La fonction B<ftrylockfile>() est une version non bloquante de "
"B<flockfile>(). Elle ne fait rien lorsqu'un autre thread est propriétaire de "
"I<*filehandle>, sinon, elle se l'approprie et incrémente le «\\ compteur de "
"verrouillage\\ »."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<ftrylockfile>()  function returns zero for success (the lock was "
"obtained), and nonzero for failure."
msgstr ""
"La fonction B<ftrylockfile>() renvoie zéro en cas de succès (le verrou a été "
"obtenu), et une valeur non nulle en cas d'échec."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr "Aucune."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<flockfile>(),\n"
"B<ftrylockfile>(),\n"
"B<funlockfile>()"
msgstr ""
"B<flockfile>(),\n"
"B<ftrylockfile>(),\n"
"B<funlockfile>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions are available when B<_POSIX_THREAD_SAFE_FUNCTIONS> is "
"defined."
msgstr ""
"Ces fonctions sont disponibles lorsque B<_POSIX_THREAD_SAFE_FUNCTIONS> est "
"défini."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<unlocked_stdio>(3)"
msgstr "B<unlocked_stdio>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
