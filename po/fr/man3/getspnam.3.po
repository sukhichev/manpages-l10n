# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Lucien Gentis <lucien.gentis@waika9.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2024-03-01 16:57+0100\n"
"PO-Revision-Date: 2023-07-05 23:49+0200\n"
"Last-Translator: Lucien Gentis <lucien.gentis@waika9.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 2.4.2\n"
"X-Poedit-Bookmarks: 84,-1,-1,-1,-1,-1,-1,-1,-1,-1\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "getspnam"
msgstr "getspnam"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"getspnam, getspnam_r, getspent, getspent_r, setspent, endspent, fgetspent, "
"fgetspent_r, sgetspent, sgetspent_r, putspent, lckpwdf, ulckpwdf - get "
"shadow password file entry"
msgstr ""
"getspnam, getspnam_r, getspent, getspent_r, setspent, endspent, fgetspent, "
"fgetspent_r, sgetspent, sgetspent_r, putspent, lckpwdf, ulckpwdf - Obtenir "
"une entrée du fichier des mots de passe cachés"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"/* General shadow password file API */\n"
"B<#include E<lt>shadow.hE<gt>>\n"
msgstr ""
"/* API globale du fichier des mots de passe cachés */\n"
"B<#include E<lt>shadow.hE<gt>>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct spwd *getspnam(const char *>I<name>B<);>\n"
"B<struct spwd *getspent(void);>\n"
msgstr ""
"B<struct spwd *getspnam(const char *>I<nom>B<);>\n"
"B<struct spwd *getspent(void);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void setspent(void);>\n"
"B<void endspent(void);>\n"
msgstr ""
"B<void setspent(void);>\n"
"B<void endspent(void);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct spwd *fgetspent(FILE *>I<stream>B<);>\n"
"B<struct spwd *sgetspent(const char *>I<s>B<);>\n"
msgstr ""
"B<struct spwd *fgetspent(FILE *>I<flux>B<);>\n"
"B<struct spwd *sgetspent(const char *>I<s>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int putspent(const struct spwd *>I<p>B<, FILE *>I<stream>B<);>\n"
msgstr "B<int putspent(const struct spwd *>I<p>B<, FILE *>I<flux>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int lckpwdf(void);>\n"
"B<int ulckpwdf(void);>\n"
msgstr ""
"B<int lckpwdf(void);>\n"
"B<int ulckpwdf(void);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"/* GNU extension */\n"
"B<#include E<lt>shadow.hE<gt>>\n"
msgstr ""
"/* Extension GNU */\n"
"B<#include E<lt>shadow.hE<gt>>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getspent_r(struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<, struct spwd **>I<spbufp>B<);>\n"
"B<int getspnam_r(const char *>I<name>B<, struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<, struct spwd **>I<spbufp>B<);>\n"
msgstr ""
"B<int getspent_r(struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<tampon>B<[.>I<taille_tampon>B<], size_t >I<taille_tampon>B<, struct spwd **>I<spbufp>B<);>\n"
"B<int getspnam_r(const char *>I<nom>B<, struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<tampon>B<[.>I<taille_tampon>B<], size_t >I<taille_tampon>B<, struct spwd **>I<spbufp>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int fgetspent_r(FILE *>I<stream>B<, struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<, struct spwd **>I<spbufp>B<);>\n"
"B<int sgetspent_r(const char *>I<s>B<, struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<buf>B<[.>I<buflen>B<], size_t >I<buflen>B<, struct spwd **>I<spbufp>B<);>\n"
msgstr ""
"B<int fgetspent_r(FILE *>I<flux>B<, struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<tampon>B<[.>I<taille_tampon>B<], size_t >I<taille_tampon>B<, struct spwd **>I<spbufp>B<);>\n"
"B<int sgetspent_r(const char *>I<s>B<, struct spwd *>I<spbuf>B<,>\n"
"B<               char >I<tampon>B<[.>I<taille_tampon>B<], size_t >I<taille_tampon>B<, struct spwd **>I<spbufp>B<);>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getspent_r>(), B<getspnam_r>(), B<fgetspent_r>(), B<sgetspent_r>():"
msgstr "B<getspent_r>(), B<getspnam_r>(), B<fgetspent_r>(), B<sgetspent_r>():"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Long ago it was considered safe to have encrypted passwords openly visible "
"in the password file.  When computers got faster and people got more "
"security-conscious, this was no longer acceptable.  Julianne Frances Haugh "
"implemented the shadow password suite that keeps the encrypted passwords in "
"the shadow password database (e.g., the local shadow password file I</etc/"
"shadow>, NIS, and LDAP), readable only by root."
msgstr ""
"Il a longtemps été considéré comme sûr d'avoir des mots de passe chiffrés "
"ouvertement visibles dans le fichier des mots de passe. Lorsque les "
"ordinateurs sont devenus plus rapides et que les gens sont devenus plus "
"conscients des problèmes de sécurité, cela n'était plus acceptable. Julianne "
"Frances Haugh a implémenté la suite d'utilitaires «\\ shadow\\ » qui "
"conserve les mots de passe chiffrés dans la base de données des mots de "
"passe cachés «\\ shadow\\ » (par exemple, le fichier local des mots de passe "
"cachés I</etc/shadow>, NIS ou LDAP), lisible seulement par le "
"superutilisateur."

#.  FIXME . I've commented out the following for the
#.  moment.  The relationship between PAM and nsswitch.conf needs
#.  to be clearly documented in one place, which is pointed to by
#.  the pages for the user, group, and shadow password functions.
#.  (Jul 2005, mtk)
#.  This shadow password setup has been superseded by PAM
#.  (pluggable authentication modules), and the file
#.  .I /etc/nsswitch.conf
#.  now describes the sources to be used.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The functions described below resemble those for the traditional password "
"database (e.g., see B<getpwnam>(3)  and B<getpwent>(3))."
msgstr ""
"Les fonctions décrites ci-dessous ressemblent aux fonctions traditionnelles "
"de la base de données des mots de passe (par exemple, consultez "
"B<getpwnam>(3) et B<getpwent>(3))."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getspnam>()  function returns a pointer to a structure containing the "
"broken-out fields of the record in the shadow password database that matches "
"the username I<name>."
msgstr ""
"La fonction B<getspnam>() renvoie un pointeur vers une structure contenant "
"les champs d'un enregistrement extrait de la base de données «\\ shadow\\ » "
"de l'entrée correspondant au I<nom> de l'utilisateur."

#.  some systems require a call of setspent() before the first getspent()
#.  glibc does not
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<getspent>()  function returns a pointer to the next entry in the "
"shadow password database.  The position in the input stream is initialized "
"by B<setspent>().  When done reading, the program may call B<endspent>()  so "
"that resources can be deallocated."
msgstr ""
"La fonction B<getspent>() renvoie un pointeur sur l'entrée suivante de la "
"base de données «\\ shadow\\ ». La position dans le flux d'entrée est "
"initialisée par B<setspent>(). Lorsque la lecture est finie, le programme "
"devrait appeler B<endspent>() pour désallouer les ressources."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<fgetspent>()  function is similar to B<getspent>()  but uses the "
"supplied stream instead of the one implicitly opened by B<setspent>()."
msgstr ""
"La fonction B<fgetspent>() est similaire à B<getspent>(), mais utilise le "
"flux spécifié plutôt que celui implicitement ouvert par B<setspent>()."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<sgetspent>()  function parses the supplied string I<s> into a struct "
"I<spwd>."
msgstr ""
"La fonction B<sgetspent>() analyse la chaîne I<s> fournie dans la structure "
"I<spwd>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<putspent>()  function writes the contents of the supplied struct "
"I<spwd> I<*p> as a text line in the shadow password file format to "
"I<stream>.  String entries with value NULL and numerical entries with value "
"-1 are written as an empty string."
msgstr ""
"La fonction B<putspent>() écrit le contenu de la structure I<spwd> I<*p> "
"fournie sous forme d'une ligne de texte au format du fichier des mots de "
"passe cachés dans le I<flux>. Les entrées chaînes de valeur B<NULL> et les "
"entrées numériques de valeur B<-1> sont écrites comme des chaînes vides."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<lckpwdf>()  function is intended to protect against multiple "
"simultaneous accesses of the shadow password database.  It tries to acquire "
"a lock, and returns 0 on success, or -1 on failure (lock not obtained within "
"15 seconds).  The B<ulckpwdf>()  function releases the lock again.  Note "
"that there is no protection against direct access of the shadow password "
"file.  Only programs that use B<lckpwdf>()  will notice the lock."
msgstr ""
"La fonction B<lckpwdf>() a pour but de protéger la base de données des mots "
"de passe cachés contre des accès simultanés. Elle tente d'obtenir un verrou, "
"renvoie B<0> si elle y arrive ou B<-1> si elle échoue (le verrou n'a pas pu "
"être obtenu dans les 15 secondes). La fonction B<ulckpwdf>() libère le "
"verrou. Veuillez noter qu'il n'y a pas de protection contre l'accès direct "
"au fichier des mots de passe cachés. Seuls les programmes qui utilisent "
"B<lckpwdf>() remarqueront le verrou."

#.  Also in libc5
#.  SUN doesn't have sgetspent()
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These were the functions that formed the original shadow API.  They are "
"widely available."
msgstr ""
"C'étaient les routines qui composaient l'API originale « shadow ». Elles "
"sont largement disponibles."

#. type: SS
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Reentrant versions"
msgstr "Versions réentrantes"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Analogous to the reentrant functions for the password database, glibc also "
"has reentrant functions for the shadow password database.  The "
"B<getspnam_r>()  function is like B<getspnam>()  but stores the retrieved "
"shadow password structure in the space pointed to by I<spbuf>.  This shadow "
"password structure contains pointers to strings, and these strings are "
"stored in the buffer I<buf> of size I<buflen>.  A pointer to the result (in "
"case of success) or NULL (in case no entry was found or an error occurred) "
"is stored in I<*spbufp>."
msgstr ""
"De manière analogue aux routines réentrantes pour la base de données des "
"mots de passe, la glibc possède aussi des versions réentrantes pour la base "
"de données des mots de passe cachés. La fonction B<getspnam_r>() est "
"équivalente à la fonction B<getspnam>(), mais enregistre la structure des "
"mots de passe cachés trouvée dans l'espace pointé par I<spbuf>. Cette "
"structure des mots de passe cachés contient des pointeurs vers des chaînes "
"qui sont stockées dans le I<tampon> de taille I<taille_tampon>. Un pointeur "
"vers le résultat (en cas de réussite) ou B<NULL> (si aucune entrée n'a été "
"trouvée ou si une erreur est survenue) est stocké dans I<*spbufp>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The functions B<getspent_r>(), B<fgetspent_r>(), and B<sgetspent_r>()  are "
"similarly analogous to their nonreentrant counterparts."
msgstr ""
"Les fonctions B<getspent_r>(), B<fgetspent_r>() et B<sgetspent_r>() sont "
"analogues à leurs homologues non réentrantes."

#.  SUN doesn't have sgetspent_r()
#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some non-glibc systems also have functions with these names, often with "
"different prototypes."
msgstr ""
"Certains systèmes non glibc ont également des fonctions portant ces noms, "
"souvent avec des prototypes différents."

#. type: SS
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Structure"
msgstr "Structure"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The shadow password structure is defined in I<E<lt>shadow.hE<gt>> as follows:"
msgstr ""
"La structure des mots de passe cachés est définie dans I<E<lt>shadow.hE<gt>> "
"de la manière suivante\\ :"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct spwd {\n"
"    char *sp_namp;     /* Login name */\n"
"    char *sp_pwdp;     /* Encrypted password */\n"
"    long  sp_lstchg;   /* Date of last change\n"
"                          (measured in days since\n"
"                          1970-01-01 00:00:00 +0000 (UTC)) */\n"
"    long  sp_min;      /* Min # of days between changes */\n"
"    long  sp_max;      /* Max # of days between changes */\n"
"    long  sp_warn;     /* # of days before password expires\n"
"                          to warn user to change it */\n"
"    long  sp_inact;    /* # of days after password expires\n"
"                          until account is disabled */\n"
"    long  sp_expire;   /* Date when account expires\n"
"                          (measured in days since\n"
"                          1970-01-01 00:00:00 +0000 (UTC)) */\n"
"    unsigned long sp_flag;  /* Reserved */\n"
"};\n"
msgstr ""
"struct spwd {\n"
"    char *sp_namp;     /* Identifiant de connexion */\n"
"    char *sp_pwdp;     /* Mot de passe chiffré */\n"
"    long sp_lstchg;    /* Date de dernière modification\n"
"                          (mesurée en jours depuis l'époque,\n"
"                          1er janvier 1970 à 00:00:00 (UTC)) */\n"
"    long sp_min;       /* Nombre de jours minimum entre\n"
"                          deux modifications */\n"
"    long sp_max;       /* Nombre de jours maximum entre\n"
"                          deux modifications */\n"
"    long sp_warn;      /* Nombre de jours avant l'expiration\n"
"                          du mot de passe pour avertir\n"
"                          l'utilisateur de le modifier */\n"
"    long sp_inact;     /* Nombre de jours après l'expiration\n"
"                          du mot de passe pour la désactivation\n"
"                          du compte */\n"
"    long sp_expire;    /* Date à laquelle le compte expirera,\n"
"                          (mesurée en jours depuis l'époque,\n"
"                          1er janvier 1970 à 00:00:00 (UTC)) */\n"
"    unsigned long sp_flag; /* Réservé */\n"
"};\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The functions that return a pointer return NULL if no more entries are "
"available or if an error occurs during processing.  The functions which have "
"I<int> as the return value return 0 for success and -1 for failure, with "
"I<errno> set to indicate the error."
msgstr ""
"Les routines qui renvoient un pointeur renvoient B<NULL> s'il n'y a plus "
"d'entrée disponible ou si une erreur est survenue pendant le traitement. Les "
"routines qui ont un I<int> comme valeur de retour renvoient B<0> en cas de "
"réussite. En cas d'erreur, B<-1> est renvoyé et I<errno> est définie pour "
"préciser l'erreur."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For the nonreentrant functions, the return value may point to static area, "
"and may be overwritten by subsequent calls to these functions."
msgstr ""
"Pour les fonctions non réentrantes, la valeur de retour peut pointer sur une "
"zone statique et peut être écrasée par des appels ultérieurs de ces "
"fonctions."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The reentrant functions return zero on success.  In case of error, an error "
"number is returned."
msgstr ""
"Les fonctions réentrantes renvoient zéro si elles réussissent. Si elles "
"échouent, une valeur d'erreur est renvoyée."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The caller does not have permission to access the shadow password file."
msgstr ""
"L'appelant n'a pas le droit d'accéder au fichier de mots de passe cachés."

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Supplied buffer is too small."
msgstr "Le tampon fourni est trop petit."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/shadow>"
msgstr "I</etc/shadow>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "local shadow password database file"
msgstr "fichier base de données des mots de passe cachés"

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/.pwd.lock>"
msgstr "I</etc/.pwd.lock>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "lock file"
msgstr "fichier verrou"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The include file I<E<lt>paths.hE<gt>> defines the constant B<_PATH_SHADOW> "
"to the pathname of the shadow password file."
msgstr ""
"Le fichier d'inclusion I<E<lt>paths.hE<gt>> définit la constante "
"B<_PATH_SHADOW> comme étant le chemin du fichier des mots de passe cachés."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getspnam>()"
msgstr "B<getspnam>()"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:getspnam locale"
msgstr "MT-Unsafe race:getspnam locale"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<getspent>()"
msgstr "B<getspent>()"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"MT-Unsafe race:getspent\n"
"race:spentbuf locale"
msgstr ""
"MT-Unsafe race:getspent\n"
"race:spentbuf locale"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<setspent>(),\n"
"B<endspent>(),\n"
"B<getspent_r>()"
msgstr ""
"B<setspent>(),\n"
"B<endspent>(),\n"
"B<getspent_r>()"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:getspent locale"
msgstr "MT-Unsafe race:getspent locale"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<fgetspent>()"
msgstr "B<fgetspent>()"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:fgetspent"
msgstr "MT-Unsafe race:fgetspent"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<sgetspent>()"
msgstr "B<sgetspent>()"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:sgetspent"
msgstr "MT-Unsafe race:sgetspent"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<putspent>(),\n"
"B<getspnam_r>(),\n"
"B<sgetspent_r>()"
msgstr ""
"B<putspent>(),\n"
"B<getspnam_r>(),\n"
"B<sgetspent_r>()"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<lckpwdf>(),\n"
"B<ulckpwdf>(),\n"
"B<fgetspent_r>()"
msgstr ""
"B<lckpwdf>(),\n"
"B<ulckpwdf>(),\n"
"B<fgetspent_r>()"

#. type: tbl table
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the above table, I<getspent> in I<race:getspent> signifies that if any of "
"the functions B<setspent>(), B<getspent>(), B<getspent_r>(), or "
"B<endspent>()  are used in parallel in different threads of a program, then "
"data races could occur."
msgstr ""
"Dans le tableau ci-dessus, I<getspent> dans I<race:getspent> signifie que si "
"des fonctions parmi B<setspent>(), B<getspent>(), B<getspent_r>() ou "
"B<endspent>() sont utilisées en parallèle dans différents threads d'un "
"programme, des situations de compétition entre données pourraient apparaître."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: debian-bookworm
msgid ""
"The shadow password database and its associated API are not specified in "
"POSIX.1.  However, many other systems provide a similar API."
msgstr ""
"La base de données de mots de passe masqués et son API ne sont pas "
"spécifiées dans POSIX.1. Cependant, beaucoup de systèmes fournissent une API "
"similaire."

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getgrnam>(3), B<getpwnam>(3), B<getpwnam_r>(3), B<shadow>(5)"
msgstr "B<getgrnam>(3), B<getpwnam>(3), B<getpwnam_r>(3), B<shadow>(5)"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: tbl table
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: SH
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Many other systems provide a similar API."
msgstr "Beaucoup de systèmes fournissent une API similaire."

#. type: Plain text
#: debian-unstable fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "None."
msgstr "Aucun."

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
