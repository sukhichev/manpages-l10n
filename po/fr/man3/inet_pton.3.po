# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-03-01 16:58+0100\n"
"PO-Revision-Date: 2024-03-16 09:37+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "inet_pton"
msgstr "inet_pton"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "inet_pton - convert IPv4 and IPv6 addresses from text to binary form"
msgstr ""
"inet_pton - Convertir des adresses IPv4 et IPv6 sous forme texte en forme "
"binaire"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>arpa/inet.hE<gt>>\n"
msgstr "B<#include E<lt>arpa/inet.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int inet_pton(int >I<af>B<, const char *restrict >I<src>B<, void *restrict >I<dst>B<);>\n"
msgstr "B<int inet_pton(int >I<af>B<, const char *restrict >I<src>B<, void *restrict >I<dst>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This function converts the character string I<src> into a network address "
"structure in the I<af> address family, then copies the network address "
"structure to I<dst>.  The I<af> argument must be either B<AF_INET> or "
"B<AF_INET6>.  I<dst> is written in network byte order."
msgstr ""
"Cette fonction convertit la chaîne de caractères I<src> en une structure "
"d'adresse réseau de la famille I<af>, puis copie cette structure dans "
"I<dst>. L'argument I<af> doit être soit B<AF_INET> soit B<AF_INET6>. I<dst> "
"est écrit dans l'ordre d'octets du réseau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following address families are currently supported:"
msgstr "Les familles d'adresses suivantes sont dès à présent supportées :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<AF_INET>"
msgstr "B<AF_INET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<src> points to a character string containing an IPv4 network address in "
"dotted-decimal format, \"I<ddd.ddd.ddd.ddd>\", where I<ddd> is a decimal "
"number of up to three digits in the range 0 to 255.  The address is "
"converted to a I<struct in_addr> and copied to I<dst>, which must be "
"I<sizeof(struct in_addr)> (4) bytes (32 bits) long."
msgstr ""
"I<src> pointe sur une chaîne de caractère contenant une adresse réseau IPv4 "
"au format décimal pointé I<ddd.ddd.ddd.ddd>, où I<ddd> est un nombre "
"décimal, contenant jusqu'à trois chiffres, dans l'intervalle 0 à 255. "
"L'adresse est alors convertie en une structure I<struct in_addr> et copiée "
"dans I<dst>, qui doit donc contenir au minimum I<sizeof(struct in_addr)> (4) "
"octets (32 bits)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<AF_INET6>"
msgstr "B<AF_INET6>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<src> points to a character string containing an IPv6 network address.  The "
"address is converted to a I<struct in6_addr> and copied to I<dst>, which "
"must be I<sizeof(struct in6_addr)> (16) bytes (128 bits) long.  The allowed "
"formats for IPv6 addresses follow these rules:"
msgstr ""
"I<src> pointe sur une chaîne de caractères contenant une adresse réseau "
"IPv6. L'adresse est convertie en une structure I<struct in6_addr> et copiée "
"dans I<dst>, qui doit donc contenir au moins I<sizeof(struct in6_addr)> (16) "
"octets (128 bits). Les formats d'adresse IPv6 autorisés suivent les règles "
"suivantes :"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "-"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The preferred format is I<x:x:x:x:x:x:x:x>.  This form consists of eight "
"hexadecimal numbers, each of which expresses a 16-bit value (i.e., each I<x> "
"can be up to 4 hex digits)."
msgstr ""
"Le format préféré est I<x:x:x:x:x:x:x:x>. Cette forme consiste en 8 nombres "
"hexadécimaux, chacun d'entre eux exprimant une valeur sur 16 bits (c'est-à-"
"dire que chaque I<x> peut contenir jusqu'à 4 symboles hexadécimaux)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A series of contiguous zero values in the preferred format can be "
"abbreviated to I<::>.  Only one instance of I<::> can occur in an address.  "
"For example, the loopback address I<0:0:0:0:0:0:0:1> can be abbreviated as "
"I<::1>.  The wildcard address, consisting of all zeros, can be written as "
"I<::>."
msgstr ""
"Une série de zéros contigus dans la forme préférée peut être abrégée en I<::"
">. Une seule instance de I<::> peut apparaître dans une adresse. Par "
"exemple, l'adresse de boucle I<0:0:0:0:0:0:0:1> peut être abrégée en I<::1>. "
"L'adresse joker, constituée uniquement de zéros, peut être écrite comme I<::"
">."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An alternate format is useful for expressing IPv4-mapped IPv6 addresses.  "
"This form is written as I<x:x:x:x:x:x:d.d.d.d>, where the six leading I<x>s "
"are hexadecimal values that define the six most-significant 16-bit pieces of "
"the address (i.e., 96 bits), and the I<d>s express a value in dotted-decimal "
"notation that defines the least significant 32 bits of the address.  An "
"example of such an address is I<::FFFF:204.152.189.116>."
msgstr ""
"Un autre format utile pour exprimer des adresses IPv4 projetées dans "
"l'espace IPv6 est I<x:x:x:x:x:x:d.d.d.d>, où les six I<x> de tête sont des "
"valeurs hexadécimales qui définissent les 6 mots 16 bits de poids fort de "
"l'adresse (c'est-à-dire 96 bits), et les I<d> expriment une valeur en "
"notation décimale pointée définissant les 32 bits de poids faible de "
"l'adresse. Un exemple d'une telle adresse est I<::FFFF:204.152.189.116>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See RFC 2373 for further details on the representation of IPv6 addresses."
msgstr ""
"Consultez la RFC 2373 pour plus de détails sur la représentation des "
"adresses IPv6."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<inet_pton>()  returns 1 on success (network address was successfully "
"converted).  0 is returned if I<src> does not contain a character string "
"representing a valid network address in the specified address family.  If "
"I<af> does not contain a valid address family, -1 is returned and I<errno> "
"is set to B<EAFNOSUPPORT>."
msgstr ""
"B<inet_pton>() renvoie 1 si elle réussit (l'adresse réseau a été convertie "
"avec succès). Elle renvoie une valeur nulle si I<src> ne contient pas une "
"adresse réseau valable pour la famille indiquée. Si I<af> ne contient pas de "
"famille d'adresse valable, -1 est renvoyé et I<errno> contient "
"B<EAFNOSUPPORT>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<inet_pton>()"
msgstr "B<inet_pton>()."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Unlike B<inet_aton>(3)  and B<inet_addr>(3), B<inet_pton>()  supports IPv6 "
"addresses.  On the other hand, B<inet_pton>()  accepts only IPv4 addresses "
"in dotted-decimal notation, whereas B<inet_aton>(3)  and B<inet_addr>(3)  "
"allow the more general numbers-and-dots notation (hexadecimal and octal "
"number formats, and formats that don't require all four bytes to be "
"explicitly written).  For an interface that handles both IPv6 addresses, and "
"IPv4 addresses in numbers-and-dots notation, see B<getaddrinfo>(3)."
msgstr ""
"Contrairement à B<inet_aton>(3) et B<inet_addr>(3), B<inet_pton>() gère les "
"adresses IPv6. D'un autre coté, B<inet_pton>() n'accepte que les adresses "
"IPv4 en notation décimale pointée alors que B<inet_aton>(3) et "
"B<inet_addr>(3) autorisent la notation plus générale numérique pointée "
"(formats à nombre hexadécimaux et octaux, de même que les formats n'exigeant "
"pas que les 4 octets soient explicitement écrits). Pour une interface gérant "
"les adresses IPv6 et IPv4 en notation numérique pointée, consultez "
"B<getaddrinfo>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<AF_INET6> does not recognize IPv4 addresses.  An explicit IPv4-mapped IPv6 "
"address must be supplied in I<src> instead."
msgstr ""
"B<AF_INET6> ne reconnaît pas les adresses IPv4. Il faut dans ce cas fournir "
"dans I<src> une adresse IPv4 projetée dans l'espace IPv6."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<inet_pton>()  and "
"B<inet_ntop>(3).  Here are some example runs:"
msgstr ""
"Le programme suivant montre une utilisation de B<inet_pton>() et "
"B<inet_ntop>(3). Voici quelques exemples d'exécution :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out i6 0:0:0:0:0:0:0:0>\n"
"::\n"
"$B< ./a.out i6 1:0:0:0:0:0:0:8>\n"
"1::8\n"
"$B< ./a.out i6 0:0:0:0:0:FFFF:204.152.189.116>\n"
"::ffff:204.152.189.116\n"
msgstr ""
"$B< ./a.out i6 0:0:0:0:0:0:0:0>\n"
"::\n"
"$B< ./a.out i6 1:0:0:0:0:0:0:8>\n"
"1::8\n"
"$B< ./a.out i6 0:0:0:0:0:FFFF:204.152.189.116>\n"
"::ffff:204.152.189.116\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Source du programme"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>arpa/inet.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    unsigned char buf[sizeof(struct in6_addr)];\n"
"    int domain, s;\n"
"    char str[INET6_ADDRSTRLEN];\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s {i4|i6|E<lt>numE<gt>} string\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    domain = (strcmp(argv[1], \"i4\") == 0) ? AF_INET :\n"
"             (strcmp(argv[1], \"i6\") == 0) ? AF_INET6 : atoi(argv[1]);\n"
"\\&\n"
"    s = inet_pton(domain, argv[2], buf);\n"
"    if (s E<lt>= 0) {\n"
"        if (s == 0)\n"
"            fprintf(stderr, \"Not in presentation format\");\n"
"        else\n"
"            perror(\"inet_pton\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (inet_ntop(domain, buf, str, INET6_ADDRSTRLEN) == NULL) {\n"
"        perror(\"inet_ntop\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"%s\\en\", str);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"#include E<lt>arpa/inet.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    unsigned char buf[sizeof(struct in6_addr)];\n"
"    int domain, s;\n"
"    char str[INET6_ADDRSTRLEN];\n"
"\\&\n"
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilisation : %s chaîne {i4|i6|E<lt>numE<gt>}\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    domain = (strcmp(argv[1], \"i4\") == 0) ? AF_INET :\n"
"             (strcmp(argv[1], \"i6\") == 0) ? AF_INET6 : atoi(argv[1]);\n"
"\\&\n"
"    s = inet_pton(domain, argv[2], buf);\n"
"    if (s E<lt>= 0) {\n"
"        if (s == 0)\n"
"            fprintf(stderr, \"Pas en format de présentation\");\n"
"        else\n"
"            perror(\"inet_pton\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    if (inet_ntop(domain, buf, str, INET6_ADDRSTRLEN) == NULL) {\n"
"        perror(\"inet_ntop\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    printf(\"%s\\en\", str);\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<getaddrinfo>(3), B<inet>(3), B<inet_ntop>(3)"
msgstr "B<getaddrinfo>(3), B<inet>(3), B<inet_ntop>(3)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>arpa/inet.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>arpa/inet.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    unsigned char buf[sizeof(struct in6_addr)];\n"
"    int domain, s;\n"
"    char str[INET6_ADDRSTRLEN];\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    unsigned char buf[sizeof(struct in6_addr)];\n"
"    int domain, s;\n"
"    char str[INET6_ADDRSTRLEN];\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s {i4|i6|E<lt>numE<gt>} string\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Utilisation : %s chaîne {i4|i6|E<lt>numE<gt>}\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    domain = (strcmp(argv[1], \"i4\") == 0) ? AF_INET :\n"
"             (strcmp(argv[1], \"i6\") == 0) ? AF_INET6 : atoi(argv[1]);\n"
msgstr ""
"    domain = (strcmp(argv[1], \"i4\") == 0) ? AF_INET :\n"
"             (strcmp(argv[1], \"i6\") == 0) ? AF_INET6 : atoi(argv[1]);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    s = inet_pton(domain, argv[2], buf);\n"
"    if (s E<lt>= 0) {\n"
"        if (s == 0)\n"
"            fprintf(stderr, \"Not in presentation format\");\n"
"        else\n"
"            perror(\"inet_pton\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    s = inet_pton(domain, argv[2], buf);\n"
"    if (s E<lt>= 0) {\n"
"        if (s == 0)\n"
"            fprintf(stderr, \"pas en format de présentation\");\n"
"        else\n"
"            perror(\"inet_pton\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (inet_ntop(domain, buf, str, INET6_ADDRSTRLEN) == NULL) {\n"
"        perror(\"inet_ntop\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (inet_ntop(domain, buf, str, INET6_ADDRSTRLEN) == NULL) {\n"
"        perror(\"inet_ntop\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    printf(\"%s\\en\", str);\n"
msgstr "    printf(\"%s\\en\", str);\n"

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "20 juillet 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
