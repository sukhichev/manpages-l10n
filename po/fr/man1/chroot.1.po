# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Marc Chaton <chaton@debian.org>
# Nicolas François <nicolas.francois@centraliens.net>, 2008-2010.
# Bastien Scher <bastien0705@gmail.com>, 2011-2013.
# David Prévot <david@tilapin.org>, 2012-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020
msgid ""
msgstr ""
"Project-Id-Version: coreutils manpages\n"
"POT-Creation-Date: 2024-02-15 17:56+0100\n"
"PO-Revision-Date: 2023-05-16 07:58+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "CHROOT"
msgstr "CHROOT"

#. type: TH
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "January 2024"
msgstr "Janvier 2024"

#. type: TH
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.4"
msgstr "GNU coreutils 9.4"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "chroot - run command or interactive shell with special root directory"
msgstr ""
"chroot - Exécuter une commande ou un shell interactif dans un répertoire "
"racine fermé"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"B<chroot> [I<\\,OPTION\\/>] I<\\,NEWROOT \\/>[I<\\,COMMAND \\/>[I<\\,ARG\\/"
">]...]"
msgstr ""
"B<chroot> [I<\\,OPTION\\/>] I<\\,NOUVELLE_RACINE \\/>[I<\\,COMMANDE \\/"
">[I<\\,ARG\\/>]...]"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "B<chroot> I<\\,OPTION\\/>"
msgstr "B<chroot> I<\\,OPTION\\/>"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Run COMMAND with root directory set to NEWROOT."
msgstr ""
"Exécuter la I<COMMANDE> avec le répertoire racine initialisé à "
"I<NOUVELLE_RACINE>."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--groups>=I<\\,G_LIST\\/>"
msgstr "B<--groups>=I<\\,LISTE_GROUPES\\/>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "specify supplementary groups as g1,g2,..,gN"
msgstr "indiquer des groupes supplémentaires, par exemple g1,g2, …,gN"

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--userspec>=I<\\,USER\\/>:GROUP"
msgstr "B<--userspec>=I<\\,UTILISATEUR\\/>B<:>I<GROUPE>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "specify user and group (ID or name) to use"
msgstr "indiquer l'utilisateur et le groupe à utiliser (identifiants ou noms)"

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--skip-chdir>"
msgstr "B<--skip-chdir>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "do not change working directory to '/'"
msgstr "ne pas changer le répertoire de travail à « / »"

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "display this help and exit"
msgstr "afficher l'aide-mémoire et quitter."

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "output version information and exit"
msgstr "afficher les informations de version et quitter."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"If no command is given, run '\"$SHELL\" B<-i>' (default: '/bin/sh B<-i>')."
msgstr ""
"Si aucune commande n'est fournie, « \\\"$shell\\\" B<-I> » est exécuté (par "
"défaut : « /bin/sh B<-i> »)."

#. type: SS
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "État de fin d'exécution :"

#. type: TP
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "125"
msgstr "125"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
msgid "if the chroot command itself fails"
msgstr "si la commande B<chroot> elle-même échoue"

#. type: TP
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "126"
msgstr "126"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND is found but cannot be invoked"
msgstr "si la I<COMMANDE> est trouvée mais ne peut pas être invoquée"

#. type: TP
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "127"
msgstr "127"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND cannot be found"
msgstr "si la I<COMMANDE> ne peut pas être trouvée"

#. type: TP
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr "-"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
msgid "the exit status of COMMAND otherwise"
msgstr "le code de retour de la I<COMMANDE> sinon"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Written by Roland McGrath."
msgstr "Écrit par Roland McGrath."

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Aide en ligne de GNU coreutils : E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Signaler toute erreur de traduction à E<lt>https://translationproject.org/"
"team/fr.htmlE<gt>"

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ce programme est un logiciel libre. Vous pouvez le modifier et le "
"redistribuer. Il n'y a AUCUNE GARANTIE dans la mesure autorisée par la loi."

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<chroot>(2)"
msgstr "B<chroot>(2)"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chrootE<gt>"
msgstr ""
"Documentation complète : E<lt>https://www.gnu.org/software/coreutils/"
"chrootE<gt>"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chroot invocation\\(aq"
msgstr ""
"aussi disponible localement à l’aide de la commande : info \\(aq(coreutils) "
"chroot invocation\\(aq"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Avril 2022"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "October 2021"
msgstr "Octobre 2021"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licence GPLv3+\\ : GNU "
"GPL version 3 ou ultérieure E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-6
msgid "chroot(2)"
msgstr "B<chroot>(2)"
