# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.18.1\n"
"POT-Creation-Date: 2024-02-09 16:59+0100\n"
"PO-Revision-Date: 2023-10-07 06:50+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-FILE"
msgstr "GRUB-FILE"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "Décembre 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-1"
msgstr "GRUB 2:2.12-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-file - check file type"
msgstr "grub-file – Vérifier le type d'un fichier"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<file> I<\\,OPTIONS FILE\\/>"
msgstr "B<file> I<\\,OPTIONS FICHIER\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is of specified type."
msgstr "Vérifier si le I<FICHIER> est du type spécifié."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-i386-xen-pae-domu>"
msgstr "B<--is-i386-xen-pae-domu>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE can be booted as i386 PAE Xen unprivileged guest kernel"
msgstr ""
"Vérifier si I<FICHIER> peut être amorcé comme noyau client non privilégié "
"Xen i386 PAE"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86_64-xen-domu>"
msgstr "B<--is-x86_64-xen-domu>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE can be booted as x86_64 Xen unprivileged guest kernel"
msgstr ""
"Vérifier si I<FICHIER> peut être amorcé comme noyau client non privilégié "
"Xen x86_64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86-xen-dom0>"
msgstr "B<--is-x86-xen-dom0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE can be used as Xen x86 privileged guest kernel"
msgstr ""
"Vérifier si I<FICHIER> peut être utilisé comme noyau client privilégié Xen "
"x86"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86-multiboot>"
msgstr "B<--is-x86-multiboot>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE can be used as x86 multiboot kernel"
msgstr "Vérifier si I<FICHIER> peut être utilisé comme noyau multiboot x86"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<--is-x86-multiboot2> Check if FILE can be used as x86 multiboot2 kernel"
msgstr ""
"B<--is-x86-multiboot2> Vérifier si I<FICHIER> peut être utilisé comme noyau "
"multiboot2 x86"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-arm-linux>"
msgstr "B<--is-arm-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is ARM Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux ARM"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-arm64-linux>"
msgstr "B<--is-arm64-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is ARM64 Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux ARM64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-ia64-linux>"
msgstr "B<--is-ia64-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is IA64 Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux IA64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-mips-linux>"
msgstr "B<--is-mips-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is MIPS Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux MIPS"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-mipsel-linux>"
msgstr "B<--is-mipsel-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is MIPSEL Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux MIPSEL"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-sparc64-linux>"
msgstr "B<--is-sparc64-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is SPARC64 Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux SPARC64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-powerpc-linux>"
msgstr "B<--is-powerpc-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is POWERPC Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux POWERPC"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86-linux>"
msgstr "B<--is-x86-linux>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is x86 Linux"
msgstr "Vérifier si I<FICHIER> est de type Linux x86"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86-linux32>"
msgstr "B<--is-x86-linux32>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is x86 Linux supporting 32-bit protocol"
msgstr ""
"Vérifier si I<FICHIER> est de type Linux x86 prenant en charge le protocole "
"32\\ bits"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86-kfreebsd>"
msgstr "B<--is-x86-kfreebsd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is x86 kFreeBSD"
msgstr "Vérifier si I<FICHIER> est de type kFreeBSD x86"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-i386-kfreebsd>"
msgstr "B<--is-i386-kfreebsd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is i386 kFreeBSD"
msgstr "Vérifier si I<FICHIER> est de type kFreeBSD i386"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86_64-kfreebsd>"
msgstr "B<--is-x86_64-kfreebsd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is x86_64 kFreeBSD"
msgstr "Vérifier si I<FICHIER> est de type kFreeBSD x86_64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86-knetbsd>"
msgstr "B<--is-x86-knetbsd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is x86 kNetBSD"
msgstr "Vérifier si I<FICHIER> est de type kNetBSD x86"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-i386-knetbsd>"
msgstr "B<--is-i386-knetbsd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is i386 kNetBSD"
msgstr "Vérifier si I<FICHIER> est de type kNetBSD i386"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<--is-x86_64-knetbsd> Check if FILE is x86_64 kNetBSD"
msgstr ""
"B<--is-x86_64-knetbsd> Vérifier si I<FICHIER> est de type kNetBSD x86_64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-i386-efi>"
msgstr "B<--is-i386-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is i386 EFI file"
msgstr "Vérifier si I<FICHIER> est un fichier EFI i386"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86_64-efi>"
msgstr "B<--is-x86_64-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is x86_64 EFI file"
msgstr "Vérifier si I<FICHIER> est un fichier EFI x86_64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-ia64-efi>"
msgstr "B<--is-ia64-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is IA64 EFI file"
msgstr "Vérifier si I<FICHIER> est un fichier EFI IA64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-arm64-efi>"
msgstr "B<--is-arm64-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is ARM64 EFI file"
msgstr "Vérifier si I<FICHIER> est un fichier EFI ARM64"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-arm-efi>"
msgstr "B<--is-arm-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is ARM EFI file"
msgstr "Vérifier si I<FICHIER> est un fichier EFI ARM"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-riscv32-efi>"
msgstr "B<--is-riscv32-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is RISC-V 32bit EFI file"
msgstr "Vérifier si I<FICHIER> est un fichier EFI RISC-V 32\\ bits"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-riscv64-efi>"
msgstr "B<--is-riscv64-efi>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is RISC-V 64bit EFI file"
msgstr "Vérifier si I<FICHIER> est un fichier EFI RISC-V 64\\ bits"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-hibernated-hiberfil>"
msgstr "B<--is-hibernated-hiberfil>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is hiberfil.sys in hibernated state"
msgstr "Vérifier si I<FICHIER> est hiberfil.sys en veille prolongée"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86_64-xnu>"
msgstr "B<--is-x86_64-xnu>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is x86_64 XNU (Mac OS X kernel)"
msgstr "Vérifier si I<FICHIER> est de type XNU x86_64 (noyau Mac OS X)"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-i386-xnu>"
msgstr "B<--is-i386-xnu>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is i386 XNU (Mac OS X kernel)"
msgstr "Vérifier si I<FICHIER> est de type XNU i386 (noyau Mac OS X)"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-xnu-hibr>"
msgstr "B<--is-xnu-hibr>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is XNU (Mac OS X kernel) hibernated image"
msgstr ""
"Vérifier si I<FICHIER> est une image XNU en veille prolongée (noyau Mac OS X)"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--is-x86-bios-bootsector>"
msgstr "B<--is-x86-bios-bootsector>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Check if FILE is BIOS bootsector"
msgstr "Vérifier si I<FICHIER> est de type secteur de démarrage du BIOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<-h>, B<--help> Display this help and exit.  B<-u>, B<--usage> Display the "
"usage of this command and exit."
msgstr ""
"B<-h>, B<--help> Afficher cet aide-mémoire puis quitter. B<-u>, B<--usage> "
"Afficher des informations sur l'usage de cette commande et quitter"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-file> is maintained as a Texinfo manual.  "
"If the B<info> and B<grub-file> programs are properly installed at your "
"site, the command"
msgstr ""
"La documentation complète de B<grub-file> est disponible dans un manuel "
"Texinfo. Si les programmes B<info> et B<grub-file> sont correctement "
"installés, la commande"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-file>"
msgstr "B<info grub-file>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "devrait vous donner accès au manuel complet."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "October 2023"
msgstr "Octobre 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13+deb12u1"
msgstr "GRUB 2.06-13+deb12u1"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "January 2024"
msgstr "Janvier 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-1"
msgstr "GRUB 2.12-1"
