# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.21.0\n"
"POT-Creation-Date: 2024-02-15 18:16+0100\n"
"PO-Revision-Date: 2024-02-24 09:33+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ZGREP"
msgstr "ZGREP"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "zgrep - search possibly compressed files for a regular expression"
msgstr ""
"zgrep – Chercher une expression rationnelle dans des fichiers "
"potentiellement compressés"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<zgrep> [ grep_options ] B<[\\ -e\\ ]>I< pattern> I<filename>.\\|.\\|."
msgstr "B<zgrep> [ grep_options ] B<[\\ -e\\ ]>I< motif> I<fichier>.\\|.\\|."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The B<zgrep> command invokes B<grep> on compressed or gzipped files.  All "
"options specified are passed directly to B<grep>.  If no file is specified, "
"then the standard input is decompressed if necessary and fed to grep.  "
"Otherwise the given files are uncompressed if necessary and fed to B<grep>."
msgstr ""
"La commande B<zgrep> invoque B<grep> sur des I<fichiers> compressés ou "
"traités par B<gzip>(1). Toutes les options spécifiées sont passées "
"directement à B<grep>. Si aucun fichier n'est indiqué, l'entrée standard est "
"décompressée si nécessaire et passée à B<grep>. Autrement, les fichiers "
"fournis sont décompressés si nécessaire et passés à B<grep>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If the GREP environment variable is set, B<zgrep> uses it as the B<grep> "
"program to be invoked."
msgstr ""
"Si la variable d'environnement GREP est définie, B<zgrep> l'utilise lors de "
"l'invocation du programme B<grep>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Exit status is 0 for a match, 1 for no matches, and 2 if trouble."
msgstr ""
"Le code de retour est B<0> pour une correspondance, B<1> s'il n'y a pas de "
"correspondance et B<2> en cas de problème."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The following B<grep> options are not supported: B<--dereference-recursive> "
"(B<-R>), B<--directories> (B<-d>), B<--exclude>, B<--exclude-from>, B<--"
"exclude-dir>, B<--include>, B<--null> (B<-Z>), B<--null-data> (B<-z>), and "
"B<--recursive> (B<-r>)."
msgstr ""
"Les options suivantes de B<grep> ne sont pas prises en charge : B<--"
"dereference-recursive> (B<-R>), B<--directories> (B<-d>), B<--exclude>, B<--"
"exclude-from>, B<--exclude-dir>, B<--include>, B<--null> (B<-Z>), B<--null-"
"data> (B<-z>) et B<--recursive> (B<-r>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Charles Levert (charles@comm.polymtl.ca)"
msgstr "Charles Levert (charles@comm.polymtl.ca)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<grep>(1), B<gzexe>(1), B<gzip>(1), B<zdiff>(1), B<zforce>(1), B<zmore>(1), "
"B<znew>(1)"
msgstr ""
"B<grep>(1), B<gzexe>(1), B<gzip>(1), B<zdiff>(1), B<zforce>(1), B<zmore>(1), "
"B<znew>(1)"

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"I<Zgrep> invokes I<grep> on compressed, xz'ed, lzma'ed, zstd'ed, bzip2'ed or "
"gzipped files.  All options specified are passed directly to I<grep>.  If no "
"file is specified, then the standard input is decompressed if necessary and "
"fed to grep.  Otherwise the given files are uncompressed if necessary and "
"fed to I<grep>."
msgstr ""
"B<zgrep> invoque B<grep> sur des I<fichiers> compressés au format xz, lzma, "
"zstd, bzip2 ou gzip. Toutes les options spécifiées sont passées directement "
"à B<grep>. Si aucun fichier n'est indiqué, l'entrée standard est "
"décompressée si nécessaire et passée à B<grep>. Autrement, les fichiers "
"fournis sont décompressés si nécessaire et passés à B<grep>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"If the GREP environment variable is set, I<zgrep> uses it as the I<grep> "
"program to be invoked."
msgstr ""
"Si la variable d'environnement GREP est définie, B<zgrep> l'utilise lors de "
"l'invocation du programme B<grep>."

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The following I<grep> options are not supported: B<--dereference-recursive> "
"(B<-R>), B<--directories> (B<-d>), B<--exclude>, B<--exclude-from>, B<--"
"exclude-dir>, B<--include>, B<--null> (B<-Z>), B<--null-data> (B<-z>), and "
"B<--recursive> (B<-r>)."
msgstr ""
"Les options suivantes de B<grep> ne sont pas prises en charge : B<--"
"dereference-recursive> (B<-R>), B<--directories> (B<-d>), B<--exclude>, B<--"
"exclude-from>, B<--exclude-dir>, B<--include>, B<--null> (B<-Z>), B<--null-"
"data> (B<-z>) et B<--recursive> (B<-r>)."

#. type: Plain text
#: opensuse-leap-15-6
msgid "grep(1), gzexe(1), gzip(1), zdiff(1), zforce(1), zmore(1), znew(1)"
msgstr "grep(1), gzexe(1), gzip(1), zdiff(1), zforce(1), zmore(1), znew(1)"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"The B<zgrep> command invokes B<grep> on compressed, xz'ed, lzma'ed, zstd'ed, "
"bzip2'ed or gzipped files.  All options specified are passed directly to "
"B<grep>.  If no file is specified, then the standard input is decompressed "
"if necessary and fed to grep.  Otherwise the given files are uncompressed if "
"necessary and fed to B<grep>."
msgstr ""
"La commande B<zgrep> invoque B<grep> sur des I<fichiers> compressés au "
"format xz, lzma, zstd, bzip2 ou gzip. Toutes les options spécifiées sont "
"passées directement à B<grep>. Si aucun fichier n'est indiqué, l'entrée "
"standard est décompressée si nécessaire et passée à B<grep>. Autrement, les "
"fichiers fournis sont décompressés si nécessaire et passés à B<grep>."
