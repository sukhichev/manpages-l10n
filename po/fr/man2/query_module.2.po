# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Thomas Vincent <tvincent@debian.org>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:05+0100\n"
"PO-Revision-Date: 2024-02-17 12:26+0100\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 1.8.11\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "query_module"
msgstr "query_module"

#. type: TH
#: archlinux fedora-40 fedora-rawhide
#, no-wrap
msgid "2023-10-31"
msgstr "31 octobre 2023"

#. type: TH
#: archlinux fedora-40 fedora-rawhide
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr "Pages du manuel de Linux 6.06"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "query_module - query the kernel for various bits pertaining to modules"
msgstr ""
"query_module - Interroger le noyau sur diverses choses relatives aux modules"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/module.hE<gt>>\n"
msgstr "B<#include E<lt>linux/module.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] int query_module(const char *>I<name>B<, int >I<which>B<,>\n"
"B<                                void >I<buf>B<[.>I<bufsize>B<], size_t >I<bufsize>B<,>\n"
"B<                                size_t *>I<ret>B<);>\n"
msgstr ""
"B<[[obsolète]] int query_module(const char *>I<name>B<, int >I<which>B<,>\n"
"B<                                void >I<buf>B<[.>I<bufsize>B<], size_t >I<bufsize>B<,>\n"
"B<                                size_t *>I<ret>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<Note>: This system call is present only before Linux 2.6."
msgstr "I<Remarque> : cet appel système n'est présent qu'avant Linux 2.6."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<query_module>()  requests information from the kernel about loadable "
"modules.  The returned information is placed in the buffer pointed to by "
"I<buf>.  The caller must specify the size of I<buf> in I<bufsize>.  The "
"precise nature and format of the returned information depend on the "
"operation specified by I<which>.  Some operations require I<name> to "
"identify a currently loaded module, some allow I<name> to be NULL, "
"indicating the kernel proper."
msgstr ""
"B<query_module>() demande au noyau des informations sur les modules "
"chargeables. L'information renvoyée est placée dans un tampon pointé par "
"I<buf>. L'appelant doit indiquer la taille de I<buf> dans I<bufsize>. La "
"nature précise et le format de l'information renvoyée dépendent de "
"l'opération qui a été spécifiée dans I<which>. Certaines opérations "
"nécessitent un I<name> pour identifier un module actuellement chargé, "
"d'autres permettent que I<name> soit NULL, indiquant le noyau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "The following values can be specified for I<which>:"
msgstr "I<which> peut prendre les valeurs suivantes\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<0>"
msgstr "B<0>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns success, if the kernel supports B<query_module>().  Used to probe "
"for availability of the system call."
msgstr ""
"Retourner avec succès si le noyau prend en charge B<query_module>(). Utilisé "
"pour tester la disponibilité de l'appel système."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<QM_MODULES>"
msgstr "B<QM_MODULES>"

#.  ret is set on ENOSPC
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns the names of all loaded modules.  The returned buffer consists of a "
"sequence of null-terminated strings; I<ret> is set to the number of modules."
msgstr ""
"Renvoyer les noms de tous les modules chargés. Le tampon renvoyé consiste en "
"une séquence de chaînes de caractères terminées par un octet nul\\ ; I<ret> "
"prend la valeur du nombre de modules."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<QM_DEPS>"
msgstr "B<QM_DEPS>"

#.  ret is set on ENOSPC
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns the names of all modules used by the indicated module.  The returned "
"buffer consists of a sequence of null-terminated strings; I<ret> is set to "
"the number of modules."
msgstr ""
"Renvoyer le nom de tous les modules utilisés par le module indiqué. Le "
"tampon renvoyé consiste en une séquence de chaînes de caractères terminées "
"par un octet nul\\ ; I<ret> prend la valeur du nombre de modules."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<QM_REFS>"
msgstr "B<QM_REFS>"

#.  ret is set on ENOSPC
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns the names of all modules using the indicated module.  This is the "
"inverse of B<QM_DEPS>.  The returned buffer consists of a sequence of null-"
"terminated strings; I<ret> is set to the number of modules."
msgstr ""
"Renvoyer le nom de tous les modules utilisant le module indiqué. C'est "
"l'inverse de B<QM_DEPS>. Le tampon renvoyé consiste en une séquence de "
"chaînes de caractères terminées par un octet nul\\ ; I<ret> prend la valeur "
"du nombre de modules."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<QM_SYMBOLS>"
msgstr "B<QM_SYMBOLS>"

#.  ret is set on ENOSPC
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns the symbols and values exported by the kernel or the indicated "
"module.  The returned buffer is an array of structures of the following form"
msgstr ""
"Renvoyer les symboles et valeurs exportés par le noyau ou le module indiqué. "
"Le tampon renvoyé est un tableau de structures de la forme suivante\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct module_symbol {\n"
"    unsigned long value;\n"
"    unsigned long name;\n"
"};\n"
msgstr ""
"struct module_symbol {\n"
"    unsigned long value;\n"
"    unsigned long name;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"followed by null-terminated strings.  The value of I<name> is the character "
"offset of the string relative to the start of I<buf>; I<ret> is set to the "
"number of symbols."
msgstr ""
"suivi par des chaînes de caractères terminées par un octet nul. La valeur de "
"I<name> est le décalage en caractère de la chaîne relativement au début de "
"I<buf>\\ ; I<ret> prend la valeur du nombre de symboles."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<QM_INFO>"
msgstr "B<QM_INFO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Returns miscellaneous information about the indicated module.  The output "
"buffer format is:"
msgstr ""
"Renvoyer diverses informations sur le module indiqué. Le format du tampon de "
"sortie est de la forme\\ :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct module_info {\n"
"    unsigned long address;\n"
"    unsigned long size;\n"
"    unsigned long flags;\n"
"};\n"
msgstr ""
"struct module_info {\n"
"    unsigned long address;\n"
"    unsigned long size;\n"
"    unsigned long flags;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"where I<address> is the kernel address at which the module resides, I<size> "
"is the size of the module in bytes, and I<flags> is a mask of "
"B<MOD_RUNNING>, B<MOD_AUTOCLEAN>, and so on, that indicates the current "
"status of the module (see the Linux kernel source file I<include/linux/"
"module.h>).  I<ret> is set to the size of the I<module_info> structure."
msgstr ""
"où I<address> est l'adresse noyau où se trouve le module, I<size> est la "
"taille en octets du module et I<flags> un masque de B<MOD_RUNNING>, "
"B<MOD_AUTOCLEAN>, etc., qui indique l'état actuel du module (consultez le "
"fichier I<include/linux/module.h> dans les sources du noyau Linux). I<ret> "
"prend la valeur de la taille de la structure I<module_info>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned and I<errno> is set "
"to indicate the error."
msgstr ""
"En cas de succès, B<0> est renvoyé. en cas d'échec, B<-1> est renvoyé et "
"I<errno> se positionné pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At least one of I<name>, I<buf>, or I<ret> was outside the program's "
"accessible address space."
msgstr ""
"Au moins une adresse parmi I<name>, I<buf> ou I<ret> est en dehors de "
"l'espace d'adressage accessible du programme."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#.  Not permitted with QM_DEPS, QM_REFS, or QM_INFO.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Invalid I<which>; or I<name> is NULL (indicating \"the kernel\"), but this "
"is not permitted with the specified value of I<which>."
msgstr ""
"I<which> est invalide ou I<name> est NULL (indiquant le noyau) mais ce n'est "
"pas permis avec la valeur de I<which>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "No module by that I<name> exists."
msgstr "Aucun module du nom I<name> n'existe."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The buffer size provided was too small.  I<ret> is set to the minimum size "
"needed."
msgstr ""
"La taille du tampon fournie est trop petite\\ ; I<ret> prend la valeur de la "
"taille minimum nécessaire."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<query_module>()  is not supported in this version of the kernel (e.g., "
"Linux 2.6 or later)."
msgstr ""
"B<query_module>() n'est pas pris en charge par cette version du noyau (par "
"exemple, Linux 2.6 ou au-delà)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#.  Removed in Linux 2.5.48
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "Removed in Linux 2.6."
msgstr "Supprimé dans Linux 2.6"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Some of the information that was formerly available via B<query_module>()  "
"can be obtained from I</proc/modules>, I</proc/kallsyms>, and the files "
"under the directory I</sys/module>."
msgstr ""
"Certains renseignements, qui étaient auparavant disponibles avec "
"B<query_module>(), peuvent être obtenus dans I</proc/modules>, I</proc/"
"kallsyms> et les fichiers du répertoire I</sys/modules>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<query_module>()  system call is not supported by glibc.  No "
"declaration is provided in glibc headers, but, through a quirk of history, "
"glibc does export an ABI for this system call.  Therefore, in order to "
"employ this system call, it is sufficient to manually declare the interface "
"in your code; alternatively, you can invoke the system call using "
"B<syscall>(2)."
msgstr ""
"L'appel système B<query_module>() n'est pas pris en charge par la glibc. Il "
"n'est pas déclaré dans les en-têtes de la glibc, mais par un caprice de "
"l'histoire, la glibc fournit une interface binaire pour cet appel système. "
"Ainsi, il suffit de déclarer manuellement l'interface dans votre code pour "
"utiliser cet appel système. Sinon, vous pouvez l'invoquer en utilisant "
"B<syscall>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<create_module>(2), B<delete_module>(2), B<get_kernel_syms>(2), "
"B<init_module>(2), B<lsmod>(8), B<modinfo>(8)"
msgstr ""
"B<create_module>(2), B<delete_module>(2), B<get_kernel_syms>(2), "
"B<init_module>(2), B<lsmod>(8), B<modinfo>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "4 décembre 2022"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#.  Removed in Linux 2.5.48
#. type: Plain text
#: debian-bookworm
msgid ""
"This system call is present only up until Linux 2.4; it was removed in Linux "
"2.6."
msgstr ""
"Cet appel système n'est présent que jusqu'à Linux\\ 2.4\\ ; il a été "
"supprimé dans Linux\\ 2.6."

#. type: Plain text
#: debian-bookworm
msgid "B<query_module>()  is Linux-specific."
msgstr "B<query_module>() est spécifique à Linux."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Pages du manuel de Linux 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pages du manuel de Linux 6.04"
