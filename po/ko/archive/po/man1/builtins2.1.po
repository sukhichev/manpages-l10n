# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 배성훈 <plodder@kldp.org>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-11-14 19:24+0100\n"
"PO-Revision-Date: 2000-08-20 08:57+0900\n"
"Last-Translator: 배성훈 <plodder@kldp.org>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: builtins2.1:3
#, no-wrap
msgid "BASH_BUILTINS"
msgstr "BASH_BUILTINS"

#. type: TH
#: builtins2.1:3
#, no-wrap
msgid "1996 March 20"
msgstr "1996년 3월 20일"

#. type: TH
#: builtins2.1:3
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: SH
#: builtins2.1:4
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: builtins2.1:11
msgid ""
"bash, :, ., alias, bg, bind, break, builtin, case, cd, command, continue, "
"declare, dirs, disown, bash-echo, enable, eval, exec, exit, bash-export, fc, "
"fg, for, getopts, hash, help, history, if, jobs, bash-kill, let, local, "
"logout, popd, pushd, bash-pwd, read, readonly, return, set, shift, shopt, "
"source, suspend, bash-test, times, trap, type, typeset, ulimit, umask, "
"unalias, unset, until, wait, while - bash built-in commands, see B<bash>(1)"
msgstr ""
"bash, :, ., alias, bg, bind, break, builtin, case, cd, command, continue, "
"declare, dirs, disown, bash-echo, enable, eval, exec, exit, bash-export, fc, "
"fg, for, getopts, hash, help, history, if, jobs, bash-kill, let, local, "
"logout, popd, pushd, bash-pwd, read, readonly, return, set, shift, shopt, "
"source, suspend, bash-test, times, trap, type, typeset, ulimit, umask, "
"unalias, unset, until, wait, while - bash built-in 명령어들, B<bash>(1)를 참고"
"하라."

#. type: SH
#: builtins2.1:11
#, no-wrap
msgid "BASH BUILTIN COMMANDS"
msgstr "BASH BUILTIN 명령어들"

#. type: SH
#: builtins2.1:14
#, no-wrap
msgid "SEE ALSO"
msgstr "관련 항목"

#. type: Plain text
#: builtins2.1:16
msgid "B<bash>(1), B<sh>(1)"
msgstr "B<bash>(1), B<sh>(1)"
