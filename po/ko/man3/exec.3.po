# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 정강훈 <skyeyes@soback.kornet.net>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:55+0100\n"
"PO-Revision-Date: 2001-03-08 08:57+0900\n"
"Last-Translator: 정강훈 <skyeyes@soback.kornet.net>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "exec"
msgstr "exec"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "execl, execlp, execle, execv, execvp, execvpe - execute a file"
msgstr "execl, execlp, execle, execv, execvp, execvpe - 파일 실행하기"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<extern char **environ;>\n"
msgstr "B<extern char **environ;>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int execl(const char *>I<pathname>B<, const char *>I<arg>B<, ...>\n"
"B</*, (char *) NULL */);>\n"
"B<int execlp(const char *>I<file>B<, const char *>I<arg>B<, ...>\n"
"B</*, (char *) NULL */);>\n"
"B<int execle(const char *>I<pathname>B<, const char *>I<arg>B<, ...>\n"
"B<                /*, (char *) NULL, char *const >I<envp>B<[] */);>\n"
"B<int execv(const char *>I<pathname>B<, char *const >I<argv>B<[]);>\n"
"B<int execvp(const char *>I<file>B<, char *const >I<argv>B<[]);>\n"
"B<int execvpe(const char *>I<file>B<, char *const >I<argv>B<[], char *const >I<envp>B<[]);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<execvpe>():"
msgstr "B<execvpe>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<exec> family of functions replaces the current process image with a "
#| "new process image.  The functions described in this manual page are front-"
#| "ends for the function B<execve>(2).  (See the manual page for B<execve> "
#| "for detailed information about the replacement of the current process.)"
msgid ""
"The B<exec>()  family of functions replaces the current process image with a "
"new process image.  The functions described in this manual page are layered "
"on top of B<execve>(2).  (See the manual page for B<execve>(2)  for further "
"details about the replacement of the current process image.)"
msgstr ""
"B<exec> 함수 계열은 현재 프로세스 이미지를 새로운 프로세스 이미지로 바꾼다.  "
"이 매뉴얼에 기술된 함수들은 B<execve>(2)  함수의 전위들이다.  (현재 프로세스"
"의 대체에 관한 세부적인 정보를 원하면 B<execve> 매뉴얼 페이지를 참고해라.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The initial argument for these functions is the pathname of a file which "
#| "is to be executed."
msgid ""
"The initial argument for these functions is the name of a file that is to be "
"executed."
msgstr "이들 함수의 초기 인자는 실행되어야 하는 파일의 경로명이다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The functions can be grouped based on the letters following the \"exec\" "
"prefix."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "l - execl(), execlp(), execle()"
msgstr "l - execl(), execlp(), execle()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<const char *arg> and subsequent ellipses in the B<execl>, "
#| "B<execlp>, and B<execle> functions can be thought of as I<arg0>, I<arg1>, "
#| "\\&..., I<argn>.  Together they describe a list of one or more pointers "
#| "to null-terminated strings that represent the argument list available to "
#| "the executed program.  The first argument, by convention, should point to "
#| "the file name associated with the file being executed.  The list of "
#| "arguments I<must> be terminated by a B<NULL> pointer."
msgid ""
"The I<const char\\ *arg> and subsequent ellipses can be thought of as "
"I<arg0>, I<arg1>, \\&..., I<argn>.  Together they describe a list of one or "
"more pointers to null-terminated strings that represent the argument list "
"available to the executed program.  The first argument, by convention, "
"should point to the filename associated with the file being executed.  The "
"list of arguments I<must> be terminated by a null pointer, and, since these "
"are variadic functions, this pointer must be cast I<(char\\ *) NULL>."
msgstr ""
"I<const char *arg> 와 B<execl>, B<execlp>, 그리고 B<execle> 함수들에 있는 연"
"속적인 것들은 I<arg0>, I<arg1>, \\&..., I<argn> 등으로 생각할 수 있다.  그것"
"들은 실행 프로그램이 이용할 수 있는 인자 리스트를 나타내는 하나나 그이상의 "
"null로 끝난 문자열을 가리킨다. 편의를 위해, 처음 인자는 실행되어야 할 파일의 "
"이름이다.  인자 리스트는 반드시 B<NULL> 포인터로 끝나야 한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"By contrast with the 'l' functions, the 'v' functions (below) specify the "
"command-line arguments of the executed program as a vector."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "v - execv(), execvp(), execvpe()"
msgstr "v - execv(), execvp(), execvpe()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<execv> and B<execvp> functions provide an array of pointers to null-"
#| "terminated strings that represent the argument list available to the new "
#| "program.  The first argument, by convention, should point to the file "
#| "name associated with the file being executed.  The array of pointers "
#| "I<must> be terminated by a B<NULL> pointer."
msgid ""
"The I<char\\ *const argv[]> argument is an array of pointers to null-"
"terminated strings that represent the argument list available to the new "
"program.  The first argument, by convention, should point to the filename "
"associated with the file being executed.  The array of pointers I<must> be "
"terminated by a null pointer."
msgstr ""
"B<execv> 와 B<execvp> 함수는 새로운 프로그램이 이용할수 있는 인자 리스트를 나"
"타내는 null로 끝난 문자열의 포인터 배열을 제공한다.  편리를 위해, 첫 인자는 "
"실행되어야 할 파일과 관련된 파일 이름 을 가리켜야 한다. 포인터 배열은 반드시 "
"B<NULL> 포인터로 끝나야 한다."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "e - execle(), execvpe()"
msgstr "e - execle(), execvpe()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The environment of the new process image is specified via the argument "
"I<envp>.  The I<envp> argument is an array of pointers to null-terminated "
"strings and I<must> be terminated by a null pointer."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All other B<exec>()  functions (which do not include 'e' in the suffix)  "
"take the environment for the new process image from the external variable "
"I<environ> in the calling process."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "p - execlp(), execvp(), execvpe()"
msgstr "p - execlp(), execvp(), execvpe()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The functions B<execlp> and B<execvp> will duplicate the actions of the "
#| "shell in searching for an executable file if the specified file name does "
#| "not contain a slash (/) character.  The search path is the path specified "
#| "in the environment by the B<PATH> variable.  If this variable isn't "
#| "specified, the default path ``:/bin:/usr/bin'' is used.  In addition, "
#| "certain errors are treated specially."
msgid ""
"These functions duplicate the actions of the shell in searching for an "
"executable file if the specified filename does not contain a slash (/) "
"character.  The file is sought in the colon-separated list of directory "
"pathnames specified in the B<PATH> environment variable.  If this variable "
"isn't defined, the path list defaults to a list that includes the "
"directories returned by I<confstr(_CS_PATH)> (which typically returns the "
"value \"/bin:/usr/bin\")  and possibly also the current working directory; "
"see NOTES for further details."
msgstr ""
"만일 지정된 파일 이름이 slash(/) 문자를 포함하지 않는다면 B<execlp> 와 "
"B<execvp> 함수는 실행파일을 찾기 위해 shell 의 행동을 복사한다.  탐색 경로는 "
"B<PATH> 변수에 지정된 경로이다.  이 변수가 지정되지 않는다면, 기본 경로 ``:/"
"bin:/usr/bin''가 사용된다.  추가적으로, 어떤 에러들은 특별하게 처리된다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<execvpe>()  searches for the program using the value of B<PATH> from the "
"caller's environment, not from the I<envp> argument."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the specified filename includes a slash character, then B<PATH> is "
"ignored, and the file at the specified pathname is executed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "In addition, certain errors are treated specially."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If permission is denied for a file (the attempted B<execve> returned "
#| "B<EACCES>), these functions will continue searching the rest of the "
#| "search path.  If no other file is found, however, they will return with "
#| "the global variable I<errno> set to B<EACCES>."
msgid ""
"If permission is denied for a file (the attempted B<execve>(2)  failed with "
"the error B<EACCES>), these functions will continue searching the rest of "
"the search path.  If no other file is found, however, they will return with "
"I<errno> set to B<EACCES>."
msgstr ""
"만일 허가권이 파일에 금지되어 있다면 ( B<execve> 는 B<EACCES>를 반환한다.), "
"이들 함수들은 탐색 경로의 나머지를 계속해서 탐색한다.  그러나 만일 어떤 다른 "
"파일이 발견된다면, 전역 변수 I<errno> 를 B<EACCES>로 설정하고 반환된다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the header of a file isn't recognized (the attempted B<execve> "
#| "returned B<ENOEXEC>), these functions will execute the shell with the "
#| "path of the file as its first argument.  (If this attempt fails, no "
#| "further searching is done.)"
msgid ""
"If the header of a file isn't recognized (the attempted B<execve>(2)  failed "
"with the error B<ENOEXEC>), these functions will execute the shell (I</bin/"
"sh>)  with the path of the file as its first argument.  (If this attempt "
"fails, no further searching is done.)"
msgstr ""
"만일 파일의 헤더가 인식되지 않는다면( B<execve> 는 B<ENOEXEC>를 반환한다), 이"
"들 함수들은 첫 인자로써 파일의 경로에 있는 shell을 실행한다.  (만일 이들 시도"
"가 실패한다면, 추가적인 탐색은 행해지지 않는다.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"All other B<exec>()  functions (which do not include 'p' in the suffix)  "
"take as their first argument a (relative or absolute) pathname that "
"identifies the program to be executed."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If any of the B<exec> functions returns, an error will have occurred.  "
#| "The return value is -1, and the global variable I<errno> will be set to "
#| "indicate the error."
msgid ""
"The B<exec>()  functions return only if an error has occurred.  The return "
"value is -1, and I<errno> is set to indicate the error."
msgstr ""
"만일 B<exec> 함수들이 반환한다면, 에러가 일어난 것이다.  반환 값은 -1이며, 전"
"역 변수 I<errno> 는 에러를 가리키도록 설정된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "All of these functions may fail and set I<errno> for any of the errors "
#| "specified for the library function B<execve>(2)."
msgid ""
"All of these functions may fail and set I<errno> for any of the errors "
"specified for B<execve>(2)."
msgstr ""
"이들 함수 모두는 실패할수 있으며 라이브러리 함수 B<execve>(2)에 지정된 에러"
"중 어떤 것으로 I<errno> 가 설정된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "속성"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"이 섹션에서 사용되는 용어에 대한 설명은 B<attributes>(7)을 참조하십시오."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "상호 작용"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "속성"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "번호"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<execl>(),\n"
"B<execle>(),\n"
"B<execv>()"
msgstr ""
"B<execl>(),\n"
"B<execle>(),\n"
"B<execv>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<execlp>(),\n"
"B<execvp>(),\n"
"B<execvpe>()"
msgstr ""
"B<execlp>(),\n"
"B<execvp>(),\n"
"B<execvpe>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#.  glibc commit 1eb8930608705702d5746e5491bab4e4429fcb83
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The default search path (used when the environment does not contain the "
"variable B<PATH>)  shows some variation across systems.  It generally "
"includes I</bin> and I</usr/bin> (in that order) and may also include the "
"current working directory.  On some other systems, the current working is "
"included after I</bin> and I</usr/bin>, as an anti-Trojan-horse measure.  "
"The glibc implementation long followed the traditional default where the "
"current working directory is included at the start of the search path.  "
"However, some code refactoring during the development of glibc 2.24 caused "
"the current working directory to be dropped altogether from the default "
"search path.  This accidental behavior change is considered mildly "
"beneficial, and won't be reverted."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The behavior of B<execlp>()  and B<execvp>()  when errors occur while "
"attempting to execute the file is historic practice, but has not "
"traditionally been documented and is not specified by the POSIX standard.  "
"BSD (and possibly other systems) do an automatic sleep and retry if "
"B<ETXTBSY> is encountered.  Linux treats it as a hard error and returns "
"immediately."
msgstr ""
"파일을 실행하는 동안 에러가 나타났을 때 B<execlp>() 그리고 B<execvp>() 의 행"
"동은 역사적 관습에 따른다. 그러나 전통적으로 문서화되지 않으며 POSIX 표준에"
"도 지정되지 않는다. BSD(그리고 다른 시스템)는 자동적으로 sleep을 하며 만일 "
"B<ETXTBSY>를 만난다면 다시 시도한다.  Linux는 어려운 에러로 대하고 바로 반환"
"한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Traditionally, the functions B<execlp>()  and B<execvp>()  ignored all "
"errors except for the ones described above and B<ENOMEM> and B<E2BIG>, upon "
"which they returned.  They now return if any error other than the ones "
"described above occurs."
msgstr ""
"전통적으로, 함수 B<execlp>() 와 B<execvp>() 는 위에 기술된 것들 그리고 "
"B<ENOMEM> 그리고 B<E2BIG>를 제외한 모든 에러를 무시하며 리턴된다.  이들 함수"
"는 위에 기술된 것들 이외의 에러를 만난다면 바로 리턴된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "I<Environment=>"
msgid "B<environ>"
msgstr "I<Environment=>"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<exec>"
msgid "B<execl>()"
msgstr "B<exec>"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<execvpe>():"
msgid "B<execlp>()"
msgstr "B<execvpe>():"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<execvpe>():"
msgid "B<execle>()"
msgstr "B<execvpe>():"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<execvpe>():"
msgid "B<execv>()"
msgstr "B<execvpe>():"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<execvpe>():"
msgid "B<execvp>()"
msgstr "B<execvpe>():"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<execvpe>():"
msgid "B<execvpe>()"
msgstr "B<execvpe>():"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "GNU."
msgstr "GNU."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Since glibc 2.19:"
msgid "glibc 2.11."
msgstr "glibc 2.19부터:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "버그"

#.  https://sourceware.org/bugzilla/show_bug.cgi?id=19534
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before glibc 2.24, B<execl>()  and B<execle>()  employed B<realloc>(3)  "
"internally and were consequently not async-signal-safe, in violation of the "
"requirements of POSIX.1.  This was fixed in glibc 2.24."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Architecture-specific details"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On sparc and sparc64, B<execv>()  is provided as a system call by the kernel "
"(with the prototype shown above)  for compatibility with SunOS.  This "
"function is I<not> employed by the B<execv>()  wrapper function on those "
"architectures."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sh>(1), B<execve>(2), B<execveat>(2), B<fork>(2), B<ptrace>(2), "
"B<fexecve>(3), B<system>(3), B<environ>(7)"
msgstr ""
"B<sh>(1), B<execve>(2), B<execveat>(2), B<fork>(2), B<ptrace>(2), "
"B<fexecve>(3), B<system>(3), B<environ>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-01-07"
msgstr "2023년 1월 7일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid "The B<execvpe>()  function first appeared in glibc 2.11."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
#, fuzzy
#| msgid "This function is a GNU extension."
msgid "The B<execvpe>()  function is a GNU extension."
msgstr "이 함수는 GNU 확장이다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "2023년 7월 20일"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
