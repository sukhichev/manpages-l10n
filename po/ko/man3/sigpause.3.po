# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:08+0100\n"
"PO-Revision-Date: 2000-07-26 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "sigpause"
msgstr "sigpause"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sigpause - atomically release blocked signals and wait for interrupt"
msgstr "sigpause - 블록화된 시그널을 자동으로 릴리즈하고 인터럽트 대기한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sigpause(int >I<sigmask>B<);>"
msgid "B<[[deprecated]] int sigpause(int >I<sigmask>B<);  /* BSD (but see NOTES) */>\n"
msgstr "B<int sigpause(int >I<sigmask>B<);>"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"
msgid "B<[[deprecated]] int sigpause(int >I<sig>B<);      /* POSIX.1 / SysV / UNIX 95 */>\n"
msgstr "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Don't use this function.  Use B<sigsuspend>(2)  instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sigpause> assigns I<sigmask> to the set of masked signals and then "
#| "waits for a signal to arrive; on return the set of masked signals is "
#| "restored."
msgid ""
"The function B<sigpause>()  is designed to wait for some signal.  It changes "
"the process's signal mask (set of blocked signals), and then waits for a "
"signal to arrive.  Upon arrival of a signal, the original signal mask is "
"restored."
msgstr ""
"B<sigpause> 는 masked signal의 설정을 위해 I<sigmask> 를 할당하고, 시그널이 "
"도착하길 기다린다; 반환시 masked signal 설정은 재저장된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If B<sigpause>()  returns, it was interrupted by a signal and the return "
"value is -1 with I<errno> set to B<EINTR>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "속성"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"이 섹션에서 사용되는 용어에 대한 설명은 B<attributes>(7)을 참조하십시오."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "상호 작용"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "속성"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "번호"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ".na\n"

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ".nh\n"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<sigpause>()"
msgstr "B<sigpause>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, this routine is a system call only on the Sparc (sparc64)  "
"architecture."
msgstr ""

#.  Libc4 and libc5 know only about the BSD version.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"glibc uses the BSD version if the B<_BSD_SOURCE> feature test macro is "
"defined and none of B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, B<_XOPEN_SOURCE>, "
"B<_GNU_SOURCE>, or B<_SVID_SOURCE> is defined.  Otherwise, the System V "
"version is used, and feature test macros must be defined as follows to "
"obtain the declaration:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#.  || (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Since glibc 2.26: _XOPEN_SOURCE E<gt>= 500"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.25 and earlier: _XOPEN_SOURCE"
msgstr ""

#
#.  For the BSD version, one usually uses a zero
#.  .I sigmask
#.  to indicate that no signals are to be blocked.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since glibc 2.19, only the System V version is exposed by I<E<lt>signal."
"hE<gt>>; applications that formerly used the BSD B<sigpause>()  should be "
"amended to use B<sigsuspend>(2)."
msgstr ""

#. #-#-#-#-#  archlinux: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  debian-unstable: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-40: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  fedora-rawhide: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-6: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, POSIX.1-2008."
msgid "POSIX.1-2001.  Obsoleted in POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#.  __xpg_sigpause: UNIX 95, spec 1170, SVID, SVr4, XPG
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The classical BSD version of this function appeared in 4.2BSD.  It sets the "
"process's signal mask to I<sigmask>.  UNIX 95 standardized the incompatible "
"System V version of this function, which removes only the specified signal "
"I<sig> from the process's signal mask.  The unfortunate situation with two "
"incompatible functions with the same name was solved by the B<\\"
"%sigsuspend>(2)  function, that takes a I<sigset_t\\ *> argument (instead of "
"an I<int>)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<sigaction>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigblock>(3), B<sigvec>(3), B<feature_test_macros>(7)"
msgstr ""
"B<kill>(2), B<sigaction>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigblock>(3), B<sigvec>(3), B<feature_test_macros>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "B<int sigpause(int >I<sigmask>B<);>"
msgid "B<int sigpause(int >I<sigmask>B<);  /* BSD (but see NOTES) */>\n"
msgstr "B<int sigpause(int >I<sigmask>B<);>"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"
msgstr "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"

#. type: Plain text
#: debian-bookworm
msgid ""
"The System V version of B<sigpause>()  is standardized in POSIX.1-2001.  It "
"is also specified in POSIX.1-2008, where it is marked obsolete."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: SS
#: debian-bookworm
#, no-wrap
msgid "History"
msgstr ""

#. type: SS
#: debian-bookworm
#, fuzzy, no-wrap
#| msgid "Linux"
msgid "Linux notes"
msgstr "리눅스"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr "2023년 7월 20일"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
