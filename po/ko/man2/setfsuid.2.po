# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 정강훈 <skyeyes@soback.kornet.net>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 17:07+0100\n"
"PO-Revision-Date: 2000-04-30 08:57+0900\n"
"Last-Translator: 정강훈 <skyeyes@soback.kornet.net>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "setfsuid"
msgstr "setfsuid"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "setfsuid - set user identity used for filesystem checks"
msgstr "setfsuid - 파일 시스템 검사를 위해 사용되는 사용자 식별자를 설정한다"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/fsuid.hE<gt>>\n"
msgstr "B<#include E<lt>sys/fsuid.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int setfsuid(uid_t >I<fsuid>B<)>"
msgid "B<[[deprecated]] int setfsuid(uid_t >I<fsuid>B<);>\n"
msgstr "B<int setfsuid(uid_t >I<fsuid>B<)>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On Linux, a process has both a filesystem user ID and an effective user ID.  "
"The (Linux-specific) filesystem user ID is used for permissions checking "
"when accessing filesystem objects, while the effective user ID is used for "
"various other kinds of permissions checks (see B<credentials>(7))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Normally, the value of the process's filesystem user ID is the same as the "
"value of its effective user ID.  This is so, because whenever a process's "
"effective user ID is changed, the kernel also changes the filesystem user ID "
"to be the same as the new value of the effective user ID.  A process can "
"cause the value of its filesystem user ID to diverge from its effective user "
"ID by using B<setfsuid>()  to change its filesystem user ID to the value "
"given in I<fsuid>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "An explict call to B<setfsuid> is usually only used by programs such as "
#| "the Linux NFS server that need to change what user ID is used for file "
#| "access without a corresponding change in the real and effective user IDs. "
#| "A change in the normal user IDs for a program such as the NFS server is a "
#| "security hole that can expose it to unwanted signals from other user IDs."
msgid ""
"Explicit calls to B<setfsuid>()  and B<setfsgid>(2)  are (were) usually used "
"only by programs such as the Linux NFS server that need to change what user "
"and group ID is used for file access without a corresponding change in the "
"real and effective user and group IDs.  A change in the normal user IDs for "
"a program such as the NFS server is (was) a security hole that can expose it "
"to unwanted signals.  (However, this issue is historical; see below.)"
msgstr ""
"대개 명시적인 B<setfsuid> 호출은 그룹 ID의 변경이 필요한 리눅스 NFS 서버 같"
"은 프로그램에서만 사용된다.  (실제 사용자 ID와 유효 사용자 ID에 변경없이 파"
"일 접근을 위해 사용되는 사용자ID를 바꾼다.)  NFS 서버같은 프로그램에서 일반 "
"사용자 ID의 변경은 다른 사용자 ID로 부터 원하지 않는 신호를 받을 수 있는 보"
"안 구멍이 될 수 있다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<setfsuid> will only succeed if the caller is the superuser or if "
#| "I<fsuid> matches either the real user ID, effective user ID, saved set-"
#| "user-ID, or the current value of I<fsuid>."
msgid ""
"B<setfsuid>()  will succeed only if the caller is the superuser or if "
"I<fsuid> matches either the caller's real user ID, effective user ID, saved "
"set-user-ID, or current filesystem user ID."
msgstr ""
"호출한 프로세스가 슈퍼유저이거나 또는 I<fsuid>가 실제 사용자 ID나 유효 사용"
"자 ID, 저장된(saved) 사용자 ID, 또는 I<fsuid>의 현재값과 일치한다면 "
"B<setfsuid>는 성공할 것이다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On both success and failure, this call returns the previous filesystem user "
"ID of the caller."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr "리눅스."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#.  Linux 1.1.44
#.  and in libc since libc 4.7.6.
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 1.2."
msgstr "Linux 1.2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"At the time when this system call was introduced, one process could send a "
"signal to another process with the same effective user ID.  This meant that "
"if a privileged process changed its effective user ID for the purpose of "
"file permission checking, then it could become vulnerable to receiving "
"signals sent by another (unprivileged) process with the same user ID.  The "
"filesystem user ID attribute was thus added to allow a process to change its "
"user ID for the purposes of file permission checking without at the same "
"time becoming vulnerable to receiving unwanted signals.  Since Linux 2.0, "
"signal permission handling is different (see B<kill>(2)), with the result "
"that a process can change its effective user ID without being vulnerable to "
"receiving signals from unwanted processes.  Thus, B<setfsuid>()  is nowadays "
"unneeded and should be avoided in new applications (likewise for "
"B<setfsgid>(2))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The original Linux B<setfsuid>()  system call supported only 16-bit user "
"IDs.  Subsequently, Linux 2.4 added B<setfsuid32>()  supporting 32-bit IDs.  "
"The glibc B<setfsuid>()  wrapper function transparently deals with the "
"variation across kernel versions."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In glibc 2.15 and earlier, when the wrapper for this system call determines "
"that the argument can't be passed to the kernel without integer truncation "
"(because the kernel is old and does not support 32-bit user IDs), it will "
"return -1 and set I<errno> to B<EINVAL> without attempting the system call."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"No error indications of any kind are returned to the caller, and the fact "
"that both successful and unsuccessful calls return the same value makes it "
"impossible to directly determine whether the call succeeded or failed.  "
"Instead, the caller must resort to looking at the return value from a "
"further call such as I<setfsuid(-1)> (which will always fail), in order to "
"determine if a preceding call to B<setfsuid>()  changed the filesystem user "
"ID.  At the very least, B<EPERM> should be returned when the call fails "
"(because the caller lacks the B<CAP_SETUID> capability)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<kill>(2), B<setfsgid>(2), B<capabilities>(7), B<credentials>(7)"
msgstr "B<kill>(2), B<setfsgid>(2), B<capabilities>(7), B<credentials>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "2022년 12월 4일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid "B<int setfsuid(uid_t >I<fsuid>B<);>\n"
msgstr "B<int setfsuid(uid_t >I<fsuid>B<);>\n"

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#.  This system call is present since Linux 1.1.44
#.  and in libc since libc 4.7.6.
#. type: Plain text
#: debian-bookworm
msgid "This system call is present since Linux 1.2."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<setfsuid>()  is Linux-specific and should not be used in programs intended "
"to be portable."
msgstr ""
"B<setfsuid>()는 리눅스에 한정적하며, 호환성을 염두에 둔 프로그램에서 사용해서"
"는 안된다."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
