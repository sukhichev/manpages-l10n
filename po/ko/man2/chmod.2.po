# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# ASPLINUX <man@asp-linux.co.kr>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:54+0100\n"
"PO-Revision-Date: 2000-07-29 08:57+0900\n"
"Last-Translator: ASPLINUX <man@asp-linux.co.kr>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "chmod"
msgstr "chmod"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "chmod, fchmod, fchmodat - change permissions of a file"
msgstr "chmod, fchmod, fchmodat - 파일의 권한 변경"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr "B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int chmod(const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"
"B<int fchmod(int >I<fd>B<, mode_t >I<mode>B<);>\n"
msgstr ""
"B<int chmod(const char *>I<pathname>B<, mode_t >I<mode>B<);>\n"
"B<int fchmod(int >I<fd>B<, mode_t >I<mode>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>sys/types.hE<gt>>\n"
#| "B<#include E<lt>sys/ipc.hE<gt>>\n"
#| "B<#include E<lt>sys/sem.hE<gt>>\n"
msgid ""
"B<#include E<lt>fcntl.hE<gt>>           /* Definition of AT_* constants */\n"
"B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/ipc.hE<gt>>\n"
"B<#include E<lt>sys/sem.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int fchmodat(int >I<dirfd>B<, const char *>I<pathname>B<, mode_t >I<mode>B<, int >I<flags>B<);>\n"
msgstr "B<int fchmodat(int >I<dirfd>B<, const char *>I<pathname>B<, mode_t >I<mode>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#.         || (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#.         || (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<fchmod>():\n"
"    Since glibc 2.24:\n"
"        _POSIX_C_SOURCE E<gt>= 199309L\n"
"    glibc 2.19 to glibc 2.23\n"
"        _POSIX_C_SOURCE\n"
"    glibc 2.16 to glibc 2.19:\n"
"        _BSD_SOURCE || _POSIX_C_SOURCE\n"
"    glibc 2.12 to glibc 2.16:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
"            || _POSIX_C_SOURCE E<gt>= 200809L\n"
"    glibc 2.11 and earlier:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fchmodat>():"
msgstr "B<fchmodat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<chmod>()  and B<fchmod>()  system calls change a file's mode bits.  "
"(The file mode consists of the file permission bits plus the set-user-ID, "
"set-group-ID, and sticky bits.)  These system calls differ only in how the "
"file is specified:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chmod>()  changes the mode of the file specified whose pathname is given "
"in I<pathname>, which is dereferenced if it is a symbolic link."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fchmod>()  changes the mode of the file referred to by the open file "
"descriptor I<fd>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The new file mode is specified in I<mode>, which is a bit mask created by "
"ORing together zero or more of the following:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_ISUID>  (04000)"
msgstr "B<S_ISUID>  (04000)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "set-user-ID (set process effective user ID on B<execve>(2))"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_ISGID>  (02000)"
msgstr "B<S_ISGID>  (02000)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"set-group-ID (set process effective group ID on B<execve>(2); mandatory "
"locking, as described in B<fcntl>(2); take a new file's group from parent "
"directory, as described in B<chown>(2)  and B<mkdir>(2))"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_ISVTX>  (01000)"
msgstr "B<S_ISVTX>  (01000)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "sticky bit (restricted deletion flag, as described in B<unlink>(2))"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IRUSR>  (00400)"
msgstr "B<S_IRUSR>  (00400)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read by owner"
msgstr "읽기"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IWUSR>  (00200)"
msgstr "B<S_IWUSR>  (00200)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write by owner"
msgstr "쓰기"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IXUSR>  (00100)"
msgstr "B<S_IXUSR>  (00100)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"execute/search by owner (\"search\" applies for directories, and means that "
"entries within the directory can be accessed)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IRGRP>  (00040)"
msgstr "B<S_IRGRP>  (00040)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "read by group"
msgstr "그룹에 의한 읽기"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IWGRP>  (00020)"
msgstr "B<S_IWGRP>  (00020)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write by group"
msgstr "그룹에 의한 쓰기"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IXGRP>  (00010)"
msgstr "B<S_IXGRP>  (00010)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "execute/search by group"
msgstr "그룹에 의한 실행/찾기"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IROTH>  (00004)"
msgstr "B<S_IROTH>  (00004)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "00004 read by others"
msgid "read by others"
msgstr "0004 다른사람이 읽기 00004 read by others"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IWOTH>  (00002)"
msgstr "B<S_IWOTH>  (00002)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "write by others"
msgstr "다른사람이 쓰기"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<S_IXOTH>  (00001)"
msgstr "B<S_IXOTH>  (00001)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "execute/search by others"
msgstr "다른사람이 실행/찾기"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The effective UID of the calling process must match the owner of the file, "
"or the process must be privileged (Linux: it must have the B<CAP_FOWNER> "
"capability)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the effective UID of the process is not zero and the group of the file "
#| "does not match the effective group ID of the process or one of its "
#| "supplementary group IDs, the S_ISGID bit will be turned off, but this "
#| "will not cause an error to be returned."
msgid ""
"If the calling process is not privileged (Linux: does not have the "
"B<CAP_FSETID> capability), and the group of the file does not match the "
"effective group ID of the process or one of its supplementary group IDs, the "
"B<S_ISGID> bit will be turned off, but this will not cause an error to be "
"returned."
msgstr ""
"프로세스의 유효UID가 0이 아니고 파일의 그룹이 프로세스의 그룹 유효ID나 추가 "
"그룹 ID중 하나가 다르다면 S_ISGID 비트가 꺼질것이다. 하지만 이것은 에러를 유"
"발하지 않는다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Depending on the file system, set user ID and set group ID execution bits "
#| "may be turned off if a file is written.  On some file systems, only the "
#| "super-user can set the sticky bit, which may have a special meaning (e."
#| "g., for directories, a file can only be deleted by the owner or the super-"
#| "user)."
msgid ""
"As a security measure, depending on the filesystem, the set-user-ID and set-"
"group-ID execution bits may be turned off if a file is written.  (On Linux, "
"this occurs if the writing process does not have the B<CAP_FSETID> "
"capability.)  On some filesystems, only the superuser can set the sticky "
"bit, which may have a special meaning.  For the sticky bit, and for set-user-"
"ID and set-group-ID bits on directories, see B<inode>(7)."
msgstr ""
"NFS파일 시스템에서 접근제어는 서버가 하기 때문에 권한 제한은 이미 열린 파일"
"에 영 향을 줄것이지만, 열린 파일은 클라이언트가 다루게 된다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On NFS filesystems, restricting the permissions will immediately influence "
"already open files, because the access control is done on the server, but "
"open files are maintained by the client.  Widening the permissions may be "
"delayed for other clients if attribute caching is enabled on them."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fchmodat()"
msgstr "fchmodat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<fchmodat>()  system call operates in exactly the same way as "
"B<chmod>(), except for the differences described here."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the pathname given in I<pathname> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<dirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<chmod>()  for a relative pathname)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<pathname> is relative and I<dirfd> is the special value B<AT_FDCWD>, "
"then I<pathname> is interpreted relative to the current working directory of "
"the calling process (like B<chmod>())."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "If I<pathname> is absolute, then I<dirfd> is ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<flags> can either be 0, or include the following flag:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_SYMLINK_NOFOLLOW>"
msgstr "B<AT_SYMLINK_NOFOLLOW>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<pathname> is a symbolic link, do not dereference it: instead operate on "
"the link itself.  This flag is not currently implemented."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<fchmodat>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr "성공시, 0을 반환한다. 실패시, -1을 반환하고 errno가 적절히 설정된다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Depending on the filesystem, errors other than those listed below can be "
"returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The more general errors for B<chmod>()  are listed below:"
msgstr "B<chmod>()의 일반적인 에러는 아래와 같다:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid "Search permission is denied on a component of the path prefix."
msgid ""
"Search permission is denied on a component of the path prefix.  (See also "
"B<path_resolution>(7).)"
msgstr "찾기 권한이 없다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<fchmod>())  The file descriptor I<fd> is not valid."
msgstr "(B<fchmod>())  파일 디스크립터 I<fd>가 적절하지 않다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<fchmodat>())  I<pathname> is relative but I<dirfd> is neither B<AT_FDCWD> "
"nor a valid file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> points outside your accessible address space."
msgstr "I<pathname> 가 접근할수 있는 주소 공간외를 가리키고 있다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "(B<fchmodat>())  Invalid flag specified in I<flags>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "I/O 에러가 발생했다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Too many symbolic links were encountered in resolving I<pathname>."
msgstr "I<pathname> 의 링크가 너무 많다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<pathname> is too long."
msgstr "I<경로명> 이 너무길다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file does not exist."
msgstr "파일이 존재하지 않는다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "이용할수 있는 커널 메모리가 충분하지 않다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "A component of the path prefix is not a directory."
msgstr "경로 접두사 요소가 디렉토리가 아니다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<fchmodat>())  I<pathname> is relative and I<dirfd> is a file descriptor "
"referring to a file other than a directory."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSUP>"
msgstr "B<ENOTSUP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"(B<fchmodat>())  I<flags> specified B<AT_SYMLINK_NOFOLLOW>, which is not "
"supported."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The effective UID does not match the owner of the file, and is not zero."
msgid ""
"The effective UID does not match the owner of the file, and the process is "
"not privileged (Linux: it does not have the B<CAP_FOWNER> capability)."
msgstr "유효 UID가 파일의 소유자와 같지 않고 0이 아니다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file is marked immutable or append-only.  (See B<ioctl_iflags>(2).)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The named file resides on a read-only filesystem."
msgstr "지정된 파일이 읽기-전용 파일 시스템에 있다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The GNU C library B<fchmodat>()  wrapper function implements the POSIX-"
"specified interface described in this page.  This interface differs from the "
"underlying Linux system call, which does I<not> have a I<flags> argument."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "glibc notes"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On older kernels where B<fchmodat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<chmod>().  When I<pathname> is a "
"relative pathname, glibc constructs a pathname based on the symbolic link in "
"I</proc/self/fd> that corresponds to the I<dirfd> argument."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<chmod>()"
msgstr "B<chmod>()"

#. type: TQ
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<fchmod>()"
msgstr "B<fchmod>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "4.4BSD, SVr4, POSIX.1-2001."
msgstr "4.4BSD, SVr4, POSIX.1-2001."

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<fchmodat>()"
msgstr "B<fchmodat>()"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008.  Linux 2.6.16, glibc 2.4."
msgstr "POSIX.1-2008.  Linux 2.6.16, glibc 2.4."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<chmod>(1), B<chown>(2), B<execve>(2), B<open>(2), B<stat>(2), B<inode>(7), "
"B<path_resolution>(7), B<symlink>(7)"
msgstr ""
"B<chmod>(1), B<chown>(2), B<execve>(2), B<open>(2), B<stat>(2), B<inode>(7), "
"B<path_resolution>(7), B<symlink>(7)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm
msgid ""
"B<fchmodat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<chmod>(), B<fchmod>(): 4.4BSD, SVr4, POSIX.1-2001i, POSIX.1-2008."
msgstr "B<chmod>(), B<fchmod>(): 4.4BSD, SVr4, POSIX.1-2001i, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm
msgid "B<fchmodat>(): POSIX.1-2008."
msgstr "B<fchmodat>(): POSIX.1-2008."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
