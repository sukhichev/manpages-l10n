# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 정강훈 <skyeyes@soback.kornet.net>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:56+0100\n"
"PO-Revision-Date: 2000-04-30 08:57+0900\n"
"Last-Translator: 정강훈 <skyeyes@soback.kornet.net>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "fsync"
msgstr "fsync"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "fsync - synchronize a file's complete in-core state with that on disk"
msgid ""
"fsync, fdatasync - synchronize a file's in-core state with storage device"
msgstr "fsync - 파일의 완전한 내부 상태와 디스크상의 상태을 동기화 시킨다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int fsync(int >I<fd>B<);>\n"
msgstr "B<int fsync(int >I<fd>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int fdatasync(int >I<fd>B<);>\n"
msgstr "B<int fdatasync(int >I<fd>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<fsync>():\n"
"    glibc 2.16 and later:\n"
"        No feature test macros need be defined\n"
"    glibc up to and including 2.15:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE\n"
"            || /* Since glibc 2.8: */ _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<fdatasync>():"
msgstr "B<fdatasync>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 199309L || _XOPEN_SOURCE E<gt>= 500\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 199309L || _XOPEN_SOURCE E<gt>= 500\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fsync>()  transfers (\"flushes\") all modified in-core data of (i.e., "
"modified buffer cache pages for) the file referred to by the file descriptor "
"I<fd> to the disk device (or other permanent storage device) so that all "
"changed information can be retrieved even if the system crashes or is "
"rebooted.  This includes writing through or flushing a disk cache if "
"present.  The call blocks until the device reports that the transfer has "
"completed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"As well as flushing the file data, B<fsync>()  also flushes the metadata "
"information associated with the file (see B<inode>(7))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Calling B<fsync>()  does not necessarily ensure that the entry in the "
"directory containing the file has also reached disk.  For that an explicit "
"B<fsync>()  on a file descriptor for the directory is also needed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<fdatasync>()  is similar to B<fsync>(), but does not flush modified "
"metadata unless that metadata is needed in order to allow a subsequent data "
"retrieval to be correctly handled.  For example, changes to I<st_atime> or "
"I<st_mtime> (respectively, time of last access and time of last "
"modification; see B<inode>(7))  do not require flushing because they are not "
"necessary for a subsequent data read to be handled correctly.  On the other "
"hand, a change to the file size (I<st_size>, as made by say "
"B<ftruncate>(2)), would require a metadata flush."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The aim of B<fdatasync>()  is to reduce disk activity for applications that "
"do not require all metadata to be synchronized with the disk."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, these system calls return zero.  On error, -1 is returned, and "
"I<errno> is set to indicate the error."
msgstr ""
"성공시, 0이 리턴된다. 에러시, -1이 리턴되며, I<errno>는 적당한 값으로 설정된"
"다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not a valid open file descriptor."
msgstr "I<fd> 는 유효한 열려진 파일 기술자가 아니다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The function was interrupted by a signal; see B<signal>(7)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#.  commit 088737f44bbf6378745f5b57b035e57ee3dc4750
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"An error occurred during synchronization.  This error may relate to data "
"written to some other file descriptor on the same file.  Since Linux 4.13, "
"errors from write-back will be reported to all file descriptors that might "
"have written the data which triggered the error.  Some filesystems (e.g., "
"NFS) keep close track of which data came through which file descriptor, and "
"give more precise reporting.  Other filesystems (e.g., most local "
"filesystems) will report errors to all file descriptors that were open on "
"the file when the error was recorded."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Disk space was exhausted while synchronizing."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: TQ
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<fd> is bound to a special file which does not support synchronization."
msgid ""
"I<fd> is bound to a special file (e.g., a pipe, FIFO, or socket)  which does "
"not support synchronization."
msgstr "I<fd 는> 동기화를 지원하지 않는 특별한 파일이다."

#. type: TQ
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<fd> is bound to a file on NFS or another filesystem which does not "
"allocate space at the time of a B<write>(2)  system call, and some previous "
"write failed due to insufficient storage space."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#.  POSIX.1-2001: It shall be defined to -1 or 0 or 200112L.
#.  -1: unavailable, 0: ask using sysconf().
#.  glibc defines them to 1.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On POSIX systems on which B<fdatasync>()  is available, "
"B<_POSIX_SYNCHRONIZED_IO> is defined in I<E<lt>unistd.hE<gt>> to a value "
"greater than 0.  (See also B<sysconf>(3).)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "POSIX.1-2001, 4.3BSD."
msgid "POSIX.1-2001, 4.2BSD."
msgstr "POSIX.1-2001, 4.3BSD."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In Linux 2.2 and earlier, B<fdatasync>()  is equivalent to B<fsync>(), and "
"so has no performance advantage."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<fsync>()  implementations in older kernels and lesser used filesystems "
"do not know how to flush disk caches.  In these cases disk caches need to be "
"disabled using B<hdparm>(8)  or B<sdparm>(8)  to guarantee safe operation."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Under AT&T UNIX System V Release 4 I<fd> needs to be opened for writing.  "
"This is by itself incompatible with the original BSD interface and forbidden "
"by POSIX, but nevertheless survives in HP-UX and AIX."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sync>(1), B<bdflush>(2), B<open>(2), B<posix_fadvise>(2), B<pwritev>(2), "
"B<sync>(2), B<sync_file_range>(2), B<fflush>(3), B<fileno>(3), B<hdparm>(8), "
"B<mount>(8)"
msgstr ""
"B<sync>(1), B<bdflush>(2), B<open>(2), B<posix_fadvise>(2), B<pwritev>(2), "
"B<sync>(2), B<sync_file_range>(2), B<fflush>(3), B<fileno>(3), B<hdparm>(8), "
"B<mount>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>, B<EINVAL>"
msgstr "B<EROFS>, B<EINVAL>"

#. type: TP
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>, B<EDQUOT>"
msgstr "B<ENOSPC>, B<EDQUOT>"

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, 4.3BSD."

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On some UNIX systems (but not Linux), I<fd> must be a I<writable> file "
"descriptor."
msgstr ""

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: Plain text
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2001, 4.3BSD."
msgstr "POSIX.1-2001, 4.3BSD."

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
