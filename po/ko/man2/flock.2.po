# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 정강훈 <skyeyes@soback.kornet.net>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-03-01 16:56+0100\n"
"PO-Revision-Date: 2000-05-01 08:57+0900\n"
"Last-Translator: 정강훈 <skyeyes@soback.kornet.net>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "flock"
msgstr "flock"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr "2023년 10월 31일"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Linux man-pages 6.03"
msgid "Linux man-pages 6.06"
msgstr "Linux man-pages 6.03"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "flock - apply or remove an advisory lock on an open file"
msgstr "flock - 열려진 파일에 권고(advisory) 잠금을 적용하거나 제거한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/file.hE<gt>>\n"
msgstr "B<#include E<lt>sys/file.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int flock(int >I<fd>B<, int >I<operation>B<);>\n"
msgstr "B<int flock(int >I<fd>B<, int >I<operation>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Apply or remove an advisory lock on an open file.  The file is specified "
#| "by I<fd>.  Valid operations are given below:"
msgid ""
"Apply or remove an advisory lock on the open file specified by I<fd>.  The "
"argument I<operation> is one of the following:"
msgstr ""
"열려진 파일에 권고 잠금을 적용하거나 제거한다.  파일은 I<fd> 로 지정된다.  유"
"효한 연산은 다음과 같다:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LOCK_SH>"
msgstr "B<LOCK_SH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Shared lock.  More than one process may hold a shared lock for a given "
#| "file at a given time."
msgid ""
"Place a shared lock.  More than one process may hold a shared lock for a "
"given file at a given time."
msgstr ""
"공유(shared) 잠금. 한개 이상의 프로세스들은 주어진 시간에 주어진 파일에 대한 "
"공유 잠금을 할수 있다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LOCK_EX>"
msgstr "B<LOCK_EX>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Exclusive lock.  Only one process may hold an exclusive lock for a given "
#| "file at a given time."
msgid ""
"Place an exclusive lock.  Only one process may hold an exclusive lock for a "
"given file at a given time."
msgstr ""
"배타(exclusive) 잠금. 단지 한개의 프로세스만이 주어진 시간에 주어진 파일에 대"
"해 배타 잠금을 할수 있다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<LOCK_UN>"
msgstr "B<LOCK_UN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Remove an existing lock held by this process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A call to B<flock>()  may block if an incompatible lock is held by another "
"process.  To make a nonblocking request, include B<LOCK_NB> (by ORing)  with "
"any of the above operations."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A single file may not simultaneously have both shared and exclusive locks."
msgstr "한 파일에 동시에 공유 잠금과 배타 잠금을 할 수 없다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Locks created by B<flock>()  are associated with an open file description "
"(see B<open>(2)).  This means that duplicate file descriptors (created by, "
"for example, B<fork>(2)  or B<dup>(2))  refer to the same lock, and this "
"lock may be modified or released using any of these file descriptors.  "
"Furthermore, the lock is released either by an explicit B<LOCK_UN> operation "
"on any of these duplicate file descriptors, or when all such file "
"descriptors have been closed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If a process uses B<open>(2)  (or similar) to obtain more than one file "
"descriptor for the same file, these file descriptors are treated "
"independently by B<flock>().  An attempt to lock the file using one of these "
"file descriptors may be denied by a lock that the calling process has "
"already placed via another file descriptor."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A process may hold only one type of lock (shared or exclusive)  on a file.  "
"Subsequent B<flock>()  calls on an already locked file will convert an "
"existing lock to the new lock mode."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Locks created by B<flock>()  are preserved across an B<execve>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A shared or exclusive lock can be placed on a file regardless of the mode in "
"which the file was opened."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"성공시, 0이 리턴된다. 에러시, -1이 리턴되며, I<errno>는 적당한 값으로 설정된"
"다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<fd> is not an open file descriptor."
msgstr "I<fd> 는 파일열기 디스크립터가 아니다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"While waiting to acquire a lock, the call was interrupted by delivery of a "
"signal caught by a handler; see B<signal>(7)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<operation> is invalid."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOLCK>"
msgstr "B<ENOLCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The kernel ran out of memory for allocating lock records."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EWOULDBLOCK>"
msgstr "B<EWOULDBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The file is locked and the B<LOCK_NB> flag was selected."
msgstr "파일은 잠겨 있고 B<LOCK_NB> 플래그가 선택되었다."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#.  E.g., according to the flock(2) man page, FreeBSD since at least 5.3
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 2.0, B<flock>()  is implemented as a system call in its own "
"right rather than being emulated in the GNU C library as a call to "
"B<fcntl>(2).  With this implementation, there is no interaction between the "
"types of lock placed by B<flock>()  and B<fcntl>(2), and B<flock>()  does "
"not detect deadlock.  (Note, however, that on some systems, such as the "
"modern BSDs, B<flock>()  and B<fcntl>(2)  locks I<do> interact with one "
"another.)"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "CIFS details"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Up to Linux 5.4, B<flock>()  is not propagated over SMB.  A file with such "
"locks will not appear locked for remote clients."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 5.5, B<flock>()  locks are emulated with SMB byte-range locks on "
"the entire file.  Similarly to NFS, this means that B<fcntl>(2)  and "
"B<flock>()  locks interact with one another.  Another important side-effect "
"is that the locks are not advisory anymore: any IO on a locked file will "
"always fail with B<EACCES> when done from a separate file descriptor.  This "
"difference originates from the design of locks in the SMB protocol, which "
"provides mandatory locking semantics."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Remote and mandatory locking semantics may vary with SMB protocol, mount "
"options and server type.  See B<mount.cifs>(8)  for additional information."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"4.4BSD (the B<flock>()  call first appeared in 4.2BSD).  A version of "
"B<flock>(), possibly implemented in terms of B<fcntl>(2), appears on most "
"UNIX systems."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NFS details"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<flock>(2)  does not lock files over NFS.  Use B<fcntl>(2)  instead: "
#| "that does work over NFS, given a sufficiently recent version of Linux and "
#| "a server which supports locking."
msgid ""
"Up to Linux 2.6.11, B<flock>()  does not lock files over NFS (i.e., the "
"scope of locks was limited to the local system).  Instead, one could use "
"B<fcntl>(2)  byte-range locking, which does work over NFS, given a "
"sufficiently recent version of Linux and a server which supports locking."
msgstr ""
"B<flock>(2)은 NFS에 있는 파일을 잠금을 하지 않는다.  대신에 B<fcntl>(2)B<를> "
"사용해라: 이 시스템 콜은 최신 버전이고 서버가 잠금을 지원한다면 NFS에서도 작"
"동한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.12, NFS clients support B<flock>()  locks by emulating them "
"as B<fcntl>(2)  byte-range locks on the entire file.  This means that "
"B<fcntl>(2)  and B<flock>()  locks I<do> interact with one another over "
"NFS.  It also means that in order to place an exclusive lock, the file must "
"be opened for writing."
msgstr ""

#.  commit 5eebde23223aeb0ad2d9e3be6590ff8bbfab0fc2
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Since Linux 2.6.37, the kernel supports a compatibility mode that allows "
"B<flock>()  locks (and also B<fcntl>(2)  byte region locks) to be treated as "
"local; see the discussion of the I<local_lock> option in B<nfs>(5)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<flock>()  places advisory locks only; given suitable permissions on a "
"file, a process is free to ignore the use of B<flock>()  and perform I/O on "
"the file."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<flock>()  and B<fcntl>(2)  locks have different semantics with respect to "
"forked processes and B<dup>(2).  On systems that implement B<flock>()  using "
"B<fcntl>(2), the semantics of B<flock>()  will be different from those "
"described in this manual page."
msgstr ""

#.  Kernel 2.5.21 changed things a little: during lock conversion
#.  it is now the highest priority process that will get the lock -- mtk
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Converting a lock (shared to exclusive, or vice versa) is not guaranteed to "
"be atomic: the existing lock is first removed, and then a new lock is "
"established.  Between these two steps, a pending lock request by another "
"process may be granted, with the result that the conversion either blocks, "
"or fails if B<LOCK_NB> was specified.  (This is the original BSD behavior, "
"and occurs on many other implementations.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<flock>(1), B<close>(2), B<dup>(2), B<execve>(2), B<fcntl>(2), B<fork>(2), "
"B<open>(2), B<lockf>(3), B<lslocks>(8)"
msgstr ""
"B<flock>(1), B<close>(2), B<dup>(2), B<execve>(2), B<fcntl>(2), B<fork>(2), "
"B<open>(2), B<lockf>(3), B<lslocks>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Documentation/filesystems/locks.txt> in the Linux kernel source tree "
"(I<Documentation/locks.txt> in older kernels)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-04"
msgstr "2022년 12월 4일"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr "Linux man-pages 6.05.01"

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"
