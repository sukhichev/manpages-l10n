# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-02-09 16:59+0100\n"
"PO-Revision-Date: 2022-06-18 15:08+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-EDITENV"
msgstr "GRUB-EDITENV"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "december 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-1"
msgstr "GRUB 2:2.12-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-editenv - edit GRUB environment block"
msgstr "grub-editenv - rediger GRUB-miljøblokke"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-editenv> [I<\\,OPTION\\/>...] I<\\,FILENAME COMMAND\\/>"
msgstr "B<grub-editenv> [I<\\,TILVALG\\/>...] I<\\,FILNAVN KOMMANDO\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Tool to edit environment block."
msgstr "Værktøj til at redigere miljøblokken."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Commands:"
msgstr "Kommandoer:"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "create"
msgstr "create"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Create a blank environment block file."
msgstr "Opret en tom miljøblokfil."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "list"
msgstr "list"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "List the current variables."
msgstr "Vis de nuværende variable."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "set [NAME=VALUE ...]"
msgstr "set [NAVN=VÆRDI ...]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set variables."
msgstr "Angiv variable."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "unset [NAME ...]"
msgstr "unset [NAVN ...]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Delete variables."
msgstr "Slet variable."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Options:"
msgstr "Tilvalg:"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "vis denne hjælpeliste"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "vis en kort besked om brug af programmet"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "udskriv uddybende meddelelser."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "udskriv programversion"

#. type: Plain text
#: archlinux
msgid "If FILENAME is `-', the default value //boot/grub/grubenv is used."
msgstr ""
"Hvis FILNAVN er \"-\", vil standardværdien //boot/grub/grubenv blive brugt."

#. type: Plain text
#: archlinux
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm //boot/grub/grubenv'."
msgstr ""
"Der er ingen \"slet\"-kommando; hvis du vil slette hele miljøet, så brug "
"\"rm //boot/grub/grubenv\"."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapporter programfejl på engelsk til E<lt>bug-grub@gnu.orgE<gt>.\n"
"Oversættelsesfejl rapporteres til E<lt>dansk@dansk-gruppen.dkE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-reboot>(8), B<grub-set-default>(8)"
msgstr "B<grub-reboot>(8), B<grub-set-default>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-editenv> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-editenv> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fulde dokumentation for B<grub-editenv> vedligeholdes som Texinfo-"
"manual. Hvis B<info> og B<grub-editenv> programmerne er korrekt installeret "
"på din side, bør kommandoen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-editenv>"
msgstr "B<info grub-editenv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "October 2023"
msgstr "oktober 2023"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "GRUB 2.06-13+deb12u1"
msgstr "GRUB 2.06-13+deb12u1"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If FILENAME is `-', the default value I<\\,/boot/grub/grubenv\\/> is used."
msgstr ""
"Hvis FILNAVN er \"-\", vil standardværdien I<\\,/boot/grub/grubenv\\/> blive "
"brugt."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm /boot/grub/grubenv'."
msgstr ""
"Der er ingen \"slet\"-kommando; hvis du vil slette hele miljøet, så brug "
"\"rm /boot/grub/grubenv\"."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "January 2024"
msgstr "januar 2024"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.12-1"
msgstr "GRUB 2.12-1"
