# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2024-01-07 12:00+0100\n"
"PO-Revision-Date: 2022-06-18 15:13+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB-SPARC64-SETUP"
msgstr "GRUB-SPARC64-SETUP"

#. type: TH
#: archlinux
#, no-wrap
msgid "December 2023"
msgstr "december 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12-1"
msgstr "GRUB 2:2.12-1"

#. type: TH
#: archlinux
#, no-wrap
msgid "System Administration Utilities"
msgstr "Redskaber for systemadministration"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux
msgid "grub-sparc64-setup - set up a device to boot using GRUB"
msgstr "grub-sparc64-setup - opsæt en enhed til at starte op via GRUB"

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux
msgid "B<grub-sparc64-setup> [I<\\,OPTION\\/>...] I<\\,DEVICE\\/>"
msgstr "B<grub-sparc64-setup> [I<\\,TILVALG\\/>...] I<\\,ENHED\\/>"

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux
msgid "Set up images to boot from DEVICE."
msgstr "Opsæt aftryk til at starte fra ENHED."

#. type: Plain text
#: archlinux
msgid ""
"You should not normally run this program directly.  Use grub-install instead."
msgstr ""
"Du bør normalt ikke køre dette program direkte. Brug i stedet grub-install."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-a>, B<--allow-floppy>"
msgstr "B<-a>, B<--allow-floppy>"

#. type: Plain text
#: archlinux
msgid ""
"make the drive also bootable as floppy (default for fdX devices). May break "
"on some BIOSes."
msgstr ""
"gør at drevet kan bootes som diskettedrev (standard for fdX-enheder). Kan "
"ødelægge opstarten med visse BIOS'er."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-b>, B<--boot-image>=I<\\,FILE\\/>"
msgstr "B<-b>, B<--boot-image>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the boot image [default=boot.img]"
msgstr "brug FIL som opstartsaftryk [standard=boot.img]"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-c>, B<--core-image>=I<\\,FILE\\/>"
msgstr "B<-c>, B<--core-image>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the core image [default=core.img]"
msgstr "brug FIL som kerneaftryk [standard=core.img]"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-d>, B<--directory>=I<\\,DIR\\/>"
msgstr "B<-d>, B<--directory>=I<\\,MAPPE\\/>"

#. type: Plain text
#: archlinux
msgid "use GRUB files in the directory DIR [default=//boot/grub]"
msgstr "brug GRUB-filer i mappen MAPPE [standard=//boot/grub]"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: archlinux
msgid "install even if problems are detected"
msgstr "installer selvom der opdages problemer"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-m>, B<--device-map>=I<\\,FILE\\/>"
msgstr "B<-m>, B<--device-map>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux
msgid "use FILE as the device map [default=//boot/grub/device.map]"
msgstr "brug FIL som enhedskort [standard=//boot/grub/device.map]"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--no-rs-codes>"
msgstr "B<--no-rs-codes>"

#. type: Plain text
#: archlinux
msgid ""
"Do not apply any reed-solomon codes when embedding core.img. This option is "
"only available on x86 BIOS targets."
msgstr ""
"Anvend ikke nogen reed-solomon-koder ved indlejring af core.img. Dette "
"tilvalg er kun tilgængeligt på x86-BIOS-mål."

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-s>, B<--skip-fs-probe>"
msgstr "B<-s>, B<--skip-fs-probe>"

#. type: Plain text
#: archlinux
msgid "do not probe for filesystems in DEVICE"
msgstr "søg ikke efter filsystemer i ENHED"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux
msgid "print verbose messages."
msgstr "udskriv uddybende meddelelser."

#. type: TP
#: archlinux
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux
msgid "give this help list"
msgstr "vis denne hjælpeliste"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux
msgid "give a short usage message"
msgstr "vis en kort besked om brug af programmet"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux
msgid "print program version"
msgstr "udskriv programversion"

#. type: Plain text
#: archlinux
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriske eller valgfri argumenter til lange tilvalg er også "
"obligatoriske henholdsvis valgfri til de tilsvarende korte."

#. type: Plain text
#: archlinux
msgid "DEVICE must be an OS device (e.g. I<\\,/dev/sda\\/>)."
msgstr "ENHED skal være en OS-enhed (f.eks. I<\\,/dev/sda\\/>)."

#. type: SH
#: archlinux
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapporter programfejl på engelsk til E<lt>bug-grub@gnu.orgE<gt>.\n"
"Oversættelsesfejl rapporteres til E<lt>dansk@dansk-gruppen.dkE<gt>."

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux
msgid "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"
msgstr "B<grub-install>(8), B<grub-mkimage>(1), B<grub-mkrescue>(1)"

#. type: Plain text
#: archlinux
msgid ""
"The full documentation for B<grub-sparc64-setup> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-sparc64-setup> programs are properly "
"installed at your site, the command"
msgstr ""
"Den fulde dokumentation for B<grub-sparc64-setup> vedligeholdes som Texinfo-"
"manual. Hvis B<info> og B<grub-sparc64-setup> programmerne er korrekt "
"installeret på din side, bør kommandoen"

#. type: Plain text
#: archlinux
msgid "B<info grub-sparc64-setup>"
msgstr "B<info grub-sparc64-setup>"

#. type: Plain text
#: archlinux
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."
