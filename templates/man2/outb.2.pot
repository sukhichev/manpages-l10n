# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:03+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "outb"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"outb, outw, outl, outsb, outsw, outsl, inb, inw, inl, insb, insw, insl, "
"outb_p, outw_p, outl_p, inb_p, inw_p, inl_p - port I/O"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/io.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<unsigned char inb(unsigned short >I<port>B<);>\n"
"B<unsigned char inb_p(unsigned short >I<port>B<);>\n"
"B<unsigned short inw(unsigned short >I<port>B<);>\n"
"B<unsigned short inw_p(unsigned short >I<port>B<);>\n"
"B<unsigned int inl(unsigned short >I<port>B<);>\n"
"B<unsigned int inl_p(unsigned short >I<port>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void outb(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outb_p(unsigned char >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outw_p(unsigned short >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
"B<void outl_p(unsigned int >I<value>B<, unsigned short >I<port>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void insb(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insw(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void insl(unsigned short >I<port>B<, void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsb(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsw(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
"B<void outsl(unsigned short >I<port>B<, const void >I<addr>B<[.>I<count>B<],>\n"
"B<           unsigned long >I<count>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This family of functions is used to do low-level port input and output.  The "
"out* functions do port output, the in* functions do port input; the b-suffix "
"functions are byte-width and the w-suffix functions word-width; the _p-"
"suffix functions pause until the I/O completes."
msgstr ""

#.  , given the following information
#.  in addition to that given in
#.  .BR outb (9).
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"They are primarily designed for internal kernel use, but can be used from "
"user space."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"You must compile with B<-O> or B<-O2> or similar.  The functions are defined "
"as inline macros, and will not be substituted in without optimization "
"enabled, causing unresolved references at link time."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"You use B<ioperm>(2)  or alternatively B<iopl>(2)  to tell the kernel to "
"allow the user space application to access the I/O ports in question.  "
"Failure to do this will cause the application to receive a segmentation "
"fault."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<outb>()  and friends are hardware-specific.  The I<value> argument is "
"passed first and the I<port> argument is passed second, which is the "
"opposite order from most DOS implementations."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<ioperm>(2), B<iopl>(2)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-11-10"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: TH
#: debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
