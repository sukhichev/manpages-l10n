# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 16:59+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "kcmp"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"kcmp - compare two processes to determine if they share a kernel resource"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/kcmp.hE<gt>>       /* Definition of B<KCMP_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>>      /* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int syscall(SYS_kcmp, pid_t >I<pid1>B<, pid_t >I<pid2>B<, int >I<type>B<,>\n"
"B<            unsigned long >I<idx1>B<, unsigned long >I<idx2>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<Note>: glibc provides no wrapper for B<kcmp>(), necessitating the use of "
"B<syscall>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<kcmp>()  system call can be used to check whether the two processes "
"identified by I<pid1> and I<pid2> share a kernel resource such as virtual "
"memory, file descriptors, and so on."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Permission to employ B<kcmp>()  is governed by ptrace access mode "
"B<PTRACE_MODE_READ_REALCREDS> checks against both I<pid1> and I<pid2>; see "
"B<ptrace>(2)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<type> argument specifies which resource is to be compared in the two "
"processes.  It has one of the following values:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_FILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether a file descriptor I<idx1> in the process I<pid1> refers to the "
"same open file description (see B<open>(2))  as file descriptor I<idx2> in "
"the process I<pid2>.  The existence of two file descriptors that refer to "
"the same open file description can occur as a result of B<dup>(2)  (and "
"similar)  B<fork>(2), or passing file descriptors via a domain socket (see "
"B<unix>(7))."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_FILES>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether the processes share the same set of open file descriptors.  "
"The arguments I<idx1> and I<idx2> are ignored.  See the discussion of the "
"B<CLONE_FILES> flag in B<clone>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_FS>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether the processes share the same filesystem information (i.e., "
"file mode creation mask, working directory, and filesystem root).  The "
"arguments I<idx1> and I<idx2> are ignored.  See the discussion of the "
"B<CLONE_FS> flag in B<clone>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_IO>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether the processes share I/O context.  The arguments I<idx1> and "
"I<idx2> are ignored.  See the discussion of the B<CLONE_IO> flag in "
"B<clone>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_SIGHAND>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether the processes share the same table of signal dispositions.  "
"The arguments I<idx1> and I<idx2> are ignored.  See the discussion of the "
"B<CLONE_SIGHAND> flag in B<clone>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_SYSVSEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether the processes share the same list of System\\ V semaphore undo "
"operations.  The arguments I<idx1> and I<idx2> are ignored.  See the "
"discussion of the B<CLONE_SYSVSEM> flag in B<clone>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_VM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether the processes share the same address space.  The arguments "
"I<idx1> and I<idx2> are ignored.  See the discussion of the B<CLONE_VM> flag "
"in B<clone>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<KCMP_EPOLL_TFD> (since Linux 4.13)"
msgstr ""

#.  commit 0791e3644e5ef21646fe565b9061788d05ec71d4
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check whether the file descriptor I<idx1> of the process I<pid1> is present "
"in the B<epoll>(7)  instance described by I<idx2> of the process I<pid2>.  "
"The argument I<idx2> is a pointer to a structure where the target file is "
"described.  This structure has the form:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct kcmp_epoll_slot {\n"
"    __u32 efd;\n"
"    __u32 tfd;\n"
"    __u64 toff;\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Within this structure, I<efd> is an epoll file descriptor returned from "
"B<epoll_create>(2), I<tfd> is a target file descriptor number, and I<toff> "
"is a target file offset counted from zero.  Several different targets may be "
"registered with the same file descriptor number and setting a specific "
"offset helps to investigate each of them."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note the B<kcmp>()  is not protected against false positives which may occur "
"if the processes are currently running.  One should stop the processes by "
"sending B<SIGSTOP> (see B<signal>(7))  prior to inspection with this system "
"call to obtain meaningful results."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The return value of a successful call to B<kcmp>()  is simply the result of "
"arithmetic comparison of kernel pointers (when the kernel compares "
"resources, it uses their memory addresses)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The easiest way to explain is to consider an example.  Suppose that I<v1> "
"and I<v2> are the addresses of appropriate resources, then the return value "
"is one of the following:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<0>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<v1> is equal to I<v2>; in other words, the two processes share the "
"resource."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<1>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<v1> is less than I<v2>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<2>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<v1> is greater than I<v2>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<3>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<v1> is not equal to I<v2>, but ordering information is unavailable."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<kcmp>()  was designed to return values suitable for sorting.  This is "
"particularly handy if one needs to compare a large number of file "
"descriptors."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"I<type> is B<KCMP_FILE> and I<fd1> or I<fd2> is not an open file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The epoll slot addressed by I<idx2> is outside of the user's address space."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<type> is invalid."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The target file is not present in B<epoll>(7)  instance."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Insufficient permission to inspect process resources.  The B<CAP_SYS_PTRACE> "
"capability is required to inspect processes that you do not own.  Other "
"ptrace limitations may also apply, such as B<CONFIG_SECURITY_YAMA>, which, "
"when I</proc/sys/kernel/yama/ptrace_scope> is 2, limits B<kcmp>()  to child "
"processes; see B<ptrace>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ESRCH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Process I<pid1> or I<pid2> does not exist."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "Linux 3.5."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Before Linux 5.12, this system call is available only if the kernel is "
"configured with B<CONFIG_CHECKPOINT_RESTORE>, since the original purpose of "
"the system call was for the checkpoint/restore in user space (CRIU) "
"feature.  (The alternative to this system call would have been to expose "
"suitable process information via the B<proc>(5)  filesystem; this was deemed "
"to be unsuitable for security reasons.)  Since Linux 5.12, this system call "
"is also available if the kernel is configured with B<CONFIG_KCMP>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See B<clone>(2)  for some background information on the shared resources "
"referred to on this page."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below uses B<kcmp>()  to test whether pairs of file descriptors "
"refer to the same open file description.  The program tests different cases "
"for the file descriptor pairs, as described in the program output.  An "
"example run of the program is as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out>\n"
"Parent PID is 1144\n"
"Parent opened file on FD 3\n"
"\\&\n"
"PID of child of fork() is 1145\n"
"\tCompare duplicate FDs from different processes:\n"
"\t\tkcmp(1145, 1144, KCMP_FILE, 3, 3) ==E<gt> same\n"
"Child opened file on FD 4\n"
"\tCompare FDs from distinct open()s in same process:\n"
"\t\tkcmp(1145, 1145, KCMP_FILE, 3, 4) ==E<gt> different\n"
"Child duplicated FD 3 to create FD 5\n"
"\tCompare duplicated FDs in same process:\n"
"\t\tkcmp(1145, 1145, KCMP_FILE, 3, 5) ==E<gt> same\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>err.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/kcmp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"static int\n"
"kcmp(pid_t pid1, pid_t pid2, int type,\n"
"     unsigned long idx1, unsigned long idx2)\n"
"{\n"
"    return syscall(SYS_kcmp, pid1, pid2, type, idx1, idx2);\n"
"}\n"
"\\&\n"
"static void\n"
"test_kcmp(char *msg, pid_t pid1, pid_t pid2, int fd_a, int fd_b)\n"
"{\n"
"    printf(\"\\et%s\\en\", msg);\n"
"    printf(\"\\et\\etkcmp(%jd, %jd, KCMP_FILE, %d, %d) ==E<gt> %s\\en\",\n"
"           (intmax_t) pid1, (intmax_t) pid2, fd_a, fd_b,\n"
"           (kcmp(pid1, pid2, KCMP_FILE, fd_a, fd_b) == 0) ?\n"
"                        \"same\" : \"different\");\n"
"}\n"
"\\&\n"
"int\n"
"main(void)\n"
"{\n"
"    int                fd1, fd2, fd3;\n"
"    static const char  pathname[] = \"/tmp/kcmp.test\";\n"
"\\&\n"
"    fd1 = open(pathname, O_CREAT | O_RDWR, 0600);\n"
"    if (fd1 == -1)\n"
"        err(EXIT_FAILURE, \"open\");\n"
"\\&\n"
"    printf(\"Parent PID is %jd\\en\", (intmax_t) getpid());\n"
"    printf(\"Parent opened file on FD %d\\en\\en\", fd1);\n"
"\\&\n"
"    switch (fork()) {\n"
"    case -1:\n"
"        err(EXIT_FAILURE, \"fork\");\n"
"\\&\n"
"    case 0:\n"
"        printf(\"PID of child of fork() is %jd\\en\", (intmax_t) getpid());\n"
"\\&\n"
"        test_kcmp(\"Compare duplicate FDs from different processes:\",\n"
"                  getpid(), getppid(), fd1, fd1);\n"
"\\&\n"
"        fd2 = open(pathname, O_CREAT | O_RDWR, 0600);\n"
"        if (fd2 == -1)\n"
"            err(EXIT_FAILURE, \"open\");\n"
"        printf(\"Child opened file on FD %d\\en\", fd2);\n"
"\\&\n"
"        test_kcmp(\"Compare FDs from distinct open()s in same process:\",\n"
"                  getpid(), getpid(), fd1, fd2);\n"
"\\&\n"
"        fd3 = dup(fd1);\n"
"        if (fd3 == -1)\n"
"            err(EXIT_FAILURE, \"dup\");\n"
"        printf(\"Child duplicated FD %d to create FD %d\\en\", fd1, fd3);\n"
"\\&\n"
"        test_kcmp(\"Compare duplicated FDs in same process:\",\n"
"                  getpid(), getpid(), fd1, fd3);\n"
"        break;\n"
"\\&\n"
"    default:\n"
"        wait(NULL);\n"
"    }\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<clone>(2), B<unshare>(2)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "The B<kcmp>()  system call first appeared in Linux 3.5."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<kcmp>()  is Linux-specific and should not be used in programs intended to "
"be portable."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"$ B<./a.out>\n"
"Parent PID is 1144\n"
"Parent opened file on FD 3\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"PID of child of fork() is 1145\n"
"\tCompare duplicate FDs from different processes:\n"
"\t\tkcmp(1145, 1144, KCMP_FILE, 3, 3) ==E<gt> same\n"
"Child opened file on FD 4\n"
"\tCompare FDs from distinct open()s in same process:\n"
"\t\tkcmp(1145, 1145, KCMP_FILE, 3, 4) ==E<gt> different\n"
"Child duplicated FD 3 to create FD 5\n"
"\tCompare duplicated FDs in same process:\n"
"\t\tkcmp(1145, 1145, KCMP_FILE, 3, 5) ==E<gt> same\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#define _GNU_SOURCE\n"
"#include E<lt>err.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/kcmp.hE<gt>\n"
"#include E<lt>stdint.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/syscall.hE<gt>\n"
"#include E<lt>sys/wait.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static int\n"
"kcmp(pid_t pid1, pid_t pid2, int type,\n"
"     unsigned long idx1, unsigned long idx2)\n"
"{\n"
"    return syscall(SYS_kcmp, pid1, pid2, type, idx1, idx2);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void\n"
"test_kcmp(char *msg, pid_t pid1, pid_t pid2, int fd_a, int fd_b)\n"
"{\n"
"    printf(\"\\et%s\\en\", msg);\n"
"    printf(\"\\et\\etkcmp(%jd, %jd, KCMP_FILE, %d, %d) ==E<gt> %s\\en\",\n"
"           (intmax_t) pid1, (intmax_t) pid2, fd_a, fd_b,\n"
"           (kcmp(pid1, pid2, KCMP_FILE, fd_a, fd_b) == 0) ?\n"
"                        \"same\" : \"different\");\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    int                fd1, fd2, fd3;\n"
"    static const char  pathname[] = \"/tmp/kcmp.test\";\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    fd1 = open(pathname, O_CREAT | O_RDWR, 0600);\n"
"    if (fd1 == -1)\n"
"        err(EXIT_FAILURE, \"open\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"Parent PID is %jd\\en\", (intmax_t) getpid());\n"
"    printf(\"Parent opened file on FD %d\\en\\en\", fd1);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    switch (fork()) {\n"
"    case -1:\n"
"        err(EXIT_FAILURE, \"fork\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    case 0:\n"
"        printf(\"PID of child of fork() is %jd\\en\", (intmax_t) getpid());\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        test_kcmp(\"Compare duplicate FDs from different processes:\",\n"
"                  getpid(), getppid(), fd1, fd1);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        fd2 = open(pathname, O_CREAT | O_RDWR, 0600);\n"
"        if (fd2 == -1)\n"
"            err(EXIT_FAILURE, \"open\");\n"
"        printf(\"Child opened file on FD %d\\en\", fd2);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        test_kcmp(\"Compare FDs from distinct open()s in same process:\",\n"
"                  getpid(), getpid(), fd1, fd2);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        fd3 = dup(fd1);\n"
"        if (fd3 == -1)\n"
"            err(EXIT_FAILURE, \"dup\");\n"
"        printf(\"Child duplicated FD %d to create FD %d\\en\", fd1, fd3);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        test_kcmp(\"Compare duplicated FDs in same process:\",\n"
"                  getpid(), getpid(), fd1, fd3);\n"
"        break;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    default:\n"
"        wait(NULL);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-05-03"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
