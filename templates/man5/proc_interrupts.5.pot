# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-23 08:04+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "proc_interrupts"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "/proc/interrupts - number of interrupts"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/interrupts>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This is used to record the number of interrupts per CPU per IO device.  "
"Since Linux 2.6.24, for the i386 and x86-64 architectures, at least, this "
"also includes interrupts internal to the system (that is, not associated "
"with a device as such), such as NMI (nonmaskable interrupt), LOC (local "
"timer interrupt), and for SMP systems, TLB (TLB flush interrupt), RES "
"(rescheduling interrupt), CAL (remote function call interrupt), and possibly "
"others.  Very easy to read formatting, done in ASCII."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "B<proc>(5)"
msgstr ""
