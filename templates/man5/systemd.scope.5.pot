# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:11+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD\\&.SCOPE"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "systemd.scope"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "systemd.scope - Scope unit configuration"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<scope>\\&.scope"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Scope units are not configured via unit configuration files, but are only "
"created programmatically using the bus interfaces of systemd\\&. They are "
"named similar to filenames\\&. A unit whose name ends in \"\\&.scope\" "
"refers to a scope unit\\&. Scopes units manage a set of system processes\\&. "
"Unlike service units, scope units manage externally created processes, and "
"do not fork off processes on its own\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The main purpose of scope units is grouping worker processes of a system "
"service for organization and for managing resources\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<systemd-run >B<--scope> may be used to easily launch a command in a new "
"scope unit from the command line\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"See the \\m[blue]B<New Control Group Interfaces>\\m[]\\&\\s-2\\u[1]\\d\\s+2 "
"for an introduction on how to make use of scope units from programs\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that, unlike service units, scope units have no \"main\" process: all "
"processes in the scope are equivalent\\&. The lifecycle of the scope unit is "
"thus not bound to the lifetime of one specific process, but to the existence "
"of at least one process in the scope\\&. This also means that the exit "
"statuses of these processes are not relevant for the scope unit failure "
"state\\&. Scope units may still enter a failure state, for example due to "
"resource exhaustion or stop timeouts being reached, but not due to programs "
"inside of them terminating uncleanly\\&. Since processes managed as scope "
"units generally remain children of the original process that forked them "
"off, it is also the job of that process to collect their exit statuses and "
"act on them as needed\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTOMATIC DEPENDENCIES"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Implicit Dependencies"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Implicit dependencies may be added as result of resource control parameters "
"as documented in B<systemd.resource-control>(5)\\&."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Default Dependencies"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following dependencies are added unless I<DefaultDependencies=no> is set:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Scope units will automatically have dependencies of type I<Conflicts=> and "
"I<Before=> on shutdown\\&.target\\&. These ensure that scope units are "
"removed prior to system shutdown\\&. Only scope units involved with early "
"boot or late system shutdown should disable I<DefaultDependencies=> "
"option\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Scope files may include a [Unit] section, which is described in B<systemd."
"unit>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Scope files may include a [Scope] section, which carries information about "
"the scope and the units it contains\\&. A number of options that may be used "
"in this section are shared with other unit types\\&. These options are "
"documented in B<systemd.kill>(5)  and B<systemd.resource-control>(5)\\&. The "
"options specific to the [Scope] section of scope units are the following:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<OOMPolicy=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Configure the out-of-memory (OOM) killing policy for the kernel and the "
"userspace OOM killer B<systemd-oomd.service>(8)\\&. On Linux, when memory "
"becomes scarce to the point that the kernel has trouble allocating memory "
"for itself, it might decide to kill a running process in order to free up "
"memory and reduce memory pressure\\&. Note that systemd-oomd\\&.service is a "
"more flexible solution that aims to prevent out-of-memory situations for the "
"userspace too, not just the kernel, by attempting to terminate services "
"earlier, before the kernel would have to act\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This setting takes one of B<continue>, B<stop> or B<kill>\\&. If set to "
"B<continue> and a process in the unit is killed by the OOM killer, this is "
"logged but the unit continues running\\&. If set to B<stop> the event is "
"logged but the unit is terminated cleanly by the service manager\\&. If set "
"to B<kill> and one of the unit\\*(Aqs processes is killed by the OOM killer "
"the kernel is instructed to kill all remaining processes of the unit too, by "
"setting the memory\\&.oom\\&.group attribute to B<1>; also see kernel page "
"\\m[blue]B<Control Group v2>\\m[]\\&\\s-2\\u[2]\\d\\s+2\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Defaults to the setting I<DefaultOOMPolicy=> in B<systemd-system.conf>(5)  "
"is set to, except for units where I<Delegate=> is turned on, where it "
"defaults to B<continue>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Use the I<OOMScoreAdjust=> setting to configure whether processes of the "
"unit shall be considered preferred or less preferred candidates for process "
"termination by the Linux OOM killer logic\\&. See B<systemd.exec>(5)  for "
"details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This setting also applies to B<systemd-oomd.service>(8)\\&. Similarly to the "
"kernel OOM kills performed by the kernel, this setting determines the state "
"of the unit after B<systemd-oomd> kills a cgroup associated with it\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "Added in version 243\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<RuntimeMaxSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Configures a maximum time for the scope to run\\&. If this is used and the "
"scope has been active for longer than the specified time it is terminated "
"and put into a failure state\\&. Pass \"infinity\" (the default) to "
"configure no runtime limit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "Added in version 244\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<RuntimeRandomizedExtraSec=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This option modifies I<RuntimeMaxSec=> by increasing the maximum runtime by "
"an evenly distributed duration between 0 and the specified value (in "
"seconds)\\&. If I<RuntimeMaxSec=> is unspecified, then this feature will be "
"disabled\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "Added in version 250\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Check B<systemd.unit>(5), B<systemd.exec>(5), and B<systemd.kill>(5)  for "
"more settings\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-run>(1), B<systemd.unit>(5), B<systemd.resource-"
"control>(5), B<systemd.service>(5), B<systemd.directives>(7)\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "New Control Group Interfaces"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"\\%https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid "Control Group v2"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "\\%https://docs.kernel.org/admin-guide/cgroup-v2.html"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This setting takes one of B<continue>, B<stop> or B<kill>\\&. If set to "
"B<continue> and a process in the unit is killed by the OOM killer, this is "
"logged but the unit continues running\\&. If set to B<stop> the event is "
"logged but the unit is terminated cleanly by the service manager\\&. If set "
"to B<kill> and one of the unit\\*(Aqs processes is killed by the OOM killer "
"the kernel is instructed to kill all remaining processes of the unit too, by "
"setting the memory\\&.oom\\&.group attribute to B<1>; also see "
"\\m[blue]B<kernel documentation>\\m[]\\&\\s-2\\u[2]\\d\\s+2\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6 opensuse-tumbleweed
msgid "kernel documentation"
msgstr ""
