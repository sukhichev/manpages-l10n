# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:11+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD\\&.TARGET"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "systemd.target"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "systemd.target - Target unit configuration"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "I<target>\\&.target"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A unit configuration file whose name ends in \"\\&.target\" encodes "
"information about a target unit of systemd\\&. Target units are used to "
"group units and to set synchronization points for ordering dependencies with "
"other unit files\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This unit type has no specific options\\&. See B<systemd.unit>(5)  for the "
"common options of all unit configuration files\\&. The common configuration "
"items are configured in the generic [Unit] and [Install] sections\\&. A "
"separate [Target] section does not exist, since no target-specific options "
"may be configured\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Target units do not offer any additional functionality on top of the generic "
"functionality provided by units\\&. They merely group units, allowing a "
"single target name to be used in I<Wants=> and I<Requires=> settings to "
"establish a dependency on a set of units defined by the target, and in "
"I<Before=> and I<After=> settings to establish ordering\\&. Targets "
"establish standardized names for synchronization points during boot and "
"shutdown\\&. Importantly, see B<systemd.special>(7)  for examples and "
"descriptions of standard systemd targets\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Target units provide a more flexible replacement for SysV runlevels in the "
"classic SysV init system\\&. For compatibility reasons special target units "
"such as runlevel3\\&.target exist which are used by the SysV runlevel "
"compatibility code in systemd, see B<systemd.special>(7)  for details\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that a target unit file must not be empty, lest it be considered a "
"masked unit\\&. It is recommended to provide a [Unit] section which includes "
"informative I<Description=> and I<Documentation=> options\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AUTOMATIC DEPENDENCIES"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Implicit Dependencies"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "There are no implicit dependencies for target units\\&."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Default Dependencies"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The following dependencies are added unless I<DefaultDependencies=no> is set:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Target units will automatically complement all configured dependencies of "
"type I<Wants=> or I<Requires=> with dependencies of type I<After=> unless "
"I<DefaultDependencies=no> is set in the specified units\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that the reverse is not true\\&. For example, defining B<Wants=that\\&."
"target> in some\\&.service will not automatically add the B<After=that\\&."
"target> ordering dependency for some\\&.service\\&. Instead, some\\&.service "
"should use the primary synchronization function of target type units, by "
"setting a specific B<After=that\\&.target> or B<Before=that\\&.target> "
"ordering dependency in its \\&.service unit file\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Target units automatically gain I<Conflicts=> and I<Before=> dependencies "
"against shutdown\\&.target\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Target unit files may include [Unit] and [Install] sections, which are "
"described in B<systemd.unit>(5)\\&. No options specific to this file type "
"are supported\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "B<Example\\ \\&1.\\ \\&Simple standalone target>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "# emergency-net\\&.target\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"[Unit]\n"
"Description=Emergency Mode with Networking\n"
"Requires=emergency\\&.target systemd-networkd\\&.service\n"
"After=emergency\\&.target systemd-networkd\\&.service\n"
"AllowIsolate=yes\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When adding dependencies to other units, it\\*(Aqs important to check if "
"they set I<DefaultDependencies=>\\&. Service units, unless they set "
"I<DefaultDependencies=no>, automatically get a dependency on sysinit\\&."
"target\\&. In this case, both emergency\\&.target and systemd-networkd\\&."
"service have I<DefaultDependencies=no>, so they are suitable for use in this "
"target, and do not pull in sysinit\\&.target\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"You can now switch into this emergency mode by running I<systemctl isolate "
"emergency-net\\&.target> or by passing the option I<systemd\\&."
"unit=emergency-net\\&.target> on the kernel command line\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Other units can have I<WantedBy=emergency-net\\&.target> in the I<[Install]> "
"section\\&. After they are enabled using B<systemctl enable>, they will be "
"started before I<emergency-net\\&.target> is started\\&. It is also possible "
"to add arbitrary units as dependencies of emergency\\&.target without "
"modifying them by using B<systemctl add-wants>\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemctl>(1), B<systemd.unit>(5), B<systemd.special>(7), "
"B<systemd.directives>(7)"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "systemd 254"
msgstr ""
