# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:29+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "TERMINAL-COLORS.D"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "File formats"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "terminal-colors.d - configure output colorization for various utilities"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "/etc/terminal-colors.d/I<[[name][@term].][type]>"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Files in this directory determine the default behavior for utilities when "
"coloring output."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The I<name> is a utility name. The name is optional and when none is "
"specified then the file is used for all unspecified utilities."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The I<term> is a terminal identifier (the B<TERM> environment variable). The "
"terminal identifier is optional and when none is specified then the file is "
"used for all unspecified terminals."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The I<type> is a file type. Supported file types are:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Turns off output colorization for all compatible utilities."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<enable>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Turns on output colorization; any matching B<disable> files are ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<scheme>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specifies colors used for output. The file format may be specific to the "
"utility, the default format is described below."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If there are more files that match for a utility, then the file with the "
"more specific filename wins. For example, the filename \"@xterm.scheme\" has "
"less priority than \"dmesg@xterm.scheme\". The lowest priority are those "
"files without a utility name and terminal identifier (e.g., \"disable\")."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The user-specific I<$XDG_CONFIG_HOME/terminal-colors.d> or I<$HOME/.config/"
"terminal-colors.d> overrides the global setting."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DEFAULT SCHEME FILES FORMAT"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The following statement is recognized:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<name color-sequence>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<name> is a logical name of color sequence (for example \"error\"). The "
"names are specific to the utilities. For more details always see the "
"B<COLORS> section in the man page for the utility."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<color-sequence> is a color name, ASCII color sequences or escape "
"sequences."
msgstr ""

#. type: SS
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Color names"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"black, blink, blue, bold, brown, cyan, darkgray, gray, green, halfbright, "
"lightblue, lightcyan, lightgray, lightgreen, lightmagenta, lightred, "
"magenta, red, reset, reverse, and yellow."
msgstr ""

#. type: SS
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ANSI color sequences"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The color sequences are composed of sequences of numbers separated by "
"semicolons. The most common codes are:"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ".sp\n"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "0"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "to restore default color"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "1"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for brighter colors"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "4"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for underlined text"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "5"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for flashing text"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "30"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for black foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "31"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for red foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "32"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for green foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "33"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for yellow (or brown) foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "34"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for blue foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "35"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for purple foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "36"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for cyan foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "37"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for white (or gray) foreground"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "40"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for black background"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "41"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for red background"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "42"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for green background"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "43"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for yellow (or brown) background"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "44"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for blue background"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "45"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for purple background"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "46"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for cyan background"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "47"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "for white (or gray) background"
msgstr ""

#. type: SS
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Escape sequences"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"To specify control or blank characters in the color sequences, C-style \\(rs-"
"escaped notation can be used:"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rsa>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Bell (ASCII 7)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rsb>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Backspace (ASCII 8)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rse>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Escape (ASCII 27)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rsf>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Form feed (ASCII 12)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rsn>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Newline (ASCII 10)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rsr>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Carriage Return (ASCII 13)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rst>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Tab (ASCII 9)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rsv>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Vertical Tab (ASCII 11)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rs?>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Delete (ASCII 127)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rs_>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Space"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rs\\(rs>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Backslash (\\(rs)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rs^>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Caret (^)"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "B<\\(rs#>"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Hash mark (#)"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Please note that escapes are necessary to enter a space, backslash, caret, "
"or any control character anywhere in the string, as well as a hash mark as "
"the first character."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"For example, to use a red background for alert messages in the output of "
"B<dmesg>(1), use:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<echo \\(aqalert 37;41\\(aq E<gt>E<gt> /etc/terminal-colors.d/dmesg.scheme>"
msgstr ""

#. type: SS
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "Comments"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Lines where the first non-blank character is a # (hash) are ignored. Any "
"other use of the hash character is not interpreted as introducing a comment."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<TERMINAL_COLORS_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "enables debug output."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<$XDG_CONFIG_HOME/terminal-colors.d>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I<$HOME/.config/terminal-colors.d>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "I</etc/terminal-colors.d>"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Disable colors for all compatible utilities:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<touch /etc/terminal-colors.d/disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Disable colors for all compatible utils on a vt100 terminal:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<touch /etc/terminal-colors.d/@vt100.disable>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Disable colors for all compatible utils except B<dmesg>(1):"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<touch /etc/terminal-colors.d/dmesg.enable>"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "COMPATIBILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The B<terminal-colors.d> functionality is currently supported by all util-"
"linux utilities which provides colorized output. For more details always see "
"the B<COLORS> section in the man page for the utility."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<terminal-colors.d> is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The I<term> is a terminal identifier (the TERM environment variable). The "
"terminal identifier is optional and when none is specified then the file is "
"used for all unspecified terminals."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The B<name> is a logical name of color sequence (for example \"error\"). The "
"names are specific to the utilities. For more details always see the COLORS "
"section in the man page for the utility."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "TERMINAL_COLORS_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The terminal-colors.d functionality is currently supported by all util-linux "
"utilities which provides colorized output. For more details always see the "
"COLORS section in the man page for the utility."
msgstr ""
