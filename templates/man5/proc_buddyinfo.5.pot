# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-23 07:59+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "proc_buddyinfo"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "/proc/buddyinfo - memory fragmentation"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/buddyinfo>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This file contains information which is used for diagnosing memory "
"fragmentation issues.  Each line starts with the identification of the node "
"and the name of the zone which together identify a memory region.  This is "
"then followed by the count of available chunks of a certain order in which "
"these zones are split.  The size in bytes of a certain order is given by the "
"formula:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(2\\[ha]order)\\ *\\ PAGE_SIZE\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"The binary buddy allocator algorithm inside the kernel will split one chunk "
"into two chunks of a smaller order (thus with half the size) or combine two "
"contiguous chunks into one larger chunk of a higher order (thus with double "
"the size) to satisfy allocation requests and to counter memory "
"fragmentation.  The order matches the column number, when starting to count "
"at zero."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "For example on an x86-64 system:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"Node 0, zone     DMA     1    1    1    0    2    1    1    0    1    1    3\n"
"Node 0, zone   DMA32    65   47    4   81   52   28   13   10    5    1  404\n"
"Node 0, zone  Normal   216   55  189  101   84   38   37   27    5    3  587\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"In this example, there is one node containing three zones and there are 11 "
"different chunk sizes.  If the page size is 4 kilobytes, then the first zone "
"called I<DMA> (on x86 the first 16 megabyte of memory) has 1 chunk of 4 "
"kilobytes (order 0) available and has 3 chunks of 4 megabytes (order 10) "
"available."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"If the memory is heavily fragmented, the counters for higher order chunks "
"will be zero and allocation of large contiguous areas will fail."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "Further information about the zones can be found in I</proc/zoneinfo>."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "B<proc>(5)"
msgstr ""
