# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-23 08:08+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "proc_kpageflags"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "/proc/kpageflags - physical pages frame masks"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/kpageflags> (since Linux 2.6.25)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This file contains 64-bit masks corresponding to each physical page frame; "
"it is indexed by page frame number (see the discussion of I</proc/>pidI</"
"pagemap>).  The bits are as follows:"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "0"
msgstr ""

#.  KPF_IDLE: commit f074a8f49eb87cde95ac9d040ad5e7ea4f029738
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "-"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_LOCKED"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "1"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_ERROR"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_REFERENCED"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "3"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_UPTODATE"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "4"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_DIRTY"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "5"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_LRU"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "6"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_ACTIVE"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "7"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_SLAB"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "8"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_WRITEBACK"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "9"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_RECLAIM"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "10"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_BUDDY"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "11"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_MMAP"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(since Linux 2.6.31)"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "12"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_ANON"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "13"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_SWAPCACHE"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "14"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_SWAPBACKED"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "15"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_COMPOUND_HEAD"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "16"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_COMPOUND_TAIL"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "17"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_HUGE"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "18"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_UNEVICTABLE"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "19"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_HWPOISON"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "20"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_NOPAGE"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "21"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_KSM"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(since Linux 2.6.32)"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "22"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_THP"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(since Linux 3.4)"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "23"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_BALLOON"
msgstr ""

#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(since Linux 3.18)"
msgstr ""

#.  KPF_BALLOON: commit 09316c09dde33aae14f34489d9e3d243ec0d5938
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "24"
msgstr ""

#.  KPF_BALLOON: commit 09316c09dde33aae14f34489d9e3d243ec0d5938
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_ZERO_PAGE"
msgstr ""

#.  KPF_BALLOON: commit 09316c09dde33aae14f34489d9e3d243ec0d5938
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(since Linux 4.0)"
msgstr ""

#.  KPF_ZERO_PAGE: commit 56873f43abdcd574b25105867a990f067747b2f4
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "25"
msgstr ""

#.  KPF_ZERO_PAGE: commit 56873f43abdcd574b25105867a990f067747b2f4
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_IDLE"
msgstr ""

#.  KPF_ZERO_PAGE: commit 56873f43abdcd574b25105867a990f067747b2f4
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(since Linux 4.3)"
msgstr ""

#.  KPF_IDLE: commit f074a8f49eb87cde95ac9d040ad5e7ea4f029738
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "26"
msgstr ""

#.  KPF_IDLE: commit f074a8f49eb87cde95ac9d040ad5e7ea4f029738
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KPF_PGTABLE"
msgstr ""

#.  KPF_IDLE: commit f074a8f49eb87cde95ac9d040ad5e7ea4f029738
#. type: tbl table
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(since Linux 4.18)"
msgstr ""

#.  commit ad3bdefe877afb47480418fdb05ecd42842de65e
#.  commit e07a4b9217d1e97d2f3a62b6b070efdc61212110
#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"For further details on the meanings of these bits, see the kernel source "
"file I<Documentation/admin-guide/mm/pagemap.rst>.  Before Linux 2.6.29, "
"B<KPF_WRITEBACK>, B<KPF_RECLAIM>, B<KPF_BUDDY>, and B<KPF_LOCKED> did not "
"report correctly."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"The I</proc/kpageflags> file is present only if the "
"B<CONFIG_PROC_PAGE_MONITOR> kernel configuration option is enabled."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "B<proc>(5)"
msgstr ""
