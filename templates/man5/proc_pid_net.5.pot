# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-23 08:18+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "proc_pid_net"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-08-15"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "/proc/pid/net/, /proc/net/ - network layer information"
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/>pidI</net/> (since Linux 2.6.25)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "See the description of I</proc/net>."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This directory contains various files and subdirectories containing "
"information about the networking layer.  The files contain ASCII structures "
"and are, therefore, readable with B<cat>(1).  However, the standard "
"B<netstat>(8)  suite provides much cleaner access to these files."
msgstr ""

#.  commit e9720acd728a46cb40daa52c99a979f7c4ff195c
#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"With the advent of network namespaces, various information relating to the "
"network stack is virtualized (see B<network_namespaces>(7)).  Thus, since "
"Linux 2.6.25, I</proc/net> is a symbolic link to the directory I</proc/self/"
"net>, which contains the same files and directories as listed below.  "
"However, these files and directories now expose information for the network "
"namespace of which the process is a member."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/arp>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This holds an ASCII readable dump of the kernel ARP table used for address "
"resolutions.  It will show both dynamically learned and preprogrammed ARP "
"entries.  The format is:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"IP address     HW type   Flags     HW address          Mask   Device\n"
"192.168.0.50   0x1       0x2       00:50:BF:25:68:F3   *      eth0\n"
"192.168.0.250  0x1       0xc       00:00:00:00:00:00   *      eth0\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Here \"IP address\" is the IPv4 address of the machine and the \"HW type\" "
"is the hardware type of the address from RFC\\ 826.  The flags are the "
"internal flags of the ARP structure (as defined in I</usr/include/linux/"
"if_arp.h>)  and the \"HW address\" is the data link layer mapping for that "
"IP address if it is known."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/dev>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"The dev pseudo-file contains network device status information.  This gives "
"the number of received and sent packets, the number of errors and collisions "
"and other basic statistics.  These are used by the B<ifconfig>(8)  program "
"to report device status.  The format is:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"Inter-|   Receive                                                |  Transmit\n"
" face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed\n"
"    lo: 2776770   11307    0    0    0     0          0         0  2776770   11307    0    0    0     0       0          0\n"
"  eth0: 1215645    2751    0    0    0     0          0         0  1782404    4324    0    0    0   427       0          0\n"
"  ppp0: 1622270    5552    1    0    0     0          0         0   354130    5669    0    0    0     0       0          0\n"
"  tap0:    7714      81    0    0    0     0          0         0     7714      81    0    0    0     0       0          0\n"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/dev_mcast>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "Defined in I</usr/src/linux/net/core/dev_mcast.c>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"indx interface_name  dmi_u dmi_g dmi_address\n"
"2    eth0            1     0     01005e000001\n"
"3    eth1            1     0     01005e000001\n"
"4    eth2            1     0     01005e000001\n"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/igmp>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Internet Group Management Protocol.  Defined in I</usr/src/linux/net/core/"
"igmp.c>."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/rarp>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This file uses the same format as the I<arp> file and contains the current "
"reverse mapping database used to provide B<rarp>(8)  reverse address lookup "
"services.  If RARP is not configured into the kernel, this file will not be "
"present."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/raw>"
msgstr ""

#.  .TP
#.  .I /proc/net/route
#.  No information, but looks similar to
#.  .BR route (8).
#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Holds a dump of the RAW socket table.  Much of the information is not of use "
"apart from debugging.  The \"sl\" value is the kernel hash slot for the "
"socket, the \"local_address\" is the local address and protocol number "
"pair.  \\&\"St\" is the internal status of the socket.  The \"tx_queue\" and "
"\"rx_queue\" are the outgoing and incoming data queue in terms of kernel "
"memory usage.  The \"tr\", \"tm-E<gt>when\", and \"rexmits\" fields are not "
"used by RAW.  The \"uid\" field holds the effective UID of the creator of "
"the socket."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/snmp>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This file holds the ASCII data needed for the IP, ICMP, TCP, and UDP "
"management information bases for an SNMP agent."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/tcp>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Holds a dump of the TCP socket table.  Much of the information is not of use "
"apart from debugging.  The \"sl\" value is the kernel hash slot for the "
"socket, the \"local_address\" is the local address and port number pair.  "
"The \"rem_address\" is the remote address and port number pair (if "
"connected).  \\&\"St\" is the internal status of the socket.  The "
"\"tx_queue\" and \"rx_queue\" are the outgoing and incoming data queue in "
"terms of kernel memory usage.  The \"tr\", \"tm-E<gt>when\", and \"rexmits\" "
"fields hold internal information of the kernel socket state and are useful "
"only for debugging.  The \"uid\" field holds the effective UID of the "
"creator of the socket."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/udp>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Holds a dump of the UDP socket table.  Much of the information is not of use "
"apart from debugging.  The \"sl\" value is the kernel hash slot for the "
"socket, the \"local_address\" is the local address and port number pair.  "
"The \"rem_address\" is the remote address and port number pair (if "
"connected).  \"St\" is the internal status of the socket.  The \"tx_queue\" "
"and \"rx_queue\" are the outgoing and incoming data queue in terms of kernel "
"memory usage.  The \"tr\", \"tm-E<gt>when\", and \"rexmits\" fields are not "
"used by UDP.  The \"uid\" field holds the effective UID of the creator of "
"the socket.  The format is:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"sl  local_address rem_address   st tx_queue rx_queue tr rexmits  tm-E<gt>when uid\n"
" 1: 01642C89:0201 0C642C89:03FF 01 00000000:00000001 01:000071BA 00000000 0\n"
" 1: 00000000:0801 00000000:0000 0A 00000000:00000000 00:00000000 6F000100 0\n"
" 1: 00000000:0201 00000000:0000 0A 00000000:00000000 00:00000000 00000000 0\n"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/unix>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Lists the UNIX domain sockets present within the system and their status.  "
"The format is:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"Num RefCount Protocol Flags    Type St Inode Path\n"
" 0: 00000002 00000000 00000000 0001 03    42\n"
" 1: 00000001 00000000 00010000 0001 01  1948 /dev/printer\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "The fields are as follows:"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<Num>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "the kernel table slot number."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<RefCount>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "the number of users of the socket."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<Protocol>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "currently always 0."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<Flags>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "the internal kernel flags holding the status of the socket."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<Type>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"the socket type.  For B<SOCK_STREAM> sockets, this is 0001; for "
"B<SOCK_DGRAM> sockets, it is 0002; and for B<SOCK_SEQPACKET> sockets, it is "
"0005."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<St>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "the internal state of the socket."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<Inode>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "the inode number of the socket."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I<Path>:"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"the bound pathname (if any) of the socket.  Sockets in the abstract "
"namespace are included in the list, and are shown with a I<Path> that "
"commences with the character '@'."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "I</proc/net/netfilter/nfnetlink_queue>"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"This file contains information about netfilter user-space queueing, if "
"used.  Each line represents a queue.  Queues that have not been subscribed "
"to by user space are not shown."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"   1   4207     0  2 65535     0     0        0  1\n"
"  (1)   (2)    (3)(4)  (5)    (6)   (7)      (8)\n"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "The fields in each line are:"
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(1)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"The ID of the queue.  This matches what is specified in the B<--queue-num> "
"or B<--queue-balance> options to the B<iptables>(8)  NFQUEUE target.  See "
"B<iptables-extensions>(8)  for more information."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(2)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "The netlink port ID subscribed to the queue."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(3)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"The number of packets currently queued and waiting to be processed by the "
"application."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(4)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"The copy mode of the queue.  It is either 1 (metadata only) or 2 (also copy "
"payload data to user space)."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(5)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Copy range; that is, how many bytes of packet payload should be copied to "
"user space at most."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(6)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"queue dropped.  Number of packets that had to be dropped by the kernel "
"because too many packets are already waiting for user space to send back the "
"mandatory accept/drop verdicts."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(7)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"queue user dropped.  Number of packets that were dropped within the netlink "
"subsystem.  Such drops usually happen when the corresponding socket buffer "
"is full; that is, user space is not able to read messages fast enough."
msgstr ""

#. type: TP
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "(8)"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"sequence number.  Every queued packet is associated with a (32-bit)  "
"monotonically increasing sequence number.  This shows the ID of the most "
"recent packet queued."
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "The last number exists only for compatibility reasons and is always 1."
msgstr ""

#. type: SH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
msgid "B<proc>(5)"
msgstr ""
