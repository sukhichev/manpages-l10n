# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 16:58+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "FSCK.MINIX"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "fsck.minix - check consistency of Minix filesystem"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<fsck.minix> [options] I<device>"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<fsck.minix> performs a consistency check for the Linux MINIX filesystem."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The program assumes the filesystem is quiescent. B<fsck.minix> should not be "
"used on a mounted device unless you can be sure nobody is writing to it. "
"Remember that the kernel can write to device when it searches for files."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The I<device> name will usually have the following form:"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ".sp\n"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "/dev/hda[1-63]"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "IDE disk 1"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "/dev/hdb[1-63]"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "IDE disk 2"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "/dev/sda[1-15]"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SCSI disk 1"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "/dev/sdb[1-15]"
msgstr ""

#. type: tbl table
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SCSI disk 2"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"If the filesystem was changed, i.e., repaired, then B<fsck.minix> will print "
"\"FILE SYSTEM HAS BEEN CHANGED\" and will B<sync>(2) three times before "
"exiting. There is I<no> need to reboot after check."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "WARNING"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<fsck.minix> should B<not> be used on a mounted filesystem. Using B<fsck."
"minix> on a mounted filesystem is very dangerous, due to the possibility "
"that deleted files are still in use, and can seriously damage a perfectly "
"good filesystem! If you absolutely have to run B<fsck.minix> on a mounted "
"filesystem, such as the root filesystem, make sure nothing is writing to the "
"disk, and that no files are \"zombies\" waiting for deletion."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-l>, B<--list>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "List all filenames."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--repair>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Perform interactive repairs."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--auto>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Perform automatic repairs. This option implies B<--repair> and serves to "
"answer all of the questions asked with the default. Note that this can be "
"extremely dangerous in the case of extensive filesystem damage."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Be verbose."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--super>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Output super-block information."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-m>, B<--uncleared>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Activate MINIX-like \"mode not cleared\" warnings."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-f>, B<--force>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Force a filesystem check even if the filesystem was marked as valid. Marking "
"is done by the kernel when the filesystem is unmounted."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DIAGNOSTICS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"There are numerous diagnostic messages. The ones mentioned here are the most "
"commonly seen in normal usage."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If the device does not exist, B<fsck.minix> will print \"unable to read "
"super block\". If the device exists, but is not a MINIX filesystem, B<fsck."
"minix> will print \"bad magic number in super-block\"."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The exit status returned by B<fsck.minix> is the sum of the following:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "No errors"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<3>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Filesystem errors corrected, system should be rebooted if filesystem was "
"mounted"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<4>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Filesystem errors left uncorrected"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<7>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Combination of exit statuses 3 and 4"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<8>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Operational error"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<16>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Usage or syntax error"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Exit status values by"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Added support for filesystem valid flag:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Check to prevent fsck of mounted filesystem added by"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Minix v2 fs support by"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "updated by"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Portability patch by"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<fsck>(8), B<fsck.ext2>(8), B<mkfs>(8), B<mkfs.ext2>(8), B<mkfs.minix>(8), "
"B<reboot>(8)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<fsck.minix> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"If the filesystem was changed, i.e., repaired, then B<fsck.minix> will print "
"\"FILE SYSTEM HAS CHANGED\" and will B<sync>(2) three times before exiting. "
"There is I<no> need to reboot after check."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr ""
