# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-05-03 10:33+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "BLKPR"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "2023-03-23"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "util-linux 2.39-rc3"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "blkpr - run persistent reservations command on a device"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<blkpr> [options] I<device>"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"B<blkpr> is used to run persistent reservations command on device that "
"supports Persistent Reservations feature."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "The I<device> argument is the pathname of the block device."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-c>, B<--command> I<command>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The command of persistent reservations, supported commands are B<register>, "
"B<reserve>, B<release>, B<preempt>, B<preempt-abort>, and B<clear>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-k>, B<--key> I<key>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "The key the command should operate on."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-K>, B<--oldkey> I<oldkey>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "The old key the command should operate on."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-f>, B<--flag> I<flag>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Supported flag is B<ignore-key>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-t>, B<--type> I<type>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Supported types are B<write-exclusive>, B<exclusive-access>, B<write-"
"exclusive-reg-only>, B<exclusive-access-reg-only>, B<write-exclusive-all-"
"regs>, and B<exclusive-access-all-regs>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Display version information and exit."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Display help text and exit."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<sg_persist>(8)"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Linux documentation at:"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "iSCSI specification at:"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "NVMe-oF specification at:"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The B<blkpr> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
