# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 16:57+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "FINDMNT"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "findmnt - find a filesystem"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt> [options]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt> [options] I<device>|I<mountpoint>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<findmnt> [options] [B<--source>] I<device> [B<--target> I<path>|B<--"
"mountpoint> I<mountpoint>]"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<findmnt> will list all mounted filesystems or search for a filesystem. The "
"B<findmnt> command is able to search in I</etc/fstab>, I</etc/mtab> or I</"
"proc/self/mountinfo>. If I<device> or I<mountpoint> is not given, all "
"filesystems are shown."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The device may be specified by device name, major:minor numbers, filesystem "
"label or UUID, or partition label or UUID. Note that B<findmnt> follows "
"B<mount>(8) behavior where a device name may be interpreted as a mountpoint "
"(and vice versa) if the B<--target>, B<--mountpoint> or B<--source> options "
"are not specified."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The command-line option B<--target> accepts any file or directory and then "
"B<findmnt> displays the filesystem for the given path."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The command prints all mounted filesystems in the tree-like format by "
"default."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The relationship between block devices and filesystems is not always one-to-"
"one. The filesystem may use more block devices. This is why B<findmnt> "
"provides SOURCE and SOURCES (pl.) columns. The column SOURCES displays all "
"devices where it is possible to find the same filesystem UUID (or another "
"tag specified in I<fstab> when executed with B<--fstab> and B<--evaluate>)."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-A>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Disable all built-in filters and print all filesystems."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--ascii>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use ascii characters for tree formatting."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-b>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-C>, B<--nocanonicalize>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Do not canonicalize paths at all. This option affects the comparing of paths "
"and the evaluation of tags (LABEL, UUID, etc.)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--canonicalize>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Canonicalize all printed paths."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--deleted>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print filesystems where target (mountpoint) is marked as deleted by kernel."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-D>, B<--df>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Imitate the output of B<df>(1). This option is equivalent to B<-o SOURCE,"
"FSTYPE,SIZE,USED,AVAIL,USE%,TARGET> but excludes all pseudo filesystems. Use "
"B<--all> to print all filesystems."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--direction> I<word>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "The search direction, either B<forward> or B<backward>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-e>, B<--evaluate>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Convert all tags (LABEL, UUID, PARTUUID, or PARTLABEL) to the corresponding "
"device names for the SOURCE column.  It\\(cqs an unusual situation, but the "
"same tag may be duplicated (used for more devices). For this purpose, there "
"is SOURCES (pl.) column. This column displays by multi-line cell all devices "
"where the tag is detected by libblkid. This option makes sense for I<fstab> "
"only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-F>, B<--tab-file> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Search in an alternative file. If used with B<--fstab>, B<--mtab> or B<--"
"kernel>, then it overrides the default paths. If specified more than once, "
"then tree-like output is disabled (see the B<--list> option)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-f>, B<--first-only>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Print the first matching filesystem only."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-i>, B<--invert>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Invert the sense of matching."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Use JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-k>, B<--kernel>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Search in I</proc/self/mountinfo>. The output is in the tree-like format. "
"This is the default. The output contains only mount options maintained by "
"kernel (see also B<--mtab>)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-l>, B<--list>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Use the list output format. This output format is automatically enabled if "
"the output is restricted by the B<-t>, B<-O>, B<-S> or B<-T> option and the "
"option B<--submounts> is not used or if more that one source file (the "
"option B<-F>) is specified."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-M>, B<--mountpoint> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Explicitly define the mountpoint file or directory. See also B<--target>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-m>, B<--mtab>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Search in I</etc/mtab>. The output is in the list format by default (see B<--"
"tree>). The output may include user space mount options."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-N>, B<--task> I<tid>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Use alternative namespace I</proc/E<lt>tidE<gt>/mountinfo> rather than the "
"default I</proc/self/mountinfo>. If the option is specified more than once, "
"then tree-like output is disabled (see the B<--list> option). See also the "
"B<unshare>(1) command."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-O>, B<--options> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Limit the set of printed filesystems. More than one option may be specified "
"in a comma-separated list. The B<-t> and B<-O> options are cumulative in "
"effect. It is different from B<-t> in that each option is matched exactly; a "
"leading I<no> at the beginning does not have global meaning. The \"no\" can "
"used for individual items in the list. The \"no\" prefix interpretation can "
"be disabled by \"+\" prefix."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Define output columns. See the B<--help> output to get a list of the "
"currently supported columns. The B<TARGET> column contains tree formatting "
"if the B<--list> or B<--raw> options are not specified."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<findmnt -o +PROPAGATION>)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Output almost all available columns. The columns that require B<--poll> are "
"not included."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-P>, B<--pairs>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>). See also option B<--"
"shell>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-p>, B<--poll>[I<=list>]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Monitor changes in the I</proc/self/mountinfo> file. Supported actions are: "
"mount, umount, remount and move. More than one action may be specified in a "
"comma-separated list. All actions are monitored by default."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The time for which B<--poll> will block can be restricted with the B<--"
"timeout> or B<--first-only> options."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The standard columns always use the new version of the information from the "
"mountinfo file, except the umount action which is based on the original "
"information cached by B<findmnt>. The poll mode allows using extra columns:"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<ACTION>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"mount, umount, move or remount action name; this column is enabled by default"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<OLD-TARGET>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "available for umount and move actions"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<OLD-OPTIONS>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "available for umount and remount actions"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--pseudo>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Print only pseudo filesystems."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--shadow>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Print only filesystems over-mounted by another filesystem."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-R>, B<--submounts>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Print recursively all submounts for the selected filesystems. The "
"restrictions defined by options B<-t>, B<-O>, B<-S>, B<-T> and B<--"
"direction> are not applied to submounts. All submounts are always printed in "
"tree-like order. The option enables the tree-like output format by default. "
"This option has no effect for B<--mtab> or B<--fstab>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Use raw output format. All potentially unsafe characters are hex-escaped "
"(\\(rsxE<lt>codeE<gt>)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--real>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Print only real filesystems."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-S>, B<--source> I<spec>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Explicitly define the mount source. Supported specifications are I<device>, "
"I<maj>B<:>I<min>, B<LABEL=>I<label>, B<UUID=>I<uuid>, B<PARTLABEL=>I<label> "
"and B<PARTUUID=>I<uuid>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--fstab>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Search in I</etc/fstab>. The output is in the list format (see B<--list>)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-T>, B<--target> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Define the mount target. If I<path> is not a mountpoint file or directory, "
"then B<findmnt> checks the I<path> elements in reverse order to get the "
"mountpoint (this feature is supported only when searching in kernel files "
"and unsupported for B<--fstab>). It\\(cqs recommended to use the option B<--"
"mountpoint> when checks of I<path> elements are unwanted and I<path> is a "
"strictly specified mountpoint."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-t>, B<--types> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Limit the set of printed filesystems. More than one type may be specified in "
"a comma-separated list. The list of filesystem types can be prefixed with "
"B<no> to specify the filesystem types on which no action should be taken. "
"For more details see B<mount>(8)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--tree>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Enable tree-like output if possible. The options is silently ignored for "
"tables where is missing child-parent relation (e.g., I<fstab>)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--shadowed>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-U>, B<--uniq>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Ignore filesystems with duplicate mount targets, thus effectively skipping "
"over-mounted mount points."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-u>, B<--notruncate>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Do not truncate text in columns. The default is to not truncate the "
"B<TARGET>, B<SOURCE>, B<UUID>, B<LABEL>, B<PARTUUID>, B<PARTLABEL> columns. "
"This option disables text truncation also in all other columns."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-v>, B<--nofsroot>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Do not print a [/dir] in the SOURCE column for bind mounts or btrfs "
"subvolumes."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-w>, B<--timeout> I<milliseconds>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specify an upper limit on the time for which B<--poll> will block, in "
"milliseconds."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-x>, B<--verify>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Check mount table content. The default is to verify I</etc/fstab> "
"parsability and usability. It\\(cqs possible to use this option also with "
"B<--tab-file>. It\\(cqs possible to specify source (device) or target "
"(mountpoint) to filter mount table. The option B<--verbose> forces "
"B<findmnt> to print more details."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Force B<findmnt> to print more information (B<--verify> only for now)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--vfs-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"When used with B<VFS-OPTIONS> column, print all VFS (fs-independent) flags. "
"This option is designed for auditing purposes to list also default VFS "
"kernel mount options which are normally not listed."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-y>, B<--shell>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The column name will be modified to contain only characters allowed for "
"shell variable identifiers. This is usable, for example, with B<--pairs>. "
"Note that this feature has been automatically enabled for B<--pairs> in "
"version 2.37, but due to compatibility issues, now it\\(cqs necessary to "
"request this behavior by B<--shell>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The exit value is 0 if there is something to display, or 1 on any error (for "
"example if no filesystem is found based on the user\\(cqs filter "
"specification, or the device path or mountpoint does not exist)."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_FSTAB>=E<lt>pathE<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "overrides the default location of the I<fstab> file"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_MTAB>=E<lt>pathE<gt>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "overrides the default location of the I<mtab> file"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBMOUNT_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "enables libmount debug output"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBSMARTCOLS_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "enables libsmartcols debug output"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<LIBSMARTCOLS_DEBUG_PADDING>=on"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "use visible padding characters."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt --fstab -t nfs>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Prints all NFS filesystems defined in I</etc/fstab>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt --fstab /mnt/foo>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Prints all I</etc/fstab> filesystems where the mountpoint directory is I</"
"mnt/foo>. It also prints bind mounts where I</mnt/foo> is a source."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt --fstab --target /mnt/foo>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Prints all I</etc/fstab> filesystems where the mountpoint directory is I</"
"mnt/foo>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt --fstab --evaluate>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Prints all I</etc/fstab> filesystems and converts LABEL= and UUID= tags to "
"the real device names."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt -n --raw --evaluate --output=target LABEL=/boot>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Prints only the mountpoint where the filesystem with label \"/boot\" is "
"mounted."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt --poll --mountpoint /mnt/foo>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Monitors mount, unmount, remount and move on I</mnt/foo>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt --poll=umount --first-only --mountpoint /mnt/foo>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Waits for I</mnt/foo> unmount."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt --poll=remount -t ext3 -O ro>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Monitors remounts to read-only mode on all ext3 filesystems."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<fstab>(5), B<mount>(8)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<findmnt> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Print the SIZE, USED and AVAIL columns in bytes rather than in a human-"
"readable format."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Convert all tags (LABEL, UUID, PARTUUID or PARTLABEL) to the corresponding "
"device names."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Search in I</proc/self/mountinfo>. The output is in the tree-like format. "
"This is the default. The output contains only mount options maintained by "
"kernel (see also B<--mtab)>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>). The key (variable "
"name) will be modified to contain only characters allowed for a shell "
"variable identifiers, for example, FS_OPTIONS and USE_PCT instead of FS-"
"OPTIONS and USE%."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Enable tree-like output if possible. The options is silently ignored for "
"tables where is missing child-parent relation (e.g., fstab)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"Check mount table content. The default is to verify I</etc/fstab> "
"parsability and usability. It\\(cqs possible to use this option also with "
"B<--tab-file>. It\\(cqs possible to specify source (device) or target "
"(mountpoint) to filter mount table. The option B<--verbose> forces findmnt "
"to print more details."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Force findmnt to print more information (B<--verify> only for now)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "LIBMOUNT_FSTAB=E<lt>pathE<gt>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "overrides the default location of the fstab file"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "LIBMOUNT_MTAB=E<lt>pathE<gt>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "overrides the default location of the mtab file"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "LIBMOUNT_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "LIBSMARTCOLS_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "LIBSMARTCOLS_DEBUG_PADDING=on"
msgstr ""
