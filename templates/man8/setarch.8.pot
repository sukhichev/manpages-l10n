# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 17:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SETARCH"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"setarch - change reported architecture in new program environment and/or set "
"personality flags"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<setarch> [I<arch>] [options] [I<program> [I<argument>...]]"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<setarch> B<--list>|B<-h>|B<-V>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<arch> [options] [I<program> [I<argument>...]]"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<setarch> modifies execution domains and process personality flags."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The execution domains currently only affects the output of B<uname -m>. For "
"example, on an AMD64 system, running B<setarch i386> I<program> will cause "
"I<program> to see i686 instead of I<x86_64> as the machine type. It can also "
"be used to set various personality options. The default I<program> is B</bin/"
"sh>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Since version 2.33 the I<arch> command line argument is optional and "
"B<setarch> may be used to change personality flags (ADDR_LIMIT_*, "
"SHORT_INODE, etc) without modification of the execution domain."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--list>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"List the architectures that B<setarch> knows about. Whether B<setarch> can "
"actually set each of these architectures depends on the running kernel."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--uname-2.6>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Causes the I<program> to see a kernel version number beginning with 2.6. "
"Turns on B<UNAME26>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Be verbose."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-3>, B<--3gb>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Specifies I<program> should use a maximum of 3GB of address space. Supported "
"on x86. Turns on B<ADDR_LIMIT_3GB>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<--4gb>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option has no effect. It is retained for backward compatibility only, "
"and may be removed in future releases."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-B>, B<--32bit>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Limit the address space to 32 bits to emulate hardware. Supported on ARM and "
"Alpha. Turns on B<ADDR_LIMIT_32BIT>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-F>, B<--fdpic-funcptrs>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Treat user-space function pointers to signal handlers as pointers to address "
"descriptors. This option has no effect on architectures that do not support "
"B<FDPIC> ELF binaries. In kernel v4.14 support is limited to ARM, Blackfin, "
"Fujitsu FR-V, and SuperH CPU architectures."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-I>, B<--short-inode>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Obsolete bug emulation flag. Turns on B<SHORT_INODE>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-L>, B<--addr-compat-layout>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Provide legacy virtual address space layout. Use when the I<program> binary "
"does not have B<PT_GNU_STACK> ELF header. Turns on B<ADDR_COMPAT_LAYOUT>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-R>, B<--addr-no-randomize>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Disables randomization of the virtual address space. Turns on "
"B<ADDR_NO_RANDOMIZE>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-S>, B<--whole-seconds>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Obsolete bug emulation flag. Turns on B<WHOLE_SECONDS>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-T>, B<--sticky-timeouts>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This makes B<select>(2), B<pselect>(2), and B<ppoll>(2) system calls "
"preserve the timeout value instead of modifying it to reflect the amount of "
"time not slept when interrupted by a signal handler. Use when I<program> "
"depends on this behavior. For more details see the timeout description in "
"B<select>(2) manual page. Turns on B<STICKY_TIMEOUTS>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-X>, B<--read-implies-exec>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If this is set then B<mmap>(3p) B<PROT_READ> will also add the B<PROT_EXEC> "
"bit - as expected by legacy x86 binaries. Notice that the ELF loader will "
"automatically set this bit when it encounters a legacy binary. Turns on "
"B<READ_IMPLIES_EXEC>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-Z>, B<--mmap-page-zero>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"SVr4 bug emulation that will set B<mmap>(3p) page zero as read-only. Use "
"when I<program> depends on this behavior, and the source code is not "
"available to be fixed. Turns on B<MMAP_PAGE_ZERO>."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"setarch --addr-no-randomize mytestprog\n"
"setarch ppc32 rpmbuild --target=ppc --rebuild foo.src.rpm\n"
"setarch ppc32 -v -vL3 rpmbuild --target=ppc --rebuild bar.src.rpm\n"
"setarch ppc32 --32bit rpmbuild --target=ppc --rebuild foo.src.rpm\n"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<personality>(2), B<select>(2)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<setarch> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr ""
