# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-05-03 10:33+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "FADVISE"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "2023-04-17"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "util-linux 2.39-rc3"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "fadvise - utility to use the posix_fadvise system call"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"B<fadvise> [B<-a> I<advice>] [B<-o> I<offset>] [B<-l> I<length>] I<filename>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"B<fadvise> [B<-a> I<advice>] [B<-o> I<offset>] [B<-l> I<length>] -d I<file-"
"descriptor>"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"B<fadvise> is a simple command wrapping posix_fadvise system call that is "
"for predeclaring an access pattern for file data."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-d>, B<--fd> I<file-descriptor>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Apply the advice to the file specified with the file descriptor instead of "
"open a file specified with a file name."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-a>, B<--advice> I<advice>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"See the command output with B<--help> option for available values for "
"advice. If this option is omitted, \"dontneed\" is used as default advice."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-o>, B<--offset> I<offset>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Specifies the beginning offset of the range, in bytes.  If this option is "
"omitted, 0 is used as default advice."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-l>, B<--length> I<length>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Specifies the length of the range, in bytes.  If this option is omitted, 0 "
"is used as default advice."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Print version and exit."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<fadvise> has the following exit status values:"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<0>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "success"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<1>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "unspecified failure"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<posix_fadvise>(2)"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The B<fadvise> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
