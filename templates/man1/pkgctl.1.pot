# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:04+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "PKGCTL"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "02/14/2024"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \" "
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "\\ \""
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl - Unified command-line frontend for devtools"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl [SUBCOMMAND] [OPTIONS]"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux
msgid "TODO"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-V, --version>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show pkgctl version information"
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<-h, --help>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Show a help text"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SUBCOMMANDS"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl aur"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Interact with the Arch User Repository"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl auth"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Authenticate with services like GitLab"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl build"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Build packages inside a clean chroot"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl db"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Pacman database modification for package update, move etc"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl diff"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Compare package files using different modes"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl release"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Release step to commit, tag and upload build artifacts"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl repo"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Manage Git packaging repositories and their configuration"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl search"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Search for an expression across the GitLab packaging group"
msgstr ""

#. type: Plain text
#: archlinux
msgid "pkgctl version"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Check and manage package versions against upstream"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"B<pkgctl-aur>(1) B<pkgctl-auth>(1) B<pkgctl-build>(1) B<pkgctl-db>(1) "
"B<pkgctl-diff>(1) B<pkgctl-release>(1) B<pkgctl-repo>(1) B<pkgctl-search>(1) "
"B<pkgctl-version>(1)"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "HOMEPAGE"
msgstr ""

#. type: Plain text
#: archlinux
msgid "I<https://gitlab\\&.archlinux\\&.org/archlinux/devtools>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Please report bugs and feature requests in the issue tracker\\&. Please do "
"your best to provide a reproducible test case for bugs\\&."
msgstr ""
