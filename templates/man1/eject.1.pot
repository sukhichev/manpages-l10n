# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-27 16:55+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EJECT"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "eject - eject removable media"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<eject> [options] I<device>|I<mountpoint>"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<eject> allows removable media (typically a CD-ROM, floppy disk, tape, JAZ, "
"ZIP or USB disk) to be ejected under software control. The command can also "
"control some multi-disc CD-ROM changers, the auto-eject feature supported by "
"some devices, and close the disc tray of some CD-ROM drives."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The device corresponding to I<device> or I<mountpoint> is ejected. If no "
"name is specified, the default name B</dev/cdrom> is used. The device may be "
"addressed by device name (e.g., \\(aqsda\\(aq), device path (e.g., \\(aq/dev/"
"sda\\(aq), UUID=I<uuid> or LABEL=I<label> tags."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"There are four different methods of ejecting, depending on whether the "
"device is a CD-ROM, SCSI device, removable floppy, or tape. By default "
"B<eject> tries all four methods in order until it succeeds."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "If a device partition is specified, the whole-disk device is used."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If the device or a device partition is currently mounted, it is unmounted "
"before ejecting. The eject is processed on exclusive open block device file "
"descriptor if B<--no-unmount> or B<--force> are not specified."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-a>, B<--auto on>|B<off>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option controls the auto-eject mode, supported by some devices. When "
"enabled, the drive automatically ejects when the device is closed."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-c>, B<--changerslot> I<slot>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"With this option a CD slot can be selected from an ATAPI/IDE CD-ROM changer. "
"The CD-ROM drive cannot be in use (mounted data CD or playing a music CD) "
"for a change request to work. Please also note that the first slot of the "
"changer is referred to as 0, not 1."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-d>, B<--default>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "List the default device name."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-F>, B<--force>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Force eject, don\\(cqt check device type, don\\(cqt open device with "
"exclusive lock. The successful result may be false positive on non hot-"
"pluggable devices."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-f>, B<--floppy>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option specifies that the drive should be ejected using a removable "
"floppy disk eject command."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-i>, B<--manualeject on>|B<off>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option controls locking of the hardware eject button. When enabled, the "
"drive will not be ejected when the button is pressed. This is useful when "
"you are carrying a laptop in a bag or case and don\\(cqt want it to eject if "
"the button is inadvertently pressed."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-M>, B<--no-partitions-unmount>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The option tells B<eject> to not try to unmount other partitions on "
"partitioned devices. If another partition is still mounted, the program will "
"not attempt to eject the media. It will attempt to unmount only the device "
"or mountpoint given on the command line."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-m>, B<--no-unmount>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"The option tells B<eject> to not try to unmount at all. If this option is "
"not specified then B<eject> opens the device with B<O_EXCL> flag to be sure "
"that the device is not used (since v2.35)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-n>, B<--noop>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"With this option the selected device is displayed but no action is performed."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-p>, B<--proc>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option allows you to use I</proc/mounts> instead I</etc/mtab>. It also "
"passes the B<-n> option to B<umount>(8)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-q>, B<--tape>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option specifies that the drive should be ejected using a tape drive "
"offline command."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-r>, B<--cdrom>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option specifies that the drive should be ejected using a CDROM eject "
"command."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-s>, B<--scsi>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"This option specifies that the drive should be ejected using SCSI commands."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-T>, B<--traytoggle>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"With this option the drive is given a CD-ROM tray close command if it\\(cqs "
"opened, and a CD-ROM tray eject command if it\\(cqs closed. Not all devices "
"support this command, because it uses the above CD-ROM tray close command."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-t>, B<--trayclose>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"With this option the drive is given a CD-ROM tray close command. Not all "
"devices support this command."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "Print version and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Run in verbose mode; more information is displayed about what the command is "
"doing."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-X>, B<--listspeed>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"With this option the CD-ROM drive will be probed to detect the available "
"speeds. The output is a list of speeds which can be used as an argument of "
"the B<-x> option. This only works with Linux 2.6.13 or higher, on previous "
"versions solely the maximum speed will be reported. Also note that some "
"drives may not correctly report the speed and therefore this option does not "
"work with them."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<-x>, B<--cdspeed> I<speed>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"With this option the drive is given a CD-ROM select speed command. The "
"I<speed> argument is a number indicating the desired speed (e.g., 8 for 8X "
"speed), or 0 for maximum data rate. Not all devices support this command and "
"you can only specify speeds that the drive is capable of. Every time the "
"media is changed this option is cleared. This option can be used alone, or "
"with the B<-t> and B<-c> options."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"Returns 0 if operation was successful, 1 if operation failed or command "
"syntax was not valid."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<eject> only works with devices that support one or more of the four "
"methods of ejecting. This includes most CD-ROM drives (IDE, SCSI, and "
"proprietary), some SCSI tape drives, JAZ drives, ZIP drives (parallel port, "
"SCSI, and IDE versions), and LS120 removable floppies. Users have also "
"reported success with floppy drives on Sun SPARC and Apple Macintosh "
"systems. If B<eject> does not work, it is most likely a limitation of the "
"kernel driver for the device and not the B<eject> program itself."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<-r>, B<-s>, B<-f>, and B<-q> options allow controlling which methods "
"are used to eject. More than one method can be specified. If none of these "
"options are specified, it tries all four (this works fine in most cases)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"B<eject> may not always be able to determine if the device is mounted (e.g., "
"if it has several names). If the device name is a symbolic link, B<eject> "
"will follow the link and use the device that it points to."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If B<eject> determines that the device can have multiple partitions, it will "
"attempt to unmount all mounted partitions of the device before ejecting (see "
"also B<--no-partitions-unmount>). If an unmount fails, the program will not "
"attempt to eject the media."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"You can eject an audio CD. Some CD-ROM drives will refuse to open the tray "
"if the drive is empty. Some devices do not support the tray close command."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"If the auto-eject feature is enabled, then the drive will always be ejected "
"after running this command. Not all Linux kernel CD-ROM drivers support the "
"auto-eject mode. There is no way to find out the state of the auto-eject "
"mode."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"You need appropriate privileges to access the device files. Running as root "
"is required to eject some devices (e.g., SCSI devices)."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "- original author,"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "and"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "- util-linux version."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "B<findmnt>(8), B<lsblk>(8), B<mount>(8), B<umount>(8)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"The B<eject> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The option tells eject to not try to unmount other partitions on partitioned "
"devices. If another partition is still mounted, the program will not attempt "
"to eject the media. It will attempt to unmount only the device or mountpoint "
"given on the command line."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid ""
"The option tells eject to not try to unmount at all. If this option is not "
"specified than B<eject> opens the device with B<O_EXCL> flag to be sure that "
"the device is not used (since v2.35)."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-6
msgid "Display version information and exit."
msgstr ""
