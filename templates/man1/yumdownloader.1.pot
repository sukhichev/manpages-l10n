# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:14+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "YUMDOWNLOADER"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Jan 22, 2023"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "4.3.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "dnf-plugins-core"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "yumdownloader - redirecting to DNF download Plugin"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Download binary or source packages."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "B<dnf download [options] E<lt>pkg-specE<gt>...>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "ARGUMENTS"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<E<lt>pkg-specE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Package specification for the package to download.  Local RPMs can be "
"specified as well. This is useful with the B<--source> option or if you want "
"to download the same RPM again."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--help-cmd>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Show this help."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--arch E<lt>archE<gt>[,E<lt>archE<gt>...]>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Limit the query to packages of given architectures (default is all "
"compatible architectures with your system). To download packages with arch "
"incompatible with your system use B<--forcearch=E<lt>archE<gt>> option to "
"change basearch."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--source>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Download the source rpm. Enables source repositories of all enabled binary "
"repositories."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--debuginfo>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Download the debuginfo rpm. Enables debuginfo repositories of all enabled "
"binary repositories."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--downloaddir>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Download directory, default is the current directory (the directory must "
"exist)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--url>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Instead of downloading, print list of urls where the rpms can be downloaded."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--urlprotocol>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Limit the protocol of the urls output by the –url option. Options are http, "
"https, rsync, ftp."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--resolve>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Resolves dependencies of specified packages and downloads missing "
"dependencies in the system."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<--alldeps>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"When used with B<--resolve>, download all dependencies (do not skip already "
"installed ones)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<dnf download dnf>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Download the latest dnf package to the current directory."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<dnf download --url dnf>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Just print the remote location url where the dnf rpm can be downloaded from."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<dnf download --url --urlprotocols=https --urlprotocols=rsync dnf>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Same as above, but limit urls to https or rsync urls."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<dnf download dnf --destdir /tmp/dnl>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Download the latest dnf package to the /tmp/dnl directory (the directory "
"must exist)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<dnf download dnf --source>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Download the latest dnf source package to the current directory."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<dnf download rpm --debuginfo>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "Download the latest rpm-debuginfo package to the current directory."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "B<dnf download btanks --resolve>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid ""
"Download the latest btanks package and the uninstalled dependencies to the "
"current directory."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: fedora-40 fedora-rawhide
#, no-wrap
msgid "Feb 08, 2024"
msgstr ""

#. type: TH
#: fedora-40 fedora-rawhide
#, no-wrap
msgid "4.5.0"
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide
msgid ""
"Limit the protocol of the urls output by the --url option. Options are http, "
"https, rsync, ftp."
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: fedora-40 fedora-rawhide
msgid "2024, Red Hat, Licensed under GPLv2+"
msgstr ""
