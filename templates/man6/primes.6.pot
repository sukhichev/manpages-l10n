# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-02-15 18:06+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "February 2, 2018"
msgstr ""

#. type: Dt
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "PRIMES 6"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "E<.Nm primes>"
msgstr ""

#. type: Nd
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "generate primes"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm primes> E<.Op Fl dh> E<.Op Ar start Op Ar stop>"
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"The E<.Nm> utility prints primes in ascending order, one per line, starting "
"at or above E<.Ar start> and continuing until, but not including E<.Ar "
"stop>.  The E<.Ar start> value must be at least 0 and not greater than E<.Ar "
"stop>.  The E<.Ar stop> value must not be greater than 3825123056546413050.  "
"The default value of E<.Ar stop> is 3825123056546413050."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"When the E<.Nm> utility is invoked with no arguments, E<.Ar start> is read "
"from standard input and E<.Ar stop> is taken to be 3825123056546413050.  The "
"E<.Ar start> value may be preceded by a single E<.Sq \\&+>.  The E<.Ar "
"start> value is terminated by a non-digit character (such as a newline).  "
"The input line must not be longer than 255 characters."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"When given the E<.Fl d> argument, E<.Nm> prints the difference between the "
"current and the previous prime."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"When given the E<.Fl h> argument, E<.Nm> prints the prime numbers in "
"hexadecimal."
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DIAGNOSTICS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Out of range or invalid input results in an appropriate error message to "
"standard error."
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.An -nosplit> Originally by E<.An Landon Curt Noll>, extended to some 64-"
"bit primes by E<.An Colin Percival>."
msgstr ""

#. type: Sh
#: debian-bookworm debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "This E<.Nm> program won't get you a world record."
msgstr ""

#. type: Dd
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
#, no-wrap
msgid "February 8, 2004"
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "E<.Nm primes> E<.Op Ar start Op Ar stop>"
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The E<.Nm> utility prints primes in ascending order, one per line, starting "
"at or above E<.Ar start> and continuing until, but not including E<.Ar "
"stop>.  The E<.Ar start> value must be at least 0 and not greater than E<.Ar "
"stop>.  The E<.Ar stop> value must not be greater than 4294967295.  The "
"default value of E<.Ar stop> is 4294967295."
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When the E<.Nm> utility is invoked with no arguments, E<.Ar start> is read "
"from standard input.  E<.Ar stop> is taken to be 4294967295.  The E<.Ar "
"start> value may be preceded by a single E<.Sq \\&+>.  The E<.Ar start> "
"value is terminated by a non-digit character (such as a newline).  The input "
"line must not be longer than 255 characters."
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid ""
"Out of range or invalid input results in an appropriate error message being "
"written to standard error."
msgstr ""

#. type: Plain text
#: fedora-40 fedora-rawhide mageia-cauldron opensuse-leap-15-6
#: opensuse-tumbleweed
msgid "E<.Nm> won't get you a world record."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"The E<.Nm> utility prints primes in ascending order, one per line, starting "
"at or above E<.Ar start> and continuing until, but not including E<.Ar "
"stop>.  The E<.Ar start> value must be at least 0 and not greater than E<.Ar "
"stop>.  The E<.Ar stop> value must not be greater than the maximum possible "
"value of unsigned integer types on your system (4294967295 for 32-bit "
"systems and 18446744073709551615 for 64-bit systems).  The default value of "
"E<.Ar stop> is 4294967295 on 32-bit and 18446744073709551615 on 64-bit."
msgstr ""

#. type: Plain text
#: mageia-cauldron
msgid ""
"When the E<.Nm> utility is invoked with no arguments, E<.Ar start> is read "
"from standard input.  E<.Ar stop> is taken to be 4294967295 on 32-bit and "
"18446744073709551615 on 64-bit.  The E<.Ar start> value may be preceded by a "
"single E<.Sq \\&+>.  The E<.Ar start> value is terminated by a non-digit "
"character (such as a newline).  The input line must not be longer than 255 "
"characters."
msgstr ""
