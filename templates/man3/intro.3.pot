# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 16:58+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "intro - introduction to library functions"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Section 3 of the manual describes all library functions excluding the "
"library functions (system call wrappers)  described in Section 2, which "
"implement system calls."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Many of the functions described in the section are part of the Standard C "
"Library (I<libc>).  Some functions are part of other libraries (e.g., the "
"math library, I<libm>, or the real-time library, I<librt>)  in which case "
"the manual page will indicate the linker option needed to link against the "
"required library (e.g., I<-lm> and I<-lrt>, respectively, for the "
"aforementioned libraries)."
msgstr ""

#
#.  There
#.  are various function groups which can be identified by a letter which
#.  is appended to the chapter number:
#.  .IP (3C)
#.  These functions,
#.  the functions from chapter 2 and from chapter 3S are
#.  contained in the C standard library libc,
#.  which will be used by
#.  .BR cc (1)
#.  by default.
#.  .IP (3S)
#.  These functions are parts of the
#.  .BR stdio (3)
#.  library.  They are contained in the standard C library libc.
#.  .IP (3M)
#.  These functions are contained in the arithmetic library libm.  They are
#.  used by the
#.  .BR f77 (1)
#.  FORTRAN compiler by default,
#.  but not by the
#.  .BR cc (1)
#.  C compiler,
#.  which needs the option \fI\-lm\fP.
#.  .IP (3F)
#.  These functions are part of the FORTRAN library libF77.  There are no
#.  special compiler flags needed to use these functions.
#.  .IP (3X)
#.  Various special libraries.  The manual pages documenting their functions
#.  specify the library names.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a function from the header file specified in the "
"man page SYNOPSIS section.  (Where required, these I<feature test macros> "
"must be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Subsections"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Section 3 of this manual is organized into subsections that reflect the "
"complex structure of the standard C library and its many implementations:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "3const"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "3head"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "3type"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This difficult history frequently makes it a poor example to follow in "
"design, implementation, and presentation."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Ideally, a library for the C language is designed such that each header file "
"presents the interface to a coherent software module.  It provides a small "
"number of function declarations and exposes only data types and constants "
"that are required for use of those functions.  Together, these are termed an "
"API or I<application program interface>.  Types and constants to be shared "
"among multiple APIs should be placed in header files that declare no "
"functions.  This organization permits a C library module to be documented "
"concisely with one header file per manual page.  Such an approach improves "
"the readability and accessibility of library documentation, and thereby the "
"usability of the software."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Certain terms and abbreviations are used to indicate UNIX variants and "
"standards to which calls in this section conform.  See B<standards>(7)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Authors and copyright conditions"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Look at the header of the manual page source for the author(s) and copyright "
"conditions.  Note that these can be different from page to page!"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
