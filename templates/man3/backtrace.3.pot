# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 16:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "backtrace"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"backtrace, backtrace_symbols, backtrace_symbols_fd - support for application "
"self-debugging"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>execinfo.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<int backtrace(void *>I<buffer>B<[.>I<size>B<], int >I<size>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char **backtrace_symbols(void *const >I<buffer>B<[.>I<size>B<], int >I<size>B<);>\n"
"B<void backtrace_symbols_fd(void *const >I<buffer>B<[.>I<size>B<], int >I<size>B<, int >I<fd>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<backtrace>()  returns a backtrace for the calling program, in the array "
"pointed to by I<buffer>.  A backtrace is the series of currently active "
"function calls for the program.  Each item in the array pointed to by "
"I<buffer> is of type I<void\\ *>, and is the return address from the "
"corresponding stack frame.  The I<size> argument specifies the maximum "
"number of addresses that can be stored in I<buffer>.  If the backtrace is "
"larger than I<size>, then the addresses corresponding to the I<size> most "
"recent function calls are returned; to obtain the complete backtrace, make "
"sure that I<buffer> and I<size> are large enough."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Given the set of addresses returned by B<backtrace>()  in I<buffer>, "
"B<backtrace_symbols>()  translates the addresses into an array of strings "
"that describe the addresses symbolically.  The I<size> argument specifies "
"the number of addresses in I<buffer>.  The symbolic representation of each "
"address consists of the function name (if this can be determined), a "
"hexadecimal offset into the function, and the actual return address (in "
"hexadecimal).  The address of the array of string pointers is returned as "
"the function result of B<backtrace_symbols>().  This array is B<malloc>(3)ed "
"by B<backtrace_symbols>(), and must be freed by the caller.  (The strings "
"pointed to by the array of pointers need not and should not be freed.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<backtrace_symbols_fd>()  takes the same I<buffer> and I<size> arguments as "
"B<backtrace_symbols>(), but instead of returning an array of strings to the "
"caller, it writes the strings, one per line, to the file descriptor I<fd>.  "
"B<backtrace_symbols_fd>()  does not call B<malloc>(3), and so can be "
"employed in situations where the latter function might fail, but see NOTES."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<backtrace>()  returns the number of addresses returned in I<buffer>, which "
"is not greater than I<size>.  If the return value is less than I<size>, then "
"the full backtrace was stored; if it is equal to I<size>, then it may have "
"been truncated, in which case the addresses of the oldest stack frames are "
"not returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, B<backtrace_symbols>()  returns a pointer to the array "
"B<malloc>(3)ed by the call; on error, NULL is returned."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<backtrace>(),\n"
"B<backtrace_symbols>(),\n"
"B<backtrace_symbols_fd>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "GNU."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.1."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions make some assumptions about how a function's return address "
"is stored on the stack.  Note the following:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Omission of the frame pointers (as implied by any of B<gcc>(1)'s nonzero "
"optimization levels) may cause these assumptions to be violated."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Inlined functions do not have stack frames."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Tail-call optimization causes one stack frame to replace another."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<backtrace>()  and B<backtrace_symbols_fd>()  don't call B<malloc>()  "
"explicitly, but they are part of I<libgcc>, which gets loaded dynamically "
"when first used.  Dynamic loading usually triggers a call to B<malloc>(3).  "
"If you need certain calls to these two functions to not allocate memory (in "
"signal handlers, for example), you need to make sure I<libgcc> is loaded "
"beforehand."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The symbol names may be unavailable without the use of special linker "
"options.  For systems using the GNU linker, it is necessary to use the I<-"
"rdynamic> linker option.  Note that names of \"static\" functions are not "
"exposed, and won't be available in the backtrace."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<backtrace>()  and "
"B<backtrace_symbols>().  The following shell session shows what we might see "
"when running the program:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< cc -rdynamic prog.c -o prog>\n"
"$B< ./prog 3>\n"
"backtrace() returned 8 addresses\n"
"\\&./prog(myfunc3+0x5c) [0x80487f0]\n"
"\\&./prog [0x8048871]\n"
"\\&./prog(myfunc+0x21) [0x8048894]\n"
"\\&./prog(myfunc+0x1a) [0x804888d]\n"
"\\&./prog(myfunc+0x1a) [0x804888d]\n"
"\\&./prog(main+0x65) [0x80488fb]\n"
"\\&/lib/libc.so.6(__libc_start_main+0xdc) [0xb7e38f9c]\n"
"\\&./prog [0x8048711]\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>execinfo.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"\\&\n"
"#define BT_BUF_SIZE 100\n"
"\\&\n"
"void\n"
"myfunc3(void)\n"
"{\n"
"    int nptrs;\n"
"    void *buffer[BT_BUF_SIZE];\n"
"    char **strings;\n"
"\\&\n"
"    nptrs = backtrace(buffer, BT_BUF_SIZE);\n"
"    printf(\"backtrace() returned %d addresses\\en\", nptrs);\n"
"\\&\n"
"    /* The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)\n"
"       would produce similar output to the following: */\n"
"\\&\n"
"    strings = backtrace_symbols(buffer, nptrs);\n"
"    if (strings == NULL) {\n"
"        perror(\"backtrace_symbols\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    for (size_t j = 0; j E<lt> nptrs; j++)\n"
"        printf(\"%s\\en\", strings[j]);\n"
"\\&\n"
"    free(strings);\n"
"}\n"
"\\&\n"
"static void   /* \"static\" means don\\[aq]t export the symbol... */\n"
"myfunc2(void)\n"
"{\n"
"    myfunc3();\n"
"}\n"
"\\&\n"
"void\n"
"myfunc(int ncalls)\n"
"{\n"
"    if (ncalls E<gt> 1)\n"
"        myfunc(ncalls - 1);\n"
"    else\n"
"        myfunc2();\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"%s num-calls\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    myfunc(atoi(argv[1]));\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<addr2line>(1), B<gcc>(1), B<gdb>(1), B<ld>(1), B<dlopen>(3), B<malloc>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<backtrace>(), B<backtrace_symbols>(), and B<backtrace_symbols_fd>()  are "
"provided since glibc 2.1."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "These functions are GNU extensions."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>execinfo.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "#define BT_BUF_SIZE 100\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"void\n"
"myfunc3(void)\n"
"{\n"
"    int nptrs;\n"
"    void *buffer[BT_BUF_SIZE];\n"
"    char **strings;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    nptrs = backtrace(buffer, BT_BUF_SIZE);\n"
"    printf(\"backtrace() returned %d addresses\\en\", nptrs);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    /* The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)\n"
"       would produce similar output to the following: */\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    strings = backtrace_symbols(buffer, nptrs);\n"
"    if (strings == NULL) {\n"
"        perror(\"backtrace_symbols\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (size_t j = 0; j E<lt> nptrs; j++)\n"
"        printf(\"%s\\en\", strings[j]);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    free(strings);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void   /* \"static\" means don\\[aq]t export the symbol... */\n"
"myfunc2(void)\n"
"{\n"
"    myfunc3();\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"void\n"
"myfunc(int ncalls)\n"
"{\n"
"    if (ncalls E<gt> 1)\n"
"        myfunc(ncalls - 1);\n"
"    else\n"
"        myfunc2();\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc != 2) {\n"
"        fprintf(stderr, \"%s num-calls\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    myfunc(atoi(argv[1]));\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
