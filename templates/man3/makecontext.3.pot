# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:00+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "makecontext"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "makecontext, swapcontext - manipulate user context"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>ucontext.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void makecontext(ucontext_t *>I<ucp>B<, void (*>I<func>B<)(), int >I<argc>B<, ...);>\n"
"B<int swapcontext(ucontext_t *restrict >I<oucp>B<,>\n"
"B<                const ucontext_t *restrict >I<ucp>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In a System V-like environment, one has the type I<ucontext_t> (defined in "
"I<E<lt>ucontext.hE<gt>> and described in B<getcontext>(3))  and the four "
"functions B<getcontext>(3), B<setcontext>(3), B<makecontext>(), and "
"B<swapcontext>()  that allow user-level context switching between multiple "
"threads of control within a process."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<makecontext>()  function modifies the context pointed to by I<ucp> "
"(which was obtained from a call to B<getcontext>(3)).  Before invoking "
"B<makecontext>(), the caller must allocate a new stack for this context and "
"assign its address to I<ucp-E<gt>uc_stack>, and define a successor context "
"and assign its address to I<ucp-E<gt>uc_link>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When this context is later activated (using B<setcontext>(3)  or "
"B<swapcontext>())  the function I<func> is called, and passed the series of "
"integer (I<int>)  arguments that follow I<argc>; the caller must specify the "
"number of these arguments in I<argc>.  When this function returns, the "
"successor context is activated.  If the successor context pointer is NULL, "
"the thread exits."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<swapcontext>()  function saves the current context in the structure "
"pointed to by I<oucp>, and then activates the context pointed to by I<ucp>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"When successful, B<swapcontext>()  does not return.  (But we may return "
"later, in case I<oucp> is activated, in which case it looks like "
"B<swapcontext>()  returns 0.)  On error, B<swapcontext>()  returns -1 and "
"sets I<errno> to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Insufficient stack space left."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<makecontext>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:ucp"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<swapcontext>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:oucp race:ucp"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"glibc 2.1.  SUSv2, POSIX.1-2001.  Removed in POSIX.1-2008, citing "
"portability issues, and recommending that applications be rewritten to use "
"POSIX threads instead."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The interpretation of I<ucp-E<gt>uc_stack> is just as in B<sigaltstack>(2), "
"namely, this struct contains the start and length of a memory area to be "
"used as the stack, regardless of the direction of growth of the stack.  "
"Thus, it is not necessary for the user program to worry about this direction."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On architectures where I<int> and pointer types are the same size (e.g., "
"x86-32, where both types are 32 bits), you may be able to get away with "
"passing pointers as arguments to B<makecontext>()  following I<argc>.  "
"However, doing this is not guaranteed to be portable, is undefined according "
"to the standards, and won't work on architectures where pointers are larger "
"than I<int>s.  Nevertheless, starting with glibc 2.8, glibc makes some "
"changes to B<makecontext>(), to permit this on some 64-bit architectures (e."
"g., x86-64)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The example program below demonstrates the use of B<getcontext>(3), "
"B<makecontext>(), and B<swapcontext>().  Running the program produces the "
"following output:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out>\n"
"main: swapcontext(&uctx_main, &uctx_func2)\n"
"func2: started\n"
"func2: swapcontext(&uctx_func2, &uctx_func1)\n"
"func1: started\n"
"func1: swapcontext(&uctx_func1, &uctx_func2)\n"
"func2: returning\n"
"func1: returning\n"
"main: exiting\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"
"\\&\n"
"static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"
"\\&\n"
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
"\\&\n"
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
"\\&\n"
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"
"\\&\n"
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"
"\\&\n"
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Successor context is f1(), unless argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"
"\\&\n"
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"\\&\n"
"    printf(\"%s: exiting\\en\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<sigaction>(2), B<sigaltstack>(2), B<sigprocmask>(2), B<getcontext>(3), "
"B<sigsetjmp>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<makecontext>()  and B<swapcontext>()  are provided since glibc 2.1."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"SUSv2, POSIX.1-2001.  POSIX.1-2008 removes the specifications of "
"B<makecontext>()  and B<swapcontext>(), citing portability issues, and "
"recommending that applications be rewritten to use POSIX threads instead."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>ucontext.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "static ucontext_t uctx_main, uctx_func1, uctx_func2;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#define handle_error(msg) \\e\n"
"    do { perror(msg); exit(EXIT_FAILURE); } while (0)\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void\n"
"func1(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func1, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func1, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void\n"
"func2(void)\n"
"{\n"
"    printf(\"%s: started\\en\", __func__);\n"
"    printf(\"%s: swapcontext(&uctx_func2, &uctx_func1)\\en\", __func__);\n"
"    if (swapcontext(&uctx_func2, &uctx_func1) == -1)\n"
"        handle_error(\"swapcontext\");\n"
"    printf(\"%s: returning\\en\", __func__);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    char func1_stack[16384];\n"
"    char func2_stack[16384];\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (getcontext(&uctx_func1) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func1.uc_stack.ss_sp = func1_stack;\n"
"    uctx_func1.uc_stack.ss_size = sizeof(func1_stack);\n"
"    uctx_func1.uc_link = &uctx_main;\n"
"    makecontext(&uctx_func1, func1, 0);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (getcontext(&uctx_func2) == -1)\n"
"        handle_error(\"getcontext\");\n"
"    uctx_func2.uc_stack.ss_sp = func2_stack;\n"
"    uctx_func2.uc_stack.ss_size = sizeof(func2_stack);\n"
"    /* Successor context is f1(), unless argc E<gt> 1 */\n"
"    uctx_func2.uc_link = (argc E<gt> 1) ? NULL : &uctx_func1;\n"
"    makecontext(&uctx_func2, func2, 0);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"%s: swapcontext(&uctx_main, &uctx_func2)\\en\", __func__);\n"
"    if (swapcontext(&uctx_main, &uctx_func2) == -1)\n"
"        handle_error(\"swapcontext\");\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"%s: exiting\\en\", __func__);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
