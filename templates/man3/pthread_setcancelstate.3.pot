# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:05+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "pthread_setcancelstate"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"pthread_setcancelstate, pthread_setcanceltype - set cancelability state and "
"type"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_setcancelstate(int >I<state>B<, int *>I<oldstate>B<);>\n"
"B<int pthread_setcanceltype(int >I<type>B<, int *>I<oldtype>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pthread_setcancelstate>()  sets the cancelability state of the calling "
"thread to the value given in I<state>.  The previous cancelability state of "
"the thread is returned in the buffer pointed to by I<oldstate>.  The "
"I<state> argument must have one of the following values:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_ENABLE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The thread is cancelable.  This is the default cancelability state in all "
"new threads, including the initial thread.  The thread's cancelability type "
"determines when a cancelable thread will respond to a cancelation request."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_DISABLE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The thread is not cancelable.  If a cancelation request is received, it is "
"blocked until cancelability is enabled."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pthread_setcanceltype>()  sets the cancelability type of the calling "
"thread to the value given in I<type>.  The previous cancelability type of "
"the thread is returned in the buffer pointed to by I<oldtype>.  The I<type> "
"argument must have one of the following values:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_DEFERRED>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A cancelation request is deferred until the thread next calls a function "
"that is a cancelation point (see B<pthreads>(7)).  This is the default "
"cancelability type in all new threads, including the initial thread."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Even with deferred cancelation, a cancelation point in an asynchronous "
"signal handler may still be acted upon and the effect is as if it was an "
"asynchronous cancelation."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<PTHREAD_CANCEL_ASYNCHRONOUS>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The thread can be canceled at any time.  (Typically, it will be canceled "
"immediately upon receiving a cancelation request, but the system doesn't "
"guarantee this.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The set-and-get operation performed by each of these functions is atomic "
"with respect to other threads in the process calling the same function."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return 0; on error, they return a nonzero error "
"number."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<pthread_setcancelstate>()  can fail with the following error:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Invalid value for I<state>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The B<pthread_setcanceltype>()  can fail with the following error:"
msgstr ""

#. #-#-#-#-#  archlinux: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH VERSIONS
#.  Available since glibc 2.0
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-40: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pthread_setcancelstate.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Invalid value for I<type>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_setcancelstate>(),\n"
"B<pthread_setcanceltype>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Async-cancel safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "AC-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.0 POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For details of what happens when a thread is canceled, see B<\\"
"%pthread_cancel>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Briefly disabling cancelability is useful if a thread performs some critical "
"action that must not be interrupted by a cancelation request.  Beware of "
"disabling cancelability for long periods, or around operations that may "
"block for long periods, since that will render the thread unresponsive to "
"cancelation requests."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Asynchronous cancelability"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Setting the cancelability type to B<PTHREAD_CANCEL_ASYNCHRONOUS> is rarely "
"useful.  Since the thread could be canceled at I<any> time, it cannot safely "
"reserve resources (e.g., allocating memory with B<malloc>(3)), acquire "
"mutexes, semaphores, or locks, and so on.  Reserving resources is unsafe "
"because the application has no way of knowing what the state of these "
"resources is when the thread is canceled; that is, did cancelation occur "
"before the resources were reserved, while they were reserved, or after they "
"were released? Furthermore, some internal data structures (e.g., the linked "
"list of free blocks managed by the B<malloc>(3)  family of functions) may be "
"left in an inconsistent state if cancelation occurs in the middle of the "
"function call.  Consequently, clean-up handlers cease to be useful."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Functions that can be safely asynchronously canceled are called I<async-"
"cancel-safe functions>.  POSIX.1-2001 and POSIX.1-2008 require only that "
"B<pthread_cancel>(3), B<pthread_setcancelstate>(), and "
"B<pthread_setcanceltype>()  be async-cancel-safe.  In general, other library "
"functions can't be safely called from an asynchronously cancelable thread."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"One of the few circumstances in which asynchronous cancelability is useful "
"is for cancelation of a thread that is in a pure compute-bound loop."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Portability notes"
msgstr ""

#.  It looks like at least Solaris, FreeBSD and Tru64 support this.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The Linux threading implementations permit the I<oldstate> argument of "
"B<pthread_setcancelstate>()  to be NULL, in which case the information about "
"the previous cancelability state is not returned to the caller.  Many other "
"implementations also permit a NULL I<oldstat> argument, but POSIX.1 does not "
"specify this point, so portable applications should always specify a non-"
"NULL value in I<oldstate>.  A precisely analogous set of statements applies "
"for the I<oldtype> argument of B<pthread_setcanceltype>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<pthread_cancel>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<pthread_cancel>(3), B<pthread_cleanup_push>(3), B<pthread_testcancel>(3), "
"B<pthreads>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
msgid ""
"For details of what happens when a thread is canceled, see "
"B<pthread_cancel>(3)."
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-30"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
