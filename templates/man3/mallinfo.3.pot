# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:01+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "mallinfo"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "mallinfo, mallinfo2 - obtain memory allocation information"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>malloc.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct mallinfo mallinfo(void);>\n"
"B<struct mallinfo2 mallinfo2(void);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"These functions return a copy of a structure containing information about "
"memory allocations performed by B<malloc>(3)  and related functions.  The "
"structure returned by each function contains the same fields.  However, the "
"older function, B<mallinfo>(), is deprecated since the type used for the "
"fields is too small (see BUGS)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Note that not all allocations are visible to these functions; see BUGS and "
"consider using B<malloc_info>(3)  instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<mallinfo2> structure returned by B<mallinfo2>()  is defined as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct mallinfo2 {\n"
"    size_t arena;     /* Non-mmapped space allocated (bytes) */\n"
"    size_t ordblks;   /* Number of free chunks */\n"
"    size_t smblks;    /* Number of free fastbin blocks */\n"
"    size_t hblks;     /* Number of mmapped regions */\n"
"    size_t hblkhd;    /* Space allocated in mmapped regions\n"
"                         (bytes) */\n"
"    size_t usmblks;   /* See below */\n"
"    size_t fsmblks;   /* Space in freed fastbin blocks (bytes) */\n"
"    size_t uordblks;  /* Total allocated space (bytes) */\n"
"    size_t fordblks;  /* Total free space (bytes) */\n"
"    size_t keepcost;  /* Top-most, releasable space (bytes) */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The I<mallinfo> structure returned by the deprecated B<mallinfo>()  function "
"is exactly the same, except that the fields are typed as I<int>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The structure fields contain the following information:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<arena>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The total amount of memory allocated by means other than B<mmap>(2)  (i.e., "
"memory allocated on the heap).  This figure includes both in-use blocks and "
"blocks on the free list."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<ordblks>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The number of ordinary (i.e., non-fastbin) free blocks."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<smblks>"
msgstr ""

#.  the glibc info page wrongly says this field is unused
#.  https://sourceware.org/bugzilla/show_bug.cgi?id=26746
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The number of fastbin free blocks (see B<mallopt>(3))."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<hblks>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The number of blocks currently allocated using B<mmap>(2).  (See the "
"discussion of B<M_MMAP_THRESHOLD> in B<mallopt>(3).)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<hblkhd>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The number of bytes in blocks currently allocated using B<mmap>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<usmblks>"
msgstr ""

#.  It seems to have been zero since at least as far back as glibc 2.15
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"This field is unused, and is always 0.  Historically, it was the \"highwater "
"mark\" for allocated space\\[em]that is, the maximum amount of space that "
"was ever allocated (in bytes); this field was maintained only in "
"nonthreading environments."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<fsmblks>"
msgstr ""

#.  the glibc info page wrongly says this field is unused
#.  https://sourceware.org/bugzilla/show_bug.cgi?id=26746
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The total number of bytes in fastbin free blocks."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<uordblks>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The total number of bytes used by in-use allocations."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<fordblks>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The total number of bytes in free blocks."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "I<keepcost>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The total amount of releasable free space at the top of the heap.  This is "
"the maximum number of bytes that could ideally (i.e., ignoring page "
"alignment restrictions, and so on) be released by B<malloc_trim>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mallinfo>(),\n"
"B<mallinfo2>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe init const:mallopt"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mallinfo>()/ B<mallinfo2>()  would access some global internal objects.  "
"If modify them with non-atomically, may get inconsistent results.  The "
"identifier I<mallopt> in I<const:mallopt> mean that B<mallopt>()  would "
"modify the global internal objects with atomics, that make sure "
"B<mallinfo>()/ B<mallinfo2>()  is safe enough, others modify with non-"
"atomically maybe not."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mallinfo>()"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.0.  SVID."
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<mallinfo2>()"
msgstr ""

#.  commit e3960d1c57e57f33e0e846d615788f4ede73b945
#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.33."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#.  FIXME . http://sourceware.org/bugzilla/show_bug.cgi?id=208
#.  See the 24 Aug 2011 mail by Paul Pluzhnikov:
#.      "[patch] Fix mallinfo() to accumulate results for all arenas"
#.  on libc-alpha@sourceware.org
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<Information is returned for only the main memory allocation area.> "
"Allocations in other arenas are excluded.  See B<malloc_stats>(3)  and "
"B<malloc_info>(3)  for alternatives that include information about other "
"arenas."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The fields of the I<mallinfo> structure that is returned by the older "
"B<mallinfo>()  function are typed as I<int>.  However, because some internal "
"bookkeeping values may be of type I<long>, the reported values may wrap "
"around zero and thus be inaccurate."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The program below employs B<mallinfo2>()  to retrieve memory allocation "
"statistics before and after allocating and freeing some blocks of memory.  "
"The statistics are displayed on standard output."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The first two command-line arguments specify the number and size of blocks "
"to be allocated with B<malloc>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The remaining three arguments specify which of the allocated blocks should "
"be freed with B<free>(3).  These three arguments are optional, and specify "
"(in order): the step size to be used in the loop that frees blocks (the "
"default is 1, meaning free all blocks in the range); the ordinal position of "
"the first block to be freed (default 0, meaning the first allocated block); "
"and a number one greater than the ordinal position of the last block to be "
"freed (default is one greater than the maximum block number).  If these "
"three arguments are omitted, then the defaults cause all allocated blocks to "
"be freed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"In the following example run of the program, 1000 allocations of 100 bytes "
"are performed, and then every second allocated block is freed:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<./a.out 1000 100 2>\n"
"============== Before allocating blocks ==============\n"
"Total non-mmapped bytes (arena):       0\n"
"# of free chunks (ordblks):            1\n"
"# of free fastbin blocks (smblks):     0\n"
"# of mapped regions (hblks):           0\n"
"Bytes in mapped regions (hblkhd):      0\n"
"Max. total allocated space (usmblks):  0\n"
"Free bytes held in fastbins (fsmblks): 0\n"
"Total allocated space (uordblks):      0\n"
"Total free space (fordblks):           0\n"
"Topmost releasable block (keepcost):   0\n"
"\\&\n"
"============== After allocating blocks ==============\n"
"Total non-mmapped bytes (arena):       135168\n"
"# of free chunks (ordblks):            1\n"
"# of free fastbin blocks (smblks):     0\n"
"# of mapped regions (hblks):           0\n"
"Bytes in mapped regions (hblkhd):      0\n"
"Max. total allocated space (usmblks):  0\n"
"Free bytes held in fastbins (fsmblks): 0\n"
"Total allocated space (uordblks):      104000\n"
"Total free space (fordblks):           31168\n"
"Topmost releasable block (keepcost):   31168\n"
"\\&\n"
"============== After freeing blocks ==============\n"
"Total non-mmapped bytes (arena):       135168\n"
"# of free chunks (ordblks):            501\n"
"# of free fastbin blocks (smblks):     0\n"
"# of mapped regions (hblks):           0\n"
"Bytes in mapped regions (hblkhd):      0\n"
"Max. total allocated space (usmblks):  0\n"
"Free bytes held in fastbins (fsmblks): 0\n"
"Total allocated space (uordblks):      52000\n"
"Total free space (fordblks):           83168\n"
"Topmost releasable block (keepcost):   31168\n"
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
"\\&\n"
"static void\n"
"display_mallinfo2(void)\n"
"{\n"
"    struct mallinfo2 mi;\n"
"\\&\n"
"    mi = mallinfo2();\n"
"\\&\n"
"    printf(\"Total non-mmapped bytes (arena):       %zu\\en\", mi.arena);\n"
"    printf(\"# of free chunks (ordblks):            %zu\\en\", mi.ordblks);\n"
"    printf(\"# of free fastbin blocks (smblks):     %zu\\en\", mi.smblks);\n"
"    printf(\"# of mapped regions (hblks):           %zu\\en\", mi.hblks);\n"
"    printf(\"Bytes in mapped regions (hblkhd):      %zu\\en\", mi.hblkhd);\n"
"    printf(\"Max. total allocated space (usmblks):  %zu\\en\", mi.usmblks);\n"
"    printf(\"Free bytes held in fastbins (fsmblks): %zu\\en\", mi.fsmblks);\n"
"    printf(\"Total allocated space (uordblks):      %zu\\en\", mi.uordblks);\n"
"    printf(\"Total free space (fordblks):           %zu\\en\", mi.fordblks);\n"
"    printf(\"Topmost releasable block (keepcost):   %zu\\en\", mi.keepcost);\n"
"}\n"
"\\&\n"
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"#define MAX_ALLOCS 2000000\n"
"    char *alloc[MAX_ALLOCS];\n"
"    size_t blockSize, numBlocks, freeBegin, freeEnd, freeStep;\n"
"\\&\n"
"    if (argc E<lt> 3 || strcmp(argv[1], \"--help\") == 0) {\n"
"        fprintf(stderr, \"%s num-blocks block-size [free-step \"\n"
"                \"[start-free [end-free]]]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
"\\&\n"
"    numBlocks = atoi(argv[1]);\n"
"    blockSize = atoi(argv[2]);\n"
"    freeStep = (argc E<gt> 3) ? atoi(argv[3]) : 1;\n"
"    freeBegin = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
"    freeEnd = (argc E<gt> 5) ? atoi(argv[5]) : numBlocks;\n"
"\\&\n"
"    printf(\"============== Before allocating blocks ==============\\en\");\n"
"    display_mallinfo2();\n"
"\\&\n"
"    for (size_t j = 0; j E<lt> numBlocks; j++) {\n"
"        if (numBlocks E<gt>= MAX_ALLOCS) {\n"
"            fprintf(stderr, \"Too many allocations\\en\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"\\&\n"
"        alloc[j] = malloc(blockSize);\n"
"        if (alloc[j] == NULL) {\n"
"            perror(\"malloc\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"
"\\&\n"
"    printf(\"\\en============== After allocating blocks ==============\\en\");\n"
"    display_mallinfo2();\n"
"\\&\n"
"    for (size_t j = freeBegin; j E<lt> freeEnd; j += freeStep)\n"
"        free(alloc[j]);\n"
"\\&\n"
"    printf(\"\\en============== After freeing blocks ==============\\en\");\n"
"    display_mallinfo2();\n"
"\\&\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mmap>(2), B<malloc>(3), B<malloc_info>(3), B<malloc_stats>(3), "
"B<malloc_trim>(3), B<mallopt>(3)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#.  mallinfo(): Available already in glibc 2.0, possibly earlier
#.  commit e3960d1c57e57f33e0e846d615788f4ede73b945
#. type: Plain text
#: debian-bookworm
msgid "The B<mallinfo2>()  function was added in glibc 2.33."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"These functions are not specified by POSIX or the C standards.  A "
"B<mallinfo>()  function exists on many System V derivatives, and was "
"specified in the SVID."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"$ B<./a.out 1000 100 2>\n"
"============== Before allocating blocks ==============\n"
"Total non-mmapped bytes (arena):       0\n"
"# of free chunks (ordblks):            1\n"
"# of free fastbin blocks (smblks):     0\n"
"# of mapped regions (hblks):           0\n"
"Bytes in mapped regions (hblkhd):      0\n"
"Max. total allocated space (usmblks):  0\n"
"Free bytes held in fastbins (fsmblks): 0\n"
"Total allocated space (uordblks):      0\n"
"Total free space (fordblks):           0\n"
"Topmost releasable block (keepcost):   0\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"============== After allocating blocks ==============\n"
"Total non-mmapped bytes (arena):       135168\n"
"# of free chunks (ordblks):            1\n"
"# of free fastbin blocks (smblks):     0\n"
"# of mapped regions (hblks):           0\n"
"Bytes in mapped regions (hblkhd):      0\n"
"Max. total allocated space (usmblks):  0\n"
"Free bytes held in fastbins (fsmblks): 0\n"
"Total allocated space (uordblks):      104000\n"
"Total free space (fordblks):           31168\n"
"Topmost releasable block (keepcost):   31168\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"============== After freeing blocks ==============\n"
"Total non-mmapped bytes (arena):       135168\n"
"# of free chunks (ordblks):            501\n"
"# of free fastbin blocks (smblks):     0\n"
"# of mapped regions (hblks):           0\n"
"Bytes in mapped regions (hblkhd):      0\n"
"Max. total allocated space (usmblks):  0\n"
"Free bytes held in fastbins (fsmblks): 0\n"
"Total allocated space (uordblks):      52000\n"
"Total free space (fordblks):           83168\n"
"Topmost releasable block (keepcost):   31168\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"#include E<lt>malloc.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"static void\n"
"display_mallinfo2(void)\n"
"{\n"
"    struct mallinfo2 mi;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid "    mi = mallinfo2();\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"Total non-mmapped bytes (arena):       %zu\\en\", mi.arena);\n"
"    printf(\"# of free chunks (ordblks):            %zu\\en\", mi.ordblks);\n"
"    printf(\"# of free fastbin blocks (smblks):     %zu\\en\", mi.smblks);\n"
"    printf(\"# of mapped regions (hblks):           %zu\\en\", mi.hblks);\n"
"    printf(\"Bytes in mapped regions (hblkhd):      %zu\\en\", mi.hblkhd);\n"
"    printf(\"Max. total allocated space (usmblks):  %zu\\en\", mi.usmblks);\n"
"    printf(\"Free bytes held in fastbins (fsmblks): %zu\\en\", mi.fsmblks);\n"
"    printf(\"Total allocated space (uordblks):      %zu\\en\", mi.uordblks);\n"
"    printf(\"Total free space (fordblks):           %zu\\en\", mi.fordblks);\n"
"    printf(\"Topmost releasable block (keepcost):   %zu\\en\", mi.keepcost);\n"
"}\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"#define MAX_ALLOCS 2000000\n"
"    char *alloc[MAX_ALLOCS];\n"
"    size_t blockSize, numBlocks, freeBegin, freeEnd, freeStep;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    if (argc E<lt> 3 || strcmp(argv[1], \"--help\") == 0) {\n"
"        fprintf(stderr, \"%s num-blocks block-size [free-step \"\n"
"                \"[start-free [end-free]]]\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    numBlocks = atoi(argv[1]);\n"
"    blockSize = atoi(argv[2]);\n"
"    freeStep = (argc E<gt> 3) ? atoi(argv[3]) : 1;\n"
"    freeBegin = (argc E<gt> 4) ? atoi(argv[4]) : 0;\n"
"    freeEnd = (argc E<gt> 5) ? atoi(argv[5]) : numBlocks;\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"============== Before allocating blocks ==============\\en\");\n"
"    display_mallinfo2();\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (size_t j = 0; j E<lt> numBlocks; j++) {\n"
"        if (numBlocks E<gt>= MAX_ALLOCS) {\n"
"            fprintf(stderr, \"Too many allocations\\en\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"        alloc[j] = malloc(blockSize);\n"
"        if (alloc[j] == NULL) {\n"
"            perror(\"malloc\");\n"
"            exit(EXIT_FAILURE);\n"
"        }\n"
"    }\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"\\en============== After allocating blocks ==============\\en\");\n"
"    display_mallinfo2();\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    for (size_t j = freeBegin; j E<lt> freeEnd; j += freeStep)\n"
"        free(alloc[j]);\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    printf(\"\\en============== After freeing blocks ==============\\en\");\n"
"    display_mallinfo2();\n"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-6
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
