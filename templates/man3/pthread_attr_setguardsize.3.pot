# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:04+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "pthread_attr_setguardsize"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2023-10-31"
msgstr ""

#. type: TH
#: archlinux fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.06"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"pthread_attr_setguardsize, pthread_attr_getguardsize - set/get guard size "
"attribute in thread attributes object"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX threads library (I<libpthread>, I<-lpthread>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>pthread.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int pthread_attr_setguardsize(pthread_attr_t *>I<attr>B<, size_t >I<guardsize>B<);>\n"
"B<int pthread_attr_getguardsize(const pthread_attr_t *restrict >I<attr>B<,>\n"
"B<                              size_t *restrict >I<guardsize>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pthread_attr_setguardsize>()  function sets the guard size attribute "
"of the thread attributes object referred to by I<attr> to the value "
"specified in I<guardsize>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<guardsize> is greater than 0, then for each new thread created using "
"I<attr> the system allocates an additional region of at least I<guardsize> "
"bytes at the end of the thread's stack to act as the guard area for the "
"stack (but see BUGS)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If I<guardsize> is 0, then new threads created with I<attr> will not have a "
"guard area."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "The default guard size is the same as the system page size."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"If the stack address attribute has been set in I<attr> (using "
"B<pthread_attr_setstack>(3)  or B<pthread_attr_setstackaddr>(3)), meaning "
"that the caller is allocating the thread's stack, then the guard size "
"attribute is ignored (i.e., no guard area is created by the system): it is "
"the application's responsibility to handle stack overflow (perhaps by using "
"B<mprotect>(2)  to manually define a guard area at the end of the stack that "
"it has allocated)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The B<pthread_attr_getguardsize>()  function returns the guard size "
"attribute of the thread attributes object referred to by I<attr> in the "
"buffer pointed to by I<guardsize>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"On success, these functions return 0; on error, they return a nonzero error "
"number."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"POSIX.1 documents an B<EINVAL> error if I<attr> or I<guardsize> is invalid.  "
"On Linux these functions always succeed (but portable and future-proof "
"applications should nevertheless handle a possible error return)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".na\n"
msgstr ""

#. type: tbl table
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ".nh\n"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<pthread_attr_setguardsize>(),\n"
"B<pthread_attr_getguardsize>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-6 opensuse-tumbleweed
msgid "glibc 2.1.  POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"A guard area consists of virtual memory pages that are protected to prevent "
"read and write access.  If a thread overflows its stack into the guard area, "
"then, on most hard architectures, it receives a B<SIGSEGV> signal, thus "
"notifying it of the overflow.  Guard areas start on page boundaries, and the "
"guard size is internally rounded up to the system page size when creating a "
"thread.  (Nevertheless, B<pthread_attr_getguardsize>()  returns the guard "
"size that was set by B<pthread_attr_setguardsize>().)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Setting a guard size of 0 may be useful to save memory in an application "
"that creates many threads and knows that stack overflow can never occur."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"Choosing a guard size larger than the default size may be necessary for "
"detecting stack overflows if a thread allocates large data structures on the "
"stack."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"As at glibc 2.8, the NPTL threading implementation includes the guard area "
"within the stack size allocation, rather than allocating extra space at the "
"end of the stack, as POSIX.1 requires.  (This can result in an B<EINVAL> "
"error from B<pthread_create>(3)  if the guard size value is too large, "
"leaving no space for the actual stack.)"
msgstr ""

#
#
#. #-#-#-#-#  archlinux: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  debian-unstable: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  fedora-40: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-6: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: pthread_attr_setguardsize.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  glibc includes the guardsize within the allocated stack size,
#.  which looks pretty clearly to be in violation of POSIX.
#.  Filed bug, 22 Oct 2008:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=6973
#.  Older reports:
#.  https//bugzilla.redhat.com/show_bug.cgi?id=435337
#.  Reportedly, LinuxThreads did the right thing, allocating
#.  extra space at the end of the stack:
#.  http://sourceware.org/ml/libc-alpha/2008-05/msg00086.html
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"The obsolete LinuxThreads implementation did the right thing, allocating "
"extra space at the end of the stack for the guard area."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid "See B<pthread_getattr_np>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-6 opensuse-tumbleweed
msgid ""
"B<mmap>(2), B<mprotect>(2), B<pthread_attr_init>(3), "
"B<pthread_attr_setstack>(3), B<pthread_attr_setstacksize>(3), "
"B<pthread_create>(3), B<pthreads>(7)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "These functions are provided since glibc 2.1."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "2023-07-20"
msgstr ""

#. type: TH
#: debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.05.01"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-6
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""
