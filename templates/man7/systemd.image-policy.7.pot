# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-01 17:10+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYSTEMD\\&.IMAGE-POLICY"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 255"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "systemd.image-policy"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "systemd.image-policy - Disk Image Dissection Policy"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"In systemd, whenever a disk image (DDI) implementing the "
"\\m[blue]B<Discoverable Partitions Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2 "
"is activated, a policy may be specified controlling which partitions to "
"mount and what kind of cryptographic protection to require\\&. Such a disk "
"image dissection policy is a string that contains per-partition-type rules, "
"separated by colons (\":\")\\&. The individual rules consist of a partition "
"identifier, an equal sign (\"=\"), and one or more flags which may be set "
"per partition\\&. If multiple flags are specified per partition they are "
"separated by a plus sign (\"+\")\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The partition identifiers currently defined are: B<root>, B<usr>, B<home>, "
"B<srv>, B<esp>, B<xbootldr>, B<swap>, B<root-verity>, B<root-verity-sig>, "
"B<usr-verity>, B<usr-verity-sig>, B<tmp>, B<var>\\&. These identifiers match "
"the relevant partition types in the Discoverable Partitions Specification, "
"but are agnostic to CPU architectures\\&. If the partition identifier is "
"left empty it defines the I<default> policy for partitions defined in the "
"Discoverable Partitions Specification for which no policy flags are "
"explicitly listed in the policy string\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The following partition policy flags are defined that dictate the existence/"
"absence, the use, and the protection level of partitions:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<unprotected> for partitions that shall exist and be used, but shall come "
"without cryptographic protection, lacking both Verity authentication and "
"LUKS encryption\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"B<verity> for partitions that shall exist and be used, with Verity "
"authentication\\&. (Note: if a DDI image carries a data partition, along "
"with a Verity partition and a signature partition for it, and only the "
"B<verity> flag is set (B<signed> is not), then the image will be set up with "
"Verity, but the signature data will not be used\\&. Or in other words: any "
"DDI with a set of partitions that qualify for B<signature> also implicitly "
"qualifies for B<verity>, and in fact also B<unprotected>)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<signed> for partitions that shall exist and be used, with Verity "
"authentication, which are also accompanied by a PKCS#7 signature of the "
"Verity root hash\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<encrypted> for partitions which shall exist and be used and are encrypted "
"with LUKS\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "B<unused> for partitions that shall exist but shall not be used\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "B<absent> for partitions that shall not exist on the image\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"By setting a combination of the flags above, alternatives can be "
"declared\\&. For example the combination \"unused+absent\" means: the "
"partition may exist (in which case it shall not be used) or may be "
"absent\\&. The combination of "
"\"unprotected+verity+signed+encrypted+unused+absent\" may be specified via "
"the special shortcut \"open\", and indicates that the partition may exist or "
"may be absent, but if it exists is used, regardless of the protection "
"level\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"As special rule: if none of the flags above are set for a listed partition "
"identifier, the default policy of B<open> is implied, i\\&.e\\&. setting "
"none of these flags listed above means effectively all flags listed above "
"will be set\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The following partition policy flags are defined that dictate the state of "
"specific GPT partition flags:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<read-only-off>, B<read-only-on> to require that the partitions have the "
"read-only partition flag off or on\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<growfs-off>, B<growfs-on> to require that the partitions have the growfs "
"partition flag off or on\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"If both B<read-only-off> and B<read-only-on> are set for a partition, then "
"the state of the read-only flag on the partition is not dictated by the "
"policy\\&. Setting neither flag is equivalent to setting both, i\\&.e\\&. "
"setting neither of these two flags means effectively both will be set\\&. A "
"similar logic applies to B<growfs-off>/B<growfs-on>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"If partitions are not listed within an image policy string, the default "
"policy flags are applied (configurable via an empty partition identifier, "
"see above)\\&. If no default policy flags are configured in the policy "
"string, it is implied to be \"absent+unused\", except for the Verity "
"partition and their signature partitions where the policy is automatically "
"derived from minimal protection level of the data partition they protect, as "
"encoded in the policy\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SPECIAL POLICIES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The special image policy string \"*\" is short for \"use everything\", i\\&."
"e\\&. is equivalent to:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "=verity+signed+encrypted+unprotected+unused+absent\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The special image policy string \"-\" is short for \"use nothing\", i\\&."
"e\\&. is equivalent to:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "=unused+absent\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The special image policy string \"~\" is short for \"everything must be "
"absent\", i\\&.e\\&. is equivalent to:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "=absent\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "USE"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-40 fedora-rawhide mageia-cauldron
msgid ""
"Most systemd components that support operating with disk images support a "
"B<--image-policy=> command line option to specify the image policy to use, "
"and default to relatively open policies (typically the \"*\" policy, as "
"described above), under the assumption that trust in disk images is "
"established before the images are passed to the program in question\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"For the host image itself B<systemd-gpt-auto-generator>(8)  is responsible "
"for processing the GPT partition table and making use of the included "
"discoverable partitions\\&. It accepts an image policy via the kernel "
"command line option B<systemd\\&.image-policy=>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"Note that image policies do not dictate how the components will mount and "
"use disk images \\(em they only dictate which parts to avoid and which "
"protection level and arrangement to require while mounting/using them\\&. "
"For example, B<systemd-sysext>(8)  only cares for the /usr/ and /opt/ trees "
"inside a disk image, and thus ignores any /home/ partitions (and similar) in "
"all cases, which might be included in the image, regardless whether the "
"configured image policy would allow access to it or not\\&. Similar, "
"B<systemd-nspawn>(1)  is not going to make use of any discovered swap "
"device, regardless if the policy would allow that or not\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"Use the B<image-policy> command of the B<systemd-analyze>(8)  tool to "
"analyze image policy strings, and determine what a specific policy string "
"means for a specific partition\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The following image policy string dictates one read-only Verity-enabled /"
"usr/ partition must exist, plus encrypted root and swap partitions\\&. All "
"other partitions are ignored:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "usr=verity+read-only-on:root=encrypted:swap=encrypted\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The following image policy string dictates an encrypted, writable root file "
"system, and optional /srv/ file system that must be encrypted if it exists "
"and no swap partition may exist:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "root=encrypted+read-only-off:srv=encrypted+absent:swap=absent\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"The following image policy string dictates a single root partition that may "
"be encrypted, but doesn\\*(Aqt have to be, and ignores swap partitions, and "
"uses all other partitions if they are available, possibly with encryption\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "root=unprotected+encrypted:swap=absent+unused:=unprotected+encrypted+absent\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"B<systemd>(1), B<systemd-dissect>(1), B<systemd-gpt-auto-generator>(8), "
"B<systemd-sysext>(8), B<systemd-analyze>(8)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid "Discoverable Partitions Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-40 fedora-rawhide
#: mageia-cauldron
msgid ""
"\\%https://uapi-group.org/specifications/specs/"
"discoverable_partitions_specification"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<verity> for partitions that shall exist and be used, with Verity "
"authentication\\&. (Note: if a DDI image carries a data partition, along "
"with a Verity partition and a signature partition for it, and only the "
"B<verity> flag is set \\(en and B<signed> is not \\(en, then the image will "
"be set up with Verity, but the signature data will not be used\\&. Or in "
"other words: any DDI with a set of partitions that qualify for B<signature> "
"also implicitly qualifies for B<verity>, and in fact B<unprotected>)\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Most systemd components that support operating with disk images support a "
"B<--image-policy=> command line option to specify the image policy to use, "
"and default to relatively open policies by default (typically the \"*\" "
"policy, as described above), under the assumption that trust in disk images "
"is established before the images are passed to the program in question\\&."
msgstr ""
